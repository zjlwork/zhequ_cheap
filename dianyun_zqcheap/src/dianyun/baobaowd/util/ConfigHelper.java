package dianyun.baobaowd.util;

import android.content.Context;
import android.os.Handler;
import android.text.TextUtils;
import dianyun.baobaowd.data.SysConfig;
import dianyun.baobaowd.data.User;
import dianyun.baobaowd.db.LightDBHelper;
import dianyun.baobaowd.dto.ResultDTO;
import dianyun.baobaowd.gson.GsonHelper;
import dianyun.baobaowd.serverinterface.GetConfig;

/**
 * 联系人数据库操作辅助
 * 
 */
public class ConfigHelper
{

	public static void getConfigThread(final Context context, final User user,
			final Handler handler)
	{
		new Thread()
		{

			@Override
			public void run()
			{
				if (user != null)
				{
					ResultDTO resultDTO = new GetConfig(user.getUid(),
							user.getToken()).getConnect();
					if (resultDTO != null && resultDTO.getCode().equals("0"))
					{
						SysConfig sysConfig = GsonHelper.gsonToObj(
								resultDTO.getResult(), SysConfig.class);
						LightDBHelper.setAskDetailPrefix(
								sysConfig.getAskDetailPrefix(), context);
						LightDBHelper.setTopicDetailPrefix(
								sysConfig.getTopicDetailPrefix(), context);
						LightDBHelper.setRewardLevel(context,
								sysConfig.getRewardLevel());
						LightDBHelper.setRegGiveTips(sysConfig.RegGiveTips, context);
						BroadCastHelper.sendRefreshRegivetipsBroadcast(context, GobalConstants.RefreshType.REGIVETIPS,sysConfig.RegGiveTips);
						// 设置柚柚育儿商城的http请求url
						if (!TextUtils.isEmpty(sysConfig.fanliTaeMallUrl))
						{
							LightDBHelper.setShopUrlPrefix(
									sysConfig.fanliTaeMallUrl, context);
						}
						// 老的设置扫一扫的http请求url-----弃用
						if (!TextUtils.isEmpty(sysConfig.GetTbFanLiIem))
						{
							LightDBHelper.setScanCheckUrlPrefix(context,
									sysConfig.GetTbFanLiIem);
						}
                        // 新的设置扫一扫获取短链的http请求url
                        if (!TextUtils.isEmpty(sysConfig.GetTbUrl))
                        {
                            LightDBHelper.setFetchCheckUrlPrefix(context,
                                    sysConfig.GetTbUrl);
                        }

                        // 设置返利淘检出关键字和URL的http请求url
                        if (!TextUtils.isEmpty(sysConfig.ExtFanliContentUrl))
                        {
                            LightDBHelper.setPrimaryWithWordsUrlPrefix(context,
                                    sysConfig.ExtFanliContentUrl);
                        }
                        if (!TextUtils.isEmpty(sysConfig.JiFenQiangSwitch))
                        {
                        	LightDBHelper.setJfqSwitch(
                        			sysConfig.JiFenQiangSwitch,context);
                        }
                        
                        
                        
						if (handler != null)
							handler.sendEmptyMessage(GobalConstants.MessageWhat.REFRESHCONFINGSUCCESS);
					}
					else
					{
						if (handler != null)
							handler.sendEmptyMessage(GobalConstants.MessageWhat.REFRESHCONFINGFAILED);
					}
				}
			}
		}.start();
	}
}
