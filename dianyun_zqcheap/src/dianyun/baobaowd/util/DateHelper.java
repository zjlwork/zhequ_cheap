package dianyun.baobaowd.util;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import android.widget.TextView;
import dianyun.baobaowd.help.LogFile;

public class DateHelper
{

	// public static final String MM_DD_HH_MM = "MM-dd HH:mm";
	public static final String YYYY_MM_DD_HH_MM_SS = "yyyy-MM-dd HH:mm:ss";
	public static final String YYYY_MM_DD_HH_MM = "yyyy-MM-dd HH:mm";
	public static final String YYYY_MM_DD_HH = "yyyy-MM-dd HH";
	// public static final String YYYY_MM_DD_HH_MM_SS_SSS =
	// "yyyy-MM-dd HH:mm:ss.SSS";
	// public static final String YYYY_MM_DD_HH_MM = "yyyy-MM-dd HH:mm";
	public static final String MM_DD_HH_MM = " MM月dd日, HH:mm";
	public static final String MM_DD = " MM月dd日";
	public static final String YYYY_MM_DD = "yyyy-MM-dd";
	public static final String YYYY_MM = "yyyy-MM";
	public static final String DD = "dd";
	public static final String HH_MM = "HH:mm";

	// public static final String HH_MM_SS = "HH:mm:ss";
	public static void setDateTv(Date lDate, TextView mMonthTv,
			TextView mDayTv, TextView mWeekTv)
	{
		String dateStr = DateHelper.getTextByDate(lDate, DateHelper.YYYY_MM_DD);
		String[] date = dateStr.split("-");
		Calendar lCompCal = Calendar.getInstance();
		lCompCal.setTime(lDate);
		String mWay = String.valueOf(lCompCal.get(Calendar.DAY_OF_WEEK));
		if ("1".equals(mWay))
		{
			mWay = "周日";
		}
		else if ("2".equals(mWay))
		{
			mWay = "周一";
		}
		else if ("3".equals(mWay))
		{
			mWay = "周二";
		}
		else if ("4".equals(mWay))
		{
			mWay = "周三";
		}
		else if ("5".equals(mWay))
		{
			mWay = "周四";
		}
		else if ("6".equals(mWay))
		{
			mWay = "周五";
		}
		else if ("7".equals(mWay))
		{
			mWay = "周六";
		}
		mDayTv.setText(date[2]);
		mMonthTv.setText("/" + date[1] + "月");
		mWeekTv.setText(mWay);
	}

	public static String getWeek(Date date)
	{
		Calendar lCompCal = Calendar.getInstance();
		lCompCal.setTime(date);
		String mWay = String.valueOf(lCompCal.get(Calendar.DAY_OF_WEEK));
		if ("1".equals(mWay))
		{
			mWay = "星期日";
		}
		else if ("2".equals(mWay))
		{
			mWay = "星期一";
		}
		else if ("3".equals(mWay))
		{
			mWay = "星期二";
		}
		else if ("4".equals(mWay))
		{
			mWay = "星期三";
		}
		else if ("5".equals(mWay))
		{
			mWay = "星期四";
		}
		else if ("6".equals(mWay))
		{
			mWay = "星期五";
		}
		else if ("7".equals(mWay))
		{
			mWay = "星期六";
		}
		return mWay;
	}

	public static int getWeekInt(Date date)
	{
		Calendar lCompCal = Calendar.getInstance();
		lCompCal.setTime(date);
		return lCompCal.get(Calendar.DAY_OF_WEEK);
	}

	public static String getDateText(String dateText, String oldpattern,
			String newpattern)
	{
		if (null != dateText)
		{
			SimpleDateFormat sdf = new SimpleDateFormat(oldpattern);
			String lOldDateText = null;
			Date lOldDate = null;
			String lNewDateText = null;
			try
			{
				lOldDate = sdf.parse(dateText);
				sdf = new SimpleDateFormat(newpattern);
				lNewDateText = sdf.format(lOldDate);
			}
			catch (Exception e)
			{
				LogFile.SaveExceptionLog(e);
			}
			return lNewDateText;
		}
		return null;
	}

	public static Date getDateByPattern(String dateText, String pattern)
	{
		if (null != dateText)
		{
			SimpleDateFormat sdf = new SimpleDateFormat(pattern);
			Date lDate = null;
			try
			{
				lDate = sdf.parse(dateText);
			}
			catch (Exception e)
			{
				LogFile.SaveExceptionLog(e);
			}
			return lDate;
		}
		return null;
	}

	

	public static String getTextByDate(Date date, String pattern)
	{
		if (null != date)
		{
			SimpleDateFormat sdf = new SimpleDateFormat(pattern);
			String lDateText = null;
			try
			{
				lDateText = sdf.format(date);
			}
			catch (Exception e)
			{
				LogFile.SaveExceptionLog(e);
			}
			return lDateText;
		}
		return null;
	}

	public static String getRefreshTime(Date date)
	{
		SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy  HH:mm:ss");
		return sdf.format(date);
	}

	private static boolean isToday(String datetime)
	{
		Calendar c = Calendar.getInstance();
		Date today = c.getTime();
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		datetime = datetime.trim();
		String ymd = datetime.substring(0, datetime.indexOf(" "));
		return format.format(today).equals(ymd);
	}

	private static boolean isYear(String datetime)
	{
		Calendar c = Calendar.getInstance();
		String nowyear = String.valueOf(c.get(Calendar.YEAR));
		datetime = datetime.trim();
		String year = datetime.substring(0, datetime.indexOf("-"));
		return nowyear.equals(year);
	}

	private static long timeDistance(Date datetime)
	{
		Calendar c = Calendar.getInstance();
		c.set(Calendar.DATE, c.get(Calendar.DATE));
		Date today = c.getTime();
		long dis = today.getTime() - datetime.getTime();
		if (dis < 0)
			return 0;
		else
			return dis;
	}

	public static String getRoleTime(String datetime)
	{
		try
		{
			SimpleDateFormat lSdf = new SimpleDateFormat(YYYY_MM_DD_HH_MM_SS);
			Calendar lCompCal = Calendar.getInstance();
			lCompCal.setTime(lSdf.parse(datetime));
			DecimalFormat df = new DecimalFormat("00");
			if (isToday(datetime))
			{
				long timeDis = timeDistance(lSdf.parse(datetime)) / 1000;
				if(timeDis<60){
					return timeDis + "秒前";
				}
				else if (timeDis < 60 * 60)
					return timeDis / 60 + "分钟前";
				else
				{
					return /* "今天 " + */lCompCal.get(Calendar.HOUR_OF_DAY)
							+ ":" + df.format(lCompCal.get(Calendar.MINUTE));
				}
			}
			else if (isYear(datetime))
			{
				return (lCompCal.get(Calendar.MONTH) + 1) + "-"
						+ lCompCal.get(Calendar.DAY_OF_MONTH) + " "
						+ lCompCal.get(Calendar.HOUR_OF_DAY) + ":"
						+ df.format(lCompCal.get(Calendar.MINUTE));
			}
			else
			{
				return lCompCal.get(Calendar.YEAR) + "-"
						+ (lCompCal.get(Calendar.MONTH) + 1) + "-"
						+ lCompCal.get(Calendar.DAY_OF_MONTH) + " "
						+ lCompCal.get(Calendar.HOUR_OF_DAY) + ":"
						+ df.format(lCompCal.get(Calendar.MINUTE));
			}
		}
		catch (Exception e)
		{
			LogFile.SaveExceptionLog(e);
		}
		finally
		{
		}
		return datetime;
	}

	public static String getRolePerinatalTime(String perinatal,
			String currentTime)
	{
		String role = null;
		try
		{
			Date perinatalDate = getDateByPattern(perinatal, YYYY_MM_DD);
			String[] perinatalTime = perinatal.split("-");
			int perinatal_year = Integer.parseInt(perinatalTime[0]);
			int perinatal_month = Integer.parseInt(perinatalTime[1]);
			int perinatal_day = Integer.parseInt(perinatalTime[2]);
			String[] nowTime = currentTime.split("-");
			int now_year = Integer.parseInt(nowTime[0]);
			int now_month = Integer.parseInt(nowTime[1]);
			int now_day = Integer.parseInt(nowTime[2]);
			Date currentDate = getDateByPattern(currentTime, YYYY_MM_DD);
			if (perinatal.compareTo(currentTime) > 0)
			{
				long dValue = currentDate.getTime()
						+ 24
						* 60
						* 60
						* 1000L
						- (perinatalDate.getTime() - 279 * 24 * 60 * 60 * 1000L);
				long week;
				long day;
				if (dValue > 60L * 60L * 1000L * 24L * 7L)
				{
					week = dValue / (60L * 60L * 1000L * 24L * 7L);
					day = (dValue % (60L * 60L * 1000L * 24L * 7L))
							/ (60L * 60L * 1000L * 24L);
					if (day != 0)
						role = "孕" + week + "周" + day + "天";
					else
						role = "孕" + week + "周";
				}
				else if (dValue > 60L * 60L * 1000L * 24L * 1L)
				{
					day = (int) (dValue / (60L * 60L * 1000L * 24L * 1L));
					role = "孕" + day + "天";
				}
				else
				{
					role = "孕1天";
				}
			}
			else if (perinatal.compareTo(currentTime) < 0)
			{
				// 宝宝生日
				int day = 0;
				int month = 0;
				int year = 0;
				if (now_day >= perinatal_day)
				{
					day = now_day - perinatal_day;
					if (now_month >= perinatal_month)
					{
						month = now_month - perinatal_month;
						year = now_year - perinatal_year;
					}
					else
					{
						year = now_year - perinatal_year - 1;
						month = now_month + 12 - perinatal_month;
					}
				}
				else
				{
					int value;
					// 12 月份为1月 问去年12月借日
					if (now_month == 1)
						value = getMaxDayByMonth((now_year - 1) + "-12")
								- perinatal_day;
					else
						value = getMaxDayByMonth(now_year + "-"
								+ (now_month - 1))
								- perinatal_day;
					if (value < 0)
						value = 0;
					day = value + now_day;
					// 月借一位换日
					month = now_month - 1;
					if (month >= perinatal_month)
					{
						month = month - perinatal_month;
						year = now_year - perinatal_year;
					}
					else
					{
						// 年借一位换月
						year = now_year - perinatal_year - 1;
						month = month + 12 - perinatal_month;
					}
				}
				if (year == 0)
				{
					if (month == 0)
					{
						if (day <= 30)
							role = "宝宝" + (day + 1) + "天";
						else
							role = "宝宝31天";
					}
					else
					{
						if (day != 0)
							role = "宝宝" + month + "个月" + day + "天";
						else
							role = "宝宝" + month + "个月";
					}
				}
				else
				{
					if (month == 0)
					{
						if (day == 0)
							role = "宝宝" + year + "岁";
						else
							role = "宝宝" + year + "岁零" + day + "天";
					}
					else
					{
						if (day != 0)
							role = "宝宝" + year + "岁" + month + "个月" + day + "天";
						else
							role = "宝宝" + year + "岁" + month + "个月";
					}
				}
			}
			else
			{
				role = "宝宝出生";
			}
		}
		catch (Exception e)
		{
			LogFile.SaveExceptionLog(e);
		}
		return role;
	}

	private static int getMaxDayByMonth(String time)
	{
		Calendar rightNow = Calendar.getInstance();
		SimpleDateFormat simpleDate = new SimpleDateFormat(YYYY_MM); // 如果写成年月日的形式的话，要写小d，如："yyyy/MM/dd"
		try
		{
			rightNow.setTime(simpleDate.parse(time)); // 要计算你想要的月份，改变这里即可
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		return rightNow.getActualMaximum(Calendar.DAY_OF_MONTH);
	}
	
}
