package dianyun.baobaowd.util;

import java.io.Serializable;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import dianyun.baobaowd.entity.CateItem;
import dianyun.baobaowd.entity.Constants;
import dianyun.zqcheap.R;

public class NotificationHelper {

    public static final String ACTION_RECEIVE_GOODS = "dianyun.zqcheap.action.open.goods";
    private static NotificationManager mNotificationManager = null;
    // private static final int NORMALID = 0;
    // private static final int NEWTASK = 1;
    // 第一类：我收到的回帖、我收到的回答、被赞、被采纳
    //
    //
    // 第二类：私信
    //
    //
    // 第三类：知识锦囊推送、帖子全局推送
    //
    //
    // 第四类：系统消息
    public static final int NORMALID = 1000;
    public static final int LETTERID = 10000;
    public static int KNOWLEDGETOPICID = 100000;
    public static int SYSTEMID = 1000000;
    public static int RECOMMENT_SHOP_NEWS = 90000001;

    public static String convert2MessTitleByMessID(Context context,
                                                   int messageType) {
        String res = "";
        switch (messageType) {
            case GobalConstants.MessageType.SYSTEMMSG_GOODS:
                res = context.getResources().getString(R.string.shop_choose);
                break;
            case GobalConstants.MessageType.SYSTEMMSG_ACTIVITY:
                // res=context.getResources().getString(R.string.shop_choose);
                break;
            case GobalConstants.MessageType.SYSTEMMSG_EXCHANGE:
                // res=context.getResources().getString(R.string.shop_choose);
                break;
            case GobalConstants.MessageType.SYSTEMMSG_FANLI:
                res = context.getResources().getString(R.string.success_fanli);
                break;
            case GobalConstants.MessageType.SYSTEMMSG_NORMAL:
                res = context.getResources().getString(R.string.sys_msg);
                break;
            case GobalConstants.MessageType.SYSTEMMSG_TIXIAN:
                res = context.getResources().getString(R.string.success_fetch_cash);
                break;
            case GobalConstants.MessageType.SYSTEMMSG_XIADAN:
                res = context.getResources().getString(R.string.success_xiadan_to_pay);
                break;
            case GobalConstants.MessageType.SYSTEMMSG_QIANDAO:
                res = context.getResources().getString(R.string.notify_qiandao);
                break;
            case GobalConstants.MessageType.SYSTEMMSG_XIAJIA:
            	res = context.getResources().getString(R.string.systemmsg);
            	break;
                
                
                
        }
        return res;
    }

    private static NotificationManager getNotificationManager(Context context) {
        if (mNotificationManager == null) {
            mNotificationManager = (NotificationManager) context
                    .getSystemService(Context.NOTIFICATION_SERVICE);
        }
        return mNotificationManager;
    }

    public static void sendNotification(Context context, int id,
                                        Notification notif) {
    	 System.out.println("showNotifyFullyById==" );
        notif.defaults = Notification.DEFAULT_SOUND
                | Notification.DEFAULT_VIBRATE;
        getNotificationManager(context).cancel(id);
        getNotificationManager(context).notify(id, notif);
        // getNotificationManager(context).cancel(id);
        // new GoneThread(id).start();
    }

    /*
     * public static void showAcceptPairNotify(Context context,String str){
     * Notification lNotification = new Notification(R.drawable.logo,str,
     * System.currentTimeMillis()); PendingIntent pdIntent =
     * PendingIntent.getActivity(context, 1, new
     * Intent(),PendingIntent.FLAG_UPDATE_CURRENT);
     * lNotification.setLatestEventInfo(context,str,str,pdIntent);
     * lNotification.flags = Notification.FLAG_AUTO_CANCEL;
     * sendNotification(context,NORMALID,lNotification); } public static void
     * showPairingNotify(Context context,String str){ Notification lNotification
     * = new Notification(R.drawable.logo,str, System.currentTimeMillis());
     * PendingIntent pdIntent = PendingIntent.getActivity(context, 1, new
     * Intent(),PendingIntent.FLAG_UPDATE_CURRENT);
     * lNotification.setLatestEventInfo(context,str,str,pdIntent);
     * lNotification.flags = Notification.FLAG_AUTO_CANCEL;
     * sendNotification(context,NORMALID,lNotification); }
     */
    public static void showNotifyById(Context context, String str,
                                      Intent intent, int notifyId) {
        // Spanned lSpannable = ExpressionHelper.getShowexpressionText(context,
        // str);
        Notification lNotification = new Notification(R.drawable.logo, str,
                System.currentTimeMillis());
        PendingIntent pdIntent = PendingIntent.getActivity(context, 1, intent,
                PendingIntent.FLAG_UPDATE_CURRENT);
        lNotification.setLatestEventInfo(context,
                context.getString(R.string.app_name), str, pdIntent);
        lNotification.flags = Notification.FLAG_AUTO_CANCEL;
        sendNotification(context, notifyId, lNotification);
    }

    public static void showNotifyFullyById(Context context, String title,
                                           String content, Intent intent, int notifyId) {
    	 System.out.println("showNotifyFullyById==" );
        // Spanned lSpannable = ExpressionHelper.getShowexpressionText(context,
        // str);
        Notification lNotification = new Notification(R.drawable.logo, content,
                System.currentTimeMillis());
        PendingIntent pdIntent = PendingIntent.getActivity(context, 1, intent,
                PendingIntent.FLAG_UPDATE_CURRENT);
        lNotification.setLatestEventInfo(context, title, content, pdIntent);
        lNotification.flags = Notification.FLAG_AUTO_CANCEL;
        lNotification.defaults = Notification.DEFAULT_SOUND
                | Notification.DEFAULT_VIBRATE;
        getNotificationManager(context).notify(notifyId, lNotification);
    }

    public static void showNotifyDefineSleftById(Context context, String title,
                                                 CateItem item, int notifyId) {
        if (context != null && !TextUtils.isEmpty(title) && item != null
                && !TextUtils.isEmpty(item.title)) {
            Notification lNotification = new Notification(R.drawable.logo,
                    item.title, System.currentTimeMillis());
            Intent buttonPlayIntent = new Intent(ACTION_RECEIVE_GOODS); // ----设置通知栏按钮广播
            buttonPlayIntent
                    .putExtra(Constants.EXTRA_NAME, (Serializable) item);
            PendingIntent pdIntent = PendingIntent.getBroadcast(context, 0,
                    buttonPlayIntent, PendingIntent.FLAG_UPDATE_CURRENT);
            lNotification.setLatestEventInfo(context, title, item.title,
                    pdIntent);
            lNotification.flags = Notification.FLAG_AUTO_CANCEL;
            lNotification.defaults = Notification.DEFAULT_SOUND
                    | Notification.DEFAULT_VIBRATE;
            getNotificationManager(context).notify(notifyId, lNotification);
        }
    }
    /*
	 * static class GoneThread extends Thread {
	 * 
	 * private int notificationId;
	 * 
	 * public GoneThread(int notificationId) { this.notificationId =
	 * notificationId; }
	 * 
	 * @Override public void run() { try { Thread.sleep(2000); } catch
	 * (InterruptedException e) { e.printStackTrace(); }
	 * MainActivity.getHandler().post(new Runnable() {
	 * 
	 * @Override public void run() {
	 * mNotificationManager.cancel(notificationId); } }); } }
	 */
}
