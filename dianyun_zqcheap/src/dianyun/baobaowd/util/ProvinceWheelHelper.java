package dianyun.baobaowd.util;

import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import android.app.Dialog;
import android.content.Context;
import android.content.res.AssetManager;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import dianyun.baobaowd.provincewheel.ArrayWheelAdapter;
import dianyun.baobaowd.provincewheel.CityModel;
import dianyun.baobaowd.provincewheel.DistrictModel;
import dianyun.baobaowd.provincewheel.OnWheelChangedListener;
import dianyun.baobaowd.provincewheel.ProvinceModel;
import dianyun.baobaowd.provincewheel.WheelView;
import dianyun.baobaowd.provincewheel.XmlParserHandler;
import dianyun.baobaowd.util.DialogHelper.EditDialogCallBack;
import dianyun.zqcheap.R;

public class ProvinceWheelHelper
{

	private WheelView mViewProvince;
	private WheelView mViewCity;
	private WheelView mViewDistrict;
	Button btn_sure;
	Button btn_cancel;
	private Context mContext;
	// TextView mAddressTv;
	String mAddress;

	public ProvinceWheelHelper(Context context, String address)
	{
		this.mContext = context;
		// this.mAddressTv = addressTv;
		this.mAddress = address;
	}

	public void showProvincePicker(Context context,final EditDialogCallBack callback)
	{
		final Dialog dialog = new Dialog(context, R.style.dialognotitle);
		LayoutInflater inflater = LayoutInflater.from(context);
		View view = inflater.inflate(R.layout.provincedialog, null);
		mViewProvince = (WheelView) view.findViewById(R.id.id_province);
		mViewCity = (WheelView) view.findViewById(R.id.id_city);
		mViewDistrict = (WheelView) view.findViewById(R.id.id_district);
//		mViewProvince.setItemHeight(ConversionHelper.dipToPx(50, context));
//		mViewCity.setItemHeight(ConversionHelper.dipToPx(50, context));
//		mViewDistrict.setItemHeight(ConversionHelper.dipToPx(50, context));
		mViewProvince.addChangingListener(new OnWheelChangedListener()
		{

			@Override
			public void onChanged(WheelView wheel, int oldValue, int newValue)
			{
				updateCities();
			}
		});
		// 添加change事件
		mViewCity.addChangingListener(new OnWheelChangedListener()
		{

			@Override
			public void onChanged(WheelView wheel, int oldValue, int newValue)
			{
				updateAreas();
			}
		});
		// 添加change事件
		mViewDistrict.addChangingListener(new OnWheelChangedListener()
		{

			@Override
			public void onChanged(WheelView wheel, int oldValue, int newValue)
			{
				mCurrentDistrictName = mDistrictDatasMap.get(mCurrentCityName)[newValue];
				// mCurrentZipCode = mZipcodeDatasMap.get(mCurrentDistrictName);
			}
		});
		btn_sure = (Button) view.findViewById(R.id.btn_datetime_sure);
		btn_cancel = (Button) view.findViewById(R.id.btn_datetime_cancel);
		// 确定
		btn_sure.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View arg0)
			{
				// mAddressTv.setText(mCurrentProviceName + "-" +
				// mCurrentCityName
				// + "-" + mCurrentDistrictName);
//				Toast.makeText(
//						mContext,
//						"当前选中:" + mCurrentProviceName + "," + mCurrentCityName
//								+ "," + mCurrentDistrictName + ","
//						/* + mCurrentZipCode */, Toast.LENGTH_SHORT).show();
				dialog.dismiss();
				mAddress = mCurrentProviceName+"-"+mCurrentCityName+"-"+mCurrentDistrictName;
				callback.clickOk(mAddress);
			}
		});
		// 取消
		btn_cancel.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View arg0)
			{
				dialog.dismiss();
			}
		});
		setUpData();
		// 设置dialog的布局,并显示
		dialog.setContentView(view);
		dialog.show();
		// layout.addView(view);
		// layout.invalidate();
	}
	String oldProviceName;
	String oldCityName;
	String oldDistrictName;

	private void setUpData()
	{
		if (!TextUtils.isEmpty(mAddress))
		{
			String[] strArray = mAddress.split("-");
			mCurrentProviceName = strArray[0];
			mCurrentCityName = strArray[1];
			mCurrentDistrictName = strArray[2];
			oldProviceName = strArray[0];
			oldCityName = strArray[1];
			oldDistrictName = strArray[2];
		}
		initProvinceDatas();
		mViewProvince.setViewAdapter(new ArrayWheelAdapter<String>(mContext,
				mProvinceDatas));
		mViewProvince
				.setCurrentItem(getPosition(oldProviceName, mProvinceDatas));
		// 设置可见条目数量
//		mViewProvince.setVisibleItems(3);
//		mViewCity.setVisibleItems(3);
//		mViewDistrict.setVisibleItems(3);
		initCities(oldCityName);
		initAreas(oldDistrictName);
	}

	/**
	 * 根据当前的省，更新市WheelView的信息
	 */
	private void updateCities()
	{
		int pCurrent = mViewProvince.getCurrentItem();
		mCurrentProviceName = mProvinceDatas[pCurrent];
		String[] cities = mCitisDatasMap.get(mCurrentProviceName);
		if (cities == null)
		{
			cities = new String[] { "" };
		}
		mViewCity
				.setViewAdapter(new ArrayWheelAdapter<String>(mContext, cities));
		mViewCity.setCurrentItem(0);
		updateAreas();
	}

	/**
	 * 根据当前的市，更新区WheelView的信息
	 */
	private void updateAreas()
	{
		int pCurrent = mViewCity.getCurrentItem();
		mCurrentCityName = mCitisDatasMap.get(mCurrentProviceName)[pCurrent];
		String[] areas = mDistrictDatasMap.get(mCurrentCityName);
		if (areas == null)
		{
			areas = new String[] { "" };
		}
		mViewDistrict.setViewAdapter(new ArrayWheelAdapter<String>(mContext,
				areas));
		mViewDistrict.setCurrentItem(0);
		mCurrentDistrictName = areas[0];
	}

	/**
	 * 根据当前的省，更新市WheelView的信息
	 */
	private void initCities(String city)
	{
		int pCurrent = mViewProvince.getCurrentItem();
		mCurrentProviceName = mProvinceDatas[pCurrent];
		String[] cities = mCitisDatasMap.get(mCurrentProviceName);
		if (cities == null)
		{
			cities = new String[] { "" };
		}
		mViewCity
				.setViewAdapter(new ArrayWheelAdapter<String>(mContext, cities));
		mViewCity.setCurrentItem(getPosition(city, cities));
	}

	/**
	 * 根据当前的市，更新区WheelView的信息
	 */
	private void initAreas(String district)
	{
		int pCurrent = mViewCity.getCurrentItem();
		mCurrentCityName = mCitisDatasMap.get(mCurrentProviceName)[pCurrent];
		String[] areas = mDistrictDatasMap.get(mCurrentCityName);
		if (areas == null)
		{
			areas = new String[] { "" };
		}
		mViewDistrict.setViewAdapter(new ArrayWheelAdapter<String>(mContext,
				areas));
		mViewDistrict.setCurrentItem(getPosition(district, areas));
	}
	/**
	 * 所有省
	 */
	protected String[] mProvinceDatas;
	/**
	 * key - 省 value - 市
	 */
	protected Map<String, String[]> mCitisDatasMap = new HashMap<String, String[]>();
	/**
	 * key - 市 values - 区
	 */
	protected Map<String, String[]> mDistrictDatasMap = new HashMap<String, String[]>();
	/**
	 * key - 区 values - 邮编
	 */
	protected Map<String, String> mZipcodeDatasMap = new HashMap<String, String>();
	/**
	 * 当前省的名称
	 */
	protected String mCurrentProviceName;
	/**
	 * 当前市的名称
	 */
	protected String mCurrentCityName;
	/**
	 * 当前区的名称
	 */
	protected String mCurrentDistrictName = "";

	/**
	 * 当前区的邮政编码
	 */
	// protected String mCurrentZipCode = "";
	private int getPosition(String str, String[] strArray)
	{
		if(TextUtils.isEmpty(str)||strArray==null)return 0;
		if (strArray != null && strArray.length > 0)
		{
			for (int i = 0; i < strArray.length; i++)
			{
				if (strArray[i].equals(str))
					return i;
			}
		}
		return 0;
	}

	/*
	 * private int getCityPosition(String city,List<CityModel> cityList){
	 * if(cityList!=null&&cityList.size()>0){ for(int
	 * i=0;i<cityList.size();i++){ CityModel lCityModel = cityList.get(i);
	 * if(lCityModel.getName().equals(city))return i; } } return 0; } private
	 * int getDistrictPosition(String district,List<DistrictModel>
	 * districtList){ if(districtList!=null&&districtList.size()>0){ for(int
	 * i=0;i<districtList.size();i++){ DistrictModel lDistrictModel =
	 * districtList.get(i); if(lDistrictModel.getName().equals(district))return
	 * i; } } return 0; }
	 */
	/**
	 * 解析省市区的XML数据
	 */
	private void initProvinceDatas()
	{
		List<ProvinceModel> provinceList = null;
		AssetManager asset = mContext.getAssets();
		try
		{
			InputStream input = asset.open("province_data.xml");
			// 创建一个解析xml的工厂对象
			SAXParserFactory spf = SAXParserFactory.newInstance();
			// 解析xml
			SAXParser parser = spf.newSAXParser();
			XmlParserHandler handler = new XmlParserHandler();
			parser.parse(input, handler);
			input.close();
			// 获取解析出来的数据
			provinceList = handler.getDataList();
			// */ 初始化默认选中的省、市、区
			if (provinceList != null && !provinceList.isEmpty())
			{
				if (TextUtils.isEmpty(mCurrentProviceName))
					mCurrentProviceName = provinceList.get(0).getName();
				List<CityModel> cityList = provinceList.get(0).getCityList();
				if (cityList != null && !cityList.isEmpty())
				{
					if (TextUtils.isEmpty(mCurrentCityName))
						mCurrentCityName = cityList.get(0).getName();
					List<DistrictModel> districtList = cityList.get(0)
							.getDistrictList();
					if (TextUtils.isEmpty(mCurrentDistrictName))
						mCurrentDistrictName = districtList.get(0).getName();
					// mCurrentZipCode = districtList.get(0).getZipcode();
				}
			}
			// */
			mProvinceDatas = new String[provinceList.size()];
			for (int i = 0; i < provinceList.size(); i++)
			{
				// 遍历所有省的数据
				mProvinceDatas[i] = provinceList.get(i).getName();
				List<CityModel> cityList = provinceList.get(i).getCityList();
				String[] cityNames = new String[cityList.size()];
				for (int j = 0; j < cityList.size(); j++)
				{
					// 遍历省下面的所有市的数据
					cityNames[j] = cityList.get(j).getName();
					List<DistrictModel> districtList = cityList.get(j)
							.getDistrictList();
					String[] distrinctNameArray = new String[districtList
							.size()];
					DistrictModel[] distrinctArray = new DistrictModel[districtList
							.size()];
					for (int k = 0; k < districtList.size(); k++)
					{
						// 遍历市下面所有区/县的数据
						DistrictModel districtModel = new DistrictModel(
								districtList.get(k).getName(), districtList
										.get(k).getZipcode());
						// 区/县对于的邮编，保存到mZipcodeDatasMap
						mZipcodeDatasMap.put(districtList.get(k).getName(),
								districtList.get(k).getZipcode());
						distrinctArray[k] = districtModel;
						distrinctNameArray[k] = districtModel.getName();
					}
					// 市-区/县的数据，保存到mDistrictDatasMap
					mDistrictDatasMap.put(cityNames[j], distrinctNameArray);
				}
				// 省-市的数据，保存到mCitisDatasMap
				mCitisDatasMap.put(provinceList.get(i).getName(), cityNames);
			}
		}
		catch (Throwable e)
		{
			e.printStackTrace();
		}
		finally
		{
		}
	}
}
