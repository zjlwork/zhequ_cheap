package dianyun.baobaowd.util;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.lang.ref.SoftReference;
import java.net.URL;
import java.util.HashMap;
import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Handler;
import android.os.Message;
import android.widget.ImageView;
import dianyun.baobaowd.data.Attachment;
import dianyun.baobaowd.help.LogFile;

public class AsycnBitmapLoader2
{

	private static final int MSG_GETLOCALIMAGE_SUCCESS = 3;
	private static final int MSG_GET_IMAGE_SUCCESS = 1;
	private static final int MSG_GET_IMAGE_FAILED = 2;

	public static AsycnBitmapLoader2 getInstance(Context context)
	{
		// System.out.println("imagecache.size========="+imagecache.size());
		return new AsycnBitmapLoader2(context);
	}
	/*
	 * private static AsycnBitmapLoader mAsycnBitmapLoader; public static
	 * AsycnBitmapLoader getInstance(Context context) { if(null ==
	 * mAsycnBitmapLoader) { mAsycnBitmapLoader = new
	 * AsycnBitmapLoader(context); } return mAsycnBitmapLoader; }
	 */private Handler handler;
	private static HashMap<String, SoftReference<Bitmap>> imagecache = new HashMap<String, SoftReference<Bitmap>>();
	private Context context;
	private ImageCallback imageCallback;
	private ImageView imageView;

	public interface DownloadProgress
	{

		public void updateProgress(int totalSize, int downLoadSize,
				Attachment attachment, boolean isFinish);
	}

	public interface ImageCallback
	{

		public void imageLoad(ImageView imageView, Bitmap bitmap);
	}

	public AsycnBitmapLoader2(Context context)
	{
		this.context = context;
		handler = new Handler()
		{

			@Override
			public void handleMessage(android.os.Message msg)
			{
				if (msg.what == MSG_GET_IMAGE_SUCCESS)
				{
					Bitmap bitmap = (Bitmap) msg.obj;
					if (imageCallback != null)
						imageCallback.imageLoad(imageView, bitmap);
				}
				else
					if (msg.what == MSG_GET_IMAGE_FAILED)
					{
						imageCallback.imageLoad(imageView, null);
					}
			};
		};
	}

	public Bitmap getBitmap(final String imageUrl, final String pre)
	{
		if (imageUrl == null || imageUrl.equals(""))
			return null;
		// 内存缓冲
		if (imagecache.containsKey(imageUrl))
		{
			SoftReference<Bitmap> ref = imagecache.get(imageUrl);
			Bitmap bitmap = ref.get();
			if (null != bitmap)
			{
				// Log.i("imageloader", "imagecache中找到        "+imageUrl);
				return bitmap;
			}
			else
			{
				return getBitmapByCacheFile(imageUrl, pre);
			}
		}
		// 本地缓冲
		else
		{
			return getBitmapByCacheFile(imageUrl, pre);
		}
	}

	public Bitmap getBitmapByCache(final String imageUrl, final String pre)
	{
		if (imageUrl == null || imageUrl.equals(""))
			return null;
		// 内存缓冲
		if (imagecache.containsKey(imageUrl))
		{
			SoftReference<Bitmap> ref = imagecache.get(imageUrl);
			Bitmap bitmap = ref.get();
			if (null != bitmap)
			{
				return bitmap;
			}
		}
		return null;
	}

	public Bitmap getBitmapByCacheFile(final String imageUrl, final String pre)
	{
		Bitmap bitmap = null;
		String bitmapName = imageUrl.substring(imageUrl.lastIndexOf("/") + 1);
		if (pre != null)
			bitmapName = pre + bitmapName;
		try
		{
			File file = context.getExternalCacheDir();
			File imagefile = new File(file.getAbsolutePath() + File.separator
					+ bitmapName);
			if (imagefile.isFile() && imagefile.exists())
			{
				System.out.println("decodefile qian  ======="
						+ System.currentTimeMillis());
				bitmap = BitmapFactory.decodeFile(imagefile.getAbsolutePath());
				System.out.println("decodefile hou =============="
						+ System.currentTimeMillis());
				if (bitmap != null)
					imagecache.put(imageUrl, new SoftReference<Bitmap>(bitmap));
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			return null;
		}
		return bitmap;
	}

	public void downloadBitmapWithProgress(final String imageUrl,
			final String pre, final DownloadProgress downloadProgress)
	{
		// 从网络获取(启动一个线程异步操作)
		new Thread()
		{

			@Override
			public void run()
			{
				try
				{
					// 放入本地缓存
					File file = context.getExternalCacheDir();
					String bitmapName = imageUrl.substring(imageUrl
							.lastIndexOf("/") + 1);
					if (pre != null)
						bitmapName = pre + bitmapName;
					File bitmapfile = new File(file.getAbsoluteFile()
							+ File.separator + bitmapName);
					if (!bitmapfile.exists())
					{
						boolean result = bitmapfile.createNewFile();
					}
					else
					{
						boolean deleteresult = bitmapfile.delete();
						boolean result = bitmapfile.createNewFile();
					}
					try
					{
						DefaultHttpClient lClient = new DefaultHttpClient();
						HttpGet lHttpGet = new HttpGet(imageUrl);
						HttpResponse lRes = lClient.execute(lHttpGet);
						if (lRes.getStatusLine().getStatusCode() == 200)
						{
							InputStream zipin = lRes.getEntity().getContent();
							Header contentEncoding = lRes
									.getFirstHeader("Content-Length");
							int totalSize = Integer.parseInt(contentEncoding
									.getValue());
							FileOutputStream out = new FileOutputStream(
									bitmapfile);
							if (totalSize > 0 && zipin != null)
							{
								int downloaded = 0;
								int l = 0;
								byte[] b = new byte[10240];
								while ((l = zipin.read(b)) != -1)
								{
									// if(disrupt){
									// downloadProgress.updateProgress(totalSize,downloaded,null,true);
									// disrupt = false;
									// return null;
									// }
									downloaded += l;
									out.write(b, 0, l);
									downloadProgress.updateProgress(totalSize,
											downloaded, null, false);
									/*
									 * try { Thread.sleep(200); } catch
									 * (InterruptedException e) {
									 * 
									 * LogFile.SaveExceptionLog(e); }
									 */
								}
								downloadProgress.updateProgress(totalSize,
										downloaded, null, true);
								Bitmap temp_bitmap = BitmapFactory
										.decodeFile(bitmapfile
												.getAbsolutePath());
								imagecache.put(imageUrl,
										new SoftReference<Bitmap>(temp_bitmap));
								// Message msg = new Message();
								// msg.what = MSG_GET_IMAGE_SUCCESS;
								// msg.obj = temp_bitmap;
								// handler.sendMessage(msg);
							}
							else
								if (totalSize == -1 && zipin != null)
								{
									int l = 0;
									byte[] b = new byte[10240];
									while ((l = zipin.read(b)) != -1)
									{
										out.write(b, 0, l);
									}
									downloadProgress.updateProgress(totalSize,
											-1, null, true);
								}
							out.flush();
							out.close();
							zipin.close();
						}
					}
					catch (Exception e)
					{
						LogFile.SaveExceptionLog(e);
						downloadProgress.updateProgress(100, 0, null, true);
					}
				}
				catch (Exception e)
				{
					LogFile.SaveExceptionLog(e);
				}
			};
		}.start();
	}

	public void downloadBitmap(ImageView imageView, final String imageUrl,
			final String pre, ImageCallback callback)
	{
		this.imageCallback = callback;
		this.imageView = imageView;
		// 从网络获取(启动一个线程异步操作)
		new Thread()
		{

			@Override
			public void run()
			{
				try
				{
					InputStream is = (new URL(imageUrl).openStream());
					Bitmap temp_bitmap = BitmapFactory.decodeStream(is);
					Message msg = new Message();
					msg.what = MSG_GET_IMAGE_SUCCESS;
					msg.obj = temp_bitmap;
					handler.sendMessage(msg);
					// 放入内存
					imagecache.put(imageUrl, new SoftReference<Bitmap>(
							temp_bitmap));
					// System.out.println("放入内存   imageUrl===="+imageUrl);
					// 放入本地缓存
					File file = context.getExternalCacheDir();
					String bitmapName = imageUrl.substring(imageUrl
							.lastIndexOf("/") + 1);
					if (pre != null)
						bitmapName = pre + bitmapName;
					File bitmapfile = new File(file.getAbsoluteFile()
							+ File.separator + bitmapName);
					if (!bitmapfile.exists())
					{
						boolean result = bitmapfile.createNewFile();
						// System.out.println("bitmapfile.createNewFile()="+result+"   "+imageUrl);
					}
					else
					{
						boolean deleteresult = bitmapfile.delete();
						boolean result = bitmapfile.createNewFile();
						// System.out.println("bitmapfile.deleteresult()="+deleteresult+"    createNewFile="+result+"   "+imageUrl);
					}
					FileOutputStream fos = new FileOutputStream(bitmapfile);
					temp_bitmap.compress(Bitmap.CompressFormat.PNG, 100, fos);
					fos.close();
				}
				catch (Exception e)
				{
					LogFile.SaveExceptionLog(e);
					Message msg = new Message();
					msg.what = MSG_GET_IMAGE_FAILED;
					msg.obj = null;
					handler.sendMessage(msg);
				}
			};
		}.start();
	}

	public void putBitmapToCache(Bitmap bitmap, String localFilePath)
	{
		imagecache.put(localFilePath, new SoftReference<Bitmap>(bitmap));
	}

	public Bitmap getBitmapFromCache(String localFilePath)
	{
		if (imagecache.containsKey(localFilePath))
		{
			SoftReference<Bitmap> ref = imagecache.get(localFilePath);
			Bitmap bitmap = ref.get();
			return bitmap;
		}
		return null;
	}

	public String saveBitmapToCacheFile(Bitmap bitmap, final String fileName,
			final String pre)
	{
		try
		{
			File file = context.getExternalCacheDir();
			String bitmapName = pre + fileName;
			File bitmapfile = new File(file.getAbsoluteFile() + File.separator
					+ bitmapName);
			if (!bitmapfile.exists())
			{
				boolean result = bitmapfile.createNewFile();
			}
			else
			{
				boolean deleteresult = bitmapfile.delete();
				boolean result = bitmapfile.createNewFile();
			}
			FileOutputStream fos = new FileOutputStream(bitmapfile);
			if (bitmapName.endsWith(".jpg") || bitmapName.endsWith(".JPG"))
			{
				bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fos);
			}
			else
			{
				bitmap.compress(Bitmap.CompressFormat.PNG, 100, fos);
			}
			fos.close();
			return bitmapfile.getAbsolutePath();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		return null;
	}
	/*
	 * public boolean loadBitmap(ImageView imageView,final String imageUrl,final
	 * String pre,ImageCallback callback2){
	 * 
	 * if(imageUrl==null||imageUrl.equals("")) return false;
	 * this.imageCallback2=callback2; if(imagecache.containsKey(imageUrl)) {
	 * SoftReference<Bitmap> ref = imagecache.get(imageUrl); Bitmap bitmap =
	 * ref.get();
	 * 
	 * if(null != bitmap) { imageView.setImageBitmap(bitmap); return true;
	 * }else{ String bitmapName =
	 * imageUrl.substring(imageUrl.lastIndexOf("/")+1); if(pre!=null)
	 * bitmapName=pre+bitmapName; File file = context.getExternalCacheDir();
	 * final File imagefile = new
	 * File(file.getAbsolutePath()+File.separator+bitmapName);
	 * if(imagefile.isFile()&&imagefile.exists()){ new Thread(){ public void
	 * run(){ Bitmap bitmap =
	 * BitmapFactory.decodeFile(imagefile.getAbsolutePath()); if(bitmap!=null){
	 * imagecache.put(imageUrl, new SoftReference<Bitmap>(bitmap)); Message msg
	 * = new Message(); msg.what = MSG_GETLOCALIMAGE_SUCCESS; msg.obj = bitmap;
	 * handler.sendMessage(msg); } } }.start(); return true; } } } return false;
	 * }
	 */
	/*
	 * private void loadBitmapByCacheFile(ImageView imageView,final String
	 * imageUrl,final String pre){
	 * 
	 * 
	 * new Thread(){ public void run(){ Bitmap bitmap = null; String bitmapName
	 * = imageUrl.substring(imageUrl.lastIndexOf("/")+1); if(pre!=null)
	 * bitmapName=pre+bitmapName; try{ File file =
	 * context.getExternalCacheDir(); File imagefile = new
	 * File(file.getAbsolutePath()+File.separator+bitmapName);
	 * if(imagefile.isFile()&&imagefile.exists()){
	 * System.out.println("imagefile.getAbsolutePath()====="
	 * +imagefile.getAbsolutePath()); bitmap =
	 * BitmapFactory.decodeFile(imagefile.getAbsolutePath()); if(bitmap!=null)
	 * imagecache.put(imageUrl, new SoftReference<Bitmap>(bitmap)); }
	 * }catch(Exception e){ e.printStackTrace(); } } }.start();
	 * 
	 * 
	 * }
	 */
}
