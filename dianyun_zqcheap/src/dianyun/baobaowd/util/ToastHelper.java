package dianyun.baobaowd.util;

import java.io.File;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.provider.MediaStore;
import android.text.ClipboardManager;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.TranslateAnimation;
import android.widget.AbsoluteLayout;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.nostra13.universalimageloader.core.ImageLoader;

import dianyun.baobaowd.db.LightDBHelper;
import dianyun.baobaowd.defineview.TipBackFeesPopup;
import dianyun.baobaowd.entity.CateItem;
import dianyun.baobaowd.interfaces.AdoptCallBack;
import dianyun.baobaowd.interfaces.ChooseImgCallBack;
import dianyun.baobaowd.interfaces.ChoosePriceWindowCallBack;
import dianyun.baobaowd.interfaces.DeleteWindowCallBack;
import dianyun.baobaowd.interfaces.DialogCallBack;
import dianyun.baobaowd.interfaces.GoPageDialogCallBack;
import dianyun.baobaowd.interfaces.IDialogCallback;
import dianyun.baobaowd.interfaces.ReportShieldDeleteWindowCallBack;
import dianyun.baobaowd.interfaces.ShareFavGoPageWindowCallBack;
import dianyun.baobaowd.interfaces.ShareFavWindowCallBack;
import dianyun.baobaowd.interfaces.ShareWindowCallBack;
import dianyun.zqcheap.R;
import dianyun.zqcheap.activity.LoginRegisterActivity;
import dianyun.zqcheap.activity.MainActivity;
import dianyun.zqcheap.activity.ShowScanResultActivity;
import dianyun.zqcheap.activity.TaskActivity;
import dianyun.zqcheap.application.BaoBaoWDApplication;
//import com.iflytek.ui.RecognizerDialog;
//import com.iflytek.speech.SpeechConfig.RATE;
//import com.iflytek.speech.SpeechError;
//import com.iflytek.ui.RecognizerDialog;
//import com.iflytek.ui.RecognizerDialogListener;

public class ToastHelper {

	// 缓存，保存当前的引擎参数到下一次启动应用程序使用.
	private static SharedPreferences mSharedPreferences;
	public static final String ENGINE_POI = "poi";
	// 识别Dialog
	// private static RecognizerDialog iatDialog;
	// 初始化参数
	private static String mInitParams;

	// private static ProgressDialog progressDialog;
	public static void show(final Context context, final String content) {
		MainActivity.getHandler().post(new Runnable() {

			@Override
			public void run() {
				Toast.makeText(context, content, Toast.LENGTH_SHORT).show();
			}
		});
	}

	public static void toastShow(final Context context, final String content) {
		Toast.makeText(context, content, Toast.LENGTH_SHORT).show();
	}

	public static void showByGravity(final Context context,
			final String content, int gravity) {
		Toast toast = Toast.makeText(context, content, Toast.LENGTH_SHORT);
		toast.setGravity(gravity, 0, 0);
		toast.show();
	}

	public static int getScreenWidth(Context context, int paddingdp) {
		DisplayMetrics dm = new DisplayMetrics();
		dm = context.getResources().getDisplayMetrics();
		int screenWidth = dm.widthPixels;
		return screenWidth - ConversionHelper.dipToPx(paddingdp, context);
	}

	public static int getScreenWidth(Context context) {
		DisplayMetrics dm = new DisplayMetrics();
		dm = context.getResources().getDisplayMetrics();
		int screenWidth = dm.widthPixels;
		return screenWidth;
	}

	public static int getScreenHeight(Context context) {
		DisplayMetrics dm = new DisplayMetrics();
		dm = context.getResources().getDisplayMetrics();
		return dm.heightPixels;
	}

	public static int getBarHeight(Context context) {
		// 获取状态栏高度
		Rect frame = new Rect();
		((Activity) context).getWindow().getDecorView()
				.getWindowVisibleDisplayFrame(frame);
		return frame.top;
	}
	
	public static void full(Activity context ,boolean enable) {
        if (enable) {
            WindowManager.LayoutParams lp = context.getWindow().getAttributes();
            lp.flags |= WindowManager.LayoutParams.FLAG_FULLSCREEN;
            context.getWindow().setAttributes(lp);
            context.getWindow().addFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
        } else {
            WindowManager.LayoutParams attr = context.getWindow().getAttributes();
            attr.flags &= (~WindowManager.LayoutParams.FLAG_FULLSCREEN);
            context. getWindow().setAttributes(attr);
            context.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
        }
    }
//	ToastHelper.getBarHeight(MainActivity.this)

	public static void showGuideDialog(final Context context, View parent,
			int[] scanLocation, int[] qiandaoLocation) {
		if (mPopupWindow == null || !mPopupWindow.isShowing()) {
			View pview = LayoutInflater.from(context).inflate(
					R.layout.showguidedialog, null, false);
			AbsoluteLayout root_layout = (AbsoluteLayout) pview
					.findViewById(R.id.root_layout);
			root_layout.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
				}
			});
			

			View guide1_layout = pview.findViewById(R.id.shop_scan_pop_lay);
			View guide2_layout = pview
					.findViewById(R.id.main_fragment_module_qiandao_lay);
			final View hint1_layout = pview.findViewById(R.id.hint1_layout);
			final View hint2_layout = pview.findViewById(R.id.hint2_layout);
			
			int screenWidth = ToastHelper.getScreenWidth(context);
			guide1_layout
					.setLayoutParams(new AbsoluteLayout.LayoutParams(
							scanLocation[2] - scanLocation[0], scanLocation[3]
									- scanLocation[1], scanLocation[0],
							scanLocation[1]));
			hint1_layout.setLayoutParams(new AbsoluteLayout.LayoutParams(
					hint1_layout.getLayoutParams().width, hint1_layout
							.getLayoutParams().height,
					(screenWidth - hint1_layout.getLayoutParams().width) / 2,
					scanLocation[1] - hint1_layout.getLayoutParams().height
							- ConversionHelper.dipToPx(10, context)));
		
			
			int padding = ConversionHelper.dipToPx(0, context);
			guide2_layout
					.setLayoutParams(new AbsoluteLayout.LayoutParams(
							qiandaoLocation[2] - qiandaoLocation[0] - padding
									* 2, qiandaoLocation[3]
									- qiandaoLocation[1] - padding * 2,
							qiandaoLocation[0] + padding, qiandaoLocation[1]
									+ padding));

			int top = (qiandaoLocation[3] - qiandaoLocation[1] - hint2_layout
					.getLayoutParams().height) / 2 + qiandaoLocation[1];
			hint2_layout.setLayoutParams(new AbsoluteLayout.LayoutParams(
					hint2_layout.getLayoutParams().width, hint2_layout
							.getLayoutParams().height, qiandaoLocation[2]
							+ ConversionHelper.dipToPx(10, context), top));

			// guide1_layout.layout(scanLocation[0], scanLocation[1],
			// scanLocation[2],scanLocation[3]);
			// guide2_layout.layout(qiandaoLocation[0], qiandaoLocation[1],
			// qiandaoLocation[2],qiandaoLocation[3]);

			final TranslateAnimation lTranslateAnimation=new TranslateAnimation(0, 50, 0, 0);
			lTranslateAnimation.setDuration(500);
			hint2_layout.startAnimation(lTranslateAnimation);
			lTranslateAnimation.setAnimationListener(new AnimationListener() {
				@Override
				public void onAnimationStart(Animation animation) {
				}
				@Override
				public void onAnimationRepeat(Animation animation) {
				}
				@Override
				public void onAnimationEnd(Animation animation) {
					hint2_layout.layout(hint2_layout.getLeft()+50, hint2_layout.getTop(), hint2_layout.getRight()+50, hint2_layout.getBottom());
					TranslateAnimation lTranslateAnimation2=new TranslateAnimation(0, -50, 0, 0);
					lTranslateAnimation2.setAnimationListener(new AnimationListener() {
						@Override
						public void onAnimationStart(Animation animation) {}
						
						@Override
						public void onAnimationRepeat(Animation animation) {}
						
						@Override
						public void onAnimationEnd(Animation animation) {
							hint2_layout.layout(hint2_layout.getLeft()-50, hint2_layout.getTop(), hint2_layout.getRight()-50, hint2_layout.getBottom());
							hint2_layout.clearAnimation();
							hint2_layout.startAnimation(lTranslateAnimation);
						}
					});
					lTranslateAnimation2.setDuration(500);
					hint2_layout.clearAnimation();
					hint2_layout.startAnimation(lTranslateAnimation2);
				}
			});
			final TranslateAnimation lTranslateAnimation3=new TranslateAnimation(0, 0, 0, -50);
			lTranslateAnimation3.setDuration(500);
			hint1_layout.startAnimation(lTranslateAnimation3);
			lTranslateAnimation3.setAnimationListener(new AnimationListener() {
				@Override
				public void onAnimationStart(Animation animation) {
				}
				@Override
				public void onAnimationRepeat(Animation animation) {
				}
				@Override
				public void onAnimationEnd(Animation animation) {
					hint1_layout.layout(hint1_layout.getLeft(), hint1_layout.getTop()-50, hint1_layout.getRight(), hint1_layout.getBottom()-50);
					TranslateAnimation lTranslateAnimation4=new TranslateAnimation(0, 0, 0, 50);
					lTranslateAnimation4.setAnimationListener(new AnimationListener() {
						@Override
						public void onAnimationStart(Animation animation) {}
						
						@Override
						public void onAnimationRepeat(Animation animation) {}
						
						@Override
						public void onAnimationEnd(Animation animation) {
							hint1_layout.layout(hint1_layout.getLeft(), hint1_layout.getTop()+50, hint1_layout.getRight(), hint1_layout.getBottom()+50);
							hint1_layout.clearAnimation();
							hint1_layout.startAnimation(lTranslateAnimation3);
						}
					});
					lTranslateAnimation4.setDuration(500);
					hint1_layout.clearAnimation();
					hint1_layout.startAnimation(lTranslateAnimation4);
				}
			});
			pview.findViewById(R.id.bt).setOnClickListener(
					new OnClickListener() {

						@Override
						public void onClick(View v) {
							mPopupWindow.dismiss();
							mPopupWindow = null;
						}
					});
			mPopupWindow = new PopupWindow(pview,
					android.view.ViewGroup.LayoutParams.MATCH_PARENT,
					android.view.ViewGroup.LayoutParams.MATCH_PARENT, true);
			mPopupWindow.setFocusable(true);
//			mPopupWindow.setTouchable(true);
//			mPopupWindow.setOutsideTouchable(true);
//			mPopupWindow.setBackgroundDrawable(new BitmapDrawable());
			mPopupWindow.showAtLocation(parent, Gravity.BOTTOM, 0, 0);
		}
	}
	public static void showGuideLoginDialog(final Context context, View parent) {
		if(parent==null||!UserHelper.isGusetUser(context))return;
		if (mPopupWindow == null || !mPopupWindow.isShowing()) {
			View pview = LayoutInflater.from(context).inflate(
					R.layout.guidelogindialog, null, false);
			TextView hint_tv = (TextView) pview
					.findViewById(R.id.hint_tv);
			Button login_bt = (Button) pview
					.findViewById(R.id.login_bt);
			login_bt.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					 if (UserHelper.isGusetUser(context)) {
				            UserHelper.gusestUserGo(context);
				     }
				}
			});
			hint_tv.setText(context.getString(R.string.login_rebate));
			
			mPopupWindow = new PopupWindow(pview,
					android.view.ViewGroup.LayoutParams.MATCH_PARENT,
					android.view.ViewGroup.LayoutParams.WRAP_CONTENT, true);
			mPopupWindow.setFocusable(true);
//			mPopupWindow.setTouchable(true);
//			mPopupWindow.setOutsideTouchable(true);
//			mPopupWindow.setBackgroundDrawable(new BitmapDrawable());
			mPopupWindow.showAtLocation(parent, Gravity.BOTTOM, 0,
					-ConversionHelper.dipToPx(50, context));
		}
	}

	public static void showScoreWallDialog(final Context context) {

		View pview = LayoutInflater.from(context).inflate(
				R.layout.scorewalldialog, null);
		final Dialog dialog = new Dialog(context, R.style.dialognotitle);
		dialog.setContentView(pview);
		dialog.show();
		LinearLayout read_layout = (LinearLayout) pview
				.findViewById(R.id.read_layout);
		final ImageView read_iv = (ImageView) pview.findViewById(R.id.read_iv);
		LinearLayout notice_layout = (LinearLayout) pview
				.findViewById(R.id.notice_layout);
		final ImageView notice_iv = (ImageView) pview
				.findViewById(R.id.notice_iv);
		final Button ok_bt = (Button) pview.findViewById(R.id.ok_bt);

		if (LightDBHelper.getNotNoticeAgain(context))
			notice_iv.setSelected(true);
		else
			notice_iv.setSelected(false);

		read_iv.setSelected(true);
		if (read_iv.isSelected()) {
			ok_bt.setEnabled(true);
			ok_bt.setSelected(false);
		} else {
			ok_bt.setEnabled(false);
			ok_bt.setSelected(true);
		}

		ok_bt.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (notice_iv.isSelected())
					LightDBHelper.setNotNoticeAgain(context, true);
				else
					LightDBHelper.setNotNoticeAgain(context, false);
				dialog.cancel();
			}
		});
		read_layout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				dialog.cancel();
				WebInsideHelper.goWebInside(context,
						GobalConstants.URL.WEBHTMLBASE
								+ GobalConstants.URL.TREATY,
						context.getString(R.string.clause));

			}
		});
		read_iv.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (read_iv.isSelected()) {
					read_iv.setSelected(false);
					ok_bt.setEnabled(false);
					ok_bt.setSelected(true);
				} else {
					read_iv.setSelected(true);
					ok_bt.setEnabled(true);
					ok_bt.setSelected(false);
				}
			}
		});
		notice_layout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (notice_iv.isSelected()) {
					notice_iv.setSelected(false);
				} else {
					notice_iv.setSelected(true);
				}
			}
		});
	}
	public static void showMaioShaItemDetailDialog(final Context context,CateItem mItem,String status) {
		
		View pview = LayoutInflater.from(context).inflate(
				R.layout.miaosha_listitemdetail, null);
		final Dialog dialog = new Dialog(context, R.style.dialognotitle);
		dialog.setContentView(pview);
		dialog.show();
		ImageView goods_image = (ImageView) pview
				.findViewById(R.id.goods_image);
		TextView goods_title_tv = (TextView) pview
				.findViewById(R.id.goods_title_tv);

		TextView goods_backfeesPrice = (TextView) pview
				.findViewById(R.id.goods_backfeesPrice);
		TextView goods_currentPrice = (TextView) pview
				.findViewById(R.id.goods_currentPrice);
		TextView goods_status_tv = (TextView) pview
				.findViewById(R.id.goods_status_tv);
		pview.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				dialog.cancel();
			}
		});
		
		ImageLoader.getInstance().displayImage(
				mItem.pics == null ? "" : mItem.pics.get(0),
				goods_image, BaoBaoWDApplication.mOptions);
		goods_title_tv.setText(mItem.title);
		goods_backfeesPrice.setText(String.valueOf((float)mItem.coins/100));
		goods_currentPrice.setText(String.valueOf(mItem.umpPrice));
		goods_status_tv.setText(status);
		
		
	}

	public static void showBackDialog(final Context context) {
		View pview = LayoutInflater.from(context).inflate(
				R.layout.backhintdialog, null);
		final Dialog dialog = new Dialog(context, R.style.dialognotitle);
		dialog.setContentView(pview);
		dialog.show();
		Button cancelBt = (Button) pview.findViewById(R.id.cancel_bt);
		Button sureBt = (Button) pview.findViewById(R.id.sure_bt);
		sureBt.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				dialog.cancel();
				((Activity) context).finish();
			}
		});
		cancelBt.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				dialog.cancel();
			}
		});
	}

	public static void showGoPageDialog(final Context context, String pageDes,
			final GoPageDialogCallBack callback) {
		View pview = LayoutInflater.from(context).inflate(
				R.layout.gopagedialog, null);
		final Dialog dialog = new Dialog(context, R.style.dialognotitle);
		dialog.setContentView(pview);
		dialog.show();
		Button cancelBt = (Button) pview.findViewById(R.id.cancel_bt);
		Button sureBt = (Button) pview.findViewById(R.id.sure_bt);
		final EditText pageEt = (EditText) pview.findViewById(R.id.page_et);
		TextView pageTv = (TextView) pview.findViewById(R.id.page_tv);
		pageTv.setText(pageDes);
		sureBt.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				dialog.cancel();
				callback.clickSure(pageEt.getText().toString().trim());
			}
		});
		cancelBt.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				dialog.cancel();
			}
		});
		pageEt.requestFocus();
	}

	public static void showAdoptDialog(final Context context,
			final AdoptCallBack callBack) {
		View pview = LayoutInflater.from(context).inflate(
				R.layout.backhintdialog, null);
		TextView titleTv = (TextView) pview.findViewById(R.id.title_tv);
		Button sureBt = (Button) pview.findViewById(R.id.sure_bt);
		Button cancelBt = (Button) pview.findViewById(R.id.cancel_bt);
		titleTv.setText(context.getString(R.string.sureadopt));
		sureBt.setText(context.getString(R.string.OkButton));
		cancelBt.setText(context.getString(R.string.CancelButton));
		final Dialog dialog = new Dialog(context, R.style.dialognotitle);
		dialog.setContentView(pview);
		dialog.show();
		sureBt.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				dialog.cancel();
				callBack.adopt();
			}
		});
		cancelBt.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				dialog.cancel();
			}
		});
	}

	public static void showSaveImgDialog(final Context context,
			final AdoptCallBack callBack) {
		View pview = LayoutInflater.from(context).inflate(
				R.layout.backhintdialog, null);
		TextView titleTv = (TextView) pview.findViewById(R.id.title_tv);
		Button sureBt = (Button) pview.findViewById(R.id.sure_bt);
		Button cancelBt = (Button) pview.findViewById(R.id.cancel_bt);
		titleTv.setText(context.getString(R.string.saveimgtophoto));
		sureBt.setText(context.getString(R.string.OkButton));
		cancelBt.setText(context.getString(R.string.CancelButton));
		final Dialog dialog = new Dialog(context, R.style.dialognotitle);
		dialog.setContentView(pview);
		dialog.show();
		sureBt.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				dialog.cancel();
				callBack.adopt();
			}
		});
		cancelBt.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				dialog.cancel();
			}
		});
	}

	public static void showMoreDetailDialog(final Context context,
			final DialogCallBack callback) {
		View pview = LayoutInflater.from(context).inflate(
				R.layout.backhintdialog, null);
		TextView titleTv = (TextView) pview.findViewById(R.id.title_tv);
		Button sureBt = (Button) pview.findViewById(R.id.sure_bt);
		Button cancelBt = (Button) pview.findViewById(R.id.cancel_bt);
		titleTv.setText(context.getString(R.string.moredetail_hint));
		sureBt.setText(context.getString(R.string.moredetail_ok));
		cancelBt.setText(context.getString(R.string.moredetail_cancel));
		final Dialog dialog = new Dialog(context, R.style.dialognotitle);
		dialog.setContentView(pview);
		dialog.show();
		sureBt.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				dialog.cancel();
				callback.clickSure();
			}
		});
		cancelBt.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				dialog.cancel();
				callback.clickCancel();
			}
		});
	}

	public static void showUpdataVersionDialog(final Context context,
			String content, final DialogCallBack callback,
			final int lastPrivateVer) {
		View pview = LayoutInflater.from(context).inflate(
				R.layout.updateversiondialog, null);
		TextView contentTv = (TextView) pview.findViewById(R.id.content_tv);
		Button sureBt = (Button) pview.findViewById(R.id.sure_bt);
		Button cancelBt = (Button) pview.findViewById(R.id.cancel_bt);
		contentTv.setText(content);
		final Dialog dialog = new Dialog(context, R.style.dialognotitle);
		dialog.setContentView(pview);
		dialog.setCanceledOnTouchOutside(false);
		dialog.setCancelable(false);
		dialog.show();

		sureBt.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				dialog.cancel();
				callback.clickSure();
			}
		});
		cancelBt.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				dialog.cancel();
				callback.clickCancel();
			}
		});
	}

	public static void showLessGoldDialog(final Context context, String title) {
		View pview = LayoutInflater.from(context).inflate(
				R.layout.lessgolddialog, null);
		TextView title_tv = (TextView) pview.findViewById(R.id.title_tv);
		// TextView contentTv = (TextView) pview.findViewById(R.id.content_tv);
		Button sureBt = (Button) pview.findViewById(R.id.sure_bt);
		Button cancelBt = (Button) pview.findViewById(R.id.cancel_bt);
		// contentTv.setText(content);
		title_tv.setText(title);
		final Dialog dialog = new Dialog(context, R.style.dialognotitle);
		dialog.setContentView(pview);
		dialog.setCanceledOnTouchOutside(false);
		dialog.setCancelable(false);
		dialog.show();

		sureBt.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				dialog.cancel();
				context.startActivity(new Intent(context, TaskActivity.class));
				((Activity) context).finish();
			}
		});
		cancelBt.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				dialog.cancel();
			}
		});
	}

	public interface dialogCancelCallback {

		public void onCancle(boolean isNeverTip);
	}

	private static TipBackFeesPopup pop;

//	public static void showTipLoginDialogWhenShop(final Context context,
//			View parentView, final dialogCancelCallback callback) {
//		pop = new TipBackFeesPopup(context, parentView);
//		pop.setOnClickListener(callback);
//		pop.show();
//	}
//
//	public static void showTipLoginDialogWhenShop(final Context context,
//			View parentView, final dialogCancelCallback callback,
//			OnDismissListener listener) {
//		pop = new TipBackFeesPopup(context, parentView);
//		pop.setOnClickListener(callback);
//		pop.setOnDismissListener(listener);
//		pop.show();
//	}

	public static void showGoLoginDialog(final Context context,final DialogCallBack callback) {
		View pview = LayoutInflater.from(context).inflate(
				R.layout.gologindialog, null);
		Button sureBt = (Button) pview.findViewById(R.id.sure_bt);
		TextView hint_tv = (TextView) pview.findViewById(R.id.hint_tv);
		Button cancelBt = (Button) pview.findViewById(R.id.cancel_bt);
		final Dialog dialog = new Dialog(context, R.style.dialognotitle);
		dialog.setContentView(pview);
		dialog.show();
		sureBt.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				dialog.cancel();
				Intent lIntent = new Intent(context,
						LoginRegisterActivity.class);
				context.startActivity(lIntent);
			}
		});
		cancelBt.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				dialog.cancel();
				callback.clickCancel();
			}
		});
	}

	/**
	 * 公共的显示Dialog
	 * 
	 * @param context
	 * @param titleString
	 * @param contentString
	 * @param cancelString
	 * @param confirmString
	 * @param callback
	 */
	public static void showCommonDialog(final Context context,
			String titleString, String contentString, String cancelString,
			String confirmString, final IDialogCallback callback) {
		View pview = LayoutInflater.from(context).inflate(
				R.layout.updateversiondialog, null);
		TextView titleTv = (TextView) pview.findViewById(R.id.title_tv);
		TextView contentTv = (TextView) pview.findViewById(R.id.content_tv);

		Button sureBt = (Button) pview.findViewById(R.id.sure_bt);
		Button cancelBt = (Button) pview.findViewById(R.id.cancel_bt);

		if (titleTv != null && !TextUtils.isEmpty(titleString)) {
			titleTv.setText(titleString);
		}
		if (contentTv != null && !TextUtils.isEmpty(contentString)) {
			contentTv.setText(contentString);
		}

		if (sureBt != null && !TextUtils.isEmpty(confirmString)) {
			sureBt.setText(confirmString);
		}
		if (cancelBt != null && !TextUtils.isEmpty(cancelString)) {
			cancelBt.setText(cancelString);
		}
		final Dialog dialog = new Dialog(context, R.style.dialognotitle);
		dialog.setContentView(pview);
		dialog.show();
		sureBt.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				dialog.cancel();
				if (callback != null) {
					callback.onConfirmOnClick();
				}
			}
		});
		cancelBt.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				dialog.cancel();
				if (callback != null) {
					callback.onCancelOnClick();
				}
			}
		});
	}

	public static Intent startCamera(String imgPath) {
		Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
		intent.putExtra(MediaStore.EXTRA_OUTPUT,
				Uri.fromFile(new File(imgPath)));
		return intent;
	}

	// public static final String IMAGE_UNSPECIFIED = "image/*";
	public static Intent startGallery() {
		Intent intent = new Intent(Intent.ACTION_PICK, null);
		intent.setDataAndType(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
				"image/*");
		return intent;
	}

	public static Intent startCropImage(Uri uri) {
		Intent intent = new Intent("com.android.camera.action.CROP");
		intent.setDataAndType(uri, "image/*");
		intent.putExtra("crop", "true");
		// aspectX aspectY 是宽高的比例
		intent.putExtra("aspectX", 1);
		intent.putExtra("aspectY", 1);
		// outputX outputY 是裁剪图片宽高
		intent.putExtra("outputX", 160);
		intent.putExtra("outputY", 160);
		// 图片格式
		intent.putExtra("outputFormat", "JPEG");
		// 不启用人脸识别
		intent.putExtra("noFaceDetection", false);
		intent.putExtra("return-data", true);
		return intent;
	}

	private static PopupWindow mPopupWindow;
	
	public static void cancelPopup(){
		if(mPopupWindow!=null){
			mPopupWindow.dismiss();
			mPopupWindow = null;
		}
	}
	

	// private static MenuPopup mSharePopWindow;

	public static void showShareFavWindow(Context context, View view,
			byte isfav, final ShareFavWindowCallBack callback) {
		if (mPopupWindow == null || !mPopupWindow.isShowing()) {
			View pview = LayoutInflater.from(context).inflate(
					R.layout.sharefavwindow, null, false);
			// RelativeLayout windowLayout = (RelativeLayout) pview
			// .findViewById(R.id.layout);
			ImageView favIv = (ImageView) pview.findViewById(R.id.fav_iv);
			final RelativeLayout shareLayout = (RelativeLayout) pview
					.findViewById(R.id.share_layout);
			final RelativeLayout favLayout = (RelativeLayout) pview
					.findViewById(R.id.fav_layout);
			if (isfav == GobalConstants.AllStatusYesOrNo.YES)
				favIv.setImageResource(R.drawable.knowledge_favselected);
			else
				favIv.setImageResource(R.drawable.knowledge_fav);
			shareLayout.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					mPopupWindow.dismiss();
					mPopupWindow = null;
					callback.share();
				}
			});
			favLayout.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					mPopupWindow.dismiss();
					mPopupWindow = null;
					callback.fav();
				}
			});
			mPopupWindow = new PopupWindow(pview,
					android.view.ViewGroup.LayoutParams.WRAP_CONTENT,
					android.view.ViewGroup.LayoutParams.WRAP_CONTENT, true);
			mPopupWindow.setFocusable(true);
			mPopupWindow.setBackgroundDrawable(new BitmapDrawable());
			mPopupWindow.showAsDropDown(view, 0,
					ConversionHelper.dipToPx(10, context));
		}
	}

	// public static void showPriceOrderWindow(Context context, View
	// parent,String orderType,
	// final PriceOrderWindowCallBack callback)
	// {
	// if (mPopupWindow == null || !mPopupWindow.isShowing())
	// {
	// View pview = LayoutInflater.from(context).inflate(
	// R.layout.priceorderwindow, null, false);
	// // RelativeLayout windowLayout = (RelativeLayout) pview
	// // .findViewById(R.id.layout);
	// RelativeLayout order_default_layout = (RelativeLayout)
	// pview.findViewById(R.id.order_default_layout);
	// RelativeLayout order_pricehtol_layout = (RelativeLayout)
	// pview.findViewById(R.id.order_pricehtol_layout);
	// RelativeLayout order_priceltoh_layout = (RelativeLayout)
	// pview.findViewById(R.id.order_priceltoh_layout);
	//
	// TextView order_default_tv = (TextView)
	// pview.findViewById(R.id.order_default_tv);
	// TextView order_pricehtol_tv = (TextView)
	// pview.findViewById(R.id.order_pricehtol_tv);
	// TextView order_priceltoh_tv = (TextView)
	// pview.findViewById(R.id.order_priceltoh_tv);
	//
	// ImageView default_selected_iv = (ImageView)
	// pview.findViewById(R.id.default_selected_iv);
	// ImageView pricehtol_selected_iv = (ImageView)
	// pview.findViewById(R.id.pricehtol_selected_iv);
	// ImageView priceltoh_selected_iv = (ImageView)
	// pview.findViewById(R.id.priceltoh_selected_iv);
	//
	// if(orderType==ShowScanResultActivity.ORDERTYPE_DEFAULT){
	// order_default_tv.setSelected(true);
	// order_pricehtol_tv.setSelected(false);
	// order_priceltoh_tv.setSelected(false);
	// default_selected_iv.setVisibility(View.VISIBLE);
	// pricehtol_selected_iv.setVisibility(View.GONE);
	// priceltoh_selected_iv.setVisibility(View.GONE);
	// }else if(orderType==ShowScanResultActivity.ORDERTYPE_PRICE_HTOL){
	// order_default_tv.setSelected(false);
	// order_pricehtol_tv.setSelected(true);
	// order_priceltoh_tv.setSelected(false);
	// default_selected_iv.setVisibility(View.GONE);
	// pricehtol_selected_iv.setVisibility(View.VISIBLE);
	// priceltoh_selected_iv.setVisibility(View.GONE);
	//
	// }else if(orderType==ShowScanResultActivity.ORDERTYPE_PRICE_LTOH){
	// order_default_tv.setSelected(false);
	// order_pricehtol_tv.setSelected(false);
	// order_priceltoh_tv.setSelected(true);
	// default_selected_iv.setVisibility(View.GONE);
	// pricehtol_selected_iv.setVisibility(View.GONE);
	// priceltoh_selected_iv.setVisibility(View.VISIBLE);
	//
	// }
	//
	//
	// order_default_layout.setOnClickListener(new OnClickListener()
	// {
	//
	// @Override
	// public void onClick(View v)
	// {
	// mPopupWindow.dismiss();
	// mPopupWindow = null;
	// callback.pricedefaultselected();
	// }
	// });
	// order_pricehtol_layout.setOnClickListener(new OnClickListener()
	// {
	//
	// @Override
	// public void onClick(View v)
	// {
	// mPopupWindow.dismiss();
	// mPopupWindow = null;
	// callback.pricehtolselected();
	// }
	// });
	// order_priceltoh_layout.setOnClickListener(new OnClickListener()
	// {
	//
	// @Override
	// public void onClick(View v)
	// {
	// mPopupWindow.dismiss();
	// mPopupWindow = null;
	// callback.priceltohselected();
	// }
	// });
	// mPopupWindow = new PopupWindow(pview,
	// android.view.ViewGroup.LayoutParams.MATCH_PARENT,
	// android.view.ViewGroup.LayoutParams.WRAP_CONTENT, true);
	// mPopupWindow.setFocusable(true);
	// mPopupWindow.setBackgroundDrawable(new BitmapDrawable());
	// mPopupWindow.showAsDropDown(parent/*, 0,
	// ConversionHelper.dipToPx(10, context)*/);
	// }
	// }

	public static void showChoosePriceWindow(Context context, View parent,
			boolean isTianmao, int beginPrice, int endPrice, int htolOrltoh,
			String orderType, final ChoosePriceWindowCallBack callback) {
		if (mPopupWindow == null || !mPopupWindow.isShowing()) {
			View pview = LayoutInflater.from(context).inflate(
					R.layout.choosepricepop, null, false);
			// RelativeLayout windowLayout = (RelativeLayout) pview
			// .findViewById(R.id.layout);
			final Button tianmaoBt = (Button) pview
					.findViewById(R.id.tianmao_bt);
			final Button htol_bt = (Button) pview.findViewById(R.id.htol_bt);
			final Button ltoh_bt = (Button) pview.findViewById(R.id.ltoh_bt);

			Button okBt = (Button) pview.findViewById(R.id.ok_bt);
			final EditText beginprice_et = (EditText) pview
					.findViewById(R.id.beginprice_et);
			final EditText endprice_et = (EditText) pview
					.findViewById(R.id.endprice_et);

			if (orderType == ShowScanResultActivity.ORDERTYPE_DEFAULT
					|| orderType == ShowScanResultActivity.ORDERTYPE_HTOL
					|| orderType == ShowScanResultActivity.ORDERTYPE_LTOH) {
				htol_bt.setVisibility(View.VISIBLE);
				ltoh_bt.setVisibility(View.VISIBLE);
			} else {
				htol_bt.setVisibility(View.GONE);
				ltoh_bt.setVisibility(View.GONE);
			}

			if (isTianmao)
				tianmaoBt.setSelected(true);
			if (beginPrice != -1) {
				beginprice_et.setText(beginPrice + "");
				beginprice_et.setSelection(beginprice_et.getText().length());
			}
			if (endPrice != -1) {
				endprice_et.setText(endPrice + "");
				endprice_et.setSelection(endprice_et.getText().length());
			}

			if (htolOrltoh == ShowScanResultActivity.ORDERTYPE_PRICE_HTOL) {
				htol_bt.setSelected(true);
			} else if (htolOrltoh == ShowScanResultActivity.ORDERTYPE_PRICE_LTOH) {
				ltoh_bt.setSelected(true);
			} else {
				htol_bt.setSelected(false);
				ltoh_bt.setSelected(false);
			}

			htol_bt.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					if (htol_bt.isSelected()) {
						htol_bt.setSelected(false);
					} else {
						htol_bt.setSelected(true);
						ltoh_bt.setSelected(false);
					}
				}
			});
			ltoh_bt.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					if (ltoh_bt.isSelected()) {
						ltoh_bt.setSelected(false);
					} else {
						ltoh_bt.setSelected(true);
						htol_bt.setSelected(false);
					}
				}
			});
			tianmaoBt.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					if (tianmaoBt.isSelected())
						tianmaoBt.setSelected(false);
					else
						tianmaoBt.setSelected(true);
				}
			});
			okBt.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					mPopupWindow.dismiss();
					mPopupWindow = null;
					try {
						int begin = -1;
						int end = -1;
						int htolOrltoh = -1;
						if (!TextUtils.isEmpty(beginprice_et.getText()))
							begin = Integer.parseInt(beginprice_et.getText()
									.toString());
						if (!TextUtils.isEmpty(endprice_et.getText()))
							end = Integer.parseInt(endprice_et.getText()
									.toString());
						if (htol_bt.isSelected()) {
							htolOrltoh = ShowScanResultActivity.ORDERTYPE_PRICE_HTOL;
						} else if (ltoh_bt.isSelected()) {
							htolOrltoh = ShowScanResultActivity.ORDERTYPE_PRICE_LTOH;
						}

						callback.choose(tianmaoBt.isSelected(), begin, end,
								htolOrltoh);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			});

			mPopupWindow = new PopupWindow(pview,
					android.view.ViewGroup.LayoutParams.MATCH_PARENT,
					android.view.ViewGroup.LayoutParams.MATCH_PARENT, true);
			mPopupWindow.setFocusable(true);
			mPopupWindow.setBackgroundDrawable(new BitmapDrawable());
			mPopupWindow.showAsDropDown(parent/*
											 * , 0, ConversionHelper.dipToPx(10,
											 * context)
											 */);
		}
	}

	public static void showShareFavGoPageWindow(Context context, View view,
			byte isfav, final ShareFavGoPageWindowCallBack callback) {
		if (mPopupWindow == null || !mPopupWindow.isShowing()) {
			View pview = LayoutInflater.from(context).inflate(
					R.layout.sharefavgopagewindow, null, false);
			// RelativeLayout windowLayout = (RelativeLayout) pview
			// .findViewById(R.id.layout);
			ImageView favIv = (ImageView) pview.findViewById(R.id.fav_iv);
			final RelativeLayout shareLayout = (RelativeLayout) pview
					.findViewById(R.id.share_layout);
			final RelativeLayout favLayout = (RelativeLayout) pview
					.findViewById(R.id.fav_layout);
			final RelativeLayout gopage_layout = (RelativeLayout) pview
					.findViewById(R.id.gopage_layout);
			if (isfav == GobalConstants.AllStatusYesOrNo.YES)
				favIv.setImageResource(R.drawable.knowledge_favselected);
			else
				favIv.setImageResource(R.drawable.knowledge_fav);
			shareLayout.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					mPopupWindow.dismiss();
					mPopupWindow = null;
					callback.share();
				}
			});
			favLayout.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					mPopupWindow.dismiss();
					mPopupWindow = null;
					callback.fav();
				}
			});
			gopage_layout.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					mPopupWindow.dismiss();
					mPopupWindow = null;
					callback.goPage();
				}
			});
			mPopupWindow = new PopupWindow(pview,
					android.view.ViewGroup.LayoutParams.WRAP_CONTENT,
					android.view.ViewGroup.LayoutParams.WRAP_CONTENT, true);
			mPopupWindow.setFocusable(true);
			mPopupWindow.setBackgroundDrawable(new BitmapDrawable());
			mPopupWindow.showAsDropDown(view, 0,
					ConversionHelper.dipToPx(10, context));
		}
	}

	// 个人信息界面
	public static void showReportShieldWindow(Context context, View view,
			final ReportShieldDeleteWindowCallBack callback) {/*
															 * if (mPopupWindow
															 * == null ||
															 * !mPopupWindow
															 * .isShowing()) {
															 * View pview =
															 * LayoutInflater
															 * .from
															 * (context).inflate
															 * ( R.layout.
															 * reportshieldwindow
															 * , null, false);
															 * // RelativeLayout
															 * windowLayout =
															 * (RelativeLayout)
															 * pview //
															 * .findViewById
															 * (R.id.layout);
															 * final
															 * RelativeLayout
															 * report_layout =
															 * (RelativeLayout)
															 * pview
															 * .findViewById
															 * (R.id
															 * .report_layout);
															 * final
															 * RelativeLayout
															 * shield_layout =
															 * (RelativeLayout)
															 * pview
															 * .findViewById
															 * (R.id
															 * .shield_layout);
															 * report_layout
															 * .setOnClickListener
															 * (new
															 * OnClickListener()
															 * {
															 * 
															 * @Override public
															 * void onClick(View
															 * v) {
															 * mPopupWindow.
															 * dismiss();
															 * mPopupWindow =
															 * null;
															 * callback.report
															 * (); } });
															 * shield_layout
															 * .setOnClickListener
															 * (new
															 * OnClickListener()
															 * {
															 * 
															 * @Override public
															 * void onClick(View
															 * v) {
															 * mPopupWindow.
															 * dismiss();
															 * mPopupWindow =
															 * null;
															 * callback.shield
															 * (); } });
															 * mPopupWindow =
															 * new
															 * PopupWindow(pview
															 * , android.view.
															 * ViewGroup
															 * .LayoutParams
															 * .WRAP_CONTENT,
															 * android
															 * .view.ViewGroup
															 * .LayoutParams
															 * .WRAP_CONTENT,
															 * true);
															 * mPopupWindow
															 * .setFocusable
															 * (true);
															 * mPopupWindow.
															 * setBackgroundDrawable
															 * (new
															 * BitmapDrawable
															 * ());
															 * mPopupWindow.
															 * showAsDropDown
															 * (view); }
															 */
	}

	// 私信界面
	public static void showReportShieldDeleteWindow(Context context, View view,
			final ReportShieldDeleteWindowCallBack callback) {/*
															 * if (mPopupWindow
															 * == null ||
															 * !mPopupWindow
															 * .isShowing()) {
															 * View pview =
															 * LayoutInflater
															 * .from
															 * (context).inflate
															 * ( R.layout.
															 * reportshielddeletewindow
															 * , null, false);
															 * // final TextView
															 * report_tv =
															 * (TextView) pview
															 * //
															 * .findViewById(R
															 * .id.report_tv);
															 * final TextView
															 * shield_tv =
															 * (TextView) pview
															 * .
															 * findViewById(R.id
															 * .shield_tv);
															 * final TextView
															 * delete_tv =
															 * (TextView) pview
															 * .
															 * findViewById(R.id
															 * .delete_tv);
															 * shield_tv
															 * .setOnClickListener
															 * (new
															 * OnClickListener()
															 * {
															 * 
															 * @Override public
															 * void onClick(View
															 * v) {
															 * mPopupWindow.
															 * dismiss();
															 * mPopupWindow =
															 * null;
															 * callback.shield
															 * (); } });
															 * delete_tv
															 * .setOnClickListener
															 * (new
															 * OnClickListener()
															 * {
															 * 
															 * @Override public
															 * void onClick(View
															 * v) {
															 * mPopupWindow.
															 * dismiss();
															 * mPopupWindow =
															 * null;
															 * callback.delete
															 * (); } });
															 * mPopupWindow =
															 * new
															 * PopupWindow(pview
															 * , android.view.
															 * ViewGroup
															 * .LayoutParams
															 * .WRAP_CONTENT,
															 * android
															 * .view.ViewGroup
															 * .LayoutParams
															 * .WRAP_CONTENT,
															 * true);
															 * mPopupWindow
															 * .setFocusable
															 * (true);
															 * mPopupWindow.
															 * setBackgroundDrawable
															 * (new
															 * BitmapDrawable
															 * ()); int mid =
															 * view.getWidth() /
															 * 2; int xoff = mid
															 * -
															 * ConversionHelper
															 * .dipToPx(60,
															 * context);
															 * mPopupWindow
															 * .showAsDropDown
															 * (view, xoff, 0);
															 * }
															 */
	}

	public static void showDeleteWindow(Context context, View view,
			final DeleteWindowCallBack callback) {/*
												 * if (mPopupWindow == null ||
												 * !mPopupWindow.isShowing()) {
												 * View pview =
												 * LayoutInflater.from
												 * (context).inflate(
												 * R.layout.deletewindow, null,
												 * false); final TextView
												 * delete_tv = (TextView) pview
												 * .
												 * findViewById(R.id.delete_tv);
												 * delete_tv
												 * .setOnClickListener(new
												 * OnClickListener() {
												 * 
												 * @Override public void
												 * onClick(View v) {
												 * mPopupWindow.dismiss();
												 * mPopupWindow = null;
												 * callback.delete(); } });
												 * mPopupWindow = new
												 * PopupWindow(pview,
												 * android.view
												 * .ViewGroup.LayoutParams
												 * .WRAP_CONTENT,
												 * android.view.ViewGroup
												 * .LayoutParams.WRAP_CONTENT,
												 * true);
												 * mPopupWindow.setFocusable
												 * (true); mPopupWindow.
												 * setBackgroundDrawable(new
												 * BitmapDrawable()); int
												 * screenWidth =
												 * getScreenWidth(context) / 2 -
												 * ConversionHelper.dipToPx(30,
												 * context);
												 * mPopupWindow.showAsDropDown
												 * (view, screenWidth, 0); }
												 */
	}

	public static void showLetterDetailDeleteWindow(Context context, View view,
			final DeleteWindowCallBack callback) {/*
												 * if (mPopupWindow == null ||
												 * !mPopupWindow.isShowing()) {
												 * View pview =
												 * LayoutInflater.from
												 * (context).inflate(
												 * R.layout.deletewindow, null,
												 * false); final TextView
												 * delete_tv = (TextView) pview
												 * .
												 * findViewById(R.id.delete_tv);
												 * delete_tv
												 * .setOnClickListener(new
												 * OnClickListener() {
												 * 
												 * @Override public void
												 * onClick(View v) {
												 * mPopupWindow.dismiss();
												 * mPopupWindow = null;
												 * callback.delete(); } });
												 * mPopupWindow = new
												 * PopupWindow(pview,
												 * android.view
												 * .ViewGroup.LayoutParams
												 * .WRAP_CONTENT,
												 * android.view.ViewGroup
												 * .LayoutParams.WRAP_CONTENT,
												 * true);
												 * mPopupWindow.setFocusable
												 * (true); mPopupWindow.
												 * setBackgroundDrawable(new
												 * BitmapDrawable()); int mid =
												 * view.getWidth() / 2; int xoff
												 * = mid -
												 * ConversionHelper.dipToPx(30,
												 * context);
												 * mPopupWindow.showAsDropDown
												 * (view, xoff, 0); }
												 */
	}

	public static void showShareWindow(Context context, View parent,
			final int type, final ShareWindowCallBack callback) {
		if (mPopupWindow == null || !mPopupWindow.isShowing()) {
			View pview = LayoutInflater.from(context).inflate(
					R.layout.sharewindow, null, false);
			LinearLayout windowLayout = (LinearLayout) pview
					.findViewById(R.id.layout);
			final RelativeLayout kong_layout = (RelativeLayout) pview
					.findViewById(R.id.kong_layout);
			final LinearLayout layout = (LinearLayout) pview
					.findViewById(R.id.layout);
			final RelativeLayout qqFriendLayout = (RelativeLayout) pview
					.findViewById(R.id.qqfriend_layout);
			final RelativeLayout qqSpaceLayout = (RelativeLayout) pview
					.findViewById(R.id.qqspace_layout);
			final ImageView qqSpaceIv = (ImageView) pview
					.findViewById(R.id.qqspace_iv);
			final TextView qqSpaceTv = (TextView) pview
					.findViewById(R.id.qqspace_tv);
			if (type == GobalConstants.LoginType.SINAWEIBO) {
				qqSpaceIv.setImageDrawable(context.getResources().getDrawable(
						R.drawable.share_weibo));
				qqSpaceTv.setText(context.getString(R.string.sinaweibo));
			} else if (type == GobalConstants.LoginType.QQ) {
				qqSpaceIv.setImageDrawable(context.getResources().getDrawable(
						R.drawable.share_qqspace));
				qqSpaceTv.setText(context.getString(R.string.qqspace));
			} else if (type == GobalConstants.LoginType.BB) {
				qqSpaceLayout.setVisibility(View.GONE);
			}
			final RelativeLayout weixinFriendLayout = (RelativeLayout) pview
					.findViewById(R.id.weixinfriend_layout);
			final RelativeLayout weixinSpaceLayout = (RelativeLayout) pview
					.findViewById(R.id.weixinspace_layout);
			kong_layout.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					mPopupWindow.dismiss();
					mPopupWindow = null;
				}
			});
			qqFriendLayout.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					mPopupWindow.dismiss();
					mPopupWindow = null;
					callback.shareToQQFriend();
				}
			});
			qqSpaceLayout.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					mPopupWindow.dismiss();
					mPopupWindow = null;
					if (type == GobalConstants.LoginType.SINAWEIBO) {
						callback.sinaweiboshare();
					} else if (type == GobalConstants.LoginType.QQ) {
						callback.shareToQQSpace();
					}
				}
			});
			weixinFriendLayout.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					mPopupWindow.dismiss();
					mPopupWindow = null;
					callback.shareWeixinFriend();
				}
			});
			weixinSpaceLayout.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					mPopupWindow.dismiss();
					mPopupWindow = null;
					callback.shareWeixinSpace();
				}
			});
			mPopupWindow = new PopupWindow(pview,
					android.view.ViewGroup.LayoutParams.MATCH_PARENT,
					android.view.ViewGroup.LayoutParams.MATCH_PARENT, true);
			mPopupWindow.setFocusable(true);
			mPopupWindow.setBackgroundDrawable(new BitmapDrawable());
			mPopupWindow.showAtLocation(parent, Gravity.BOTTOM, 0, 0);
		}
	}

	public static void showSpeechDialog(final Context context,
			final EditText titleEt, final EditText contentEt) {
	}

	public static void showCopyDialog(final Context context, final String str) {
		View pview = LayoutInflater.from(context).inflate(R.layout.copydialog,
				null);
		final Dialog dialog = new Dialog(context, R.style.dialognotitle);
		dialog.setContentView(pview);
		dialog.show();
		TextView copyTv = (TextView) pview.findViewById(R.id.copy_tv);
		copyTv.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				ClipboardManager cmb = (ClipboardManager) context
						.getSystemService(Context.CLIPBOARD_SERVICE);
				cmb.setText(str);
				dialog.cancel();
			}
		});
	}

	public static void chooseImgDialog(Context context,
			final ChooseImgCallBack callback) {
		View pview = LayoutInflater.from(context).inflate(
				R.layout.chooseavatardialog, null);
		final Dialog dialog = new Dialog(context, R.style.dialognotitle);
		dialog.setContentView(pview);
		dialog.show();
		TextView titleTv = (TextView) pview.findViewById(R.id.title_tv);
		titleTv.setText(context.getString(R.string.choose));
		RelativeLayout cameraLayout = (RelativeLayout) pview
				.findViewById(R.id.camera_layout);
		RelativeLayout albumsLayout = (RelativeLayout) pview
				.findViewById(R.id.albums_layout);
		Button cancelBt = (Button) pview.findViewById(R.id.cancel_bt);
		cameraLayout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				dialog.cancel();
				callback.clickCamera();
			}
		});
		albumsLayout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				dialog.cancel();
				callback.clickGallery();
			}
		});
		cancelBt.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				dialog.cancel();
			}
		});
	}
}
