package dianyun.baobaowd.util;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.AnimationDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import dianyun.baobaowd.interfaces.DialogCallBack;
import dianyun.zqcheap.R;

public class DialogHelper
{

	public interface EditDialogCallBack
	{

		void clickOk(String edStr);

		void clickCancel();
	}

	public static void showforgetPwDialog(Context context,
			final EditDialogCallBack callback)
	{
		View pview = LayoutInflater.from(context).inflate(
				R.layout.edittextdialog, null);
		final Dialog dialog = new Dialog(context, R.style.dialognotitle);
		dialog.setContentView(pview);
		dialog.show();
		final EditText edittext = (EditText) pview.findViewById(R.id.edittext);
		final TextView titleTv = (TextView) pview.findViewById(R.id.title_tv);
		titleTv.setText(context.getString(R.string.inputmailorphonenumber));
		Button cancelBt = (Button) pview.findViewById(R.id.cancel_bt);
		Button sureBt = (Button) pview.findViewById(R.id.sure_bt);
		sureBt.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View v)
			{
				dialog.cancel();
				callback.clickOk(edittext.getText().toString().trim());
			}
		});
		cancelBt.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View v)
			{
				dialog.cancel();
				callback.clickCancel();
			}
		});
	}

	public static Dialog showDetailLoadingDialog(final Context context,String content,
			final DialogCallBack callback)
	{
		final View pview = LayoutInflater.from(context).inflate(
				R.layout.detailloadingdialog, null);
		ImageView lImageView = (ImageView) pview.findViewById(R.id.gifview);
		TextView contentTv = (TextView) pview.findViewById(R.id.content_tv);
		contentTv.setText(content);
		AnimationDrawable animationDrawable = (AnimationDrawable) lImageView
				.getDrawable();
		animationDrawable.start();
		// lGifView.setGifImage(R.drawable.detailloading);
		final Dialog dialog = new Dialog(context, R.style.dialognotitle);
		dialog.setContentView(pview);
		dialog.show();
		ImageView cancelIv = (ImageView) pview.findViewById(R.id.cancel_iv);
		cancelIv.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View v)
			{
				dialog.cancel();
				callback.clickSure();
			}
		});
		return dialog;
	}
	public static Dialog showPaySuccessDialog(final Context context,
			final DialogCallBack callback)
	{
		final View pview = LayoutInflater.from(context).inflate(
				R.layout.paysuccessdialog, null);

		
		final Dialog dialog = new Dialog(context, R.style.dialognotitle);
		dialog.setContentView(pview);
		dialog.show();
		dialog.setCanceledOnTouchOutside(false);
		dialog.setCancelable(false);
		Button sure_bt = (Button) pview.findViewById(R.id.sure_bt);
		sure_bt.setOnClickListener(new OnClickListener()
		{
			
			@Override
			public void onClick(View v)
			{
				dialog.cancel();
				callback.clickSure();
			}
		});
		return dialog;
	}

	public static Dialog showProgressDialog(final Context context, String hint)
	{
		View pview = LayoutInflater.from(context).inflate(
				R.layout.detailloadingdialog, null);
		final Dialog dialog = new Dialog(context, R.style.dialognotitle);
		// GifView lGifView = (GifView)pview.findViewById(R.id.gifview);
		// lGifView.setGifImage(R.drawable.detailloading);
		ImageView lImageView = (ImageView) pview.findViewById(R.id.gifview);
		AnimationDrawable animationDrawable = (AnimationDrawable) lImageView
				.getDrawable();
		animationDrawable.start();
		dialog.setContentView(pview);
		dialog.show();
		ImageView cancelIv = (ImageView) pview.findViewById(R.id.cancel_iv);
		TextView textView = (TextView) pview.findViewById(R.id.content_tv);
		textView.setText(hint);
		cancelIv.setVisibility(View.GONE);
		return dialog;
	}

	public static void cancelProgressDialog(Dialog progressDialog)
	{
		if (progressDialog != null)
		{
			progressDialog.dismiss();
			progressDialog = null;
		}
	}
	
	
	
	
	
	
	
	
	

	public static void showChangeTextDialog(Context context, String title,
			String value, final EditDialogCallBack callback)
	{
		View pview = LayoutInflater.from(context).inflate(
				R.layout.changenamedialog, null);
		final Dialog dialog = new Dialog(context, R.style.dialognotitle);
		dialog.setContentView(pview);
		dialog.show();
		final EditText edittext = (EditText) pview.findViewById(R.id.edittext);
		final TextView titleTv = (TextView) pview.findViewById(R.id.title_tv);
		titleTv.setText(title);
		edittext.setText(value);
		edittext.setSelection(value.length());
		Button cancelBt = (Button) pview.findViewById(R.id.cancel_bt);
		Button sureBt = (Button) pview.findViewById(R.id.sure_bt);
		sureBt.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View v)
			{
				dialog.cancel();
				String edit = edittext.getText().toString();
				callback.clickOk(edit);
			}
		});
		cancelBt.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View v)
			{
				dialog.cancel();
			}
		});
	}

	public static void showPlayMusicHintDialog(final Context context,
			final DialogCallBack callback)
	{
		View pview = LayoutInflater.from(context).inflate(R.layout.hintdialog,
				null);
		final Dialog dialog = new Dialog(context, R.style.dialognotitle);
		dialog.setContentView(pview);
		dialog.show();
		Button cancelBt = (Button) pview.findViewById(R.id.cancel_bt);
		Button sureBt = (Button) pview.findViewById(R.id.sure_bt);
		cancelBt.setText(context.getString(R.string.play_notnow));
		sureBt.setText(context.getString(R.string.play_now));
		TextView hintTv = (TextView) pview.findViewById(R.id.hint_tv);
		hintTv.setText(context.getString(R.string.nowifihint));
		sureBt.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View v)
			{
				dialog.cancel();
				callback.clickSure();
			}
		});
		cancelBt.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View v)
			{
				dialog.cancel();
				callback.clickCancel();
			}
		});
	}

	

	public static void showDownloadMusicHintDialog(final Context context,
			final DialogCallBack callback)
	{
		View pview = LayoutInflater.from(context).inflate(R.layout.hintdialog,
				null);
		final Dialog dialog = new Dialog(context, R.style.dialognotitle);
		dialog.setContentView(pview);
		dialog.show();
		Button cancelBt = (Button) pview.findViewById(R.id.cancel_bt);
		Button sureBt = (Button) pview.findViewById(R.id.sure_bt);
		TextView hintTv = (TextView) pview.findViewById(R.id.hint_tv);
		hintTv.setText(context.getString(R.string.surecanceldownload));
		sureBt.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View v)
			{
				dialog.cancel();
				callback.clickSure();
			}
		});
		cancelBt.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View v)
			{
				dialog.cancel();
				callback.clickCancel();
			}
		});
	}
}
