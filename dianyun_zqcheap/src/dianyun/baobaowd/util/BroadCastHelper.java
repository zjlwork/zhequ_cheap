
package dianyun.baobaowd.util;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Environment;
import dianyun.baobaowd.data.Post;
import dianyun.baobaowd.data.Question;
import dianyun.baobaowd.data.ReceiveLetter;
import dianyun.baobaowd.data.Topic;
import dianyun.baobaowd.data.User;

public class BroadCastHelper
{

	// 扫描某一文件
	public static void sendRefreshOnePhotoBroadcast(Context context,
			String filePath)
	{
		context.sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE,
				Uri.parse("file://" + filePath)));
	}

	// 扫描sdcard
	public static void sendRefreshPhotoBroadcast(Context context)
	{
		context.sendBroadcast(new Intent(Intent.ACTION_MEDIA_MOUNTED, Uri
				.parse("file://" + Environment.getExternalStorageDirectory())));
	}

	public static void sendMuiscOperateBroadcast(Context context, int operate)
	{/*
		Intent lIntent = new Intent();
		lIntent.setAction(GobalConstants.IntentFilterAction.MUSICOPERATE);
		lIntent.putExtra(MusicHelper.OPERATE, operate);
		context.sendBroadcast(lIntent);
	*/}

	public static void sendRefreshAppBroadcast(Context context, String appId,
			byte result)
	{
		Intent lIntent = new Intent();
		lIntent.setAction(GobalConstants.IntentFilterAction.REFRESHAPPAWARD);
		lIntent.putExtra(GobalConstants.Data.APPID, appId);
		lIntent.putExtra(GobalConstants.Data.AWARDSUCCESSORFAILED, result);
		context.sendBroadcast(lIntent);
	}
	public static void sendChangeMaxSeqIdBroadcast(Context context, long maxSeqId)
	{
		Intent lIntent = new Intent();
		lIntent.setAction(GobalConstants.IntentFilterAction.REFRESH);
		lIntent.putExtra(GobalConstants.Data.REFRESHTYPE, GobalConstants.RefreshType.CHANGEMAXSEQID);
		lIntent.putExtra(GobalConstants.Data.MAXSEQID, maxSeqId);
		context.sendBroadcast(lIntent);
	}

	public static void sendRefreshTaskStatusBroadcast(Context context)
	{
		Intent lIntent = new Intent();
		lIntent.setAction(GobalConstants.IntentFilterAction.REFRESH);
		lIntent.putExtra(GobalConstants.Data.REFRESHTYPE,
				GobalConstants.RefreshType.REFRESHTASKSTATUS);
		context.sendBroadcast(lIntent);
	}

	public static void sendRefreshUserGoldBroadcast(Context context, int gold)
	{
		Intent lIntent = new Intent();
		lIntent.setAction(GobalConstants.IntentFilterAction.REFRESH);
		lIntent.putExtra(GobalConstants.Data.REFRESHTYPE,
				GobalConstants.RefreshType.CHAGNEUSERGOLD);
		User lUser = UserHelper.getUser();
		lUser.setYcoins(lUser.getYcoins() + gold);
		UserHelper.setUser(lUser);
		context.sendBroadcast(lIntent);
	}
	//分享订单 
	public static void sendAddUserGoldBroadcast(Context context, int gold)
	{
		Intent lIntent = new Intent();
		lIntent.setAction(GobalConstants.IntentFilterAction.REFRESH);
		lIntent.putExtra(GobalConstants.Data.REFRESHTYPE,
				GobalConstants.RefreshType.ADDUSERGOLD);
		lIntent.putExtra(GobalConstants.Data.ADDGOLD_COUNT,gold);
		User lUser = UserHelper.getUser();
		lUser.setYcoins(lUser.getYcoins() + gold);
		UserHelper.setUser(lUser);
		context.sendBroadcast(lIntent);
	}

	// public static void sendRefreshTaskStatusVOrG(Context context){
	// Intent lIntent = new Intent();
	// lIntent.setAction(GobalConstants.IntentFilterAction.REFRESH);
	// lIntent.putExtra(GobalConstants.Data.REFRESHTYPE,
	// GobalConstants.RefreshType.TASKSTATUS);
	// context.sendBroadcast(lIntent);
	// }
	public static void sendRefreshMainBroadcast(Context context,
			byte refreshType)
	{
		Intent lIntent = new Intent();
		lIntent.setAction(GobalConstants.IntentFilterAction.REFRESH);
		lIntent.putExtra(GobalConstants.Data.REFRESHTYPE, refreshType);
		context.sendBroadcast(lIntent);
	}
	
//	REGIVETIPS
	public static void sendRefreshRegivetipsBroadcast(Context context,
			byte refreshType,String regivetips)
	{
		Intent lIntent = new Intent();
		lIntent.setAction(GobalConstants.IntentFilterAction.REFRESH);
		lIntent.putExtra(GobalConstants.Data.REFRESHTYPE, refreshType);
		lIntent.putExtra("REGIVETIPS", regivetips);
		context.sendBroadcast(lIntent);
	}

	public static void sendReceiveLetterBroadcast(Context context,
			ReceiveLetter receiveLetter)
	{
		Intent lIntent = new Intent();
		lIntent.setAction(GobalConstants.IntentFilterAction.REFRESH);
		lIntent.putExtra(GobalConstants.Data.REFRESHTYPE,
				GobalConstants.RefreshType.RECEIVELETTER);
		lIntent.putExtra(GobalConstants.Data.RECEIVELETTER, receiveLetter);
		context.sendBroadcast(lIntent);
	}

	// profilecenteractivity
	public static void sendRefreshProfileBroadcast(Context context)
	{
		Intent lIntent = new Intent();
		lIntent.setAction(GobalConstants.IntentFilterAction.REFRESHPROFILE);
		lIntent.putExtra(GobalConstants.Data.REFRESHTYPE,
				GobalConstants.RefreshType.PROFILE);
		context.sendBroadcast(lIntent);
	}

	// mainactivity
	// public static void sendRefreshUserBroadcast(Context context){
	// Intent lIntent = new Intent();
	// lIntent.setAction(GobalConstants.IntentFilterAction.REFRESH);
	// lIntent.putExtra(GobalConstants.Data.REFRESHTYPE,
	// GobalConstants.RefreshType.USER);
	// context.sendBroadcast(lIntent);
	// }
	//
	public static void sendRefreshMsgCenterBroadcast(Context context,
			int messagetype)
	{
		Intent lIntent = new Intent();
		lIntent.setAction(GobalConstants.IntentFilterAction.REFRESH);
		lIntent.putExtra(GobalConstants.Data.REFRESHTYPE,
				GobalConstants.RefreshType.MSGCENTER);
		lIntent.putExtra(GobalConstants.Data.MESSAGETYPE, messagetype);
		context.sendBroadcast(lIntent);
	}

	public static void sendRefreshWeatherBroadcast(Context context)
	{
		Intent lIntent = new Intent();
		lIntent.setAction(GobalConstants.IntentFilterAction.REFRESH);
		lIntent.putExtra(GobalConstants.Data.REFRESHTYPE,
				GobalConstants.RefreshType.WEATHER);
		context.sendBroadcast(lIntent);
	}

	public static void sendRefreshMoodBroadcast(Context context)
	{
		Intent lIntent = new Intent();
		lIntent.setAction(GobalConstants.IntentFilterAction.REFRESH);
		lIntent.putExtra(GobalConstants.Data.REFRESHTYPE,
				GobalConstants.RefreshType.MOOD);
		context.sendBroadcast(lIntent);
	}

	/*
	 * public static void sendRefreshWeatherBroadcast(Context context,String
	 * weatherCode){ Intent lIntent = new Intent();
	 * lIntent.setAction(GobalConstants.IntentFilterAction.REFRESH);
	 * lIntent.putExtra(GobalConstants.Data.REFRESHTYPE,
	 * GobalConstants.RefreshType.WEATHER);
	 * lIntent.putExtra(GobalConstants.Data.WEATHERCODE, weatherCode);
	 * context.sendBroadcast(lIntent); } public static void
	 * sendRefreshMoodBroadcast(Context context,String moodCode){ Intent lIntent
	 * = new Intent();
	 * lIntent.setAction(GobalConstants.IntentFilterAction.REFRESH);
	 * lIntent.putExtra(GobalConstants.Data.REFRESHTYPE,
	 * GobalConstants.RefreshType.MOOD);
	 * lIntent.putExtra(GobalConstants.Data.MOODCODE, moodCode);
	 * context.sendBroadcast(lIntent); }
	 */
	/*
	 * public static void sendRefreshNewReplyBroadcast(Context context){ Intent
	 * lIntent = new Intent();
	 * lIntent.setAction(GobalConstants.IntentFilterAction.REFRESH);
	 * lIntent.putExtra(GobalConstants.Data.REFRESHTYPE,
	 * GobalConstants.RefreshType.NEWREPLY); context.sendBroadcast(lIntent); }
	 * public static void sendRefreshNewAppreciationBroadcast(Context context){
	 * Intent lIntent = new Intent();
	 * lIntent.setAction(GobalConstants.IntentFilterAction.REFRESH);
	 * lIntent.putExtra(GobalConstants.Data.REFRESHTYPE,
	 * GobalConstants.RefreshType.NEWAPPRECIATION);
	 * context.sendBroadcast(lIntent); }
	 */
	// public static void sendRefreshQuestionBroadcast(Context context){
	// Intent lIntent = new Intent();
	// lIntent.setAction(GobalConstants.IntentFilterAction.REFRESH);
	// lIntent.putExtra(GobalConstants.Data.REFRESHTYPE,
	// GobalConstants.RefreshType.QUESTION);
	// context.sendBroadcast(lIntent);
	// }
	//
	public static void sendNewQuestionBroadcast(Context context,
			Question question)
	{
		Intent lIntent = new Intent();
		lIntent.setAction(GobalConstants.IntentFilterAction.REFRESH);
		lIntent.putExtra(GobalConstants.Data.REFRESHTYPE,
				GobalConstants.RefreshType.SENDNEWQUESTION);
		lIntent.putExtra(GobalConstants.Data.QUESTION, question);
		context.sendBroadcast(lIntent);
	}

	public static void sendCreateTopicBroadcast(Context context, Topic topic)
	{
		Intent lIntent = new Intent();
		lIntent.setAction(GobalConstants.IntentFilterAction.REFRESH);
		lIntent.putExtra(GobalConstants.Data.REFRESHTYPE,
				GobalConstants.RefreshType.SENDNEWPOSTTOPIC);
		lIntent.putExtra(GobalConstants.Data.TOPIC, topic);
		context.sendBroadcast(lIntent);
	}

	public static void sendNewPostReplyBroadcast(Context context, Post post)
	{
		Intent lIntent = new Intent();
		lIntent.setAction(GobalConstants.IntentFilterAction.REFRESH);
		lIntent.putExtra(GobalConstants.Data.REFRESHTYPE,
				GobalConstants.RefreshType.SENDNEWPOSTREPLY);
		lIntent.putExtra(GobalConstants.Data.POST, post);
		context.sendBroadcast(lIntent);
	}

	public static void sendFavQuestionBroadcast(Context context,
			Question question)
	{
		Intent lIntent = new Intent();
		lIntent.setAction(GobalConstants.IntentFilterAction.REFRESH);
		lIntent.putExtra(GobalConstants.Data.REFRESHTYPE,
				GobalConstants.RefreshType.QUESTIONFAV);
		lIntent.putExtra(GobalConstants.Data.QUESTION, question);
		context.sendBroadcast(lIntent);
	}

	public static void sendCancelFavArticleBroadcast(Context context,
			String articleId)
	{
		Intent lIntent = new Intent();
		lIntent.setAction(GobalConstants.IntentFilterAction.CANCELFAVARTICLE);
		lIntent.putExtra(GobalConstants.Data.ARTICLEID, articleId);
		lIntent.putExtra(GobalConstants.Data.FAVARTICLE,
				GobalConstants.FAVORCANCEL.CANCEL);
		context.sendBroadcast(lIntent);
	}

	public static void sendFavArticleBroadcast(Context context, String articleId)
	{
		Intent lIntent = new Intent();
		lIntent.setAction(GobalConstants.IntentFilterAction.CANCELFAVARTICLE);
		lIntent.putExtra(GobalConstants.Data.ARTICLEID, articleId);
		lIntent.putExtra(GobalConstants.Data.FAVARTICLE,
				GobalConstants.FAVORCANCEL.FAV);
		context.sendBroadcast(lIntent);
	}

	public static void sendFavTopicBroadcast(Context context, Topic topic)
	{
		Intent lIntent = new Intent();
		lIntent.setAction(GobalConstants.IntentFilterAction.REFRESH);
		lIntent.putExtra(GobalConstants.Data.TOPIC, topic);
		lIntent.putExtra(GobalConstants.Data.REFRESHTYPE,
				GobalConstants.RefreshType.TOPICFAV);
		context.sendBroadcast(lIntent);
	}

	
	public static void sendUpdateVersionBroadcast(Context context, String url,
			String note,int lastPrivateVer)
	{
		Intent lIntent = new Intent();
		lIntent.setAction(GobalConstants.IntentFilterAction.REFRESH);
		lIntent.putExtra(GobalConstants.Data.REFRESHTYPE,
				GobalConstants.RefreshType.UPDATEVERSION);
		lIntent.putExtra(GobalConstants.Version.URL, url);
		lIntent.putExtra(GobalConstants.Version.NOTE, note);
		lIntent.putExtra(GobalConstants.Version.LASTPRIVATEVER, lastPrivateVer);
		context.sendBroadcast(lIntent);
	}
}
