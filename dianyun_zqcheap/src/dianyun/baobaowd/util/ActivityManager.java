package dianyun.baobaowd.util;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;
import android.app.Activity;

public class ActivityManager
{

	private static Stack<Activity> activityStack;
	private static ActivityManager instance;

	private ActivityManager()
	{
	}

	public static ActivityManager getScreenManager()
	{
		if (instance == null)
		{
			instance = new ActivityManager();
		}
		return instance;
	}

	// 退出栈顶Activity
	public static void popActivity(Activity activity)
	{
		if (activity != null)
		{
			System.out.println("popActivity activity==" + activity);
			// 在从自定义集合中取出当前Activity时，也进行了Activity的关闭操作
			activity.finish();
			activityStack.remove(activity);
			activity = null;
		}
	}

	// 获得当前栈顶Activity
	public static Activity currentActivity()
	{
		Activity activity = null;
		if (activityStack!=null&&!activityStack.empty())
			activity = activityStack.lastElement();
		return activity;
	}

	// 将当前Activity推入栈中
	public static void pushActivity(Activity activity)
	{
		if (activityStack == null)
		{
			activityStack = new Stack<Activity>();
		}
		System.out.println("pushActivity activity==" + activity);
		activityStack.add(activity);
	}

	// 退出栈中所有Activity
	// public void popAllActivityExceptOne(Activity pActivity) {
	// while (true) {
	// Activity activity = currentActivity();
	// if (activity == null) {
	// break;
	// }
	// // if (activity.getClass().equals(cls)) {
	// // break;
	// // }
	// popActivity(activity);
	// }
	// }
	// public void popAllActivityExceptOne(Activity pActivity) {
	// for(int i=0;i<activityStack.size();i++){
	// Activity activity =activityStack.get(i);
	// if(activity!=pActivity){
	// popActivity(activity);
	// }
	// }
	// }
	// 退出栈中所有Activity
	// public void popAllActivityExceptOne(Activity pActivity) {
	// while (true) {
	// if (activityStack.size()==1) {
	// break;
	// }
	// Activity activity = currentActivity();
	// if (activity!=pActivity) {
	// popActivity(activity);
	// }
	// }
	// }
	public void popAllActivityExceptOne(Activity pActivity)
	{
		List<Activity> activityList = getRemoveActivity(pActivity);
		if (activityList != null && activityList.size() > 0)
		{
			for (Activity activity : activityList)
			{
				popActivity(activity);
			}
		}
	}

	private List<Activity> getRemoveActivity(Activity pActivity)
	{
		List<Activity> activityList = null;
		if (activityStack != null && activityStack.size() > 0)
		{
			activityList = new ArrayList<Activity>();
			for (int i = 0; i < activityStack.size(); i++)
			{
				Activity activity = activityStack.get(i);
				if (activity != pActivity)
				{
					activityList.add(activity);
				}
			}
		}
		return activityList;
	}

	// 退出栈中所有Activity
	public static void popAllActivity()
	{
		while (true)
		{
			Activity activity = currentActivity();
			if (activity == null)
			{
				break;
			}
			popActivity(activity);
		}
	}
}