package dianyun.baobaowd.util;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.UUID;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.media.ExifInterface;
import dianyun.baobaowd.help.FileHelper;
import dianyun.baobaowd.help.LogFile;

public class ImageHelper
{

	// 宽或高大于2048
	// Bitmap too large to be uploaded into a texture
	public static int IMGMAXSIZE = 1080;
	public static int IMGMINSIZE = 200;
	public static int IMGQUALITYE = 100;

	private static int getCameraImgRotateDegree(String path)
	{
		int degree = 0;
		ExifInterface exif;
		try
		{
			exif = new ExifInterface(path);
			int orientation = exif.getAttributeInt(
					ExifInterface.TAG_ORIENTATION, -1);
			switch (orientation)
			{
			case ExifInterface.ORIENTATION_ROTATE_90:
				degree = 90;
				break;
			case ExifInterface.ORIENTATION_ROTATE_180:
				degree = 180;
				break;
			case ExifInterface.ORIENTATION_ROTATE_270:
				degree = 270;
				break;
			}
		}
		catch (IOException e)
		{
			LogFile.SaveExceptionLog(e);
		}
		return degree;
	}

	/**
	 * 把图片变成圆角
	 * 
	 * @param bitmap
	 *            需要修改的图片
	 * @param pixels
	 *            圆角的弧度
	 * @return 圆角图片
	 */
	/*
	 * public static Bitmap toRoundCorner(Bitmap bitmap, int pixels) {
	 * 
	 * Bitmap output = Bitmap.createBitmap(bitmap.getWidth(),
	 * bitmap.getHeight(), Config.ARGB_8888); Canvas canvas = new
	 * Canvas(output);
	 * 
	 * final int color = 0xff424242; final Paint paint = new Paint();
	 * 
	 * 
	 * Rect rect = new Rect(0, 0,bitmap.getWidth() , bitmap.getHeight()); final
	 * RectF rectF = new RectF(rect); final float roundPx = pixels;
	 * 
	 * paint.setAntiAlias(true); canvas.drawARGB(0, 0, 0, 0);
	 * paint.setColor(color); canvas.drawRoundRect(rectF, roundPx, roundPx,
	 * paint); paint.setXfermode(new PorterDuffXfermode(Mode.SRC_IN));
	 * canvas.drawBitmap(bitmap, rect, rect, paint);
	 * 
	 * return output; }
	 */
	public static final Bitmap grey(Bitmap bitmap)
	{
		int width = bitmap.getWidth();
		int height = bitmap.getHeight();
		Bitmap faceIconGreyBitmap = Bitmap.createBitmap(width, height,
				Bitmap.Config.ARGB_8888);
		Canvas canvas = new Canvas(faceIconGreyBitmap);
		Paint paint = new Paint();
		ColorMatrix colorMatrix = new ColorMatrix();
		colorMatrix.setSaturation(0);
		ColorMatrixColorFilter colorMatrixFilter = new ColorMatrixColorFilter(
				colorMatrix);
		paint.setColorFilter(colorMatrixFilter);
		canvas.drawBitmap(bitmap, 0, 0, paint);
		return faceIconGreyBitmap;
	}

	// 头像花圆角
	public static Bitmap toRoundBitmap(Bitmap bitmap)
	{
		int width = bitmap.getWidth();
		int height = bitmap.getHeight();
		float roundPx;
		float left, top, right, bottom, dst_left, dst_top, dst_right, dst_bottom;
		if (width <= height)
		{
			roundPx = width / 2;
			top = 0;
			bottom = width;
			left = 0;
			right = width;
			height = width;
			dst_left = 0;
			dst_top = 0;
			dst_right = width;
			dst_bottom = width;
		}
		else
		{
			roundPx = height / 2;
			float clip = (width - height) / 2;
			left = clip;
			right = width - clip;
			top = 0;
			bottom = height;
			width = height;
			dst_left = 0;
			dst_top = 0;
			dst_right = height;
			dst_bottom = height;
		}
		Bitmap output = Bitmap.createBitmap(width, height, Config.ARGB_8888);
		Canvas canvas = new Canvas(output);
		final int color = 0xff424242;
		final Paint paint = new Paint();
		final Rect src = new Rect((int) left, (int) top, (int) right,
				(int) bottom);
		final Rect dst = new Rect((int) dst_left, (int) dst_top,
				(int) dst_right, (int) dst_bottom);
		final RectF rectF = new RectF(dst);
		paint.setAntiAlias(true);
		canvas.drawARGB(0, 0, 0, 0);
		paint.setColor(color);
		canvas.drawRoundRect(rectF, roundPx, roundPx, paint);
		paint.setXfermode(new PorterDuffXfermode(Mode.SRC_IN));
		canvas.drawBitmap(bitmap, src, dst, paint);
		return output;
	}

	// 图片画圆角
	public static Bitmap getRoundCornerBitmap(Bitmap bitmap, int width,
			int height, float roundPX)
	{
		Bitmap bitmap2 = Bitmap.createBitmap(width, height, Config.ARGB_8888);
		Canvas canvas = new Canvas(bitmap2);
		final int color = 0xff424242;
		final Paint paint = new Paint();
		final Rect rect = new Rect(0, 0, width, height);
		final RectF rectF = new RectF(rect);
		paint.setColor(color);
		paint.setAntiAlias(true);
		canvas.drawARGB(0, 0, 0, 0);
		canvas.drawRoundRect(rectF, roundPX, roundPX, paint);
		paint.setXfermode(new PorterDuffXfermode(Mode.SRC_IN));
		Matrix matrix = new Matrix();
		float scaleWidth = (float) width / bitmap.getWidth();
		float scaleHeight = (float) height / bitmap.getHeight();
		float scale = scaleWidth > scaleHeight ? scaleWidth : scaleHeight;
		matrix.postScale(scale, scale);
		if (scaleWidth > scaleHeight)
		{
			float newHeight = scaleWidth * bitmap.getHeight();
			int top = (int) ((newHeight - height) / 2);
			top = (int) (top / scaleWidth);
			bitmap = Bitmap.createBitmap(bitmap, 0, top, bitmap.getWidth(),
					bitmap.getHeight() - top, matrix, true);
		}
		else
		{
			float newWidth = scaleHeight * bitmap.getWidth();
			int left = (int) ((newWidth - width) / 2);
			left = (int) (left / scaleHeight);
			bitmap = Bitmap.createBitmap(bitmap, left, 0, bitmap.getWidth()
					- left, bitmap.getHeight(), matrix, true);
		}
		canvas.drawBitmap(bitmap, rect, rect, paint);
		return bitmap2;
	}

	/*
	 * public static Bitmap getRoundCornerBitmap(Bitmap bitmap,int width,int
	 * height, int roundPX) { //根据源文件新建一个darwable对象 Drawable imageDrawable = new
	 * BitmapDrawable(bitmap);
	 * 
	 * // 新建一个新的输出图片 Bitmap output = Bitmap.createBitmap(width, height,
	 * Bitmap.Config.ARGB_8888); Canvas canvas = new Canvas(output);
	 * 
	 * // 新建一个矩形 RectF outerRect = new RectF(0, 0, width, height);
	 * 
	 * // 产生一个红色的圆角矩形 Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
	 * paint.setColor(Color.RED); canvas.drawRoundRect(outerRect, roundPX,
	 * roundPX, paint);
	 * 
	 * // 将源图片绘制到这个圆角矩形上 paint.setXfermode(new
	 * PorterDuffXfermode(PorterDuff.Mode.SRC_IN)); imageDrawable.setBounds(0,
	 * 0, width, height); canvas.saveLayer(outerRect, paint,
	 * Canvas.ALL_SAVE_FLAG); imageDrawable.draw(canvas); canvas.restore();
	 * 
	 * return output; }
	 */
	public static byte[] bmpToByteArray(final Bitmap bmp,
			final boolean needRecycle)
	{
		ByteArrayOutputStream output = new ByteArrayOutputStream();
		bmp.compress(CompressFormat.PNG, 100, output);
		if (needRecycle)
		{
			bmp.recycle();
		}
		byte[] result = output.toByteArray();
		try
		{
			output.close();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		return result;
	}

	// 以maxsize 大小为参考 旋转图片 压缩图片
	/*
	 * public static Bitmap getBitmapByMaxSize(String pathString,int maxSize){
	 * int degree = getCameraImgRotateDegree(pathString); Bitmap bm =null;
	 * if(degree != 0){ Matrix matrix = new Matrix(); matrix.postRotate(degree);
	 * BitmapFactory.Options options= new BitmapFactory.Options();
	 * options.inSampleSize=getSampleSize(pathString, maxSize); bm =
	 * BitmapFactory.decodeFile(pathString,options); bm =
	 * Bitmap.createBitmap(bm, 0, 0, bm.getWidth(), bm.getHeight(), matrix,
	 * true); }else{ BitmapFactory.Options options= new BitmapFactory.Options();
	 * options.inSampleSize=getSampleSize(pathString, maxSize); bm =
	 * BitmapFactory.decodeFile(pathString,options); }
	 * 
	 * return bm; }
	 */
	public static Bitmap getBitmapByMaxSize(String pathString, int maxSize)
	{
		int degree = getCameraImgRotateDegree(pathString);
		Bitmap bm = null;
		Matrix matrix = new Matrix();
		BitmapFactory.Options options = new BitmapFactory.Options();
		options.inSampleSize = getSampleSize(pathString, maxSize);
		bm = BitmapFactory.decodeFile(pathString, options);
		if (degree != 0)
		{
			matrix.postRotate(degree);
			bm = Bitmap.createBitmap(bm, 0, 0, bm.getWidth(), bm.getHeight(),
					matrix, true);
		}
		return bm;
	}

	public static Bitmap getSmallBitmap(Bitmap bm, int needSize)
	{
		int bmWidth = bm.getWidth();
		int bmHeight = bm.getHeight();
		Matrix matrix = new Matrix();
		int shortSize = bmWidth < bmHeight ? bmWidth : bmHeight;
		float scaleSize = ((float) needSize) / shortSize;
		matrix.postScale(scaleSize, scaleSize);
		bm = Bitmap.createBitmap(bm, 0, 0, bmWidth, bmHeight, matrix, true);
		return bm;
	}

	/*
	 * public static Bitmap getBitmapBySize(String pathString,int maxSize,int
	 * needSize){ int degree = getCameraImgRotateDegree(pathString); Bitmap bm
	 * =null; BitmapFactory.Options options= new BitmapFactory.Options();
	 * options.inSampleSize=getSampleSize(pathString, maxSize); bm =
	 * BitmapFactory.decodeFile(pathString,options); int bmWidth =
	 * bm.getWidth(); int bmHeight = bm.getHeight(); Matrix matrix = new
	 * Matrix(); if(degree != 0){ matrix.postRotate(degree); } int shortSize =
	 * bmWidth<bmHeight?bmWidth:bmHeight; float scaleSize = ((float) needSize)
	 * /shortSize ; matrix.postScale(scaleSize, scaleSize); bm =
	 * Bitmap.createBitmap(bm, 0, 0, bmWidth, bmHeight, matrix, true); return
	 * bm; }
	 */
	// filePath 完整路径12312/123.jpg
	public static String saveImgData(Bitmap bitmap)
	{
		String filePath = FileHelper.getDataPath()
				+ UUID.randomUUID().toString()
				+ GobalConstants.Suffix.PIC_SUFFIX_JPG;
		File file = new File(filePath);
		OutputStream out = null;
		try
		{
			if (!file.exists())
				file.createNewFile();
			out = new FileOutputStream(file);
			if (null != bitmap && filePath != null)
			{
				if (filePath.endsWith(".jpg") || filePath.endsWith(".JPG"))
				{
					bitmap.compress(Bitmap.CompressFormat.JPEG, 100, out);
				}
				else
				{
					bitmap.compress(Bitmap.CompressFormat.PNG, 100, out);
				}
				out.flush();
				return file.getAbsolutePath();
			}
			else
			{
				return null;
			}
		}
		catch (Exception e)
		{
			return null;
		}
		finally
		{
			if (null != out)
				try
				{
					out.close();
				}
				catch (IOException e)
				{
					// LogFile.SaveExceptionLog(e);
				}
		}
	}
	public static String saveFavImgData(Bitmap bitmap,String filePath)
	{
	
		File file = new File(filePath);
		OutputStream out = null;
		try
		{
			if (!file.exists())
				file.createNewFile();
			out = new FileOutputStream(file);
			if (null != bitmap && filePath != null)
			{
				if (filePath.endsWith(".jpg") || filePath.endsWith(".JPG"))
				{
					bitmap.compress(Bitmap.CompressFormat.JPEG, 100, out);
				}
				else
				{
					bitmap.compress(Bitmap.CompressFormat.PNG, 100, out);
				}
				out.flush();
				return file.getAbsolutePath();
			}
			else
			{
				return null;
			}
		}
		catch (Exception e)
		{
			return null;
		}
		finally
		{
			if (null != out)
				try
			{
					out.close();
			}
			catch (IOException e)
			{
				// LogFile.SaveExceptionLog(e);
			}
		}
	}

	public static String saveSmallImgData(Bitmap bitmap, String fileName)
	{
		String filePath = FileHelper.getSmallImgPath() + fileName;
		File file = new File(filePath);
		OutputStream out = null;
		try
		{
			if (!file.exists())
				file.createNewFile();
			out = new FileOutputStream(file);
			if (null != bitmap && filePath != null)
			{
				if (filePath.endsWith(".jpg") || filePath.endsWith(".JPG"))
				{
					bitmap.compress(Bitmap.CompressFormat.JPEG, 100, out);
				}
				else
				{
					bitmap.compress(Bitmap.CompressFormat.PNG, 100, out);
				}
				out.flush();
				return file.getAbsolutePath();
			}
			else
			{
				return null;
			}
		}
		catch (Exception e)
		{
			return null;
		}
		finally
		{
			if (null != out)
				try
				{
					out.close();
				}
				catch (IOException e)
				{
					// LogFile.SaveExceptionLog(e);
				}
		}
	}

	/*
	 * public static String saveImgDataByQuality(Bitmap bitmap,int quality) {
	 * String filePath =
	 * FileHelper.getDataPath()+UUID.randomUUID().toString()+GobalConstants
	 * .Suffix.PIC_SUFFIX_JPG; File file = new File(filePath); OutputStream out
	 * = null; try { if (!file.exists()) file.createNewFile(); out = new
	 * FileOutputStream(file); if (null != bitmap&&filePath!=null) {
	 * if(filePath.endsWith(".png")||filePath.endsWith(".PNG")){
	 * bitmap.compress(Bitmap.CompressFormat.PNG, quality, out); }else{
	 * bitmap.compress(Bitmap.CompressFormat.JPEG, quality, out);
	 * 
	 * } out.flush(); return file.getAbsolutePath(); } else { return null; } }
	 * catch (Exception e) { return null; } finally { if (null != out) try {
	 * out.close(); } catch (IOException e) { // LogFile.SaveExceptionLog(e); }
	 * } }
	 */
	/*
	 * public static String saveImgDataByQualitySuffix(Bitmap bitmap,int
	 * quality,String suffix) { String filePath =
	 * FileHelper.getDataPath()+UUID.randomUUID().toString()+suffix; File file =
	 * new File(filePath); OutputStream out = null; try { if (!file.exists())
	 * file.createNewFile(); out = new FileOutputStream(file); if (null !=
	 * bitmap&&filePath!=null) {
	 * if(filePath.endsWith(".png")||filePath.endsWith(".PNG")){
	 * bitmap.compress(Bitmap.CompressFormat.PNG, quality, out); }else{
	 * bitmap.compress(Bitmap.CompressFormat.JPEG, quality, out);
	 * 
	 * } out.flush(); return file.getAbsolutePath(); } else { return null; } }
	 * catch (Exception e) { return null; } finally { if (null != out) try {
	 * out.close(); } catch (IOException e) { // LogFile.SaveExceptionLog(e); }
	 * } }
	 */
	public static long getBitmapMemery(String path)
	{
		BitmapFactory.Options options = new BitmapFactory.Options();
		// 设置只取长宽
		options.inJustDecodeBounds = false;
		Bitmap bm = BitmapFactory.decodeFile(path, options);
		int width = options.outWidth;// 实际宽度
		int height = options.outHeight;// 实际高度
		System.out.println("bm.getConfig()=" + bm.getConfig() + "  width="
				+ width + "  height=" + height);
		long memory = 0;
		if (bm.getConfig() == Bitmap.Config.ALPHA_8)
		{
			memory = width * height;
		}
		else
			if (bm.getConfig() == Bitmap.Config.ARGB_4444
					|| bm.getConfig() == Bitmap.Config.RGB_565)
			{
				memory = width * height * 2;
			}
			else
				if (bm.getConfig() == Bitmap.Config.ARGB_8888)
				{
					memory = width * height * 4;
				}
		System.out.println("memory=" + memory);
		return memory;
	}

	private static int getSampleSize(String path, int size)
	{
		// 得到原始图片宽高
		BitmapFactory.Options opts = new BitmapFactory.Options();
		opts.inJustDecodeBounds = true;
		BitmapFactory.decodeFile(path, opts);
		double width = opts.outWidth;
		double height = opts.outHeight;
		int sampleSize = 1;
		if (width > height && width > size)
		{
			sampleSize = (int) Math.floor(width / size);
		}
		else
			if (width < height && height > size)
			{
				sampleSize = (int) Math.floor(height / size);
			}
		return sampleSize;
	}

	// 内存2M为限
	private static int overMemory(int width, int height)
	{
		// 图片所占内存
		int size = width * height * 4 / 1024 / 1024;
		// 和6m比较
		return size / 6;
	}

	// 宽或高变为指定大小 另一半同比例压缩
	/*
	 * public static Bitmap getNewBitmap(Bitmap bm,int newSize){ int width =
	 * bm.getWidth(); int height = bm.getHeight();
	 * if(width<height&&height>newSize){ float scaleWidth = ((float) newSize) /
	 * height; Matrix matrix = new Matrix(); matrix.postScale(scaleWidth,
	 * scaleWidth); Bitmap newbm = Bitmap.createBitmap(bm, 0, 0, width, height,
	 * matrix, true); return newbm;
	 * 
	 * }else if(height<width&&width>newSize){ float scaleWidth = ((float)
	 * newSize) / width; Matrix matrix = new Matrix();
	 * matrix.postScale(scaleWidth, scaleWidth); Bitmap newbm =
	 * Bitmap.createBitmap(bm, 0, 0, width, height, matrix, true); return newbm;
	 * } return bm; }
	 */
	// 按照宽度等比例拉伸或压缩
	public static Bitmap getBitmapByWidth(Bitmap bm, int newSize)
	{
		int width = bm.getWidth();
		int height = bm.getHeight();
		float scale = ((float) newSize) / width;
		if (height * scale < newSize * 2)
		{
			Matrix matrix = new Matrix();
			matrix.postScale(scale, scale);
			Bitmap newbm = Bitmap.createBitmap(bm, 0, 0, width, height, matrix,
					true);
			return newbm;
		}
		else
		{
			return null;
		}
	}
	/*
	 * public static Bitmap getShortBitmap(String filePath,int size){ if
	 * (filePath == null || new File(filePath).isDirectory()) return null; int
	 * scalesize = 1; float targetHeight; float targetWidth; try{
	 * BitmapFactory.Options options = new BitmapFactory.Options(); //设置只取长宽
	 * options.inJustDecodeBounds = true;
	 * 
	 * Bitmap bm = BitmapFactory.decodeFile(filePath,options); int width =
	 * options.outWidth;//实际宽度 int height = options.outHeight;//实际高度
	 * 
	 * // if(width > height){ if(height >size){ targetHeight = size; targetWidth
	 * = targetHeight*width/height; scalesize = height/size;
	 * options.inSampleSize = scalesize; options.inJustDecodeBounds = false; bm
	 * = BitmapFactory.decodeFile(filePath,options); bm =
	 * Bitmap.createScaledBitmap(bm, (int) targetWidth, (int) targetHeight,
	 * false); bm = Bitmap.createBitmap(bm, (bm.getWidth()-size)/2, 0, size,
	 * size); }else{ targetHeight = size; targetWidth =
	 * targetHeight*width/height; scalesize = height/size; options.inSampleSize
	 * = scalesize; options.inJustDecodeBounds = false; bm =
	 * BitmapFactory.decodeFile(filePath,options); bm =
	 * Bitmap.createScaledBitmap(bm, (int) targetWidth, (int) targetHeight,
	 * false); bm = Bitmap.createBitmap(bm, 0, 0, size, size); } }else{ if(width
	 * > size){ targetWidth = size; targetHeight = targetWidth*height/width;
	 * scalesize = width/size; options.inSampleSize = scalesize;
	 * options.inJustDecodeBounds = false; bm =
	 * BitmapFactory.decodeFile(filePath,options); bm =
	 * Bitmap.createScaledBitmap(bm, (int) targetWidth, (int) targetHeight,
	 * false); bm = Bitmap.createBitmap(bm, 0, (bm.getHeight()-size)/2, size,
	 * size);
	 * 
	 * }else{ targetWidth = size; targetHeight = targetWidth*height/width;
	 * scalesize = width/size; options.inSampleSize = scalesize;
	 * options.inJustDecodeBounds = false; bm =
	 * BitmapFactory.decodeFile(filePath,options); bm =
	 * Bitmap.createScaledBitmap(bm, (int) targetWidth, (int) targetHeight,
	 * false); bm = Bitmap.createBitmap(bm, 0, 0, size, size); } }
	 * 
	 * return bm; }catch(Exception e){ // LogFile.SaveExceptionLog(e); } return
	 * null; } //拍照专用 public static Bitmap getNewBitmapByHeight(Bitmap bm,int
	 * newSize){ int width = bm.getWidth(); int height = bm.getHeight();
	 * if(height>newSize){ float scaleWidth = ((float) newSize) / height; Matrix
	 * matrix = new Matrix(); matrix.postScale(scaleWidth, scaleWidth); Bitmap
	 * newbm = Bitmap.createBitmap(bm, 0, 0, width, height, matrix, true);
	 * return newbm;
	 * 
	 * } return bm; }
	 * 
	 * 
	 * public static Bitmap getNewBitmap(Bitmap bm,int newSize){ int width =
	 * bm.getWidth(); int height = bm.getHeight();
	 * if(width<height&&height>newSize){ float scaleWidth = ((float) newSize) /
	 * height; Matrix matrix = new Matrix(); matrix.postScale(scaleWidth,
	 * scaleWidth); Bitmap newbm = Bitmap.createBitmap(bm, 0, 0, width, height,
	 * matrix, true); return newbm;
	 * 
	 * }else if(height<width&&width>newSize){ float scaleWidth = ((float)
	 * newSize) / width; Matrix matrix = new Matrix();
	 * matrix.postScale(scaleWidth, scaleWidth); Bitmap newbm =
	 * Bitmap.createBitmap(bm, 0, 0, width, height, matrix, true); return newbm;
	 * } return bm; }
	 * 
	 * public static int getSampleSize(String path,int size){ //得到原始图片宽高
	 * BitmapFactory.Options opts = new BitmapFactory.Options();
	 * opts.inJustDecodeBounds = true; BitmapFactory.decodeFile(path, opts); int
	 * oriWidth = opts.outWidth; int oriHeight = opts.outHeight; int sampleSize
	 * =0; if(oriWidth<2048&&oriHeight<2048){ sampleSize =1; }else {
	 * if(oriWidth>oriHeight){ sampleSize = oriWidth/size; }else{ sampleSize =
	 * oriHeight/size; } } return sampleSize; }
	 * 
	 * public static int getSampleSize(Bitmap bitmap,int size){ int oriWidth =
	 * bitmap.getWidth(); int oriHeight = bitmap.getHeight(); int sampleSize =0;
	 * if(oriWidth<2048&&oriHeight<2048){ sampleSize =1; }else {
	 * if(oriWidth>oriHeight){ sampleSize = oriWidth/size; }else{ sampleSize =
	 * oriHeight/size; } } return sampleSize; }
	 */
	
	
	

/** 水平方向模糊度 */
private static float hRadius = 10;
/** 竖直方向模糊度 */
private static float vRadius = 10;
/** 模糊迭代度 */
private static int iterations = 7;

/**
     * 高斯模糊
     */
    public static Bitmap BoxBlurFilter(Bitmap bmp) {
        int width = bmp.getWidth();
        int height = bmp.getHeight();
        int[] inPixels = new int[width * height];
        int[] outPixels = new int[width * height];
        Bitmap bitmap = Bitmap.createBitmap(width, height,Bitmap.Config.ARGB_8888);
        bmp.getPixels(inPixels, 0, width, 0, 0, width, height);
        for (int i = 0; i < iterations; i++) {
            blur(inPixels, outPixels, width, height, hRadius);
            blur(outPixels, inPixels, height, width, vRadius);
        }
        blurFractional(inPixels, outPixels, width, height, hRadius);
        blurFractional(outPixels, inPixels, height, width, vRadius);
        bitmap.setPixels(inPixels, 0, width, 0, 0, width, height);
//        Drawable drawable = new BitmapDrawable(bitmap);
        return bitmap;
    }
	
	
	
	

public static void blur(int[] in, int[] out, int width, int height,
            float radius) {
        int widthMinus1 = width - 1;
        int r = (int) radius;
        int tableSize = 2 * r + 1;
        int divide[] = new int[256 * tableSize];

        for (int i = 0; i < 256 * tableSize; i++)
            divide[i] = i / tableSize;

        int inIndex = 0;

        for (int y = 0; y < height; y++) {
            int outIndex = y;
            int ta = 0, tr = 0, tg = 0, tb = 0;

            for (int i = -r; i <= r; i++) {
                int rgb = in[inIndex + clamp(i, 0, width - 1)];
                ta += (rgb >> 24) & 0xff;
                tr += (rgb >> 16) & 0xff;
                tg += (rgb >> 8) & 0xff;
                tb += rgb & 0xff;
            }

            for (int x = 0; x < width; x++) {
                out[outIndex] = (divide[ta] << 24) | (divide[tr] << 16)
                        | (divide[tg] << 8) | divide[tb];

                int i1 = x + r + 1;
                if (i1 > widthMinus1)
                    i1 = widthMinus1;
                int i2 = x - r;
                if (i2 < 0)
                    i2 = 0;
                int rgb1 = in[inIndex + i1];
                int rgb2 = in[inIndex + i2];

                ta += ((rgb1 >> 24) & 0xff) - ((rgb2 >> 24) & 0xff);
                tr += ((rgb1 & 0xff0000) - (rgb2 & 0xff0000)) >> 16;
                tg += ((rgb1 & 0xff00) - (rgb2 & 0xff00)) >> 8;
                tb += (rgb1 & 0xff) - (rgb2 & 0xff);
                outIndex += height;
            }
            inIndex += width;
        }
    }

    public static void blurFractional(int[] in, int[] out, int width,
            int height, float radius) {
        radius -= (int) radius;
        float f = 1.0f / (1 + 2 * radius);
        int inIndex = 0;

        for (int y = 0; y < height; y++) {
            int outIndex = y;

            out[outIndex] = in[0];
            outIndex += height;
            for (int x = 1; x < width - 1; x++) {
                int i = inIndex + x;
                int rgb1 = in[i - 1];
                int rgb2 = in[i];
                int rgb3 = in[i + 1];

                int a1 = (rgb1 >> 24) & 0xff;
                int r1 = (rgb1 >> 16) & 0xff;
                int g1 = (rgb1 >> 8) & 0xff;
                int b1 = rgb1 & 0xff;
                int a2 = (rgb2 >> 24) & 0xff;
                int r2 = (rgb2 >> 16) & 0xff;
                int g2 = (rgb2 >> 8) & 0xff;
                int b2 = rgb2 & 0xff;
                int a3 = (rgb3 >> 24) & 0xff;
                int r3 = (rgb3 >> 16) & 0xff;
                int g3 = (rgb3 >> 8) & 0xff;
                int b3 = rgb3 & 0xff;
                a1 = a2 + (int) ((a1 + a3) * radius);
                r1 = r2 + (int) ((r1 + r3) * radius);
                g1 = g2 + (int) ((g1 + g3) * radius);
                b1 = b2 + (int) ((b1 + b3) * radius);
                a1 *= f;
                r1 *= f;
                g1 *= f;
                b1 *= f;
                out[outIndex] = (a1 << 24) | (r1 << 16) | (g1 << 8) | b1;
                outIndex += height;
            }
            out[outIndex] = in[width - 1];
            inIndex += width;
        }
    }
	
    public static int clamp(int x, int a, int b) {
        return (x < a) ? a : (x > b) ? b : x;
    }
	
	
	 public static Bitmap doBlur(Bitmap sentBitmap, int radius, boolean canReuseInBitmap) {  
		  
	      
	        Bitmap bitmap;  
	        if (canReuseInBitmap) {  
	            bitmap = sentBitmap;  
	        } else {  
	            bitmap = sentBitmap.copy(sentBitmap.getConfig(), true);  
	        }  
	  
	        if (radius < 1) {  
	            return (null);  
	        }  
	  
	        int w = bitmap.getWidth();  
	        int h = bitmap.getHeight();  
	  
	        int[] pix = new int[w * h];  
	        bitmap.getPixels(pix, 0, w, 0, 0, w, h);  
	  
	        int wm = w - 1;  
	        int hm = h - 1;  
	        int wh = w * h;  
	        int div = radius + radius + 1;  
	  
	        int r[] = new int[wh];  
	        int g[] = new int[wh];  
	        int b[] = new int[wh];  
	        int rsum, gsum, bsum, x, y, i, p, yp, yi, yw;  
	        int vmin[] = new int[Math.max(w, h)];  
	  
	        int divsum = (div + 1) >> 1;  
	        divsum *= divsum;  
	        int dv[] = new int[256 * divsum];  
	        for (i = 0; i < 256 * divsum; i++) {  
	            dv[i] = (i / divsum);  
	        }  
	  
	        yw = yi = 0;  
	  
	        int[][] stack = new int[div][3];  
	        int stackpointer;  
	        int stackstart;  
	        int[] sir;  
	        int rbs;  
	        int r1 = radius + 1;  
	        int routsum, goutsum, boutsum;  
	        int rinsum, ginsum, binsum;  
	  
	        for (y = 0; y < h; y++) {  
	            rinsum = ginsum = binsum = routsum = goutsum = boutsum = rsum = gsum = bsum = 0;  
	            for (i = -radius; i <= radius; i++) {  
	                p = pix[yi + Math.min(wm, Math.max(i, 0))];  
	                sir = stack[i + radius];  
	                sir[0] = (p & 0xff0000) >> 16;  
	                sir[1] = (p & 0x00ff00) >> 8;  
	                sir[2] = (p & 0x0000ff);  
	                rbs = r1 - Math.abs(i);  
	                rsum += sir[0] * rbs;  
	                gsum += sir[1] * rbs;  
	                bsum += sir[2] * rbs;  
	                if (i > 0) {  
	                    rinsum += sir[0];  
	                    ginsum += sir[1];  
	                    binsum += sir[2];  
	                } else {  
	                    routsum += sir[0];  
	                    goutsum += sir[1];  
	                    boutsum += sir[2];  
	                }  
	            }  
	            stackpointer = radius;  
	  
	            for (x = 0; x < w; x++) {  
	  
	                r[yi] = dv[rsum];  
	                g[yi] = dv[gsum];  
	                b[yi] = dv[bsum];  
	  
	                rsum -= routsum;  
	                gsum -= goutsum;  
	                bsum -= boutsum;  
	  
	                stackstart = stackpointer - radius + div;  
	                sir = stack[stackstart % div];  
	  
	                routsum -= sir[0];  
	                goutsum -= sir[1];  
	                boutsum -= sir[2];  
	  
	                if (y == 0) {  
	                    vmin[x] = Math.min(x + radius + 1, wm);  
	                }  
	                p = pix[yw + vmin[x]];  
	  
	                sir[0] = (p & 0xff0000) >> 16;  
	                sir[1] = (p & 0x00ff00) >> 8;  
	                sir[2] = (p & 0x0000ff);  
	  
	                rinsum += sir[0];  
	                ginsum += sir[1];  
	                binsum += sir[2];  
	  
	                rsum += rinsum;  
	                gsum += ginsum;  
	                bsum += binsum;  
	  
	                stackpointer = (stackpointer + 1) % div;  
	                sir = stack[(stackpointer) % div];  
	  
	                routsum += sir[0];  
	                goutsum += sir[1];  
	                boutsum += sir[2];  
	  
	                rinsum -= sir[0];  
	                ginsum -= sir[1];  
	                binsum -= sir[2];  
	  
	                yi++;  
	            }  
	            yw += w;  
	        }  
	        for (x = 0; x < w; x++) {  
	            rinsum = ginsum = binsum = routsum = goutsum = boutsum = rsum = gsum = bsum = 0;  
	            yp = -radius * w;  
	            for (i = -radius; i <= radius; i++) {  
	                yi = Math.max(0, yp) + x;  
	  
	                sir = stack[i + radius];  
	  
	                sir[0] = r[yi];  
	                sir[1] = g[yi];  
	                sir[2] = b[yi];  
	  
	                rbs = r1 - Math.abs(i);  
	  
	                rsum += r[yi] * rbs;  
	                gsum += g[yi] * rbs;  
	                bsum += b[yi] * rbs;  
	  
	                if (i > 0) {  
	                    rinsum += sir[0];  
	                    ginsum += sir[1];  
	                    binsum += sir[2];  
	                } else {  
	                    routsum += sir[0];  
	                    goutsum += sir[1];  
	                    boutsum += sir[2];  
	                }  
	  
	                if (i < hm) {  
	                    yp += w;  
	                }  
	            }  
	            yi = x;  
	            stackpointer = radius;  
	            for (y = 0; y < h; y++) {  
	                // Preserve alpha channel: ( 0xff000000 & pix[yi] )  
	                pix[yi] = (0xff000000 & pix[yi]) | (dv[rsum] << 16) | (dv[gsum] << 8) | dv[bsum];  
	  
	                rsum -= routsum;  
	                gsum -= goutsum;  
	                bsum -= boutsum;  
	  
	                stackstart = stackpointer - radius + div;  
	                sir = stack[stackstart % div];  
	  
	                routsum -= sir[0];  
	                goutsum -= sir[1];  
	                boutsum -= sir[2];  
	  
	                if (x == 0) {  
	                    vmin[y] = Math.min(y + r1, hm) * w;  
	                }  
	                p = x + vmin[y];  
	  
	                sir[0] = r[p];  
	                sir[1] = g[p];  
	                sir[2] = b[p];  
	  
	                rinsum += sir[0];  
	                ginsum += sir[1];  
	                binsum += sir[2];  
	  
	                rsum += rinsum;  
	                gsum += ginsum;  
	                bsum += binsum;  
	  
	                stackpointer = (stackpointer + 1) % div;  
	                sir = stack[stackpointer];  
	  
	                routsum += sir[0];  
	                goutsum += sir[1];  
	                boutsum += sir[2];  
	  
	                rinsum -= sir[0];  
	                ginsum -= sir[1];  
	                binsum -= sir[2];  
	  
	                yi += w;  
	            }  
	        }  
	  
	        bitmap.setPixels(pix, 0, w, 0, 0, w, h);  
	  
	        return (bitmap);  
	    } 
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
