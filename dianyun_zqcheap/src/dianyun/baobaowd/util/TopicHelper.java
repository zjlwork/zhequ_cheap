package dianyun.baobaowd.util;


/**
 * 联系人数据库操作辅助
 * 
 */
public class TopicHelper
{

	public static final String TAOBAO = ".taobao.com";
	public static final String JINDONG = ".jd.com";
	public static final String YAMAXUN = ".amazon.cn";
	public static final String SULING = ".suning.com";
	public static final String TMALL = ".tmall.com";
	public static final String YHD = ".yhd.com";
	public static final String GOME = ".gome.com";
	public static final String VIP = ".vip.com";
	public static final String YOUYOUURL = "http://www.ask360.me";
	public static final String BEIBEI = ".beibei.com";
	public static final String ASKME = ".ask360.me";
	public static final String YOYOURL = ".yoyo360.cn";

	public static boolean commodityidNeedAutoLink(String host)
	{
		if (host != null)
		{
			if (host.toLowerCase().contains(TAOBAO)
					|| host.toLowerCase().contains(JINDONG)
					|| host.toLowerCase().contains(YAMAXUN)
					|| host.toLowerCase().contains(SULING)
					|| host.toLowerCase().contains(TMALL)
					|| host.toLowerCase().contains(YHD)
					|| host.toLowerCase().contains(GOME)
					|| host.toLowerCase().contains(VIP)
					|| host.toLowerCase().contains(YOUYOUURL)
					|| host.toLowerCase().contains(BEIBEI)
					|| host.toLowerCase().contains(YOYOURL)
					|| host.toLowerCase().contains(ASKME))
				return true;
		}
		
		return false;
	}

	public static boolean talkNeedAutoLink(String str)
	{
		if (str != null)
		{
			if (str.contains(ASKME) || str.contains(YOYOURL))
				return true;
		}
		return false;
	}

	
}
