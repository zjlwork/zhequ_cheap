package dianyun.baobaowd.util;


public class GobalConstants
{

	/**
	 * 正式服务和测试服务地址开关 true:正式 false:测试
	 */
	public static boolean mIsOfficalRest = true;
	public static class URL
	{

        public static final String MAIN_HIGHLESTID=mIsOfficalRest?"697":"697";
        public static final String NINE_NINECHEEAPEST=mIsOfficalRest?"696":"696";


        // 重要
		// RegisterPushHelper registerPush 别忘记 测试服 正式服切换 logfile 是否记录
		// 正式服务器
		public static final String DEFAULTPREFIX = mIsOfficalRest ? "http://api.yoyo360.cn/rest/"
				: "http://api.test.keep.im:8119/ask/rest/";
		public static final String WEBHTMLBASE = mIsOfficalRest ? "http://www.ask360.me"
				: "http://www.test.ask360.me";
		public static final String YOYOBASE = mIsOfficalRest ? "http://www.yoyo360.cn"
				: "http://garden.test.keep.im/yoyo";
		
		
		
				/**
		 * 绑定淘宝和本地账户的接口url
		 */
		public static final String SHOP_BINDACCOUNT = mIsOfficalRest ? "http://api.ask360.me:8119/ask/rest/user/fanliTaoBind.json"
				: "http://api.test.ask360.me:8119/ask/rest/user/fanliTaoBind.json";
		/**
		 * 扫一扫使用说明URL
		 * 
		 */
		public static final String SHOP_SCAN_HELP_URL = "/doc/fanligo_qrcode.html";
		/**
		 * 默认的商品Http请求前缀
		 * 
		 */
		public static final String SHOP_DEFAULT_PREFIX = "http://fanli.wx.jaeapp.com/";
		/**
		 * 默认的扫描解析URL请求
		 */
		public static final String SCAN_CHECK_DEFAULT_PREFIX = mIsOfficalRest?"http://tburl.yoyo360.cn:8080/tburl/item2.json":"http://tburl.yoyo360.cn:8080/tburltest/item2.json";
        /**
         * 默认的获取短链的URL请求地址
         */
        public static final String GET_CHECK_DEFAULT_PREFIX = mIsOfficalRest?"http://tburl.yoyo360.cn:8080/tburl/exturl.json":"http://tburl.yoyo360.cn:8080/tburltest/exturl.json";

        /**
         * 默认的获取获取关键字搜索结果的URL请求地址
         */
        public static final String GET_SEARCH_RESULT_WITH_PRIMARYKEY_URL_DEFAULT_PREFIX = mIsOfficalRest?"http://fanli.wx.jaeapp.com/item/search.json":"http://yyfmalltest-1.wx.jaeapp.com/item/search.json";
        /**
         * 默认获取检测URL的http
         */
        public static final String GET_FETCH_PRIMARY_WORDS_WITH_URL_DEFAULT_PREFIX = mIsOfficalRest?"http://tburl.yoyo360.cn:8080/tburl/extract.json":"http://tburl.yoyo360.cn:8080/tburltest/extract.json";

        public static final String SEARCHBASE = "http://search.ask360.me:8119";
		// public static final String
		// SHAREDEFAULTARTICLE="http://www.ask360.me/article/8e6f9ac4-164a-1e33-aff1-b36d523ec183.html";
		public static final String SHAREIMAGEURL = "http://img.yoyo360.cn/icon/zhequ_logo.png";
		public static final String APPLYFORMAINWEBSITE = "http://a.app.qq.com/o/simple.jsp?pkgname=dianyun.baobaowd&g_f=991653";
		
		public static final String MIAOSHAROLE ="http://fanlitao.yoyo360.cn/h5/activity/2/347f1b75-2073-481f-ba1f-97ea47694bad.html";

		
		
		
		public static final String HOSPITAL = "/yisheng.html";
		public static final String GETSEARCH = "/search/rest/search.json";
		public static final String GETSEARCHWORDS = "/search/rest/words.json";
		public static final String USERAVATARSET = "user/avatar/set.json";
		public static final String USERSET = "user/profile/set.json";
		public static final String USERBGSET = "user/profile/set/bg.json";
		public static final String QQLOGIN = "user/login/qq.json";
		public static final String BBREGISTER = "user/reg.json";
		public static final String BBLOGIN = "user/login.json";
		public static final String TAOBAOLOGIN = "user/login/taobao.json";
		public static final String CHECKIN = "coin/daily/fanCheckin.json";
		
		public static final String SHAREARTICLE = "coin/daily/share.json";
		public static final String SINAWEIBOLOGIN = "user/login/weibo.json";
		public static final String SENDQUESTION = "question/create.json";
		public static final String SENDANSWER = "answer/create.json";
		public static final String SENDTHANKS = "answer/appreciation/add.json";
		public static final String SENDADOPT = "answer/accept.json";
		public static final String SENDMAIL = "user/password/sendemail.json";
		public static final String SENDLETTER = "follow/mail/compose.json";
		public static final String GETQUESTIONS = "question/list.json";
		public static final String GETARTICES = "article/list.json";
		public static final String GETVIEWPICTURES = "viewPicture/list.json";
		public static final String GETTODAYARTICE = "article/today/detail_1.json";
		public static final String GETDAYARTICE = "article/today/detail_2.json";
		public static final String GETBOARDS = "board/list.json";
		public static final String GETFAVARTICES = "article/fav/list.json";
		public static final String GETFOLLOWS = "follow/list.json";
		public static final String GETFOLLOWCOUNT = "follow/count.json";
		public static final String GETFANS = "follow/fans/list.json";
		public static final String GETLETTERS = "follow/list2.json";
		public static final String GETMAILS = "follow/mail/list.json";
		public static final String GETUSERTIPS = "tips/tipsGroup/list.json";
		public static final String GETALlTIPS = "tips/tipsGroup/listAll.json";
		public static final String GETSHARE = "user/share/url/get.json";
		
		public static final String GETRECOMMENDQUESTIONS = "question/list/recommended.json";
		public static final String UPLOADATTACHMENT = "attachment/upload.json";
		public static final String GETUSERRANK = "user/list.json";
		public static final String GETUSERYESTERDAYRANK = "user/list/yesterday.json";
		public static final String GETANSWERS = "answer/list.json";
		public static final String GETRACEQUESTIONS = "question/list/race.json";
		public static final String GETHOTQUESTIONS = "question/reward/list.json";
		public static final String GETONEQUESTION = "question/list/newest.json";
		// public static final String GETHOTQUESTIONS =
		// "question/hot/list.json";
		public static final String GETQUESTIONDETAIL = "question/detail.json";
		public static final String GETGIFTS = "gift/list.json";
        public static final String GETHUAFEILIST = "gift/mobile.json";

        public static final String GETAPPS = "app/list.json";
		public static final String GETHOTGIFTS = "gift/hotGift/list.json";
		public static final String EXCHANGEGIFTS = "gift/order/add.json";
		public static final String GETMYQUESTIONS = "question/list/mine.json";
		public static final String GETMYTOPICS = "topic/list/his.json";
		public static final String GETMPTOPICS = "topic/mp/list.json";
		public static final String GETMYTOPICREPLYS = "post/list/mypost.json";
		public static final String GETMYRECEIVETOPICREPLYS = "post/list/mine.json";
		public static final String GETFAVQUESTIONS = "question/fav/list.json";
		public static final String GETFAVTOPICS = "topic/fav/list.json";
		public static final String FAVQUESTION = "question/fav/add.json";
		public static final String FAVARTICLE = "article/fav/add.json";
		public static final String FAVTOPIC = "topic/fav/add.json";
		public static final String CANCELFAVARTICLE = "article/fav/del.json";
		public static final String CANCELFAVTOPIC = "topic/fav/del.json";
		public static final String READACTICLE = "article/status/read.json";
		public static final String REPORT = "user/report.json";
		public static final String CANCELFAVQUESTION = "question/fav/del.json";
		public static final String GETCOUNSELLORS = "user/counsellor/list.json";
		public static final String GETOTHERQUESTIONS = "question/list/his.json";
		public static final String GETOTHENOBESTRQUESTIONS = "question/list/his/unaccepted.json";
		public static final String GETMYRECEIVEREPLY = "answer/list/mine.json";
		public static final String GETMYBESTANSWER = "answer/list/mypost/best.json";
		public static final String GETOTHERBESTANSWER = "answer/list/his/best.json";
		public static final String GETMYANSWERS = "answer/list/mypost.json";
		public static final String GETOTHERANSWERS = "answer/list/post/his.json";
		public static final String GETMYRECEIVETHANKS = "answer/appreciation/list.json";
		public static final String GETOTHERRECEIVETHANKS = "answer/appreciation/list/his.json";
		public static final String GETUSERINFO = "user/profile/view.json";
		public static final String GETGUESTUSERINFO = "guest/reg.json";
		public static final String GETUSERLASTWEEKRANK = "user/view/lastweek.json";
		public static final String GETXMPPUSER = "user/xmpp.json";
		public static final String DELETEQUESTION = "question/delete.json";
		public static final String DELETEANSWER = "answer/delete.json";
		public static final String GETNEWESTCOUNT = "answer/util/count.json";
		public static final String GETMESSAGELIST = "message/list.json";
		public static final String GETRECEIVERADDRESS = "receiverAddress/list/get.json";
		public static final String SETRECEIVERADDRESS = "receiverAddress/address/set.json";
		public static final String GETINVITECOUNT = "coin/invite/count.json";
		public static final String GETINVITEUID = "user/inviteUid/get.json";
		public static final String GETINVITEUSERCOUNT = "user/invite/total.json";
		
		public static final String GETTOPICSBYREPLY = "topic/list.json";
		public static final String GETTOPICSBYSEND = "topic/list2.json";
		public static final String GETTOPICSBYMARROW = "topic/list3.json";
		public static final String DELETEFOLLOW = "follow/delete.json";
		public static final String CREATEFOLLOW = "follow/create.json";
		public static final String CREATEPOSTTOPIC = "topic/create.json";
		public static final String CREATEPOSTREPLY = "post/create.json";
		public static final String GETTOPICREPLY = "post/list.json";
		public static final String GETLZTOPICREPLY = "post/list/author.json";
		public static final String GETTOPICDETAIL = "topic/detail.json";
		public static final String LOGACTIVE = "logActive/write.json";
		public static final String GETTOPICSBYTIPS = "tips/tipsKey/get.json";
		public static final String GETUSERADOPTCOUNT = "user/util/count.json";
		public static final String DELETELETTER = "follow/mail/delete.json";
		public static final String DELETEUSERALLLETTER = "/follow/mail/delete/f.json";
		public static final String SHIELDUSER = "follow/blacklist/add.json";
		public static final String CANCELSHIELDUSER = "follow/blacklist/delete.json";
		public static final String GETSHIELDUSERS = "follow/blacklist/list.json";
		public static final String GETCONFIG = "admin/sys/config.json";
		public static final String GETONLINEUSERCOUNT = "admin/sys/online.json";
		public static final String APPAWARD = "app/appAward.json";
		public static final String PHOTOAUTO = "authcode/send.json";
		public static final String VERIFYPHOTO = "authcode/verify.json";
		public static final String PHOTOREGISTE = "user/phone/reg.json";
		public static final String PHOTOCHANGEPW = "user/phone/password.json";
		public static final String GETSUBJECTS = "subject/list.json";
		public static final String GETPORTALS = "portal/list.json";
		public static final String GETSUBJECTDETAIL = "subject/detail.json";
		public static final String GETFEEDS = "feed/list.json";
		public static final String GETMEDALS = "medal/user/list.json";
		public static final String GETATTENTIONUSERS = "follow/uid/list.json";
		public static final String GETSTARS = "user/medalCountTop/list.json";
		public static final String GETNEWSTARS = "user/medalNewTop/list.json";
		public static final String GETUSERLEVEL = "user/level.json";
		public static final String CREATEFEELING = "feelings/create.json";
		public static final String GETFEELINGLIST = "feelings/list.json";
		public static final String GETTALENTS = "talent/list.json";
		public static final String GETMAXSEQID = "question/board/lastSeqId.json";
		public static final String GETBOARDQUESTIONS = "question/list/board.json";
		public static final String GETBOARDDETAIL = "board/detail.json";
		public static final String BINDPHONE = "user/pay/phone/bind.json";
		public static final String BINDPHONESENDCODE = "authcode/pay/phone/sendcode.json";
		public static final String BINDPAY = "user/pay/account/bind.json";
		public static final String BINDPAYSENDCODE = "authcode/pay/account/sendcode.json";
		public static final String EXCHANGESENDCODE = "authcode/ycoin/pay/apply.json";
		public static final String GETUSERBINDINFO = "user/pay/get.json";
		public static final String GETORDERS = "taeorder/list.json";
		public static final String EXCHANGEMONGY = "ycoin/apply.json";
		public static final String GETAPPLYLIST = "ycoin/applyList.json";
		public static final String GETAPPLYLIST1 = "ycoin/applyList1.json";
		public static final String GETWELCOMEPICTURE = "admin/welcomePicture/get.json";
		public static final String GETADS = "notice/list.json";
		public static final String GETRANK = "user/tongji/allYcoinsList.json";
		public static final String GETCHECKININFO = "coin/daily/fanCheckinInfo.json";
		public static final String GETHOTWORDS = "hotWords/list.json";
		public static final String JIFENBAOHOWTOUSE = "/doc/jifenbao.html";
		public static final String HELPCENTER = "/doc/zhequ/help.html";
		public static final String DOC_COIN = "/doc/zhequ/coin.html";
		public static final String GETDIGS = "dig/list.json";
		public static final String SHAREORDER = "coin/daily/shareOrder.json";
		public static final String GETMIAOSHAPICTURES = "viewPicture/list.json";
		
		
		
		
		
		public static final String QQGETUSER = "5222";
		public static final String SINAWEIBOGETUSER = "5222";
		public static final String GOLD = "/index.php?m=Mobile&a=gold";
		public static final String TREATY = "/index.php?m=Mobile&a=treaty";
		public static final String COUNSELLOR = "/index.php?m=Mobile&a=counsellor";
		public static final String LEVEL = "/index.php?m=Mobile&a=level";
		public static final String TOOLS = "/tools/";
		public static final String REGISTER = "/invite/";
		public static final String default_xmpp_port = "5222";
		public static final String CHECKVERSIONURL = "/fanliVersion.json";
		public static final String ASSETS_PATH = "file:///android_asset/";
	}

	public static class SharedPreference
	{

		public final static String sharedpreference_name = "baobaowenda";
		public final static String KEY_LOGINTYPE = "logintype";
		public final static String KEY_USERJSON = "userjson";
		public final static String KEY_XMPPUSERJSON = "xmppuserjson";
		public final static String KEY_XIAOMIJSON = "xiaomijson";
		public final static String KEY_WEATHERJSON = "weatherJson";
		public final static String KEY_TODAYMOOD = "todayMood";
		public final static String KEY_MAINBG = "mainbg";
		public final static String KEY_LASTREFRESHTIME = "LastRefreshTime";
		public final static String KEY_BINDPHONE = "bindphone";
		public final static String KEY_BINDPAY = "bindpay";
		public final static String KEY_ONLINEUSERTIME = "onlineusertime";
		public final static String KEY_ONLINEUSERCOUNT = "onlineusercount";
		public final static String KEY_ONLINEUSERCOUNTRATIO = "onlineusercountRatio";
		public final static String KEY_CHECKINTIME = "CheckInTime";
		public final static String KEY_STARTUPSUCCESSTIME = "StartUpSuccessTime";
		public final static String KEY_BECOMEUSERSUCCESSTIME = "BecomeUserSuccessTime";
		public final static String KEY_RECEIVEADDRESS = "ReceiveAddress";
		public final static String KEY_SHAREARTICLETIME = "ShareArticleTime";
		public final static String KEY_SHARETIME = "shareTime";
		public final static String KEY_CHECKVERSIONTIME = "checkVersionTime";
		public final static String KEY_ISGETTINGMORENEW = "isGettingMoreNew";
		public final static String KEY_ISGETTINGMOREOLD = "isGettingMoreOld";
		public final static String KEY_ASKSHARE = "askshare";
		public final static String KEY_INVITEUID = "inviteUid";
		public final static String KEY_HADNEWREPLY = "hadnewreply";
		public final static String KEY_HADNEWAPPRECIATION = "hadnewappreciation";
		public final static String KEY_ANSWERSHARE = "answershare";
		public final static String KEY_THANKSHARE = "thankshare";
		public final static String KEY_BESTANSWERSHARE = "bestanswershare";
		public final static String KEY_PUSHSHARE = "pushshare";
		public final static String KEY_ISDOWNLOADAPK = "isdownloadapk";
		public final static String KEY_NOTNOTICEAGAIN = "notnoticeagain";
		public final static String KEY_GOLDUSEGUIDE = "guideuseguide";
		public final static String KEY_FIRSTGUIDE = "firstguide";
		public final static String KEY_SECONDGUIDE = "secondguide";
		public final static String KEY_ISREGISTER = "isregister";
		public final static String KEY_ISTIPORNOTFESS = "istipwhenisguest";
        public final static String KEY_ISGOSHOPTIPORNOTFESS = "istipwhengotoshopcar";

        public final static String KEY_SHORTCUT = "Shortcut";
		public final static String KEY_ISMAINACTIVITYALIVE = "ismainactivityalive";
		public final static String KEY_NEWREPLYCOUNT = "newreplycount";
		public final static String KEY_SHARECOUNT = "shareCount";
		public final static String KEY_INVITECOUNT = "inviteCount";
		public final static String KEY_LASTBORADPOSITION = "lastBoardPosition";
		public final static String KEY_REWARDLEVEL = "rewardLevel";
		public final static String KEY_NEWAPPRECIATIONCOUNT = "newappreciationcount";
		public final static String KEY_NEWQUESTIONCOUNT = "newquestioncount";
		public final static String KEY_NEWPOSTCOUNT = "newpostcount";
		public final static String KEY_NEWMAILCOUNT = "newmailcount";
		public final static String KEY_NEWFEEDCOUNT = "newfeedcount";
		public final static String KEY_BESTANSWERCOUNT = "bestanswercount";
		public final static String KEY_SYSTEMMSGCOUNT = "systemmsgcount";
//		最新返利数：fanliCount
//		最新提现数：cashCount
		public final static String KEY_FANLICOUNT = "fanliCount";
		public final static String KEY_CASHCOUNT = "cashCount";
		
		
		public final static String KEY_QUESTIONMAXSEQID = "questionMaxSeqId";
		public final static String KEY_ARTICLEMAXSEQID = "articleMaxSeqId";
		public final static String KEY_LOGINTIME = "logintime";
		public final static String KEY_QQBOUND = "qqbound";
		public final static String KEY_WEIXINBOUND = "weixinbound";
		public final static String KEY_POSTMAXSEQID = "postMaxSeqId";
		public final static String KEY_LASTPRIVATEVER = "lastPrivateVer";
		// mailMaxSeqId
		public final static String KEY_MAILMAXSEQID = "mailMaxSeqId";
		public final static String KEY_FEEDMAXSEQID = "feedMaxSeqId";
		public final static String KEY_TOPICDETAILPREFIX = "topicDetailPrefix";
		public final static String KEY_REGIVETIPS = "RegGiveTips";
		public final static String KEY_ASKDETAILPREFIX = "askDetailPrefix";
		public final static String KEY_JFQSWITCH = "jfqSwitch";
		public final static String KEY_STARTIVURL = "startIvUrl";
//		fanliInviteUserList
		public final static String KEY_FANLIINVITEUSERLIST = "fanliInviteUserList";
//		fanliProfileApp
		public final static String KEY_FANLIPROFILEAPP = "fanliProfileApp";
		
		public final static String KEY_STARTIVLOCALPATH = "startIvLocalpath";
		/**
		 * 商城的域名头部
		 */
		public final static String KEY_SHOPPREFIX = "shopPrefix";
		public final static String KEY_SCAN_CHECK_PREFIX = "scan_check_Prefix";
        public final static String KEY_FETCH_CHECK_PREFIX = "fetch_scan_check_Prefix";
        public final static String KEY_SEARCH_RESULT_WITH_CHECK_PREFIX = "get_search_result_with_primary_url_check_Prefix";
        public final static String KEY_GET_PRIMARY_WITH_WORDS_PREFIX = "get_primary_words_with_url_check_Prefix";

        public final static String KEY_SEARCHWORDS = "searchWords";
		public final static String KEY_ISBINDTAOBAO = "is bind taobao";

        public final static String KEY_MAINBGINDEX= "MAIN FRAGMENT BG INDEX";
        public final static String KEY_MAINNOTIFYMESSAGE= "MAIN NOTIFY MESSAGE";

        public final static String KEY_QIANDAOMESSAGE= "QIANDAO MESSAGE";

    }

	public static class AppID
	{

		public final static int DIANYUN_CHAIFENID = 3;
	}

	public static class MessageWhat
	{

		public final static int REFRESHCONFINGSUCCESS = 1;
		public final static int REFRESHCONFINGFAILED = 2;
	}

	public static class BindMode
	{

		public static final int EXCHANGEMONEY = 1;
		public static final int EXCHANGEGOLD = 2;
	}

	public static class AccountType
	{

    	
//    	账号类型必填 * 
//    	0:none ; 1:支付宝 ; 2:财富通 ;3:贝宝
    	
        public final static byte NONE = 0;
        public final static byte ALIPAY = 1;
        public final static byte CAIFUTONG = 2;
        public final static byte BEIBAO = 3;
	}

	public static class QuestionMedal
	{

		// 1.青铜顾问
		// 2.白银顾问
		// 3.黄金顾问
		// 4.铂金顾问
		// 5.钻石顾问
		// 6. 情感达人
		// 7.分享达人
		// 8.时尚达人
		// 9.话题达人
		// 10.爱购达人
		public final static int QT = 1;
		public final static int BY = 2;
		public final static int HJ = 3;
		public final static int BJ = 4;
		public final static int ZS = 5;
	}

	public static class NewestToastType
	{

		public final static int RECEIVEPOST = 1;
		public final static int RECEIVEREPLY = 2;
		public final static int ADOPT = 3;
		public final static int APPRECIATION = 4;
		public final static int LETTER = 5;
		public final static int SYSTEMMSG = 6;
	}

	public static class FromType
	{

		public final static int NORMAL = 0;
		public final static int BOARD_SUBJECT = 1;
	}

	public static class MainbgName
	{

		public final static String ONE = "1";
		public final static String TWO = "2";
		public final static String THREE = "3";
		public final static String FOUR = "4";
	}

	public static class TopicOrderType
	{

		public final static int REPLY = 1;
		public final static int SEND = 2;
		public final static int MARROW = 3;
	}

	public static class TopicShowStatus
	{

		public final static byte mainpageshow = 1;
		public final static byte boardshow = 2;
		public final static byte normalshow = 0;
	}

	public static class LoginType
	{

		public final static int NOTLOGIN = 0;
		public final static int QQ = 1;
		public final static int SINAWEIBO = 2;
		public final static int PHONE = 4;
		public final static int BB = 3;
		public final static int TAE = 5;
	}

	public static class MainKind
	{

		public final static int BOARD = 0;
		public final static int RECOMMEND = 1;
		public final static int NEWS = 2;
	}

	// 1：备孕prepare； 2：怀孕 pregnancy； 3：育儿
	public static class DayArticleStatus
	{

		public final static int PREPARE = 1;
		public final static int PREGNANCY = 2;
		public final static int HASCHILD = 3;
	}

	/*
	 * public static class FollowStatus{ public final static byte NO = 0; public
	 * final static byte YES = 1;
	 * 
	 * }
	 */
	public static class TopicActionType
	{

		public final static byte GETNEW = 0;
		public final static byte GETOLD = 1;
	}

	/*
	 * public static class TopicMainPageShowStatus{ public final static byte NO
	 * = 0; public final static byte YES= 1;
	 * 
	 * 
	 * 
	 * 
	 * } public static class TopicStatusYesOrNO{ public final static byte NO =
	 * 0; public final static byte YES= 1;
	 * 
	 * }
	 */
	public static class ArticleDayType
	{

		public final static int NEWEST = 0;
		public final static int PREGNANT = 1;
		public final static int PERINATAL = 2;
		public final static int BBBIRTHDAY = 3;
	}

	public static class MainPartId
	{

		// 1 问答广场
		// 2 情感驿站
		// 3 育儿分享
		// 4 时尚辣妈
		// 5 美图晒晒
		// 6 谈天说地
		// 7 母婴用品
		// 8 建议反馈
		// 10 淘宝商家
		// 11 儿歌听听
		// 12 美食厨房
		// 16同城
		// 17 同龄
		public final static long MAINPART_QUESTIONID = 1;
		public final static long MAINPART_EMOTIONALSKYID = 2;
		public final static long MAINPART_EXPERIENCEID = 3;
		public final static long MAINPART_FASHIONMOTHERID = 4;
		public final static long MAINPART_PHOTOSHOWID = 5;
		public final static long MAINPART_TALK = 6;
		public final static long MAINPART_COMMODITYID = 7;
		public final static long MAINPART_ADVISEID = 8;
		public final static long MAINPART_TAOBAOID = 10;
		public final static long MAINPART_MUSICID = 11;
		public final static long MAINPART_FOOD = 12;
		public final static long MAINPART_SAMECITY = 16;
		public final static long MAINPART_SAMEAGE = 17;
	}

	public static class BoardMode
	{

		public final static byte SUBSCRIBE = 0;
		public final static byte RANK = 1;
	}

	public static class RefreshType
	{

		// // 刷新user信息
		public final static byte USER = 1;
		//
		// 登录user成功（切换账号）
		public final static byte CHANGEUSER = 23;
		// 退出登录
		public final static byte LOGOUT = 20;
		// // 个人中心更新user
		// public final static byte UPDATEUSER = 29;
		// 个人中心更新user （名字 身份 金币数）
		// public final static byte UPDATEUSER_NORMAL = 31;
		//
		// // 地址
		// public final static byte UPDATEUSER_CITY = 32;
		// // 状态
		// public final static byte UPDATEUSER_STATUS= 33;
		//
		// //背景图
		// public final static byte UPDATEUSER_BG= 34;
		//
		// //头像
		// public final static byte UPDATEUSER_AVATAR= 35;
		public final static byte USERTHANKS = 5;
		public final static byte PROFILE = 6;
		public final static byte PROFILETHANKS = 7;
		public final static byte QUESTION = 2;
		// public final static byte NEWREPLY = 3;
		public final static byte UPDATEVERSION = 4;
		public final static byte ALLNEW = 8;
		// public final static byte NEWAPPRECIATION = 9;
		public final static byte SENDNEWQUESTION = 10;
		public final static byte SENDNEWPOSTTOPIC = 14;
		public final static byte TOPICFAV = 16;
		public final static byte SENDNEWPOSTREPLY = 15;
		public final static byte MSGCENTER = 11;
		public final static byte TASKSTATUS = 12;
		public final static byte BOARD = 13;
		// public final static byte NEWESTTOAST = 17;
		public final static byte RECEIVELETTER = 18;
		public final static byte QUESTIONFAV = 19;
		public final static byte FIRSTREGISTEROVER = 21;
		public final static byte SENDREPLYREPLY = 22;
		public final static byte REFRESHTASKSTATUS = 24;
		public final static byte CHAGNEUSERGOLD = 25;
		public final static byte WEATHER = 26;
		public final static byte MOOD = 27;
		public final static byte PORTAL = 28;
		public final static byte CHANGEMAXSEQID = 30;
		public final static byte SHAREORDERSUCCESS = 31;
		public final static byte ADDUSERGOLD = 32;
		public final static byte REGIVETIPS   = 33;
		public final static byte WXLOGINSUCCESS   = 100;
		
		
		
		// public final static byte INSTALLSUCCESSORFAIL = 25;
	}

	public static class QuestionType
	{

		public final static int NEWEST = 1;
		public final static int RECOMMEND = 2;
		public final static int RACE = 3;
		public final static int MYQUESTION = 4;
		public final static int HOT = 5;
	}

	public static class PullType
	{

		public final static int NEWEST = 1;
		public final static int SAMEAGE = 2;
		public final static int RACE = 3;
		public final static int OTHERREPLYME = 4;
		public final static int THANKS = 5;
	}

	public static class PullUpType
	{

		public final static int MYQUESTIONS = 1;
		public final static int QUESTIONDETAIL = 2;
		public final static int MYANSWER = 3;
	}

	public static class Gender
	{

		public final static byte FATHER = 1;
		public final static byte MOTHER = 2;
	}

	public static class AllStatusYesOrNo
	{

		public final static byte YES = 1;
		public final static byte NO = 0;
	}

	/**
	 * 类型 1：专题 2：帖子
	 */
	public static class PortalType
	{

		public final static int SUBJECT = 1;
		public final static int TOPIC = 2;
	}

	public static class UserType
	{

		public final static byte NO = 0;
		public final static byte ISSELF = 1;
	}

	public static class TopicTopStatus
	{

		public final static byte NO = 0;
		public final static byte BOARD = 1;
		public final static byte MAINPAGE = 2;
	}

	public static class FollowStatus
	{

		public final static byte NOATTENTION = 0;
		public final static byte ATTENTION = 1;
		public final static byte ATTENTIONEACHOTHER = 2;
	}

	public static class XiaoMiPushDataType
	{

		public final static int ALIAS = 1;
		public final static int DEFAULT_TOPIC = 2;
		public final static int TOPIC = 3;
	}

	public static class PushDataStatus
	{

		public final static int NORMAL = 0;
		public final static int FAILED = 1;
		public final static int SUCCESS = 2;
	}

	public static class Status
	{

		public final static byte PREPARE = 0;
		public final static byte PERINATAL = 1;
		public final static byte BBBIRTHDAY = 2;
	}

	public static class FAVORCANCEL
	{

		public final static byte FAV = 1;
		public final static byte CANCEL = 0;
	}

	public static class Data
	{

		public final static String RECEIVELETTER = "ReceiveLetter";
		public final static String QUESTION = "question";
		public final static String ANSWER = "answer";
		public final static String QUESTIONID = "questionId";
		public final static String USER = "user";
		public final static String USERSTATUS = "userStatus";
		public final static String ARTICLE = "article";
		public final static String URL = "url";
		public final static String INVITEFRIENDURL = "invitefriendurl";
		public final static String TITLE = "title";
		public final static String SHAREIMAGEURL = "shareImageUrl";
		public final static String REFRESHTYPE = "refreshType";
		public final static String ADDGOLD_COUNT = "addgold_count";
		public final static String ARTICLEID = "articleId";
		public final static String TOPICID = "topicId";
		public final static String SUBJECTID = "subjectId";
		public final static String 	SEQID = "seqId";
		public final static String SUBJECT = "subject";
		public final static String FAVARTICLE = "article";
		public final static String MESSAGETYPE = "MessageType";
		public final static String MYQUESTIONTYPE = "MyQuestionType";
		public final static String IMGFILEPATH = "ImgFilePath";
		public final static String FROM = "from";
		public final static String GIFTLIST = "giftlist";
		public final static String TOPIC = "topic";
		public final static String TOPICREPLY = "topicReply";
		public final static String POST = "post";
		public final static String MAIL = "mail";
		public final static String FOLLOWER = "follower";
		public final static String KEYID = "keyId";
		public final static String LV = "Lv.";
		// 知识跳转
		public final static String NOTIFY = "notify";
		public final static String ACTIVITY = "activity";
		public final static String MAINPARTID = "MainPartId";
		public final static String BOARD = "Board";
		public final static String BOARDID = "BoardId";
		public final static String ATTACHMENTLIST = "attachmentlist";
		public final static String SELECTEDPOSITION = "selectedposition";
		public final static String GIFTID = "giftId";
		public final static String APPID = "appId";
		public final static String AWARDSUCCESSORFAILED = "awardsuccessorfailed";
		public final static String PHONENUMBER = "phoneNumber";
		public final static String PHONEOPERATE = "phoneOperate";
		public final static String PHONECHANGEPW = "phoneChangePwForget";
		public final static String PHONEJUSTCHANGEPW = "phoneChangePw";
		public final static String WEATHERCODE = "weatherCode";
		public final static String MOODCODE = "moodCode";
		public final static String CITY = "city";
		public final static String BBSTATUS = "bbStatus";
		public final static String BBBIRTHDAY = "bbbirthday";
		public final static String MAXSEQID = "maxSeqId";
		public final static String BINDMODE = "BindMode";
		// public final static String APPAWARDGOLD="appAwardGold";
	}

	public static class RequestCode
	{

		public static final int CHOOSE_CAPTURE = 3;
		public static final int CHOOSE_PICTURE = 4;
		public static final int CAPTURE_CUT = 5;
	}

	public static class IntentFilterAction
	{

		public static final String CANCELFAVARTICLE = "cancelfavarticle";
		public static final String REFRESHPROFILE = "refreshprofile";
		public static final String REFRESH = "refresh";
		public static final String REFRESHTASKSTATUS = "refreshTaskStatus";
		public static final String CANCELFAVQUESTION = "cancelfavquestion";
		public static final String REFRESHAPPAWARD = "refreshAppAward";
		public static final String MUSICOPERATE = "musicoperate";
	}

	public static class Suffix
	{

		/** 文件后缀 */
		public static final String PIC_SUFFIX_JPG = ".jpg";
		/** 文件后缀 */
		public static final String PIC_SUFFIX_PNG = ".png";
		public static final String PIC_SUFFIX_APK = ".apk";
	}

	/*
	 * public static class MessageReceiverType{ //在线收到消息 public static final int
	 * ONLINE = 0; //手动离线取 Manual public static final int MANUALOFFLINE = 1;
	 * //自动离线取Automatic public static final int AUTOMATICOFFLINE = 5; //第一次登陆默认取
	 * public static final int FIRSTDEFAULTGET = 2; //手动获取更多房间 public static
	 * final int GETMOREROOM = 3; //手动获取房间里更多消息 public static final int
	 * GETMOREMESSAGE = 4; }
	 */
	public static class PullRefreshType
	{

		public static final int DOWN = 1;
		// 自动离线取Automatic
		public static final int UP = 2;
	}

	public static class MessageType
	{

		public final static int APPRECIATION = 1;
		public final static int REPLY = 2;
		public final static int GOLDCHANGE = 3;
		public final static int REPLYTOREPLY = 4;
		public final static int ADOPT = 5;
		public final static int SYSTEMMSG_NORMAL = 120;
		public final static int SYSTEMMSG_FANLI = 121;
		public final static int SYSTEMMSG_TIXIAN = 122;
		public final static int SYSTEMMSG_EXCHANGE = 123;
		public final static int SYSTEMMSG_GOODS = 20;
		public final static int SYSTEMMSG_ACTIVITY = 21;
        public final static int SYSTEMMSG_XIADAN = 22;
        public final static int SYSTEMMSG_QIANDAO = 124;
        public final static int SYSTEMMSG_XIAJIA = 125;


        // 120 系统通知
		// 121 返利成功通知
		// 122 提现成功通知
		// 123 兑换成成功
		// public final static int WARM = 7;
		// public final static int DELETEQUESTION = 8;
		public final static int WARMADOPT = 101;
		public final static int ARTICLE = 102;
		public final static int TOPICREPLY = 103;
		public final static int LETTER = 104;
		public final static int BACKTOPIC = 105;
	}

	public static class MessageCenterType
	{

		public final static int APPRECIATION = 1;
		public final static int REPLY = 2;
		public final static int BEST = 3;
		public final static int SYSTEM = 4;
		public final static int LETTER = 5;
		public final static int POST = 6;
	}

	public static class MyAnswersType
	{

		public final static int ALL = 1;
		public final static int BEST = 2;
	}

	public static class MyQuestionsType
	{

		public final static int ALL = 1;
		public final static int NOBESTANSWER = 2;
	}
	public static final long GUESTUSERUID = 1000L;
	public static final String GUESTUSERTOKEN = "a5e313ac2df3f6a42626d3b34c62669c";

	/*
	 * public static final String CLOCKTYPE = "clockType"; public static final
	 * int heartAlarmID = 10000000; public static class ClockType { public
	 * static final int NORMAL = 1; public static final int HEART = 2; } public
	 * static class Action { public static final String SERVICE_ALARM =
	 * "im.todo.service.alarm"; public static final String ACTION_TIME_SET =
	 * "android.intent.action.TIME_SET"; }
	 */
	public static class Version
	{

		public final static String URL = "url";
		public final static String NOTE = "note";
		public final static String LASTPRIVATEVER = "lastPrivateVer";
	}
}
