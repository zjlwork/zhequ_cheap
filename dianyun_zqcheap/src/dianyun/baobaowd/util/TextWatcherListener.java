package dianyun.baobaowd.util;

import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;

public class TextWatcherListener implements TextWatcher
{

	boolean notNeed;
	int mCount;
	EditText mContentEditText;

	public TextWatcherListener(boolean notNeed, int mCount,
			EditText mContentEditText)
	{
		this.notNeed = notNeed;
		this.mCount = mCount;
		this.mContentEditText = mContentEditText;
	}

	@Override
	public void onTextChanged(CharSequence s, int start, int before, int count)
	{
	}

	@Override
	public void beforeTextChanged(CharSequence s, int start, int count,
			int after)
	{
		if (!notNeed)
		{
			int oldLength = s.length();
			if (oldLength + after >= mCount)
			{
				notNeed = true;
				mContentEditText.setText(mContentEditText.getText().toString());
				mContentEditText.setSelection(mContentEditText.getText()
						.toString().length());
			}
		}
		else
		{
			notNeed = false;
		}
	}

	@Override
	public void afterTextChanged(Editable s)
	{
	}
}
