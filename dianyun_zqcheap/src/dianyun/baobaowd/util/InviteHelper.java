
package dianyun.baobaowd.util;

import android.text.TextUtils;
import dianyun.baobaowd.data.User;
import dianyun.baobaowd.db.LightDBHelper;
import dianyun.baobaowd.dto.ResultDTO;
import dianyun.baobaowd.serverinterface.GetInviteUid;
import dianyun.zqcheap.application.BaoBaoWDApplication;

public class InviteHelper {

	public static void getInviteUid() {
		
		 long inviteUid = LightDBHelper.getInviteUid(BaoBaoWDApplication.context);
         if(inviteUid==-1){
        	 new Thread() {
     			public void run() {
     				try {
     					User mUser = UserHelper.getUser();
     					ResultDTO lResultDTO = new GetInviteUid(mUser.getUid(),
     							mUser.getToken()).getConnect();
     					if (lResultDTO != null && lResultDTO.getCode().equals("0")) {
     						
     						if(!TextUtils.isEmpty(lResultDTO.getResult()))
     							LightDBHelper.setInviteUid(BaoBaoWDApplication.context,
     								Long.parseLong(lResultDTO.getResult()));
     						else
     							LightDBHelper.setInviteUid(BaoBaoWDApplication.context,
         								0L);
     					}
     				} catch (Exception e) {
     				}

     			}
     		}.start();
         }
	}

}
