
package dianyun.baobaowd.util;

import java.util.Date;

import android.content.Context;
import android.text.TextUtils;
import dianyun.baobaowd.db.LightDBHelper;

/**
 * 联系人数据库操作辅助
 * 
 */
public class ShareHelper
{

	public static boolean isAllowShare(Context context)
	{
		try
		{
			long nowTimeLong = new Date().getTime();
			String preTime = LightDBHelper.getShareTime(context);
			int shareCount = LightDBHelper.getShareCount(context);
			if (preTime == null || preTime.equals(""))
			{
				LightDBHelper
						.setShareTime(String.valueOf(nowTimeLong), context);
				LightDBHelper.setShareCount(context, 1);
				return true;
			}
			else
			{
				long preTimeLong = Long.parseLong(preTime);
				if (Math.abs(nowTimeLong - preTimeLong) < 7L * 24L * 60L * 60L
						* 1000)
				{
					if (shareCount < 1)
					{
						LightDBHelper.setShareCount(context, ++shareCount);
						return true;
					}
					else
					{
						return false;
					}
				}
				else
				{
					LightDBHelper.setShareTime(String.valueOf(nowTimeLong),
							context);
					LightDBHelper.setShareCount(context, 1);
					return true;
				}
			}
		}
		catch (Exception e)
		{
			LightDBHelper.setShareTime("", context);
			return true;
		}
	}

	public static boolean isAllowCheckVersion(Context context)
	{
		try
		{
			String preTime = LightDBHelper.getCheckVersionTime(context);
			String nowTime = DateHelper.getTextByDate(new Date(),
					DateHelper.YYYY_MM_DD);
			if (TextUtils.isEmpty(preTime)||!preTime.equals(nowTime))
			{
				return true;
			}
			
		}
		catch (Exception e)
		{
			LightDBHelper.setShareTime("", context);
			return false;
		}
		return false;
	}
	
//	
//	public static void toShareWeixin(Context context ,String title, String summaryTxt, String url,
//			boolean toSpace)
//	{
//		if(title==null||summaryTxt==null||url==null)return ;
//		Intent lIntent = new Intent (context,WXEntryActivity.class);
//		lIntent.putExtra("title", title);
//		lIntent.putExtra("summaryTxt", summaryTxt);
//		lIntent.putExtra("url", url);
//		lIntent.putExtra("toSpace", toSpace);
//		context.startActivity(lIntent);
//		
//	}
	
	
	
}
