package dianyun.baobaowd.util;

import java.util.Map;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.View;

import com.alibaba.cchannel.CloudChannelConstants;
import com.alibaba.cchannel.core.task.RunnableCallback;
import com.alibaba.cchannel.core.task.RunnableCallbackAdapter;
import com.alibaba.cchannel.plugin.CloudPushService;
import com.alibaba.sdk.android.AlibabaSDK;
import com.alibaba.sdk.android.callback.InitResultCallback;
import com.alibaba.sdk.android.login.LoginService;
import com.alibaba.sdk.android.login.callback.LoginCallback;
import com.alibaba.sdk.android.session.SessionListener;
import com.alibaba.sdk.android.session.model.Session;
import com.alibaba.sdk.android.trade.CartService;
import com.alibaba.sdk.android.trade.ItemService;
import com.alibaba.sdk.android.trade.callback.TradeProcessCallback;
import com.alibaba.sdk.android.trade.model.TaokeParams;
import com.alibaba.sdk.android.trade.model.TradeResult;
import com.taobao.tae.sdk.webview.TaeWebViewUiSettings;

import dianyun.baobaowd.data.User;
import dianyun.baobaowd.db.LightDBHelper;
import dianyun.baobaowd.entity.Constants;
import dianyun.baobaowd.help.ShopHttpHelper;
import dianyun.baobaowd.interfaces.IDialogCallback;
import dianyun.zqcheap.R;
import dianyun.zqcheap.activity.PaySuccessActivity;

public class TaeSdkUtil {
	private final static String TAG = "TaeSdkUtil";
	public final static String ACCOUNTPREFIX = "dy";


	public static void initTaeSdk(final Context context) {
		AlibabaSDK.asyncInit(context, new InitResultCallback() {

			@Override
			public void onFailure(int code, String msg) {
				Log.d(TAG, "init:" + msg + "code:" + code);
			}

			@Override
			public void onSuccess() {
				Log.d(TAG, "success");
				System.out.println(TAG + "success");
				TaeSdkUtil.setSessionStateChanger(context);
//				initCloudChannel(context);
			}
		});
	}

	private static void saveUserAccount(final Context context,
			final String userAccount) {
		SharedPreferences accountStore = context.getSharedPreferences(
				"account", Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = accountStore.edit();
		editor.putString("accountName", userAccount);
		editor.commit();
	}

	private static void removeUserAccount(final Context context) {
		// 删除本地存储的用户名
		SharedPreferences accountStore = context.getSharedPreferences(
				"account", Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = accountStore.edit();
		editor.remove("accountName");
		editor.commit();
	}

	public static void bindAccount(final Context context,
			final long userId) {/*
		final String fullAccount = ACCOUNTPREFIX+GobalConstants.AppID.DIANYUN_CHAIFENID+"_"+userId;
		AlibabaSDK.getService(CloudPushService.class).bindAccount(fullAccount,
				new RunnableCallback<Void>() {
					@Override
					public void onSuccess(Void aVoid) {
						saveUserAccount(context, fullAccount);
						// 本地存储绑定的用户名
						// Toast.makeText(context, "赞! 账号绑定成功 ~",
						// Toast.LENGTH_SHORT).show();
						Log.d(TAG, "@用户绑定账号 ：" + fullAccount + " 成功");
					}

					@Override
					public void onFailed(Exception e) {
						// Toast.makeText(context, "衰! 账号绑定失败 ~",
						// Toast.LENGTH_SHORT).show();
						Log.d(TAG,
								"@用户绑定账号 ：" + fullAccount + " 失败，原因 ： "
										+ e.toString());
					}
				});
	*/}

	public static void unbindAccount(final Context context,
			final String useraccount) {/*
		AlibabaSDK.getService(CloudPushService.class).unbindAccount(
				new RunnableCallback<Void>() {
					@Override
					public void onSuccess(Void aVoid) {
						// Toast.makeText(context, "赞! 账号解绑成功 ~",
						// Toast.LENGTH_SHORT).show();
						Log.d(TAG, "@用户解绑账户 ：" + useraccount + " 成功");
						removeUserAccount(context);
					}

					@Override
					public void onFailed(Exception e) {
						// Toast.makeText(context, "衰! 账号解绑失败 ~",
						// Toast.LENGTH_SHORT).show();
						Log.d(TAG,
								"@用户解绑账户 ：" + useraccount + " 失败，原因 ："
										+ e.toString());
					}
				});
	*/}

	public static void addTag(final Context context, final String userlabel) {
		AlibabaSDK.getService(CloudPushService.class).addTag(userlabel,
				new RunnableCallback<Boolean>() {
					@Override
					public void onSuccess(Boolean aBoolean) {

						// Toast.makeText(context, "赞! 增加标签成功 ~",
						// Toast.LENGTH_SHORT).show();
						Log.d(TAG, "@用户增加标签 ：" + userlabel + " 成功");
					}

					@Override
					public void onFailed(Exception e) {
						// Toast.makeText(context, "衰! 增加标签失败 ~",
						// Toast.LENGTH_SHORT).show();
						Log.d(TAG,
								"@用户增加标签 ：" + userlabel + " 失败，原因："
										+ e.toString());
					}
				});
	}

	public static void removeTag(final Context context, final String userlabel) {
		AlibabaSDK.getService(CloudPushService.class).removeTag(userlabel,
				new RunnableCallback<Boolean>() {
					@Override
					public void onSuccess(Boolean aBoolean) {

						// Toast.makeText(context, "赞! 删除标签成功 ~",
						// Toast.LENGTH_SHORT).show();
						Log.d(TAG, "@用户删除标签 ：" + userlabel + " 成功");
					}

					@Override
					public void onFailed(Exception e) {
						// Toast.makeText(context, "衰! 删除标签失败 ~",
						// Toast.LENGTH_SHORT).show();
						Log.d(TAG,
								"@用户删除标签 ：" + userlabel + " 失败，原因："
										+ e.toString());
					}
				});
	}

	/**
	 * 初始化云推送通道
	 * 
	 * @param applicationContext
	 */
	private static void initCloudChannel(final Context applicationContext) {
		CloudPushService cloudPushService = AlibabaSDK
				.getService(CloudPushService.class);
		if (cloudPushService != null) {
			cloudPushService.register(applicationContext,
					new RunnableCallbackAdapter<Void>() {
						@Override
						public void onSuccess(Void result) {
							Log.d(CloudChannelConstants.TAG,
									"init cloudchannel success");
							if (UserHelper.getUser() != null) {
								TaeSdkUtil.bindAccount(applicationContext,
										 UserHelper.getUser().getUid());
							}
						}

						@Override
						public void onFailed(Exception exception) {
							Log.d(CloudChannelConstants.TAG,
									"init cloudchannel failed");
						}
					});
		} else {
			Log.i(CloudChannelConstants.TAG, "CloudPushService is null");
		}

	}

	public static void setSessionStateChanger(final Context context) {
		LoginService loginService = AlibabaSDK.getService(LoginService.class);
		if (loginService == null) {
			return;
		}
		loginService.setSessionListener(new SessionListener() {

			@Override
			public void onStateChanged(Session session) {
				if (session.isLogin()) {
					final User luser = UserHelper.getUser();
					ShopHttpHelper.bindTaoBaoAndYoYo(context,
							"" + luser.getUid(), "" + luser.getToken(),
							session.getUserId(), session.getUser().avatarUrl,
							session.getUser().nick);
				}
			}
		});
	}

	public static void showLogin(final Activity activity,final TAELoginCallback callback) {
		final User luser = UserHelper.getUser();
		LoginService loginService = AlibabaSDK.getService(LoginService.class);
		if (loginService == null) {
			return;
		}
		loginService.showLogin(activity, new LoginCallback() {

			@Override
			public void onFailure(int arg0, String msg) {
				Log.d(Constants.TAG, msg);
				callback.result(null);
			}

			@Override
			public void onSuccess(Session session) {
				Log.d(Constants.TAG,
						session.getUserId() + "--state:" + session.isLogin()
								+ "---" + session.getLoginTime() + "---"
								+ session.getUser().avatarUrl + "---"
								+ session.getUser().nick);
				
				callback.result(session);
				
//				ShopHttpHelper.bindTaoBaoAndYoYo(
//						activity.getApplicationContext(), "" + luser.getUid(),
//						"" + luser.getToken(), session.getUserId(),
//						session.getUser().avatarUrl, session.getUser().nick);
			}
		});
	}
	
	
	
	
	

	/*public static void showPage(final Activity activity, final String url) {
		ItemService itemService = AlibabaSDK.getService(ItemService.class);
		if (itemService == null) {
			return;
		}
		itemService.showPage(activity, new TradeProcessCallback() {

			@Override
			public void onFailure(int code, String msg) {
				Log.d(Constants.TAG, msg + "---code:" + code);
				if (code == ResultCode.QUERY_ORDER_RESULT_EXCEPTION.code) {
					Toast.makeText(activity.getBaseContext(), "确认交易订单失败",
							Toast.LENGTH_SHORT).show();
				} else {
					Toast.makeText(activity.getBaseContext(), "交易异常:" + code,
							Toast.LENGTH_SHORT).show();
				}
			}

			@Override
			public void onPaySuccess(
					com.alibaba.sdk.android.trade.model.TradeResult tradeResult) {
				Toast.makeText(
						activity.getBaseContext(),
						"支付成功" + tradeResult.paySuccessOrders + "   "
								+ tradeResult.payFailedOrders,
						Toast.LENGTH_SHORT).show();
			}
		}, null, url);
	}*/

	public static void showMjbPage(final Activity activity, final String url) {
		ItemService itemService = AlibabaSDK.getService(ItemService.class);
		if (itemService == null) {
			return;
		}
		itemService.showPage(activity, null, null, url);
	}

	public static void showTAEItemDetail(final Activity activity,
			String itemID, String pid, boolean isTK, int productType,
			Map<String, String> map,View parentView) {
		if (isTK) {
			TaokeParams taokeParams = new TaokeParams();
			taokeParams.pid = pid;
			showTaoBaoKeItemDetail(activity, itemID, productType, taokeParams,
					map,parentView);
		} else {
			showItemDetail(activity, itemID, productType, map,parentView);
		}
//		ToastHelper.showGuideLoginDialog(activity, parentView);
	}

	private static void showItemDetail(final Activity activity, String itemID,
			int productType, Map<String, String> map,final View parentView) {
		ItemService itemService = AlibabaSDK.getService(ItemService.class);
		if (itemService == null) {
			return;
		}
		
		itemService.showItemDetailByOpenItemId(activity,
				new TradeProcessCallback() {

					@Override
					public void onFailure(int arg0, String arg1) {
						System.out.println("onFailure====="+arg1);
						ToastHelper.cancelPopup();
					}

					@Override
					public void onPaySuccess(TradeResult arg0) {
						ToastHelper.show(activity, activity.getResources()
								.getString(R.string.paysuccess));
						LoginService loginService = AlibabaSDK
								.getService(LoginService.class);
						if (loginService == null) {
							return;
						}
						Session session = loginService.getSession();
						if (session != null && session.isLogin()) {
							final User luser = UserHelper.getUser();
							ShopHttpHelper.bindTaoBaoAndYoYo(activity, ""
									+ luser.getUid(), "" + luser.getToken(),
									session.getUserId(),
									session.getUser().avatarUrl,
									session.getUser().nick);
						}
						
						if (UserHelper.isGusetUser(activity)) return;
						Utils.goActivity(activity, PaySuccessActivity.class);

					}

				}, null, itemID, productType, map);
	}

	private static void showTaoBaoKeItemDetail(final Activity activity,
			String itemID, int productType, TaokeParams taokeParams,
			Map<String, String> map,final View parentView) {
		TaeWebViewUiSettings taeWebViewUiSettings = new TaeWebViewUiSettings();
		// TaokeParams taokeParams = new TaokeParams();
		// taokeParams.pid="mm_88888888_6666666_68686868";
		// taokeParams.unionId="test";
		ItemService itemService = AlibabaSDK.getService(ItemService.class);
		if (itemService == null) {
			return;
		}
		itemService.showTaokeItemDetailByOpenItemId(activity,
				new TradeProcessCallback() {

					@Override
					public void onFailure(int arg0, String arg1) {
						System.out.println("onFailure====="+arg1);
						ToastHelper.cancelPopup();
					}

					@Override
					public void onPaySuccess(TradeResult msg) {

						ToastHelper.show(activity, activity.getResources()
								.getString(R.string.paysuccess));
						LoginService loginService = AlibabaSDK
								.getService(LoginService.class);
						if (loginService == null) {
							return;
						}
						Session session = loginService.getSession();
						final User luser = UserHelper.getUser();
						if (session != null && session.isLogin()
								&& luser.getIsGuest() == 0) {

							ShopHttpHelper.bindTaoBaoAndYoYo(activity, ""
									+ luser.getUid(), "" + luser.getToken(),
									session.getUserId(),
									session.getUser().avatarUrl,
									session.getUser().nick);
						}
						if (UserHelper.isGusetUser(activity)) return;
						Utils.goActivity(activity, PaySuccessActivity.class);
					}
				}, null, itemID, productType, map, taokeParams);
	}

	/* *//**
	 * 显示购物车
	 * 
	 * @param activity
	 */
	/*
	 * public static void showShopMarket(final Activity activity) { CartService
	 * cartService=AlibabaSDK.getService(CartService.class);
	 * if(cartService==null) { return ; } cartService.showCart(activity, new
	 * TradeProcessCallback() {
	 * 
	 * 
	 * @Override public void onFailure(int arg0, String arg1) { Log.d(TAG,
	 * "showCart---failure:---" + arg1); }
	 * 
	 * @Override public void onPaySuccess(TradeResult arg0) {
	 * ToastHelper.show(activity,
	 * activity.getResources().getString(R.string.paysuccess)); LoginService
	 * loginService = AlibabaSDK.getService(LoginService.class);
	 * if(loginService==null) { return ; } Session session =
	 * loginService.getSession(); final User luser = UserHelper.getUser(); if
	 * (session != null && session.isLogin() && luser.getIsGuest() == 0) {
	 * ShopHttpHelper.bindTaoBaoAndYoYo(activity, "" + luser.getUid(), "" +
	 * luser.getToken(), session.getUserId(), session.getUser().avatarUrl,
	 * session.getUser().nick); }
	 * 
	 * 
	 * 
	 * 
	 * } }); }
	 */

	/**
	 * 显示购物车
	 * 
	 * @param activity
	 */
	public static void showShopMarket(final Activity activity) {
		if (!LightDBHelper.getIsNotTipGoShopOrNotFees(activity)) {
			ToastHelper.showCommonDialog(
					activity,
					activity.getResources().getString(
							R.string.shop_tip_dialog_title),
					activity.getResources().getString(
							R.string.shop_tip_dialog_maybe_no_backfee),
					activity.getResources().getString(
							R.string.shop_tip_dialog_no_tip),
					activity.getResources().getString(
							R.string.shop_tip_dialog_ok),
					new IDialogCallback() {
						@Override
						public void onConfirmOnClick() {
							goShopCar(activity);
						}

						@Override
						public void onCancelOnClick() {
							LightDBHelper.setIsNotTipGoShopOrNotFees(activity,
									true);
							goShopCar(activity);
						}
					});
		} else {
			goShopCar(activity);
		}

	}

	private static void goShopCar(final Activity activity) {

		CartService cartService = AlibabaSDK.getService(CartService.class);
		if (cartService == null) {
			return;
		}
		cartService.showCart(activity, new TradeProcessCallback() {

			@Override
			public void onFailure(int arg0, String arg1) {
				Log.d(TAG, "showCart---failure:---" + arg1);
			}

			@Override
			public void onPaySuccess(TradeResult arg0) {
				ToastHelper.show(activity,
						activity.getResources().getString(R.string.paysuccess));
				LoginService loginService = AlibabaSDK
						.getService(LoginService.class);
				if (loginService == null) {
					return;
				}
				Session session = loginService.getSession();
				final User luser = UserHelper.getUser();
				if (session != null && session.isLogin()
						&& luser.getIsGuest() == 0) {
					ShopHttpHelper.bindTaoBaoAndYoYo(activity,
							"" + luser.getUid(), "" + luser.getToken(),
							session.getUserId(), session.getUser().avatarUrl,
							session.getUser().nick);
				}

				if (UserHelper.isGusetUser(activity)) return;
				Utils.goActivity(activity, PaySuccessActivity.class);

			}
		});
	}
	
	
	
	
	
	
	
	
	public interface TAELoginCallback{
		void result(Session session);
	}
	
	
}
