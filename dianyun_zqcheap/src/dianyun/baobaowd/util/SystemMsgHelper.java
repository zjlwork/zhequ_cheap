package dianyun.baobaowd.util;

import java.util.List;
import android.content.Context;
import dianyun.baobaowd.data.Message;
import dianyun.baobaowd.db.SystemMsgDBHelper;
import dianyun.baobaowd.db.TableConstants;
import dianyun.baobaowd.help.LogFile;

/**
 * 联系人数据库操作辅助
 * 
 */
public class SystemMsgHelper
{

	public static long getSystemmsgMaxSeqId(Context context)
	{
		SystemMsgDBHelper lSystemMsgDBHelper = null;
		try
		{
			lSystemMsgDBHelper = new SystemMsgDBHelper(context,
					TableConstants.TABLE_SYSTEMMSG);
			return lSystemMsgDBHelper.getSystemmsgMaxSeqId();
		}
		catch (Exception e)
		{
			LogFile.SaveExceptionLog(e);
		}
		finally
		{
			if (null != lSystemMsgDBHelper)
				lSystemMsgDBHelper.closeDB();
		}
		return 0;
	}

	public static List<Message> getMessageList(Context context)
	{
		SystemMsgDBHelper lSystemMsgDBHelper = null;
		try
		{
			lSystemMsgDBHelper = new SystemMsgDBHelper(context,
					TableConstants.TABLE_SYSTEMMSG);
			return lSystemMsgDBHelper.getMessageList();
		}
		catch (Exception e)
		{
			LogFile.SaveExceptionLog(e);
		}
		finally
		{
			if (null != lSystemMsgDBHelper)
				lSystemMsgDBHelper.closeDB();
		}
		return null;
	}

	public static void addMessageList(Context context, List<Message> messageList)
	{
		if (messageList != null && messageList.size() > 0)
		{
			SystemMsgDBHelper lSystemMsgDBHelper = null;
		
			try
			{
				lSystemMsgDBHelper = new SystemMsgDBHelper(context,
						TableConstants.TABLE_SYSTEMMSG);
			
				lSystemMsgDBHelper.beginTransaciton();
				for (Message message : messageList)
				{
					
					if (!lSystemMsgDBHelper.isExist(message.getSeqId()))
					{
						lSystemMsgDBHelper.insert(message);
					}
				}
				lSystemMsgDBHelper.setTransactionSuccessful();
			}
			catch (Exception e)
			{
				LogFile.SaveExceptionLog(e);
			}
			finally
			{
				if (null != lSystemMsgDBHelper)
				{
					lSystemMsgDBHelper.endTransaction();
					lSystemMsgDBHelper.closeDB();
				}
			
			}
		}
	}
	public static void deleteAll(Context context)
	{
		
			SystemMsgDBHelper lSystemMsgDBHelper = null;
			
			try
			{
				lSystemMsgDBHelper = new SystemMsgDBHelper(context,
						TableConstants.TABLE_SYSTEMMSG);
				lSystemMsgDBHelper.deleteAll();
				
			}
			catch (Exception e)
			{
				LogFile.SaveExceptionLog(e);
			}
			finally
			{
				if (null != lSystemMsgDBHelper)
				{
					lSystemMsgDBHelper.closeDB();
				}
				
			}
		
	}
}
