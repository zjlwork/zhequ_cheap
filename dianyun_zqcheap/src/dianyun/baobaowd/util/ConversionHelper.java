package dianyun.baobaowd.util;

import android.content.Context;
import android.graphics.Paint;
import android.graphics.Paint.FontMetrics;
import android.util.TypedValue;

public class ConversionHelper
{

	public static int dipToPx(int dip, Context context)
	{
		float fPx = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dip,
				context.getResources().getDisplayMetrics());
		int px = Math.round(fPx);
		return px;
	}

	public static int dipToPx(float dip, Context context)
	{
		float fPx = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dip,
				context.getResources().getDisplayMetrics());
		int px = Math.round(fPx);
		return px;
	}

	public static int pxToDip(int px, Context context)
	{
		// 同理 px转dip：
		float fDip = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_PX, px,
				context.getResources().getDisplayMetrics());
		int iDip = Math.round(fDip);
		return iDip;
	}

	// 获取字符串长度
	public static float getTextLength(String text, float lFloat)
	{
		Paint lPaint = new Paint();
		lPaint.setTextSize(lFloat);
		return lPaint.measureText(text);
	}

	// DisplayMetrics dm = new DisplayMetrics();
	// dm = getResources().getDisplayMetrics();
	//
	//
	// System.out.println("dm.widthPixels====="+dm.widthPixels);
	/*
	 *//**
	 * 将dip转换为px
	 * 
	 * @param context
	 * @param dipValue
	 * @return
	 */
	/*
	 * public static int dip2px(float dipValue,Context context) { final float
	 * scale = context.getResources().getDisplayMetrics().density; return (int)
	 * (dipValue * scale + 0.5f); }
	 *//**
	 * 将px转换为dip
	 * 
	 * @param context
	 * @param dipValue
	 * @return
	 */
	/*
	 * public static int px2dip(Context context, float pxValue) { final float
	 * scale = context.getResources().getDisplayMetrics().density; return (int)
	 * (pxValue / scale + 0.5f); }
	 */
	// 获取字符串高度
	public static int getFontHeight(float fontSize)
	{
		Paint paint = new Paint();
		paint.setTextSize(fontSize);
		FontMetrics fm = paint.getFontMetrics();
		return (int) Math.ceil(fm.descent - fm.top) + 1;
	}

	// 获取字符串实际高度
	public static int getFontHeight2(float fontSize)
	{
		Paint paint = new Paint();
		paint.setTextSize(fontSize);
		FontMetrics fm = paint.getFontMetrics();
		return (int) Math.ceil(fm.descent - fm.ascent);
	}
}
