package dianyun.baobaowd.util;

import java.io.File;
import java.util.List;
import java.util.UUID;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.provider.MediaStore.MediaColumns;
import android.widget.ImageView;
import android.widget.LinearLayout;
import dianyun.baobaowd.help.FileHelper;
import dianyun.baobaowd.help.LogFile;

public class CameraGalleryHelper
{

	public static final String IMAGE_UNSPECIFIED = "image/*";

	public static String startCamera(Context context)
	{
		Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
		String tempImgPath = FileHelper.getTempPath()
				+ UUID.randomUUID().toString()
				+ GobalConstants.Suffix.PIC_SUFFIX_JPG;
		intent.putExtra(MediaStore.EXTRA_OUTPUT,
				Uri.fromFile(new File(tempImgPath)));
		((Activity) context).startActivityForResult(intent,
				GobalConstants.RequestCode.CHOOSE_CAPTURE);
		return tempImgPath;
	}

	public static void startGallery(Context context)
	{
		Intent intent = new Intent(Intent.ACTION_PICK, null);
		intent.setDataAndType(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
				IMAGE_UNSPECIFIED);
		((Activity) context).startActivityForResult(intent,
				GobalConstants.RequestCode.CHOOSE_PICTURE);
	}

	private static void startCropImage(Context context, Uri uri)
	{
		Intent intent = new Intent("com.android.camera.action.CROP");
		intent.setDataAndType(uri, IMAGE_UNSPECIFIED);
		intent.putExtra("crop", "true");
		// aspectX aspectY 是宽高的比例
		intent.putExtra("aspectX", 1);
		intent.putExtra("aspectY", 1);
		// outputX outputY 是裁剪图片宽高
		intent.putExtra("outputX", 210);
		intent.putExtra("outputY", 210);
		// 图片格式
		intent.putExtra("outputFormat", "JPEG");
		// 不启用人脸识别
		intent.putExtra("noFaceDetection", false);
		intent.putExtra("return-data", true);
		((Activity) context).startActivityForResult(intent,
				GobalConstants.RequestCode.CAPTURE_CUT);
	}

	public static void handleCreateResult(Context context, int requestCode,
			int resultCode, Intent data, String tempImgPath,
			LinearLayout mAttachmentLayout, List<String> mImgsPath)
	{
		if (resultCode != Activity.RESULT_OK)
			return;
		// 拍照
		if (requestCode == GobalConstants.RequestCode.CHOOSE_CAPTURE)
		{
			// 设置文件保存路径这里放在跟目录下
			if (tempImgPath != null)
			{
				AttachmentHelper.saveImg(tempImgPath, mImgsPath);
				AttachmentHelper.refreshAttachmentLayout(context,
						mAttachmentLayout, mImgsPath);
			}
		}
		if (data == null)
			return;
		// 读取相册缩放图片
		if (requestCode == GobalConstants.RequestCode.CHOOSE_PICTURE)
		{
			try
			{
				Uri uri = data.getData();
				// 通过URI获取图片绝对地址
				String[] proj = { MediaColumns.DATA };
				Cursor cursor = ((Activity) context).managedQuery(uri, proj,
						null, null, null);
				int actual_image_column_index = cursor
						.getColumnIndexOrThrow(MediaColumns.DATA);
				// 游标跳到首位，防止越界
				cursor.moveToFirst();
				String img_path = cursor.getString(actual_image_column_index);
				AttachmentHelper.saveImg(img_path, mImgsPath);
				AttachmentHelper.refreshAttachmentLayout(context,
						mAttachmentLayout, mImgsPath);
			}
			catch (Exception e)
			{
				LogFile.SaveExceptionLog(e);
			}
		}
		System.out.println("   mImgsPath==handleCreateResult=="
				+ mImgsPath.size());
	}

	public static String handleReplyResult(Context context, int requestCode,
			int resultCode, Intent data, String tempImgPath, ImageView mImgIv,
			String mFilePath)
	{
		if (resultCode != Activity.RESULT_OK)
			return null;
		// 拍照
		if (requestCode == GobalConstants.RequestCode.CHOOSE_CAPTURE)
		{
			// 设置文件保存路径这里放在跟目录下
			if (tempImgPath != null)
			{
				mFilePath = AttachmentHelper.saveImg(tempImgPath, mImgIv);
				return mFilePath;
			}
		}
		if (data == null)
			return null;
		// 读取相册缩放图片
		if (requestCode == GobalConstants.RequestCode.CHOOSE_PICTURE)
		{
			// startCropImage(data.getData());
			try
			{
				Uri uri = data.getData();
				// 通过URI获取图片绝对地址
				String[] proj = { MediaColumns.DATA };
				Cursor cursor = ((Activity) context).managedQuery(uri, proj,
						null, null, null);
				int actual_image_column_index = cursor
						.getColumnIndexOrThrow(MediaColumns.DATA);
				// 游标跳到首位，防止越界
				cursor.moveToFirst();
				String img_path = cursor.getString(actual_image_column_index);
				mFilePath = AttachmentHelper.saveImg(img_path, mImgIv);
			}
			catch (Exception e)
			{
				LogFile.SaveExceptionLog(e);
			}
		}
		System.out.println("handleReplyResult mFilePath====" + mFilePath);
		return mFilePath;
	}

	public static String handleChangeAvatarResult(Context context,
			int requestCode, int resultCode, Intent data, String mTempImgPath,
			ImageView mAvatarIv, String mAvatarLocalPath)
	{
		if (resultCode != Activity.RESULT_OK)
			return null;
		// 拍照
		if (requestCode == GobalConstants.RequestCode.CHOOSE_CAPTURE)
		{
			// 设置文件保存路径这里放在跟目录下
			if (mTempImgPath != null)
			{
				File picture = new File(mTempImgPath);
				startCropImage(context, Uri.fromFile(picture));
			}
		}
		if (data == null)
			return null;
		// 读取相册缩放图片
		if (requestCode == GobalConstants.RequestCode.CHOOSE_PICTURE)
		{
			startCropImage(context, data.getData());
		}
		// 处理结果
		if (requestCode == GobalConstants.RequestCode.CAPTURE_CUT)
		{
			Bundle extras = data.getExtras();
			if (extras != null)
			{
				Bitmap photo = extras.getParcelable("data");
				mAvatarLocalPath = ImageHelper.saveImgData(photo);
				mAvatarIv.setImageBitmap(ImageHelper.toRoundBitmap(photo));
			}
		}
		return mAvatarLocalPath;
	}
}
