package dianyun.baobaowd.util;

import java.util.Date;
import java.util.UUID;
import android.content.Context;
import android.text.TextUtils;
import dianyun.baobaowd.data.User;
import dianyun.baobaowd.data.UserStatus;
import dianyun.baobaowd.db.TableConstants;
import dianyun.baobaowd.db.UserStatusDBHelper;
import dianyun.baobaowd.help.LogFile;

/**
 * 联系人数据库操作辅助
 * 
 */
public class UserStatusHelper
{

	public static void saveUserStatus(Context context, User user)
	{
		UserStatus lUserStatus = null;
		String nowTime = DateHelper.getTextByDate(new Date(),
				DateHelper.YYYY_MM_DD);
		if (TextUtils.isEmpty(user.getBabyBirthday()))
		{
			String lastTime = DateHelper.getTextByDate(new Date(),
					DateHelper.YYYY_MM_DD);
			int cycle = 28;
			int period = 7;
			lUserStatus = new UserStatus(UUID.randomUUID().toString(),
					GobalConstants.DayArticleStatus.PREPARE, lastTime, period,
					cycle, (byte) 0);
		}
		else
		{
			if (user.getBabyBirthday().compareTo(nowTime) >= 0)
			{
				lUserStatus = new UserStatus(UUID.randomUUID().toString(),
						GobalConstants.DayArticleStatus.PREGNANCY,
						user.getBabyBirthday(), 0, 0, (byte) 0);
			}
			else
				if (user.getBabyBirthday().compareTo(nowTime) < 0)
				{
					lUserStatus = new UserStatus(UUID.randomUUID().toString(),
							GobalConstants.DayArticleStatus.HASCHILD,
							user.getBabyBirthday(), 0, 0, user.getBabyGender());
				}
		}
		UserStatusHelper.deleteAllUserStatus(context);
		UserStatusHelper.addUserStatus(context, lUserStatus);
	}

	public static UserStatus getUserStatus(Context context)
	{
		UserStatusDBHelper lUserStatusDBHelper = null;
		try
		{
			lUserStatusDBHelper = new UserStatusDBHelper(context,
					TableConstants.TABLE_USERSTATUS);
			return lUserStatusDBHelper.getUserStatus();
		}
		catch (Exception e)
		{
			LogFile.SaveExceptionLog(e);
		}
		finally
		{
			if (null != lUserStatusDBHelper)
				lUserStatusDBHelper.closeDB();
		}
		return null;
	}

	public static long addUserStatus(Context context, UserStatus userStatus)
	{
		UserStatusDBHelper lUserStatusDBHelper = null;
		try
		{
			lUserStatusDBHelper = new UserStatusDBHelper(context,
					TableConstants.TABLE_USERSTATUS);
			return lUserStatusDBHelper.insert(userStatus);
		}
		catch (Exception e)
		{
			LogFile.SaveExceptionLog(e);
		}
		finally
		{
			if (null != lUserStatusDBHelper)
				lUserStatusDBHelper.closeDB();
		}
		return -1;
	}

	/*
	 * public static void updateUser(Context context,User user){ UserDBHelper
	 * lUserDBHelper = null; try { lUserDBHelper = new UserDBHelper(context,
	 * TableConstants.TABLE_USER); lUserDBHelper.update(user); User mUser =
	 * ((BaoBaoWDApplication)context.getApplicationContext()).getUser();
	 * if(user.getUid().equals(mUser.getUid())){
	 * ((BaoBaoWDApplication)context.getApplicationContext()).setUser(user); } }
	 * catch (Exception e) { LogFile.SaveExceptionLog(e); } finally { if (null
	 * != lUserDBHelper) lUserDBHelper.closeDB(); } }
	 */
	public static void deleteUserStatus(Context context, UserStatus userStatus)
	{
		UserStatusDBHelper lUserStatusDBHelper = null;
		try
		{
			lUserStatusDBHelper = new UserStatusDBHelper(context,
					TableConstants.TABLE_USERSTATUS);
			lUserStatusDBHelper.delete(userStatus);
		}
		catch (Exception e)
		{
			LogFile.SaveExceptionLog(e);
		}
		finally
		{
			if (null != lUserStatusDBHelper)
				lUserStatusDBHelper.closeDB();
		}
	}

	public static void deleteAllUserStatus(Context context)
	{
		UserStatusDBHelper lUserStatusDBHelper = null;
		try
		{
			lUserStatusDBHelper = new UserStatusDBHelper(context,
					TableConstants.TABLE_USERSTATUS);
			lUserStatusDBHelper.deleteAll();
		}
		catch (Exception e)
		{
			LogFile.SaveExceptionLog(e);
		}
		finally
		{
			if (null != lUserStatusDBHelper)
				lUserStatusDBHelper.closeDB();
		}
	}

	public static void updateUserStatus(Context context, UserStatus userStatus)
	{
		UserStatusDBHelper lUserStatusDBHelper = null;
		try
		{
			lUserStatusDBHelper = new UserStatusDBHelper(context,
					TableConstants.TABLE_USERSTATUS);
			lUserStatusDBHelper.update(userStatus);
		}
		catch (Exception e)
		{
			LogFile.SaveExceptionLog(e);
		}
		finally
		{
			if (null != lUserStatusDBHelper)
				lUserStatusDBHelper.closeDB();
		}
	}
}
