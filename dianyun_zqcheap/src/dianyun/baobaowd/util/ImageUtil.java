

package dianyun.baobaowd.util;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;

import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Paint;
import android.graphics.PixelFormat;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import dianyun.zqcheap.application.BaoBaoWDApplication;

public class ImageUtil
{

	public static byte[] bitmap2ByteArray(Bitmap bitmap)
	{
		byte[] res=null;
		
		if(bitmap!=null)
		{
			ByteArrayOutputStream outStream=new ByteArrayOutputStream();
			bitmap.compress(CompressFormat.PNG, 100, outStream);
			res=outStream.toByteArray();
		}
		return res;
	}
	public static Drawable loadImageFromUrl(String url)
	{
		URL m;
		InputStream i = null;
		try
		{
			m = new URL(url);
			i = (InputStream) m.getContent();
		}
		catch (MalformedURLException e1)
		{
			e1.printStackTrace();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		Drawable d = Drawable.createFromStream(i, "src");
		return d;
	}

	public static Bitmap processBitmap(String url, int reqImageWidth,
			int reqImageHeight, String id)
	{
		String imageHost = null;
		imageHost = BaoBaoWDApplication.cachePath;
		/*
		 * if(TextUtils.isEmpty(id)){ }else{ imageHost=
		 * ConfigUtility.LIVE_IMAGE_CACHE_PATH; }
		 */
		if (!TextUtils.isEmpty(url))
		{
			File file = new File(imageHost + File.separator
					+ MD5Util.getMD5String(url));
			if (file.exists())
			{
				return decodeSampledBitmapFromFile(file.toString(),
						reqImageWidth, reqImageHeight);
			}
			if (!TelInfo.isNetAvailable(BaoBaoWDApplication.context))
			{
				return null;
			}
			InputStream ip = HttpUtils.getRequestStream(
					BaoBaoWDApplication.context, url);
			if (ip != null)
			{
				final File f = downLoadImage(ip, url, imageHost);
				if (f != null)
				{
					// Return a sampled down version
					return decodeSampledBitmapFromFile(f.toString(),
							reqImageWidth, reqImageHeight);
				}
			}
			return null;
		}
		else
			return null;
	}

	
	final static int buffer_size = 1024;
	public static final int IO_BUFFER_SIZE = 8 * 1024;

	public static File downLoadImage(InputStream in, String path, String host)
	{
		BufferedOutputStream out = null;
		// FileInputStream input =null;
		FileOutputStream fout = null;
		BufferedInputStream bif = null;
		File f = null;
		try
		{
			f = new File(host + File.separator + MD5Util.getMD5String(path));
			// final InputStream input = new
			// BufferedInputStream(in,DiskLruCache.IO_BUFFER_SIZE);
			bif = new BufferedInputStream(in, IO_BUFFER_SIZE);
			fout = new FileOutputStream(f);
			out = new BufferedOutputStream(fout, IO_BUFFER_SIZE);
			byte[] bytes = new byte[buffer_size];
			int count = 0;
			// for (;;) {
			while ((count = bif.read(bytes, 0, buffer_size)) != -1)
			{
				// int count = input.read(bytes, 0, buffer_size);
				// if (count == -1)
				// break;
				out.write(bytes, 0, count);
				out.flush();
			}
			/*
			 * Bitmap bitmap = BitmapFactory.decodeStream(bif); if(bitmap
			 * !=null){ boolean saved = bitmap.compress(CompressFormat.JPEG,
			 * 100, out); out.flush(); if(!saved){ return null; } }
			 */
			return f;
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			try
			{
				if (in != null)
					in.close();
				if (bif != null)
					bif.close();
				if (out != null)
					out.close();
				if (fout != null)
					fout.close();
			}
			catch (IOException e)
			{
				e.printStackTrace();
			}
		}
		return null;
	}

	public static class FlushedInputStream extends FilterInputStream
	{

		public FlushedInputStream(InputStream inputStream)
		{
			super(inputStream);
		}

		@Override
		public long skip(long n) throws IOException
		{
			long totalBytesSkipped = 0L;
			while (totalBytesSkipped < n)
			{
				long bytesSkipped = in.skip(n - totalBytesSkipped);
				if (bytesSkipped == 0L)
				{
					int by_te = read();
					if (by_te < 0)
					{
						break; // we reached EOF
					}
					else
					{
						bytesSkipped = 1; // we read one byte
					}
				}
				totalBytesSkipped += bytesSkipped;
			}
			return totalBytesSkipped;
		}
	}

	public static Bitmap getBitmapFromUrl(String url) throws Exception
	{
		byte[] bytes = HttpUtils.getBytesFromUrl(BaoBaoWDApplication.context,
				url);
		return byteToBitmap(bytes);
	}

	public static Bitmap getRoundBitmapFromUrl(String url, int pixels)
			throws Exception
	{
		byte[] bytes = HttpUtils.getBytesFromUrl(BaoBaoWDApplication.context,
				url);
		Bitmap bitmap = byteToBitmap(bytes);
		return toRoundCorner(bitmap, pixels);
	}

	public static Drawable geRoundDrawableFromUrl(String url, int pixels)
			throws Exception
	{
		byte[] bytes = HttpUtils.getBytesFromUrl(BaoBaoWDApplication.context,
				url);
		BitmapDrawable bitmapDrawable = (BitmapDrawable) byteToDrawable(bytes);
		return toRoundCorner(bitmapDrawable, pixels);
	}

	public static Bitmap byteToBitmap(byte[] byteArray)
	{
		if (byteArray.length != 0)
		{
			return BitmapFactory
					.decodeByteArray(byteArray, 0, byteArray.length);
		}
		else
		{
			return null;
		}
	}

	public static Drawable byteToDrawable(byte[] byteArray)
	{
		ByteArrayInputStream ins = new ByteArrayInputStream(byteArray);
		return Drawable.createFromStream(ins, null);
	}

	public static byte[] Bitmap2Bytes(Bitmap bm)
	{
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		bm.compress(Bitmap.CompressFormat.PNG, 100, baos);
		return baos.toByteArray();
	}

	public static Bitmap drawableToBitmap(Drawable drawable)
	{
		Bitmap bitmap = Bitmap
				.createBitmap(
						drawable.getIntrinsicWidth(),
						drawable.getIntrinsicHeight(),
						drawable.getOpacity() != PixelFormat.OPAQUE ? Bitmap.Config.ARGB_8888
								: Bitmap.Config.RGB_565);
		Canvas canvas = new Canvas(bitmap);
		drawable.setBounds(0, 0, drawable.getIntrinsicWidth(),
				drawable.getIntrinsicHeight());
		drawable.draw(canvas);
		return bitmap;
	}

	public static Bitmap toGrayscale(Bitmap bmpOriginal)
	{
		int width, height;
		height = bmpOriginal.getHeight();
		width = bmpOriginal.getWidth();
		Bitmap bmpGrayscale = Bitmap.createBitmap(width, height,
				Bitmap.Config.RGB_565);
		Canvas c = new Canvas(bmpGrayscale);
		Paint paint = new Paint();
		ColorMatrix cm = new ColorMatrix();
		cm.setSaturation(0);
		ColorMatrixColorFilter f = new ColorMatrixColorFilter(cm);
		paint.setColorFilter(f);
		c.drawBitmap(bmpOriginal, 0, 0, paint);
		return bmpGrayscale;
	}

	public static Bitmap toGrayscale(Bitmap bmpOriginal, int pixels)
	{
		return toRoundCorner(toGrayscale(bmpOriginal), pixels);
	}

	public static Bitmap toRoundCorner(Bitmap bitmap, int pixels)
	{
		Bitmap output = Bitmap.createBitmap(bitmap.getWidth(),
				bitmap.getHeight(), Config.ARGB_8888);
		Canvas canvas = new Canvas(output);
		final int color = 0xff424242;
		final Paint paint = new Paint();
		final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
		final RectF rectF = new RectF(rect);
		final float roundPx = pixels;
		paint.setAntiAlias(true);
		canvas.drawARGB(0, 0, 0, 0);
		paint.setColor(color);
		canvas.drawRoundRect(rectF, roundPx, roundPx, paint);
		paint.setXfermode(new PorterDuffXfermode(Mode.SRC_IN));
		canvas.drawBitmap(bitmap, rect, rect, paint);
		return output;
	}

	public static BitmapDrawable toRoundCorner(BitmapDrawable bitmapDrawable,
			int pixels)
	{
		Bitmap bitmap = bitmapDrawable.getBitmap();
		bitmapDrawable = new BitmapDrawable(toRoundCorner(bitmap, pixels));
		return bitmapDrawable;
	}

	public static int computeSampleSize(BitmapFactory.Options options,
			int minSideLength, int maxNumOfPixels)
	{
		int initialSize = calculateInSampleSize(options, minSideLength,
				maxNumOfPixels);
		int roundedSize;
		if (initialSize <= 8)
		{
			roundedSize = 1;
			while (roundedSize < initialSize)
			{
				roundedSize <<= 1;
			}
		}
		else
		{
			roundedSize = (initialSize + 7) / 8 * 8;
		}
		return roundedSize;
	}

	public static int calculateInSampleSize(BitmapFactory.Options options,
			int reqWidth, int reqHeight)
	{
		// Raw height and width of image
		final int height = options.outHeight;
		final int width = options.outWidth;
		int inSampleSize = 1;
		reqHeight = height * reqWidth / width;
		if (height > reqHeight || width > reqWidth)
		{
			if (width > height)
			{
				inSampleSize = Math.round((float) width / (float) reqWidth);
			}
			else
			{
				// inSampleSize = Math.round((float) width / (float) reqWidth);
			}
			// This offers some additional logic in case the image has a strange
			// aspect ratio. For example, a panorama may have a much larger
			// width than height. In these cases the total pixels might still
			// end up being too large to fit comfortably in memory, so we should
			// be more aggressive with sample down the image (=larger
			// inSampleSize).
			final float totalPixels = width * height;
			// Anything more than 2x the requested pixels we'll sample down
			// further.
			final float totalReqPixelsCap = reqWidth * reqHeight * 2;
			while (totalPixels / (inSampleSize * inSampleSize) > totalReqPixelsCap)
			{
				inSampleSize++;
			}
		}
		inSampleSize = 4;
		return inSampleSize;
	}

	public static Bitmap decodeSampledBitmapFromFile(String filename,
			int reqWidth, int reqHeight)
	{
		// First decode with inJustDecodeBounds=true to check dimensions
		final BitmapFactory.Options options = new BitmapFactory.Options();
		options.inJustDecodeBounds = true;
		BitmapFactory.decodeFile(filename, options);
		// Calculate inSampleSize
		if (reqHeight == 0)
		{
			options.inSampleSize = calculateInSampleSize(options, reqWidth,
					reqHeight);
		}
		else
		{
			options.inSampleSize = 1;// calculateInSampleSize(options, reqWidth,
										// reqHeight);
		}
		// Decode bitmap with inSampleSize set
		options.inJustDecodeBounds = false;
		FileInputStream finput;
		try
		{
			finput = new FileInputStream(filename);
			return BitmapFactory.decodeStream(finput, null, options);
		}
		catch (FileNotFoundException e)
		{
			e.printStackTrace();
		}
		return null;
	}
}
