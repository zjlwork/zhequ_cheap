package dianyun.baobaowd.util;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.provider.Settings;
import android.provider.Settings.SettingNotFoundException;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.view.WindowManager;
import dianyun.baobaowd.data.VersionData;
import dianyun.baobaowd.db.LightDBHelper;
import dianyun.baobaowd.help.LogFile;
import dianyun.baobaowd.serverinterface.CheckVersion;
import dianyun.zqcheap.R;
import dianyun.zqcheap.activity.HtmlActivity;
import dianyun.zqcheap.application.BaoBaoWDApplication;

public class Utils {

    private final static String CREATE_SHORTCUR_KEY = "DIANYUN.ISCREATE.SHORTCUT";

    /**
     * 验证手机格式
     */
    public static boolean isMobileNO(String mobiles) {
		/*
		移动：134、135、136、137、138、139、150、151、157(TD)、158、159、187、188
		联通：130、131、132、152、155、156、185、186
		电信：133、153、180、189、（1349卫通）
		总结起来就是第一位必定为1，第二位必定为3或5或8，其他位置的可以为0-9
		*/
        String telRegex = "[1][358]\\d{9}";//"[1]"代表第1位为数字1，"[358]"代表第二位可以为3、5、8中的一个，"\\d{9}"代表后面是可以是0～9的数字，有9位。
        if (TextUtils.isEmpty(mobiles)) return false;
        else return mobiles.matches(telRegex);
    }
    public static String convertTimeToDay(Context context, int lastMinutesSource) {
        String res = "";
        int hour = 0;
        int minute = 0;
        int day = 0;
        int lastMinutes = Math.abs(lastMinutesSource);
        if (lastMinutes < 60) {
            res += lastMinutes
                    + context.getResources().getString(R.string.minute);
        } else if (lastMinutes < 60 * 24) {
            hour = lastMinutes / 60;
            minute = lastMinutes % 60;
            res += hour + context.getResources().getString(R.string.hour);
            if (minute > 0) {
                res += minute + "分";
            }
        } else {
            day = lastMinutes / (24 * 60);
            int temp = lastMinutes % (24 * 60);
            hour = temp / 60;
            res += day + context.getResources().getString(R.string.day) + hour
                    + context.getResources().getString(R.string.hour);
        }
        if (lastMinutesSource < 0) {
            // 负数 代表是 距离下架时间
            res = String
                    .format(context.getResources().getString(
                            R.string.last_to_end), res);
        } else {
            // 正数 代表是 距离上架时间
            res = String.format(
                    context.getResources().getString(R.string.last_to_start),
                    res);
        }
        return res;
    }

    /**
     * 获取配置信息中配置是否需要创建快捷方式
     *
     * @param context
     * @param metaKey
     * @return
     */
    public static boolean getIsCreateShortCut(Context context) {
        boolean res = false;
        if (context == null) {
            return res;
        }
        ApplicationInfo info;
        try {
            info = context.getPackageManager().getApplicationInfo(
                    context.getPackageName(), PackageManager.GET_META_DATA);
            res = info.metaData.getBoolean(CREATE_SHORTCUR_KEY);
        } catch (NameNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return res;
    }

    public static boolean isExistsKey(String name, Iterator itn) {
        boolean res = false;
        if (itn != null && name != null && !name.equals("")) {
            while (itn.hasNext()) {
                String key = (String) itn.next();
                if (key.equals(name)) {
                    res = true;
                    break;
                }
            }
        }
        return res;
    }

    public static void goHtmlActivity(Context context, String title,
                                      String webUrl) {
        Intent itn = new Intent(context, HtmlActivity.class);
        itn.putExtra(GobalConstants.Data.URL, webUrl);
        itn.putExtra(GobalConstants.Data.TITLE, title);
        context.startActivity(itn);
    }

    public static String getPhoneNumber(Context context) {
        TelephonyManager teltManager = (TelephonyManager) context
                .getSystemService(Context.TELEPHONY_SERVICE);
        return teltManager.getLine1Number();
    }

    public static boolean isNetAvailable(Context paramContext) {
        boolean isAvilable = false;
        try {
            ConnectivityManager connectManger = (ConnectivityManager) paramContext
                    .getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo localNetworkInfo = connectManger.getActiveNetworkInfo();
            if (localNetworkInfo != null) {
                /*
                 * State mobile = connectManger.getNetworkInfo(
				 * ConnectivityManager.TYPE_MOBILE).getState(); State wifi =
				 * connectManger.getNetworkInfo(
				 * ConnectivityManager.TYPE_WIFI).getState(); if (mobile ==
				 * State.CONNECTED || mobile == State.CONNECTING) if (wifi ==
				 * State.CONNECTED || wifi == State.CONNECTING)
				 * paramContext.startActivity(new Intent(
				 * Settings.ACTION_WIRELESS_SETTINGS));
				 */
                isAvilable = localNetworkInfo.isAvailable();
            }
        } catch (Exception localException) {
        }
        return isAvilable;
    }

    public final static String NO_TIME = "00:00";
    public static Calendar mCalendar = Calendar.getInstance(TimeZone
            .getTimeZone("GMT+8"));

    public static String getFormatTime(Context context, long millionTime) {
        if (millionTime > 0) {
            SimpleDateFormat format = new SimpleDateFormat("mm:ss");
            format.setTimeZone(TimeZone.getTimeZone("GMT"));
            mCalendar.clear();
            mCalendar.setTimeInMillis(millionTime);
            int hours = mCalendar.get(Calendar.HOUR);
            if (hours > 0) {
                format.applyPattern("HH:mm:ss");
            }
            return format.format(mCalendar.getTime());
        }
        return NO_TIME;
    }

    public static String getFormatTimeforRadio(Context context, long millionTime) {
        if (millionTime > 0) {
            SimpleDateFormat format = new SimpleDateFormat("HH:mm");
            format.setTimeZone(TimeZone.getTimeZone("GMT+8"));
            mCalendar.clear();
            mCalendar.setTimeInMillis(millionTime);
            return format.format(mCalendar.getTime());
        }
        return NO_TIME;
    }

    public static boolean isAutoBrightness(ContentResolver aContentResolver) {
        boolean automicBrightness = false;
        try {
            automicBrightness = Settings.System.getInt(aContentResolver,
                    Settings.System.SCREEN_BRIGHTNESS_MODE) == Settings.System.SCREEN_BRIGHTNESS_MODE_AUTOMATIC;
        } catch (SettingNotFoundException e) {
            e.printStackTrace();
        }
        return automicBrightness;
    }

    public static int getScreenBrightness(Context context) {
        int nowBrightnessValue = 0;
        ContentResolver resolver = context.getContentResolver();
        try {
            nowBrightnessValue = android.provider.Settings.System.getInt(
                    resolver, Settings.System.SCREEN_BRIGHTNESS);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return nowBrightnessValue;
    }

    public static void setBrightness(Activity activity, int brightness) {
        // Settings.System.putInt(activity.getContentResolver(),
        // Settings.System.SCREEN_BRIGHTNESS_MODE,
        // Settings.System.SCREEN_BRIGHTNESS_MODE_MANUAL);
        WindowManager.LayoutParams lp = activity.getWindow().getAttributes();
        lp.screenBrightness = Math.max(Float.valueOf(brightness) * (1f / 255f),
                0.2f);
        activity.getWindow().setAttributes(lp);
    }

    public static void stopAutoBrightness(Activity activity) {
        Settings.System.putInt(activity.getContentResolver(),
                Settings.System.SCREEN_BRIGHTNESS_MODE,
                Settings.System.SCREEN_BRIGHTNESS_MODE_MANUAL);
    }

    public static void startAutoBrightness(Activity activity) {
        Settings.System.putInt(activity.getContentResolver(),
                Settings.System.SCREEN_BRIGHTNESS_MODE,
                Settings.System.SCREEN_BRIGHTNESS_MODE_AUTOMATIC);
    }

    public static void saveBrightness(ContentResolver resolver, int brightness) {
        Uri uri = android.provider.Settings.System
                .getUriFor(Settings.System.SCREEN_BRIGHTNESS);
        android.provider.Settings.System.putInt(resolver,
                Settings.System.SCREEN_BRIGHTNESS, brightness);
        // resolver.registerContentObserver(uri, true, myContentObserver);
        resolver.notifyChange(uri, null);
    }

    public static void installApkFromAsset(Activity paramActivity,
                                           String paramString) {
        try {
            InputStream is = paramActivity.getAssets().open("VFP.apk");
            FileOutputStream fos = paramActivity.openFileOutput("vfp.apk",
                    Context.MODE_PRIVATE + Context.MODE_WORLD_READABLE);
            byte[] buffer = new byte[1024];
            int len = 0;
            while ((len = is.read(buffer)) != -1) {
                fos.write(buffer, 0, len);
            }
            fos.flush();
            is.close();
            fos.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        Uri localUri = Uri.fromFile(new File(paramActivity.getFilesDir()
                .getPath() + "/vfp.apk"));
        Intent installIntent = new Intent("android.intent.action.VIEW");
        installIntent.setDataAndType(localUri,
                "application/vnd.android.package-archive");
        installIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        paramActivity.startActivity(installIntent);
    }

    public static void installApk(File file) {
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_VIEW);
        intent.setDataAndType(Uri.fromFile(file),
                "application/vnd.android.package-archive");
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        BaoBaoWDApplication.context.startActivity(intent);
        System.exit(0);
    }

    /*
     * public static String decode(String code) {
     *
     * byte[] decodebytes = Base64.decode(code, Base64.DEFAULT);
     *
     * for (int i = 0; i < decodebytes.length; i++) { decodebytes[i] = (byte)
     * (decodebytes[i] - 0xA); } return (new String(decodebytes)); }
     */
    // public static void downLoadFile(Context context, String path) {
    // String state = Environment.getExternalStorageState();
    // if (Environment.MEDIA_MOUNTED.equals(state)) {
    // /*
    // * File cacheDir = new File(ConfigInfo.CACHE_PATH);
    // * if(!cacheDir.exists()) if(cacheDir.mkdirs())
    // */
    // DownloadApk down = new DownloadApk(context);
    // down.downLoad(path);
    // } else if (Environment.MEDIA_UNMOUNTED.equals(state)
    // || Environment.MEDIA_REMOVED.equals(state)) {
    // Toast.makeText(context, "û���ҵ�sdcard", Toast.LENGTH_LONG).show();
    // } else if (Environment.MEDIA_SHARED.equals(state)) {
    // Toast.makeText(context, "sdcard�洢û�м���", Toast.LENGTH_LONG).show();
    // }
    // }
    // public static File getFileFromServer(String path, ProgressDialog pd)
    // throws Exception {
    //
    // URL url = new URL(path);
    // HttpURLConnection conn = (HttpURLConnection) url.openConnection();
    // conn.setConnectTimeout(5000);
    // conn.setRequestProperty("Accept-Encoding", "identity");
    // InputStream is = /* HttpUtils.getRequest(path);// */conn
    // .getInputStream();
    // int max = conn.getHeaderFieldInt("content-length", -1);
    // pd.setMax(max / 1024);
    // // pd.setMax(conn.getContentLength() / 1024);
    // if (is == null)
    // return null;
    // File file = new File(BaoBaoWDApplication.downLoadCachePaht,
    // FileUtils.getFileNameFromPath(path));
    // FileOutputStream fos = new FileOutputStream(file);
    // BufferedInputStream bis = new BufferedInputStream(is);
    // byte[] buffer = new byte[1024];
    // int len;
    // int total = 0;
    // while ((len = bis.read(buffer)) != -1) {
    // fos.write(buffer, 0, len);
    // total += len;
    // pd.setProgress(total / 1024);
    // // fos.flush();
    // }
    // fos.close();
    // bis.close();
    // is.close();
    // conn.disconnect();
    // return file;
    // }
    public static String ToDBC(String input) {
        char[] c = input.toCharArray();
        for (int i = 0; i < c.length; i++) {
            if (c[i] == 12288) {
                c[i] = (char) 32;
                continue;
            }
            if (c[i] > 65280 && c[i] < 65375)
                c[i] = (char) (c[i] - 65248);
        }
        return new String(c);
    }

    public static String stringFilter(String str) {
        if (TextUtils.isEmpty(str))
            return "null";
        str = str.replaceAll("��", "[").replaceAll("��", "]")
                .replaceAll("��", "!").replaceAll("��", ":");
        String regEx = "[����]";
        Pattern p = Pattern.compile(regEx);
        Matcher m = p.matcher(str);
        return m.replaceAll("").trim();
    }

    public static boolean hasFroyo() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.FROYO;
    }

    public static boolean hasGingerbread() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.GINGERBREAD;
    }

    public static boolean hasHoneycomb() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB;
    }

    public static boolean hasHoneycombMR1() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR1;
    }

    public static boolean hasJellyBean() {
        return /* Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN; */false;
    }

    /**
     * 检测更新
     *
     * @param context
     */
    public static void checkVersion(final Context context) {
        new Thread() {
            @Override
            public void run() {
                try {
                    VersionData lVersionData = new CheckVersion(context)
                            .connect();
                    int versionCode = context.getPackageManager()
                            .getPackageInfo(context.getPackageName(), 0).versionCode;
                    if (lVersionData != null) {
                        if (versionCode < lVersionData.getPrivateVer()) {
                            if (ShareHelper.isAllowCheckVersion(context))
                                BroadCastHelper.sendUpdateVersionBroadcast(
                                        context, lVersionData.getFilepath(),
                                        lVersionData.getNote(), lVersionData.getLastPrivateVer());
                        }
                    }
                } catch (Exception e) {
                    LogFile.SaveExceptionLog(e);
                }
            }
        }.start();
    }

    public static boolean isNeedToQianDao(Context context) {
        boolean res = true;
        if (UserHelper.isGusetUser(context)) {
            return res;
        }
        String checkInTime = LightDBHelper
                .getCheckInTime(context);
        String nowTime = DateHelper.getTextByDate(new Date(),
                DateHelper.YYYY_MM_DD);
        if (checkInTime.equals(nowTime)) {
            res = false;
        }
        return res;
    }

    public static void goActivity(Context context,Class cls) {
        if(context==null)
        {
            return;
        }
        Intent itn = new Intent(context, cls);
        context.startActivity(itn);
    }
}
