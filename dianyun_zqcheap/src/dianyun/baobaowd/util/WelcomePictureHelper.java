package dianyun.baobaowd.util;

import android.content.Context;
import dianyun.baobaowd.data.WelcomePicture;
import dianyun.baobaowd.db.TableConstants;
import dianyun.baobaowd.db.WelcomePicDBHelper;
import dianyun.baobaowd.help.LogFile;

/**
 * 联系人数据库操作辅助
 * 
 */
public class WelcomePictureHelper
{

	
	
	


	
	public static WelcomePicture getWelcomePicture(Context context)
	{
		WelcomePicDBHelper lWelcomePictureDBHelper = null;
		try
		{
			lWelcomePictureDBHelper = new WelcomePicDBHelper(context, TableConstants.TABLE_WELCOMEPICTURE);
			return lWelcomePictureDBHelper.getWelcomePicture();
		}
		catch (Exception e)
		{
			LogFile.SaveExceptionLog(e);
		}
		finally
		{
			if (null != lWelcomePictureDBHelper)
				lWelcomePictureDBHelper.closeDB();
		}
		return null;
	}
	public static void deleteWelcomePicture(Context context)
	{
		WelcomePicDBHelper lWelcomePictureDBHelper = null;
		try
		{
			lWelcomePictureDBHelper = new WelcomePicDBHelper(context, TableConstants.TABLE_WELCOMEPICTURE);
			lWelcomePictureDBHelper.deleteWelcomePicture();
		}
		catch (Exception e)
		{
			LogFile.SaveExceptionLog(e);
		}
		finally
		{
			if (null != lWelcomePictureDBHelper)
				lWelcomePictureDBHelper.closeDB();
		}
	}

	

	public static long addWelcomePicture(Context context,WelcomePicture welcomePicture)
	{
		WelcomePicDBHelper lWelcomePictureDBHelper = null;
		try
		{
			lWelcomePictureDBHelper = new WelcomePicDBHelper(context, TableConstants.TABLE_WELCOMEPICTURE);
			if(welcomePicture!=null){
					lWelcomePictureDBHelper.insert(welcomePicture);
			}
		
		}
		catch (Exception e)
		{
			LogFile.SaveExceptionLog(e);
		}
		finally
		{
			if (null != lWelcomePictureDBHelper){
				lWelcomePictureDBHelper.closeDB();
			}
		}
		return -1;
	}
	
	

}
