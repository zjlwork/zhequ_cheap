package dianyun.baobaowd.util;

import java.util.List;

import android.content.Context;
import dianyun.baobaowd.db.MenuDBHelper;
import dianyun.baobaowd.db.TableConstants;
import dianyun.baobaowd.entity.Menu;
import dianyun.baobaowd.help.LogFile;

/**
 * 联系人数据库操作辅助
 * 
 */
public class MenuHelper
{


	


//	public static boolean isExist(Context context, Menu menu)
//	{
//		MenuDBHelper lMenuDBHelper = null;
//		try
//		{
//			lMenuDBHelper = new MenuDBHelper(context, TableConstants.TABLE_SHOP_MENU);
//			return lMenuDBHelper.isExist(menu.cateId);
//		}
//		catch (Exception e)
//		{
//			LogFile.SaveExceptionLog(e);
//		}
//		finally
//		{
//			if (null != lMenuDBHelper)
//				lMenuDBHelper.closeDB();
//		}
//		return false;
//	}

	
	public static void deleteMenuListByParntId(Context context, long parentId)
	{
		MenuDBHelper lMenuDBHelper = null;
		try
		{
			lMenuDBHelper = new MenuDBHelper(context, TableConstants.TABLE_SHOP_MENU);
			lMenuDBHelper.deleteMenuByParentId(parentId);
		}
		catch (Exception e)
		{
			LogFile.SaveExceptionLog(e);
		}
		finally
		{
			if (null != lMenuDBHelper)
				lMenuDBHelper.closeDB();
		}
	}

	
//	public static long addMenu(Context context, Menu menu)
//	{
//		MenuDBHelper lMenuDBHelper = null;
//		try
//		{
//			lMenuDBHelper = new MenuDBHelper(context, TableConstants.TABLE_SHOP_MENU);
//			return lMenuDBHelper.insert(menu);
//		}
//		catch (Exception e)
//		{
//			LogFile.SaveExceptionLog(e);
//		}
//		finally
//		{
//			if (null != lMenuDBHelper)
//				lMenuDBHelper.closeDB();
//		}
//		return -1;
//	}
	public static long addMenuList(Context context, List<Menu> menuList,long parentId)
	{
		MenuDBHelper lMenuDBHelper = null;
		try
		{
			lMenuDBHelper = new MenuDBHelper(context, TableConstants.TABLE_SHOP_MENU);
			if(menuList!=null&&menuList.size()>0){
				lMenuDBHelper.beginTransaciton();
				for(Menu menu:menuList){
					menu.parentId = parentId;
					lMenuDBHelper.insert(menu);
				}
				lMenuDBHelper.setTransactionSuccessful();
			}
		
		}
		catch (Exception e)
		{
			LogFile.SaveExceptionLog(e);
		}
		finally
		{
			if (null != lMenuDBHelper){
				lMenuDBHelper.endTransaction();
				lMenuDBHelper.closeDB();
			}
		}
		return -1;
	}
	public static List<Menu> getMenuListByparentId(Context context, long parentId)
	{
		MenuDBHelper lMenuDBHelper = null;
		try
		{
			lMenuDBHelper = new MenuDBHelper(context, TableConstants.TABLE_SHOP_MENU);
			return lMenuDBHelper.getMenuByParentId(parentId);
			
		}
		catch (Exception e)
		{
			LogFile.SaveExceptionLog(e);
		}
		finally
		{
			if (null != lMenuDBHelper){
				lMenuDBHelper.closeDB();
			}
		}
		return null;
	}
	

//	public static void updateMenu(Context context, Menu menu)
//	{
//		MenuDBHelper lMenuDBHelper = null;
//		try
//		{
//			lMenuDBHelper = new MenuDBHelper(context, TableConstants.TABLE_SHOP_MENU);
//			lMenuDBHelper.update(menu);
//		}
//		catch (Exception e)
//		{
//			LogFile.SaveExceptionLog(e);
//		}
//		finally
//		{
//			if (null != lMenuDBHelper)
//				lMenuDBHelper.closeDB();
//		}
//	}

	

}
