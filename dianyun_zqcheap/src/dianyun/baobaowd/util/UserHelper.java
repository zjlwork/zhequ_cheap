package dianyun.baobaowd.util;

import java.util.Date;
import java.util.HashMap;
import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;

import dianyun.baobaowd.data.User;
import dianyun.baobaowd.db.TableConstants;
import dianyun.baobaowd.db.UserDBHelper;
import dianyun.baobaowd.dto.ResultDTO;
import dianyun.baobaowd.gson.GsonHelper;
import dianyun.baobaowd.help.LogFile;
import dianyun.baobaowd.serverinterface.GetGuestUserInfo;
import dianyun.baobaowd.serverinterface.UserSet;
import dianyun.baobaowd.xiaomipush.RegisterPushHelper;
import dianyun.zqcheap.R;
import dianyun.zqcheap.activity.LoginRegisterActivity;
import dianyun.zqcheap.application.BaoBaoWDApplication;

/**
 * 联系人数据库操作辅助
 * 
 */
public class UserHelper
{

	public static final String DEFAULTTOPIC = "fanli_topic";
	public static final String ALIASSUFFIX = "fanli_";
	public static HashMap<Long, Byte> mAttentionUserHashMap = new HashMap<Long, Byte>();
	
	private static User mUser;
	
	
	
	public static User getUser()
	{
		if(mUser==null){
			mUser = UserHelper.getMeUser(BaoBaoWDApplication.context);
			if (mUser == null)
				mUser = UserHelper.getGuestUser(BaoBaoWDApplication.context);
		}
		return mUser;
	}
	public static void refreshUser(){
		mUser = UserHelper.getMeUser(BaoBaoWDApplication.context);
		if (mUser == null)
			mUser = UserHelper.getGuestUser(BaoBaoWDApplication.context);
	}
	
	public static void setUser(User user)
	{
		mUser = user;
		if (!UserHelper.isExist(BaoBaoWDApplication.context, user))
			UserHelper.addUser(BaoBaoWDApplication.context, user);
		else
			UserHelper.updateUser(BaoBaoWDApplication.context, user);
	}
	public static void insertOrUpdateUser(User user)
	{
		if (!UserHelper.isExist(BaoBaoWDApplication.context, user))
			UserHelper.addUser(BaoBaoWDApplication.context, user);
		else
			UserHelper.updateUser(BaoBaoWDApplication.context, user);
	}
	public static void logoutUser(){
		mUser = null;
	}

	
	
	public static int  getBBStatus(String birthday){
		if(!TextUtils.isEmpty(birthday)){
			String currentTime = DateHelper.getTextByDate(new Date(), DateHelper.YYYY_MM_DD);
			if (birthday.compareTo(currentTime) >= 0)
				return GobalConstants.DayArticleStatus.PREGNANCY;
			else
				return GobalConstants.DayArticleStatus.HASCHILD;
		}else
			return GobalConstants.DayArticleStatus.PREPARE;
	}
	public static String  getBBStatusStr(int status){
		if(status==GobalConstants.DayArticleStatus.HASCHILD){
			return "宝宝圈";
		}else if(status==GobalConstants.DayArticleStatus.PREGNANCY){
			return "预产圈";
		}else{
			return "备孕圈";
		}
	}
	
	
	public static void initAttentionUserHashMap(Context context)
	{/*
		List<AttentionUser> attentionUserList = AttentionUserHelper
				.getAttentionUserList(context);
		if (attentionUserList != null && attentionUserList.size() > 0)
		{
			for (AttentionUser attentionUser : attentionUserList)
			{
				mAttentionUserHashMap.put(attentionUser.getToUid(),
						attentionUser.getStatus());
			}
		}
	*/}

	public static void addGusetUser(Context context){
		User mGuestUser = UserHelper.getGuestUser(context);
		if (mGuestUser == null)
		{
			mGuestUser = new User(GobalConstants.GUESTUSERUID,
					context.getString(R.string.guestnickname),
					GobalConstants.GUESTUSERTOKEN);
			mGuestUser.setIsGuest(GobalConstants.AllStatusYesOrNo.YES);
			addUser(context, mGuestUser);
		}
	}
	
	
	
	public static void clearAttentionUsers(){
		mAttentionUserHashMap.clear();
	}
	public static boolean containsKey(Long uid)
	{
		return mAttentionUserHashMap.containsKey(uid);
	}

	public static void removeByKey(Long uid)
	{
		mAttentionUserHashMap.remove(uid);
	}

	public static void putByKey(Long uid, Byte status)
	{
		mAttentionUserHashMap.put(uid, status);
	}

	

	
	public static void insertOrUpdateUser(UserDBHelper lUserDBHelper, User user)
	{
		User lUser = getUserByUid(lUserDBHelper, user.getUid());
		if (lUser != null)
		{
			user.setIsSelf(lUser.getIsSelf());
			user.setToken(lUser.getToken());
			lUserDBHelper.update(user);
		}
		else
		{
			lUserDBHelper.insert(user);
		}
	}

	
	public static void setUserNameTv(User user, Context context,
			TextView usernameTv)
	{
		usernameTv.setText("");
		String username = user.getNickname();
		

		if (!TextUtils.isEmpty(username))
		{
			username=username.replaceAll("\r", "");
			username=username.replaceAll("\n", "");
			if (username.length() >= 7)
				username = username.substring(0, 7);
			usernameTv.setText(username);
		}
		Long uid = user.getUid();
		if(uid!=null){
			if (uid >= 10000 && uid < 20000)
				usernameTv.setTextColor(context.getResources().getColor(
						R.color.topbgcolor));
			else
				usernameTv.setTextColor(context.getResources().getColor(
						R.color.nametext));
		}
	}

	
	public static void setStatusTv(String perinatal, Context context,
			TextView statusTv)
	{
		if (TextUtils.isEmpty(perinatal))
		{
			statusTv.setText(context.getString(R.string.prepare));
		}
		else
		{
			String currentTime = DateHelper.getTextByDate(new Date(),
					DateHelper.YYYY_MM_DD);
			statusTv.setText(DateHelper.getRolePerinatalTime(perinatal,
					currentTime));
		}
	}

	public static void setPersonalCenterStatusTv(String perinatal, Byte gender,
			Context context, TextView statusTv)
	{
		if (TextUtils.isEmpty(perinatal))
		{
			statusTv.setText(context.getString(R.string.prepare));
		}
		else
		{
			String currentTime = DateHelper.getTextByDate(new Date(),
					DateHelper.YYYY_MM_DD);
			String per = DateHelper
					.getRolePerinatalTime(perinatal, currentTime);
			if (perinatal.compareTo(currentTime) < 0)
			{
				if (gender != null
						&& gender.equals(GobalConstants.Gender.FATHER))
				{
					per += "(男孩)";
				}
				else
				{
					per += "(女孩)";
				}
			}
			statusTv.setText(per);
		}
	}

	public static void setBBGenderIv(Context context, User user,
			ImageView genderIv)
	{
		String perinatal = user.getBabyBirthday();
		if (TextUtils.isEmpty(perinatal))
			genderIv.setVisibility(View.GONE);
		else
		{
			String currentTime = DateHelper.getTextByDate(new Date(),
					DateHelper.YYYY_MM_DD);
			if (perinatal.compareTo(currentTime) <= 0)
			{
				genderIv.setVisibility(View.VISIBLE);
				Byte gender = user.getBabyGender();
				if (gender != null
						&& gender.equals(GobalConstants.Gender.FATHER))
				{
					genderIv.setImageResource(R.drawable.father);
				}
				else
				{
					genderIv.setImageResource(R.drawable.mother);
				}
			}
			else
			{
				genderIv.setVisibility(View.GONE);
			}
		}
	}

	/*
	 * public static void setMedalTv(Context context,List<Integer>
	 * medalIdList,TextView medalTv){ if
	 * (medalIdList!=null&&medalIdList.size()>0) { Integer medalId =
	 * medalIdList.get(0); if(medalId == GobalConstants.QuestionMedal.QT){
	 * medalTv.setText(context.getString(R.string.medal_qt)); }else if(medalId
	 * == GobalConstants.QuestionMedal.BY){
	 * medalTv.setText(context.getString(R.string.medal_by)); }else if(medalId
	 * == GobalConstants.QuestionMedal.HJ){
	 * medalTv.setText(context.getString(R.string.medal_hj)); }else if(medalId
	 * == GobalConstants.QuestionMedal.BJ){
	 * medalTv.setText(context.getString(R.string.medal_bj)); }else if(medalId
	 * == GobalConstants.QuestionMedal.ZS){
	 * medalTv.setText(context.getString(R.string.medal_zs)); }else {
	 * medalTv.setText(context.getString(R.string.medal_qt)); } } }
	 */
	public static void setAddressTv(String address, TextView addressTv)
	{
		
		try{
			if (!TextUtils.isEmpty(address))
			{
				addressTv.setText(address.split("-")[1]);
			}
			else
			{
				addressTv.setText("未知");
			}
		}catch(Exception e){
			
		}
		
		
	}

	
	//  浙江省-杭州市
//	public static String getProvinceCity(String address)
//	{
//		if (!TextUtils.isEmpty(address))
//		{
//			return address.split("-")[0]+"-"+address.split("-")[1];
//		}
//		else
//		{
//			return null;
//		}
//	}
	//  杭州市
	public static String getCityStr(String address)
	{
		if (!TextUtils.isEmpty(address))
		{
			return address.split("-")[1];
		}
		else
		{
			return null;
		}
	}

	public static void setLevImg(int lev, ImageView imageView)
	{
		if (lev == 0 || lev == 1)
			imageView.setImageResource(R.drawable.level_1);
		else if (lev == 2)
			imageView.setImageResource(R.drawable.level_2);
		else if (lev == 3)
			imageView.setImageResource(R.drawable.level_3);
		else if (lev == 4)
			imageView.setImageResource(R.drawable.level_4);
		else if (lev == 5)
			imageView.setImageResource(R.drawable.level_5);
		else if (lev == 6)
			imageView.setImageResource(R.drawable.level_6);
		else if (lev == 7)
			imageView.setImageResource(R.drawable.level_7);
		else if (lev == 8)
			imageView.setImageResource(R.drawable.level_8);
		else if (lev == 9)
			imageView.setImageResource(R.drawable.level_9);
		else if (lev == 10)
			imageView.setImageResource(R.drawable.level_10);
		else if (lev == 11)
			imageView.setImageResource(R.drawable.level_11);
		else if (lev == 12)
			imageView.setImageResource(R.drawable.level_12);
		else if (lev == 13)
			imageView.setImageResource(R.drawable.level_13);
		else if (lev == 14)
			imageView.setImageResource(R.drawable.level_14);
		else if (lev == 15)
			imageView.setImageResource(R.drawable.level_15);
		else if (lev == 16)
			imageView.setImageResource(R.drawable.level_16);
		else if (lev == 17)
			imageView.setImageResource(R.drawable.level_17);
		else if (lev == 18)
			imageView.setImageResource(R.drawable.level_18);
		else if (lev == 19)
			imageView.setImageResource(R.drawable.level_19);
		else if (lev == 20)
			imageView.setImageResource(R.drawable.level_20);
		else if (lev == 21)
			imageView.setImageResource(R.drawable.level_21);
		else if (lev == 22)
			imageView.setImageResource(R.drawable.level_22);
		else if (lev == 23)
			imageView.setImageResource(R.drawable.level_23);
		else if (lev == 24)
			imageView.setImageResource(R.drawable.level_24);
		else if (lev == 25)
			imageView.setImageResource(R.drawable.level_25);
		else
			imageView.setImageResource(R.drawable.level_25);
	}

	public static void guestCopyUser(Context context, User user)
	{
		User guestUser = UserHelper.getGuestUser(context);
		if (guestUser != null)
		{
			guestUser.setGender(user.getGender());
			guestUser.setBabyGender(user.getBabyGender());
			guestUser.setBabyBirthday(user.getBabyBirthday());
			guestUser.setNickname(user.getNickname());
			guestUser.setCity(user.getCity());
			UserHelper.updateUser(context, guestUser);
		}
	}

	public static void goPersonCenterActivity(Context context, User user)
	{/*
		Intent lIntent = new Intent(context, PersonCenterActivity.class);
		lIntent.putExtra(GobalConstants.Data.USER, user);
		context.startActivity(lIntent);
	*/}

	public static int getCurrentWeek(Context context)
	{
		User user = getUser();
		double perinatalTime = DateHelper.getDateByPattern(user.getPerinatal(),
				DateHelper.YYYY_MM_DD).getTime();
		double nowTime = new Date().getTime();
		double allDaysTime = 280d * 24d * 60d * 60d * 1000d;
		double servenDaysTime = 7d * 24d * 60d * 60d * 1000d;
		double temp = (allDaysTime - (perinatalTime - nowTime))
				/ (servenDaysTime);
		int currentWeek = (int) Math.ceil(temp);
		if (currentWeek <= 0)
			currentWeek = 1;
		if (currentWeek > 40)
			currentWeek = 40;
		return currentWeek;
	}

	public static int getCurrentWeek(String perinatal)
	{
		double perinatalTime = DateHelper.getDateByPattern(perinatal,
				DateHelper.YYYY_MM_DD).getTime();
		double nowTime = new Date().getTime();
		double allDaysTime = 280d * 24d * 60d * 60d * 1000d;
		double servenDaysTime = 7d * 24d * 60d * 60d * 1000d;
		double temp = (allDaysTime - (perinatalTime - nowTime))
				/ (servenDaysTime);
		int currentWeek = (int) Math.ceil(temp);
		if (currentWeek <= 0)
			currentWeek = 1;
		if (currentWeek > 40)
			currentWeek = 40;
		return currentWeek;
	}

	public static String getMenstrualStr(String perinatal)
	{
		long perinatalTime = DateHelper.getDateByPattern(perinatal,
				DateHelper.YYYY_MM_DD).getTime();
		long menstrualTime = perinatalTime - 280L * 24L * 60L * 60L * 1000L;
		Date menstrualDate = new Date(menstrualTime);
		return DateHelper.getTextByDate(menstrualDate, DateHelper.YYYY_MM_DD);
	}

	public static boolean isExist(Context context, User user)
	{
		UserDBHelper lUserDBHelper = null;
		try
		{
			lUserDBHelper = new UserDBHelper(context, TableConstants.TABLE_USER);
			return lUserDBHelper.isExist(user.getUid());
		}
		catch (Exception e)
		{
			LogFile.SaveExceptionLog(e);
		}
		finally
		{
			if (null != lUserDBHelper)
				lUserDBHelper.closeDB();
		}
		return false;
	}

	
	public static void deleteUser(Context context, User user)
	{
		UserDBHelper lUserDBHelper = null;
		try
		{
			lUserDBHelper = new UserDBHelper(context, TableConstants.TABLE_USER);
			lUserDBHelper.delete(user);
		}
		catch (Exception e)
		{
			LogFile.SaveExceptionLog(e);
		}
		finally
		{
			if (null != lUserDBHelper)
				lUserDBHelper.closeDB();
		}
	}

	
	public static long addUser(Context context, User user)
	{
		UserDBHelper lUserDBHelper = null;
		try
		{
			lUserDBHelper = new UserDBHelper(context, TableConstants.TABLE_USER);
			return lUserDBHelper.insert(user);
		}
		catch (Exception e)
		{
			LogFile.SaveExceptionLog(e);
		}
		finally
		{
			if (null != lUserDBHelper)
				lUserDBHelper.closeDB();
		}
		return -1;
	}
	

	public static void updateUser(Context context, User user)
	{
		UserDBHelper lUserDBHelper = null;
		try
		{
			lUserDBHelper = new UserDBHelper(context, TableConstants.TABLE_USER);
			lUserDBHelper.update(user);
		}
		catch (Exception e)
		{
			LogFile.SaveExceptionLog(e);
		}
		finally
		{
			if (null != lUserDBHelper)
				lUserDBHelper.closeDB();
		}
	}

	public static User getMeUser(Context context)
	{
		UserDBHelper lUserDBHelper = null;
		try
		{
			lUserDBHelper = new UserDBHelper(context, TableConstants.TABLE_USER);
			return lUserDBHelper.getMeUser(context);
		}
		catch (Exception e)
		{
			LogFile.SaveExceptionLog(e);
		}
		finally
		{
			if (null != lUserDBHelper)
				lUserDBHelper.closeDB();
		}
		return null;
	}

	public static User getGuestUser(Context context)
	{
		UserDBHelper lUserDBHelper = null;
		try
		{
			lUserDBHelper = new UserDBHelper(context, TableConstants.TABLE_USER);
			return lUserDBHelper.getGuestUser(context);
		}
		catch (Exception e)
		{
			LogFile.SaveExceptionLog(e);
		}
		finally
		{
			if (null != lUserDBHelper)
				lUserDBHelper.closeDB();
		}
		return null;
	}
	public static List<User> getUserListByUids(Context context,String uids)
	{
		UserDBHelper lUserDBHelper = null;
		try
		{
			lUserDBHelper = new UserDBHelper(context, TableConstants.TABLE_USER);
			return lUserDBHelper.getUserListByUid(uids);
		}
		catch (Exception e)
		{
			LogFile.SaveExceptionLog(e);
		}
		finally
		{
			if (null != lUserDBHelper)
				lUserDBHelper.closeDB();
		}
		return null;
	}

	public static User getUserByUid(UserDBHelper lUserDBHelper, long uid)
	{
		User user = lUserDBHelper.getBaseUserByUid(uid);

		return user;
	}

	/*public static String getUserTopicByBirthday(User user)
	{
		Date date = DateHelper.getDateByPattern(user.getBabyBirthday(),
				DateHelper.YYYY_MM_DD);
		Long nowtimeLong = new Date().getTime();
		if (date == null)
			return TOPICSUFFIX + 1;
		Long perinatalLong = DateHelper.getDateByPattern(
				user.getBabyBirthday(), DateHelper.YYYY_MM_DD).getTime();
		Long dValue = Math.abs(perinatalLong - nowtimeLong);
		long week = dValue / (60 * 60 * 1000 * 24 * 7);
		return TOPICSUFFIX + (week + 2);
	}*/

	public static void showUserAvatar(Context context, User mUser,
			final ImageView mUserAvatarIv)
	{
		mUserAvatarIv.setImageResource(R.drawable.defaultavatar);
		if (mUser.getProfileImage() != null
				&& !mUser.getProfileImage().equals(""))
		{
			DisplayImageOptions options = new DisplayImageOptions.Builder()
					.showImageOnLoading(R.drawable.defaultavatar)
					.showImageForEmptyUri(R.drawable.defaultavatar)
					.showImageOnFail(R.drawable.defaultavatar)
					.cacheInMemory(true).cacheOnDisk(true)
					.considerExifParams(true).imageScaleType(ImageScaleType.EXACTLY_STRETCHED)
					.displayer(new RoundedBitmapDisplayer(360))
					.bitmapConfig(Bitmap.Config.RGB_565).build();
			ImageLoader.getInstance().displayImage(mUser.getProfileImage(),
					mUserAvatarIv, options);
			// mUserAvatarIv.setTag(R.id.smallPath, mUser.getProfileImage());
			// BaoBaoWDApplication.mImageLoader.loadDrawable(
			// mUser.getProfileImage(), mUserAvatarIv, true, 360);
		}
	}
	public static void showUserAvatar2(Context context, User mUser,
			final ImageView mUserAvatarIv)
	{
		if (mUser.getProfileImage() != null
				&& !mUser.getProfileImage().equals(""))
		{
			DisplayImageOptions options = new DisplayImageOptions.Builder()
			.cacheInMemory(true).cacheOnDisk(true)
			.considerExifParams(true).imageScaleType(ImageScaleType.EXACTLY_STRETCHED)
			.bitmapConfig(Bitmap.Config.RGB_565).build();
			ImageLoader.getInstance().displayImage(mUser.getProfileImage(),
					mUserAvatarIv, options);
			
		}
	}

	public static class LoadRoundImage extends AsyncTask<Void, Void, Bitmap>
	{

		private ImageView mImageView;
		private Bitmap mBitmap;

		public LoadRoundImage(ImageView img, Bitmap bitmap)
		{
			mImageView = img;
			mBitmap = bitmap;
		}

		@Override
		protected Bitmap doInBackground(Void... params)
		{
			Bitmap res = null;
			if (mImageView != null && mBitmap != null)
			{
				res = ImageHelper.getRoundCornerBitmap(mBitmap,
						mImageView.getLayoutParams().width,
						mImageView.getLayoutParams().height, 360);
			}
			return res;
		}

		@Override
		protected void onPostExecute(Bitmap result)
		{
			if (mImageView != null && result != null)
			{
				mImageView.setImageBitmap(result);
			}
			super.onPostExecute(result);
		}
	}


	public static void showUserBg(Context context, User mUser,
			final ImageView mPcbgIv)
	{
		if (mUser.getBgImageUrl() != null)
		{
				DisplayImageOptions options = new DisplayImageOptions.Builder()
//				.showImageOnLoading(R.drawable.personalcenterbg)
				.showImageForEmptyUri(R.drawable.personalcenterbg)
				.showImageOnFail(R.drawable.personalcenterbg)
				.cacheInMemory(true).cacheOnDisk(true)
				.considerExifParams(true)
				.bitmapConfig(Bitmap.Config.RGB_565).build();
			
				ImageLoader.getInstance().displayImage(mUser.getBgImageUrl(),mPcbgIv,
						options);
			
		}
	}


	
	public static boolean isGusetUser(Context context)
	{
		User user = getUser();
		if (user!=null&&user.getIsGuest().equals(GobalConstants.AllStatusYesOrNo.YES))
			return true;
		return false;
	}

	public static void gusestUserGo(Context context)
	{
//		ToastHelper.showGoLoginDialog(context);
		Intent lIntent = new Intent(context,
				LoginRegisterActivity.class);
		context.startActivity(lIntent);
	}

	public static void getNetGuestUser(final Context context)
	{
		new Thread()
		{

			@Override
			public void run()
			{
				ResultDTO resultDTO = new GetGuestUserInfo(
						BaoBaoWDApplication.deviceId).postConnect();
				if (resultDTO != null && resultDTO.getCode().equals("0"))
				{
					User mGuestUser = GsonHelper.gsonToObj(
							resultDTO.getResult(), User.class);
					mGuestUser.setIsGuest(GobalConstants.AllStatusYesOrNo.YES);
					User guestUser = UserHelper.getGuestUser(context);
					mGuestUser.setBabyBirthday(guestUser.getBabyBirthday());
					mGuestUser.setBabyGender(guestUser.getBabyGender());
					ResultDTO userSetResultDTO = new UserSet(
							mGuestUser.getUid(), mGuestUser.getToken(),
							mGuestUser.getGender(), mGuestUser.getBabyGender(),
							mGuestUser.getBabyBirthday(),
							mGuestUser.getNickname(), mGuestUser.getCity())
							.postConnect();
					if (userSetResultDTO != null
							&& userSetResultDTO.getCode().equals("0"))
					{
						UserHelper.deleteUser(context, guestUser);
						UserHelper.addUser(context, mGuestUser);
						refreshUser();
						BroadCastHelper.sendRefreshMainBroadcast(context,
								GobalConstants.RefreshType.USER);
						RegisterPushHelper.logout(context);
						
					}
				}
			}
		}.start();
	}
	
	
	public static String getSameCity(String cityStr){
//		if(cityStr.endsWith("市"))
//		 cityStr = cityStr.substring(0,cityStr.lastIndexOf("市"));
		if(cityStr==null)return null;
		return cityStr+"同城";
	}
	public static String getSameAge(String bbbirthday){
		if(!TextUtils.isEmpty(bbbirthday)){
			String[] age = bbbirthday.split("-");
			String  year = age[0].substring(2);
			String month = age[1];
			String currentTime = DateHelper.getTextByDate(new Date(), DateHelper.YYYY_MM_DD);
			if (bbbirthday.compareTo(currentTime) >= 0)
				return year+"年"+month+"月"+"预产圈";
			else
				return year+"年"+month+"月"+"宝宝圈";
		}else
			return "备孕圈";
	}
	
	
	
	

}
