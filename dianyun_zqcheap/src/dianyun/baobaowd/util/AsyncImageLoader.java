package dianyun.baobaowd.util;

import java.lang.ref.ReferenceQueue;
import java.lang.ref.SoftReference;
import java.util.concurrent.ExecutorService;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.util.LruCache;
import android.util.Log;
import android.widget.ImageView;
import dianyun.zqcheap.R;

@SuppressLint("NewApi")
public class AsyncImageLoader
{

	private int mReqImageWidth;
	private int mReqImageHeight;
	private LruCache<String, SoftReference<Bitmap>> imageCache;
	private ReferenceQueue<Bitmap> rf;
	private ExecutorService executor = null;
	// LruCache<String, Bitmap> mMemCache;
	private final int ROLATEPIX = 10;
	private final int ANNATIONDURATION = 100;

	public AsyncImageLoader(Context context)
	{
		// executor = Executors.newFixedThreadPool(10);
		/*
		 * executor = new ThreadPoolExecutor(4, 10, 180L, TimeUnit.SECONDS, new
		 * LinkedBlockingQueue<Runnable>(), new ThreadFactory() { private final
		 * AtomicInteger mCount = new AtomicInteger(1);
		 * 
		 * @Override public Thread newThread(Runnable r) { try{ return new
		 * Thread(r, "loadimage"+ this.mCount.getAndIncrement());
		 * }catch(Exception e){ return new Thread(r, "loadimage"+
		 * this.mCount.getAndIncrement()); } } }, new
		 * ThreadPoolExecutor.DiscardOldestPolicy());
		 */
		mReqImageWidth = 62;// context.getResources().getDimensionPixelSize(
		// R.dimen.main_imge_width);
		mReqImageHeight = 62;// context.getResources().getDimensionPixelSize(
		// R.dimen.main_imge_height);
		int maxMemory = (int) (Runtime.getRuntime().maxMemory() / 1024);
		// 使用最大可用内存值的1/8作为缓存的大小。
		int cacheSize = maxMemory / 10;
		imageCache = new LruCache<String, SoftReference<Bitmap>>(cacheSize)
		{

			protected int sizeOf(String key, Bitmap bitmap)
			{
				return bitmap.getRowBytes() * bitmap.getHeight();
				// return bitmap.getByteCount() / 1024;
			}
		};
		rf = new ReferenceQueue<Bitmap>();
	}

	public void setReqWidth(int width)
	{
		mReqImageWidth = width;
		mReqImageHeight = 0;
	}

	public Bitmap loadDrawable(final String imageUrl,
			final ImageView imageView, final boolean isCorner,
			final int rotationPX)
	{
		if (imageCache.get(imageUrl) != null)
		{
			SoftReference<Bitmap> softReference = imageCache.get(imageUrl);
			Bitmap drawable = softReference.get();
			if (drawable != null)
			{
				showImage(drawable, imageView, imageUrl);
				return drawable;
			}
			else
			{
				imageCache.remove(imageUrl);
			}
		}
		new DownloadTask(imageUrl, imageView, "", null, 0, isCorner, rotationPX)
				.execute();
		// Looper.loop();
		return null;
	}

	public void loadNativeDrawable(final String imageUrl,
			final ImageView imageView, Point point)
	{
		new loadTask(imageView, imageUrl, point).execute();
		// Looper.loop();
	}

	public void clearCash()
	{
		if (imageCache != null)
		{
			imageCache.evictAll();
		}
	}

	public Bitmap loadDrawable(final String imageUrl,
			final ImageView imageView, Handler h, int pos)
	{
		if (imageCache.get(imageUrl) != null)
		{
			SoftReference<Bitmap> softReference = imageCache.get(imageUrl);
			Bitmap drawable = softReference.get();
			if (drawable != null)
			{
				showImage(drawable, imageView, imageUrl);
			}
			return drawable;
		}
		else
		{
			imageCache.remove(imageUrl);
		}
		new DownloadTask(imageUrl, imageView, "", h, pos, false, 0).execute();
		// Looper.loop();
		return null;
	}

	class loadTask extends AsyncTask<String, Void, Bitmap>
	{

		String mPath = "";
		Point mPoint = null;
		ImageView mImageView = null;

		public loadTask(final ImageView imgV, final String path,
				final Point Point)
		{
			mPath = path;
			mPoint = Point;
			mImageView = imgV;
		}

		private int computeScale(BitmapFactory.Options options, int viewWidth,
				int viewHeight)
		{
			int inSampleSize = 1;
			if (viewWidth == 0 || viewWidth == 0)
			{
				return inSampleSize;
			}
			int bitmapWidth = options.outWidth;
			int bitmapHeight = options.outHeight;
			// ����Bitmap�Ŀ�Ȼ�߶ȴ��������趨ͼƬ��View�Ŀ�ߣ���������ű���
			if (bitmapWidth > viewWidth || bitmapHeight > viewWidth)
			{
				int widthScale = Math.round((float) bitmapWidth
						/ (float) viewWidth);
				int heightScale = Math.round((float) bitmapHeight
						/ (float) viewWidth);
				// Ϊ�˱�֤ͼƬ�����ű��Σ�����ȡ��߱�����С���Ǹ�
				inSampleSize = widthScale < heightScale ? widthScale
						: heightScale;
			}
			return inSampleSize;
		}

		private Bitmap decodeThumbBitmapForFile(String path, int viewWidth,
				int viewHeight)
		{
			BitmapFactory.Options options = new BitmapFactory.Options();
			// ����Ϊtrue,��ʾ����Bitmap���󣬸ö���ռ�ڴ�
			options.inJustDecodeBounds = true;
			BitmapFactory.decodeFile(path, options);
			// �������ű���
			options.inSampleSize = computeScale(options, viewWidth, viewHeight);
			// ����Ϊfalse,����Bitmap������뵽�ڴ���
			options.inJustDecodeBounds = false;
			return BitmapFactory.decodeFile(path, options);
		}

		@Override
		protected Bitmap doInBackground(String... params)
		{
			Bitmap mBitmap = decodeThumbBitmapForFile(mPath, mPoint == null ? 0
					: mPoint.x, mPoint == null ? 0 : mPoint.y);
			return mBitmap;
		}

		@Override
		protected void onPostExecute(Bitmap result)
		{
			if (result != null)
			{
				mImageView.setImageBitmap(result);
			}
			super.onPostExecute(result);
		}
	}

	class DownloadTask extends AsyncTask<String, Void, Bitmap>
	{

		DownloadTask(String imageUrl, ImageView imageView, String id,
				Handler imageHandler, int pos, final boolean isCorner,
				final int rotationPX)
		{
			this.imageUrl = imageUrl;
			this.id = id;
			this.imageView = imageView;
			mImageHandler = imageHandler;
			mPos = pos;
			isToCorner = isCorner;
			mRotationPX = rotationPX;
		}
		private boolean isToCorner = false;
		private int mRotationPX = 0;
		private String imageUrl;
		private String id;
		private ImageView imageView;
		Handler mImageHandler;
		private int mPos;

		@Override
		protected Bitmap doInBackground(String... params)
		{
			// Log.e("getImage", imageCache.containsKey(imageUrl) + "--"
			// + imageUrl + "--size:" + imageCache.size());
			if (imageCache.get(imageUrl) != null)
			{
				SoftReference<Bitmap> softReference = imageCache.get(imageUrl);
				Bitmap drawable = softReference.get();
				if (drawable != null)
				{
					// Message message = mImageHandler.obtainMessage(0,
					// drawable);
					// mImageHandler.sendMessage(message);
					return drawable;
				}
				else
				{
					imageCache.remove(imageUrl);
				}
			}
			Bitmap bitmap = null;
			try
			{
				if (bitmap == null)
				{
					bitmap = ImageUtil.processBitmap(imageUrl,
							imageView.getLayoutParams().width,
							imageView.getLayoutParams().height, id);
					if (bitmap != null)
					{
						if (isToCorner)
						{
							bitmap = ImageHelper.getRoundCornerBitmap(bitmap,
									imageView.getLayoutParams().width,
									imageView.getLayoutParams().height,
									mRotationPX);
						}
						imageCache.put(imageUrl, new SoftReference<Bitmap>(
								bitmap, rf));
						// Message message = mImageHandler.obtainMessage(0,
						// bitmap);
						// mImageHandler.sendMessage(message);
					}
				}
			}
			catch (OutOfMemoryError e)
			{
				e.printStackTrace();
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
			return bitmap;
		}

		@Override
		protected void onPostExecute(Bitmap result)
		{
			if (result != null)
			{
				if (mImageHandler == null)
				{
					showImage(result, imageView, imageUrl);
				}
				else
				{
					// mCallback.imageLoaded(result);
					Message message = mImageHandler.obtainMessage(0, result);
					message.arg1 = mPos;
					Bundle bundle = new Bundle();
					bundle.putString("path", imageUrl);
					message.setData(bundle);
					mImageHandler.sendMessage(message);
				}
			}
			super.onPostExecute(result);
		}
	}

	private void showImage(Bitmap result, ImageView imageView, String imgUrl)
	{
		if (imageView != null)
		{
			Log.i("showImage", "tag:"
					+ (imageView.getTag(R.id.smallPath) == null ? "null"
							: imageView.getTag(R.id.smallPath).toString())
					+ "---" + imgUrl);
			if (imageView.getTag(R.id.smallPath) != null
					&& imageView.getTag(R.id.smallPath).toString()
							.equals(imgUrl))
			{
				imageView.setImageBitmap(result);
				// Bitmap bitmap = result;
				// BitmapDrawable bitmapDrawable = new BitmapDrawable(
				// BaoBaoWDApplication.context.getResources(), bitmap);
				// final TransitionDrawable td = new TransitionDrawable(
				// new Drawable[] {
				// new ColorDrawable(android.R.color.transparent),
				// bitmapDrawable });
				// imageView.setImageDrawable(td);
				// td.startTransition(ANNATIONDURATION);
				// } else if (imageView.getTag() == null) {
				// // imageView.setImageBitmap(result);
				// Bitmap bitmap =result;
				//
				// // Bitmap bitmap = (ImageHelper.getRoundCornerBitmap(result,
				// // imageView.getLayoutParams().width,
				// // imageView.getLayoutParams().height, ROLATEPIX));
				//
				// BitmapDrawable bitmapDrawable = new BitmapDrawable(
				// BaoBaoWDApplication.context.getResources(), bitmap);
				// final TransitionDrawable td = new TransitionDrawable(
				// new Drawable[] {
				// new ColorDrawable(android.R.color.transparent),
				// bitmapDrawable });
				// imageView.setImageDrawable(td);
				// td.startTransition(ANNATIONDURATION);
			}
		}
	}

	public interface ImageCallback
	{

		public void imageLoaded(Bitmap imageDrawable);
	}
}