package dianyun.baobaowd.util;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;

import com.tencent.connect.share.QQShare;
import com.tencent.mm.sdk.openapi.SendMessageToWX;
import com.tencent.mm.sdk.openapi.WXMediaMessage;
import com.tencent.mm.sdk.openapi.WXWebpageObject;

import dianyun.zqcheap.R;

public class TencentHelper
{

//	public static Tencent getTencent(Context context)
//	{
//		Tencent mTencent = Tencent
//				.createInstance(QQConstants.QQAPP_ID, context);
//		mTencent.setAccessToken(QQPreferenceUtil.getInstance(context)
//				.getString(QQConstants.ACCESS_TOKEN, ""), String
//				.valueOf(QQPreferenceUtil.getInstance(context).getLong(
//						QQConstants.EXPIRES_IN, 0)));
//		mTencent.setOpenId(QQPreferenceUtil.getInstance(context).getString(
//				QQConstants.OPENID, ""));
//		return mTencent;
//	}

	public static Bundle getShareToQQFriendBundle(String title,
			String imageUrl, String targetUrl, String summary, String appName)
	{
//		Bundle bundle = new Bundle();
//		bundle.putString("title", title);
//		bundle.putString("imageUrl", imageUrl);
//		bundle.putString("targetUrl", targetUrl);
//		bundle.putString("summary", summary);
//		// bundle.putString("site", siteUrl.getText() + "");
//		bundle.putString("appName", appName);
//		return bundle;
		
		
		
		 	final Bundle params = new Bundle();
			params.putString(QQShare.SHARE_TO_QQ_TITLE, title);
            params.putString(QQShare.SHARE_TO_QQ_TARGET_URL, targetUrl);
            params.putString(QQShare.SHARE_TO_QQ_SUMMARY, summary);
            params.putString(QQShare.SHARE_TO_QQ_IMAGE_URL, imageUrl);
            params.putString(QQShare.SHARE_TO_QQ_APP_NAME, appName);
            params.putInt(QQShare.SHARE_TO_QQ_KEY_TYPE, 1);
            params.putInt(QQShare.SHARE_TO_QQ_EXT_INT, 0);
            return params;
	}

	public static Bundle getShareToQQSpaceBundle(String title, String url,
			String summary, String images, String type)
	{
		Bundle parmas = new Bundle();
		parmas.putString("title", title);
		parmas.putString("url", url);
		// parmas.putString("comment", article.getContent());
		parmas.putString("summary", summary);
		parmas.putString("images", images);
		parmas.putString("type", type);
		return parmas;
	}

	private static String buildTransaction(final String type)
	{
		return (type == null) ? String.valueOf(System.currentTimeMillis())
				: type + System.currentTimeMillis();
	}

	public static SendMessageToWX.Req getShareWeixinReq(String title,
			String description, String webpageUrl, Context context,
			boolean toSpace)
	{
		WXWebpageObject webpage = new WXWebpageObject();
		webpage.webpageUrl = webpageUrl;
		WXMediaMessage msg = new WXMediaMessage(webpage);
		msg.title = title;
		msg.description = description;
		if(toSpace)msg.title =description;
		Bitmap thumb = BitmapFactory.decodeResource(context.getResources(),
				R.drawable.sharelogo);
		msg.thumbData = ImageHelper.bmpToByteArray(thumb, true);
		SendMessageToWX.Req req = new SendMessageToWX.Req();
		req.transaction = buildTransaction("webpage");
		req.message = msg;
		req.scene = toSpace ? SendMessageToWX.Req.WXSceneTimeline
				: SendMessageToWX.Req.WXSceneSession;
		return req;
	}
}
