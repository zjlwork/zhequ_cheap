package dianyun.baobaowd.util;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class NetworkStatus
{

	public static final int NETWORK_TYPE_WAP = 0;
	public static final int NETWORK_TYPE_NET = 1;
	public static final int NETWORK_TYPE_WIFI = 2;
	public static final int NETWORK_UNAVAILABLE = -1;

	public static int getNetWorkStatus(Context pContext)
	{
		if (pContext == null)
			return 0;
		ConnectivityManager lConnectivity = (ConnectivityManager) pContext
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		if (null != lConnectivity)
		{
			NetworkInfo info = lConnectivity.getActiveNetworkInfo();
			if (info != null)
			{
				if (info.getTypeName().toLowerCase().equals("wifi"))
				{
					if (info.getState() == NetworkInfo.State.CONNECTED)
					{
						return NETWORK_TYPE_WIFI;
					}
					else
					{
						return NETWORK_UNAVAILABLE;
					}
				}
				else
					if (info.getTypeName().toLowerCase().equals("mobile"))
					{
						if (info.getState() == NetworkInfo.State.CONNECTED)
						{
							if (info.getExtraInfo() != null
									&& info.getExtraInfo().indexOf("wap") != -1)
								return NETWORK_TYPE_NET;
							else
								return NETWORK_TYPE_NET;
						}
					}
			}
			// NetworkInfo[] info = lConnectivity.getAllNetworkInfo();
			// if (info != null) {
			// for (int i = 0; i < info.length; i++) {
			// if(info[i].getTypeName().toLowerCase().equals("wifi")){
			// if (info[i].getState() == NetworkInfo.State.CONNECTED) {
			// return NETWORK_TYPE_WIFI;
			// }else{
			// return NETWORK_UNAVAILABLE;
			// }
			// }else if(info[i].getTypeName().toLowerCase().equals("mobile")){
			// if (info[i].getState() == NetworkInfo.State.CONNECTED) {
			// if (info[i].getExtraInfo() != null
			// && info[i].getExtraInfo().indexOf("wap") != -1)
			// return NETWORK_TYPE_WAP;
			// else
			// return NETWORK_TYPE_NET;
			// }
			// }
			// }
			// } else {
			// return NETWORK_UNAVAILABLE;
			// }
		}
		return NETWORK_UNAVAILABLE;
	}
	// public static DefaultHttpClient getHttpClient(Context pContext){
	// switch(getNetWorkStatus(pContext)){
	// case NETWORK_TYPE_WAP:
	// return WapHttpClient.getInstance();
	// case NETWORK_TYPE_NET:
	// case NETWORK_TYPE_WIFI:
	// return NetHttpClient.getInstance();
	// case NETWORK_UNAVAILABLE:
	// return null;
	// }
	// return null;
	// }
}
