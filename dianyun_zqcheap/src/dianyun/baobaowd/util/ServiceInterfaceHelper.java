package dianyun.baobaowd.util;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.os.AsyncTask;
import dianyun.baobaowd.dto.ResultDTO;
import dianyun.baobaowd.interfaces.DialogCallBack;
import dianyun.baobaowd.serverinterface.GetUserInfo;
import dianyun.zqcheap.R;

public class ServiceInterfaceHelper {
	

	public interface InterfaceCallBack {
		public void getResultDTO(ResultDTO resultDTO);
	}

	public static void getUserInfo(final Context context, final long uid,
			final String token, final long queryUid, boolean showDialog,
			final InterfaceCallBack callback) {
		new GetUserInfoAsnycTask(context, uid, token, queryUid, showDialog, callback).execute();
	}

	protected static class GetUserInfoAsnycTask extends
			AsyncTask<Void, Void, ResultDTO> {

		private InterfaceCallBack mCallback = null;
		private Dialog mProgressDialog = null;
		private Context mContext;
		private boolean mIsShowDialog;
		long uid;
		String token;
		long queryUid;

		public GetUserInfoAsnycTask( Context context,  long uid,
				 String token,  long queryUid, boolean showDialog,
				 InterfaceCallBack callback) {
			this.mContext = context;
			this.mCallback = callback;
			this.mIsShowDialog = showDialog;
			this.uid = uid;
			this.token = token;
			this.queryUid = queryUid;
			
			
		}

		@Override
		protected void onPreExecute() {
			if (mIsShowDialog) {
				mProgressDialog = DialogHelper.showDetailLoadingDialog(mContext,
						mContext.getString(R.string.loginloading),
						new DialogCallBack() {

							@Override
							public void clickSure() {
								((Activity) mContext).finish();
							}

							@Override
							public void clickCancel() {
							}
						});
				mProgressDialog.setCanceledOnTouchOutside(false);
				mProgressDialog.setCancelable(false);
			}
			super.onPreExecute();
		}

		@Override
		protected void onPostExecute(ResultDTO result) {
			if (mIsShowDialog && mProgressDialog != null) {
				mProgressDialog.dismiss();
			}
			if (null != mCallback) {
				mCallback.getResultDTO(result);
			}
			super.onPostExecute(result);
		}

		@Override
		protected ResultDTO doInBackground(Void... params) {
			return new GetUserInfo(uid, token,
					queryUid).getConnect();
		}
	}

}
