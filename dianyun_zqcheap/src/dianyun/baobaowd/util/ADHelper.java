package dianyun.baobaowd.util;

import android.app.Notification;
import android.content.Context;
import android.text.TextUtils;

import com.google.gson.reflect.TypeToken;

import java.util.List;

import dianyun.baobaowd.data.Notice;
import dianyun.baobaowd.db.LightDBHelper;
import dianyun.baobaowd.gson.GsonHelper;


public class ADHelper
{
//	public static List<Notice> mADList;

	public static List<Notice> getADList(Context context) {
		List<Notice> res=null;
        String message=LightDBHelper.getMainNotifyMessage(context);
        if(!TextUtils.isEmpty(message))
        {
            res= (List<Notice>)GsonHelper.gsonToObj(message,new TypeToken<List<Notice>>(){});
        }
        return res;
	}

	public static void setADList(Context context,List<Notice> mADList) {
        if(mADList!=null&&mADList.size()>0) {
            String message =GsonHelper.gsonTojson(mADList);
            LightDBHelper.setMainNotifyMessage(context,message);
        }
	}
}