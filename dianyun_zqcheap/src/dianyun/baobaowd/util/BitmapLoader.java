package dianyun.baobaowd.util;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.URL;
import java.util.List;

import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Handler;
import android.os.Message;
import android.support.v4.util.LruCache;
import android.util.Log;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.nostra13.universalimageloader.core.ImageLoader;

import dianyun.baobaowd.data.Attachment;
import dianyun.baobaowd.help.LogFile;
import dianyun.zqcheap.R;
import dianyun.zqcheap.application.BaoBaoWDApplication;

public class BitmapLoader
{

	private static final int MSG_GET_IMAGE_SUCCESS = 1;
	private static final int MSG_GET_IMAGE_FAILED = 2;

	public static void setImageBitmap(ImageView imgV, String url)
	{
		if (imgV != null)
		{
			imgV.setTag(R.id.smallPath, url);
			ImageLoader.getInstance().displayImage(url, imgV,
					BaoBaoWDApplication.mOptions);
		}
	}

	public static void setMedalLayout(Context context,
			LinearLayout medalLayout, List<Integer> medalIdList)
	{/*
		int sumChildCount = medalLayout.getChildCount();
		if (medalIdList != null && medalIdList.size() > 0 && sumChildCount > 0)
		{
			medalLayout.setVisibility(View.VISIBLE);
			for (int i = 0; i < sumChildCount; i++)
			{
				ImageView lImageView = (ImageView) medalLayout.getChildAt(i);
				if (i >= medalIdList.size())
				{
					lImageView.setVisibility(View.GONE);
				}
				else
				{
					lImageView.setVisibility(View.VISIBLE);
					Medal lMedal = MedalHelper.mMedalMap
							.get(medalIdList.get(i));
					if (lMedal != null)
					{
						lImageView.setVisibility(View.VISIBLE);
						lImageView.setImageResource(R.drawable.medal_bdefault);
						lImageView.setTag(R.id.smallPath, lMedal.getSmallPic());
						ImageLoader.getInstance().displayImage(
								lMedal.getSmallPic(), lImageView,
								BaoBaoWDApplication.mOptions);
					}
				}
			}
		}
		else
		{
			medalLayout.setVisibility(View.GONE);
		}
	*/}

	public static BitmapLoader getInstance(Context context)
	{
		if (imagecache == null)
		{
			// final int memClass = ((ActivityManager) context.getSystemService(
			// Context.ACTIVITY_SERVICE)).getMemoryClass();
			//
			// final int cacheSize = 1024 * 1024 * memClass / 16;
			// LruCache通过构造函数传入缓存值，以KB为单位。
			int maxMemory = (int) (Runtime.getRuntime().maxMemory() / 1024);
			// 使用最大可用内存值的1/8作为缓存的大小。
			int cacheSize = maxMemory / 10;
			System.out.println("cacheSize==========" + cacheSize);
			imagecache = new LruCache<String, Bitmap>(cacheSize)
			{

				@Override
				protected int sizeOf(String key, Bitmap bitmap)
				{
					return bitmap.getRowBytes() * bitmap.getHeight();
					// return bitmap.getByteCount() / 1024;
				}

				@Override
				protected void entryRemoved(boolean evicted, String key,
						Bitmap oldValue, Bitmap newValue)
				{
					Log.i("entryRemoved", key + "---" + evicted);
					// oldValue.recycle();
					// oldValue=null;
					//
					// System.gc();
					super.entryRemoved(evicted, key, oldValue, newValue);
				}
			};
		}
		return new BitmapLoader(context);
	}
	private Handler handler;
	private static LruCache<String, Bitmap> imagecache;
	private Context context;
	private ImageCallback imageCallback;
	private ImageView imageView;

	public interface DownloadProgress
	{

		public void updateProgress(int totalSize, int downLoadSize,
				Attachment attachment, boolean isFinish);
	}

	public interface ImageCallback
	{

		public void imageLoad(ImageView imageView, Bitmap bitmap);
	}

	public BitmapLoader(Context context)
	{
		this.context = context;
		handler = new Handler()
		{

			@Override
			public void handleMessage(android.os.Message msg)
			{
				if (msg.what == MSG_GET_IMAGE_SUCCESS)
				{
					Bitmap bitmap = (Bitmap) msg.obj;
					if (imageCallback != null)
						imageCallback.imageLoad(imageView, bitmap);
				}
				else if (msg.what == MSG_GET_IMAGE_FAILED)
				{
					imageCallback.imageLoad(imageView, null);
				}
			};
		};
	}

	public Bitmap getBitmap(final String imageUrl, final String pre)
	{
		if (imageUrl == null || imageUrl.equals(""))
			return null;
		// 内存缓冲
		if (imagecache.get(imageUrl) != null)
		{
			// SoftReference<Bitmap> ref = imagecache.get(imageUrl);
			Bitmap bitmap = imagecache.get(imageUrl);
			if (null != bitmap)
			{
				// Log.i("imageloader", "imagecache中找到        "+imageUrl);
				return bitmap;
			}
			else
			{
				return getBitmapByCacheFile(imageUrl, pre);
			}
		}
		// 本地缓冲
		else
		{
			return getBitmapByCacheFile(imageUrl, pre);
		}
	}

	public Bitmap getBitmapByCache(final String imageUrl, final String pre)
	{
		if (imageUrl == null || imageUrl.equals(""))
			return null;
		// 内存缓冲
		if (imagecache.get(imageUrl) != null)
		{
			// SoftReference<Bitmap> ref = imagecache.get(imageUrl);
			Bitmap bitmap = imagecache.get(imageUrl);
			if (null != bitmap)
			{
				return bitmap;
			}
		}
		return null;
	}

	public Bitmap getBitmapByCacheFile(final String imageUrl, final String pre)
	{
		Bitmap bitmap = null;
		String bitmapName = imageUrl.substring(imageUrl.lastIndexOf("/") + 1);
		if (pre != null)
			bitmapName = pre + bitmapName;
		try
		{
			File file = context.getExternalCacheDir();
			File imagefile = new File(file.getAbsolutePath() + File.separator
					+ bitmapName);
			if (imagefile.isFile() && imagefile.exists())
			{
				// System.out.println("decodefile qian  ======="+System.currentTimeMillis());
				bitmap = BitmapFactory.decodeFile(imagefile.getAbsolutePath());
				// bitmap = BitmapFactory.decodeStream(new FileInputStream(new
				// File(imagefile.getAbsolutePath())));
				// System.out.println("decodefile hou =============="+System.currentTimeMillis());
				if (bitmap != null)
				{
					putBitmapToCache(bitmap, imageUrl);
				}
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			return null;
		}
		return bitmap;
	}

	public String getCacheFilePath(final String imageUrl, final String pre)
	{
		String bitmapName = imageUrl.substring(imageUrl.lastIndexOf("/") + 1);
		if (pre != null)
			bitmapName = pre + bitmapName;
		try
		{
			File file = context.getExternalCacheDir();
			File imagefile = new File(file.getAbsolutePath() + File.separator
					+ bitmapName);
			if (imagefile.isFile() && imagefile.exists())
			{
				return imagefile.getAbsolutePath();
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			return null;
		}
		return null;
	}

	public void downloadBitmapWithProgress(final String imageUrl,
			final String pre, final DownloadProgress downloadProgress)
	{
		// 从网络获取(启动一个线程异步操作)
		new Thread()
		{

			@Override
			public void run()
			{
				try
				{
					// 放入本地缓存
					File file = context.getExternalCacheDir();
					String bitmapName = imageUrl.substring(imageUrl
							.lastIndexOf("/") + 1);
					if (pre != null)
						bitmapName = pre + bitmapName;
					File bitmapfile = new File(file.getAbsoluteFile()
							+ File.separator + bitmapName);
					if (!bitmapfile.exists())
					{
						boolean result = bitmapfile.createNewFile();
					}
					else
					{
						boolean deleteresult = bitmapfile.delete();
						boolean result = bitmapfile.createNewFile();
					}
					try
					{
						DefaultHttpClient lClient = new DefaultHttpClient();
						HttpGet lHttpGet = new HttpGet(imageUrl);
						HttpResponse lRes = lClient.execute(lHttpGet);
						if (lRes.getStatusLine().getStatusCode() == 200)
						{
							InputStream zipin = lRes.getEntity().getContent();
							Header contentEncoding = lRes
									.getFirstHeader("Content-Length");
							int totalSize = Integer.parseInt(contentEncoding
									.getValue());
							FileOutputStream out = new FileOutputStream(
									bitmapfile);
							if (totalSize > 0 && zipin != null)
							{
								int downloaded = 0;
								int l = 0;
								byte[] b = new byte[10240];
								while ((l = zipin.read(b)) != -1)
								{
									downloaded += l;
									out.write(b, 0, l);
									downloadProgress.updateProgress(totalSize,
											downloaded, null, false);
								}
								downloadProgress.updateProgress(totalSize,
										downloaded, null, true);
								Bitmap temp_bitmap = BitmapFactory
										.decodeFile(bitmapfile
												.getAbsolutePath());
								// imagecache.put(imageUrl, temp_bitmap);
								putBitmapToCache(temp_bitmap, imageUrl);
							}
							else if (totalSize == -1 && zipin != null)
							{
								int l = 0;
								byte[] b = new byte[10240];
								while ((l = zipin.read(b)) != -1)
								{
									out.write(b, 0, l);
								}
								downloadProgress.updateProgress(totalSize, -1,
										null, true);
							}
							out.flush();
							out.close();
							zipin.close();
						}
					}
					catch (Exception e)
					{
						LogFile.SaveExceptionLog(e);
						downloadProgress.updateProgress(100, 0, null, true);
					}
				}
				catch (Exception e)
				{
					LogFile.SaveExceptionLog(e);
				}
			};
		}.start();
	}

	public void downloadBitmap(ImageView imageView, final String imageUrl,
			final String pre, ImageCallback callback)
	{
		this.imageCallback = callback;
		this.imageView = imageView;
		// 从网络获取(启动一个线程异步操作)
		new Thread()
		{

			@Override
			public void run()
			{
				try
				{
					// plusDownLoadCount();
					InputStream is = (new URL(imageUrl).openStream());
					Bitmap temp_bitmap = BitmapFactory.decodeStream(is);
					Message msg = new Message();
					msg.what = MSG_GET_IMAGE_SUCCESS;
					msg.obj = temp_bitmap;
					handler.sendMessage(msg);
					// 放入内存
					// imagecache.put(imageUrl, temp_bitmap);
					putBitmapToCache(temp_bitmap, imageUrl);
					// System.out.println("放入内存   imageUrl===="+imageUrl);
					// 放入本地缓存
					File file = context.getExternalCacheDir();
					String bitmapName = imageUrl.substring(imageUrl
							.lastIndexOf("/") + 1);
					if (pre != null)
						bitmapName = pre + bitmapName;
					File bitmapfile = new File(file.getAbsoluteFile()
							+ File.separator + bitmapName);
					if (!bitmapfile.exists())
					{
						boolean result = bitmapfile.createNewFile();
						// System.out.println("bitmapfile.createNewFile()="+result+"   "+imageUrl);
					}
					else
					{
						boolean deleteresult = bitmapfile.delete();
						boolean result = bitmapfile.createNewFile();
						// System.out.println("bitmapfile.deleteresult()="+deleteresult+"    createNewFile="+result+"   "+imageUrl);
					}
					FileOutputStream fos = new FileOutputStream(bitmapfile);
					temp_bitmap.compress(Bitmap.CompressFormat.PNG, 100, fos);
					fos.close();
				}
				catch (Exception e)
				{
					LogFile.SaveExceptionLog(e);
					Message msg = new Message();
					msg.what = MSG_GET_IMAGE_FAILED;
					msg.obj = null;
					handler.sendMessage(msg);
				}
				finally
				{
					// minusDownLoadCount();
				}
			};
		}.start();
	}

	public void downloadBitmap(final String imageUrl, final String pre)
	{
		// 从网络获取(启动一个线程异步操作)
		new Thread()
		{

			@Override
			public void run()
			{
				try
				{
					InputStream is = (new URL(imageUrl).openStream());
					Bitmap temp_bitmap = BitmapFactory.decodeStream(is);
					// 放入内存
					// imagecache.put(imageUrl, temp_bitmap);
					putBitmapToCache(temp_bitmap, imageUrl);
					// System.out.println("放入内存   imageUrl===="+imageUrl);
					// 放入本地缓存
					File file = context.getExternalCacheDir();
					String bitmapName = imageUrl.substring(imageUrl
							.lastIndexOf("/") + 1);
					if (pre != null)
						bitmapName = pre + bitmapName;
					File bitmapfile = new File(file.getAbsoluteFile()
							+ File.separator + bitmapName);
					if (!bitmapfile.exists())
					{
						boolean result = bitmapfile.createNewFile();
						// System.out.println("bitmapfile.createNewFile()="+result+"   "+imageUrl);
					}
					else
					{
						boolean deleteresult = bitmapfile.delete();
						boolean result = bitmapfile.createNewFile();
						// System.out.println("bitmapfile.deleteresult()="+deleteresult+"    createNewFile="+result+"   "+imageUrl);
					}
					FileOutputStream fos = new FileOutputStream(bitmapfile);
					temp_bitmap.compress(Bitmap.CompressFormat.PNG, 100, fos);
					fos.close();
				}
				catch (Exception e)
				{
					LogFile.SaveExceptionLog(e);
				}
			};
		}.start();
	}

	public void downloadViewPicture(final String imageUrl, final String pre)
	{
		try
		{
			InputStream is = (new URL(imageUrl).openStream());
			Bitmap temp_bitmap = BitmapFactory.decodeStream(is);
			// 放入内存
			// imagecache.put(imageUrl, temp_bitmap);
			putBitmapToCache(temp_bitmap, imageUrl);
			// System.out.println("放入内存   imageUrl===="+imageUrl);
			// 放入本地缓存
			File file = context.getExternalCacheDir();
			String bitmapName = imageUrl
					.substring(imageUrl.lastIndexOf("/") + 1);
			if (pre != null)
				bitmapName = pre + bitmapName;
			File bitmapfile = new File(file.getAbsoluteFile() + File.separator
					+ bitmapName);
			if (!bitmapfile.exists())
			{
				boolean result = bitmapfile.createNewFile();
				// System.out.println("bitmapfile.createNewFile()="+result+"   "+imageUrl);
			}
			else
			{
				boolean deleteresult = bitmapfile.delete();
				boolean result = bitmapfile.createNewFile();
				// System.out.println("bitmapfile.deleteresult()="+deleteresult+"    createNewFile="+result+"   "+imageUrl);
			}
			FileOutputStream fos = new FileOutputStream(bitmapfile);
			temp_bitmap.compress(Bitmap.CompressFormat.PNG, 100, fos);
			fos.close();
		}
		catch (Exception e)
		{
			LogFile.SaveExceptionLog(e);
		}
	}

	public void putBitmapToCache(Bitmap bitmap, String localFilePath)
	{
		if (bitmap != null && localFilePath != null)
			imagecache.put(localFilePath, bitmap);
	}

	public Bitmap getBitmapFromCache(String localFilePath)
	{
		if (imagecache.get(localFilePath) != null)
		{
			// SoftReference<Bitmap> ref = imagecache.get(localFilePath);
			Bitmap bitmap = imagecache.get(localFilePath);
			return bitmap;
		}
		return null;
	}

	public String saveBitmapToCacheFile(Bitmap bitmap, final String fileName,
			final String pre)
	{
		try
		{
			File file = context.getExternalCacheDir();
			String bitmapName = pre + fileName;
			File bitmapfile = new File(file.getAbsoluteFile() + File.separator
					+ bitmapName);
			if (!bitmapfile.exists())
			{
				boolean result = bitmapfile.createNewFile();
			}
			else
			{
				boolean deleteresult = bitmapfile.delete();
				boolean result = bitmapfile.createNewFile();
			}
			FileOutputStream fos = new FileOutputStream(bitmapfile);
			if (bitmapName.endsWith(".jpg") || bitmapName.endsWith(".JPG"))
			{
				bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fos);
			}
			else
			{
				bitmap.compress(Bitmap.CompressFormat.PNG, 100, fos);
			}
			fos.close();
			return bitmapfile.getAbsolutePath();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		return null;
	}
}
