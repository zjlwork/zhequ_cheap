package dianyun.baobaowd.util;

import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Dialog;
import android.content.Context;
import android.os.AsyncTask;
import android.text.TextUtils;
import android.widget.Toast;

import com.google.gson.reflect.TypeToken;

import dianyun.baobaowd.data.ShareOrderLog;
import dianyun.baobaowd.data.User;
import dianyun.baobaowd.data.UserShareUrl;
import dianyun.baobaowd.db.TableConstants;
import dianyun.baobaowd.db.UserShareUrlDBHelper;
import dianyun.baobaowd.dto.ResultDTO;
import dianyun.baobaowd.gson.GsonHelper;
import dianyun.baobaowd.help.LogFile;
import dianyun.baobaowd.serverinterface.GetShareUrl;
import dianyun.baobaowd.serverinterface.ShareOrder;
import dianyun.zqcheap.R;

/**
 * 联系人数据库操作辅助
 * 
 */
public class UserShareUrlHelper
{

	

	public static void addUserShareUrlList(Context context,List<UserShareUrl> userShareUrlList)
	{
		if(userShareUrlList==null||userShareUrlList.size()==0)return;
		UserShareUrlDBHelper lUserShareUrlDBHelper = null;
		try
		{
			lUserShareUrlDBHelper = new UserShareUrlDBHelper(context,
					TableConstants.TABLE_USERSHAREURL);
			lUserShareUrlDBHelper.beginTransaciton();
			User mUser = UserHelper.getUser();
			
			for(UserShareUrl userShareUrl:userShareUrlList){
				String urlValue = userShareUrl.getUrlValue();
				if(!TextUtils.isEmpty(urlValue)){
					urlValue =urlValue.replace("{uid}", String.valueOf(mUser.getUid()))
							.replace("{token}",mUser.getToken());
					userShareUrl.setUrlValue(urlValue);
				}
				lUserShareUrlDBHelper.insert(userShareUrl);
			}
			lUserShareUrlDBHelper.setTransactionSuccessful();
		}
		catch (Exception e)
		{
			LogFile.SaveExceptionLog(e);
		}
		finally
		{
			if (null != lUserShareUrlDBHelper){
				lUserShareUrlDBHelper.endTransaction();
				lUserShareUrlDBHelper.closeDB();
			}
		}
		
	}
	public static UserShareUrl getUserShareUrlByKey(Context context,String urlKey)
	{
		UserShareUrlDBHelper lUserShareUrlDBHelper = null;
		try
		{
			lUserShareUrlDBHelper = new UserShareUrlDBHelper(context,
					TableConstants.TABLE_USERSHAREURL);
			return lUserShareUrlDBHelper.getUserShareUrlByKey(urlKey);
		}
		catch (Exception e)
		{
			LogFile.SaveExceptionLog(e);
		}
		finally
		{
			if (null != lUserShareUrlDBHelper){
				lUserShareUrlDBHelper.closeDB();
			}
		}
		return null;
		
	}
	public static long deleteAll(Context context)
	{
		UserShareUrlDBHelper lUserShareUrlDBHelper = null;
		try
		{
			lUserShareUrlDBHelper = new UserShareUrlDBHelper(context,
					TableConstants.TABLE_USERSHAREURL);
			return lUserShareUrlDBHelper.deleteAll();
		}
		catch (Exception e)
		{
			LogFile.SaveExceptionLog(e);
		}
		finally
		{
			if (null != lUserShareUrlDBHelper){
				lUserShareUrlDBHelper.closeDB();
			}
		}
		return 0L;
		
	}
	
	
	public static void getShareUrl(Context context, boolean isShowDialog,
			GetShareUrlCallBack callback){
		new GetShareUrlAsnycTask(context, isShowDialog, callback).execute();
	}
	
	public static void shareOrder(Context context, boolean isShowDialog,String seqId,boolean rebate){
		new ShareOrderAsnycTask(context, isShowDialog, seqId,rebate).execute();
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	protected static class GetShareUrlAsnycTask extends
    AsyncTask<Void, Void, List<UserShareUrl>> {
    	
    	private Dialog mDialog = null;
    	private Context mContext;
    	private boolean mIsShowDialog;
    	GetShareUrlCallBack callback;
    	public GetShareUrlAsnycTask(Context context, boolean isShowDialog,
    			GetShareUrlCallBack callback) {
    		mContext = context;
    		mIsShowDialog = isShowDialog;
    		this.callback = callback;
    	}
    	
    	@Override
    	protected void onPreExecute() {
    		if (mIsShowDialog) {
    			mDialog = DialogHelper.showProgressDialog(mContext, mContext
    					.getResources().getString(R.string.loginloading));
    		}
    		super.onPreExecute();
    	}
    	
    	@Override
    	protected void onPostExecute(List<UserShareUrl> result) {
    		if (mIsShowDialog && mDialog != null) {
    			mDialog.dismiss();
    		}
    		if(result!=null&&result.size()>0){
    			UserShareUrlHelper.deleteAll(mContext);
    			UserShareUrlHelper.addUserShareUrlList(mContext, result);
    		}
    		if (null != callback) {
    			callback.getResult();
    		}
    		super.onPostExecute(result);
    	}
    	
    	@Override
    	protected List<UserShareUrl> doInBackground(Void... params) {
    		User mUser = UserHelper.getUser();
    		List<UserShareUrl> shareUrlList =null;
    		ResultDTO requestResult = new GetShareUrl(mUser.getUid(), mUser.getToken()).getConnect();
    		if (requestResult != null && requestResult.getCode().equals("0")) {
    			String dataBody = requestResult.getResult();
    			if (!TextUtils.isEmpty(dataBody)) {
    				shareUrlList = GsonHelper.gsonToObj(dataBody,
    						new TypeToken<List<UserShareUrl>>() {
    				});
    				
    			}
    		}
    		return shareUrlList;
    	}
    }
	
	
	protected static class ShareOrderAsnycTask extends
	AsyncTask<Void, Void, ShareOrderLog> {
		
		private Dialog mDialog = null;
		private Context mContext;
		private boolean mIsShowDialog;
		private String seqId;
		private boolean rebate;
		public ShareOrderAsnycTask(Context context, boolean isShowDialog,String seqId,boolean rebate) {
			mContext = context;
			mIsShowDialog = isShowDialog;
			this.seqId = seqId;
			this.rebate = rebate;
		}
		
		@Override
		protected void onPreExecute() {
			if (mIsShowDialog) {
				mDialog = DialogHelper.showProgressDialog(mContext, mContext
						.getResources().getString(R.string.loginloading));
			}
			super.onPreExecute();
		}

		@Override
		protected ShareOrderLog doInBackground(Void... params) {
			ShareOrderLog	lShareOrderLog = null;
			try {
				
				User mUser = UserHelper.getUser();
				ResultDTO requestResult = new ShareOrder(mUser.getUid(), mUser.getToken(),seqId,rebate).postConnect();
				if (requestResult != null && requestResult.getCode().equals("0")) {
					String dataBody = requestResult.getResult();
	    			if (!TextUtils.isEmpty(dataBody)) {
	    				JSONObject jsonObject = new JSONObject(dataBody);
	    				lShareOrderLog = GsonHelper.gsonToObj(jsonObject.get("shareOrderLog").toString(),ShareOrderLog.class);
	    				
	    			}
				}else if(requestResult != null && (requestResult.getCode().equals("-1050")||
						requestResult.getCode().equals("-1051")||requestResult.getCode().equals("-1052"))){
				}
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return lShareOrderLog;
		}

		@Override
		protected void onPostExecute(ShareOrderLog result) {
			if (mIsShowDialog && mDialog != null) {
				mDialog.dismiss();
			}
			if(result!=null&&result.coins>0){
				Toast.makeText(mContext,String.format(mContext.getString(R.string.sharesuccess_getgoldcount), result.coins) 
						, Toast.LENGTH_SHORT).show();
				BroadCastHelper.sendAddUserGoldBroadcast(mContext, result.coins);
			}
			super.onPostExecute(result);
		}
		
	}
	
	
	public interface GetShareUrlCallBack{
		void getResult();
	}


	
}
