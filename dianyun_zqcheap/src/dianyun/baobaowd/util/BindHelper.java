package dianyun.baobaowd.util;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.text.TextUtils;
import android.widget.Toast;
import dianyun.baobaowd.data.User;
import dianyun.baobaowd.data.UserPayExt;
import dianyun.baobaowd.db.LightDBHelper;
import dianyun.baobaowd.dto.ResultDTO;
import dianyun.baobaowd.gson.GsonHelper;
import dianyun.baobaowd.interfaces.DialogCallBack;
import dianyun.baobaowd.serverinterface.GetUserBindInfo;
import dianyun.zqcheap.R;
import dianyun.zqcheap.activity.BindAlipayActivity;
import dianyun.zqcheap.activity.BindPhoneActivity;
import dianyun.zqcheap.activity.ExchangeMoneyActivity;
import dianyun.zqcheap.application.BaoBaoWDApplication;

public class BindHelper {

	Context mContext;

	public BindHelper(Context context) {
		this.mContext = context;
	}

	boolean stop = false;

	public void cancelJudge() {
		this.stop = true;
		DialogHelper.cancelProgressDialog(mProgressDialog);
	}

	public void judgeBind() {
		
		if (UserHelper.isGusetUser(mContext))
		{
			UserHelper.gusestUserGo(mContext);
		}
		else
		{
			String phone = LightDBHelper.getBindPhone(BaoBaoWDApplication.context);
			String pay = LightDBHelper.getBindPay(BaoBaoWDApplication.context);
			if (!TextUtils.isEmpty(phone) && !TextUtils.isEmpty(pay)) {
				Intent lIntent = new Intent(mContext, ExchangeMoneyActivity.class);
				mContext.startActivity(lIntent);
			} else {
				if (NetworkStatus.getNetWorkStatus(mContext) > 0)
					new GetUserBindInfoThread().start();
				else
					Toast.makeText(mContext,
							mContext.getString(R.string.no_network),
							Toast.LENGTH_SHORT).show();
			}
		}
	}

	Dialog mProgressDialog;

	class GetUserBindInfoThread extends Thread {

		private Handler handler;
		private ResultDTO resultDTO;

		public GetUserBindInfoThread() {
			handler = new Handler();
			mProgressDialog = DialogHelper.showDetailLoadingDialog(
	                   mContext,mContext.getString(R.string.getbindinfoing), new DialogCallBack() {

	                        @Override
	                        public void clickSure() {
	                        	cancelJudge();
	                        }

	                        @Override
	                        public void clickCancel() {
	                        }
	                    });
			mProgressDialog.setCanceledOnTouchOutside(false);
			mProgressDialog.setCancelable(false);
		}

		@Override
		public void run() {
			User mUser = UserHelper.getUser();
			resultDTO = new GetUserBindInfo(mUser.getUid(), mUser.getToken())
					.getConnect();
			handler.post(new Runnable() {

				@Override
				public void run() {

					DialogHelper.cancelProgressDialog(mProgressDialog);
					if (stop)
						return;
					if (resultDTO != null && resultDTO.getCode().equals("0")) {
						UserPayExt lUserPayExt = GsonHelper.gsonToObj(
								resultDTO.getResult(), UserPayExt.class);
						if (lUserPayExt != null) {
							String account = lUserPayExt.getAccount();
							String phone = lUserPayExt.getCashPhone();

							if (TextUtils.isEmpty(phone)) {
								Intent lIntent = new Intent(mContext,
										BindPhoneActivity.class);
								lIntent.putExtra(GobalConstants.Data.BINDMODE,
										GobalConstants.BindMode.EXCHANGEMONEY);
								mContext.startActivity(lIntent);
							} else {
								LightDBHelper.setBindPhone(phone, mContext);
								Intent lIntent = new Intent();
								if (TextUtils.isEmpty(account)) {
									lIntent.setClass(mContext,
											BindAlipayActivity.class);
								} else {
									LightDBHelper.setBindPay(account, mContext);
									lIntent.setClass(mContext,
											ExchangeMoneyActivity.class);
								}
								mContext.startActivity(lIntent);
							}

						}
					} else {
						Toast.makeText(
								mContext,
								mContext.getString(R.string.getbindinfo_failed),
								Toast.LENGTH_SHORT).show();
					}
				}
			});
		}
	}

}
