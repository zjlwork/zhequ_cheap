package dianyun.baobaowd.util;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.telephony.TelephonyManager;

public class TelInfo
{

	public static boolean isTelecomProvider(Context context)
	{
		TelephonyManager teltManager = (TelephonyManager) context
				.getSystemService(Context.TELEPHONY_SERVICE);
		String operator = teltManager.getSimOperator();
		if (operator != null)
		{
			if (operator.equals("46000") || operator.equals("46002")
					|| operator.equals("46007"))
			{
				// china mobile
				return false;
			}
			else
				if (operator.equals("46001"))
				{
					// china Unicom
					return false;
				}
				else
					if (operator.equals("46003"))
					{
						// china Telecom
						return true;
					}
		}
		return false;
	}

	public static boolean isNetAvailable(Context paramContext)
	{
		boolean isAvilable = false;
		try
		{
			ConnectivityManager connectManger = (ConnectivityManager) paramContext
					.getSystemService(Context.CONNECTIVITY_SERVICE);
			NetworkInfo localNetworkInfo = connectManger.getActiveNetworkInfo();
			if (localNetworkInfo != null)
			{
				isAvilable = localNetworkInfo.isAvailable();
			}
		}
		catch (Exception localException)
		{
			localException.printStackTrace();
		}
		return isAvilable;
	}

	public static int getNetworkType(Context context)
	{
		ConnectivityManager m_ConnectivityManager = (ConnectivityManager) context
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo networkInfo = m_ConnectivityManager.getActiveNetworkInfo();
		if (networkInfo != null)
			return networkInfo.getType();
		return -1;
	}
}
