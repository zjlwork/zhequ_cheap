package dianyun.baobaowd.util;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

import dianyun.baobaowd.data.Attachment;
import dianyun.baobaowd.help.FileHelper;
import dianyun.zqcheap.R;
import dianyun.zqcheap.application.BaoBaoWDApplication;

public class AttachmentHelper
{

	private static int SIZE_1080 = 1080;
	private static int SIZE_720 = 720;
	private static int SIZE_480 = 480;
	private static int SIZE_200 = 200;

	public static String getFileUrlByScreen(Context context, String fileUrl)
	{
		String fileName = fileUrl
				.substring(fileUrl.lastIndexOf(File.separator) + 1);
		String url = fileUrl.substring(0,
				fileUrl.lastIndexOf(File.separator) + 1);
		String name = fileName.substring(0, fileName.lastIndexOf("."));
		String suffix = fileName.substring(fileName.lastIndexOf("."));
		String newFileName;
		if (ToastHelper.getScreenWidth(context) >= SIZE_1080)
		{
			newFileName = name + "_" + SIZE_1080 + suffix;
		}
		else if (ToastHelper.getScreenWidth(context) >= SIZE_720)
		{
			newFileName = name + "_" + SIZE_720 + suffix;
		}
		else if (ToastHelper.getScreenWidth(context) >= SIZE_480)
		{
			newFileName = name + "_" + SIZE_480 + suffix;
		}
		else
		{
			newFileName = name + "_" + SIZE_480 + suffix;
		}
		return url + newFileName;
	}

	public static String getSmallFileUrl(Context context, String fileUrl)
	{
		String fileName = fileUrl
				.substring(fileUrl.lastIndexOf(File.separator) + 1);
		String url = fileUrl.substring(0,
				fileUrl.lastIndexOf(File.separator) + 1);
		int index = fileName.lastIndexOf(".");
		if (index < 0)
		{
			index = 0;
		}
		String name = fileName.substring(0, index);
		String suffix = (index == 0 ? "" : fileName.substring(index));
		String newFileName = name + "_" + SIZE_200 + suffix;
		return url + newFileName;
	}

	public static String getMeduimFileUrl(Context context, String fileUrl)
	{
		String fileName = fileUrl
				.substring(fileUrl.lastIndexOf(File.separator) + 1);
		String url = fileUrl.substring(0,
				fileUrl.lastIndexOf(File.separator) + 1);
		int index = fileName.lastIndexOf(".");
		if (index < 0)
		{
			index = 0;
		}
		String name = fileName.substring(0, index);
		String suffix = (index == 0 ? "" : fileName.substring(index));
		String newFileName = name + "_" + SIZE_480 + suffix;
		return url + newFileName;
	}

	public static String getSmallFileLocalPath(String fileLocalPath)
	{
		String smallFileName = fileLocalPath.substring(fileLocalPath
				.lastIndexOf(File.separator) + 1);
		String smallFilePath = FileHelper.getSmallImgPath() + smallFileName;
		return smallFilePath;
	}

	public interface ClickImgCallBack
	{

		void callback(Attachment selectedAttachment);
	}

	private static int getSelecetedPostion(
			ArrayList<Attachment> attachmentList, Attachment lAttachment)
	{
		if (attachmentList != null && attachmentList.size() > 0)
			for (int i = 0; i < attachmentList.size(); i++)
			{
				Attachment attachment = attachmentList.get(i);
				if (attachment.getAttId().equals(lAttachment.getAttId()))
				{
					return i;
				}
			}
		return 0;
	}

	public static void goShowImageActivity(Context context,
			ArrayList<Attachment> attachmentList, Attachment selectedAttachment)
	{/*
		if (attachmentList == null || selectedAttachment == null)
			return;
		int selectedPostion = getSelecetedPostion(attachmentList,
				selectedAttachment);
		Intent lIntent = new Intent(context, ShowImageActivity.class);
		lIntent.putExtra(GobalConstants.Data.ATTACHMENTLIST, attachmentList);
		lIntent.putExtra(GobalConstants.Data.SELECTEDPOSITION, selectedPostion);
		context.startActivity(lIntent);
	*/}

	public static void showDetailImgs(final Context context,
			final ArrayList<Attachment> attachmentList,
			final ArrayList<Attachment> allattachmentList,
			LinearLayout mContentImgLayout, boolean isWidthFull)
	{
		int sumChildCount = mContentImgLayout.getChildCount();
		if (attachmentList != null && attachmentList.size() > 0
				&& sumChildCount > 0)
		{
			mContentImgLayout.setVisibility(View.VISIBLE);
			for (int i = 0; i < sumChildCount; i++)
			{
				ImageView lImageView = (ImageView) mContentImgLayout
						.getChildAt(i);
				if (isWidthFull)
				{
					// LinearLayout.LayoutParams lp =
					// (LinearLayout.LayoutParams) lImageView
					// .getLayoutParams();
					// lp.width =
					// context.getResources().getDisplayMetrics().widthPixels
					// - 2 * ConversionHelper.dipToPx(15, context);
					// lp.height = lp.width * 3 / 4;
					// lImageView.setLayoutParams(lp);
				}
				if (i >= attachmentList.size())
				{
					lImageView.setVisibility(View.GONE);
				}
				else
				{
					lImageView.setVisibility(View.VISIBLE);
					final Attachment lAttachment = attachmentList.get(i);
					String smallFileUrl = lAttachment.getFileUrl();
					smallFileUrl = AttachmentHelper.getMeduimFileUrl(context,
							lAttachment.getFileUrl());
					// BaoBaoWDApplication.mPhotoBitmap.display(lImageView,
					// smallFileUrl,
					// ConversionHelper.dipToPx(90, context),
					// ConversionHelper.dipToPx(70, context));
					ImageLoader.getInstance().displayImage(smallFileUrl,
							lImageView, BaoBaoWDApplication.mOptions);
					lImageView.setOnClickListener(new OnClickListener()
					{

						@Override
						public void onClick(View v)
						{
							if (allattachmentList != null)
								AttachmentHelper.goShowImageActivity(context,
										allattachmentList, lAttachment);
						}
					});
				}
			}
		}
		else
		{
			mContentImgLayout.setVisibility(View.GONE);
		}
	}

	// public static void showDetailImgs(final Context context,
	// final ArrayList<Attachment> attachmentList,
	// final ArrayList<Attachment> allattachmentList,
	// LinearLayout mContentImgLayout, boolean isWidthFull)
	// {
	// int sumChildCount = mContentImgLayout.getChildCount();
	// if (attachmentList != null && attachmentList.size() > 0
	// && sumChildCount > 0)
	// {
	// mContentImgLayout.setVisibility(View.VISIBLE);
	// for (int i = 0; i < sumChildCount; i++)
	// {
	// final MDefineImageView lImageView = (MDefineImageView) mContentImgLayout
	// .getChildAt(i);
	// if (isWidthFull)
	// {
	// // LinearLayout.LayoutParams lp =
	// // (LinearLayout.LayoutParams) lImageView
	// // .getLayoutParams();
	// // lp.width =
	// // context.getResources().getDisplayMetrics().widthPixels
	// // - 2 * ConversionHelper.dipToPx(15, context);
	// // lp.height = lp.width * 3 / 4;
	// // lImageView.setLayoutParams(lp);
	// }
	// if (i >= attachmentList.size())
	// {
	// lImageView.setVisibility(View.GONE);
	// }
	// else
	// {
	// lImageView.setVisibility(View.VISIBLE);
	// final Attachment lAttachment = attachmentList.get(i);
	// String smallFileUrl = lAttachment.getFileUrl();
	// smallFileUrl = AttachmentHelper.getSmallFileUrl(context,
	// lAttachment.getFileUrl());
	// // BaoBaoWDApplication.mPhotoBitmap.display(lImageView,
	// // smallFileUrl,
	// // ConversionHelper.dipToPx(90, context),
	// // ConversionHelper.dipToPx(70, context));
	// // ImageLoader.getInstance().displayImage(smallFileUrl,
	// // lImageView, BaoBaoWDApplication.mOptions);
	// ImageLoader.getInstance().displayImage(smallFileUrl,
	// lImageView, BaoBaoWDApplication.mOptions,
	// new SimpleImageLoadingListener()
	// {
	//
	// @Override
	// public void onLoadingComplete(String imageUri,
	// View view, Bitmap loadedImage)
	// {
	// if (imageUri.endsWith(".gif"))
	// {
	// new AttachmentHelper.loadGifTask(
	// ImageLoader.getInstance()
	// .getDiskCache()
	// .get(imageUri),
	// lImageView, loadedImage);
	// }
	// else
	// {
	// lImageView.setImageBitmap(loadedImage);
	// }
	// super.onLoadingComplete(imageUri, view,
	// loadedImage);
	// }
	// }, null);
	// lImageView.setOnClickListener(new OnClickListener()
	// {
	//
	// @Override
	// public void onClick(View v)
	// {
	// if (allattachmentList != null)
	// AttachmentHelper.goShowImageActivity(context,
	// allattachmentList, lAttachment);
	// }
	// });
	// }
	// }
	// }
	// else
	// {
	// mContentImgLayout.setVisibility(View.GONE);
	// }
	// }
	public static void showDetailImgs(final Context context,
			final ArrayList<Attachment> attachmentList,
			final ArrayList<Attachment> allattachmentList,
			RelativeLayout mContentImgLayout)
	{
		int sumChildCount = mContentImgLayout.getChildCount();
		if (attachmentList != null && attachmentList.size() > 0
				&& sumChildCount > 0)
		{
			mContentImgLayout.setVisibility(View.VISIBLE);
			for (int i = 0; i < sumChildCount; i++)
			{
				ImageView lImageView = (ImageView) mContentImgLayout
						.getChildAt(i);
				if (i >= attachmentList.size())
				{
					lImageView.setVisibility(View.GONE);
				}
				else
				{
					lImageView.setVisibility(View.VISIBLE);
					final Attachment lAttachment = attachmentList.get(i);
					String smallFileUrl = lAttachment.getFileUrl();
					smallFileUrl = AttachmentHelper.getMeduimFileUrl(context,
							lAttachment.getFileUrl());
					// BaoBaoWDApplication.mPhotoBitmap.display(lImageView,
					// smallFileUrl,
					// ConversionHelper.dipToPx(90, context),
					// ConversionHelper.dipToPx(70, context));
					ImageLoader.getInstance().displayImage(smallFileUrl,
							lImageView, BaoBaoWDApplication.mOptions);
					lImageView.setOnClickListener(new OnClickListener()
					{

						@Override
						public void onClick(View v)
						{
							if (allattachmentList != null)
								AttachmentHelper.goShowImageActivity(context,
										allattachmentList, lAttachment);
						}
					});
				}
			}
		}
		else
		{
			mContentImgLayout.setVisibility(View.GONE);
		}
	}

	public static void showTopicPreImg(final Context context,
			final ArrayList<Attachment> attachmentList,
			final ImageView mContentImgLayout, final ProgressBar mProgressBar)
	{
		if (mContentImgLayout == null)
		{
			return;
		}
		DisplayImageOptions mOptions = new DisplayImageOptions.Builder()
				.showImageOnLoading(R.drawable.topic_list_default_img)
				.showImageForEmptyUri(R.drawable.topic_list_default_img)
				.showImageOnFail(R.drawable.topic_list_default_img)
				.cacheInMemory(true).cacheOnDisk(true).considerExifParams(true)
				// .imageScaleType(ImageScaleType.EXACTLY_STRETCHED)
				.bitmapConfig(Bitmap.Config.RGB_565).build();
		if (attachmentList != null && attachmentList.size() > 0)
		{
			final Attachment lAttachment = attachmentList.get(0);
			String smallFileUrl = lAttachment.getFileUrl();
			smallFileUrl = AttachmentHelper.getMeduimFileUrl(context,
					lAttachment.getFileUrl());
			ImageLoader.getInstance().displayImage(smallFileUrl,
					mContentImgLayout, mOptions,
					new SimpleImageLoadingListener()
					{

						@Override
						public void onLoadingCancelled(String imageUri,
								View view)
						{
							mProgressBar.setVisibility(View.GONE);
							// mContentImgLayout
							// .setImageResource(R.drawable.topic_list_default_img);
							super.onLoadingCancelled(imageUri, view);
						}

						@Override
						public void onLoadingFailed(String imageUri, View view,
								FailReason failReason)
						{
							mProgressBar.setVisibility(View.GONE);
							mContentImgLayout
									.setImageResource(R.drawable.topic_list_default_img);
							super.onLoadingFailed(imageUri, view, failReason);
						}

						@Override
						public void onLoadingStarted(String imageUri, View view)
						{
							mProgressBar.setVisibility(View.VISIBLE);
							// mContentImgLayout
							// .setImageResource(R.drawable.topic_list_default_img);
							super.onLoadingStarted(imageUri, view);
						}

						@Override
						public void onLoadingComplete(String imageUri,
								View view, Bitmap loadedImage)
						{
							if (mProgressBar != null)
							{
								mProgressBar.setVisibility(View.GONE);
							}
							super.onLoadingComplete(imageUri, view, loadedImage);
						}
					});
		}
		else
		{
			// mContentImgLayout.setVisibility(View.GONE);
			mProgressBar.setVisibility(View.GONE);
			// 设置默认背景图
		}
	}

	public static void showBoardImgs(final Context context,
			final List<Attachment> attachmentList,
			RelativeLayout mContentImgLayout)
	{
		int sumChildCount = mContentImgLayout.getChildCount();
		if (attachmentList != null && attachmentList.size() > 0
				&& sumChildCount > 0)
		{
			mContentImgLayout.setVisibility(View.VISIBLE);
			for (int i = 0; i < sumChildCount; i++)
			{
				ImageView lImageView = (ImageView) mContentImgLayout
						.getChildAt(i);
				if (i >= attachmentList.size())
				{
					lImageView.setVisibility(View.GONE);
				}
				else
				{
					lImageView.setVisibility(View.VISIBLE);
					final Attachment lAttachment = attachmentList.get(i);
					String smallFileUrl = lAttachment.getFileUrl();
					// smallFileUrl = AttachmentHelper.getSmallFileUrl(context,
					// lAttachment.getFileUrl());
					// BaoBaoWDApplication.mPhotoBitmap.display(lImageView,
					// smallFileUrl,
					// ConversionHelper.dipToPx(30, context),
					// ConversionHelper.dipToPx(30, context));
					ImageLoader.getInstance().displayImage(smallFileUrl,
							lImageView, BaoBaoWDApplication.mOptions);
				}
			}
		}
		else
		{
			mContentImgLayout.setVisibility(View.GONE);
		}
	}

	public static void showReplyAttachment(final Context context,
			List<Attachment> attachmentList,
			final ArrayList<Attachment> allAttachmentList,
			ImageView lImageView, boolean clickEvent)
	{
		if (attachmentList != null && attachmentList.size() > 0)
		{
			lImageView.setVisibility(View.VISIBLE);
			final Attachment lAttachment = attachmentList.get(0);
			String smallFileUrl = lAttachment.getFileUrl();
			smallFileUrl = AttachmentHelper.getSmallFileUrl(context,
					lAttachment.getFileUrl());
			// BaoBaoWDApplication.mPhotoBitmap.display(lImageView,
			// smallFileUrl,
			// ConversionHelper.dipToPx(90, context),
			// ConversionHelper.dipToPx(70, context));
			ImageLoader.getInstance().displayImage(smallFileUrl, lImageView,
					BaoBaoWDApplication.mOptions);
			if (clickEvent)
			{
				lImageView.setOnClickListener(new OnClickListener()
				{

					@Override
					public void onClick(View v)
					{
						if (allAttachmentList != null)
						{
							AttachmentHelper.goShowImageActivity(context,
									allAttachmentList, lAttachment);
						}
					}
				});
			}
		}
		else
			lImageView.setVisibility(View.GONE);
	};

	public static void refreshAttachmentLayout(final Context context,
			final LinearLayout mAttachmentLayout, final List<String> mImgsPath)
	{
		mAttachmentLayout.removeAllViewsInLayout();
		if (mImgsPath.size() > 0)
		{
			mAttachmentLayout.setVisibility(View.VISIBLE);
			for (int i = 0; i < mImgsPath.size(); i++)
			{
				final String filePath = mImgsPath.get(i);
				View imgLayout = LayoutInflater.from(context).inflate(
						R.layout.addimg_layout, null);
				ImageView imageView = (ImageView) imgLayout
						.findViewById(R.id.img_iv);
				imageView.setImageBitmap(BitmapFactory.decodeFile(filePath));
				imgLayout.setOnClickListener(new OnClickListener()
				{

					@Override
					public void onClick(View v)
					{
						mImgsPath.remove(filePath);
						refreshAttachmentLayout(context, mAttachmentLayout,
								mImgsPath);
					}
				});
				mAttachmentLayout.addView(imgLayout);
			}
		}
		else
		{
			mAttachmentLayout.setVisibility(View.GONE);
		}
	}

	public static void saveImg(String fileLocalPath, List<String> mImgsPath)
	{
		Bitmap bitmap = ImageHelper.getBitmapByMaxSize(fileLocalPath,
				ImageHelper.IMGMAXSIZE);
		if (bitmap != null)
		{
			String filePath = ImageHelper.saveImgData(bitmap);
			mImgsPath.add(filePath);
			bitmap = ImageHelper.getSmallBitmap(bitmap, ImageHelper.IMGMINSIZE);
			String fileName = filePath.substring(filePath
					.lastIndexOf(File.separator) + 1);
			ImageHelper.saveSmallImgData(bitmap, fileName);
		}
	}

	public static String saveImg(String fileLocalPath, ImageView mImgIv)
	{
		Bitmap bitmap = ImageHelper.getBitmapByMaxSize(fileLocalPath,
				ImageHelper.IMGMAXSIZE);
		String lFilePath = null;
		if (bitmap != null)
		{
			lFilePath = ImageHelper.saveImgData(bitmap);
			bitmap = ImageHelper.getSmallBitmap(bitmap, ImageHelper.IMGMINSIZE);
			String fileName = lFilePath.substring(lFilePath
					.lastIndexOf(File.separator) + 1);
			ImageHelper.saveSmallImgData(bitmap, fileName);
			mImgIv.setImageBitmap(bitmap);
		}
		return lFilePath;
	}

	public static void showSubjectAttachment(ImageView infoIv,
			final ArrayList<Attachment> attachmentList)
	{
		infoIv.setImageResource(R.drawable.defaultsmallimg);
		if (attachmentList != null && attachmentList.size() > 0)
		{
			infoIv.setVisibility(View.VISIBLE);
			final Attachment lAttachment = attachmentList.get(0);
			if (lAttachment.getFileUrl() != null
					&& !lAttachment.getFileUrl().equals(""))
			{
				String smallFileUrl = lAttachment.getFileUrl();
				ImageLoader.getInstance().displayImage(smallFileUrl, infoIv,
						BaoBaoWDApplication.mOptions);
			}
		}
	}
	// public static class loadGifTask extends AsyncTask<Void, Void, byte[]>
	// {
	//
	// private File mImageUrlFile;
	// private ImageViewEx mImageView;
	// private Bitmap mLoadedImage;
	//
	// public loadGifTask(File file, ImageViewEx img, Bitmap loadedImage)
	// {
	// mImageUrlFile = file;
	// mImageView = img;
	// mLoadedImage = loadedImage;
	// }
	//
	// // @Override
	// // protected Movie doInBackground(Void... params)
	// // {
	// // Movie res = null;
	// // res = getMovie(mImageUrlFile);
	// // return res;
	// // }
	// //
	// // @Override
	// // protected void onPostExecute(Movie movie)
	// // {
	// // if (mImageView != null)
	// // {
	// // if (movie != null)
	// // {
	// // if ((movie.width() == 0 || movie.height() == 0))
	// // {
	// // mImageView.setImageBitmap(mLoadedImage);
	// // }
	// // else
	// // {
	// // // mImageView.s
	// // }
	// // }
	// // else
	// // {
	// // mImageView.setImageBitmap(mLoadedImage);
	// // }
	// // }
	// // super.onPostExecute(movie);
	// // }
	// // private Movie getMovie(File file)
	// // {
	// // Movie res = null;
	// // FileInputStream inputStream = null;
	// // try
	// // {
	// // if (file != null && file.exists())
	// // {
	// // inputStream = new FileInputStream(file);
	// // res = Movie.decodeStream(inputStream);
	// // }
	// // }
	// // catch (FileNotFoundException e)
	// // {
	// // // TODO Auto-generated catch block
	// // e.printStackTrace();
	// // }
	// // finally
	// // {
	// // if (inputStream != null)
	// // {
	// // try
	// // {
	// // inputStream.close();
	// // }
	// // catch (IOException e)
	// // {
	// // // TODO Auto-generated catch block
	// // e.printStackTrace();
	// // }
	// // }
	// // }
	// // return res;
	// // }
	// //
	// private byte[] getMovie(File file)
	// {
	// byte[] res = null;
	// FileInputStream inputStream = null;
	// try
	// {
	// if (file != null && file.exists())
	// {
	// inputStream = new FileInputStream(file);
	// res = new byte[inputStream.available()];
	// inputStream.read(res);
	// }
	// }
	// catch (IOException e)
	// {
	// // TODO Auto-generated catch block
	// e.printStackTrace();
	// }
	// finally
	// {
	// if (inputStream != null)
	// {
	// try
	// {
	// inputStream.close();
	// }
	// catch (IOException e)
	// {
	// // TODO Auto-generated catch block
	// e.printStackTrace();
	// }
	// }
	// }
	// return res;
	// }
	//
	// @Override
	// protected byte[] doInBackground(Void... params)
	// {
	// return getMovie(mImageUrlFile);
	// }
	//
	// @Override
	// protected void onPostExecute(byte[] movie)
	// {
	// if (mImageView != null)
	// {
	// if (movie != null && movie.length > 0)
	// {
	// mImageView.setSourceBlocking(movie);
	// }
	// else
	// {
	// mImageView.setImageBitmap(mLoadedImage);
	// }
	// }
	// super.onPostExecute(movie);
	// }
	// }
}
