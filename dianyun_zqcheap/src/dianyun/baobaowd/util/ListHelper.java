package dianyun.baobaowd.util;

import java.util.List;

public class ListHelper
{

	public static boolean isExist(List<String> strList, String str)
	{
		if (strList != null && strList.size() > 0)
		{
			for (String string : strList)
			{
				if (string.equals(str))
					return true;
			}
		}
		return false;
	}
}
