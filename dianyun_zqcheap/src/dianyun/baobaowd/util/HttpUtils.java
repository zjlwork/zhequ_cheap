package dianyun.baobaowd.util;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.params.ConnManagerParams;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.entity.BufferedHttpEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.CoreConnectionPNames;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import android.content.Context;
import android.os.Handler;
import android.view.View;

public class HttpUtils
{

	public static InputStream getRequestFromUrl(String urlString)
	{
		HttpURLConnection urlConnection = null;
		try
		{
			final URL url = new URL(urlString);
			urlConnection = (HttpURLConnection) url.openConnection();
			return urlConnection.getInputStream();
		}
		catch (Exception e)
		{
			e.printStackTrace();
			return null;
		}
	}

	public static boolean sendRequestByGet(String url) throws Exception
	{
		HttpGet get = new HttpGet(url);
		DefaultHttpClient client = new DefaultHttpClient();
		HttpResponse response = client.execute(get);
		HttpEntity entity = response.getEntity();
		int rescode = response.getStatusLine().getStatusCode();
		if (rescode == 200)
		{
			String code = EntityUtils.toString(entity, HTTP.UTF_8);
			if ("0".equals(code) || "10".equals(code))
			{
				return true;
			}
		}
		return false;
	}

	public static HttpClient getHttpClient()
	{
		HttpClient ahc;
		try
		{
			final Class<?> ahcClass = Class
					.forName("android.net.http.AndroidHttpClient");
			final Method newInstance = ahcClass.getMethod("newInstance",
					String.class);
			ahc = (HttpClient) newInstance.invoke(null, "ImageCache");
		}
		catch (final ClassNotFoundException e)
		{
			DefaultHttpClient dhc = new DefaultHttpClient();
			final HttpParams params = dhc.getParams();
			dhc = null;
			params.setIntParameter(CoreConnectionPNames.CONNECTION_TIMEOUT,
					20 * 1000);
			final SchemeRegistry registry = new SchemeRegistry();
			registry.register(new Scheme("http", PlainSocketFactory
					.getSocketFactory(), 80));
			registry.register(new Scheme("https", SSLSocketFactory
					.getSocketFactory(), 443));
			final ThreadSafeClientConnManager manager = new ThreadSafeClientConnManager(
					params, registry);
			ahc = new DefaultHttpClient(manager, params);
		}
		catch (final NoSuchMethodException e)
		{
			final RuntimeException re = new RuntimeException(
					"Programming error");
			re.initCause(e);
			throw re;
		}
		catch (final IllegalAccessException e)
		{
			final RuntimeException re = new RuntimeException(
					"Programming error");
			re.initCause(e);
			throw re;
		}
		catch (final InvocationTargetException e)
		{
			final RuntimeException re = new RuntimeException(
					"Programming error");
			re.initCause(e);
			throw re;
		}
		return ahc;
	}

	public static/* InputStream */String getRequest(Context context,
			String path, boolean save)
	{
		if (!TelInfo.isNetAvailable(context))
		{
			return null;
		}
		String data = null;
		try
		{
			HttpGet httpRequest = new HttpGet(path);
			// httpclient.getParams().setParameter(
			// CoreConnectionPNames.CONNECTION_TIMEOUT, 5000);
			// InputStream is = null;
			BasicHttpParams httpParameters = new BasicHttpParams();
			// Set the default soket timeout (SO_TIMEOUT)
			HttpConnectionParams
					.setConnectionTimeout(httpParameters, 20 * 1000);
			// in milliseconds which is the timeout for waiting for data.
			HttpConnectionParams.setSoTimeout(httpParameters, 20 * 1000);
			ConnManagerParams.setTimeout(httpParameters, 20 * 1000);
			HttpClient httpclient = new DefaultHttpClient(httpParameters);
			HttpResponse response = null;
			response = httpclient.execute(httpRequest);
			if (response.getStatusLine().getStatusCode() == HttpURLConnection.HTTP_OK)
			{
				HttpEntity entity = response.getEntity();
				data = EntityUtils.toString(entity, HTTP.UTF_8);
				// BufferedHttpEntity bufferedHttpEntity;
				// bufferedHttpEntity = new BufferedHttpEntity(entity);
				// is = bufferedHttpEntity.getContent();
			}
		}
		catch (ClientProtocolException e)
		{
			e.printStackTrace();
			return null;
		}
		catch (IOException e)
		{
			e.printStackTrace();
			return null;
		}
		catch (Exception e)
		{
			e.printStackTrace();
			return null;
		}
		return data;
	}

	public static InputStream getRequestStream(Context context, String path)
	{
		if (!TelInfo.isNetAvailable(context))
		{
			return null;
		}
		HttpGet httpRequest = new HttpGet(path);
		HttpClient httpclient = new DefaultHttpClient();
		InputStream is = null;
		HttpResponse response = null;
		try
		{
			response = httpclient.execute(httpRequest);
			if (response.getStatusLine().getStatusCode() == HttpURLConnection.HTTP_OK)
			{
				HttpEntity entity = response.getEntity();
				BufferedHttpEntity bufferedHttpEntity;
				bufferedHttpEntity = new BufferedHttpEntity(entity);
				is = bufferedHttpEntity.getContent();
			}
		}
		catch (ClientProtocolException e)
		{
			e.printStackTrace();
			return null;
		}
		catch (IOException e)
		{
			e.printStackTrace();
			return null;
		}
		catch (Exception e)
		{
			e.printStackTrace();
			return null;
		}
		return is;
	}

	public static byte[] readInputStream(InputStream inStream) throws Exception
	{
		ByteArrayOutputStream outSteam = new ByteArrayOutputStream();
		byte[] buffer = new byte[4096];
		int len = 0;
		while ((len = inStream.read(buffer)) != -1)
		{
			outSteam.write(buffer, 0, len);
		}
		outSteam.close();
		inStream.close();
		return outSteam.toByteArray();
	}

	public static byte[] getBytesFromUrl(Context context, String url)
			throws Exception
	{
		InputStream is = getRequestStream(context, url);
		if (is != null)
			return readInputStream(is);
		else
			return null;
	}

	public static String sendByPost(String path)
	{
		HttpPost post = new HttpPost(path);
		try
		{
			DefaultHttpClient client = new DefaultHttpClient();
			HttpResponse response = client.execute(post);
			HttpEntity entity = response.getEntity();
			if (response.getStatusLine().getStatusCode() == 200)
			{
				String e = EntityUtils.toString(entity);
				return e;
			}
		}
		catch (UnsupportedEncodingException e)
		{
			e.printStackTrace();
			return "";
		}
		catch (ClientProtocolException e)
		{
			e.printStackTrace();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		return "";
	}

	public static String PostString(String url, HashMap<String, String> params)
	{
		String res = "";
		List<NameValuePair> paramPairs = new ArrayList<NameValuePair>();
		if (params != null && !params.isEmpty())
		{
			for (Map.Entry<String, String> entry : params.entrySet())
			{
				paramPairs.add(new BasicNameValuePair(entry.getKey(), entry
						.getValue()));
			}
		}
		try
		{
			UrlEncodedFormEntity entitydata = new UrlEncodedFormEntity(
					paramPairs, HTTP.UTF_8);
			HttpPost post = new HttpPost(url);
			post.setEntity(entitydata);
			DefaultHttpClient client = new DefaultHttpClient();
			HttpResponse response = client.execute(post);
			HttpEntity entity = response.getEntity();
			if (response.getStatusLine().getStatusCode() == 200)
			{
				res = EntityUtils.toString(entity);
			}
		}
		catch (UnsupportedEncodingException e)
		{
			e.printStackTrace();
		}
		catch (ClientProtocolException e)
		{
			e.printStackTrace();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		return res;
	}

	public static boolean sendByPost(String url, HashMap<String, String> params)
	{
		List<NameValuePair> paramPairs = new ArrayList<NameValuePair>();
		if (params != null && !params.isEmpty())
		{
			for (Map.Entry<String, String> entry : params.entrySet())
			{
				paramPairs.add(new BasicNameValuePair(entry.getKey(), entry
						.getValue()));
			}
		}
		try
		{
			UrlEncodedFormEntity entitydata = new UrlEncodedFormEntity(
					paramPairs, HTTP.UTF_8);
			HttpPost post = new HttpPost(url);
			post.setEntity(entitydata);
			DefaultHttpClient client = new DefaultHttpClient();
			HttpResponse response = client.execute(post);
			HttpEntity entity = response.getEntity();
			if (response.getStatusLine().getStatusCode() == 200)
			{
				String strResult = EntityUtils.toString(entity);
				if ("0".equals(strResult))
					return true;
				else
					return false;
			}
		}
		catch (UnsupportedEncodingException e)
		{
			e.printStackTrace();
		}
		catch (ClientProtocolException e)
		{
			e.printStackTrace();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		return false;
	}

	public static void getFlow(final View convertView, final Handler handler)
	{
	}

	public static void sendOrderInfo(final String type, final Handler mHandle)
	{
	}
}
