
package dianyun.baobaowd.util;

import android.content.Context;
import android.content.Intent;
import dianyun.zqcheap.activity.HtmlActivity;


public class WebInsideHelper
{
	
	public static void goWebInside(Context context,String url,String title){
		Intent lIntent = new Intent(context,
				HtmlActivity.class);
		lIntent.putExtra(GobalConstants.Data.URL,url);
		lIntent.putExtra(GobalConstants.Data.TITLE,title);
		context.startActivity(lIntent);
	}
	
}
