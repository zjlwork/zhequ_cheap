package dianyun.baobaowd.util;

import java.util.ArrayList;
import java.util.List;

public class AppDownloadHelper
{

	private static List<String> downloadingList = new ArrayList<String>();
	private static List<String> downloadedList = new ArrayList<String>();

	public static List<String> getDownloadingList()
	{
		return downloadingList;
	}

	/*
	 * public static void setDownloadingList(List<String> downloadingList) {
	 * AppDownloadHelper.downloadingList = downloadingList; }
	 */
	public static List<String> getDownloadedList()
	{
		return downloadedList;
	}

	/*
	 * public static void setDownloadedList(List<String> downloadedList) {
	 * AppDownloadHelper.downloadedList = downloadedList; }
	 */
	public static boolean isDownloading(String packagename)
	{
		for (String str : downloadingList)
		{
			if (str.equals(packagename))
				return true;
		}
		return false;
	}

	public static boolean isDownloaded(String packagename)
	{
		for (String str : downloadedList)
		{
			if (str.equals(packagename))
				return true;
		}
		return false;
	}

	public static void addDownloaded(String packagename)
	{
		downloadedList.add(packagename);
	}

	public static void addDownloading(String packagename)
	{
		downloadingList.add(packagename);
	}

	public static void removeDownloading(String packagename)
	{
		if (isDownloading(packagename))
			downloadingList.remove(packagename);
	}
	/*
	 * Handler appDownLoadHelper =new Handler(){
	 * 
	 * public void handleMessage(android.os.Message msg) { switch (msg.what) {
	 * case SINAWEIBOAUTHSUCCESS: break;
	 * 
	 * } }; };
	 */
	/*
	 * int fileLength; int downloadFileLength; private void DownFile(String
	 * urlString) { InputStream inputStream; URLConnection connection;
	 * OutputStream outputStream; try { URL url=new URL(urlString);
	 * connection=url.openConnection(); if (connection.getReadTimeout()==15) {
	 * mNotifyHandler.setDownloadFileLength(-1); return; }
	 * inputStream=connection.getInputStream();
	 * 
	 * } catch (IOException e) { LogFile.SaveExceptionLog(e);
	 * mNotifyHandler.setDownloadFileLength(-1); return; }
	 * 
	 * 
	 * 文件的保存路径和和文件名其中Nobody.mp3是在手机SD卡上要保存的路径，如果不存在则新建
	 * 
	 * String fileName =
	 * urlString.substring(urlString.lastIndexOf("/")+1,urlString
	 * .lastIndexOf("?"));
	 * 
	 * String savePath=FileHelper.getDataPath()+fileName; File file =new
	 * File(savePath); if (!file.exists()) { try { file.createNewFile(); } catch
	 * (IOException e) { LogFile.SaveExceptionLog(e) ;
	 * mNotifyHandler.setDownloadFileLength(-1); return; } } try {
	 * outputStream=new FileOutputStream(file);
	 * fileLength=connection.getContentLength(); byte [] buffer=new byte[1024];
	 * int temp =-1; mNotifyHandler.setFilelength(fileLength); while((temp =
	 * inputStream.read(buffer))!=-1){ outputStream.write(buffer,0,temp);
	 * downloadFileLength+=temp;
	 * mNotifyHandler.setDownloadFileLength(downloadFileLength); }
	 * mNotifyHandler.setLocalFilePath(savePath); outputStream.flush(); }catch
	 * (Exception e) { e.printStackTrace();
	 * mNotifyHandler.setDownloadFileLength(-1); return; }
	 * 
	 * }
	 */
}
