package dianyun.baobaowd.help;

import android.content.Context;
import android.net.ConnectivityManager;
import android.widget.Toast;

public class Util
{

	public static boolean checkNet(Context context)
	{
		if (hasConnectedNetwork(context))
		{
			return true;
		}
//		showToast(context, context.getResources().getString(R.string.no_network));
		return false;
	}

	public static boolean hasConnectedNetwork(Context context)
	{
		ConnectivityManager connectivity = (ConnectivityManager) context
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		if (connectivity == null)
		{
			return false;
		}
		return connectivity.getActiveNetworkInfo() != null;
	}

	public static void showToast(final Context context, final String s)
	{
		// MainActivity.getHandler().post(
		// new Runnable() {
		//
		// @Override
		// public void run() {
		// Toast.makeText(context, s, Toast.LENGTH_SHORT).show();
		// }
		// }
		// );
		System.out.println("showToast====" + s);
	}

	public static void showToastLong(Context context, String s)
	{
		Toast.makeText(context, s, Toast.LENGTH_LONG).show();
	}
}
