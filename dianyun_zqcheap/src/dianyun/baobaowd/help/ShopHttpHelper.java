package dianyun.baobaowd.help;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Dialog;
import android.content.Context;
import android.os.AsyncTask;
import android.text.TextUtils;
import android.util.Log;

import com.google.gson.reflect.TypeToken;

import dianyun.baobaowd.data.HotWord;
import dianyun.baobaowd.data.User;
import dianyun.baobaowd.data.ViewPicture;
import dianyun.baobaowd.db.LightDBHelper;
import dianyun.baobaowd.db.ShopDBHelper;
import dianyun.baobaowd.defineview.DianYunProgressDialog;
import dianyun.baobaowd.dto.ResultDTO;
import dianyun.baobaowd.entity.CateItem;
import dianyun.baobaowd.entity.LocalMenu;
import dianyun.baobaowd.entity.Menu;
import dianyun.baobaowd.entity.SecKill;
import dianyun.baobaowd.gson.GsonHelper;
import dianyun.baobaowd.serverinterface.ShopHttpRequest;
import dianyun.baobaowd.util.DialogHelper;
import dianyun.baobaowd.util.GobalConstants;
import dianyun.baobaowd.util.MenuHelper;
import dianyun.baobaowd.util.UserHelper;
import dianyun.baobaowd.util.Utils;
import dianyun.zqcheap.R;

public class ShopHttpHelper {

    public static final int MAXTIMEOUT = 60 * 1000;
    private static final String TAG = "ShopHttpHelper";

    public static void getFreezeYoCoins(Context context, long uid,boolean isShowDialog,
    		GetFreezeYoCoinsCallback callback) {
    	new GetFreezeYoCoinsAsnycTask(context, uid,isShowDialog, callback).execute();
    }
    public static void getMenu(Context context, boolean isShowDialog,long parentId,
                               GetMenuCallback callback) {
        new GetSecondMenusAsnycTask(context, isShowDialog,parentId, callback).execute();
    }
    public static void getMenu(Context context, boolean isShowDialog,
    		ShopDataCallback callback) {
//    	new GetSecondMenusAsnycTask(context, isShowDialog,parentId, callback).execute();
    }

    public static void getChildData(Context context, boolean isShowDialog,
                                    String menuID, String parentID, boolean isMenu, boolean isSave,
                                    ShopDataCallback callback) {
        new shopChildDataAsnycTask(context, isShowDialog, menuID, parentID,
                isMenu, isSave, callback).execute();
    }

    public static void getMoreChildData(Context context, boolean isShowDialog,
                                        int pageIndex, int pageSize, String parentID,
                                        ShopDataCallback callback) {
        new shopLoadMoreChildDataAsnycTask(context, isShowDialog, parentID,
                pageIndex, pageSize, callback).execute();
    }

    public static void getCheapData(Context context, boolean isShowDialog,
                                    int pageIndex, int pageSize,
                                    ShopDataCallback callback) {
        new shopLoadCheapDataAsnycTask(context, isShowDialog, GobalConstants.URL.NINE_NINECHEEAPEST,
                pageIndex, pageSize, callback).execute();
    }
    public static void getMiaoShaData(Context context, boolean isShowDialog,
    		MiaoShaDataCallback callback) {
    	new MiaoShaDataAsnycTask(context, isShowDialog, callback).execute();
    }
    public static void miaoSha(Context context, boolean isShowDialog,long uid,long timeId,
    		MiaoShaCallback callback) {
    	new MiaoShaAsnycTask(context, isShowDialog,uid,timeId, callback).execute();
    }

    public static void getHighlestData(Context context, boolean isShowDialog,
                                       int pageIndex, int pageSize,
                                       ShopDataCallback callback) {
        new shopLoadHighlestDataAsnycTask(context, isShowDialog, GobalConstants.URL.MAIN_HIGHLESTID,
                pageIndex, pageSize, callback).execute();
    }
    public static void getFocusPictureData(Context context, boolean isShowDialog,
    		ShopDataCallback callback) {
    	new GetFocusPictureDataAsnycTask(context, isShowDialog,callback).execute();
    }
    public static void getMaoshaPictureData(Context context, boolean isShowDialog,
    		MiaoshaPictureCallback callback) {
    	new GetMiaoshaPictureDataAsnycTask(context, isShowDialog,callback).execute();
    }

    public static void bindTaoBaoAndYoYo(final Context context,
                                         final String uid, final String token, final String taoUserID,
                                         final String taoUserActarUrl, final String nickName) {
        new Thread() {

            @Override
            public void run() {
                int errorCount = 0;
                ResultDTO result = null;
                while (Utils.isNetAvailable(context)
                        && !LightDBHelper.getIsBind(context) && errorCount <= 3) {
                    result = ShopHttpRequest.getInstance(context).bindAccount(
                            uid, token, taoUserID, taoUserActarUrl, nickName);
                    if (result != null && result.getCode().equals("0")) {
                        User tempUser = UserHelper.getUser();
                        if (tempUser != null && tempUser.getIsGuest() == 0
                        		&& tempUser.getUid().toString().equals(result.getResult())) {
                        		 LightDBHelper.setIsBind(context, true);
                        }
                        break;
                    } else {
                        errorCount++;
                    }
                }
                super.run();
            }
        }.start();
    }

    public static void bindTaoBaoAndYoYo_Only(final Context context,
                                              final String uid, final String token, final String taoUserID,
                                              final String taoUserActarUrl, final String nickName) {
        new Thread() {

            @Override
            public void run() {
                ResultDTO result = ShopHttpRequest.getInstance(context)
                        .bindAccount(uid, token, taoUserID, taoUserActarUrl,
                                nickName);
                if (result != null && result.getCode().equals("0")) {
                    User tempUser = UserHelper.getUser();
                    if (tempUser != null && tempUser.getIsGuest() == 0 
                    		&& tempUser.getUid().toString().equals(result.getResult())) {
                    		 LightDBHelper.setIsBind(context, true);
                    }
                }
                super.run();
            }
        }.start();
    }

    public static void requestCheckUrl(Context context, String scan_url,
                                       ScanCheckCallback callback) {
        new shopScanCheckAsnycTask(context, scan_url, callback).execute();
    }

    public static void requestPrimaryWithUrl(Context context, String orignalUrl, String primaryWords,String order, String httpUrlString,
    		int pagesize, int pageIndex,int  startPrice,int endPrice,boolean isMall, PrimaryKeyWithUrlCallback callback) {
        new shopPrimaryKeyWithUrlRequestAsnycTask(context, orignalUrl, primaryWords,order, httpUrlString, pagesize, pageIndex
        		,  startPrice, endPrice, isMall, callback).execute();
    }
    public static void requestHotWords(Context context, long uid, String token, HotWordCallback callback) {
    	new HotWordsWithUrlRequestAsnycTask(context, uid, token, callback).execute();
    }

//    protected static class shopAsnycTask extends
//            AsyncTask<Void, Void, List<Menu>> {
//
//        private ShopDataCallback mCallback = null;
//        private Dialog mDialog = null;
//        private Context mContext;
//        private boolean mIsShowDialog;
//
//        public shopAsnycTask(Context context, boolean isShowDialog,
//                             ShopDataCallback callback) {
//            mContext = context;
//            mCallback = callback;
//            mIsShowDialog = isShowDialog;
//        }
//
//        @Override
//        protected void onPreExecute() {
//            if (mIsShowDialog) {
//                mDialog = DialogHelper.showProgressDialog(mContext, mContext
//                        .getResources().getString(R.string.loginloading));
//            }
//            super.onPreExecute();
//        }
//
//        @Override
//        protected void onPostExecute(List<Menu> result) {
//            if (mIsShowDialog && mDialog != null) {
//                mDialog.dismiss();
//            }
//            if (null != mCallback) {
//                mCallback.getMenu(result);
//            }
//            super.onPostExecute(result);
//        }
//
//        @Override
//        protected List<Menu> doInBackground(Void... params) {
//        	List<Menu> menuList =null;
//            ResultDTO requestResult = ShopHttpRequest.getInstance(mContext)
//                    .getMenu();
//            if (requestResult != null && requestResult.getCode().equals("0")) {
//            	 MenuHelper.deleteMenuListByParntId(mContext, 0);
//                String dataBody = requestResult.getResult();
//                if (!TextUtils.isEmpty(dataBody)) {
//                     menuList = GsonHelper.gsonToObj(dataBody,
//                            new TypeToken<List<Menu>>() {
//                            });
//                    MenuHelper.addMenuList(mContext, menuList);
//                }
//            }
//            return menuList;
//        }
//    }
    protected static class GetSecondMenusAsnycTask extends
    AsyncTask<Void, Void, List<Menu>> {
    	
    	private GetMenuCallback mCallback = null;
    	private Dialog mDialog = null;
    	private Context mContext;
    	private boolean mIsShowDialog;
    	private long parentId;
    	
    	public GetSecondMenusAsnycTask(Context context, boolean isShowDialog,long parentId,
    			GetMenuCallback callback) {
    		mContext = context;
    		mCallback = callback;
    		mIsShowDialog = isShowDialog;
    		this.parentId = parentId;
    	}
    	
    	@Override
    	protected void onPreExecute() {
    		if (mIsShowDialog) {
    			mDialog = DialogHelper.showProgressDialog(mContext, mContext
    					.getResources().getString(R.string.loginloading));
    		}
    		super.onPreExecute();
    	}
    	
    	@Override
    	protected void onPostExecute(List<Menu> result) {
    		if (mIsShowDialog && mDialog != null) {
    			mDialog.dismiss();
    		}
    		if(result!=null&&result.size()>0){
	    		MenuHelper.deleteMenuListByParntId(mContext, parentId);
				MenuHelper.addMenuList(mContext, result,parentId);
    		}
    		
    		
    		
    		if (null != mCallback) {
    			mCallback.getMenu(result,parentId);
    		}
    		super.onPostExecute(result);
    	}
    	
    	@Override
    	protected List<Menu> doInBackground(Void... params) {
    		List<Menu> menuList =null;
    		ResultDTO requestResult =null;
    		if(parentId==0)
    		 requestResult = ShopHttpRequest.getInstance(mContext)
    				.getMenu();
    		else
    			requestResult = ShopHttpRequest.getInstance(mContext)
				.getSecondMenus(parentId);
    		if (requestResult != null && requestResult.getCode().equals("0")) {
    			String dataBody = requestResult.getResult();
    			if (!TextUtils.isEmpty(dataBody)) {
    				menuList = GsonHelper.gsonToObj(dataBody,
    						new TypeToken<List<Menu>>() {
    				});
    				
    			}
    		}
    		return menuList;
    	}
    }
    protected static class GetFreezeYoCoinsAsnycTask extends
    AsyncTask<Void, Void, Integer> {
    	
    	private GetFreezeYoCoinsCallback mCallback = null;
    	private Dialog mDialog = null;
    	private Context mContext;
    	private boolean mIsShowDialog;
    	long uid;
    	
    	public GetFreezeYoCoinsAsnycTask(Context context, long uid,boolean isShowDialog,
    			GetFreezeYoCoinsCallback callback) {
    		mContext = context;
    		mCallback = callback;
    		mIsShowDialog = isShowDialog;
    		this.uid = uid;
    	}
    	
    	@Override
    	protected void onPreExecute() {
    		if (mIsShowDialog) {
    			mDialog = DialogHelper.showProgressDialog(mContext, mContext
    					.getResources().getString(R.string.loginloading));
    		}
    		super.onPreExecute();
    	}
    	
    	@Override
    	protected void onPostExecute(Integer result) {
    		if (mIsShowDialog && mDialog != null) {
    			mDialog.dismiss();
    		}
    		if (null != mCallback) {
    			mCallback.getResult(result);
    		}
    		super.onPostExecute(result);
    	}
    	
    	
    	@Override
    	protected Integer doInBackground(Void... params) {
    		Integer res = 0;
    		ResultDTO requestResult = ShopHttpRequest.getInstance(mContext)
    				.getFreenzeYoCoins(uid);
    		if (requestResult != null && requestResult.getCode().equals("0")) {
    			String dataBody = requestResult.getResult();
    			if (!TextUtils.isEmpty(dataBody)) {
    				res = Integer.parseInt(dataBody);
    			}
    			
    		}
    		return res;
    	}
    }

    protected static class shopChildDataAsnycTask extends
            AsyncTask<Void, Void, List<CateItem>> {

        private ShopDataCallback mCallback = null;
        private Dialog mDialog = null;
        private Context mContext;
        private boolean mIsShowDialog;
        private String mParentID;
        private String mMenuID;
        private boolean mIsMenu;
        private boolean mIsSave;

        public shopChildDataAsnycTask(Context context, boolean isShowDialog,
                                      String menuID, String parentID, boolean isMenu, boolean isSave,
                                      ShopDataCallback callback) {
            mContext = context;
            mCallback = callback;
            mIsShowDialog = isShowDialog;
            mParentID = parentID;
            mIsMenu = isMenu;
            mMenuID = menuID;
            mIsSave = isSave;
        }

        @Override
        protected void onPreExecute() {
            if (mIsShowDialog) {
                mDialog = DialogHelper.showProgressDialog(mContext, mContext
                        .getResources().getString(R.string.loginloading));
            }
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(List<CateItem> result) {
            if (mIsShowDialog && mDialog != null) {
                mDialog.dismiss();
            }
            if (null != mCallback) {
                mCallback.getChildsData(result);
            }
            super.onPostExecute(result);
        }

        @Override
        protected List<CateItem> doInBackground(Void... params) {
            List<CateItem> res = null;
            if (!mIsMenu && TextUtils.isEmpty(mParentID)) {
                return res;
            }
            ResultDTO requestResult = ShopHttpRequest.getInstance(mContext)
                    .getMenuChilds(mIsMenu ? mMenuID : mParentID);
            if (requestResult != null && requestResult.getCode().equals("0")) {
                String dataBody = requestResult.getResult();
                if (!TextUtils.isEmpty(dataBody)) {
                    res = GsonHelper.gsonToObj(dataBody,
                            new TypeToken<List<CateItem>>() {
                            });
                    // 插入数据库并更新
                    if (mIsSave) {
                        if (mIsMenu) {
                            ShopDBHelper.insertShopMenuChildData(mContext,
                                    mMenuID, dataBody);
                        } else {
                            ShopDBHelper.saveShopMenu_CategoryItemByCateID(
                                    mContext, mMenuID, mParentID, dataBody);
                        }
                    }
                }
                if (res == null) {
                    res = new ArrayList<CateItem>();
                }
            }
            return res;
        }
    }

    protected static class shopLoadMoreChildDataAsnycTask extends
            AsyncTask<Void, Void, List<CateItem>> {

        private ShopDataCallback mCallback = null;
        private Dialog mDialog = null;
        private Context mContext;
        private boolean mIsShowDialog;
        private String mParentID;
        private int mPageIndex;
        private int mPageSize;

        public shopLoadMoreChildDataAsnycTask(Context context,
                                              boolean isShowDialog, String parentID, int pageIndex,
                                              int pageSize, ShopDataCallback callback) {
            mContext = context;
            mCallback = callback;
            mIsShowDialog = isShowDialog;
            mParentID = parentID;
            mPageIndex = pageIndex;
            mPageSize = pageSize;
        }

        @Override
        protected void onPreExecute() {
            if (mIsShowDialog) {
                mDialog = DialogHelper.showProgressDialog(mContext, mContext
                        .getResources().getString(R.string.loginloading));
            }
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(List<CateItem> result) {
            if (mIsShowDialog && mDialog != null) {
                mDialog.dismiss();
            }
            if (null != mCallback) {
                mCallback.getChildsData(result);
            }
            super.onPostExecute(result);
        }

        @Override
        protected List<CateItem> doInBackground(Void... params) {
            List<CateItem> res = null;
            ResultDTO requestResult = ShopHttpRequest.getInstance(mContext)
                    .getMoreChilds(mParentID, mPageIndex, mPageSize);
            if (requestResult != null && requestResult.getCode().equals("0")) {
                String dataBody = requestResult.getResult();
                if (!TextUtils.isEmpty(dataBody)) {
                    res = GsonHelper.gsonToObj(dataBody,
                            new TypeToken<List<CateItem>>() {
                            });
                }
            }
            return res;
        }
    }


    protected static class shopLoadCheapDataAsnycTask extends
            AsyncTask<Void, Void, List<CateItem>> {

        private ShopDataCallback mCallback = null;
        private Dialog mDialog = null;
        private Context mContext;
        private boolean mIsShowDialog;
        private int mPageIndex;
        private int mPageSize;
        private String mParentID;


        public shopLoadCheapDataAsnycTask(Context context,
                                          boolean isShowDialog, String parentID, int pageIndex,
                                          int pageSize, ShopDataCallback callback) {
            mContext = context;
            mCallback = callback;
            mIsShowDialog = isShowDialog;
            mPageIndex = pageIndex;
            mPageSize = pageSize;
            mParentID = parentID;

        }

        @Override
        protected void onPreExecute() {
            if (mIsShowDialog) {
                mDialog = DialogHelper.showProgressDialog(mContext, mContext
                        .getResources().getString(R.string.loginloading));
            }
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(List<CateItem> result) {
            if (mIsShowDialog && mDialog != null) {
                mDialog.dismiss();
            }
            if (null != mCallback) {
                mCallback.getChildsData(result);
            }
            super.onPostExecute(result);
        }

        @Override
        protected List<CateItem> doInBackground(Void... params) {
            List<CateItem> res = null;
            ResultDTO requestResult =
                    ShopHttpRequest.getInstance(mContext)
                            .getCheapData(mParentID, mPageIndex, mPageSize);
            if (requestResult != null && requestResult.getCode().equals("0")) {
                String dataBody = requestResult.getResult();
                if (!TextUtils.isEmpty(dataBody)) {
                    res = GsonHelper.gsonToObj(dataBody,
                            new TypeToken<List<CateItem>>() {
                            });
                }
            }
            return res;
        }
    }
    protected static class MiaoShaDataAsnycTask extends
    AsyncTask<Void, Void, List<SecKill>> {
    	
    	private MiaoShaDataCallback mCallback = null;
    	private Dialog mDialog = null;
    	private Context mContext;
    	private boolean mIsShowDialog;
    	
    	
    	public MiaoShaDataAsnycTask(Context context,
    			boolean isShowDialog, MiaoShaDataCallback callback) {
    		mContext = context;
    		mCallback = callback;
    		mIsShowDialog = isShowDialog;
    		
    	}
    	
    	@Override
    	protected void onPreExecute() {
    		if (mIsShowDialog) {
    			mDialog = DialogHelper.showProgressDialog(mContext, mContext
    					.getResources().getString(R.string.loginloading));
    		}
    		super.onPreExecute();
    	}
    	
    	@Override
    	protected void onPostExecute(List<SecKill> result) {
    		if (mIsShowDialog && mDialog != null) {
    			mDialog.dismiss();
    		}
    		if (null != mCallback) {
    			mCallback.result(result);
    		}
    		super.onPostExecute(result);
    	}
    	
    	@Override
    	protected List<SecKill> doInBackground(Void... params) {
    		List<SecKill> res = null;
    		ResultDTO requestResult =
    				ShopHttpRequest.getInstance(mContext)
    				.getMiaoShaData();
    		if (requestResult != null && requestResult.getCode().equals("0")) {
    			String dataBody = requestResult.getResult();
    			if (!TextUtils.isEmpty(dataBody)) {
    				res = GsonHelper.gsonToObj(dataBody,
    						new TypeToken<List<SecKill>>() {
    				});
    			}
    		}
    		return res;
    	}
    }
    protected static class MiaoShaAsnycTask extends
    AsyncTask<Void, Void, ResultDTO> {
    	
    	private MiaoShaCallback mCallback = null;
    	private Dialog mDialog = null;
    	private Context mContext;
    	private boolean mIsShowDialog;
    	long uid;
    	long timeId;
    	
    	
    	public MiaoShaAsnycTask(Context context,
    			boolean isShowDialog,long uid,long timeId,  MiaoShaCallback callback) {
    		mContext = context;
    		mCallback = callback;
    		mIsShowDialog = isShowDialog;
    		this.uid = uid;
    		this.timeId = timeId;
    		
    	}
    	
    	@Override
    	protected void onPreExecute() {
    		if (mIsShowDialog) {
    			mDialog = DialogHelper.showProgressDialog(mContext, mContext
    					.getResources().getString(R.string.loginloading));
    		}
    		super.onPreExecute();
    	}
    	
    	@Override
    	protected void onPostExecute(ResultDTO result) {
    		if (mIsShowDialog && mDialog != null) {
    			mDialog.dismiss();
    		}
    		
    		
    		if (null != mCallback) {
    			mCallback.result(result);
    		}
    		super.onPostExecute(result);
    	}
    	
    	@Override
    	protected ResultDTO doInBackground(Void... params) {
    		return ShopHttpRequest.getInstance(mContext)
    				.miaoSha(uid, timeId);
    	}
    }


    protected static class shopLoadHighlestDataAsnycTask extends
            AsyncTask<Void, Void, List<CateItem>> {

        private final String mParentID;
        private ShopDataCallback mCallback = null;
        private Dialog mDialog = null;
        private Context mContext;
        private boolean mIsShowDialog;
        private int mPageIndex;
        private int mPageSize;

        public shopLoadHighlestDataAsnycTask(Context context,
                                             boolean isShowDialog, String parentID, int pageIndex,
                                             int pageSize, ShopDataCallback callback) {
            mContext = context;
            mCallback = callback;
            mIsShowDialog = isShowDialog;
            mPageIndex = pageIndex;
            mPageSize = pageSize;
            mParentID = parentID;

        }

        @Override
        protected void onPreExecute() {
            if (mIsShowDialog) {
                mDialog = DialogHelper.showProgressDialog(mContext, mContext
                        .getResources().getString(R.string.loginloading));
            }
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(List<CateItem> result) {
            if (mIsShowDialog && mDialog != null) {
                mDialog.dismiss();
            }
            if (null != mCallback) {
                mCallback.getChildsData(result);
            }
            super.onPostExecute(result);
        }

        @Override
        protected List<CateItem> doInBackground(Void... params) {
            List<CateItem> res = null;
            ResultDTO requestResult = ShopHttpRequest.getInstance(mContext)
                    .getHighlestData(mParentID, mPageIndex, mPageSize);
            if (requestResult != null && requestResult.getCode().equals("0")) {
                String dataBody = requestResult.getResult();
                if (!TextUtils.isEmpty(dataBody)) {
                    res = GsonHelper.gsonToObj(dataBody,
                            new TypeToken<List<CateItem>>() {
                            });
                }
            }
            return res;
        }
    }
    protected static class GetFocusPictureDataAsnycTask extends
    AsyncTask<Void, Void, List<CateItem>> {
    	
    	private ShopDataCallback mCallback = null;
    	private Dialog mDialog = null;
    	private Context mContext;
    	private boolean mIsShowDialog;
    	
    	public GetFocusPictureDataAsnycTask(Context context,
    			boolean isShowDialog,ShopDataCallback callback) {
    		mContext = context;
    		mCallback = callback;
    		mIsShowDialog = isShowDialog;
    		
    	}
    	
    	@Override
    	protected void onPreExecute() {
    		if (mIsShowDialog) {
    			mDialog = DialogHelper.showProgressDialog(mContext, mContext
    					.getResources().getString(R.string.loginloading));
    		}
    		super.onPreExecute();
    	}
    	
    	@Override
    	protected void onPostExecute(List<CateItem> result) {
    		if (mIsShowDialog && mDialog != null) {
    			mDialog.dismiss();
    		}
    		if(result!=null&&result.size()>0)
    		ShopDBHelper.insertFocusPictureData(mContext, GsonHelper.gsonTojson(result));
    		if (null != mCallback) {
    			mCallback.getChildsData(result);
    		}
    		super.onPostExecute(result);
    	}
    	
    	@Override
    	protected List<CateItem> doInBackground(Void... params) {
    		List<CateItem> res = null;
    		ResultDTO requestResult = ShopHttpRequest.getInstance(mContext)
    				.getFocusPictureData();
    		if (requestResult != null && requestResult.getCode().equals("0")) {
    			String dataBody = requestResult.getResult();
    			if (!TextUtils.isEmpty(dataBody)) {
    				res = GsonHelper.gsonToObj(dataBody,
    						new TypeToken<List<CateItem>>() {
    				});
    			}
    		}
    		return res;
    	}
    }
    protected static class GetMiaoshaPictureDataAsnycTask extends
    AsyncTask<Void, Void, List<ViewPicture>> {
    	
    	private MiaoshaPictureCallback mCallback = null;
    	private Dialog mDialog = null;
    	private Context mContext;
    	private boolean mIsShowDialog;
    	
    	public GetMiaoshaPictureDataAsnycTask(Context context,
    			boolean isShowDialog,MiaoshaPictureCallback callback) {
    		mContext = context;
    		mCallback = callback;
    		mIsShowDialog = isShowDialog;
    		
    	}
    	
    	@Override
    	protected void onPreExecute() {
    		if (mIsShowDialog) {
    			mDialog = DialogHelper.showProgressDialog(mContext, mContext
    					.getResources().getString(R.string.loginloading));
    		}
    		super.onPreExecute();
    	}
    	
    	@Override
    	protected void onPostExecute(List<ViewPicture> result) {
    		if (mIsShowDialog && mDialog != null) {
    			mDialog.dismiss();
    		}
    		if(result!=null&&result.size()>0)
    			ShopDBHelper.insertMiaoshaPictureData(mContext, GsonHelper.gsonTojson(result));
    		if (null != mCallback) {
    			mCallback.getChildsData(result);
    		}
    		super.onPostExecute(result);
    	}
    	
    	@Override
    	protected List<ViewPicture> doInBackground(Void... params) {
    		List<ViewPicture> res = null;
    		User user = UserHelper.getUser();
    		ResultDTO requestResult = ShopHttpRequest.getInstance(mContext)
    				.getMiaoShaPictureData(user.getUid(),user.getToken(),ViewPicture.VIEWTYPE_MIAOSHA);
    		if (requestResult != null && requestResult.getCode().equals("0")) {
    			String dataBody = requestResult.getResult();
    			if (!TextUtils.isEmpty(dataBody)) {
    				res = GsonHelper.gsonToObj(dataBody,
    						new TypeToken<List<ViewPicture>>() {
    				});
    			}
    		}
    		return res;
    	}
    }


    protected static class shopScanCheckAsnycTask extends
            AsyncTask<Void, Void, CateItem> {

        private ScanCheckCallback mCallback = null;
        private DianYunProgressDialog mProgressDialog = null;
        private Context mContext;
        private String mCheckUrl;
        private String mErrmsg = "";

        public shopScanCheckAsnycTask(Context context, String url,
                                      ScanCheckCallback callback) {
            mContext = context;
            mCallback = callback;
            mCheckUrl = url;
        }

        @Override
        protected void onPreExecute() {
            mProgressDialog = new DianYunProgressDialog(mContext,
                    R.style.dianyunProgressDialogStyle);
            mProgressDialog.show();
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(CateItem result) {
            if (mProgressDialog != null) {
                mProgressDialog.dismiss();
            }
            if (null != mCallback) {
                mCallback.getCheckResult(result, mErrmsg);
            }
            super.onPostExecute(result);
        }

        @Override
        protected CateItem doInBackground(Void... params) {
            CateItem res = null;
            String tempURL = mCheckUrl;
            if (!Util.checkNet(mContext)) {
                mErrmsg = mContext.getResources()
                        .getString(R.string.no_network);
                return res;
            }
            //增加一步查询
            ResultDTO requestResult = ShopHttpRequest.getInstance(mContext)
                    .getHttpCheckUrl(tempURL);
            if (requestResult != null) {
                if (requestResult.getCode().equals("0")) {
                    String dataBody = requestResult.getResult();
                    if (!TextUtils.isEmpty(dataBody)) {
//                        res = GsonHelper.gsonToObj(dataBody, CateItem.class);
                        //SET URL
                        tempURL = dataBody;
                    }
                }
            } else {
                return null;
            }

            URL url;
            HttpURLConnection conn;
            int code = 200;
            try {
                while (true) {
                    url = new URL(tempURL);
                    conn = (HttpURLConnection) url.openConnection();
                    conn.setInstanceFollowRedirects(false); // 取消重定向
                    conn.setConnectTimeout(MAXTIMEOUT);
                    conn.connect();
                    code = conn.getResponseCode();
                    if (code >= 300 && code < 309) {
                        tempURL = conn.getHeaderField("Location");
                        Log.d(TAG, "code:" + code + "=======url:" + tempURL);
                    } else {
                        break;
                    }
                }
            } catch (MalformedURLException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            if (TextUtils.isEmpty(tempURL)) {
                tempURL = mCheckUrl;
            }
            Log.e(TAG, tempURL);
            requestResult = ShopHttpRequest.getInstance(mContext)
                    .scanCheckUrl(tempURL);
            if (requestResult != null) {
                if (requestResult.getCode().equals("0")) {
                    String dataBody = requestResult.getResult();
                    if (!TextUtils.isEmpty(dataBody)) {
                        res = GsonHelper.gsonToObj(dataBody, CateItem.class);
                    }
                } else {
                    // 二次请求
                    if (!Util.checkNet(mContext)) {
                        mErrmsg = mContext.getResources().getString(
                                R.string.no_network);
                        return res;
                    }
                    requestResult = ShopHttpRequest.getInstance(mContext)
                            .scanCheckUrl(tempURL);
                    if (requestResult != null) {
                        if (requestResult.getCode().equals("0")) {
                            String dataBody = requestResult.getResult();
                            if (!TextUtils.isEmpty(dataBody)) {
                                res = GsonHelper.gsonToObj(dataBody,
                                        CateItem.class);
                            }
                        } else {
                            mErrmsg = requestResult.getErrorMsg();
                            LogFile.SaveLog("shop scan failed:"
                                    + "redirectUrl:" + tempURL
                                    + "\n original url:" + mCheckUrl);
                        }
                    }
                }
            }
            return res;
        }
    }

    protected static class shopPrimaryKeyWithUrlRequestAsnycTask extends
            AsyncTask<Void, Void, List<CateItem>> {

        private PrimaryKeyWithUrlCallback mCallback = null;
        private DianYunProgressDialog mProgressDialog = null;
        private Context mContext;
        private String mCheckUrl;
        private String mOrder;
        private String mErrmsg = "";
        /**
         * 文字描述
         */
        private String mDescString = "";
        /**
         * Http连接
         */
        private String mHttpString = "";

        private int mPageIndex = 1;
        private int mPageSize = 30;
        
        int  startPrice;int endPrice;boolean isMall;

        public shopPrimaryKeyWithUrlRequestAsnycTask(Context context, String orignalUrl, String primaryWords,String order, String httpUrlString, 
        		int pagesize, int pageIndex ,int  startPrice,int endPrice,boolean isMall,PrimaryKeyWithUrlCallback callback) {
            mContext = context;
            mCallback = callback;
            mCheckUrl = orignalUrl;
            mPageSize = pagesize;
            mPageIndex = pageIndex;
            mDescString = primaryWords;
            mHttpString = httpUrlString;
            mOrder = order;
            this.startPrice = startPrice;
            this.endPrice = endPrice;
            this.isMall = isMall;

        }

        @Override
        protected void onPreExecute() {
            mProgressDialog = new DianYunProgressDialog(mContext,
                    R.style.dianyunProgressDialogStyle);
            if (mPageIndex <= 1) {
                mProgressDialog.show();
            }
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(List<CateItem> result) {
            if (mProgressDialog != null) {
                mProgressDialog.dismiss();
            }
            if (null != mCallback) {
                mCallback.getResult(result, mDescString, mHttpString, mErrmsg);
            }
            super.onPostExecute(result);
        }

        @Override
        protected List<CateItem> doInBackground(Void... params) {
            List<CateItem> res = null;
            String tempURL = mCheckUrl;
            ResultDTO requestResult = null;
            if (mPageIndex <= 1) {
                if (!Util.checkNet(mContext)) {
                    mErrmsg = mContext.getResources()
                            .getString(R.string.no_network);
                    return res;
                }
                //增加一步查询
                requestResult = ShopHttpRequest.getInstance(mContext)
                        .getHttpWithPrimaryWords(tempURL);
                if (requestResult != null) {
                    if (requestResult.getCode().equals("0")) {
                        String dataBody = requestResult.getResult();
                        if (!TextUtils.isEmpty(dataBody)) {
//                        res = GsonHelper.gsonToObj(dataBody, CateItem.class);
                            try {
                                JSONObject jsonObject = new JSONObject(dataBody);
                                //SET URL
                                tempURL = jsonObject.getString("key");
                                //解析 文字+URL
                                mDescString = jsonObject.getString("value");
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                    else
                    {
                        mErrmsg=requestResult.getErrorMsg();
                    }
                } else {
                    return null;
                }
                if(TextUtils.isEmpty(mDescString))
                {
                    return null;
                }
                if (null != mCallback) {
                    mCallback.getDeseStr(mDescString);
                }
                if (!TextUtils.isEmpty(tempURL)) {
                    URL url;
                    HttpURLConnection conn;
                    int code = 200;
                    try {
                        while (true) {
                            url = new URL(tempURL);
                            conn = (HttpURLConnection) url.openConnection();
                            conn.setInstanceFollowRedirects(false); // 取消重定向
                            conn.setConnectTimeout(MAXTIMEOUT);
                            conn.connect();
                            code = conn.getResponseCode();
                            if (code >= 300 && code < 309) {
                                tempURL = conn.getHeaderField("Location");
                                Log.d(TAG, "code:" + code + "=======url:" + tempURL);
                            } else {
                                break;
                            }
                        }
                    } catch (MalformedURLException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    } catch (IOException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }
                if (TextUtils.isEmpty(tempURL)) {
                    tempURL = mCheckUrl;
                }
                else {
                    mHttpString = tempURL;
                }
            }
//            Log.e(TAG, tempURL);
            /**
             * 注意---------------这里一定要加上分页信息 以及 文字 和 httpurl---------------------
             */
            requestResult = ShopHttpRequest.getInstance(mContext)
                    .getCateItemListByHttpWithPrimaryWords(mHttpString, mDescString,mOrder, mPageSize, mPageIndex,startPrice,endPrice,isMall);
            if (requestResult != null) {
                if (requestResult.getCode().equals("0")) {
                    mDescString=requestResult.keywords;
                    if (null != mCallback) {
                        mCallback.getDeseStr(mDescString);
                    }
                    String dataBody = requestResult.getResult();
                    if (!TextUtils.isEmpty(dataBody)) {
                        res = GsonHelper.gsonToObj(dataBody, new TypeToken<List<CateItem>>() {
                        });
                    }
                } else {
                    if(true)
                    {
                        //新版本去掉搜索结果失败 重试case
                        return res;
                    }
                    // 二次请求
                    if (!Util.checkNet(mContext)) {
                        mErrmsg = mContext.getResources().getString(
                                R.string.no_network);
                        return res;
                    }
                    requestResult = ShopHttpRequest.getInstance(mContext)
                            .getCateItemListByHttpWithPrimaryWords(mHttpString, mDescString,mOrder, mPageSize, mPageIndex,startPrice,endPrice,isMall);
                    if (requestResult != null) {
                        if (requestResult.getCode().equals("0")) {
                            String dataBody = requestResult.getResult();
                            if (!TextUtils.isEmpty(dataBody)) {
                                res = GsonHelper.gsonToObj(dataBody, new TypeToken<List<CateItem>>() {
                                });

                            }
                        } else {
                            mErrmsg = requestResult.getErrorMsg();
                            LogFile.SaveLog("shop scan failed:"
                                    + "redirectUrl:" + tempURL
                                    + "\n original url:" + mCheckUrl);
                        }
                    }
                }
            }
            return res;
        }
    }
    
    
    protected static class HotWordsWithUrlRequestAsnycTask extends
    AsyncTask<Void, Void, List<HotWord>> {
    	
    	private HotWordCallback mCallback = null;
    	private DianYunProgressDialog mProgressDialog = null;
    	private Context mContext;
    	private long uid;
    	private String token;
    	
    	private int mPageIndex = 1;
    	private int mPageSize = 30;
    	
    	public HotWordsWithUrlRequestAsnycTask(Context context, long uid, String token,HotWordCallback callback) {
    		mContext = context;
    		mCallback = callback;
    		this.uid = uid;
    		this.token = token;
    		
    	}
    	
    	@Override
    	protected void onPreExecute() {
    		mProgressDialog = new DianYunProgressDialog(mContext,
    				R.style.dianyunProgressDialogStyle);
    		if (mPageIndex <= 1) {
    			mProgressDialog.show();
    		}
    		super.onPreExecute();
    	}
    	
    	@Override
    	protected void onPostExecute(List<HotWord> result) {
    		if (mProgressDialog != null) {
    			mProgressDialog.dismiss();
    		}
    		if (null != mCallback) {
    			mCallback.getResult(result);
    		}
    		super.onPostExecute(result);
    	}
    	
    	@Override
    	protected List<HotWord> doInBackground(Void... params) {
    		List<HotWord> res = null;
    		
    		ResultDTO   requestResult = ShopHttpRequest.getInstance(mContext)
    				.getHotWordList(uid, token);
    		if (requestResult != null) {
    			if (requestResult.getCode().equals("0")) {
    				String dataBody = requestResult.getResult();
    				if (!TextUtils.isEmpty(dataBody)) {
    					res = GsonHelper.gsonToObj(dataBody, new TypeToken<List<HotWord>>() {
    					});
    				}
    				return res;
    			} 
    		}
    		return res;
    	}
    }

    public interface ScanCheckCallback {

        public void getCheckResult(CateItem item, String errmsg);
    }

    /**
     * 关键字搜索的回调接口
     */
    public interface PrimaryKeyWithUrlCallback {

        /**
         * 获取文字和HTTPURL
         *
         * @param descString
         * @param httpString
         * @param errmsg
         */
//        public void getCheckResult(String descString, String httpString, String errmsg);

        /**
         * 获取商品列表集合
         *
         * @param itemList
         * @param errmsg
         */
        public void getResult(List<CateItem> itemList, String descString, String httpString, String errmsg);
        
        
        public void getDeseStr(String descString);
    }
    
    public interface HotWordCallback {
    	public void getResult(List<HotWord> wordList);
    }

    public interface ShopDataCallback {

        public void getMenu(List<LocalMenu> result);

        public void getChildsData(List<CateItem> result);
    }
    public interface MiaoshaPictureCallback {
    	public void getChildsData(List<ViewPicture> result);
    }
    public interface MiaoShaDataCallback {
    	
    	
    	public void result(List<SecKill> result);
    }
    
    public interface MiaoShaCallback {
    	
    	
    	public void result(ResultDTO result);
    }
    
    public interface GetMenuCallback {
    	
    	public void getMenu(List<Menu> result,long parentId);
    	
    }
    
    
    
    public interface GetFreezeYoCoinsCallback {
    	
    	public void getResult(Integer freezeYoCoins);
    	
    }
}
