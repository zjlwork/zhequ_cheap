package dianyun.baobaowd.help;

import java.io.PrintWriter;
import java.io.RandomAccessFile;
import java.io.StringWriter;
import java.io.Writer;
import java.text.SimpleDateFormat;

import dianyun.zqcheap.application.BaoBaoWDApplication;

public class LogFile {

	public static void SaveLog(String log) {/*
											 * if(log.length()>10001) log =
											 * log.substring(0,10000); String
											 * rootPath =
											 * FileHelper.getLogPath() +
											 * getDateString()+ "log.txt"; try {
											 * java.util.Date date = new
											 * java.util.Date();
											 * SimpleDateFormat dateFormat = new
											 * SimpleDateFormat(
											 * "yyyy-MM-dd HH:mm:ss"); String
											 * exceptionDate =
											 * dateFormat.format(date); //
											 * 将LOG输出到指定的文件下面 RandomAccessFile
											 * raf = new
											 * RandomAccessFile(rootPath, "rw");
											 * raf.seek(raf.length());
											 * raf.writeUTF("Time is " +
											 * exceptionDate + "\n log is " +
											 * log + "\n"); raf.close(); } catch
											 * (Exception e1) {
											 * e1.printStackTrace(); }
											 */
	}

	public static String getDateString() {
		java.util.Date date = new java.util.Date();
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		String lDate = dateFormat.format(date);
		return lDate;
	}

	public static void SaveErrorLog(String log) {
		String rootPath = FileHelper.getLogPath() + getDateString() + "_"
				+ BaoBaoWDApplication.versionCode + "_" + "error.txt";
		try {
			java.util.Date date = new java.util.Date();
			SimpleDateFormat dateFormat = new SimpleDateFormat(
					"yyyy-MM-dd HH:mm:ss");
			String exceptionDate = dateFormat.format(date);
			// 将LOG输出到指定的文件下面
			RandomAccessFile raf = new RandomAccessFile(rootPath, "rw");
			raf.seek(raf.length());
			raf.writeUTF("Time is " + exceptionDate + "\n log is " + log + "\n");
			raf.close();
		} catch (Exception e1) {
			e1.printStackTrace();
		}
	}

	public static void SaveXmppLog(String log) {/*
												 * String rootPath =
												 * FileHelper.getLogPath() +
												 * getDateString()+ "xmpp.txt";
												 * try { java.util.Date date =
												 * new java.util.Date();
												 * SimpleDateFormat dateFormat =
												 * new SimpleDateFormat(
												 * "yyyy-MM-dd HH:mm:ss");
												 * String exceptionDate =
												 * dateFormat.format(date); //
												 * 将LOG输出到指定的文件下面
												 * RandomAccessFile raf = new
												 * RandomAccessFile(rootPath,
												 * "rw");
												 * raf.seek(raf.length());
												 * raf.writeUTF("Time is " +
												 * exceptionDate + "\n log is "
												 * + log + "\n"); raf.close(); }
												 * catch (Exception e1) {
												 * e1.printStackTrace(); }
												 */
	}

	public static void SaveXmppExceptionLog(Exception e) {/*
														 * e.printStackTrace();
														 * Writer info = new
														 * StringWriter();
														 * PrintWriter
														 * printWriter = new
														 * PrintWriter(info);
														 * e.printStackTrace
														 * (printWriter); String
														 * log =
														 * info.toString();
														 * String rootPath =
														 * FileHelper
														 * .getExceptionPath()+
														 * getDateString() +
														 * "xmpp.txt"; try {
														 * java.util.Date date =
														 * new java.util.Date();
														 * SimpleDateFormat
														 * dateFormat = new
														 * SimpleDateFormat(
														 * "yyyy-MM-dd HH:mm:ss"
														 * ); String
														 * exceptionDate =
														 * dateFormat
														 * .format(date); //
														 * 将LOG输出到指定的文件下面
														 * RandomAccessFile raf
														 * = new
														 * RandomAccessFile
														 * (rootPath, "rw");
														 * raf.
														 * seek(raf.length());
														 * raf
														 * .writeUTF("Time is "
														 * + exceptionDate +
														 * "\n log is " + log +
														 * "\n"); raf.close(); }
														 * catch (Exception e1)
														 * {
														 * e1.printStackTrace();
														 * }
														 */
	}

	public static void SaveExceptionLog(Exception e) {/*
		e.printStackTrace();
		Writer info = new StringWriter();
		PrintWriter printWriter = new PrintWriter(info);
		e.printStackTrace(printWriter);
		String log = info.toString();
		String rootPath = FileHelper.getExceptionPath() + getDateString()
				+ "exception.txt";
		try {
			java.util.Date date = new java.util.Date();
			SimpleDateFormat dateFormat = new SimpleDateFormat(
					"yyyy-MM-dd HH:mm:ss");
			String exceptionDate = dateFormat.format(date); // 将LOG输出到指定的文件下面
			RandomAccessFile raf = new RandomAccessFile(rootPath, "rw");
			raf.seek(raf.length());
			raf.writeUTF("Time is " + exceptionDate + "\n log is " + log + "\n");
			raf.close();
		} catch (Exception e1) {
			e1.printStackTrace();
		}

	*/}
}
