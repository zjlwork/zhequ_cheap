package dianyun.baobaowd.help;

import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.UUID;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import android.content.Context;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;

public class FileHelper
{

	private static final String ROOTFOLD = "dianyun_zqcheap";
	
	public static void deleteSDCardFolder(File dir) {
	       File to = new File(dir.getAbsolutePath() + System.currentTimeMillis());
	       dir.renameTo(to);
	       if (to.isDirectory()) {
	           String[] children = to.list();
	           for (int i = 0; i < children.length; i++) {
	               File temp = new File(to, children[i]);
	               if (temp.isDirectory()) {
	                   deleteSDCardFolder(temp);
	               } else {
	                   boolean b = temp.delete();
	                   if (b == false) {
//	                       Log.d("deleteSDCardFolder", "DELETE FAIL");
	                   }
	               }
	           }
	           to.delete();
	       }
	
	}
	
	
	// 递归删除
	public static void recursionDeleteFile(File file)
	{
		try
		{
			if (file == null  || !file.exists())
			{
				return;
			}
			// File file = context.getExternalCacheDir();
			if (file.isFile())
			{
				file.delete();
				return;
			}
			if (file.isDirectory())
			{
				File[] childFile = file.listFiles();
				if (childFile == null || childFile.length == 0)
				{
					file.delete();
					return;
				}
				for (File f : childFile)
				{
					recursionDeleteFile(f);
				}
				file.delete();
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	public static void copyFile(Context context, String from, String to)
	{
		// 例：from:890.salid;
		// to:/mnt/sdcard/to/890.salid
		try
		{
			// int bytesum = 0;
			int byteread = 0;
			File tofile = new File(to);// from为assest中所要写到的sdcard中的名称
			if (!tofile.exists())
			{
				tofile.createNewFile();
			}
			InputStream inStream = context.getResources().getAssets()
					.open(from);// 将assets中的内容以流的形式展示出来
			OutputStream fs = new BufferedOutputStream(new FileOutputStream(to));// to为要写入sdcard中的文件名称
			byte[] buffer = new byte[1024];
			while ((byteread = inStream.read(buffer)) != -1)
			{
				// bytesum += byteread;
				fs.write(buffer, 0, byteread);
			}
			inStream.close();
			fs.close();
		}
		catch (Exception e)
		{
		}
	}

	public static void copyFile(String from, String to)
	{
		try
		{
			int byteread = 0;
			File tofile = new File(to);
			if (!tofile.exists())
			{
				tofile.createNewFile();
			}
			InputStream inStream = new FileInputStream(new File(from));
			OutputStream fs = new BufferedOutputStream(new FileOutputStream(to));
			byte[] buffer = new byte[1024];
			while ((byteread = inStream.read(buffer)) != -1)
			{
				fs.write(buffer, 0, byteread);
			}
			inStream.close();
			fs.close();
		}
		catch (Exception e)
		{
		}
	}
	public static void copyFile(File from, String to)
	{
		try
		{
			int byteread = 0;
			File tofile = new File(to);
			if (!tofile.exists())
			{
				tofile.createNewFile();
			}
			InputStream inStream = new FileInputStream(from);
			OutputStream fs = new BufferedOutputStream(new FileOutputStream(to));
			byte[] buffer = new byte[1024];
			while ((byteread = inStream.read(buffer)) != -1)
			{
				fs.write(buffer, 0, byteread);
			}
			inStream.close();
			fs.close();
		}
		catch (Exception e)
		{
		}
	}

	public static String downloadImage(String url) throws IOException
	{
		if (url == null)
			return null;
		DefaultHttpClient lClient = new DefaultHttpClient();
		HttpGet lHttpGet = new HttpGet(url);
		HttpResponse lRes = lClient.execute(lHttpGet);
		System.out.println("getAvatar-getParams--"
				+ lRes.getParams().getParameter("content-length"));
		if (lRes.getStatusLine().getStatusCode() == 200)
		{
			InputStream lIs = lRes.getEntity().getContent();
			String suffix = url.substring(url.lastIndexOf("."));
			String filePath = getDataPath() + UUID.randomUUID().toString()
					+ suffix;
			File file = new File(filePath);
			if(!file.exists())file.createNewFile();
			FileOutputStream out = new FileOutputStream(file);
			int l = 0;
			byte[] b = new byte[1024];
			while ((l = lIs.read(b)) != -1)
			{
				out.write(b, 0, l);
			}
			out.flush();
			out.close();
			return filePath;
		}
		return null;
	}
	public static String downloadWelcomePic(String url) throws IOException
	{
		if (url == null)
			return null;
		DefaultHttpClient lClient = new DefaultHttpClient();
		HttpGet lHttpGet = new HttpGet(url);
		HttpResponse lRes = lClient.execute(lHttpGet);
		System.out.println("getAvatar-getParams--"
				+ lRes.getParams().getParameter("content-length"));
		if (lRes.getStatusLine().getStatusCode() == 200)
		{
			InputStream lIs = lRes.getEntity().getContent();
			String suffix = url.substring(url.lastIndexOf("."));
			String filePath = getWelcomePicDataPath() + UUID.randomUUID().toString()
					+ suffix;
			File file = new File(filePath);
			if(!file.exists())file.createNewFile();
			FileOutputStream out = new FileOutputStream(file);
			int l = 0;
			byte[] b = new byte[1024];
			while ((l = lIs.read(b)) != -1)
			{
				out.write(b, 0, l);
			}
			out.flush();
			out.close();
			return filePath;
		}
		return null;
	}

	public static void sendMsg(int flag, Handler handler)
	{
		Message msg = new Message();
		msg.what = flag;
		handler.sendMessage(msg);
	}

	public static byte[] toByteArray(InputStream in) throws IOException
	{
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		copy(in, out);
		return out.toByteArray();
	}
	private static final int BUFFER_SIZE = 0x1000;

	public static long copy(InputStream from, OutputStream to)
			throws IOException
	{
		byte[] buf = new byte[BUFFER_SIZE];
		long total = 0;
		while (true)
		{
			int r = from.read(buf);
			if (r == -1)
			{
				break;
			}
			to.write(buf, 0, r);
			total += r;
		}
		return total;
	}

	public static String getAvatarPath()
	{
		String folderPath = Environment.getExternalStorageDirectory()
				.toString()
				+ File.separator
				+ ROOTFOLD
				+ File.separator
				+ "avatar" + File.separator;
		setNoMediaPath(folderPath);
		File folder = new File(folderPath);
		if (!folder.isDirectory())
		{
			folder.mkdirs();
		}
		return folderPath;
	}


	public static String getTempPath()
	{
		String folderPath = Environment.getExternalStorageDirectory()
				.toString()
				+ File.separator
				+ ROOTFOLD
				+ File.separator
				+ "temppath" + File.separator;
		setNoMediaPath(folderPath);
		File folder = new File(folderPath);
		if (!folder.isDirectory())
		{
			folder.mkdirs();
		}
		return folderPath;
	}

	public static String getUserFavDataPath()
	{
		String folderPath = Environment.getExternalStorageDirectory()
				.toString()
				+ File.separator
				+ ROOTFOLD
				+ File.separator
				+ "baobaowduserfav" + File.separator;
		File folder = new File(folderPath);
		if (!folder.isDirectory())
		{
			folder.mkdirs();
		}
		return folderPath;
	}

	public static String getDownloadAppsPath()
	{
		String folderPath = Environment.getExternalStorageDirectory()
				.toString()
				+ File.separator
				+ ROOTFOLD
				+ File.separator
				+ "apps" + File.separator;
		File folder = new File(folderPath);
		if (!folder.isDirectory())
		{
			folder.mkdirs();
		}
		return folderPath;
	}

	public static String getDataPath()
	{
		String folderPath = Environment.getExternalStorageDirectory()
				.toString()
				+ File.separator
				+ ROOTFOLD
				+ File.separator
				+ "data" + File.separator;
		setNoMediaPath(folderPath);
		File folder = new File(folderPath);
		if (!folder.isDirectory())
		{
			folder.mkdirs();
		}
		return folderPath;
	}
	public static String getWelcomePicDataPath()
	{
		String folderPath = Environment.getExternalStorageDirectory()
				.toString()
				+ File.separator
				+ ROOTFOLD
				+ File.separator
				+ "welcomePicdata" + File.separator;
		setNoMediaPath(folderPath);
		File folder = new File(folderPath);
		if (!folder.isDirectory())
		{
			folder.mkdirs();
		}
		return folderPath;
	}
	public static String getMyAppPath()
	{
		String folderPath = Environment.getExternalStorageDirectory()
				.toString()
				+ File.separator
				+ ROOTFOLD
				+ File.separator
				+ "myapp" + File.separator;
		setNoMediaPath(folderPath);
		File folder = new File(folderPath);
		if (!folder.isDirectory())
		{
			folder.mkdirs();
		}
		return folderPath;
	}

	public static String getSmallImgPath()
	{
		String folderPath = Environment.getExternalStorageDirectory()
				.toString()
				+ File.separator
				+ ROOTFOLD
				+ File.separator
				+ "smallImg" + File.separator;
		setNoMediaPath(folderPath);
		File folder = new File(folderPath);
		if (!folder.isDirectory())
		{
			folder.mkdirs();
		}
		return folderPath;
	}

	public static String getYunYingPicPath()
	{
		String folderPath = Environment.getExternalStorageDirectory()
				.toString()
				+ File.separator
				+ ROOTFOLD
				+ File.separator
				+ "yunyingpic" + File.separator;
		setNoMediaPath(folderPath);
		File folder = new File(folderPath);
		if (!folder.isDirectory())
		{
			folder.mkdirs();
		}
		return folderPath;
	}

	public static String getLogPath()
	{
		String folderPath = Environment.getExternalStorageDirectory()
				.toString()
				+ File.separator
				+ ROOTFOLD
				+ File.separator
				+ "log" + File.separator;
		File folder = new File(folderPath);
		if (!folder.isDirectory())
		{
			folder.mkdirs();
		}
		return folderPath;
	}

	public static String setNoMediaPath(String path)
	{
		String folderPath = path + ".nomedia/";
		File folder = new File(folderPath);
		if (!folder.isDirectory())
		{
			folder.mkdirs();
		}
		return folderPath;
	}

	public static String getExceptionPath()
	{
		String folderPath = Environment.getExternalStorageDirectory()
				.toString()
				+ File.separator
				+ ROOTFOLD
				+ File.separator
				+ "exception" + File.separator;
		File folder = new File(folderPath);
		if (!folder.isDirectory())
		{
			folder.mkdirs();
		}
		return folderPath;
	}

	public static String getMusicPath()
	{
		String folderPath = Environment.getExternalStorageDirectory()
				.toString()
				+ File.separator
				+ ROOTFOLD
				+ File.separator
				+ "music" + File.separator;
		File folder = new File(folderPath);
		if (!folder.isDirectory())
		{
			folder.mkdirs();
		}
		return folderPath;
	}

	public static String getTempMusicPath()
	{
		String folderPath = Environment.getExternalStorageDirectory()
				.toString()
				+ File.separator
				+ ROOTFOLD
				+ File.separator
				+ "tempmusic" + File.separator;
		File folder = new File(folderPath);
		if (!folder.isDirectory())
		{
			folder.mkdirs();
		}
		return folderPath;
	}
}
