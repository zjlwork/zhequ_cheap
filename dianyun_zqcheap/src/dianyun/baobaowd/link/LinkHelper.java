package dianyun.baobaowd.link;

import java.util.Iterator;
import java.util.Set;
import java.util.regex.Pattern;
import android.content.Intent;
import android.net.Uri;
import android.text.util.Linkify;
import android.widget.TextView;


public class LinkHelper
{
	private static final Uri PROFILE_URI = Uri.parse(Defs.MENTIONS_SCHEMA);
	
	  public static void extractUrl2Link(TextView v) {
		  try{
			  Pattern wikiWordMatcher = Pattern.compile("(http|https)+://[^\\s]*");
//			  Pattern wikiWordMatcher = Pattern.compile("(http|https)+:\\/\\/(\\w+(-\\w+)*)(\\.(\\w+(-\\w+)*))*(\\?\\S*)?.*(\\W)\\b");
//				Pattern wikiWordMatcher = Pattern.compile("(((http\\:\\/\\/)|(https\\:\\/\\/)|(www\\.))[a-zA-Z0-9_\\.]+)");
//				Pattern wikiWordMatcher = Pattern.compile("https?:\\/\\/(([a-zA-Z0-9_-])+(\\.)?)*(:\\d+)?(\\/((\\.)?(\\?)?=?&?[a-zA-Z0-9_-](\\?)?)*)*$");
				
				String mentionsScheme = String.format("%s/?%s=", Defs.MENTIONS_SCHEMA, Defs.PARAM_UID);
//				String mentionsScheme = String.format("%s", Defs.MENTIONS_SCHEMA);
				Linkify.addLinks(v, wikiWordMatcher, mentionsScheme);
		  }catch(Exception e){
			  e.printStackTrace();
		  }
		  
		  
		  

		
	  } 
	  
	  public static String  extractUidFromUri(Intent intent) {
		  String url ="";
		  try{
			  Uri uri = intent.getData();
				if (uri !=null && PROFILE_URI.getScheme().equals(uri.getScheme())) {
//					url = uri.getQueryParameter(Defs.PARAM_UID);
//					Set<String> lSet = uri.getQueryParameterNames();
//					Iterator<String> i = lSet.iterator();//先迭代出来  
//			          
//			        while(i.hasNext()){
//			        	String name = i.next();
//			        	if(!name.equals(Defs.PARAM_UID))
//			        		url+="&"+name+"="+uri.getQueryParameter(name);
//			        } 
			        
					url = uri.toString();
					String param = Defs.PARAM_UID+"=";
					int beginIndex = url.indexOf(param)+param.length();
			        if(beginIndex!=-1){
			        	url = url.substring(beginIndex);
			        }
			        
			        
			        
			        
			        
					
			    }
				/*if(url.indexOf("www") == 0){
					url = "http://"+url;
				}else*/ if(url.indexOf("https") == 0){
					String bUid = url.substring(5, url.length());
					url = "http"+bUid;
				}
		  }catch(Exception e){
			  e.printStackTrace();
		  }
		
			
			return url;
			
			
			
			
			
			
		}
	  
	  
}
