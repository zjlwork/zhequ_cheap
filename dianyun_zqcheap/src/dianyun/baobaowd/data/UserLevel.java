package dianyun.baobaowd.data;

public class UserLevel
{

	// 当前等级
	private Integer curLevel = 0;
	// 当前经验
	private Integer userW = 0;
	// 当前等级所需经验
	private Integer curLevelW = 0;
	// 下一级所需经验
	private Integer nextLevelW = 0;

	public Integer getCurLevel()
	{
		return curLevel;
	}

	public void setCurLevel(Integer curLevel)
	{
		this.curLevel = curLevel;
	}

	public Integer getUserW()
	{
		return userW;
	}

	public void setUserW(Integer userW)
	{
		this.userW = userW;
	}

	public Integer getCurLevelW()
	{
		return curLevelW;
	}

	public void setCurLevelW(Integer curLevelW)
	{
		this.curLevelW = curLevelW;
	}

	public Integer getNextLevelW()
	{
		return nextLevelW;
	}

	public void setNextLevelW(Integer nextLevelW)
	{
		this.nextLevelW = nextLevelW;
	}
}
