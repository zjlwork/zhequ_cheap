package dianyun.baobaowd.data;

public class UserShareUrl {
	
	
	public static final String KEY_FANLIDOWNAPP = "fanliDownApp";
	public static final String KEY_FANLIPROFILEAPP = "fanliProfileApp";
	public static final String KEY_FANLIINVITEUSERLIST = "fanliInviteUserList";
	
	public static final String KEY_FANLISHAREORDER = "fanliShareOrder";
	public static final String KEY_FANLISHAREORDERUNREBATE = "fanliShareOrderUnRebate";
	
	
//	折趣取：urlKey = fanliShareOrder
//	柚柚育儿取：urlKey = yoyoShareOrder
	
	

	// 标识Key
	private String urlKey;

	// 对应的appId
	private Integer appId;

	// url的值
	private String urlValue;

	// 分享对应的Icon
	private String urlIcon;

	// url标题
	private String urlTitle;

	// url说明
	private String urlDesc;

	// url分享时的文字内容
	private String urlContent;

	public UserShareUrl() {

	}
	
	

	public UserShareUrl(String urlKey, Integer appId, String urlValue,
			String urlIcon, String urlTitle, String urlDesc, String urlContent) {
		super();
		this.urlKey = urlKey;
		this.appId = appId;
		this.urlValue = urlValue;
		this.urlIcon = urlIcon;
		this.urlTitle = urlTitle;
		this.urlDesc = urlDesc;
		this.urlContent = urlContent;
	}






	public String getUrlKey() {
		return urlKey;
	}

	public void setUrlKey(String urlKey) {
		this.urlKey = urlKey;
	}

	public Integer getAppId() {
		return appId;
	}

	public void setAppId(Integer appId) {
		this.appId = appId;
	}

	public String getUrlValue() {
		return urlValue;
	}

	public void setUrlValue(String urlValue) {
		this.urlValue = urlValue;
	}

	public String getUrlIcon() {
		return urlIcon;
	}

	public void setUrlIcon(String urlIcon) {
		this.urlIcon = urlIcon;
	}

	public String getUrlTitle() {
		return urlTitle;
	}

	public void setUrlTitle(String urlTitle) {
		this.urlTitle = urlTitle;
	}

	public String getUrlDesc() {
		return urlDesc;
	}

	public void setUrlDesc(String urlDesc) {
		this.urlDesc = urlDesc;
	}

	public String getUrlContent() {
		return urlContent;
	}

	public void setUrlContent(String urlContent) {
		this.urlContent = urlContent;
	}
	
	
	
	
	
	
	
	

}
