package dianyun.baobaowd.data;

import android.os.Parcel;
import android.os.Parcelable;

public class ReceiveLetter implements Parcelable
{

	private Long uid;
	private String nickname;
	/**
	 * 头像图片的url
	 */
	private String profileImage;
	/**
	 * 内容
	 */
	private String content;
	private String postTime;
	private String msg;

	public String getMsg()
	{
		return msg;
	}

	public void setMsg(String msg)
	{
		this.msg = msg;
	}

	public Long getUid()
	{
		return uid;
	}

	public void setUid(Long uid)
	{
		this.uid = uid;
	}

	public String getNickname()
	{
		return nickname;
	}

	public void setNickname(String nickname)
	{
		this.nickname = nickname;
	}

	public String getProfileImage()
	{
		return profileImage;
	}

	public void setProfileImage(String profileImage)
	{
		this.profileImage = profileImage;
	}

	public String getContent()
	{
		return content;
	}

	public void setContent(String content)
	{
		this.content = content;
	}

	public String getPostTime()
	{
		return postTime;
	}

	public void setPostTime(String postTime)
	{
		this.postTime = postTime;
	}

	private void readFromParcel(Parcel in)
	{
		if (uid == null)
			uid = 0L;
		this.uid = in.readLong();
		this.nickname = in.readString();
		this.profileImage = in.readString();
		this.content = in.readString();
		this.postTime = in.readString();
	}

	public ReceiveLetter(Parcel source)
	{
		readFromParcel(source);
	}
	public static final Parcelable.Creator<ReceiveLetter> CREATOR = new Parcelable.Creator<ReceiveLetter>()
	{

		@Override
		public ReceiveLetter[] newArray(int size)
		{
			return new ReceiveLetter[size];
		}

		@Override
		public ReceiveLetter createFromParcel(Parcel source)
		{
			return new ReceiveLetter(source);
		}
	};

	@Override
	public int describeContents()
	{
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags)
	{
		if (uid == null)
			uid = 0L;
		dest.writeLong(uid);
		dest.writeString(nickname);
		dest.writeString(profileImage);
		dest.writeString(content);
		dest.writeString(postTime);
	}
}
