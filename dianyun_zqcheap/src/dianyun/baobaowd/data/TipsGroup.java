package dianyun.baobaowd.data;

import java.util.List;

public class TipsGroup
{

	private Integer groupId;
	private String name;
	private List<TipsKey> tipsKeyBaseDTOList;

	public TipsGroup()
	{
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name == null ? null : name.trim();
	}

	public Integer getGroupId()
	{
		return groupId;
	}

	public void setGroupId(Integer groupId)
	{
		this.groupId = groupId;
	}

	public List<TipsKey> getTipsKeyBaseDTOList()
	{
		return tipsKeyBaseDTOList;
	}

	public void setTipsKeyBaseDTOList(List<TipsKey> tipsKeyBaseDTOList)
	{
		this.tipsKeyBaseDTOList = tipsKeyBaseDTOList;
	}
}