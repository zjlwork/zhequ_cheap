package dianyun.baobaowd.data;

public class Order {
    //排序标号，由大到小
    private Long seqId;
    //订单创建,订单付款,订单成功,订单关闭,订单退款
    private String orderStatus;
    //金币数
    private Integer itemCoins=0;
    //商品图片
    private String picUrl;
    //商品数量
    private Integer itemAmount=0;
    //商品标题
    private String tbItemTitle;
    //商品促销价
    private String itemUpmPrice;
    //商品原价
    private String itemPrice;
    
    //0:审核中,1:已返利
    private Integer sendStatus=0;
    
    public static final int SENDSTATUS_AUDITING=0;
    public static final int SENDSTATUS_REBATESUCCESS=1;
    
    //预计返利百分比,1-99的数字,为空则为订单没有确认收货
    private Integer ycoinsReturnPer=0;
    //预计多少天后返利,为空则为订单没有确认收货
    private Integer ycoinsReturnDays=0;
    
    //2 已确认收货，0 未确认收货
    private Integer orderStatusCode;
    public static final int ORDERSTATUSCODE_NOTRECEIVE=0;
    public static final int ORDERSTATUSCODE_RECEIVE=2;
    
    
    //  淘宝混淆ID
    private String tbItemId;
    //1:淘宝，2:商城
    private Short itemType;
    //是否为淘客
    private Short isTk;
    private String taobaoPid;
    
    
    
    
    
    
    
    public String getTaobaoPid() {
		return taobaoPid;
	}


	public void setTaobaoPid(String taobaoPid) {
		this.taobaoPid = taobaoPid;
	}


	public String getTbItemId() {
		return tbItemId;
	}


	public void setTbItemId(String tbItemId) {
		this.tbItemId = tbItemId;
	}


	public Short getItemType() {
		if(itemType==null)itemType=0;
		return itemType;
	}


	public void setItemType(Short itemType) {
		this.itemType = itemType;
	}


	public Short getIsTk() {
		if(isTk==null)isTk=0;
		return isTk;
	}


	public void setIsTk(Short isTk) {
		this.isTk = isTk;
	}


	public Integer getOrderStatusCode() {
    	if(orderStatusCode==null)orderStatusCode=0;
		return orderStatusCode;
	}


	public void setOrderStatusCode(Integer orderStatusCode) {
		this.orderStatusCode = orderStatusCode;
	}


	public Integer getSendStatus() {
    	if(sendStatus==null)sendStatus=0;
		return sendStatus;
	}


	public void setSendStatus(Integer sendStatus) {
		this.sendStatus = sendStatus;
	}


	public Integer getYcoinsReturnPer() {
		if(ycoinsReturnPer==null)ycoinsReturnPer=0;
		return ycoinsReturnPer;
	}


	public void setYcoinsReturnPer(Integer ycoinsReturnPer) {
		this.ycoinsReturnPer = ycoinsReturnPer;
	}


	public Integer getYcoinsReturnDays() {
		if(sendStatus==null)ycoinsReturnDays=0;
		return ycoinsReturnDays;
	}


	public void setYcoinsReturnDays(Integer ycoinsReturnDays) {
		this.ycoinsReturnDays = ycoinsReturnDays;
	}


	public Order(){
    	
    }
    
	
	public Long getSeqId()
	{
		return seqId;
	}
	
	public void setSeqId(Long seqId)
	{
		this.seqId = seqId;
	}
	
	public String getOrderStatus()
	{
		return orderStatus;
	}
	
	public void setOrderStatus(String orderStatus)
	{
		this.orderStatus = orderStatus;
	}
	
	public Integer getItemCoins()
	{
		if(itemCoins==null)itemCoins=0;
		return itemCoins;
	}
	
	public void setItemCoins(Integer itemCoins)
	{
		this.itemCoins = itemCoins;
	}
	
	public String getPicUrl()
	{
		return picUrl;
	}
	
	public void setPicUrl(String picUrl)
	{
		this.picUrl = picUrl;
	}
	
	public Integer getItemAmount()
	{
		if(itemAmount==null)itemAmount=0;
		return itemAmount;
	}
	
	public void setItemAmount(Integer itemAmount)
	{
		this.itemAmount = itemAmount;
	}
	
	public String getTbItemTitle()
	{
		return tbItemTitle;
	}
	
	public void setTbItemTitle(String tbItemTitle)
	{
		this.tbItemTitle = tbItemTitle;
	}
	
	public String getItemUpmPrice()
	{
		return itemUpmPrice;
	}
	
	public void setItemUpmPrice(String itemUpmPrice)
	{
		this.itemUpmPrice = itemUpmPrice;
	}
	
	public String getItemPrice()
	{
		return itemPrice;
	}
	
	public void setItemPrice(String itemPrice)
	{
		this.itemPrice = itemPrice;
	}
    
    
    
    
    
    
    
    
    
    
    
    
}