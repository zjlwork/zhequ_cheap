package dianyun.baobaowd.data;

import java.lang.Double;

public class OnlineCount
{

	/**
	 * 用户数
	 */
	private Integer count;
	/**
	 * 计算比例
	 */
	private Double ratio;

	public OnlineCount()
	{
	}

	public Integer getCount()
	{
		return count;
	}

	public void setCount(Integer count)
	{
		this.count = count;
	}

	public Double getRatio()
	{
		return ratio;
	}

	public void setRatio(Double ratio)
	{
		this.ratio = ratio;
	}
}
