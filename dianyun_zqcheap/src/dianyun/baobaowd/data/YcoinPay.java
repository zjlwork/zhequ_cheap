package dianyun.baobaowd.data;

//提现或兑换金币申请记录
public class YcoinPay {
	//排序字段，倒序
    private Long payId;
 	//兑换数量，payType为提现时单位为分，需除于100
    private Integer payAmount;
	//兑换状态，-1：失败，0：审核中，1：成功
    private Integer payStatus;
	//申请记录时间
    private String createTime;
	//-1：提现，-2：兑换金币 -3兑换商品
	private Integer payType;
	
	
	private String giftDesc;
	private String imgUrl;
	
	private Integer ycoinPrice;
	
	public final static int PAYSTAUTS_FAILED=-1;
	public final static int PAYSTAUTS_AUDIT=0;
	public final static int PAYSTAUTS_SUCCESS=1;
	
	public final static int PAYTYPE_MONDY=-1;
	public final static int PAYTYPE_GOLD=-2;
	public final static int PAYTYPE_PRIZE=-3;
	
	
	
	
	
	
	
	
	public YcoinPay(){
		
	}
	
	
	
	
	
	
	public Integer getYcoinPrice() {
		if(ycoinPrice==null)ycoinPrice=0;
		return ycoinPrice;
	}






	public void setYcoinPrice(Integer ycoinPrice) {
		this.ycoinPrice = ycoinPrice;
	}






	public String getGiftDesc() {
		return giftDesc;
	}






	public void setGiftDesc(String giftDesc) {
		this.giftDesc = giftDesc;
	}






	public String getImgUrl() {
		return imgUrl;
	}






	public void setImgUrl(String imgUrl) {
		this.imgUrl = imgUrl;
	}






	public Long getPayId()
	{
		return payId;
	}
	
	public void setPayId(Long payId)
	{
		this.payId = payId;
	}
	
	public Integer getPayAmount()
	{
		return payAmount;
	}
	
	public void setPayAmount(Integer payAmount)
	{
		this.payAmount = payAmount;
	}
	
	public Integer getPayStatus()
	{
		return payStatus;
	}
	
	public void setPayStatus(Integer payStatus)
	{
		this.payStatus = payStatus;
	}
	
	public String getCreateTime()
	{
		return createTime;
	}
	
	public void setCreateTime(String createTime)
	{
		this.createTime = createTime;
	}
	
	public Integer getPayType()
	{
		return payType;
	}
	
	public void setPayType(Integer payType)
	{
		this.payType = payType;
	}
	
	
	
	
	
}