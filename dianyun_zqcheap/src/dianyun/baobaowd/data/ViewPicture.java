package dianyun.baobaowd.data;

import java.util.List;

public class ViewPicture
{

	
	public static final int VIEWTYPE_MAIN = 2;
	public static final int VIEWTYPE_FOUND = 3;
	public static final int VIEWTYPE_MIAOSHA = 4;
	
	
	
	
	
	
	
	public static final int JUMETYPE_BOARD = 1;
	public static final int JUMETYPE_APPINSIDE = 2;
	public static final int JUMETYPE_WEBINSIDE = 3;
	public static final int JUMETYPE_WEBOUTSIDE = 4;
	public static final int JUMETYPE_QUESTIONDETAIL= 5;
	public static final int JUMETYPE_TOPICDETAIL = 6;
	public static final int JUMETYPE_MIAOSHA = 7;
	
	
	
	public static final String JUMPVALUE_ASK = "1";
	public static final String JUMPVALUE_ARTICLE = "2";
	private Long seqId;
	// 类型
	private Integer viewType;
	// 跳转类型 (1:转到论坛版块；2：应用内其它页面；3：应用内打开网页；4应用外打开网页)
	private Integer jumpType;
	/*
	 * 跳转值 jumpType=1 (论坛版块对应的ID) jumpType=2 (1:直接提问；2：当日知识) jumpType=3 (url)
	 * jumpType=4 (url)
	 * jumpType=7(秒杀)   (1=秒杀;   2=高返利  3= 9.9 )
	 */
	
	private String jumpValue;
	// 标题
	private String title;
	// 替换规则( OnLineNumber:在线人数; BabyStatus:用户状态; )
	private String noteReplaceRule;

	// //图片列表
	 private List<Attachment> attachmentList;
	 private String imgUrl;
	public ViewPicture(Long seqId, Integer viewType, Integer jumpType,
			String jumpValue, String title, String noteReplaceRule,String imgUrl)
	{
		super();
		this.seqId = seqId;
		this.viewType = viewType;
		this.jumpType = jumpType;
		this.jumpValue = jumpValue;
		this.title = title;
		this.noteReplaceRule = noteReplaceRule;
		this.imgUrl = imgUrl;
	}
	
	
	
	

	
	
	public String getImgUrl()
	{
		return imgUrl;
	}






	
	public void setImgUrl(String imgUrl)
	{
		this.imgUrl = imgUrl;
	}






	public List<Attachment> getAttachmentList()
	{
		return attachmentList;
	}





	
	public void setAttachmentList(List<Attachment> attachmentList)
	{
		this.attachmentList = attachmentList;
	}





	public Long getSeqId()
	{
		return seqId;
	}

	public void setSeqId(Long seqId)
	{
		this.seqId = seqId;
	}

	public String getTitle()
	{
		return title;
	}

	public void setTitle(String title)
	{
		this.title = title;
	}

	public Integer getViewType()
	{
		return viewType;
	}

	public void setViewType(Integer viewType)
	{
		this.viewType = viewType;
	}

	public Integer getJumpType()
	{
		if(jumpType==null)jumpType=0;
		return jumpType;
	}

	public void setJumpType(Integer jumpType)
	{
		this.jumpType = jumpType;
	}

	public String getJumpValue()
	{
		return jumpValue;
	}

	public void setJumpValue(String jumpValue)
	{
		this.jumpValue = jumpValue;
	}

	public String getNoteReplaceRule()
	{
		return noteReplaceRule;
	}

	public void setNoteReplaceRule(String noteReplaceRule)
	{
		this.noteReplaceRule = noteReplaceRule;
	}
}