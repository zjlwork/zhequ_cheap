package dianyun.baobaowd.data;

import java.util.ArrayList;
import java.util.List;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

public class User implements Parcelable
{

	public User()
	{
	}
	private Long uid = 0L;
	/**
	 * 昵称
	 */
	private String nickname;
	/**
	 * 性别， 1：男 2：女
	 */
	private Byte gender = 0;
	/**
	 * 预产期
	 */
	private String perinatal;
	/**
	 * 宝宝生日
	 */
	private String babyBirthday;
	/**
	 * 宝宝性别 1：男 2：女
	 */
	private Byte babyGender = 0;
	/**
	 * 头像图片的url
	 */
	private String profileImage;
	private String email;
	private String password;
	private String regTime;
	private String token;
	/**
	 * iOS推送用
	 */
	private String deviceToken;
	/**
	 * 金币
	 */
	private int coins = 0;
	/**
	 * 威望值
	 */
	private int credits = 0;
	/**
	 * 分享设置 1：开启 0:关闭
	 */
	private Byte isShared = 0;
	/**
	 * 是否是自己
	 */
	private Byte isSelf = 0;
	/**
	 * 提问数
	 */
	private int questionCount;
	/**
	 * 回答数
	 */
	private int answerCount;
	/**
	 * 被感谢次数
	 */
	private int appreciationCount;
	/**
	 * 用户状态 0： 正常 1： 禁用
	 */
	private Byte status = 0;
	/**
	 * 背景图url
	 */
	private String bgImageUrl;
	/**
	 * 是否妈妈顾问团 1:是 0:否
	 */
	private Byte isCounsellor = 0;
	/**
	 * 用户等级
	 */
	private int level = 0;
	/**
	 * 手机设备标示
	 */
	private String deviceId;
	private int accInLastWeek = 0;
	/**
	 * 昨日获取金币数
	 */
	private int coinsYesterday = 0;
	private boolean isAttentioned;
	/**
	 * 关注的状态 0：未关注 1；已关注 2： 互相关注
	 */
	private Byte followStatus = 0;
	private int fansCount = 0;
	private int attentionCount = 0;
	/**
	 * 是否为临时用户 1:是 0：否
	 */
	private Byte isGuest;
	private String city;
	private String medalIdListStr;
	//我的柚币
	private Integer ycoins = 0;
	
	
	//排行榜临时用 （比如昨天获取的柚币）
	private int ycoinsNum;//柚币数
	
	
	
	
	private int inviteUserCount;
	
	
	private int freezeYcoins;
	
	
	
	
	
	public int getFreezeYcoins() {
		return freezeYcoins;
	}


	public void setFreezeYcoins(int freezeYcoins) {
		this.freezeYcoins = freezeYcoins;
	}


	public int getInviteUserCount() {
		return inviteUserCount;
	}


	public void setInviteUserCount(int inviteUserCount) {
		this.inviteUserCount = inviteUserCount;
	}


	public int getYcoinsNum() {
		return ycoinsNum;
	}


	public void setYcoinsNum(int ycoinsNum) {
		this.ycoinsNum = ycoinsNum;
	}


	public Integer getYcoins()
	{
		if(ycoins==null)ycoins=0;
		return ycoins;
	}

	
	public void setYcoins(Integer ycoins)
	{
		this.ycoins = ycoins;
	}
	private List<Integer> medalIdList = new ArrayList<Integer>();

	public String getMedalIdListStr()
	{
		return medalIdListStr;
	}

	public void setMedalIdListStr(String medalIdListStr)
	{
		this.medalIdListStr = medalIdListStr;
	}

	public List<Integer> getMedalIdList()
	{
		medalIdList.clear();
		if (!TextUtils.isEmpty(medalIdListStr))
		{
			String[] medalIds = medalIdListStr.split(",");
			for (int i = 0; i < medalIds.length; i++)
			{
				medalIdList.add(Integer.parseInt(medalIds[i]));
			}
		}
		return medalIdList;
	}

	//
	// public void setMedalIdList(List<Integer> medalIdList) {
	// this.medalIdList = medalIdList;
	// }
	public String getCity()
	{
		return city;
	}

	public void setCity(String city)
	{
		this.city = city;
	}

	public Byte getIsGuest()
	{
		if (isGuest == null)
			isGuest = 0;
		return isGuest;
	}

	public void setIsGuest(Byte isGuest)
	{
		this.isGuest = isGuest;
	}

	public int getFansCount()
	{
		return fansCount;
	}

	public void setFansCount(int fansCount)
	{
		this.fansCount = fansCount;
	}

	public int getAttentionCount()
	{
		return attentionCount;
	}

	public void setAttentionCount(int attentionCount)
	{
		this.attentionCount = attentionCount;
	}

	public Byte getFollowStatus()
	{
		return followStatus;
	}

	public void setFollowStatus(Byte followStatus)
	{
		this.followStatus = followStatus;
	}

	public boolean isAttentioned()
	{
		return isAttentioned;
	}

	public void setAttentioned(boolean isAttentioned)
	{
		this.isAttentioned = isAttentioned;
	}

	public int getCoinsYesterday()
	{
		return coinsYesterday;
	}

	public void setCoinsYesterday(int coinsYesterday)
	{
		this.coinsYesterday = coinsYesterday;
	}

	public int getAccInLastWeek()
	{
		return accInLastWeek;
	}

	public void setAccInLastWeek(int accInLastWeek)
	{
		this.accInLastWeek = accInLastWeek;
	}

	public int getQuestionCount()
	{
		return questionCount;
	}

	public void setQuestionCount(int questionCount)
	{
		this.questionCount = questionCount;
	}

	public int getAnswerCount()
	{
		return answerCount;
	}

	public void setAnswerCount(int answerCount)
	{
		this.answerCount = answerCount;
	}

	public int getAppreciationCount()
	{
		return appreciationCount;
	}

	public void setAppreciationCount(int appreciationCount)
	{
		this.appreciationCount = appreciationCount;
	}

	public Byte getIsSelf()
	{
		return isSelf;
	}

	public void setIsSelf(Byte isSelf)
	{
		this.isSelf = isSelf;
	}

	public String getDeviceId()
	{
		return deviceId;
	}

	public void setDeviceId(String deviceId)
	{
		this.deviceId = deviceId;
	}
	
	
	
	
	

	

	public User(Long uid, String nickname, String token)
	{
		super();
		this.uid = uid;
		this.token = token;
		this.nickname = nickname;
	}

	// 数据库对应
	public User(Long uid, String nickname, Byte gender, String perinatal,
			String babyBirthday, Byte babyGender, String profileImage,
			String email, String password, String regTime, String token,
			String deviceToken, int coins, int credits, Byte isShared,
			Byte isSelf, int questionCount, int answerCount,
			int appreciationCount, Byte status, String bgImageUrl,
			Byte isCounsellor, int level, String deviceId, int accInLastWeek,
			int coinsYesterday, Byte followStatus, int fansCount,
			int attentionCount, Byte isGuest, String city, String medalIdListStr,
			Integer ycoins,int freezeYcoins)
	{
		super();
		this.uid = uid;
		this.nickname = nickname;
		this.gender = gender;
		this.perinatal = perinatal;
		this.babyBirthday = babyBirthday;
		this.babyGender = babyGender;
		this.profileImage = profileImage;
		this.email = email;
		this.password = password;
		this.regTime = regTime;
		this.token = token;
		this.deviceToken = deviceToken;
		this.coins = coins;
		this.credits = credits;
		this.isShared = isShared;
		this.isSelf = isSelf;
		this.questionCount = questionCount;
		this.answerCount = answerCount;
		this.appreciationCount = appreciationCount;
		this.status = status;
		this.bgImageUrl = bgImageUrl;
		this.isCounsellor = isCounsellor;
		this.level = level;
		this.deviceId = deviceId;
		this.accInLastWeek = accInLastWeek;
		this.coinsYesterday = coinsYesterday;
		this.followStatus = followStatus;
		this.fansCount = fansCount;
		this.attentionCount = attentionCount;
		this.isGuest = isGuest;
		this.city = city;
		this.medalIdListStr = medalIdListStr;
		this.ycoins = ycoins;
		this.freezeYcoins = freezeYcoins;
	}

	public Long getUid()
	{
		return uid;
	}

	public void setUid(Long uid)
	{
		this.uid = uid;
	}

	public String getNickname()
	{
		return nickname;
	}

	// public String getFullNickname() {
	// return nickname;
	// }
	public void setNickname(String nickname)
	{
		this.nickname = nickname;
	}

	public Byte getStatus()
	{
		return status;
	}

	public void setStatus(Byte status)
	{
		this.status = status;
	}

	public String getBgImageUrl()
	{
		return bgImageUrl;
	}

	public void setBgImageUrl(String bgImageUrl)
	{
		this.bgImageUrl = bgImageUrl;
	}

	public Byte getIsCounsellor()
	{
		if (isCounsellor == null)
			isCounsellor = 0;
		return isCounsellor;
	}

	public void setIsCounsellor(Byte isCounsellor)
	{
		this.isCounsellor = isCounsellor;
	}

	public int getLevel()
	{
		return level;
	}

	public void setLevel(int level)
	{
		this.level = level;
	}

	public String getProfileImage()
	{
		return profileImage;
	}

	public void setProfileImage(String profileImage)
	{
		this.profileImage = profileImage;
	}

	public String getEmail()
	{
		return email;
	}

	public void setEmail(String email)
	{
		this.email = email;
	}

	public String getPassword()
	{
		return password;
	}

	public void setPassword(String password)
	{
		this.password = password;
	}

	public String getToken()
	{
		return token;
	}

	public void setToken(String token)
	{
		this.token = token;
	}

	public String getDeviceToken()
	{
		return deviceToken;
	}

	public void setDeviceToken(String deviceToken)
	{
		this.deviceToken = deviceToken;
	}

	public Byte getGender()
	{
		return gender;
	}

	public void setGender(Byte gender)
	{
		this.gender = gender;
	}

	public String getPerinatal()
	{
		return perinatal;
	}

	public void setPerinatal(String perinatal)
	{
		this.perinatal = perinatal;
	}

	public String getBabyBirthday()
	{
		return babyBirthday;
	}

	public void setBabyBirthday(String babyBirthday)
	{
		this.babyBirthday = babyBirthday;
	}

	public String getRegTime()
	{
		return regTime;
	}

	public void setRegTime(String regTime)
	{
		this.regTime = regTime;
	}

//	public int getCoins()
//	{
//		return coins;
//	}

//	public void setCoins(int coins)
//	{
//		this.coins = coins;
//	}

	public int getCredits()
	{
		return credits;
	}

	public void setCredits(int credits)
	{
		this.credits = credits;
	}

	public Byte getIsShared()
	{
		return isShared;
	}

	public void setIsShared(Byte isShared)
	{
		this.isShared = isShared;
	}

	public Byte getBabyGender()
	{
		if (babyGender == null)
			babyGender = 0;
		return babyGender;
	}

	public void setBabyGender(Byte babyGender)
	{
		this.babyGender = babyGender;
	}

	private void readFromParcel(Parcel in)
	{
		if (gender == null)
			gender = 0;
		if (babyGender == null)
			babyGender = 0;
		if (isShared == null)
			isShared = 0;
		if (isSelf == null)
			isSelf = 0;
		if (status == null)
			status = 0;
		if (isCounsellor == null)
			isCounsellor = 0;
		if (followStatus == null)
			followStatus = 0;
		if (isGuest == null)
			isGuest = 0;
		if (ycoins == null)
			ycoins = 0;
		this.uid = in.readLong();
		this.nickname = in.readString();
		this.gender = in.readByte();
		this.perinatal = in.readString();
		this.babyBirthday = in.readString();
		this.babyGender = in.readByte();
		this.profileImage = in.readString();
		this.email = in.readString();
		this.password = in.readString();
		this.regTime = in.readString();
		this.token = in.readString();
		this.deviceToken = in.readString();
		this.coins = in.readInt();
		this.credits = in.readInt();
		this.isShared = in.readByte();
		this.isSelf = in.readByte();
		this.questionCount = in.readInt();
		this.answerCount = in.readInt();
		this.appreciationCount = in.readInt();
		this.status = in.readByte();
		this.bgImageUrl = in.readString();
		this.isCounsellor = in.readByte();
		this.level = in.readInt();
		this.deviceId = in.readString();
		this.accInLastWeek = in.readInt();
		this.coinsYesterday = in.readInt();
		this.fansCount = in.readInt();
		this.attentionCount = in.readInt();
		this.followStatus = in.readByte();
		this.isGuest = in.readByte();
		this.city = in.readString();
		this.medalIdListStr = in.readString();
		this.ycoins = in.readInt();
		this.freezeYcoins = in.readInt();
	}

	public User(Parcel source)
	{
		readFromParcel(source);
	}
	public static final Parcelable.Creator<User> CREATOR = new Parcelable.Creator<User>()
	{

		@Override
		public User[] newArray(int size)
		{
			return new User[size];
		}

		@Override
		public User createFromParcel(Parcel source)
		{
			return new User(source);
		}
	};

	@Override
	public int describeContents()
	{
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags)
	{
		if (gender == null)
			gender = 0;
		if (babyGender == null)
			babyGender = 0;
		if (isShared == null)
			isShared = 0;
		if (isSelf == null)
			isSelf = 0;
		if (status == null)
			status = 0;
		if (isCounsellor == null)
			isCounsellor = 0;
		if (followStatus == null)
			followStatus = 0;
		if (isGuest == null)
			isGuest = 0;
		if (ycoins == null)
			ycoins = 0;
		dest.writeLong(uid);
		dest.writeString(nickname);
		dest.writeByte(gender);
		dest.writeString(perinatal);
		dest.writeString(babyBirthday);
		dest.writeByte(babyGender);
		dest.writeString(profileImage);
		dest.writeString(email);
		dest.writeString(password);
		dest.writeString(regTime);
		dest.writeString(token);
		dest.writeString(deviceToken);
		dest.writeInt(coins);
		dest.writeInt(credits);
		dest.writeByte(isShared);
		dest.writeByte(isSelf);
		dest.writeInt(questionCount);
		dest.writeInt(answerCount);
		dest.writeInt(appreciationCount);
		dest.writeByte(status);
		dest.writeString(bgImageUrl);
		dest.writeByte(isCounsellor);
		dest.writeInt(level);
		dest.writeString(deviceId);
		dest.writeInt(accInLastWeek);
		dest.writeInt(coinsYesterday);
		dest.writeInt(fansCount);
		dest.writeInt(attentionCount);
		dest.writeByte(followStatus);
		dest.writeByte(isGuest);
		dest.writeString(city);
		dest.writeString(medalIdListStr);
		dest.writeInt(ycoins);
		dest.writeInt(freezeYcoins);
	}
}