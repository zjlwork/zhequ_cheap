package dianyun.baobaowd.data;

public class Message
{

	private Long seqId;
	private String messageId;
	private int messageType;
	private Long timestamp;
	private Long uid;
	private String contentObject;

	public Message()
	{
	}

	public Message(Long seqId, String messageId, int messageType,
			Long timestamp, Long uid, String contentObject)
	{
		super();
		this.seqId = seqId;
		this.messageId = messageId;
		this.messageType = messageType;
		this.timestamp = timestamp;
		this.uid = uid;
		this.contentObject = contentObject;
	}

	public Long getSeqId()
	{
		return seqId;
	}

	public void setSeqId(Long seqId)
	{
		this.seqId = seqId;
	}

	public String getMessageId()
	{
		return messageId;
	}

	public void setMessageId(String messageId)
	{
		this.messageId = messageId;
	}

	public int getMessageType()
	{
		return messageType;
	}

	public void setMessageType(int messageType)
	{
		this.messageType = messageType;
	}

	public Long getTimestamp()
	{
		return timestamp;
	}

	public void setTimestamp(Long timestamp)
	{
		this.timestamp = timestamp;
	}

	public Long getUid()
	{
		return uid;
	}

	public void setUid(Long uid)
	{
		this.uid = uid;
	}

	public String getContentObject()
	{
		return contentObject;
	}

	public void setContentObject(String contentObject)
	{
		this.contentObject = contentObject;
	}
}
