package dianyun.baobaowd.data;


public class Talent {
    private Long seqId;

    //用户id
    private Long uid;

    //达人标签
    private String tag;

    
    
    
    
    
    
    
    
	
	public Talent(Long seqId, Long uid, String tag)
	{
		super();
		this.seqId = seqId;
		this.uid = uid;
		this.tag = tag;
	}


	public Long getSeqId()
	{
		return seqId;
	}

	
	public void setSeqId(Long seqId)
	{
		this.seqId = seqId;
	}

	
	public Long getUid()
	{
		return uid;
	}

	
	public void setUid(Long uid)
	{
		this.uid = uid;
	}

	
	public String getTag()
	{
		return tag;
	}

	
	public void setTag(String tag)
	{
		this.tag = tag;
	}

    
    
    
    
    
    
    
    
    
  
}