package dianyun.baobaowd.data;

public class VersionData
{

	private String ver;
	private String filepath;
	private String note;
	private int privateVer;
	private int lastPrivateVer;

	public VersionData()
	{
	}

	public String getVer()
	{
		return ver;
	}

	public void setVer(String ver)
	{
		this.ver = ver;
	}

	public String getFilepath()
	{
		return filepath;
	}

	public void setFilepath(String filepath)
	{
		this.filepath = filepath;
	}

	public String getNote()
	{
		return note;
	}

	public void setNote(String note)
	{
		this.note = note;
	}

	public int getPrivateVer()
	{
		return privateVer;
	}

	public void setPrivateVer(int privateVer)
	{
		this.privateVer = privateVer;
	}

	
	public int getLastPrivateVer()
	{
		return lastPrivateVer;
	}

	
	public void setLastPrivateVer(int lastPrivateVer)
	{
		this.lastPrivateVer = lastPrivateVer;
	}
	
	
	
	
}
