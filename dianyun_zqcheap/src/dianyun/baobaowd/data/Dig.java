package dianyun.baobaowd.data;

public class Dig {
    public Integer seqId=0;

    //排序字段
    public String sortBy;
    //通道标识  点乐:dianle majibao
    public String digKey;

    //标记位   0:无   1:新增  2:热门
    public String flag;

    //标题
    public String title;

    //说明
    public String comment;

    //是否激活
    public Byte enabled=0;
    
    //显示类型  0:sdk打开   1:URL打开
    public Byte showType=0;

    //当是url打开时   这里值是URL地址
    public String showValue;
    
//    public static final String DIAKEY_DIANLE = "dianle";
    public static final byte SHOWTYPE_SDKOPEN = 0;
    public static final byte SHOWTYPE_URLOPEN = 1;
    
    public static final byte DIGTYPE_GOLD = 0;
    public static final byte DIGTYPE_NOTGOLD = 1;
    
    public static final String DIGKEY_MAJIBAO = "majibao";
    public static final String DIGKEY_DIANLE = "dianle";
    public static final String DIGKEY_YOUMI = "youmi";
    public static final String DIGKEY_DUOMENG = "domob";
    
    
    
    
}