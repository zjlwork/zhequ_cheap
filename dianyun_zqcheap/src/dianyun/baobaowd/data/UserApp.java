package dianyun.baobaowd.data;

/**
 * Created by wencongjian on 14-5-14.
 */
public class UserApp
{

	// appId
	private String appId;
	// 图片
	private String picture;
	// 应用名称
	private String appName;
	// 说明
	private String desc;
	// 奖励金币数
	private Integer coin = 0;
	// 详细说明url
	private String descUrl;
	// 下载地址
	private String url;
	// 版本
	private String version;
	// 完成条件文字
	private String constraint;
	// 大小
	private String size;
	// 排序
	private Integer sort = 0;
	// 时间
	private String postTime;
	// 是否完成
	private Byte isFinished = 0;

	public UserApp()
	{
	}

	public String getAppId()
	{
		return appId;
	}

	public void setAppId(String appId)
	{
		this.appId = appId;
	}

	public String getPicture()
	{
		return picture;
	}

	public void setPicture(String picture)
	{
		this.picture = picture;
	}

	public String getAppName()
	{
		return appName;
	}

	public void setAppName(String appName)
	{
		this.appName = appName;
	}

	public String getDesc()
	{
		return desc;
	}

	public void setDesc(String desc)
	{
		this.desc = desc;
	}

	public Integer getCoin()
	{
		if (coin == null)
			coin = 0;
		return coin;
	}

	public void setCoin(Integer coin)
	{
		this.coin = coin;
	}

	public String getDescUrl()
	{
		return descUrl;
	}

	public void setDescUrl(String descUrl)
	{
		this.descUrl = descUrl;
	}

	public String getUrl()
	{
		return url;
	}

	public void setUrl(String url)
	{
		this.url = url;
	}

	public String getVersion()
	{
		return version;
	}

	public void setVersion(String version)
	{
		this.version = version;
	}

	public String getConstraint()
	{
		return constraint;
	}

	public void setConstraint(String constraint)
	{
		this.constraint = constraint;
	}

	public String getSize()
	{
		return size;
	}

	public void setSize(String size)
	{
		this.size = size;
	}

	public Integer getSort()
	{
		if (sort == null)
			sort = 0;
		return sort;
	}

	public void setSort(Integer sort)
	{
		this.sort = sort;
	}

	public String getPostTime()
	{
		return postTime;
	}

	public void setPostTime(String postTime)
	{
		this.postTime = postTime;
	}

	public Byte getIsFinished()
	{
		if (isFinished == null)
			isFinished = 0;
		return isFinished;
	}

	public void setIsFinished(Byte isFinished)
	{
		this.isFinished = isFinished;
	}
}
