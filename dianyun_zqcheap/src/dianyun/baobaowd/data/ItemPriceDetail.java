package dianyun.baobaowd.data;

import java.util.List;

public class ItemPriceDetail
{

	private Long priceGroupId;
	private Long itemId;
	private String title;
	private List<ItemPrice> itemPriceList;
}