package dianyun.baobaowd.data;

import java.util.List;

public class XiaoMiUser2
{

	private String alias;
	private boolean aliasStatus;
	private boolean unSetAliasStatus;
	private List<String> topics;
	private List<Boolean> topicsStatus;
	private List<Boolean> unSetTopicsStatus;

	public String getAlias()
	{
		return alias;
	}

	public void setAlias(String alias)
	{
		this.alias = alias;
	}

	public boolean isAliasStatus()
	{
		return aliasStatus;
	}

	public void setAliasStatus(boolean aliasStatus)
	{
		this.aliasStatus = aliasStatus;
	}

	public List<String> getTopics()
	{
		return topics;
	}

	public void setTopics(List<String> topics)
	{
		this.topics = topics;
	}

	public List<Boolean> getTopicsStatus()
	{
		return topicsStatus;
	}

	public void setTopicsStatus(List<Boolean> topicsStatus)
	{
		this.topicsStatus = topicsStatus;
	}

	public boolean isUnSetAliasStatus()
	{
		return unSetAliasStatus;
	}

	public void setUnSetAliasStatus(boolean unSetAliasStatus)
	{
		this.unSetAliasStatus = unSetAliasStatus;
	}

	public List<Boolean> getUnSetTopicsStatus()
	{
		return unSetTopicsStatus;
	}

	public void setUnSetTopicsStatus(List<Boolean> unSetTopicsStatus)
	{
		this.unSetTopicsStatus = unSetTopicsStatus;
	}

	public XiaoMiUser2(String alias, boolean aliasStatus,
			boolean unSetAliasStatus, List<String> topics,
			List<Boolean> topicsStatus, List<Boolean> unSetTopicsStatus)
	{
		super();
		this.alias = alias;
		this.aliasStatus = aliasStatus;
		this.unSetAliasStatus = unSetAliasStatus;
		this.topics = topics;
		this.topicsStatus = topicsStatus;
		this.unSetTopicsStatus = unSetTopicsStatus;
	}
}
