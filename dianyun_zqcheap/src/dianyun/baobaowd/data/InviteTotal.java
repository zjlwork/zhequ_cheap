package dianyun.baobaowd.data;

public class InviteTotal {

	private Integer userCount;
	private Integer taeOrderTotal;
	private Integer ycoinExtraTotal;
	private Integer coinExtraTotal;

	public InviteTotal() {

	}

	public Integer getUserCount() {
		if (userCount == null)
			userCount = 0;
		return userCount;
	}

	public void setUserCount(Integer userCount) {
		this.userCount = userCount;
	}

	public Integer getTaeOrderTotal() {
		if (taeOrderTotal == null)
			taeOrderTotal = 0;
		return taeOrderTotal;
	}

	public void setTaeOrderTotal(Integer taeOrderTotal) {
		this.taeOrderTotal = taeOrderTotal;
	}

	public Integer getYcoinExtraTotal() {
		if (ycoinExtraTotal == null)
			ycoinExtraTotal = 0;
		return ycoinExtraTotal;
	}

	public void setYcoinExtraTotal(Integer ycoinExtraTotal) {
		this.ycoinExtraTotal = ycoinExtraTotal;
	}

	public Integer getCoinExtraTotal() {
		if (coinExtraTotal == null)
			coinExtraTotal = 0;
		return coinExtraTotal;
	}

	public void setCoinExtraTotal(Integer coinExtraTotal) {
		this.coinExtraTotal = coinExtraTotal;
	}

}
