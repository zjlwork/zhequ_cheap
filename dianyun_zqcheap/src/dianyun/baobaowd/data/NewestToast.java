package dianyun.baobaowd.data;

public class NewestToast
{

	private int toastId;
	private String toastContent;
	private String toastTime;
	private byte isRead = 0;

	public int getToastId()
	{
		return toastId;
	}

	public void setToastId(int toastId)
	{
		this.toastId = toastId;
	}

	public String getToastContent()
	{
		return toastContent;
	}

	public void setToastContent(String toastContent)
	{
		this.toastContent = toastContent;
	}

	public String getToastTime()
	{
		return toastTime;
	}

	public void setToastTime(String toastTime)
	{
		this.toastTime = toastTime;
	}

	public byte getIsRead()
	{
		return isRead;
	}

	public void setIsRead(byte isRead)
	{
		this.isRead = isRead;
	}

	public NewestToast(int toastId, String toastContent, String toastTime,
			byte isRead)
	{
		super();
		this.toastId = toastId;
		this.toastContent = toastContent;
		this.toastTime = toastTime;
		this.isRead = isRead;
	}
}
