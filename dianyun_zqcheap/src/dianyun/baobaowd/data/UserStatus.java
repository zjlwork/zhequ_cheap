package dianyun.baobaowd.data;

import android.os.Parcel;
import android.os.Parcelable;

public class UserStatus implements Parcelable
{

	private String userStatusId;
	// dayarticlestatus
	private int type;
	// 最后月经去期 预产期 宝宝生日
	private String time;
	// 月经持续天数
	private int period;
	// 周期
	private int cycle;
	private byte babyGender;

	public UserStatus()
	{
	}

	public UserStatus(String userStatusId, int type, String time, int period,
			int cycle, byte babyGender)
	{
		super();
		this.userStatusId = userStatusId;
		this.type = type;
		this.time = time;
		this.period = period;
		this.cycle = cycle;
		this.babyGender = babyGender;
	}

	public String getUserStatusId()
	{
		return userStatusId;
	}

	public void setUserStatusId(String userStatusId)
	{
		this.userStatusId = userStatusId;
	}

	public int getType()
	{
		return type;
	}

	public void setType(int type)
	{
		this.type = type;
	}

	public String getTime()
	{
		return time;
	}

	public void setTime(String time)
	{
		this.time = time;
	}

	public int getPeriod()
	{
		return period;
	}

	public void setPeriod(int period)
	{
		this.period = period;
	}

	public int getCycle()
	{
		return cycle;
	}

	public void setCycle(int cycle)
	{
		this.cycle = cycle;
	}

	public byte getBabyGender()
	{
		return babyGender;
	}

	public void setBabyGender(byte babyGender)
	{
		this.babyGender = babyGender;
	}

	private void readFromParcel(Parcel in)
	{
		this.userStatusId = in.readString();
		this.type = in.readInt();
		this.time = in.readString();
		this.period = in.readInt();
		this.cycle = in.readInt();
		this.babyGender = in.readByte();
	}

	public UserStatus(Parcel source)
	{
		readFromParcel(source);
	}
	public static final Parcelable.Creator<UserStatus> CREATOR = new Parcelable.Creator<UserStatus>()
	{

		@Override
		public UserStatus[] newArray(int size)
		{
			return new UserStatus[size];
		}

		@Override
		public UserStatus createFromParcel(Parcel source)
		{
			return new UserStatus(source);
		}
	};

	@Override
	public int describeContents()
	{
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags)
	{
		dest.writeString(userStatusId);
		dest.writeInt(type);
		dest.writeString(time);
		dest.writeInt(period);
		dest.writeInt(cycle);
		dest.writeByte(babyGender);
	}
}
