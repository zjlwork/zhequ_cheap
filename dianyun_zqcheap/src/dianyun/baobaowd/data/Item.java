package dianyun.baobaowd.data;

import java.math.BigDecimal;
import java.util.Date;

public class Item
{

	private Long itemId;
	private Long productId;
	private Long classId;
	// 专辑编号
	private Long specialId;
	// 名称
	private String title;
	// 品牌
	private String brands;
	// 标价
	private BigDecimal price;
	// 售价
	private BigDecimal salePrice;
	// 图片
	private String pic;
	// 最大(快递)包装数量
	private Integer packNum;
	// 金币返还数
	private Float obtain;
	private Date createTime;
	// 可销售库存数量
	private Float num;

	public Long getItemId()
	{
		return itemId;
	}

	public void setItemId(Long itemId)
	{
		this.itemId = itemId;
	}

	public Long getProductId()
	{
		return productId;
	}

	public void setProductId(Long productId)
	{
		this.productId = productId;
	}

	public Long getClassId()
	{
		return classId;
	}

	public void setClassId(Long classId)
	{
		this.classId = classId;
	}

	public Long getSpecialId()
	{
		return specialId;
	}

	public void setSpecialId(Long specialId)
	{
		this.specialId = specialId;
	}

	public String getTitle()
	{
		return title;
	}

	public void setTitle(String title)
	{
		this.title = title;
	}

	public String getBrands()
	{
		return brands;
	}

	public void setBrands(String brands)
	{
		this.brands = brands;
	}

	public BigDecimal getPrice()
	{
		return price;
	}

	public void setPrice(BigDecimal price)
	{
		this.price = price;
	}

	public BigDecimal getSalePrice()
	{
		return salePrice;
	}

	public void setSalePrice(BigDecimal salePrice)
	{
		this.salePrice = salePrice;
	}

	public String getPic()
	{
		return pic;
	}

	public void setPic(String pic)
	{
		this.pic = pic;
	}

	public Integer getPackNum()
	{
		return packNum;
	}

	public void setPackNum(Integer packNum)
	{
		this.packNum = packNum;
	}

	public Float getObtain()
	{
		return obtain;
	}

	public void setObtain(Float obtain)
	{
		this.obtain = obtain;
	}

	public Date getCreateTime()
	{
		return createTime;
	}

	public void setCreateTime(Date createTime)
	{
		this.createTime = createTime;
	}

	public Float getNum()
	{
		return num;
	}

	public void setNum(Float num)
	{
		this.num = num;
	}
}