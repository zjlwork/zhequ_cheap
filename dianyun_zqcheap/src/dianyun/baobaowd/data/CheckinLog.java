package dianyun.baobaowd.data;


public class CheckinLog {
    private Long uid;
    //上次签到日期
    private String checkinDate;
    //连续签到天数
    private int maxCheckinNum;
    //下次签到获得金币数
    private int nextCoins;
    //本次获得金币数
    private int coins;
    //今日是否已签到
    private boolean hasCheckin;
    //当前金币
    private int currentCoins;
    
    
    
	public Long getUid() {
		return uid;
	}
	public void setUid(Long uid) {
		this.uid = uid;
	}
	public String getCheckinDate() {
		return checkinDate;
	}
	public void setCheckinDate(String checkinDate) {
		this.checkinDate = checkinDate;
	}
	public int getMaxCheckinNum() {
		return maxCheckinNum;
	}
	public void setMaxCheckinNum(int maxCheckinNum) {
		this.maxCheckinNum = maxCheckinNum;
	}
	public int getNextCoins() {
		return nextCoins;
	}
	public void setNextCoins(int nextCoins) {
		this.nextCoins = nextCoins;
	}
	public int getCoins() {
		return coins;
	}
	public void setCoins(int coins) {
		this.coins = coins;
	}
	
	public int getCurrentCoins() {
		return currentCoins;
	}
	public void setCurrentCoins(int currentCoins) {
		this.currentCoins = currentCoins;
	}
	public boolean isHasCheckin() {
		return hasCheckin;
	}
	public void setHasCheckin(boolean hasCheckin) {
		this.hasCheckin = hasCheckin;
	}
    
    
    
    
    
    
    
    
    
    
    
}