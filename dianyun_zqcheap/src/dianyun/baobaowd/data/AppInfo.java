package dianyun.baobaowd.data;

public class AppInfo
{

	private String name;
	private String packageName;

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public String getPackageName()
	{
		return packageName;
	}

	public void setPackageName(String packageName)
	{
		this.packageName = packageName;
	}

	public AppInfo()
	{
	}
}
