package dianyun.baobaowd.data;

import android.os.Parcel;
import android.os.Parcelable;

public class Attachment implements Parcelable
{

	public Attachment()
	{
	}
	/**
	 * 客户端生成的 uuid
	 */
	private String attId;
	/**
	 * 文件网址
	 */
	private String fileUrl;
	/**
	 * 文件大小（字节）
	 */
	private Long attSize = 0L;
	/**
	 * 文件名，如“test.jpg”
	 */
	private String fileName;
	private String questionId;
	private String answerId;
	private String fileLocalPath;
	private String topicId;
	private String postId;
	private String subjectId;
	private Long boardId=0L;
//	private Long viewpic_seqId;

	
	
	
	
//	public Long getViewpic_seqId()
//	{
//		return viewpic_seqId;
//	}
//
//	
//	public void setViewpic_seqId(Long viewpic_seqId)
//	{
//		this.viewpic_seqId = viewpic_seqId;
//	}

	// private long portalId;
	// public long getPortalId() {
	// return portalId;
	// }
	//
	//
	//
	// public void setPortalId(long portalId) {
	// this.portalId = portalId;
	// }
	public String getSubjectId()
	{
		return subjectId;
	}

	public Long getBoardId()
	{
		return boardId;
	}

	public void setBoardId(Long boardId)
	{
		this.boardId = boardId;
	}

	public void setSubjectId(String subjectId)
	{
		this.subjectId = subjectId;
	}

	public String getAnswerId()
	{
		return answerId;
	}

	public void setAnswerId(String answerId)
	{
		this.answerId = answerId;
	}

	public Attachment(String attId, String fileUrl, Long attSize,
			String fileName, String fileLocalPath)
	{
		super();
		this.attId = attId;
		this.fileUrl = fileUrl;
		this.attSize = attSize;
		this.fileName = fileName;
		this.fileLocalPath = fileLocalPath;
	}

	public String getPostId()
	{
		return postId;
	}

	public void setPostId(String postId)
	{
		this.postId = postId;
	}

	public String getTopicId()
	{
		return topicId;
	}

	public void setTopicId(String topicId)
	{
		this.topicId = topicId;
	}

	public String getFileLocalPath()
	{
		return fileLocalPath;
	}

	public void setFileLocalPath(String fileLocalPath)
	{
		this.fileLocalPath = fileLocalPath;
	}

	public String getQuestionId()
	{
		return questionId;
	}

	public void setQuestionId(String questionId)
	{
		this.questionId = questionId;
	}

	public String getAttId()
	{
		return attId;
	}

	public void setAttId(String attId)
	{
		this.attId = attId;
	}

	public String getFileUrl()
	{
		return fileUrl;
	}

	public void setFileUrl(String fileUrl)
	{
		this.fileUrl = fileUrl;
	}

	public Long getAttSize()
	{
		return attSize;
	}

	public void setAttSize(Long attSize)
	{
		this.attSize = attSize;
	}

	public String getFileName()
	{
		return fileName;
	}

	public void setFileName(String fileName)
	{
		this.fileName = fileName;
	}

	// private String attId;
	// private String fileUrl;
	// private Long attSize;
	// private String fileName;
	private void readFromParcel(Parcel in)
	{
		if(boardId==null)boardId=0L;
		if(attSize==null)attSize=0L;
		this.attId = in.readString();
		this.fileUrl = in.readString();
		this.attSize = in.readLong();
		this.fileName = in.readString();
		this.fileLocalPath = in.readString();
		this.questionId = in.readString();
		this.answerId = in.readString();
		this.topicId = in.readString();
		this.postId = in.readString();
		this.subjectId = in.readString();
		this.boardId = in.readLong();
//		this.viewpic_seqId = in.readLong();
	}

	public Attachment(Parcel source)
	{
		readFromParcel(source);
	}
	public static final Parcelable.Creator<Attachment> CREATOR = new Parcelable.Creator<Attachment>()
	{

		@Override
		public Attachment[] newArray(int size)
		{
			return new Attachment[size];
		}

		@Override
		public Attachment createFromParcel(Parcel source)
		{
			return new Attachment(source);
		}
	};

	@Override
	public int describeContents()
	{
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags)
	{
		
		if(boardId==null)boardId=0L;
		if(attSize==null)attSize=0L;
		
		
		dest.writeString(attId);
		dest.writeString(fileUrl);
		dest.writeLong(attSize);
		dest.writeString(fileName);
		dest.writeString(fileLocalPath);
		dest.writeString(questionId);
		dest.writeString(answerId);
		dest.writeString(topicId);
		dest.writeString(postId);
		dest.writeString(subjectId);
		dest.writeLong(boardId);
//		dest.writeLong(viewpic_seqId);
	}
}