package dianyun.baobaowd.data;

public class Drafts
{
	
	public static final int TYPE_ASK=1;
	public static final int TYPE_REPLY=2;
	public static final int TYPE_TOPIC=3;
	public static final int TYPE_TOPICREPLY=4;
	public static final int TYPE_LETTER=5;
	
	

	private String draftsId;
	private String title;
	private String content;
	private int type;
	
	
	
	
	
	
	
	
	
	
	
	
	public Drafts(String draftsId, String title, String content, int type)
	{
		super();
		this.draftsId = draftsId;
		this.title = title;
		this.content = content;
		this.type = type;
	}

	public String getDraftsId()
	{
		return draftsId;
	}
	
	public void setDraftsId(String draftsId)
	{
		this.draftsId = draftsId;
	}
	
	public String getTitle()
	{
		return title;
	}
	
	public void setTitle(String title)
	{
		this.title = title;
	}
	
	public String getContent()
	{
		return content;
	}
	
	public void setContent(String content)
	{
		this.content = content;
	}
	
	public int getType()
	{
		return type;
	}
	
	public void setType(int type)
	{
		this.type = type;
	}
	
	
	
	
	
	
	
	
}