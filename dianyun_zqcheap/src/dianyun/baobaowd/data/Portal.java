package dianyun.baobaowd.data;

public class Portal
{

	private Long seqId;
	/**
	 * 标题
	 */
	private String title;
	/**
	 * 图片地址
	 */
	private String picUrl;
	/**
	 * 时间
	 */
	// @JsonDeserialize(using = CustomDateDeSerializerMySQLDateTime.class)
	private String postTime;
	/**
	 * 类型 1：专题 2：帖子
	 */
	private Byte portalType;
	/**
	 * 被引用的帖子或专题的ID 暂时没用上
	 */
	private String referId;
	/**
	 * 被引用的对象
	 */
	private Object referObj;

	public Portal(Long seqId, String title, String picUrl, String postTime)
	{
		super();
		this.seqId = seqId;
		this.title = title;
		this.picUrl = picUrl;
		this.postTime = postTime;
	}

	public Long getSeqId()
	{
		return seqId;
	}

	public void setSeqId(Long seqId)
	{
		this.seqId = seqId;
	}

	public String getReferId()
	{
		return referId;
	}

	public void setReferId(String referId)
	{
		this.referId = referId;
	}

	public String getTitle()
	{
		return title;
	}

	public void setTitle(String title)
	{
		this.title = title;
	}

	public String getPicUrl()
	{
		return picUrl;
	}

	public void setPicUrl(String picUrl)
	{
		this.picUrl = picUrl;
	}

	public Byte getPortalType()
	{
		return portalType;
	}

	public void setPortalType(Byte portalType)
	{
		this.portalType = portalType;
	}

	public String getPostTime()
	{
		return postTime;
	}

	public void setPostTime(String postTime)
	{
		this.postTime = postTime;
	}

	public Object getReferObj()
	{
		return referObj;
	}

	public void setReferObj(Object referObj)
	{
		this.referObj = referObj;
	}
}
