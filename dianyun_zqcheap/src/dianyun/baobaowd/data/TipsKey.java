package dianyun.baobaowd.data;

public class TipsKey
{

	private Integer keyId;
	private Integer groupId;
	private String name;

	// private List<Tips> tipsList;
	// private List<TopicDTO> topicDTOList;
	public Integer getKeyId()
	{
		return keyId;
	}

	public void setKeyId(Integer keyId)
	{
		this.keyId = keyId;
	}

	public Integer getGroupId()
	{
		return groupId;
	}

	public void setGroupId(Integer groupId)
	{
		this.groupId = groupId;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name == null ? null : name.trim();
	}

	public TipsKey()
	{
	}

	public TipsKey(Integer keyId, String name)
	{
		super();
		this.keyId = keyId;
		this.name = name;
	}
	// public List<Tips> getTipsList() {
	// return tipsList;
	// }
	//
	// public void setTipsList(List<Tips> tipsList) {
	//
	// this.tipsList = tipsList;
	// }
	// public List<TopicDTO> getTopicDTOList() {
	// return topicDTOList;
	// }
	//
	// public void setTopicDTOList(List<TopicDTO> topicDTOList) {
	// this.topicDTOList = topicDTOList;
	// }
}