package dianyun.baobaowd.data;

public class XiaoMiPushData
{

	private String name;
	private Integer status;
	private Integer unStatus;
	private Integer type;

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public Integer getStatus()
	{
		return status;
	}

	public void setStatus(Integer status)
	{
		this.status = status;
	}

	public Integer getUnStatus()
	{
		return unStatus;
	}

	public void setUnStatus(Integer unStatus)
	{
		this.unStatus = unStatus;
	}

	public Integer getType()
	{
		return type;
	}

	public void setType(Integer type)
	{
		this.type = type;
	}

	public XiaoMiPushData(String name, Integer status, Integer unStatus,
			Integer type)
	{
		super();
		this.name = name;
		this.status = status;
		this.unStatus = unStatus;
		this.type = type;
	}
}
