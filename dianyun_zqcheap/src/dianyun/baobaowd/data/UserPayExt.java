package dianyun.baobaowd.data;


public class UserPayExt {
	private Long uid;

	private String cashPhone;

	private Byte accountType;

	private String account;
	
	private String accountName;

	
	public UserPayExt(){
		
	}
	
	
	
	public Long getUid()
	{
		return uid;
	}

	
	public void setUid(Long uid)
	{
		this.uid = uid;
	}

	
	public String getCashPhone()
	{
		return cashPhone;
	}

	
	public void setCashPhone(String cashPhone)
	{
		this.cashPhone = cashPhone;
	}

	
	public Byte getAccountType()
	{
		return accountType;
	}

	
	public void setAccountType(Byte accountType)
	{
		this.accountType = accountType;
	}

	
	public String getAccount()
	{
		return account;
	}

	
	public void setAccount(String account)
	{
		this.account = account;
	}

	
	public String getAccountName()
	{
		return accountName;
	}

	
	public void setAccountName(String accountName)
	{
		this.accountName = accountName;
	}

	
	
	
}
