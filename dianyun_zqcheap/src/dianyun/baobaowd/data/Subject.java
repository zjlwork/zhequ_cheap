package dianyun.baobaowd.data;

import java.util.List;
import android.os.Parcel;
import android.os.Parcelable;

public class Subject implements Parcelable
{

	private Long seqId;
	// 专题唯一标识 ID
	private String subjectId;
	// 标题
	private String title;
	// 查看次数
	private Integer viewCount;
	// 宝宝状态 1备孕 2怀孕 3育儿
	private Byte babyStatus;
	// 状态对应的开始天数
	private Integer babyBeginDay;
	// 状态对应的结束天数
	private Integer babyEndDay;
	// 顺序
	// private Integer sortBy;
	// 最后更新时间
	private String postTime;
	// 新建时间
	private String createTime;
	// 专题生效时间
	// private Date beginTime;
	// 专题失效时间
	// private Date endTime;
	// 专题说明
	private String subjectDesc;
	// 图片
	private List<Attachment> attachmentList;
	private List<Topic> topicList;
	// 服务端来源
	private int fromType;

	public Subject(Long seqId, String subjectId, String title,
			Integer viewCount, Byte babyStatus, Integer babyBeginDay,
			Integer babyEndDay, String postTime, String createTime,
			String subjectDesc, int fromType)
	{
		super();
		this.seqId = seqId;
		this.subjectId = subjectId;
		this.title = title;
		this.viewCount = viewCount;
		this.babyStatus = babyStatus;
		this.babyBeginDay = babyBeginDay;
		this.babyEndDay = babyEndDay;
		this.postTime = postTime;
		this.createTime = createTime;
		this.subjectDesc = subjectDesc;
		this.fromType = fromType;
	}

	public Long getSeqId()
	{
		return seqId;
	}

	public void setSeqId(Long seqId)
	{
		this.seqId = seqId;
	}

	public int getFromType()
	{
		return fromType;
	}

	public void setFromType(int fromType)
	{
		this.fromType = fromType;
	}

	public List<Topic> getTopicList()
	{
		return topicList;
	}

	public void setTopicList(List<Topic> topicList)
	{
		this.topicList = topicList;
	}

	public String getSubjectId()
	{
		return subjectId;
	}

	public void setSubjectId(String subjectId)
	{
		this.subjectId = subjectId;
	}

	public String getTitle()
	{
		return title;
	}

	public void setTitle(String title)
	{
		this.title = title;
	}

	public Integer getViewCount()
	{
		return viewCount;
	}

	public void setViewCount(Integer viewCount)
	{
		this.viewCount = viewCount;
	}

	public Byte getBabyStatus()
	{
		return babyStatus;
	}

	public void setBabyStatus(Byte babyStatus)
	{
		this.babyStatus = babyStatus;
	}

	public Integer getBabyBeginDay()
	{
		return babyBeginDay;
	}

	public void setBabyBeginDay(Integer babyBeginDay)
	{
		this.babyBeginDay = babyBeginDay;
	}

	public Integer getBabyEndDay()
	{
		return babyEndDay;
	}

	public void setBabyEndDay(Integer babyEndDay)
	{
		this.babyEndDay = babyEndDay;
	}

	public String getPostTime()
	{
		return postTime;
	}

	public void setPostTime(String postTime)
	{
		this.postTime = postTime;
	}

	public String getCreateTime()
	{
		return createTime;
	}

	public void setCreateTime(String createTime)
	{
		this.createTime = createTime;
	}

	public String getSubjectDesc()
	{
		return subjectDesc;
	}

	public void setSubjectDesc(String subjectDesc)
	{
		this.subjectDesc = subjectDesc;
	}

	public List<Attachment> getAttachmentList()
	{
		return attachmentList;
	}

	public void setAttachmentList(List<Attachment> attachmentList)
	{
		this.attachmentList = attachmentList;
	}

	private void initDefualtValue()
	{
		if (seqId == null)
			seqId = 0L;
		if (viewCount == null)
			viewCount = 0;
		if (babyStatus == null)
			babyStatus = 0;
		if (babyBeginDay == null)
			babyBeginDay = 0;
		if (babyEndDay == null)
			babyEndDay = 0;
	}

	private void readFromParcel(Parcel in)
	{
		initDefualtValue();
		this.seqId = in.readLong();
		this.subjectId = in.readString();
		this.title = in.readString();
		this.viewCount = in.readInt();
		this.babyStatus = in.readByte();
		this.babyBeginDay = in.readInt();
		this.babyEndDay = in.readInt();
		this.postTime = in.readString();
		this.createTime = in.readString();
		this.subjectDesc = in.readString();
		this.attachmentList = in.readArrayList(Attachment.class
				.getClassLoader());
		this.topicList = in.readArrayList(Topic.class.getClassLoader());
		this.fromType = in.readInt();
	}

	public Subject(Parcel source)
	{
		readFromParcel(source);
	}
	public static final Parcelable.Creator<Subject> CREATOR = new Parcelable.Creator<Subject>()
	{

		@Override
		public Subject[] newArray(int size)
		{
			return new Subject[size];
		}

		@Override
		public Subject createFromParcel(Parcel source)
		{
			return new Subject(source);
		}
	};

	@Override
	public int describeContents()
	{
		return 0;
	}

	// Long seqId,String subjectId, String title, Integer viewCount,
	// Byte babyStatus, Integer babyBeginDay, Integer babyEndDay,
	// String postTime, String createTime, String subjectDesc,int fromType
	//
	// private List<Attachment> attachmentList;
	//
	// private List<Topic> topicList;
	@Override
	public void writeToParcel(Parcel dest, int flags)
	{
		initDefualtValue();
		dest.writeLong(seqId);
		dest.writeString(subjectId);
		dest.writeString(title);
		dest.writeInt(viewCount);
		dest.writeByte(babyStatus);
		dest.writeInt(babyBeginDay);
		dest.writeInt(babyEndDay);
		dest.writeString(postTime);
		dest.writeString(createTime);
		dest.writeString(subjectDesc);
		dest.writeList(attachmentList);
		dest.writeList(topicList);
		dest.writeInt(fromType);
	}
}