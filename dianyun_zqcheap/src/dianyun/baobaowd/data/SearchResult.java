package dianyun.baobaowd.data;

import java.util.List;

public class SearchResult {
	
	/**
	 * 搜索类型
	 * 1、提问
	 * 2、帖子
	 * 3、用户
	 */
	private Byte searchType;
	
	/**
	 * 拆分过的关键词列表
	 */
	private List<String> spiltWords;
	
	/**
	 * 根据搜索类型确定
	 * 提问搜索, T为Question对象
	 * 帖子搜索，T为Topic对象
	 * 用户搜索,T为User对象
	 */
	private List<Question> resultList;

	public List<String> getSpiltWords() {
		return spiltWords;
	}

	public void setSpiltWords(List<String> spiltWords) {
		this.spiltWords = spiltWords;
	}

	public List<Question> getResultList() {
		return resultList;
	}

	public void setResultList(List<Question> resultList) {
		this.resultList = resultList;
	}

	public Byte getSearchType() {
		return searchType;
	}

	public void setSearchType(Byte searchType) {
		this.searchType = searchType;
	}
}
