package dianyun.baobaowd.data;

public class ReceiverAddress
{

	private Long addrId = 0L;
	private Long uid = 0L;
	private String name;
	private String state  ;
	private String city ;
	private String district ;
	private String address;
	private String zip;
	private String mobile = "no";
	private String phone;

	public ReceiverAddress()
	{
	}

	
	public  String  getProvince(){
		return state+"-"+city+"-"+district;
	}
	
	
	public ReceiverAddress(Long uid, String name, String phone, String zip,
			String address)
	{
		this.uid = uid;
		this.name = name;
		this.phone = phone;
		this.zip = zip;
		this.address = address;
	}

	/*
	 * 地址ID PK
	 */
	public Long getAddrId()
	{
		return addrId;
	}

	public void setAddrId(Long addrId)
	{
		this.addrId = addrId;
	}

	/*
	 * 用户id
	 */
	public Long getUid()
	{
		return uid;
	}

	public void setUid(Long uid)
	{
		this.uid = uid;
	}

	/*
	 * 收件人名称 不能为空
	 */
	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name == null ? null : name.trim();
	}

	/*
	 * 省(自治区) 不能为空
	 */
	public String getState()
	{
		return state;
	}

	public void setState(String state)
	{
		this.state = state == null ? null : state.trim();
	}

	/*
	 * 市 不能为空 （如果是省是直辖市 ，这里为字符串）
	 */
	public String getCity()
	{
		return city;
	}

	public void setCity(String city)
	{
		this.city = city == null ? null : city.trim();
	}

	/*
	 * 县 不能为空
	 */
	public String getDistrict()
	{
		return district;
	}

	public void setDistrict(String district)
	{
		this.district = district == null ? null : district.trim();
	}

	/*
	 * 详细地址 不能为空
	 */
	public String getAddress()
	{
		return address;
	}

	public void setAddress(String address)
	{
		this.address = address == null ? null : address.trim();
	}

	/*
	 * 邮编 不能为空
	 */
	public String getZip()
	{
		return zip;
	}

	public void setZip(String zip)
	{
		this.zip = zip == null ? null : zip.trim();
	}

	/*
	 * 手机 不能为空 可以用空字符串
	 */
	// public String getMobile() {
	// return mobile;
	// }
	//
	// public void setMobile(String mobile) {
	// this.mobile = mobile == null ? null : mobile.trim();
	// }
	/*
	 * 电话 不能为空 可以用空字符串
	 */
	public String getPhone()
	{
		return phone;
	}

	public void setPhone(String phone)
	{
		this.phone = phone == null ? null : phone.trim();
	}
}