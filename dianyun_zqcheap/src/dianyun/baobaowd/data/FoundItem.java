package dianyun.baobaowd.data;


public class FoundItem
{
	public static final int TYPE_TASK=1;
	public static final int TYPE_STORE=2;
	public static final int TYPE_RANK=3;
	public static final int TYPE_COUNSELOR=4;
	public static final int TYPE_TOOLS=5;
	public static final int TYPE_TALENT=6;
	private int type;
	private String name;
	private int drawable;
	
	
	
	
	public FoundItem(int type, String name,int drawable)
	{
		super();
		this.type = type;
		this.name = name;
		this.drawable = drawable;
	}

	
	
	public int getDrawable()
	{
		return drawable;
	}


	
	public void setDrawable(int drawable)
	{
		this.drawable = drawable;
	}


	public int getType()
	{
		return type;
	}
	
	public void setType(int type)
	{
		this.type = type;
	}
	
	public String getName()
	{
		return name;
	}
	
	public void setName(String name)
	{
		this.name = name;
	}
	
	
	
	
	
	
}
