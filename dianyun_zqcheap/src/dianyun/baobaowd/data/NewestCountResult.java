package dianyun.baobaowd.data;

public class NewestCountResult
{

	public NewestCountResult()
	{
	}
	private int answerCount;
	private int questionCount;
	private int appreciationCount;
	private int messageCount;
	private int accAnswerCount;
	private int postCount;
	private int mailCount;
	private int feedCount;
	
	
	
	
//	最新返利数：fanliCount
//	最新提现数：cashCount
	
	private int fanliCount;
	private int cashCount;
	
	

	public int getFanliCount() {
		return fanliCount;
	}

	public void setFanliCount(int fanliCount) {
		this.fanliCount = fanliCount;
	}

	public int getCashCount() {
		return cashCount;
	}

	public void setCashCount(int cashCount) {
		this.cashCount = cashCount;
	}

	public int getFeedCount()
	{
		return feedCount;
	}

	public void setFeedCount(int feedCount)
	{
		this.feedCount = feedCount;
	}

	public int getPostCount()
	{
		return postCount;
	}

	public void setPostCount(int postCount)
	{
		this.postCount = postCount;
	}

	public int getMailCount()
	{
		return mailCount;
	}

	public void setMailCount(int mailCount)
	{
		this.mailCount = mailCount;
	}

	public int getAnswerCount()
	{
		return answerCount;
	}

	public void setAnswerCount(int answerCount)
	{
		this.answerCount = answerCount;
	}

	public int getQuestionCount()
	{
		return questionCount;
	}

	public void setQuestionCount(int questionCount)
	{
		this.questionCount = questionCount;
	}

	public int getAppreciationCount()
	{
		return appreciationCount;
	}

	public void setAppreciationCount(int appreciationCount)
	{
		this.appreciationCount = appreciationCount;
	}

	public int getMessageCount()
	{
		return messageCount;
	}

	public void setMessageCount(int messageCount)
	{
		this.messageCount = messageCount;
	}

	public int getAccAnswerCount()
	{
		return accAnswerCount;
	}

	public void setAccAnswerCount(int accAnswerCount)
	{
		this.accAnswerCount = accAnswerCount;
	}
}