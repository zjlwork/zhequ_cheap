package dianyun.baobaowd.data;

import java.util.List;

public class ItemDetail extends Item
{

	private String desc;
	private List<Attachment> images;
	private List<Attachment> detailImages;
	private String detailDesc;
	// 价格属性
	private ItemPriceDetail itemPriceDetail;
	// 其它属性列表
	private List<ItemPropeDetail> itemPropeDetailList;
}