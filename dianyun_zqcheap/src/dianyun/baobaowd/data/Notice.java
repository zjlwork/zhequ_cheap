package dianyun.baobaowd.data;

public class Notice {
	public Notice() {

	}

	private Long seqId;
	private String content;

	public Long getSeqId() {
		return seqId;
	}

	public void setSeqId(Long seqId) {
		this.seqId = seqId;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

}
