package dianyun.baobaowd.data;

import java.util.Date;

/**
 * 商品专辑
 */
public class Special
{

	private Long specialId;
	/**
	 * 标题
	 */
	private String title;
	/**
	 * 开始时间
	 */
	private Date startTime;
	/**
	 * 结束
	 */
	private Date stopTime;
	/**
	 * 图片
	 */
	private String pic;
	/**
	 * 说明
	 */
	private String desc;
	/**
	 * 排序
	 */
	private Integer sort;
	private Date createTime;
	private String pictures;

	public Long getSpecialId()
	{
		return specialId;
	}

	public void setSpecialId(Long specialId)
	{
		this.specialId = specialId;
	}

	public String getTitle()
	{
		return title;
	}

	public void setTitle(String title)
	{
		this.title = title == null ? null : title.trim();
	}

	public Date getStartTime()
	{
		return startTime;
	}

	public void setStartTime(Date startTime)
	{
		this.startTime = startTime;
	}

	public Date getStopTime()
	{
		return stopTime;
	}

	public void setStopTime(Date stopTime)
	{
		this.stopTime = stopTime;
	}

	public String getPic()
	{
		return pic;
	}

	public void setPic(String pic)
	{
		this.pic = pic == null ? null : pic.trim();
	}

	public String getDesc()
	{
		return desc;
	}

	public void setDesc(String desc)
	{
		this.desc = desc == null ? null : desc.trim();
	}

	public Integer getSort()
	{
		return sort;
	}

	public void setSort(Integer sort)
	{
		this.sort = sort;
	}

	public Date getCreateTime()
	{
		return createTime;
	}

	public void setCreateTime(Date createTime)
	{
		this.createTime = createTime;
	}

	public String getPictures()
	{
		return pictures;
	}

	public void setPictures(String pictures)
	{
		this.pictures = pictures == null ? null : pictures.trim();
	}
}