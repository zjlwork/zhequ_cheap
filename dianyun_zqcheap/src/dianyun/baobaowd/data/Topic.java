package dianyun.baobaowd.data;

import java.util.List;
import android.os.Parcel;
import android.os.Parcelable;

/**
 * 主题帖
 */
public class Topic implements Parcelable
{

	/**
	 * 客户端生成的uuid
	 */
	private String topicId;
	/**
	 * 自增序号
	 */
	private Long seqId = 0L;
	/**
	 * 帖子内容
	 */
	private String title;
	/**
	 * 帖子内容
	 */
	private String content;
	/**
	 * 版块ID
	 */
	private Long boardId = 0L;
	/**
	 * 发帖人
	 */
	private User user;
	private Long uid;
	/**
	 * 发帖时间
	 */
	private String postTime;
	/**
	 * 浏览次数
	 */
	private Integer viewCount = 0;
	/**
	 * 回帖次数
	 */
	private Integer commentCount = 0;
	/**
	 * 发帖人的昵称
	 */
	private String username;
	/**
	 * 发帖人的宝宝生日
	 */
	private String babyBirthday;
	/**
	 * 来源 0：正常用户 1：马甲用户
	 */
	private Byte postFrom = 0;
	/**
	 * 是否置顶 1：置顶
	 */
	private Byte isTop = 0;
	/**
	 * 是否加精 1：加精
	 */
	private Byte isRecommended = 0;
	/**
	 * 是否显示在主屏 1：是
	 */
	private Byte isMainPageShow = 0;
	/**
	 * 帖子的图片列表
	 */
	private List<Attachment> attachmentList;
	private Long lastReplyTime = 0L;
	/**
	 * 加精时间
	 */
	private Long recommendedTime = 0L;
	/**
	 * 被设置为首页精彩推荐的时间
	 */
	private Long mainPageShowTime = 0L;
	/**
	 * 是否已收藏 1： 是 0： 否
	 */
	private Byte isFav = 0;
	private byte isNewestReply = 0;
	private byte isNewestTopic = 0;
	private byte isMarrowTopic = 0;
	private long maxPostSeqId = 0L;
	public static final int TOPICTYPE_NORMAL = 0;
	public static final int TOPICTYPE_MUSIC = 1;
	/**
	 * 帖子类型 0 普通帖 1 音乐帖
	 */
	private Byte topicType = 0;
	/**
	 * 引用ID
	 */
	private String referId;
	/**
	 * 引用对象
	 */
	private Object referObj;
	
	private String city;
	private Integer bbStatus=0;
	
	
	
	/**
	 * 热门
	 */
	private Byte isHot=0;
	/**
	 * 热门设置时间
	 */
	private Long hotTime=0L;
	
	
	
	
	
	
	
//	
//	"boardId":3, //同城:10000,同龄:10001
//	"city":"天津市-天津市",
//	"bbStatus":0,//备孕:0,仅显示指定备孕状态是需要填写
//	"babyBirthday":"1984-01-01"
	
	
	
	 
	public Long getHotTime()
	{
		if(hotTime==null)hotTime=0L;
		return hotTime;
	}

	
	public void setHotTime(Long hotTime)
	{
		this.hotTime = hotTime;
	}
	
	
	
	
	
	
	
	

	
	public Byte getIsHot()
	{
		if(isHot==null)isHot=0;
		return isHot;
	}


	
	public void setIsHot(Byte isHot)
	{
		this.isHot = isHot;
	}


	public Topic(){
		 
	 }

	// 发帖
	public Topic(String topicId, String title, String content, Long boardId,
			List<Attachment> attachmentList,String city,Integer bbStatus,String babyBirthday)
	{
		super();
		this.topicId = topicId;
		this.title = title;
		this.content = content;
		this.boardId = boardId;
		this.attachmentList = attachmentList;
		this.city = city;
		this.bbStatus = bbStatus;
		this.babyBirthday = babyBirthday;
	}

	public Topic(String topicId, Long seqId, String title, String content,
			Long boardId, Long uid, String postTime, Integer viewCount,
			Integer commentCount, String username, String babyBirthday,
			Byte postFrom, Byte isTop, Byte isRecommended, Byte isMainPageShow,
			Long lastReplyTime, Byte isFav, Long recommendedTime,
			Long mainPageShowTime, byte isNewestReply, byte isNewestTopic,
			byte isMarrowTopic, long maxPostSeqId, byte topicType,
			String referId,String city,Integer bbStatus,Byte isHot,Long hotTime)
	{
		super();
		this.seqId = seqId;
		this.topicId = topicId;
		this.title = title;
		this.content = content;
		this.boardId = boardId;
		this.uid = uid;
		this.postTime = postTime;
		this.viewCount = viewCount;
		this.commentCount = commentCount;
		this.username = username;
		this.babyBirthday = babyBirthday;
		this.postFrom = postFrom;
		this.isTop = isTop;
		this.isRecommended = isRecommended;
		this.isMainPageShow = isMainPageShow;
		this.lastReplyTime = lastReplyTime;
		this.isFav = isFav;
		this.recommendedTime = recommendedTime;
		this.mainPageShowTime = mainPageShowTime;
		this.isNewestReply = isNewestReply;
		this.isNewestTopic = isNewestTopic;
		this.isMarrowTopic = isMarrowTopic;
		this.maxPostSeqId = maxPostSeqId;
		this.topicType = topicType;
		this.referId = referId;
		this.city = city;
		this.bbStatus = bbStatus;
		this.isHot = isHot;
		this.hotTime = hotTime;
	}
	
	
	
	

	
	public String getCity()
	{
		return city;
	}

	
	public void setCity(String city)
	{
		this.city = city;
	}

	
	public Integer getBbStatus()
	{
		return bbStatus;
	}

	
	public void setBbStatus(Integer bbStatus)
	{
		this.bbStatus = bbStatus;
	}

	public Byte getTopicType()
	{
		return topicType;
	}

	public void setTopicType(Byte topicType)
	{
		this.topicType = topicType;
	}

	public String getReferId()
	{
		return referId;
	}

	public void setReferId(String referId)
	{
		this.referId = referId;
	}

	public Object getReferObj()
	{
		return referObj;
	}

	public void setReferObj(Object referObj)
	{
		this.referObj = referObj;
	}

	public long getMaxPostSeqId()
	{
		return maxPostSeqId;
	}

	//
	//
	//
	// public void setMaxPostSeqId(long maxPostSeqId) {
	// this.maxPostSeqId = maxPostSeqId;
	// }
	public byte getIsNewestReply()
	{
		return isNewestReply;
	}

	public void setMaxPostSeqId(long maxPostSeqId)
	{
		this.maxPostSeqId = maxPostSeqId;
	}

	public void setIsNewestReply(byte isNewestReply)
	{
		this.isNewestReply = isNewestReply;
	}

	public byte getIsNewestTopic()
	{
		return isNewestTopic;
	}

	public void setIsNewestTopic(byte isNewestTopic)
	{
		this.isNewestTopic = isNewestTopic;
	}

	public byte getIsMarrowTopic()
	{
		return isMarrowTopic;
	}

	public void setIsMarrowTopic(byte isMarrowTopic)
	{
		this.isMarrowTopic = isMarrowTopic;
	}

	public Long getUid()
	{
		return uid;
	}

	public void setUid(Long uid)
	{
		this.uid = uid;
	}

	public Long getRecommendedTime()
	{
		return recommendedTime;
	}

	public void setRecommendedTime(Long recommendedTime)
	{
		this.recommendedTime = recommendedTime;
	}

	public Long getMainPageShowTime()
	{
		return mainPageShowTime;
	}

	public void setMainPageShowTime(Long mainPageShowTime)
	{
		this.mainPageShowTime = mainPageShowTime;
	}

	public Byte getIsFav()
	{
		return isFav;
	}

	public void setIsFav(Byte isFav)
	{
		this.isFav = isFav;
	}

	public Long getLastReplyTime()
	{
		return lastReplyTime;
	}

	public void setLastReplyTime(Long lastReplyTime)
	{
		this.lastReplyTime = lastReplyTime;
	}

	public Long getSeqId()
	{
		return seqId;
	}

	public void setSeqId(Long seqId)
	{
		this.seqId = seqId;
	}

	public String getTopicId()
	{
		return topicId;
	}

	public void setTopicId(String topicId)
	{
		this.topicId = topicId;
	}

	public String getTitle()
	{
		return title;
	}

	public void setTitle(String title)
	{
		this.title = title;
	}

	public String getContent()
	{
		return content;
	}

	public void setContent(String content)
	{
		this.content = content;
	}

	public Long getBoardId()
	{
		return boardId;
	}

	public void setBoardId(Long boardId)
	{
		this.boardId = boardId;
	}

	public User getUser()
	{
		return user;
	}

	public void setUser(User user)
	{
		this.user = user;
	}

	public String getPostTime()
	{
		return postTime;
	}

	public void setPostTime(String postTime)
	{
		this.postTime = postTime;
	}

	public Integer getViewCount()
	{
		return viewCount;
	}

	public void setViewCount(Integer viewCount)
	{
		this.viewCount = viewCount;
	}

	public Integer getCommentCount()
	{
		return commentCount;
	}

	public void setCommentCount(Integer commentCount)
	{
		this.commentCount = commentCount;
	}

	public String getUsername()
	{
		return username;
	}

	public void setUsername(String username)
	{
		this.username = username;
	}

	public String getBabyBirthday()
	{
		return babyBirthday;
	}

	public void setBabyBirthday(String babyBirthday)
	{
		this.babyBirthday = babyBirthday;
	}

	public Byte getPostFrom()
	{
		return postFrom;
	}

	public void setPostFrom(Byte postFrom)
	{
		this.postFrom = postFrom;
	}

	public Byte getIsTop()
	{
		return isTop;
	}

	public void setIsTop(Byte isTop)
	{
		this.isTop = isTop;
	}

	public Byte getIsRecommended()
	{
		return isRecommended;
	}

	public void setIsRecommended(Byte isRecommended)
	{
		this.isRecommended = isRecommended;
	}

	public Byte getIsMainPageShow()
	{
		return isMainPageShow;
	}

	public void setIsMainPageShow(Byte isMainPageShow)
	{
		this.isMainPageShow = isMainPageShow;
	}

	public List<Attachment> getAttachmentList()
	{
		return attachmentList;
	}

	public void setAttachmentList(List<Attachment> attachmentList)
	{
		this.attachmentList = attachmentList;
	}

	private void readFromParcel(Parcel in)
	{
		initDefaultValue();
		this.topicId = in.readString();
		this.seqId = in.readLong();
		this.title = in.readString();
		this.content = in.readString();
		this.boardId = in.readLong();
		this.user = in.readParcelable(User.class.getClassLoader());
		this.postTime = in.readString();
		this.viewCount = in.readInt();
		this.commentCount = in.readInt();
		this.username = in.readString();
		this.babyBirthday = in.readString();
		this.postFrom = in.readByte();
		this.isTop = in.readByte();
		this.isRecommended = in.readByte();
		this.isMainPageShow = in.readByte();
		this.attachmentList = in.readArrayList(Attachment.class
				.getClassLoader());
		this.lastReplyTime = in.readLong();
		this.isFav = in.readByte();
		this.recommendedTime = in.readLong();
		this.mainPageShowTime = in.readLong();
		this.isNewestReply = in.readByte();
		this.isNewestTopic = in.readByte();
		this.isMarrowTopic = in.readByte();
		this.maxPostSeqId = in.readLong();
		this.topicType = in.readByte();
		this.referId = in.readString();
		this.city = in.readString();
		this.bbStatus= in.readInt();
		this.isHot= in.readByte();
		this.hotTime= in.readLong();
		
	}

	public Topic(Parcel source)
	{
		readFromParcel(source);
	}
	public static final Parcelable.Creator<Topic> CREATOR = new Parcelable.Creator<Topic>()
	{

		@Override
		public Topic[] newArray(int size)
		{
			return new Topic[size];
		}

		@Override
		public Topic createFromParcel(Parcel source)
		{
			return new Topic(source);
		}
	};

	@Override
	public int describeContents()
	{
		return 0;
	}

	private void initDefaultValue()
	{
		if (seqId == null)
			seqId = 0L;
		if (boardId == null)
			boardId = 0L;
		if (viewCount == null)
			viewCount = 0;
		if (commentCount == null)
			commentCount = 0;
		if (postFrom == null)
			postFrom = 0;
		if (isTop == null)
			isTop = 0;
		if (isRecommended == null)
			isRecommended = 0;
		if (isMainPageShow == null)
			isMainPageShow = 0;
		if (isFav == null)
			isFav = 0;
		if (lastReplyTime == null)
			lastReplyTime = 0L;
		if (recommendedTime == null)
			recommendedTime = 0L;
		if (mainPageShowTime == null)
			mainPageShowTime = 0L;
		if (mainPageShowTime == null)
			mainPageShowTime = 0L;
		if (mainPageShowTime == null)
			mainPageShowTime = 0L;
		if (topicType == null)
			topicType = 0;
		if (bbStatus == null)
			bbStatus = 0;
		if (isHot == null)
			isHot = 0;
		if (hotTime == null)
			hotTime = 0L;
		
	}

	@Override
	public void writeToParcel(Parcel dest, int flags)
	{
		initDefaultValue();
		dest.writeString(topicId);
		dest.writeLong(seqId);
		dest.writeString(title);
		dest.writeString(content);
		dest.writeLong(boardId);
		dest.writeParcelable(user, flags);
		dest.writeString(postTime);
		dest.writeInt(viewCount);
		dest.writeInt(commentCount);
		dest.writeString(username);
		dest.writeString(babyBirthday);
		dest.writeByte(postFrom);
		dest.writeByte(isTop);
		dest.writeByte(isRecommended);
		dest.writeByte(isMainPageShow);
		dest.writeList(attachmentList);
		dest.writeLong(lastReplyTime);
		dest.writeByte(isFav);
		dest.writeLong(recommendedTime);
		dest.writeLong(mainPageShowTime);
		dest.writeByte(isNewestReply);
		dest.writeByte(isNewestTopic);
		dest.writeByte(isMarrowTopic);
		dest.writeLong(maxPostSeqId);
		dest.writeByte(topicType);
		dest.writeString(referId);
		dest.writeString(city);
		dest.writeInt(bbStatus);
		dest.writeByte(isHot);
		dest.writeLong(hotTime);
		
		
	}
}