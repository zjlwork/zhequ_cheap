package dianyun.baobaowd.data;

public class Tips
{

	private Long tipsId;
	// private Integer keyId;
	private String topicId;

	// private String title;
	public Long getTipsId()
	{
		return tipsId;
	}

	public void setTipsId(Long tipsId)
	{
		this.tipsId = tipsId;
	}

	// public Integer getKeyId() {
	// return keyId;
	// }
	//
	// public void setKeyId(Integer keyId) {
	// this.keyId = keyId;
	// }
	public String getTopicId()
	{
		return topicId;
	}

	public void setTopicId(String topicId)
	{
		this.topicId = topicId == null ? null : topicId.trim();
	}
	// public String getTitle() {
	// return title;
	// }
	//
	// public void setTitle(String title) {
	// this.title = title;
	// }
}