package dianyun.baobaowd.data;

import java.util.List;

public class WelcomePicture {

	public WelcomePicture() {

	}

	private Integer seqId=0;

	private String channel;

	private String device;

	private String askVersion;

	private Byte screenType;

	private String endDate;

	private String postTime;

	private List<Attachment> attachmentList;
	
	
	 //跳转类型 (1:转到论坛版块；2：应用内其它页面；3：应用内打开网页；4:应用外打开网页; 5:问题详情; 6：帖子详情 ；
    private Integer jumpType=0;
    

   /*
    * 跳转值
    * jumpType=1 (论坛版块对应的ID)
    * jumpType=2 (1:直接提问；2：当日知识)
    * jumpType=3 (url)
    * jumpType=4 (url)
    * jumpType=5 (问题ID)
    * jumpType=6 (帖子ID)
    * 
    */
   private String jumpValue;
   
	
	
	private String fileUrl;
	private String localPath;
	

	private Integer appId;
	
	
	public String getFileUrl() {
		return fileUrl;
	}


	public void setFileUrl(String fileUrl) {
		this.fileUrl = fileUrl;
	}


	public String getLocalPath() {
		return localPath;
	}


	public void setLocalPath(String localPath) {
		this.localPath = localPath;
	}


	//	seqId LONG PRIMARY KEY NOT NULL,fileUrl Text,localPath TEXT,jumpType INTEGER, jumpValue TEXT
	public WelcomePicture(int seqId,String fileUrl,String localPath,int jumpType,String jumpValue ){
		this.seqId = seqId;
		this.fileUrl = fileUrl;
		this.localPath = localPath;
		this.jumpType = jumpType;
		this.jumpValue = jumpValue;
	}
	

	public Integer getJumpType() {
		if(jumpType==null)jumpType=0;
		return jumpType;
	}

	public void setJumpType(Integer jumpType) {
		this.jumpType = jumpType;
	}

	public String getJumpValue() {
		return jumpValue;
	}

	public void setJumpValue(String jumpValue) {
		this.jumpValue = jumpValue;
	}

	public Integer getSeqId() {
		if(seqId==null)seqId=0;
		return seqId;
	}

	public void setSeqId(Integer seqId) {
		this.seqId = seqId;
	}

	public String getChannel() {
		return channel;
	}

	public void setChannel(String channel) {
		this.channel = channel;
	}

	public String getDevice() {
		return device;
	}

	public void setDevice(String device) {
		this.device = device;
	}

	public String getAskVersion() {
		return askVersion;
	}

	public void setAskVersion(String askVersion) {
		this.askVersion = askVersion;
	}

	public Byte getScreenType() {
		return screenType;
	}

	public void setScreenType(Byte screenType) {
		this.screenType = screenType;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public String getPostTime() {
		return postTime;
	}

	public void setPostTime(String postTime) {
		this.postTime = postTime;
	}

	public List<Attachment> getAttachmentList() {
		return attachmentList;
	}

	public void setAttachmentList(List<Attachment> attachmentList) {
		this.attachmentList = attachmentList;
	}

	public Integer getAppId() {
		return appId;
	}

	public void setAppId(Integer appId) {
		this.appId = appId;
	}

}
