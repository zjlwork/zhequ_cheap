package dianyun.baobaowd.data;

public class Relation
{

	private String relationId;
	private String subjectId;
	private String topicId;
	private String albumId;
	private String musicId;

	public Relation()
	{
	}

	public Relation(String relationId, String subjectId, String topicId)
	{
		super();
		this.relationId = relationId;
		this.subjectId = subjectId;
		this.topicId = topicId;
	}

	public void setAlbumMusicRelation(String relationId, String albumId,
			String musicId)
	{
		this.relationId = relationId;
		this.albumId = albumId;
		this.musicId = musicId;
	}

	public String getAlbumId()
	{
		return albumId;
	}

	public void setAlbumId(String albumId)
	{
		this.albumId = albumId;
	}

	public String getMusicId()
	{
		return musicId;
	}

	public void setMusicId(String musicId)
	{
		this.musicId = musicId;
	}

	public String getRelationId()
	{
		return relationId;
	}

	public void setRelationId(String relationId)
	{
		this.relationId = relationId;
	}

	public String getSubjectId()
	{
		return subjectId;
	}

	public void setSubjectId(String subjectId)
	{
		this.subjectId = subjectId;
	}

	public String getTopicId()
	{
		return topicId;
	}

	public void setTopicId(String topicId)
	{
		this.topicId = topicId;
	}
}