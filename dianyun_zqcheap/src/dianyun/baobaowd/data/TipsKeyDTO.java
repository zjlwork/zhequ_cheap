package dianyun.baobaowd.data;

import java.util.List;

public class TipsKeyDTO
{

	private Long tipsMinId = 0L;
	private List<Topic> topicDTOList;

	public List<Topic> getTopicDTOList()
	{
		return topicDTOList;
	}

	public void setTopicDTOList(List<Topic> topicDTOList)
	{
		this.topicDTOList = topicDTOList;
	}

	public Long getTipsMinId()
	{
		return tipsMinId;
	}

	public void setTipsMinId(Long tipsMinId)
	{
		this.tipsMinId = tipsMinId;
	}
}
