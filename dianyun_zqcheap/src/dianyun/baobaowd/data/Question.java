package dianyun.baobaowd.data;

import java.util.List;
import android.os.Parcel;
import android.os.Parcelable;

public class Question implements Parcelable
{

	/**
	 * 序号
	 */
	private Long seqId = 0L;
	/**
	 * uuid
	 */
	private String questionId;
	private Long uid = 0L;
	/**
	 * 内容
	 */
	private String content;
	/**
	 * 发布者的
	 */
	private User user;
	/**
	 * 发布时间
	 */
	private String postTime;
	/**
	 * 浏览次数
	 */
	private int viewCount = 0;
	/**
	 * 回复次数
	 */
	private int commentCount = 0;
	/**
	 * 是否置顶 1：是 0：否
	 */
	private Byte isTop = 0;
	/**
	 * 是否已经有被采纳答案 1： 是 0： 否
	 */
	private Byte bestAnswerStatus = 0;
	/**
	 * 是否收藏
	 */
	private Byte isFav = 0;
	private byte isNewest;
	private byte isRecommend;
	private byte isRace;
	private byte isMyQuestion;
	private long maxSeqId;
	private byte isMyFav;
	/**
	 * 热门
	 */
	private Byte isHot;
	/**
	 * 热门设置时间
	 */
	private Long hotTime;
	private List<Attachment> attachmentList;
	/**
	 * 悬赏的金币数
	 */
	private Integer reward = 0;
	
	private Long boardId=0L;
	private String city;
	private Integer bbStatus=0;
	private String babyBirthday;
	
	
//	boardId":3, //同城:16,同龄:17
//	"city":"天津市-天津市",
//	"bbStatus":0,//备孕:0,仅显示指定备孕状态是需要填写
//	"babyBirthday
	
	
	
	
	
	
	

	public Question()
	{
	}
	

	// 数据库对应
	public Question(String questionId, Long seqId, String content, Long uid,
			String postTime, int viewCount, int commentCount, Byte isTop,
			Byte bestAnswerStatus, Byte isFav, byte isNewest, byte isRecommend,
			byte isRace, byte isMyQuestion, long maxSeqId, byte isMyFav,
			Byte isHot, Long hotTime, Integer reward,Long boardId,String city
			,Integer bbStatus,String babyBirthday)
	{
		super();
		this.seqId = seqId;
		this.questionId = questionId;
		this.content = content;
		this.postTime = postTime;
		this.viewCount = viewCount;
		this.commentCount = commentCount;
		this.isTop = isTop;
		this.bestAnswerStatus = bestAnswerStatus;
		this.isFav = isFav;
		this.uid = uid;
		this.isNewest = isNewest;
		this.isRecommend = isRecommend;
		this.isRace = isRace;
		this.isMyQuestion = isMyQuestion;
		this.maxSeqId = maxSeqId;
		this.isMyFav = isMyFav;
		this.isHot = isHot;
		this.hotTime = hotTime;
		this.reward = reward;
		
		this.boardId = boardId;
		this.city = city;
		this.bbStatus = bbStatus;
		this.babyBirthday = babyBirthday;
	}

	
	
	// 添加问题
	public Question(String questionId, String content, User user, Long uid,
			String postTime, Integer reward,Long boardId,String city,Integer bbStatus,String babyBirthday)
	{
		this.questionId = questionId;
		this.content = content;
		this.user = user;
		this.postTime = postTime;
		this.uid = uid;
		this.questionId = questionId;
		this.reward = reward;
		this.boardId = boardId;
		this.city = city;
		this.bbStatus = bbStatus;
		this.babyBirthday = babyBirthday;
	}
	
	
	
	

	
	public Long getBoardId()
	{
		return boardId;
	}


	
	public void setBoardId(Long boardId)
	{
		this.boardId = boardId;
	}


	
	public String getCity()
	{
		return city;
	}


	
	public void setCity(String city)
	{
		this.city = city;
	}


	
	public Integer getBbStatus()
	{
		return bbStatus;
	}


	
	public void setBbStatus(Integer bbStatus)
	{
		this.bbStatus = bbStatus;
	}


	
	public String getBabyBirthday()
	{
		return babyBirthday;
	}


	
	public void setBabyBirthday(String babyBirthday)
	{
		this.babyBirthday = babyBirthday;
	}


	public Integer getReward()
	{
		if (reward == null)
			reward = 0;
		return reward;
	}

	public void setReward(Integer reward)
	{
		this.reward = reward;
	}

	public byte getIsMyFav()
	{
		return isMyFav;
	}

	public void setIsMyFav(byte isMyFav)
	{
		this.isMyFav = isMyFav;
	}

	public Byte getIsFav()
	{
		return isFav;
	}

	public void setIsFav(Byte isFav)
	{
		this.isFav = isFav;
	}

	public Byte getIsTop()
	{
		return isTop;
	}

	public void setIsTop(Byte isTop)
	{
		this.isTop = isTop;
	}

	public Byte getBestAnswerStatus()
	{
		return bestAnswerStatus;
	}

	public void setBestAnswerStatus(Byte bestAnswerStatus)
	{
		this.bestAnswerStatus = bestAnswerStatus;
	}

	public long getMaxSeqId()
	{
		return maxSeqId;
	}

	public void setMaxSeqId(long maxSeqId)
	{
		this.maxSeqId = maxSeqId;
	}

	public byte getIsNewest()
	{
		return isNewest;
	}

	public void setIsNewest(byte isNewest)
	{
		this.isNewest = isNewest;
	}

	public byte getIsRecommend()
	{
		return isRecommend;
	}

	public void setIsRecommend(byte isRecommend)
	{
		this.isRecommend = isRecommend;
	}

	public byte getIsRace()
	{
		return isRace;
	}

	public void setIsRace(byte isRace)
	{
		this.isRace = isRace;
	}

	public byte getIsMyQuestion()
	{
		return isMyQuestion;
	}

	public void setIsMyQuestion(byte isMyQuestion)
	{
		this.isMyQuestion = isMyQuestion;
	}

	public Long getUid()
	{
		return uid;
	}

	public void setUid(Long uid)
	{
		this.uid = uid;
	}

	public List<Attachment> getAttachmentList()
	{
		return attachmentList;
	}

	public void setAttachmentList(List<Attachment> attachmentList)
	{
		this.attachmentList = attachmentList;
	}

	public Long getSeqId()
	{
		return seqId;
	}

	public void setSeqId(Long seqId)
	{
		this.seqId = seqId;
	}

	public String getQuestionId()
	{
		return questionId;
	}

	public void setQuestionId(String questionId)
	{
		this.questionId = questionId;
	}

	public String getContent()
	{
		return content;
	}

	public void setContent(String content)
	{
		this.content = content;
	}

	public User getUser()
	{
		return user;
	}

	public void setUser(User user)
	{
		this.user = user;
	}

	public String getPostTime()
	{
		return postTime;
	}

	public void setPostTime(String postTime)
	{
		this.postTime = postTime;
	}

	public int getViewCount()
	{
		return viewCount;
	}

	public void setViewCount(int viewCount)
	{
		this.viewCount = viewCount;
	}

	public int getCommentCount()
	{
		return commentCount;
	}

	public void setCommentCount(int commentCount)
	{
		this.commentCount = commentCount;
	}

	public Byte getIsHot()
	{
		return isHot;
	}

	public void setIsHot(Byte isHot)
	{
		this.isHot = isHot;
	}

	public Long getHotTime()
	{
		return hotTime;
	}

	public void setHotTime(Long hotTime)
	{
		this.hotTime = hotTime;
	}

	private void readFromParcel(Parcel in)
	{
		if (isTop == null)
			isTop = 0;
		if (bestAnswerStatus == null)
			bestAnswerStatus = 0;
		if (isFav == null)
			isFav = 0;
		if (isHot == null)
			isHot = 0;
		if (hotTime == null)
			hotTime = 0L;
		if (reward == null)
			reward = 0;
		if (boardId == null)
			boardId = 0L;
		if (bbStatus == null)
			bbStatus = 0;
		
		
		
		
		this.seqId = in.readLong();
		this.questionId = in.readString();
		this.content = in.readString();
		this.user = in.readParcelable(User.class.getClassLoader());
		this.postTime = in.readString();
		this.viewCount = in.readInt();
		this.commentCount = in.readInt();
		this.isTop = in.readByte();
		this.bestAnswerStatus = in.readByte();
		this.isFav = in.readByte();
		this.attachmentList = in.readArrayList(Attachment.class
				.getClassLoader());
		this.uid = in.readLong();
		this.isNewest = in.readByte();
		this.isRecommend = in.readByte();
		this.isRace = in.readByte();
		this.isMyQuestion = in.readByte();
		this.maxSeqId = in.readLong();
		this.isMyFav = in.readByte();
		this.isHot = in.readByte();
		this.hotTime = in.readLong();
		this.reward = in.readInt();
		this.boardId = in.readLong();
		this.city = in.readString();
		this.bbStatus = in.readInt();
		this.babyBirthday = in.readString();
		
		
	}

	public Question(Parcel source)
	{
		readFromParcel(source);
	}
	public static final Parcelable.Creator<Question> CREATOR = new Parcelable.Creator<Question>()
	{

		@Override
		public Question[] newArray(int size)
		{
			return new Question[size];
		}

		@Override
		public Question createFromParcel(Parcel source)
		{
			return new Question(source);
		}
	};

	@Override
	public int describeContents()
	{
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags)
	{
		if (isTop == null)
			isTop = 0;
		if (bestAnswerStatus == null)
			bestAnswerStatus = 0;
		if (isFav == null)
			isFav = 0;
		if (isHot == null)
			isHot = 0;
		if (hotTime == null)
			hotTime = 0L;
		if (reward == null)
			reward = 0;
		
		if (boardId == null)
			boardId = 0L;
		if (bbStatus == null)
			bbStatus = 0;
	
		
		
		dest.writeLong(seqId);
		dest.writeString(questionId);
		dest.writeString(content);
		dest.writeParcelable(user, flags);
		dest.writeString(postTime);
		dest.writeInt(viewCount);
		dest.writeInt(commentCount);
		dest.writeByte(isTop);
		dest.writeByte(bestAnswerStatus);
		dest.writeByte(isFav);
		dest.writeList(attachmentList);
		dest.writeLong(uid);
		dest.writeByte(isNewest);
		dest.writeByte(isRecommend);
		dest.writeByte(isRace);
		dest.writeByte(isMyQuestion);
		dest.writeLong(maxSeqId);
		dest.writeByte(isMyFav);
		dest.writeByte(isHot);
		dest.writeLong(hotTime);
		dest.writeInt(reward);
		dest.writeLong(boardId);
		dest.writeString(city);
		dest.writeInt(bbStatus);
		dest.writeString(babyBirthday);
	}
}
