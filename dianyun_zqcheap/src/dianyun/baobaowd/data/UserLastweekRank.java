package dianyun.baobaowd.data;

public class UserLastweekRank
{

	private int accInLastWeek;
	private Long accInLastWeekRank;

	public UserLastweekRank()
	{
	}

	public int getAccInLastWeek()
	{
		return accInLastWeek;
	}

	public void setAccInLastWeek(int accInLastWeek)
	{
		this.accInLastWeek = accInLastWeek;
	}

	public Long getAccInLastWeekRank()
	{
		return accInLastWeekRank;
	}

	public void setAccInLastWeekRank(Long accInLastWeekRank)
	{
		this.accInLastWeekRank = accInLastWeekRank;
	}
}
