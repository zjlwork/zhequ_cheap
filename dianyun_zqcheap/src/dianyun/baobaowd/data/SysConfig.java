package dianyun.baobaowd.data;

public class SysConfig
{

	/**
	 * 帖子页面前缀 http://www.test.ask360.me/topic/id/ 前缀+topicId+.html
	 * http://www.test
	 * .ask360.me/topic/id/d57edaad-c739-f12e-b3ea-28750c0a45ae.html
	 */
	private String topicDetailPrefix;
	/**
	 * 提问页面前缀 http://www.test.ask360.me/ask/id/ 前缀+topicId+.html
	 * http://www.test.
	 * ask360.me/ask/id/d57edaad-c739-f12e-b3ea-28750c0a45ae.html
	 */
	private String askDetailPrefix;
	/**
	 * 返利淘商城的Http请求前缀
	 */
	public String fanliTaeMallUrl;
	/**
	 * 柚柚育儿商城的Http请求前缀
	 */
	public String yoyoTaeMallUrl;
	/**
	 * 扫一扫http头前缀
	 */
	public String tbItemFindUrl;
	/**
	 * 悬赏等级
	 */
	private Integer rewardLevel = 0;
    /**
     * 获取淘宝连接的短链的URL地址
      */
    public String GetTbUrl="";
    /**
     * 返利淘使用的查询返利商品前缀
     */
    public String GetTbFanLiIem="";
    /**
     * 柚柚育儿使用的查询返利商品前缀
     */
    public String   GetTbYoyoItem="";

    /**
     * 柚柚育儿 检出关键字和URL的地址
     */
    public String ExtYoyoContentUrl="";

    /**
     * 返利淘 检出关键字和URL的地址
     */
    public String ExtFanliContentUrl="";
    
    public String JiFenQiangSwitch="";
    public String RegGiveTips="";
    
    public static final String SWITCH_ON = "on";
    public static final  String SWITCH_OFF = "off";
     
    

	public SysConfig()
	{
	}

	public Integer getRewardLevel()
	{
		if (rewardLevel == null)
			rewardLevel = 0;
		return rewardLevel;
	}

	public void setRewardLevel(Integer rewardLevel)
	{
		this.rewardLevel = rewardLevel;
	}

	public String getTopicDetailPrefix()
	{
		return topicDetailPrefix;
	}

	public void setTopicDetailPrefix(String topicDetailPrefix)
	{
		this.topicDetailPrefix = topicDetailPrefix;
	}

	public String getAskDetailPrefix()
	{
		return askDetailPrefix;
	}

	public void setAskDetailPrefix(String askDetailPrefix)
	{
		this.askDetailPrefix = askDetailPrefix;
	}
}
