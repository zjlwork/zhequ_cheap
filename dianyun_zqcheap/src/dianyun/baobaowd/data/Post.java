package dianyun.baobaowd.data;

import java.util.List;
import android.os.Parcel;
import android.os.Parcelable;

/**
 * 回帖
 */
public class Post implements Parcelable
{

	/**
	 * 客户端生成的uuid
	 */
	private String postId;
	/**
	 * 自增序号
	 */
	private Long seqId;
	/**
	 * 回答的问题ID
	 */
	private String topicId;
	/**
	 * 发布时间
	 */
	private String postTime;
	/**
	 * 引用的回答对应的ID
	 */
	private String referPostId;
	/**
	 * 内容
	 */
	private String content;
	/**
	 * 回答者
	 */
	private User user;
	private long uid;
	private List<Attachment> attachmentList;
	/**
	 * 问题的内容
	 */
	private String topicContent;
	/**
	 * 提问者的昵称
	 */
	private String topicNickname;
	private String referPostContent;
	private byte isMyReceivePost;
	private byte isTopicReply;

	public byte getIsTopicReply()
	{
		return isTopicReply;
	}

	public void setIsTopicReply(byte isTopicReply)
	{
		this.isTopicReply = isTopicReply;
	}

	public byte getIsMyReceivePost()
	{
		return isMyReceivePost;
	}

	public void setIsMyReceivePost(byte isMyReceivePost)
	{
		this.isMyReceivePost = isMyReceivePost;
	}

	public Post(String postId, String topicId, String content, User user)
	{
		super();
		this.postId = postId;
		this.topicId = topicId;
		this.content = content;
		this.user = user;
	}

	/*
	 * public Post( String postId,Long seqId, String topicId, String postTime,
	 * String referPostId, String content, User user, List<Attachment>
	 * attachmentList, String topicContent, String topicNickname, String
	 * referPostContent) { super(); this.seqId = seqId; this.postId = postId;
	 * this.topicId = topicId; this.postTime = postTime; this.referPostId =
	 * referPostId; this.content = content; this.user = user;
	 * this.attachmentList = attachmentList; this.topicContent = topicContent;
	 * this.topicNickname = topicNickname; this.referPostContent =
	 * referPostContent; }
	 */
	public Post(String postId, Long seqId, String topicId, String postTime,
			String referPostId, String content, long uid, String topicContent,
			String topicNickname, String referPostContent,
			byte isMyReceivePost, byte isTopicReply)
	{
		super();
		this.seqId = seqId;
		this.postId = postId;
		this.topicId = topicId;
		this.postTime = postTime;
		this.referPostId = referPostId;
		this.content = content;
		this.uid = uid;
		this.topicContent = topicContent;
		this.topicNickname = topicNickname;
		this.referPostContent = referPostContent;
		this.isMyReceivePost = isMyReceivePost;
		this.isTopicReply = isTopicReply;
	}

	public long getUid()
	{
		return uid;
	}

	public void setUid(long uid)
	{
		this.uid = uid;
	}

	public Long getSeqId()
	{
		return seqId;
	}

	public void setSeqId(Long seqId)
	{
		this.seqId = seqId;
	}

	public String getPostId()
	{
		return postId;
	}

	public void setPostId(String postId)
	{
		this.postId = postId;
	}

	public String getTopicId()
	{
		return topicId;
	}

	public void setTopicId(String topicId)
	{
		this.topicId = topicId;
	}

	public String getPostTime()
	{
		return postTime;
	}

	public void setPostTime(String postTime)
	{
		this.postTime = postTime;
	}

	public String getReferPostId()
	{
		return referPostId;
	}

	public void setReferPostId(String referPostId)
	{
		this.referPostId = referPostId;
	}

	public String getContent()
	{
		return content;
	}

	public void setContent(String content)
	{
		this.content = content;
	}

	public User getUser()
	{
		return user;
	}

	public void setUser(User user)
	{
		this.user = user;
	}

	public List<Attachment> getAttachmentList()
	{
		return attachmentList;
	}

	public void setAttachmentList(List<Attachment> attachmentList)
	{
		this.attachmentList = attachmentList;
	}

	public String getTopicContent()
	{
		return topicContent;
	}

	public void setTopicContent(String topicContent)
	{
		this.topicContent = topicContent;
	}

	public String getTopicNickname()
	{
		return topicNickname;
	}

	public void setTopicNickname(String topicNickname)
	{
		this.topicNickname = topicNickname;
	}

	public String getReferPostContent()
	{
		return referPostContent;
	}

	public void setReferPostContent(String referPostContent)
	{
		this.referPostContent = referPostContent;
	}

	private void readFromParcel(Parcel in)
	{
		if (seqId == null)
			seqId = 0L;
		this.postId = in.readString();
		this.seqId = in.readLong();
		this.topicId = in.readString();
		this.postTime = in.readString();
		this.referPostId = in.readString();
		this.content = in.readString();
		this.user = in.readParcelable(User.class.getClassLoader());
		this.uid = in.readLong();
		this.attachmentList = in.readArrayList(Attachment.class
				.getClassLoader());
		this.topicContent = in.readString();
		this.topicNickname = in.readString();
		this.referPostContent = in.readString();
		this.isMyReceivePost = in.readByte();
		this.isTopicReply = in.readByte();
	}

	public Post(Parcel source)
	{
		readFromParcel(source);
	}
	public static final Parcelable.Creator<Post> CREATOR = new Parcelable.Creator<Post>()
	{

		@Override
		public Post[] newArray(int size)
		{
			return new Post[size];
		}

		@Override
		public Post createFromParcel(Parcel source)
		{
			return new Post(source);
		}
	};

	@Override
	public int describeContents()
	{
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags)
	{
		if (seqId == null)
			seqId = 0L;
		dest.writeString(postId);
		dest.writeLong(seqId);
		dest.writeString(topicId);
		dest.writeString(postTime);
		dest.writeString(referPostId);
		dest.writeString(content);
		dest.writeParcelable(user, flags);
		dest.writeLong(uid);
		dest.writeList(attachmentList);
		dest.writeString(topicContent);
		dest.writeString(topicNickname);
		dest.writeString(referPostContent);
		dest.writeByte(isMyReceivePost);
		dest.writeByte(isTopicReply);
	}
}