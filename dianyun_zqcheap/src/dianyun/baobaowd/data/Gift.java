package dianyun.baobaowd.data;

import android.os.Parcel;
import android.os.Parcelable;

public class Gift implements Parcelable
{

	/**
	 * 淘宝商品URL
	 */
	public String taobaoUrl;
	/**
	 * 淘宝商品ID
	 */
	public String taobaoItemId;
	/**
	 * 礼品ID(服务端生成)
	 */
	private Long giftId = 0L;
	/**
	 * 礼品描述
	 */
	private String giftDesc;
	/**
	 * 所需金币数
	 */
	private Integer price = 0;
    /**
     * 所需金币数(返利淘专用)
     */
    private Integer ycoinPrice = 0;
	/**
	 * 库存量（暂时用不到 ）
	 */
	private Integer quantity = 0;
	/**
	 * 图片路径
	 */
	private String imgUrl;
	private boolean isSelected;

	public boolean isSelected()
	{
		return isSelected;
	}

	public void setSelected(boolean isSelected)
	{
		this.isSelected = isSelected;
	}

	public Long getGiftId()
	{
		return giftId;
	}

	public void setGiftId(Long giftId)
	{
		this.giftId = giftId;
	}

	public String getGiftDesc()
	{
		return giftDesc;
	}

	public void setGiftDesc(String giftDesc)
	{
		this.giftDesc = giftDesc;
	}

	public Integer getFanliTaoPrice()
	{
		return ycoinPrice;
	}

	public void setFanliTaoPrice(Integer price)
	{
		this.ycoinPrice = price;
	}


    public Integer getPrice()
    {
        return price;
    }

    public void setPrice(Integer price)
    {
        this.price = price;
    }

	public Integer getQuantity()
	{
		return quantity;
	}

	public void setQuantity(Integer quantity)
	{
		this.quantity = quantity;
	}

	public String getImgUrl()
	{
		return imgUrl;
	}

	public void setImgUrl(String imgUrl)
	{
		this.imgUrl = imgUrl;
	}

	private void readFromParcel(Parcel in)
	{
		if (giftId == null)
			giftId = 0L;
		if (price == null)
			price = 0;
		if (quantity == null)
			quantity = 0;
		this.giftId = in.readLong();
		this.giftDesc = in.readString();
		this.price = in.readInt();
        this.ycoinPrice = in.readInt();
		this.quantity = in.readInt();
		this.imgUrl = in.readString();

	}

	public Gift(Parcel source)
	{
		readFromParcel(source);
	}
	public static final Parcelable.Creator<Gift> CREATOR = new Parcelable.Creator<Gift>()
	{

		@Override
		public Gift[] newArray(int size)
		{
			return new Gift[size];
		}

		@Override
		public Gift createFromParcel(Parcel source)
		{
			return new Gift(source);
		}
	};

	@Override
	public int describeContents()
	{
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags)
	{
		if (giftId == null)
			giftId = 0L;
		if (price == null)
			price = 0;
		if (quantity == null)
			quantity = 0;
		dest.writeLong(giftId);
		dest.writeString(giftDesc);
		dest.writeInt(price);
        dest.writeInt(ycoinPrice);
		dest.writeInt(quantity);
		dest.writeString(imgUrl);
	}
}
