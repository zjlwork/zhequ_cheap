package dianyun.baobaowd.gson;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class GsonFactory
{

	private static Gson lGson;

	private GsonFactory()
	{
	}

	public static Gson getGsonInstance()
	{
		if (null == lGson)
		{
			lGson = new GsonBuilder().create();
		}
		return lGson;
	}
}
