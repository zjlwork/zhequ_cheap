package dianyun.baobaowd.gson;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import dianyun.baobaowd.help.LogFile;

public class GsonHelper
{

	public static <T> T gsonToObj(String gsonStr, Class<T> clazz)
	{
		try{
			Gson gson = GsonFactory.getGsonInstance();
			return gson.fromJson(gsonStr, clazz);
		}catch(Exception e){
			 System.out.println("gsonToObj e==null");
			LogFile.SaveExceptionLog(e);
		}
		return null;
	}

	public static <T> List<T> gsonToObj(String jsonString,
			TypeToken<List<T>> typeToken)
	{
		try{
			Gson gson = GsonFactory.getGsonInstance();
			Type type = typeToken.getType();
			return gson.fromJson(jsonString, type);
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}

	public static String gsonTojson(Object object)
	{
		try{
			Gson gson = GsonFactory.getGsonInstance();
			return gson.toJson(object);
		}catch(Exception e){
			
		}
		return null;
	}
	
	
	
	
	
	
	
	
	
	
	
}
