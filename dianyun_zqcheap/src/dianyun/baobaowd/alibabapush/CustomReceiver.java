package dianyun.baobaowd.alibabapush;

import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import android.content.Context;
import android.util.Log;

import com.alibaba.cchannel.push.receiver.CPushMessage;
import com.alibaba.cchannel.push.receiver.CPushMessageReceiver;
import com.alibaba.cchannel.push.receiver.ChannelStatus;
 
public class CustomReceiver extends CPushMessageReceiver {

    // 消息接收部分的LOG_TAG
    public static final String REC_TAG = "receiver";
    /**
     * 接收到消息的处理
     * @param context
     * @param message
     */
    @Override
    protected void onMessage(Context context, CPushMessage cPushMessage) {
    	 try {

             Log.d(REC_TAG,"收到一条推送消息 ： " + cPushMessage.getTitle()+new String(cPushMessage.getContent(), "UTF-8"));

             // 持久化推送的消息到数据库
//             new MessageDao(context).add(new MessageEntity(cPushMessage.getMessageId(), cPushMessage.getAppId(), cPushMessage.getTitle(), 
// 				new String(cPushMessage.getContent(), "UTF-8"), new SimpleDateFormat("HH:mm:ss").format(new Date())));
//
//             // 刷新下消息列表
//             ActivityBox.CPDMainActivity.initMessageView();
             
             
             
             
             
         } catch (UnsupportedEncodingException e) {
             Log.d(REC_TAG, e.toString());
         }
    }
 
    /**
     * 接收到通知的扩展处理
     * @param context
     * @param title
     * @param summary
     * @param extraMap
     */
    @Override
    protected void onNotification(Context context, String title, String summary, Map<String, String> extraMap) {
    	  // TODO 处理推送通知
        if ( null != extraMap ) {
            for (Map.Entry<String, String> entry : extraMap.entrySet()) {
                Log.d(REC_TAG,"@Get diy param : Key=" + entry.getKey() + " , Value=" + entry.getValue());
            }
        } else {
            Log.d(REC_TAG,"@收到通知 && 自定义消息为空");
        }
        Log.d(REC_TAG,"收到一条推送通知 ： " + title );
    }
 
    /**
     * 从通知栏打开通知的扩展处理
     * @param context
     * @param title
     * @param summary
     * @param extraMap
     */
    @Override
    protected void onNotificationOpened(Context context, String title, String summary, String extraMap) {
 
    }
 
    /**
     * 从通知栏删除通知的扩展处理
     * @param context
     * @param messageId
     */
    @Override
    protected void onNotificationRemoved(Context context, long messageId) {
 
    }
 
    /**
     * 通道状态改变
     * @param context
     * @param channelStatus
     */
    @Override
    protected void onChannelStatusChanged(Context context, ChannelStatus channelStatus) {
 
    }
 
}