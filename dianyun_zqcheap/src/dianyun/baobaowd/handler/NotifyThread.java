package dianyun.baobaowd.handler;

import android.content.Context;
import dianyun.baobaowd.help.FileHelper;

public class NotifyThread extends Thread
{

	private Context mContext;
	private NotifyHandler mNotifyHandler;

	public NotifyThread(Context context, NotifyHandler pNotifyHandler)
	{
		this.mContext = context;
		this.mNotifyHandler = pNotifyHandler;
	}
	private boolean flag = true;

	@Override
	public void run()
	{
		while (flag)
		{
			try
			{
				if (mNotifyHandler.getDownloadFileLength() == -1)
				{
					FileHelper.sendMsg(-1, mNotifyHandler);
					flag = false;
					break;
				}
				else if (mNotifyHandler.getDownloadFileLength() == 0)
				{
					FileHelper.sendMsg(0, mNotifyHandler);
				}
				else
				{
					if (mNotifyHandler.getDownloadFileLength() < mNotifyHandler
							.getFilelength())
					{
						FileHelper.sendMsg(1, mNotifyHandler);
					}
					else
					{
						FileHelper.sendMsg(2, mNotifyHandler);
						flag = false;
						break;
					}
				}
				Thread.sleep(1000);
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
		}
	}
}
