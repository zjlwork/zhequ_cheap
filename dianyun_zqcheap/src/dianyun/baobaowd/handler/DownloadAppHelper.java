package dianyun.baobaowd.handler;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.widget.Toast;
import dianyun.baobaowd.util.AppHelper;
import dianyun.baobaowd.util.BroadCastHelper;
import dianyun.baobaowd.util.GobalConstants;
import dianyun.zqcheap.R;

public class DownloadAppHelper
{

	private static NotificationManager nm;
	private Notification notification;
	private boolean cancelUpdate = false;
	private MyHandler myHandler;
	private static ExecutorService executorService = Executors
			.newFixedThreadPool(5); // 固定五个线程来执行任务
	public static Map<Integer, Integer> download = new HashMap<Integer, Integer>();
	public static List<String> downloadAPkList = new ArrayList<String>();
	public Context context;

	/*
	 * @Override public IBinder onBind(Intent intent) { return null; }
	 * 
	 * @Override public void onStart(Intent intent, int startId) {
	 * super.onStart(intent, startId); }
	 * 
	 * @Override public void onCreate() { super.onCreate();
	 * System.out.println("DownloadService========="); nm =
	 * (NotificationManager) getSystemService(NOTIFICATION_SERVICE); myHandler =
	 * new MyHandler(Looper.myLooper(), DownloadAppHelper.this); context = this;
	 * }
	 * 
	 * @Override public void onDestroy() { super.onDestroy(); }
	 */
	public DownloadAppHelper(Context context)
	{
		this.context = context;
		if (nm == null)
			nm = (NotificationManager) context
					.getSystemService(Context.NOTIFICATION_SERVICE);
		myHandler = new MyHandler(context);
	}

	public void downNewFile(final String url, final int notificationId,
			final String name, final String fileLocalPath, String appId)
	{
		if (download.containsKey(notificationId))
			return;
		notification = new Notification();
		notification.icon = android.R.drawable.stat_sys_download;
		// notification.icon=android.R.drawable.stat_sys_download_done;
		notification.tickerText = name
				+ context.getString(R.string.download_begin);
		notification.when = System.currentTimeMillis();
		notification.defaults = Notification.DEFAULT_LIGHTS;
		// 显示在“正在进行中”
		notification.flags = Notification.FLAG_NO_CLEAR
				| Notification.FLAG_ONGOING_EVENT;
		PendingIntent contentIntent = PendingIntent.getActivity(context,
				notificationId, new Intent(), 0);
		notification.setLatestEventInfo(context, name, "0%", contentIntent);
		download.put(notificationId, 0);
		// 将下载任务添加到任务栏中
		nm.notify(notificationId, notification);
		// 启动线程开始执行下载任务
		downloadAPkList.add(appId);
		downFile(url, notificationId, name, fileLocalPath, appId);
	}

	// 下载更新文件
	private void downFile(final String url, final int notificationId,
			final String name, final String fileLocalPath, final String appId)
	{
		executorService.execute(new Runnable()
		{

			@Override
			public void run()
			{
				// TODO Auto-generated method stub
				File tempFile = null;
				try
				{
					HttpClient client = new DefaultHttpClient();
					// params[0]代表连接的url
					HttpGet get = new HttpGet(url);
					HttpResponse response = client.execute(get);
					HttpEntity entity = response.getEntity();
					long length = entity.getContentLength();
					InputStream is = entity.getContent();
					if (is != null)
					{
						// File rootFile = new
						// File(Environment.getExternalStorageDirectory(),"/zhtrade");
						// if (!rootFile.exists() && !rootFile.isDirectory())
						// rootFile.mkdir();
						// tempFile = new
						// File(Environment.getExternalStorageDirectory(),"/zhtrade/"+
						// url.substring(url.lastIndexOf("/"),url.indexOf("?"))+"_"+notificationId+".apk");
						tempFile = new File(fileLocalPath);
						if (tempFile.exists())
							tempFile.delete();
						tempFile.createNewFile();
						// 已读出流作为参数创建一个带有缓冲的输出流
						BufferedInputStream bis = new BufferedInputStream(is);
						// 创建一个新的写入流，讲读取到的图像数据写入到文件中
						FileOutputStream fos = new FileOutputStream(tempFile);
						// 已写入流作为参数创建一个带有缓冲的写入流
						BufferedOutputStream bos = new BufferedOutputStream(fos);
						int read;
						long count = 0;
						int precent = 0;
						byte[] buffer = new byte[1024];
						while ((read = bis.read(buffer)) != -1 && !cancelUpdate)
						{
							bos.write(buffer, 0, read);
							count += read;
							precent = (int) (((double) count / length) * 100);
							// 每下载完成1%就通知任务栏进行修改下载进度
							if (precent - download.get(notificationId) >= 1)
							{
								download.put(notificationId, precent);
								Message message = myHandler.obtainMessage(
										DOWNLOADING, precent);
								Bundle bundle = new Bundle();
								bundle.putString("name", name);
								message.setData(bundle);
								message.arg1 = notificationId;
								myHandler.sendMessage(message);
							}
						}
						bos.flush();
						bos.close();
						fos.flush();
						fos.close();
						is.close();
						bis.close();
					}
					if (!cancelUpdate)
					{
						Message message = myHandler.obtainMessage(
								DOWNLOADSUCCESS, tempFile);
						message.arg1 = notificationId;
						Bundle bundle = new Bundle();
						bundle.putString("name", name);
						bundle.putString("appId", appId);
						message.setData(bundle);
						myHandler.sendMessage(message);
					}
					else
					{
						tempFile.delete();
					}
				}
				catch (ClientProtocolException e)
				{
					if (tempFile.exists())
						tempFile.delete();
					Message message = myHandler.obtainMessage(
							DOWNLOADFAILED,
							name
									+ context
											.getString(R.string.file_download_failed));
					message.arg1 = notificationId;
					Bundle bundle = new Bundle();
					bundle.putString("name", name);
					bundle.putString("appId", appId);
					message.setData(bundle);
					myHandler.sendMessage(message);
				}
				catch (IOException e)
				{
					if (tempFile.exists())
						tempFile.delete();
					Message message = myHandler.obtainMessage(
							DOWNLOADFAILED,
							name
									+ context
											.getString(R.string.file_download_failed));
					message.arg1 = notificationId;
					Bundle bundle = new Bundle();
					bundle.putString("name", name);
					bundle.putString("appId", appId);
					message.setData(bundle);
					myHandler.sendMessage(message);
				}
				catch (Exception e)
				{
					if (tempFile.exists())
						tempFile.delete();
					Message message = myHandler.obtainMessage(
							DOWNLOADFAILED,
							name
									+ context
											.getString(R.string.file_download_failed));
					message.arg1 = notificationId;
					Bundle bundle = new Bundle();
					bundle.putString("name", name);
					bundle.putString("appId", appId);
					message.setData(bundle);
					myHandler.sendMessage(message);
				}
			}
		});
	}
	public static final int BEGIN = 0;
	public static final int DOWNLOADSUCCESS = 2;
	public static final int DOWNLOADING = 3;
	public static final int DOWNLOADFAILED = 4;

	/* 事件处理类 */
	class MyHandler extends Handler
	{

		private Context context;

		public MyHandler(Context c)
		{
			this.context = c;
		}

		@Override
		public void handleMessage(Message msg)
		{
			PendingIntent contentIntent = null;
			super.handleMessage(msg);
			if (msg != null)
			{
				switch (msg.what)
				{
				case BEGIN:
					Toast.makeText(context, msg.obj.toString(),
							Toast.LENGTH_SHORT).show();
					// download.remove(msg.arg1);
					break;
				case DOWNLOADSUCCESS:
					contentIntent = PendingIntent.getActivity(context,
							msg.arg1, new Intent(), 0);
					notification
							.setLatestEventInfo(
									context,
									msg.getData().getString("name")
											+ context
													.getString(R.string.file_download_success),
									"100%", contentIntent);
					nm.notify(msg.arg1, notification);
					// 下载完成后清除所有下载信息，执行安装提示
					// download.remove(msg.arg1);
					nm.cancel(msg.arg1);
					AppHelper.instanll((File) msg.obj, context);
					String appId = msg.getData().getString("appId");
					BroadCastHelper.sendRefreshAppBroadcast(context, appId,
							GobalConstants.AllStatusYesOrNo.NO);
					break;
				case DOWNLOADING:
					contentIntent = PendingIntent.getActivity(context,
							msg.arg1, new Intent(), 0);
					notification.setLatestEventInfo(context, msg.getData()
							.getString("name"), download.get(msg.arg1) + "%",
							contentIntent);
					nm.notify(msg.arg1, notification);
					break;
				case DOWNLOADFAILED:
					Toast.makeText(context, msg.obj.toString(),
							Toast.LENGTH_SHORT).show();
					// 下载失败后清除所有下载信息，执行安装提示
					download.remove(msg.arg1);
					String appId1 = msg.getData().getString("appId");
					downloadAPkList.remove(appId1);
					nm.cancel(msg.arg1);
					break;
				}
			}
		}
	}
}
