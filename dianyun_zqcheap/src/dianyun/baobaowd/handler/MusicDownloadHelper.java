package dianyun.baobaowd.handler;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import dianyun.baobaowd.help.FileHelper;

public class MusicDownloadHelper
{

	private boolean stopDown;
	public static final String MP3 = ".mp3";

	public interface DownloadProgress
	{

		void setProgress(int progress, boolean isFinish);
	}

	public static String getTempMusicLocalPath(String musicId)
	{
		// String musicName = music.getFileUrl().substring(
		// music.getFileUrl().lastIndexOf("/")+1);
		return FileHelper.getTempMusicPath() + musicId + MP3;
	}

	public static String getMusicLocalPath(String musicId)
	{
		// String musicName = music.getFileUrl().substring(
		// music.getFileUrl().lastIndexOf("/")+1);
		return FileHelper.getMusicPath() + musicId + MP3;
	}

	public void setStopDown(boolean stopDown)
	{
		this.stopDown = stopDown;
	}

	public void download(final String url, final String path,
			final DownloadProgress downloadProgress)
	{
		new Thread()
		{

			@Override
			public void run()
			{
				File tempFile = null;
				BufferedInputStream bis = null;
				FileOutputStream fos = null;
				BufferedOutputStream bos = null;
				try
				{
					HttpClient client = new DefaultHttpClient();
					// params[0]代表连接的url
					HttpGet get = new HttpGet(url);
					HttpResponse response = client.execute(get);
					HttpEntity entity = response.getEntity();
					long length = entity.getContentLength();
					InputStream is = entity.getContent();
					if (is != null)
					{
						tempFile = new File(path);
						if (tempFile.exists())
							tempFile.delete();
						tempFile.createNewFile();
						// 已读出流作为参数创建一个带有缓冲的输出流
						bis = new BufferedInputStream(is);
						// 创建一个新的写入流，讲读取到的图像数据写入到文件中
						fos = new FileOutputStream(tempFile);
						// 已写入流作为参数创建一个带有缓冲的写入流
						bos = new BufferedOutputStream(fos);
						int read;
						long count = 0;
						int precent = 0;
						byte[] buffer = new byte[1024];
						while ((read = bis.read(buffer)) != -1)
						{
							if (stopDown)
								return;
							bos.write(buffer, 0, read);
							count += read;
							precent = (int) (count * 100 / length);
							downloadProgress.setProgress(precent, false);
						}
						downloadProgress.setProgress(100, true);
						bos.flush();
						fos.flush();
						is.close();
					}
				}
				catch (Exception e)
				{
					e.printStackTrace();
					if (tempFile != null && tempFile.exists())
						tempFile.delete();
					downloadProgress.setProgress(-1, true);
				}
				finally
				{
					try
					{
						bos.close();
						fos.close();
						bis.close();
					}
					catch (Exception e)
					{
					}
				}
			};
		}.start();
	}
}
