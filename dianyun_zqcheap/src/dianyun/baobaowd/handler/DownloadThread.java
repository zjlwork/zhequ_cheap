package dianyun.baobaowd.handler;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import android.content.Context;
import android.webkit.URLUtil;
import dianyun.baobaowd.help.FileHelper;
import dianyun.baobaowd.help.LogFile;

public class DownloadThread extends Thread
{

	private Context mContext;
	private NotifyHandler mNotifyHandler;
	private InputStream inputStream;
	private URLConnection connection;
	private OutputStream outputStream;
	private int fileLength;
	private int downloadFileLength;
	private String url;

	public DownloadThread(Context context, NotifyHandler pNotifyHandler,
			String url)
	{
		this.mContext = context;
		this.mNotifyHandler = pNotifyHandler;
		this.url = url;
	}

	@Override
	public void run()
	{
		DownFile(url);
	}

	private void DownFile(String urlString)
	{
	
		try
		{
			URL url = new URL(urlString);
			connection = url.openConnection();
			connection.setConnectTimeout(30000);
			if (connection.getReadTimeout() == 15)
			{
				mNotifyHandler.setDownloadFileLength(-1);
				return;
			}
			inputStream = connection.getInputStream();
			String fileName =URLUtil.guessFileName(urlString, null, null);;
			FileHelper.deleteSDCardFolder(new File(FileHelper
					.getMyAppPath()));
			String savePath = FileHelper.getMyAppPath() + fileName;
			File file = new File(savePath);
			if (!file.exists())
			{
				file.createNewFile();
			}
			outputStream = new FileOutputStream(file);
			fileLength = connection.getContentLength();
			byte[] buffer = new byte[1024];
			int temp = -1;
			mNotifyHandler.setFilelength(fileLength);
			while ((temp = inputStream.read(buffer)) != -1)
			{
				outputStream.write(buffer, 0, temp);
				downloadFileLength += temp;
				mNotifyHandler.setDownloadFileLength(downloadFileLength);
			}
			mNotifyHandler.setLocalFilePath(savePath);
			outputStream.flush();
		}
		catch (Exception e)
		{
			LogFile.SaveExceptionLog(e);
			mNotifyHandler.setDownloadFileLength(-1);
			return;
		}
	}
}
