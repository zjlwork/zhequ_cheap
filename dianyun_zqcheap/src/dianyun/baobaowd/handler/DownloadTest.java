package dianyun.baobaowd.handler;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import dianyun.zqcheap.R;

public class DownloadTest extends Activity implements OnClickListener
{

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.down_file);
		Intent intent = new Intent(this, DownloadService.class);
		startService(intent);
		bindListener();
	}

	void bindListener()
	{
		findViewById(R.id.btn_load_btn_one).setOnClickListener(this);
		findViewById(R.id.btn_load_btn_two).setOnClickListener(this);
		findViewById(R.id.btn_load_btn_three).setOnClickListener(this);
	}

	@Override
	public void onClick(View v)
	{
		switch (v.getId())
		{
		// case R.id.btn_load_btn_one:
		// DownloadService.downNewFile("http://m.appchina.com/market/e/882602/0/16/44DFFDF6E89D0CC2F5CB41CE041E9BB7/packagename.apk?refererPage=m.cherry.soft_list",
		// 351, "百度知道");
		// break;
		// case R.id.btn_load_btn_two:
		// DownloadService.downNewFile("http://m.appchina.com/market/e/886657/feature/1/360F5D5A76A3E0E397451CE9EA503DE6/com.sogou.novel?refererPage=m.cherry.soft_main",
		// 1071, "搜狗小说");
		// break;
		// case R.id.btn_load_btn_three:
		// DownloadService.downNewFile("http://m.appchina.com/market/e/885495/feature/2/360F5D5A76A3E0E397451CE9EA503DE6/com.king2.yyh?refererPage=m.cherry.soft_main",
		// 1072, "君王2(新服开启)");
		// break;
		default:
			break;
		}
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event)
	{
		// TODO Auto-generated method stub
		if (keyCode == KeyEvent.KEYCODE_BACK)
		{
			System.exit(0);
			return false;
		}
		return super.onKeyDown(keyCode, event);
	}
}
