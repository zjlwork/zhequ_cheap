package dianyun.baobaowd.handler;

import java.io.File;
import java.math.BigDecimal;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.RemoteViews;
import dianyun.baobaowd.db.LightDBHelper;
import dianyun.zqcheap.R;

public class NotifyHandler extends Handler
{

	private int downloadFileLength;
	private int filelength;
	private Context mContext;
	// private RemoteViews mContentView;
	Notification notification;
	NotificationManager manager;
	private String localFilePath;
	private static final int PROGRESSNotificationID = 1000;

	public String getLocalFilePath()
	{
		return localFilePath;
	}

	public void setLocalFilePath(String localFilePath)
	{
		this.localFilePath = localFilePath;
	}

	public NotifyHandler(Context pContext)
	{
		this.mContext = pContext;
		notification = new Notification(R.drawable.logo,
				mContext.getString(R.string.download_file),
				System.currentTimeMillis());
		manager = (NotificationManager) mContext
				.getSystemService(Context.NOTIFICATION_SERVICE);
		notification.flags = Notification.FLAG_NO_CLEAR;
		notification.contentView = new RemoteViews(mContext
				.getApplicationContext().getPackageName(),
				R.layout.notifyprogresslayout);
		notification.contentIntent = PendingIntent.getActivity(mContext, 0,
				new Intent(), PendingIntent.FLAG_UPDATE_CURRENT);
	}

	public int getDownloadFileLength()
	{
		return downloadFileLength;
	}

	public void setDownloadFileLength(int downloadFileLength)
	{
		this.downloadFileLength = downloadFileLength;
	}

	public int getFilelength()
	{
		return filelength;
	}

	public void setFilelength(int filelength)
	{
		this.filelength = filelength;
	}

	@Override
	public void handleMessage(Message msg)
	{// 定义一个Handler，用于处理下载线程与UI间通讯
		if (!Thread.currentThread().isInterrupted())
		{
			switch (msg.what)
			{
			case 0:
				notification.contentView.setProgressBar(R.id.pb, filelength,
						downloadFileLength, false);
				notification.contentView.setTextViewText(R.id.progresscounttv,
						0 + "%");
				notification.contentView.setTextViewText(R.id.appname,
						mContext.getString(R.string.app_name));
				manager.notify(PROGRESSNotificationID, notification);
				break;
			case 1:
				notification.contentView.setProgressBar(R.id.pb, filelength,
						downloadFileLength, false);
				notification.contentView.setInt(R.id.pb, "setVisibility",
						View.VISIBLE);
				int count = downloadFileLength * 100 / filelength;
				notification.contentView.setTextViewText(R.id.progresscounttv,
						count + "%");
				notification.contentView.setTextViewText(R.id.appname,
						mContext.getString(R.string.app_name) + "("
								+ getSize(filelength) + ")");
				manager.notify(PROGRESSNotificationID, notification);
				break;
			case 2:
				notification.contentView.setInt(R.id.pb, "setVisibility",
						View.GONE);
				notification.contentView.setTextViewText(R.id.progresscounttv,
						mContext.getString(R.string.file_download_success));
				// notification.flags =
				// Notification.FLAG_NO_CLEAR|Notification.FLAG_AUTO_CANCEL;
				manager.notify(PROGRESSNotificationID, notification);
				manager.cancel(PROGRESSNotificationID);
				Intent intent = new Intent(Intent.ACTION_VIEW);
				intent.setDataAndType(Uri.fromFile(new File(localFilePath)),
						"application/vnd.android.package-archive");
				intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				mContext.startActivity(intent);
				LightDBHelper.setIsDownloadApk(mContext, false);
				break;
			case -1:
				notification.flags = Notification.FLAG_AUTO_CANCEL;
				notification.contentView.setInt(R.id.pb, "setVisibility",
						View.GONE);
				notification.contentView.setTextViewText(R.id.progresscounttv,
						mContext.getString(R.string.file_download_failed));
				manager.notify(PROGRESSNotificationID, notification);
				LightDBHelper.setIsDownloadApk(mContext, false);
				LightDBHelper.setCheckVersionTime("",mContext);
				break;
			}
		}
		super.handleMessage(msg);
	}

	private String getSize(long fileLength)
	{
		if (fileLength >= (1024 * 1024))
		{
			double f = fileLength / (1024 * 1024.0);
			BigDecimal b = new BigDecimal(f);
			double f1 = b.setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
			return f1 + "M";
		}
		if (fileLength < (1024 * 1024) && fileLength >= 1024)
		{
			double f = fileLength / 1024.0;
			BigDecimal b = new BigDecimal(f);
			double f1 = b.setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
			return f1 + "K";
		}
		if (fileLength < 1024)
		{
			return fileLength + "B";
		}
		return "";
	}
}
