package dianyun.baobaowd.sinaweibo;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.widget.Toast;

import com.sina.weibo.sdk.auth.AuthInfo;
import com.sina.weibo.sdk.auth.Oauth2AccessToken;
import com.sina.weibo.sdk.auth.WeiboAuthListener;
import com.sina.weibo.sdk.auth.sso.SsoHandler;
import com.sina.weibo.sdk.exception.WeiboException;
import com.sina.weibo.sdk.net.RequestListener;
import com.sina.weibo.sdk.openapi.StatusesAPI;
import com.sina.weibo.sdk.openapi.UsersAPI;

import dianyun.baobaowd.util.ThirdPartHelper;
import dianyun.zqcheap.R;

public class SinaWeiboHelper {

	/** 用于获取微博信息流等操作的API */
	// private mStatusesAPI;

	public static void share(final Context context, final String title,
			final String url) {
		if (title == null || url == null)
			return;
		// 获取当前已保存过的 Token
		Oauth2AccessToken mAccessToken = AccessTokenKeeper
				.readAccessToken(context);
		// 对statusAPI实例化
		StatusesAPI mStatusesAPI = new StatusesAPI(context,
				ThirdPartHelper.SINA_APP_KEY, mAccessToken);
		Drawable drawable = context.getResources().getDrawable(
				R.drawable.sharelogo);
		Bitmap bitmap = ((BitmapDrawable) drawable).getBitmap();
		mStatusesAPI.upload(title + url, bitmap, null, null,
				new RequestListener() {

					@Override
					public void onWeiboException(WeiboException arg0) {
						Toast.makeText(context, "分享失败", Toast.LENGTH_SHORT)
								.show();
					}

					@Override
					public void onComplete(String arg0) {
						Toast.makeText(context, "分享成功，去你的新浪微博看看吧！",
								Toast.LENGTH_SHORT).show();
					}
				});

	}

	public static SsoHandler login(final Activity context,final SinaLoginCallback callback) {

		AuthInfo mAuthInfo = new AuthInfo(context,
				ThirdPartHelper.SINA_APP_KEY,
				ThirdPartHelper.SINA_REDIRECT_URL, ThirdPartHelper.SCOPE);
		SsoHandler mSsoHandler = new SsoHandler(context, mAuthInfo);
		mSsoHandler.authorize(new WeiboAuthListener() {

			@Override
			public void onWeiboException(WeiboException arg0) {
				callback.result(false);
			}

			@Override
			public void onComplete(Bundle values) {
				Oauth2AccessToken mAccessToken = Oauth2AccessToken
						.parseAccessToken(values);
				// 从这里获取用户输入的 电话号码信息
//				String phoneNum = mAccessToken.getPhoneNum();
				if (mAccessToken.isSessionValid()) {
					// 显示 Token
					// updateTokenView(false);

					// 保存 Token 到 SharedPreferences
					AccessTokenKeeper.writeAccessToken(context, mAccessToken);
//					Toast.makeText(context, "weibosdk_demo_toast_auth_success",
//							Toast.LENGTH_SHORT).show();

					// 获取用户信息接口
					UsersAPI mUsersAPI = new UsersAPI(context,
							ThirdPartHelper.SINA_APP_KEY, mAccessToken);
					long uid = Long.parseLong(mAccessToken.getUid());
					mUsersAPI.show(uid, new RequestListener() {

						@Override
						public void onWeiboException(WeiboException arg0) {
							callback.result(false);
						}

						@Override
						public void onComplete(String response) {
							if (!TextUtils.isEmpty(response)) {
								AccessTokenKeeper.writeUserJson(context,
										response);
								System.out.println("mListener=====" + response);
								callback.result(true);
							}
						}
					});
				} else {
					callback.result(false);
				}
			}

			@Override
			public void onCancel() {

			}
		});

		return mSsoHandler;
	}
	
	
	
	
//	public interface SinaLoginCallback(){
//		
//	}
	
	
	
	public interface SinaLoginCallback
	{
		void result(boolean success);
	}
	

/*	
	SsoHandler    mSsoHandler ;
	  *//** 用户信息接口 *//*
    private UsersAPI mUsersAPI;
	
    *//** 封装了 "access_token"，"expires_in"，"refresh_token"，并提供了他们的管理功能  *//*
    private Oauth2AccessToken mAccessToken;
	
    *//**
     * 微博认证授权回调类。
     * 1. SSO 授权时，需要在 {@link #onActivityResult} 中调用 {@link SsoHandler#authorizeCallBack} 后，
     *    该回调才会被执行。
     * 2. 非 SSO 授权时，当授权结束后，该回调就会被执行。
     * 当授权成功后，请保存该 access_token、expires_in、uid 等信息到 SharedPreferences 中。
     *//*
    class AuthListener implements WeiboAuthListener {
        
        @Override
        public void onComplete(Bundle values) {
            // 从 Bundle 中解析 Token
            mAccessToken = Oauth2AccessToken.parseAccessToken(values);
            //从这里获取用户输入的 电话号码信息 
            String  phoneNum =  mAccessToken.getPhoneNum();
            if (mAccessToken.isSessionValid()) {
                // 显示 Token
//                updateTokenView(false);
                
                // 保存 Token 到 SharedPreferences  
                AccessTokenKeeper.writeAccessToken(LoginRegisterActivity.this, mAccessToken);
                Toast.makeText(LoginRegisterActivity.this, 
                        "weibosdk_demo_toast_auth_success", Toast.LENGTH_SHORT).show();
                
                
                // 获取用户信息接口
                mUsersAPI = new UsersAPI(LoginRegisterActivity.this, ThirdPartHelper.SINA_APP_KEY, mAccessToken);
                long uid = Long.parseLong(mAccessToken.getUid());
                mUsersAPI.show(uid, mListener);
            } else {
                // 以下几种情况，您会收到 Code：
                // 1. 当您未在平台上注册的应用程序的包名与签名时；
                // 2. 当您注册的应用程序包名与签名不正确时；
                // 3. 当您在平台上注册的包名和签名与您当前测试的应用的包名和签名不匹配时。
                String code = values.getString("code");
                String message = getString(R.string.weibosdk_demo_toast_auth_failed)"weibosdk_demo_toast_auth_failed";
                if (!TextUtils.isEmpty(code)) {
                    message = message + "\nObtained the code: " + code;
                }
                Toast.makeText(LoginRegisterActivity.this, message, Toast.LENGTH_LONG).show();
                mHandler.sendEmptyMessage(SINAWEIBOAUTHFAILED);
            }
        }

        @Override
        public void onCancel() {
//            Toast.makeText(LoginRegisterActivity.this, 
//                  "weibosdk_demo_toast_auth_canceled", Toast.LENGTH_LONG).show();
//            mHandler.sendEmptyMessage(SINAWEIBOAUTHFAILED);
        }

        @Override
        public void onWeiboException(WeiboException e) {
//            Toast.makeText(LoginRegisterActivity.this, 
//                    "Auth exception : " + e.getMessage(), Toast.LENGTH_LONG).show();
            mHandler.sendEmptyMessage(SINAWEIBOAUTHFAILED);
        }
    }
	
	
	
    *//**
     * 微博 OpenAPI 回调接口。
     *//*
    private RequestListener mListener = new RequestListener() {
        @Override
        public void onComplete(String response) {
            if (!TextUtils.isEmpty(response)) {
//                LogUtil.i(TAG, response);
                // 调用 User#parse 将JSON串解析成User对象
//                User user = User.parse(response);
//                if (user != null) {
//                    Toast.makeText(LoginRegisterActivity.this, 
//                            "获取User信息成功，用户昵称：" + user.screen_name, 
//                            Toast.LENGTH_LONG).show();
//                } else {
//                    Toast.makeText(LoginRegisterActivity.this, response, Toast.LENGTH_LONG).show();
//                }
            	AccessTokenKeeper.writeUserJson(LoginRegisterActivity.this, response);
            	System.out.println("mListener====="+response);
            	mHandler.sendEmptyMessage(SINAWEIBOAUTHSUCCESS);
            }
        }

        @Override
        public void onWeiboException(WeiboException e) {
//            LogUtil.e(TAG, e.getMessage());
            ErrorInfo info = ErrorInfo.parse(e.getMessage());
//            Toast.makeText(LoginRegisterActivity.this, info.toString(), Toast.LENGTH_LONG).show();
            mHandler.sendEmptyMessage(SINAWEIBOAUTHFAILED);
        }
    };*/
	
	
}
