package dianyun.baobaowd.xiaomipush;

import java.util.Date;
import java.util.List;

import android.content.Context;
import android.util.Log;

import com.xiaomi.mipush.sdk.MiPushClient;
import com.xiaomi.mipush.sdk.MiPushCommandMessage;
import com.xiaomi.mipush.sdk.MiPushMessage;
import com.xiaomi.mipush.sdk.PushMessageReceiver;

import dianyun.baobaowd.data.XiaoMiPushData;
import dianyun.baobaowd.gson.GsonHelper;
import dianyun.baobaowd.help.LogFile;
import dianyun.baobaowd.util.Base64;
import dianyun.baobaowd.util.DateHelper;
import dianyun.baobaowd.util.GobalConstants;
import dianyun.baobaowd.util.ZipUtil;
import dianyun.zqcheap.service.MyMessage;
import dianyun.zqcheap.service.ReceiveHelper;

/**
 * 1、PushMessageReceiver是个抽象类，该类继承了BroadcastReceiver。
 * 2、需要将自定义的DemoMessageReceiver注册在AndroidManifest.xml文件中 <receiver
 * android:exported="true"
 * android:name="com.xiaomi.mipushdemo.DemoMessageReceiver"> <intent-filter>
 * <action android:name="com.xiaomi.mipush.RECEIVE_MESSAGE" /> </intent-filter>
 * <intent-filter> <action android:name="com.xiaomi.mipush.ERROR" />
 * </intent-filter> </receiver>
 * 3、DemoMessageReceiver的onCommandResult方法用来接收客户端向服务器发送命令后的响应结果
 * 4、DemoMessageReceiver的onReceiveMessage方法用来接收服务器向客户端发送的消息
 * 5、onReceiveMessage和onCommandResult方法运行在非UI线程中
 *
 * @author wangkuiwei
 */
public class DemoMessageReceiver extends PushMessageReceiver {

    public static final String TAG = "dianyun.baobaowd";
    private String mRegId;
    private long mResultCode = -1;
    private String mReason;
    private String mCommand;
    private String mMessage;
    private String mTopic;
    private String mAlias;
    private String mStartTime;
    private String mEndTime;

    // static HashMap<String,Integer> messageReceivedList = new HashMap<String,
    // Integer>();//缓存收到的消息ID，防止重复通知
    @Override
    public void onReceiveMessage(Context context, MiPushMessage message) {
        Log.v(TAG, "onReceiveMessage is called. " + message.toString());

        String messageTime = null;
        String body = message.getContent();
        System.out.println("msg.toXML()==" + message.toString());
        try {
            String bodyXml = ZipUtil.uncompress(Base64.decode(body));
            LogFile.SaveLog(bodyXml);
            MyMessage myMessage = GsonHelper
                    .gsonToObj(bodyXml, MyMessage.class);
            messageTime = DateHelper.getTextByDate(
                    new Date(myMessage.getTimestamp()),
                    DateHelper.YYYY_MM_DD_HH_MM);
           
            System.out.println("bodyXml==" + bodyXml);
            LogFile.SaveLog("bodyXml==" + bodyXml);
            if (myMessage.getMessageType() == GobalConstants.MessageType.SYSTEMMSG_NORMAL ||
                    myMessage.getMessageType() == GobalConstants.MessageType.SYSTEMMSG_FANLI
                    || myMessage.getMessageType() == GobalConstants.MessageType.SYSTEMMSG_TIXIAN
                    || myMessage.getMessageType() == GobalConstants.MessageType.SYSTEMMSG_EXCHANGE || myMessage.getMessageType() == GobalConstants.MessageType.SYSTEMMSG_GOODS
                    || myMessage.getMessageType() == GobalConstants.MessageType.SYSTEMMSG_QIANDAO
                    || myMessage.getMessageType() == GobalConstants.MessageType.SYSTEMMSG_XIADAN
                    || myMessage.getMessageType() == GobalConstants.MessageType.SYSTEMMSG_XIAJIA
                    || myMessage.getMessageType() == GobalConstants.MessageType.SYSTEMMSG_ACTIVITY) {
                ReceiveHelper.receiveNewestToastType(context,
                        myMessage.getContentObject().toString(),
                        myMessage.getMessageType());
            }

        } catch (Exception e) {
            LogFile.SaveExceptionLog(e);
        }
    }

    @Override
    public void onCommandResult(Context context, MiPushCommandMessage message) {
        Log.v(TAG, "onCommandResult is called. " + message.toString());
        String command = message.getCommand();
        List<String> arguments = message.getCommandArguments();
        if (arguments != null) {
            if (MiPushClient.COMMAND_REGISTER.equals(command)
                    && arguments.size() == 1) {
                mRegId = arguments.get(0);
            } else if ((MiPushClient.COMMAND_SET_ALIAS.equals(command))
                    && arguments.size() == 1) {
                mAlias = arguments.get(0);
                if (mAlias != null) {
                    XiaoMiPushData lXiaoMiPushData = RegisterPushHelper
                            .getPushDataByName(context, mAlias);
                    if (lXiaoMiPushData != null)
                        lXiaoMiPushData
                                .setStatus(GobalConstants.PushDataStatus.SUCCESS);
                    RegisterPushHelper.updateXiaoMiPushData(context,
                            lXiaoMiPushData);
                    System.out.println("COMMAND_SET_ALIAS==true" + mAlias);
                }
            } else if (MiPushClient.COMMAND_UNSET_ALIAS.equals(command)) {
                mAlias = arguments.get(0);
                if (mAlias != null) {
                    XiaoMiPushData lXiaoMiPushData = RegisterPushHelper
                            .getPushDataByName(context, mAlias);
                    if (lXiaoMiPushData != null)
                        lXiaoMiPushData
                                .setUnStatus(GobalConstants.PushDataStatus.SUCCESS);
                    RegisterPushHelper.updateXiaoMiPushData(context,
                            lXiaoMiPushData);
                    List<XiaoMiPushData> XiaoMiPushDataList = RegisterPushHelper
                            .getUnRegisterFailedPushDataList(context);
                    if (XiaoMiPushDataList == null
                            || XiaoMiPushDataList.size() == 0) {
                        RegisterPushHelper.deleteAll(context);
                    }
                    System.out.println("COMMAND_UNSET_ALIAS==true" + mAlias);
                }
            } else if ((MiPushClient.COMMAND_SUBSCRIBE_TOPIC.equals(command))
                    && arguments.size() == 1) {
                mTopic = arguments.get(0);
                if (mTopic != null) {
                    XiaoMiPushData lXiaoMiPushData = RegisterPushHelper
                            .getPushDataByName(context, mTopic);
                    if (lXiaoMiPushData != null)
                        lXiaoMiPushData
                                .setStatus(GobalConstants.PushDataStatus.SUCCESS);
                    RegisterPushHelper.updateXiaoMiPushData(context,
                            lXiaoMiPushData);
                    System.out
                            .println("COMMAND_SUBSCRIBE_TOPIC==true" + mTopic);
                }
            } else if (MiPushClient.COMMAND_UNSUBSCRIBE_TOPIC.equals(command)) {
                mTopic = arguments.get(0);
                if (mTopic != null) {
                    XiaoMiPushData lXiaoMiPushData = RegisterPushHelper
                            .getPushDataByName(context, mTopic);
                    if (lXiaoMiPushData != null)
                        lXiaoMiPushData
                                .setUnStatus(GobalConstants.PushDataStatus.SUCCESS);
                    RegisterPushHelper.updateXiaoMiPushData(context,
                            lXiaoMiPushData);
                    List<XiaoMiPushData> XiaoMiPushDataList = RegisterPushHelper
                            .getUnRegisterFailedPushDataList(context);
                    if (XiaoMiPushDataList == null
                            || XiaoMiPushDataList.size() == 0) {
                        RegisterPushHelper.deleteAll(context);
                    }
                    System.out.println("COMMAND_UNSUBSCRIBE_TOPIC==true"
                            + mTopic);
                }
            } else if (MiPushClient.COMMAND_SET_ACCEPT_TIME.equals(command)
                    && arguments.size() == 2) {
                mStartTime = arguments.get(0);
                mEndTime = arguments.get(1);
            }
        }
    }
}
