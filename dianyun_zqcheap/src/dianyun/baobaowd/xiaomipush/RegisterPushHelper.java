package dianyun.baobaowd.xiaomipush;

import java.util.ArrayList;
import java.util.List;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningAppProcessInfo;
import android.content.Context;
import android.os.Process;
import com.xiaomi.mipush.sdk.Constants;
import com.xiaomi.mipush.sdk.MiPushClient;
import dianyun.baobaowd.data.User;
import dianyun.baobaowd.data.XiaoMiPushData;
import dianyun.baobaowd.db.TableConstants;
import dianyun.baobaowd.db.XiaoMiPushDataDBHelper;
import dianyun.baobaowd.help.LogFile;
import dianyun.baobaowd.util.GobalConstants;
import dianyun.baobaowd.util.ThirdPartHelper;
import dianyun.baobaowd.util.UserHelper;

public class RegisterPushHelper
{

	public static void registerPush(Context context)
	{
		Constants.useOfficial(); // 正式服
//		 Constants.useSandbox();//测试服
		if (shouldInit(context))
		{
			MiPushClient.registerPush(context, ThirdPartHelper.XIAOMIAPP_ID, ThirdPartHelper.XIAOMIAPP_KEY);
		}
	}

	public static void unregisterPush(Context context)
	{
		MiPushClient.unregisterPush(context);
	}

	public static void pausePush(Context context)
	{
		MiPushClient.pausePush(context, null);
	}

	public static void resumePush(Context context)
	{
		MiPushClient.resumePush(context, null);
	}

	private static boolean shouldInit(Context context)
	{
		ActivityManager am = ((ActivityManager) context
				.getSystemService(Context.ACTIVITY_SERVICE));
		List<RunningAppProcessInfo> processInfos = am.getRunningAppProcesses();
		String mainProcessName = context.getPackageName();
		int myPid = Process.myPid();
		for (RunningAppProcessInfo info : processInfos)
		{
			if (info.pid == myPid && mainProcessName.equals(info.processName))
			{
				return true;
			}
		}
		return false;
	}

	public static void logout(Context context)
	{
		changeXiaoMiPushDataListUnRegister(context);
		unSetAliasAndTopics(context);
	}

	private static void setAliasAndTopics(Context context,
			List<XiaoMiPushData> xiaoMiPushDataList)
	{
		if (xiaoMiPushDataList == null)
			return;
		for (int i = 0; i < xiaoMiPushDataList.size(); i++)
		{
			XiaoMiPushData xiaoMiPushData = xiaoMiPushDataList.get(i);
			if (xiaoMiPushData.getStatus() != GobalConstants.PushDataStatus.SUCCESS)
			{
				if (xiaoMiPushData.getType() == GobalConstants.XiaoMiPushDataType.ALIAS)
				{
					MiPushClient.setAlias(context, xiaoMiPushData.getName(),
							null);
					System.out.println("aliasStatus----------------"
							+ xiaoMiPushData.getName());
				}
				else
				{
					MiPushClient.subscribe(context, xiaoMiPushData.getName(),
							null);
					System.out.println("topicStatus----------------"
							+ xiaoMiPushData.getName());
				}
			}
		}
	}

	public static void unSetAliasAndTopics(Context context)
	{
		List<XiaoMiPushData> xiaoMiPushDataList = getUnRegisterFailedPushDataList(context);
		if (xiaoMiPushDataList == null)
			return;
		for (int i = 0; i < xiaoMiPushDataList.size(); i++)
		{
			XiaoMiPushData xiaoMiPushData = xiaoMiPushDataList.get(i);
			if (xiaoMiPushData.getUnStatus() != GobalConstants.PushDataStatus.SUCCESS)
			{
				if (xiaoMiPushData.getType() == GobalConstants.XiaoMiPushDataType.ALIAS)
				{
					MiPushClient.unsetAlias(context, xiaoMiPushData.getName(),
							null);
					System.out.println("unaliasStatus----------------"
							+ xiaoMiPushData.getName());
				}
				else
				{
					MiPushClient.unsubscribe(context, xiaoMiPushData.getName(),
							null);
					System.out.println("untopicStatus----------------"
							+ xiaoMiPushData.getName());
				}
			}
		}
	}

	private static List<XiaoMiPushData> getXiaoMiPushDataList(Context context)
	{
		XiaoMiPushDataDBHelper lXiaoMiPushDataDBHelper = null;
		try
		{
			lXiaoMiPushDataDBHelper = new XiaoMiPushDataDBHelper(context,
					TableConstants.TABLE_XIAOMIPUSHDATA);
			return lXiaoMiPushDataDBHelper.getPushDataList();
		}
		catch (Exception e)
		{
			LogFile.SaveExceptionLog(e);
		}
		finally
		{
			if (null != lXiaoMiPushDataDBHelper)
				lXiaoMiPushDataDBHelper.closeDB();
		}
		return null;
	}

	public static void changeXiaoMiPushDataListUnRegister(Context context)
	{
		List<XiaoMiPushData> xiaoMiPushDataList = getXiaoMiPushDataList(context);
		if (xiaoMiPushDataList == null)
			return;
		XiaoMiPushDataDBHelper lXiaoMiPushDataDBHelper = null;
		try
		{
			lXiaoMiPushDataDBHelper = new XiaoMiPushDataDBHelper(context,
					TableConstants.TABLE_XIAOMIPUSHDATA);
			for (int i = 0; i < xiaoMiPushDataList.size(); i++)
			{
				XiaoMiPushData xiaoMiPushData = xiaoMiPushDataList.get(i);
				xiaoMiPushData
						.setUnStatus(GobalConstants.PushDataStatus.FAILED);
				lXiaoMiPushDataDBHelper.update(xiaoMiPushData);
			}
		}
		catch (Exception e)
		{
			LogFile.SaveExceptionLog(e);
		}
		finally
		{
			if (null != lXiaoMiPushDataDBHelper)
				lXiaoMiPushDataDBHelper.closeDB();
		}
	}

	private static void addXiaoMiPushDataList(Context context,
			List<XiaoMiPushData> xiaoMiPushDataList)
	{
		if (xiaoMiPushDataList == null)
			return;
		XiaoMiPushDataDBHelper lXiaoMiPushDataDBHelper = null;
		try
		{
			lXiaoMiPushDataDBHelper = new XiaoMiPushDataDBHelper(context,
					TableConstants.TABLE_XIAOMIPUSHDATA);
			for (int i = 0; i < xiaoMiPushDataList.size(); i++)
			{
				XiaoMiPushData xiaoMiPushData = xiaoMiPushDataList.get(i);
				lXiaoMiPushDataDBHelper.insert(xiaoMiPushData);
			}
		}
		catch (Exception e)
		{
			LogFile.SaveExceptionLog(e);
		}
		finally
		{
			if (null != lXiaoMiPushDataDBHelper)
				lXiaoMiPushDataDBHelper.closeDB();
		}
	}

	public static List<XiaoMiPushData> getUnRegisterFailedPushDataList(
			Context context)
	{
		XiaoMiPushDataDBHelper lXiaoMiPushDataDBHelper = null;
		try
		{
			lXiaoMiPushDataDBHelper = new XiaoMiPushDataDBHelper(context,
					TableConstants.TABLE_XIAOMIPUSHDATA);
			return lXiaoMiPushDataDBHelper.getUnRegisterFailedPushDataList();
		}
		catch (Exception e)
		{
			LogFile.SaveExceptionLog(e);
		}
		finally
		{
			if (null != lXiaoMiPushDataDBHelper)
				lXiaoMiPushDataDBHelper.closeDB();
		}
		return null;
	}

	public static void deleteAll(Context context)
	{
		XiaoMiPushDataDBHelper lXiaoMiPushDataDBHelper = null;
		try
		{
			lXiaoMiPushDataDBHelper = new XiaoMiPushDataDBHelper(context,
					TableConstants.TABLE_XIAOMIPUSHDATA);
			lXiaoMiPushDataDBHelper.deleteAll();
		}
		catch (Exception e)
		{
			LogFile.SaveExceptionLog(e);
		}
		finally
		{
			if (null != lXiaoMiPushDataDBHelper)
				lXiaoMiPushDataDBHelper.closeDB();
		}
	}

	public static XiaoMiPushData getPushDataByName(Context context, String name)
	{
		XiaoMiPushDataDBHelper lXiaoMiPushDataDBHelper = null;
		try
		{
			lXiaoMiPushDataDBHelper = new XiaoMiPushDataDBHelper(context,
					TableConstants.TABLE_XIAOMIPUSHDATA);
			return lXiaoMiPushDataDBHelper.getPushDataByName(name);
		}
		catch (Exception e)
		{
			LogFile.SaveExceptionLog(e);
		}
		finally
		{
			if (null != lXiaoMiPushDataDBHelper)
				lXiaoMiPushDataDBHelper.closeDB();
		}
		return null;
	}

	public static void deletePushDataByName(Context context, String name)
	{
		XiaoMiPushDataDBHelper lXiaoMiPushDataDBHelper = null;
		try
		{
			lXiaoMiPushDataDBHelper = new XiaoMiPushDataDBHelper(context,
					TableConstants.TABLE_XIAOMIPUSHDATA);
			lXiaoMiPushDataDBHelper.deleteByName(name);
		}
		catch (Exception e)
		{
			LogFile.SaveExceptionLog(e);
		}
		finally
		{
			if (null != lXiaoMiPushDataDBHelper)
				lXiaoMiPushDataDBHelper.closeDB();
		}
	}

	public static void updateXiaoMiPushData(Context context,
			XiaoMiPushData xiaoMiPushData)
	{
		XiaoMiPushDataDBHelper lXiaoMiPushDataDBHelper = null;
		try
		{
			lXiaoMiPushDataDBHelper = new XiaoMiPushDataDBHelper(context,
					TableConstants.TABLE_XIAOMIPUSHDATA);
			lXiaoMiPushDataDBHelper.update(xiaoMiPushData);
		}
		catch (Exception e)
		{
			LogFile.SaveExceptionLog(e);
		}
		finally
		{
			if (null != lXiaoMiPushDataDBHelper)
				lXiaoMiPushDataDBHelper.closeDB();
		}
	}

	public static void registerAliasAndTopics(Context context, User user)
	{
		List<XiaoMiPushData> xiaoMiPushDataList = getXiaoMiPushDataList(context);
		if (xiaoMiPushDataList != null && xiaoMiPushDataList.size() > 0)
		{
			setAliasAndTopics(context, xiaoMiPushDataList);
		}
		else
		{
			xiaoMiPushDataList = new ArrayList<XiaoMiPushData>();
			XiaoMiPushData lXiaoMiPushData = new XiaoMiPushData(
					UserHelper.ALIASSUFFIX + user.getUid(),
					GobalConstants.PushDataStatus.NORMAL,
					GobalConstants.PushDataStatus.NORMAL,
					GobalConstants.XiaoMiPushDataType.ALIAS);
			xiaoMiPushDataList.add(lXiaoMiPushData);
			XiaoMiPushData lXiaoMiPushData2 = new XiaoMiPushData(
					UserHelper.DEFAULTTOPIC,
					GobalConstants.PushDataStatus.NORMAL,
					GobalConstants.PushDataStatus.NORMAL,
					GobalConstants.XiaoMiPushDataType.DEFAULT_TOPIC);
			xiaoMiPushDataList.add(lXiaoMiPushData2);

			RegisterPushHelper.addXiaoMiPushDataList(context,
					xiaoMiPushDataList);
			setAliasAndTopics(context, xiaoMiPushDataList);
		}
	}
}
