package dianyun.baobaowd.xiaomipush;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.widget.Toast;

public class ApplicationHandler extends Handler
{

	private Context context;

	public ApplicationHandler(Context context)
	{
		this.context = context;
	}

	@Override
	public void handleMessage(Message msg)
	{
		String s = (String) msg.obj;
		Toast.makeText(context, s, Toast.LENGTH_LONG).show();
	}
}