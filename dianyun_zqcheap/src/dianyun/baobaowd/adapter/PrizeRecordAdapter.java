package dianyun.baobaowd.adapter;

import java.io.Serializable;
import java.util.List;

import android.content.Context;
import android.graphics.Bitmap;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;

import dianyun.baobaowd.data.YcoinPay;
import dianyun.zqcheap.R;

public class PrizeRecordAdapter extends BaseAdapter {

	private List<YcoinPay> mDataList;
	private Context mContext;
	DisplayImageOptions mOptions;

	public PrizeRecordAdapter(List<YcoinPay> pDataList, Context mContext) {
		super();
		this.mDataList = pDataList;
		this.mContext = mContext;
		mOptions = new DisplayImageOptions.Builder()
				.showImageOnLoading(R.drawable.defaultsmallimg)
				.showImageForEmptyUri(R.drawable.defaultsmallimg)
				.showImageOnFail(R.drawable.defaultsmallimg)
				.cacheInMemory(true).cacheOnDisk(true).considerExifParams(true)
				.imageScaleType(ImageScaleType.EXACTLY_STRETCHED)
				.bitmapConfig(Bitmap.Config.RGB_565).build();
	}

	@Override
	public int getCount() {
		return mDataList.size();
	}

	@Override
	public Object getItem(int position) {
		return mDataList.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(final int position, View view, ViewGroup parent) {
		final YcoinPay lYcoinPay = mDataList.get(position);
		final HolderView lHolderView;
		if (null == view) {
			view = LayoutInflater.from(mContext).inflate(
					R.layout.prizerecordadapter, null);
			lHolderView = new HolderView();
			lHolderView
					.setGoodsIv((ImageView) view.findViewById(R.id.goods_iv));
			lHolderView.setRebatestatus_tv((TextView) view
					.findViewById(R.id.rebatestatus_tv));
			lHolderView.setRebategold_tv((TextView) view
					.findViewById(R.id.rebategold_tv));
			lHolderView.setDesTv((TextView) view.findViewById(R.id.des_tv));
			lHolderView.setTime_tv((TextView) view.findViewById(R.id.time_tv));

			view.setTag(lHolderView);
		} else {
			lHolderView = (HolderView) view.getTag();
		}

		lHolderView.getDesTv().setText(lYcoinPay.getGiftDesc());

		if (!TextUtils.isEmpty(lYcoinPay.getImgUrl())) {
			ImageLoader.getInstance().displayImage(lYcoinPay.getImgUrl(),
					lHolderView.getGoodsIv(), mOptions);
		}

		lHolderView.getTime_tv().setText(lYcoinPay.getCreateTime()+mContext.getString(R.string.exchange_prize));
		lHolderView.getRebategold_tv().setText(
				lYcoinPay.getPayAmount() + mContext.getString(R.string.gold));

		if(lYcoinPay.getPayStatus()==YcoinPay.PAYSTAUTS_FAILED){
			lHolderView.getRebatestatus_tv().setText(mContext.getString(R.string.paystatus_goldfailed));
		}else if(lYcoinPay.getPayStatus()==YcoinPay.PAYSTAUTS_AUDIT){
			lHolderView.getRebatestatus_tv().setText(mContext.getString(R.string.paystatus_audit));
		}else if(lYcoinPay.getPayStatus()==YcoinPay.PAYSTAUTS_SUCCESS){
			lHolderView.getRebatestatus_tv().setText(mContext.getString(R.string.paystatus_sendalready));
		}
		
		
		
		return view;

	}

	public class HolderView implements Serializable {

		private static final long serialVersionUID = 1L;
		private ImageView goodsIv;
		private TextView desTv;
		// private TextView priceTv;
		// private TextView oldpriceTv;
		// private TextView percent_tv;
		private TextView time_tv;
		private TextView rebategold_tv;
		private TextView count_tv;
		private TextView rebatestatus_tv;

		// private RelativeLayout countdown_layout;
		// private View progress_v;

		public TextView getCount_tv() {
			return count_tv;
		}

		public void setCount_tv(TextView count_tv) {
			this.count_tv = count_tv;
		}

		//
		// public TextView getPercent_tv() {
		// return percent_tv;
		// }
		//
		// public void setPercent_tv(TextView percent_tv) {
		// this.percent_tv = percent_tv;
		// }

		public TextView getTime_tv() {
			return time_tv;
		}

		public void setTime_tv(TextView time_tv) {
			this.time_tv = time_tv;
		}

		// public RelativeLayout getCountdown_layout() {
		// return countdown_layout;
		// }
		//
		// public void setCountdown_layout(RelativeLayout countdown_layout) {
		// this.countdown_layout = countdown_layout;
		// }
		//
		// public View getProgress_v() {
		// return progress_v;
		// }
		//
		// public void setProgress_v(View progress_v) {
		// this.progress_v = progress_v;
		// }

		public ImageView getGoodsIv() {
			return goodsIv;
		}

		public void setGoodsIv(ImageView goodsIv) {
			this.goodsIv = goodsIv;
		}

		public TextView getRebategold_tv() {
			return rebategold_tv;
		}

		public void setRebategold_tv(TextView rebategold_tv) {
			this.rebategold_tv = rebategold_tv;
		}

		public TextView getRebatestatus_tv() {
			return rebatestatus_tv;
		}

		public void setRebatestatus_tv(TextView rebatestatus_tv) {
			this.rebatestatus_tv = rebatestatus_tv;
		}

		public TextView getDesTv() {
			return desTv;
		}

		public void setDesTv(TextView desTv) {
			this.desTv = desTv;
		}

		// public TextView getPriceTv() {
		// return priceTv;
		// }
		//
		// public void setPriceTv(TextView priceTv) {
		// this.priceTv = priceTv;
		// }
		//
		// public TextView getOldpriceTv() {
		// return oldpriceTv;
		// }
		//
		// public void setOldpriceTv(TextView oldpriceTv) {
		// this.oldpriceTv = oldpriceTv;
		// }
	}
}
