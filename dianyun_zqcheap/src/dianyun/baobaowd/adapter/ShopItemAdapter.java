package dianyun.baobaowd.adapter;

import java.util.List;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import dianyun.zqcheap.fragment.ShopListFragment;

public class ShopItemAdapter extends FragmentStatePagerAdapter
{

	private FragmentManager mFragmentManager;
	private List<ShopListFragment> mList;

	public ShopItemAdapter(FragmentManager fm, List<ShopListFragment> ee)
	{
		super(fm);
		mList = ee;
		mFragmentManager = fm;
		
	}

	

	@Override
	public Fragment getItem(int arg0)
	{
		Fragment res = null;
		if (mList != null)
		{
			res = mList.get(arg0);
		}
		return res;
	}

	@Override
	public int getCount()
	{
		int res = 0;
		if (mList != null)
		{
			res = mList.size();
		}
		return res;
	}

	@Override
	public CharSequence getPageTitle(int position)
	{
		if (mList != null && mList.size() > position)
		{
			return mList.get(position).getTitle();
		}
		return "";
	}

	// @Override
	// public void destroyItem(ViewGroup container, int position, Object object)
	// {
	// super.destroyItem(container, position, object);
	// }
	//
//	@Override
//	public Object instantiateItem(ViewGroup container, int position)
//	{
//		ShopListFragment fragment = (ShopListFragment) super.instantiateItem(
//				container, position);
//		if (fragment.mIsInit)
//		{
//			fragment.reInitData();
//		}
//		return fragment;
//	}
}
