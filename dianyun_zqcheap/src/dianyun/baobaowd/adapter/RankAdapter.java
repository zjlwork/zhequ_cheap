package dianyun.baobaowd.adapter;

import java.io.Serializable;
import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import dianyun.baobaowd.data.User;
import dianyun.baobaowd.util.UserHelper;
import dianyun.zqcheap.R;

public class RankAdapter extends BaseAdapter
{

	private List<User> mUserList;
	private Context mContext;
	

	public RankAdapter( Context mContext,List<User> mUserList)
	{
		super();
		this.mContext = mContext;
		this.mUserList = mUserList;
	}
	

	@Override
	public int getCount()
	{
		return mUserList.size();
	}

	@Override
	public Object getItem(int position)
	{
		return mUserList.get(position);
	}

	@Override
	public long getItemId(int position)
	{
		return position;
	}

	@Override
	public View getView(final int position, View view, ViewGroup parent)
	{
		final User user = mUserList.get(position);
		final HolderView lHolderView;
		if (null == view)
		{
			view = LayoutInflater.from(mContext).inflate(
					R.layout.rankadapter, null);
			lHolderView = new HolderView();
			lHolderView.setNameTv((TextView) view
					.findViewById(R.id.username_tv));
			lHolderView.setRankTv((TextView) view.findViewById(R.id.rank_tv));
			lHolderView.setAvatarIv((ImageView) view
					.findViewById(R.id.avatar_iv));
			lHolderView.setRankIv((ImageView) view.findViewById(R.id.rank_iv));
			lHolderView.setDesTv((TextView) view.findViewById(R.id.des_tv));
			
		
			view.setTag(lHolderView);
		}
		else
		{
			lHolderView = (HolderView) view.getTag();
		}
		
		UserHelper.setUserNameTv(user, mContext, lHolderView.getNameTv());
		lHolderView.getAvatarIv().setImageResource(R.drawable.defaultavatar);
		UserHelper.showUserAvatar(mContext, user, lHolderView.getAvatarIv());
//		lHolderView.getDesTv().setCompoundDrawablesWithIntrinsicBounds(
//				mContext.getResources().getDrawable(R.drawable.goldiv), null,
//				null, null);
		lHolderView.getDesTv().setText(""+user.getYcoinsNum());
		if(position<3){
			lHolderView.getRankIv().setVisibility(View.VISIBLE);
			lHolderView.getRankTv().setVisibility(View.GONE);
			if(position==0)
				lHolderView.getRankIv().setImageResource(R.drawable.rank_numone);
			else if(position==1)
				lHolderView.getRankIv().setImageResource(R.drawable.rank_numtwo);
			if(position==2)
				lHolderView.getRankIv().setImageResource(R.drawable.rank_numthree);
		}else{
			lHolderView.getRankIv().setVisibility(View.GONE);
			lHolderView.getRankTv().setVisibility(View.VISIBLE);
		}
		lHolderView.getRankTv().setText(String.valueOf(position+1));

//		lHolderView.getAvatarIv().setOnClickListener(new OnClickListener()
//		{
//
//			@Override
//			public void onClick(View v)
//			{
//				UserHelper.goPersonCenterActivity(mContext, user);
//			}
//		});
		
		return view;
	}



	public class HolderView implements Serializable
	{

		private static final long serialVersionUID = 1L;
		private TextView rankTv;
		private TextView nameTv;
		private ImageView avatarIv;
		private TextView desTv;
		private ImageView rankIv;
		private RelativeLayout rankLayout;
		
		
		public RelativeLayout getRankLayout()
		{
			return rankLayout;
		}


		
		public void setRankLayout(RelativeLayout rankLayout)
		{
			this.rankLayout = rankLayout;
		}


		public TextView getDesTv()
		{
			return desTv;
		}

		
		public void setDesTv(TextView desTv)
		{
			this.desTv = desTv;
		}

		
		public ImageView getRankIv()
		{
			return rankIv;
		}

		
		public void setRankIv(ImageView rankIv)
		{
			this.rankIv = rankIv;
		}

		public TextView getNameTv()
		{
			return nameTv;
		}

		public void setNameTv(TextView nameTv)
		{
			this.nameTv = nameTv;
		}

		
		public TextView getRankTv()
		{
			return rankTv;
		}

		public void setRankTv(TextView rankTv)
		{
			this.rankTv = rankTv;
		}

		

		
		public ImageView getAvatarIv()
		{
			return avatarIv;
		}

		public void setAvatarIv(ImageView avatarIv)
		{
			this.avatarIv = avatarIv;
		}

		

		
	}
	
	
	
	
}
