package dianyun.baobaowd.adapter;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Toast;

import com.google.gson.reflect.TypeToken;

import dianyun.baobaowd.adapter.viewpagerbase.PagerViewHelper;
import dianyun.baobaowd.data.User;
import dianyun.baobaowd.data.YcoinPay;
import dianyun.baobaowd.db.LightDBHelper;
import dianyun.baobaowd.defineview.xlistview.CustomListView;
import dianyun.baobaowd.defineview.xlistview.CustomListView.OnLoadMoreListener;
import dianyun.baobaowd.defineview.xlistview.CustomListView.OnRefreshListener;
import dianyun.baobaowd.dto.ResultDTO;
import dianyun.baobaowd.gson.GsonHelper;
import dianyun.baobaowd.serverinterface.GetApplyList;
import dianyun.baobaowd.util.BroadCastHelper;
import dianyun.baobaowd.util.ConversionHelper;
import dianyun.baobaowd.util.GobalConstants;
import dianyun.baobaowd.util.NetworkStatus;
import dianyun.baobaowd.util.UserHelper;
import dianyun.zqcheap.R;
import dianyun.zqcheap.activity.OrderRecordActivity;
import dianyun.zqcheap.activity.RecordActivity;

public class ExchangeRecordPagerViewHelper extends PagerViewHelper
{

	private PrizeRecordAdapter mPrizeRecordAdapter;
	private List<YcoinPay> mPrizeDataList;
	private ExchangeRecordAdapter mExchangeRecordAdapter;
	private List<YcoinPay> mTiXianDataList;


	private int mPayType;
	
	

	public ExchangeRecordPagerViewHelper(Context context, int type)
	{
		super(context,type);
		mTiXianDataList = new ArrayList<YcoinPay>();
		mExchangeRecordAdapter = new ExchangeRecordAdapter(mTiXianDataList, context);
		mPrizeDataList = new ArrayList<YcoinPay>();
		mPrizeRecordAdapter = new PrizeRecordAdapter(mPrizeDataList, context);
		if (mType == RecordActivity.RECORD_TIXIAN)
			mPayType = YcoinPay.PAYTYPE_MONDY;
		else if (mType == RecordActivity.RECORD_PRIZE)
			mPayType = YcoinPay.PAYTYPE_PRIZE;
		
		
	}

	public View initView()
	{
		View lView = LayoutInflater.from(mContext).inflate(
				R.layout.questionpagerview_layout, null);
		mListView = (CustomListView) lView.findViewById(R.id.listview);
		if (mType == RecordActivity.RECORD_TIXIAN){
			mListView.setAdapter(mExchangeRecordAdapter);
//			mListView.setDivider(mContext.getResources().getDrawable(R.drawable.listdivider));
			mListView.setDividerHeight(ConversionHelper.dipToPx(0, mContext));
		}else if (mType == RecordActivity.RECORD_PRIZE){
			mListView.setAdapter(mPrizeRecordAdapter);
			mListView.setDivider(null);
			mListView.setDividerHeight(ConversionHelper.dipToPx(8, mContext));
		}
		mListView.setCacheColorHint(0);

		mListView.setOnItemClickListener(new OnItemClickListener()
		{

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1,
					int position, long arg3)
			{
//				if (position > 0)
//				{
//					if (mType == GobalConstants.MyAnswersType.ALL)
//					{
//						Answer lAnswer = mMyAnswersList.get(position - 1);
//						QuestionHelper.goQuestionDetailActivity(mContext,
//								lAnswer.getQuestionId());
//					}
//					else
//						if (mType == GobalConstants.MyAnswersType.BEST)
//						{
//							AcceptedAnswer lAcceptedAnswer = mMyBestAnswersList
//									.get(position-1);
//							QuestionHelper.goQuestionDetailActivity(mContext,
//									lAcceptedAnswer.getQuestionId());
//						}
//				}
			}
		});
		mListView.setOnRefreshListener(new OnRefreshListener()
		{

			@Override
			public void onRefresh()
			{
				if (NetworkStatus.getNetWorkStatus(mContext) > 0)
				{
					new GetApplyListThread(1,OrderRecordActivity.PAGESIZE)
							.start();
				}
				else
				{
					Toast.makeText(mContext,
							mContext.getString(R.string.no_network), Toast.LENGTH_SHORT)
							.show();
					mListView.onRefreshComplete();
				}
			}
		});
		mListView.setOnLoadListener(new OnLoadMoreListener()
		{

			@Override
			public void onLoadMore()
			{
				if (NetworkStatus.getNetWorkStatus(mContext) > 0)
				{
					int nextpage = getNextPage();
					new GetApplyListThread(nextpage,OrderRecordActivity.PAGESIZE).start();
				}
				else
				{
					Toast.makeText(mContext,
							mContext.getString(R.string.no_network), Toast.LENGTH_SHORT)
							.show();
					mListView.onLoadMoreComplete();
				}
			}
		});
		if (mType == RecordActivity.RECORD_TIXIAN)
			noDataStatus(mTiXianDataList);
		else if (mType == RecordActivity.RECORD_PRIZE)
			noDataStatus(mPrizeDataList);
		mListView.refresh();
		return lView;
	}

	class GetApplyListThread extends Thread
	{

		private Handler handler;
		private ResultDTO resultDTO;
		int curPage;
		int pagesize;
		boolean getnew = false;

		public GetApplyListThread(int curPage, int pagesize)
		{
			handler = new Handler();
			this.curPage = curPage;
			this.pagesize = pagesize;
			if (curPage == 0 || curPage == 1)getnew=true;
		}

		@Override
		public void run()
		{
			User mUser = UserHelper.getUser();
			resultDTO = new GetApplyList(mUser.getUid(), mUser.getToken(),
						curPage, pagesize,mPayType).getConnect();
			
			handler.post(new Runnable()
			{

				@Override
				public void run()
				{
					if (resultDTO != null && resultDTO.getCode().equals("0"))
					{
						List<YcoinPay> applyList = GsonHelper.gsonToObj(
								resultDTO.getResult(),
								new TypeToken<List<YcoinPay>>()
								{
								});
						if (mType == RecordActivity.RECORD_TIXIAN)
							refreshList(applyList,mTiXianDataList,mExchangeRecordAdapter,
									getnew);	
						else if (mType == RecordActivity.RECORD_PRIZE)
							refreshList(applyList,mPrizeDataList,mPrizeRecordAdapter,
									getnew);	
						noDataStatus(applyList);
						if(getnew){
							LightDBHelper.setCashCount(mContext, 0);
							BroadCastHelper.sendRefreshMainBroadcast(mContext,GobalConstants.RefreshType.ALLNEW);
						}
					}
					if (getnew)
						mListView.onRefreshComplete();
					else
						mListView.onLoadMoreComplete();
				}
			});
		}
	}
	
	
	
	private int getNextPage()
	{
		if (mType == RecordActivity.RECORD_TIXIAN){
			if (mTiXianDataList == null || mTiXianDataList.size() == 0)
				return 1;
			else
				return mTiXianDataList.size() / OrderRecordActivity.PAGESIZE + 1;
		}else if (mType == RecordActivity.RECORD_PRIZE){
			if (mPrizeDataList == null || mPrizeDataList.size() == 0)
				return 1;
			else
				return mPrizeDataList.size() / OrderRecordActivity.PAGESIZE + 1;
		}
		return 1;
	}
	
	
	
	
	
	

}
