package dianyun.baobaowd.adapter;

import java.util.List;

import android.content.Context;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;

import dianyun.baobaowd.entity.CateItem;
import dianyun.zqcheap.R;
import dianyun.zqcheap.application.BaoBaoWDApplication;

public class MainGoodsAdapter extends BaseAdapter {

    private Context mContext;
    private List<CateItem> mDataList;
    private boolean mIsHasPadding = true;
    private View mCurrentActivityView;

    public MainGoodsAdapter(Context context, List<CateItem> datalist) {
        mContext = context;
        mDataList = datalist;
    }

    public void setNoPadding() {
        mIsHasPadding = false;
    }

    public void setDataSource(List<CateItem> datalist) {
        mDataList = datalist;
        notifyDataSetChanged();
    }

    public void setCurrentActivityRootView(View activityView) {
        mCurrentActivityView = activityView;
    }

    @Override
    public int getCount() {
        int res = 0;
        if (mDataList != null) {
            res = mDataList.size();
        }
        return res;
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder = null;
        if (convertView == null) {
//			if (!mIsHasPadding)
//			{
            convertView = LayoutInflater.from(mContext).inflate(
                    R.layout.main_goods_item_lay, null);
//			}
//			else
//			{
//            convertView = LayoutInflater.from(mContext).inflate(
//                    R.layout.goods_item_lay, null);
//			}
            viewHolder = new ViewHolder();
            viewHolder.mCurrentPriceTV = (TextView) convertView
                    .findViewById(R.id.goods_currentPrice);
            viewHolder.mPriceTV = (TextView) convertView
                    .findViewById(R.id.goods_Price);
            viewHolder.mPriceTV.getPaint()
                    .setFlags(Paint.STRIKE_THRU_TEXT_FLAG); // 中划线
            viewHolder.mGoodsTitleTV = (TextView) convertView
                    .findViewById(R.id.goods_title);
            viewHolder.mGoodsBackFeesTV = (TextView) convertView
                    .findViewById(R.id.goods_backfees);
            viewHolder.mGoodsImageView = (ImageView) convertView
                    .findViewById(R.id.goods_image);
            convertView.setTag(viewHolder);
        }
        viewHolder = (ViewHolder) convertView.getTag();
        // 初始化
        ImageLoader.getInstance().displayImage(
                mDataList.get(position).pics == null ? ""
                        : mDataList.get(position).pics.get(0),
                viewHolder.mGoodsImageView, BaoBaoWDApplication.mOptions);
        viewHolder.mGoodsTitleTV.setText(mDataList.get(position).title);
        viewHolder.mCurrentPriceTV.setText(""
                + mDataList.get(position).umpPrice);
        viewHolder.mPriceTV.setText("" + mDataList.get(position).price);
        if (mDataList.get(position).coins == null || mDataList.get(position).coins == 0) {
            viewHolder.mGoodsBackFeesTV.setVisibility(View.GONE);
        } else {
            viewHolder.mGoodsBackFeesTV.setVisibility(View.VISIBLE);
        }
        viewHolder.mGoodsBackFeesTV.setText("返" + (mDataList.get(position).coins == null ? 0 : mDataList.get(position).coins)
                + "金币");
        return convertView;
    }

    private class ViewHolder {

        public TextView mCurrentPriceTV;
        public TextView mPriceTV;
        public ImageView mGoodsImageView;
        public TextView mGoodsTitleTV;
        public TextView mGoodsBackFeesTV;
    }
}
