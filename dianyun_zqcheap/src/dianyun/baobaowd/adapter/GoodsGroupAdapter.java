package dianyun.baobaowd.adapter;

import java.io.Serializable;
import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import dianyun.baobaowd.data.Item;
import dianyun.zqcheap.R;

public class GoodsGroupAdapter extends BaseAdapter
{

	private List<Item> mDataList;
	private Context mContext;

	public GoodsGroupAdapter(List<Item> pDataList, Context mContext)
	{
		super();
		this.mDataList = pDataList;
		this.mContext = mContext;
	}

	@Override
	public int getCount()
	{
		return mDataList.size();
	}

	@Override
	public Object getItem(int position)
	{
		return mDataList.get(position);
	}

	@Override
	public long getItemId(int position)
	{
		return position;
	}

	@Override
	public View getView(final int position, View view, ViewGroup parent)
	{
		final Item lItem = mDataList.get(position);
		final HolderView lHolderView;
		if (null == view)
		{
			view = LayoutInflater.from(mContext).inflate(
					R.layout.goodsgroupadapter, null);
			lHolderView = new HolderView();
			lHolderView.setNameTv((TextView) view.findViewById(R.id.name_tv));
			lHolderView.setPriceTv((TextView) view.findViewById(R.id.price_tv));
			lHolderView.setOldPriceTv((TextView) view
					.findViewById(R.id.oldprice_tv));
			lHolderView
					.setGoodsIv((ImageView) view.findViewById(R.id.goods_iv));
			view.setTag(lHolderView);
		}
		else
		{
			lHolderView = (HolderView) view.getTag();
		}
		lHolderView.getNameTv().setText(lItem.getTitle());
		lHolderView.getOldPriceTv().setText(lItem.getPrice() + "");
		lHolderView.getPriceTv().setText(lItem.getSalePrice() + "");
		return view;
	}

	public class HolderView implements Serializable
	{

		private static final long serialVersionUID = 1L;
		private ImageView goodsIv;
		private TextView priceTv;
		private TextView oldPriceTv;
		private TextView nameTv;

		public ImageView getGoodsIv()
		{
			return goodsIv;
		}

		public void setGoodsIv(ImageView goodsIv)
		{
			this.goodsIv = goodsIv;
		}

		public TextView getPriceTv()
		{
			return priceTv;
		}

		public void setPriceTv(TextView priceTv)
		{
			this.priceTv = priceTv;
		}

		public TextView getOldPriceTv()
		{
			return oldPriceTv;
		}

		public void setOldPriceTv(TextView oldPriceTv)
		{
			this.oldPriceTv = oldPriceTv;
		}

		public TextView getNameTv()
		{
			return nameTv;
		}

		public void setNameTv(TextView nameTv)
		{
			this.nameTv = nameTv;
		}
	}
}
