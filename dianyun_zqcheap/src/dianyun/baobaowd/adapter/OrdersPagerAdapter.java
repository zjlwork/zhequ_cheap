package dianyun.baobaowd.adapter;

import java.util.List;
import android.content.Context;
import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;

public class OrdersPagerAdapter extends PagerAdapter
{

	private Context mContext;
	private List<OrdersPagerViewHelper> mPagerViewHelperList;

	public OrdersPagerAdapter(
			List<OrdersPagerViewHelper> mPagerViewHelperList,
			Context pContext)
	{
		this.mContext = pContext;
		this.mPagerViewHelperList = mPagerViewHelperList;
	}

	@Override
	public int getCount()
	{
		return mPagerViewHelperList.size();
	}

	@Override
	public Object instantiateItem(View collection, final int position)
	{
		OrdersPagerViewHelper lPagerViewHelper = mPagerViewHelperList
				.get(position);
		View lView = lPagerViewHelper.initView();
		((ViewPager) collection).addView(lView);
//		System.out.println("instantiateItem========="+position);
		return lView;
	}

	@Override
	public void destroyItem(View collection, int position, Object view)
	{
		((ViewPager) collection).removeView((View) view);
//		System.out.println("destroyItem========="+position);
	}

	@Override
	public boolean isViewFromObject(View view, Object object)
	{
		return view == (object);
	}

	@Override
	public void finishUpdate(View arg0)
	{
	}

	@Override
	public void restoreState(Parcelable arg0, ClassLoader arg1)
	{
	}

	@Override
	public Parcelable saveState()
	{
		return null;
	}

	@Override
	public void startUpdate(View arg0)
	{
	}

	@Override
	public int getItemPosition(Object object)
	{
		return POSITION_NONE;
	}
}