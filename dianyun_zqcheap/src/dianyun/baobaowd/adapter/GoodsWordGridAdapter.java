package dianyun.baobaowd.adapter;

import java.io.Serializable;
import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;

import dianyun.baobaowd.data.User;
import dianyun.baobaowd.entity.Menu;
import dianyun.baobaowd.util.UserHelper;
import dianyun.zqcheap.R;
import dianyun.zqcheap.application.BaoBaoWDApplication;

public class GoodsWordGridAdapter extends BaseAdapter
{

	private List<Menu> mDataList;
	private Context mContext;
	private User mUser;
	private int imageWidth;

	public void setImageWidth(int imageWidth){
		this.imageWidth =  imageWidth;
		notifyDataSetChanged();
	}
	public GoodsWordGridAdapter(List<Menu> pDataList, Context mContext,int imageWidth)
	{
		super();
		this.mDataList = pDataList;
		this.mContext = mContext;
		this.imageWidth = imageWidth;
		mUser = UserHelper.getUser();
	}

	@Override
	public int getCount()
	{
		return mDataList.size();
	}

	@Override
	public Object getItem(int position)
	{
		return mDataList.get(position);
	}

	@Override
	public long getItemId(int position)
	{
		return position;
	}

	@Override
	public View getView(final int position, View view, ViewGroup parent)
	{
		final Menu lMenu = mDataList.get(position);
		final HolderView lHolderView;
		if (null == view)
		{
			view = LayoutInflater.from(mContext).inflate(R.layout.goodswordgridadapter,
					null);
			lHolderView = new HolderView();
			lHolderView.setItemIv((ImageView) view.findViewById(R.id.item_iv));
			lHolderView.setItemTv((TextView) view.findViewById(R.id.item_tv));
			view.setTag(lHolderView);
		}
		else
		{
			lHolderView = (HolderView) view.getTag();
		}
		lHolderView.getItemTv().setText(lMenu.name);
		lHolderView.getItemIv().setLayoutParams(new LinearLayout.LayoutParams(imageWidth,imageWidth));
		ImageLoader.getInstance().displayImage(lMenu.pic,
                    lHolderView.getItemIv(), BaoBaoWDApplication.mOptions);
		
		return view;
	}

	public class HolderView implements Serializable
	{

		private static final long serialVersionUID = 1L;
		private TextView itemTv;
		private ImageView itemIv;
		public TextView getItemTv() {
			return itemTv;
		}
		public void setItemTv(TextView itemTv) {
			this.itemTv = itemTv;
		}
		public ImageView getItemIv() {
			return itemIv;
		}
		public void setItemIv(ImageView itemIv) {
			this.itemIv = itemIv;
		}

		
	}
}
