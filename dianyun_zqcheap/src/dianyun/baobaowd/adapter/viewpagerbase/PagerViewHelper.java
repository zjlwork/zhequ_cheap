package dianyun.baobaowd.adapter.viewpagerbase;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.BaseAdapter;
import dianyun.baobaowd.data.User;
import dianyun.baobaowd.defineview.xlistview.CustomListView;
import dianyun.zqcheap.R;

public abstract class PagerViewHelper
{

	protected Context mContext;
	protected int mType;
	protected CustomListView mListView;
	protected User mOtherUser;
	protected int mPosition = 0;
	
	public void  setPositon(int mPosition){
		this.mPosition = mPosition;
		
	}

	public PagerViewHelper(Context context)
	{
		this.mContext = context;
	}
	public PagerViewHelper(Context context, int type)
	{
		this.mContext = context;
		this.mType = type;
	}

	public int getType()
	{
		return mType;
	}

	protected <T> void noDataStatus(List<T>  list)
	{
		if (list != null && list.size() > 0)
		{
			if (list.size() < 20)
				mListView.setCanLoadMore(false);
			else
				mListView.setCanLoadMore(true);
		}
		else
		{
			mListView.setCanLoadMore(false);
		}
	}

	
	protected View initView()
	{
		View lView = LayoutInflater.from(mContext).inflate(
				R.layout.questionpagerview_layout, null);
		
		return lView;
	}

	protected <T> void refreshList(List<T> templist,List<T> totalList,BaseAdapter adpter, boolean isGetNew)
	{
		if (isGetNew)
			totalList.clear();
		if (templist != null && templist.size() > 0)
			totalList.addAll(templist);
		adpter.notifyDataSetChanged();
	}

	
	public void setOtherUser(User mOtherUser)
	{
		this.mOtherUser = mOtherUser;
	}
}
