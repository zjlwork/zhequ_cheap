package dianyun.baobaowd.adapter;

import java.io.Serializable;
import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import dianyun.baobaowd.data.HotWord;
import dianyun.baobaowd.data.User;
import dianyun.baobaowd.util.UserHelper;
import dianyun.zqcheap.R;
import dianyun.zqcheap.activity.ShowScanResultActivity;

public class HotTagAdapter extends BaseAdapter
{

	private List<HotWord> mDataList;
	private Context mContext;
	private User mUser;

	public HotTagAdapter(List<HotWord> pDataList, Context mContext)
	{
		super();
		this.mDataList = pDataList;
		this.mContext = mContext;
		mUser = UserHelper.getUser();
	}

	@Override
	public int getCount()
	{
		return mDataList.size();
	}

	@Override
	public Object getItem(int position)
	{
		return mDataList.get(position);
	}

	@Override
	public long getItemId(int position)
	{
		return position;
	}

	@Override
	public View getView(final int position, View view, ViewGroup parent)
	{
		final HotWord lHotWord = mDataList.get(position);
		final HolderView lHolderView;
		if (null == view)
		{
			view = LayoutInflater.from(mContext).inflate(R.layout.gridkeywordadapter,
					null);
			lHolderView = new HolderView();
			lHolderView.setSortTv((TextView) view.findViewById(R.id.sort_tv));
			lHolderView.getSortTv().setText(lHotWord.getWord());
			view.setTag(lHolderView);
		}
		else
		{
			lHolderView = (HolderView) view.getTag();
		}
		lHolderView.getSortTv().setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				((ShowScanResultActivity)mContext).hotWordClick(lHotWord);
				
			}
		});
		return view;
		
	}

	public class HolderView implements Serializable
	{

		private static final long serialVersionUID = 1L;
		private TextView sortTv;

		public TextView getSortTv()
		{
			return sortTv;
		}

		public void setSortTv(TextView sortTv)
		{
			this.sortTv = sortTv;
		}
	}
}
