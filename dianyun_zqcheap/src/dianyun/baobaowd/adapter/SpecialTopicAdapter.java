package dianyun.baobaowd.adapter;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import dianyun.baobaowd.defineview.SpecialTopicView;
import dianyun.baobaowd.entity.CateItem;
import dianyun.zqcheap.R;

public class SpecialTopicAdapter extends BaseAdapter
{

	private Context mContext;
	private List<List<CateItem>> mDataList;
	private View mCurrentActivityView;

	public SpecialTopicAdapter(Context context, List<List<CateItem>> datalist)
	{
		mContext = context;
		mDataList = datalist;
	}

	public void setDataSource(List<List<CateItem>> datalist)
	{
		mDataList = datalist;
	}

	public void setCurrentActivityRootView(View activityView)
	{
		mCurrentActivityView = activityView;
	}

	@Override
	public int getCount()
	{
		int res = 0;
		if (mDataList != null)
		{
			res = mDataList.size();
		}
		return res;
	}

	@Override
	public Object getItem(int position)
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getItemId(int position)
	{
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent)
	{
		ViewHolder viewHolder = null;
		if (convertView == null)
		{
			convertView = LayoutInflater.from(mContext).inflate(
					R.layout.special_list_item_lay, null);
			viewHolder = new ViewHolder();
			viewHolder.mCurrentSpecialView = (SpecialTopicView) convertView
					.findViewById(R.id.special_item_view);
			viewHolder.mCurrentSpecialView
					.setCurrentActivityRootView(mCurrentActivityView);
			convertView.setTag(viewHolder);
		}
		viewHolder = (ViewHolder) convertView.getTag();
		// 初始化
		viewHolder.mCurrentSpecialView.setDataSource(mDataList.get(position));
		return convertView;
	}

	private class ViewHolder
	{

		public SpecialTopicView mCurrentSpecialView;
	}
}
