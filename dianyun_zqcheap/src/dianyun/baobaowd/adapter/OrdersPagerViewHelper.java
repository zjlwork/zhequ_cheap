package dianyun.baobaowd.adapter;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Toast;

import com.google.gson.reflect.TypeToken;

import dianyun.baobaowd.data.Order;
import dianyun.baobaowd.data.User;
import dianyun.baobaowd.db.LightDBHelper;
import dianyun.baobaowd.defineview.xlistview.CustomListView;
import dianyun.baobaowd.defineview.xlistview.CustomListView.OnLoadMoreListener;
import dianyun.baobaowd.defineview.xlistview.CustomListView.OnRefreshListener;
import dianyun.baobaowd.dto.ResultDTO;
import dianyun.baobaowd.gson.GsonHelper;
import dianyun.baobaowd.serverinterface.GetOrders;
import dianyun.baobaowd.serverinterface.ShopHttpRequest;
import dianyun.baobaowd.util.BroadCastHelper;
import dianyun.baobaowd.util.ConversionHelper;
import dianyun.baobaowd.util.GobalConstants;
import dianyun.baobaowd.util.NetworkStatus;
import dianyun.baobaowd.util.TaeSdkUtil;
import dianyun.baobaowd.util.UserHelper;
import dianyun.zqcheap.R;
import dianyun.zqcheap.activity.OrderRecordActivity;

public class OrdersPagerViewHelper
{

	Context mContext;
	List<Order> mList;
	OrderRecordAdapter mOrderRecordAdapter;
	CustomListView mListView;
	int mMode;
	View mCurrentActivityView;

	public OrdersPagerViewHelper(Context context, int mMode)
	{
		this.mContext = context;
		this.mMode = mMode;
		mList = new ArrayList<Order>();
		mOrderRecordAdapter = new OrderRecordAdapter(mList, context);
		if (mMode == OrderRecordActivity.UNREBATE)
		{
			mOrderRecordAdapter.setRebateStatus(false);
		}
		else if (mMode == OrderRecordActivity.REBATE)
		{
			mOrderRecordAdapter.setRebateStatus(true);
		}
	}
	 public void setCurrentActivityRootView(View activityView) {
	        mCurrentActivityView = activityView;
	    }
	
	private void noDataStatus(List<Order> list)
	{
		if (list != null && list.size() > 0)
		{
			if (list.size() < 20)
				mListView.setCanLoadMore(false);
			else
				mListView.setCanLoadMore(true);
		}
		else
		{
			mListView.setCanLoadMore(false);
		}
	}

	

	public View initView()
	{
		View lView = LayoutInflater.from(mContext).inflate(
				R.layout.questionpagerview_layout, null);
		mListView = (CustomListView) lView.findViewById(R.id.listview);
		mListView.setDividerHeight(ConversionHelper.dipToPx(8, mContext));
		mListView.setAdapter(mOrderRecordAdapter);
		mListView.setCacheColorHint(0);
		
		mListView.setOnRefreshListener(new OnRefreshListener()
		{

			@Override
			public void onRefresh()
			{
				if (NetworkStatus.getNetWorkStatus(mContext) > 0)
				{
					new GetOrdersThread(OrderRecordActivity.PAGESIZE, 1, mMode).start();
				}
				else
				{
					Toast.makeText(mContext,
							mContext.getString(R.string.no_network), Toast.LENGTH_SHORT)
							.show();
					mListView.onRefreshComplete();
				}
			}
		});
		mListView.setOnLoadListener(new OnLoadMoreListener()
		{

			@Override
			public void onLoadMore()
			{
				if (NetworkStatus.getNetWorkStatus(mContext) > 0)
				{
					int nextpage = getNextPage();
					new GetOrdersThread(OrderRecordActivity.PAGESIZE, nextpage, mMode).start();
				}
				else
				{
					Toast.makeText(mContext,
							mContext.getString(R.string.no_network),
							Toast.LENGTH_SHORT).show();
					mListView.onLoadMoreComplete();
				}
			}
		});
		mListView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				if(position>0){
					Order order = mList.get(position-1);
					
					if(order.getItemType()!=null){
						TaeSdkUtil.showTAEItemDetail(
							(Activity) mContext, order.getTbItemId(),
							order.getTaobaoPid(),order.getIsTk()==1,
							order.getItemType(), null,mCurrentActivityView);
					}
				}
				
				
			}
		});
		noDataStatus(mList);
		mListView.refresh();
		return lView;
	}

	private int getNextPage()
	{
		if (mList == null || mList.size() == 0)
			return 1;
		else
			return mList.size() / OrderRecordActivity.PAGESIZE + 1;
	}
	
	class GetOrdersThread extends Thread
	{

		private Handler handler;
		private ResultDTO resultDTO;
		private int pagesize;
		private int curPage;
		int mode;

		public GetOrdersThread(int pagesize, int curPage, int mode)
		{
			handler = new Handler();
			this.pagesize = pagesize;
			this.curPage = curPage;
			this.mode = mode;
		}

		@Override
		public void run()
		{
			User mUser =UserHelper.getUser();
			if (mode == OrderRecordActivity.UNREBATE)
			{
				resultDTO = ShopHttpRequest.getInstance(
						mContext).getOrders(mUser.getUid(),
						mUser.getToken(), curPage, pagesize);
			}
			else if (mode == OrderRecordActivity.REBATE)
			{
				resultDTO = new GetOrders(mUser.getUid(),
						mUser.getToken(), OrderRecordActivity.SENDSTATUS_REBATE, curPage,
						pagesize).getConnect();
			}
			handler.post(new Runnable()
			{

				@Override
				public void run()
				{
					if (resultDTO != null && resultDTO.getCode().equals("0"))
					{
						List<Order> orders = GsonHelper.gsonToObj(
								resultDTO.getResult(),
								new TypeToken<List<Order>>()
								{
								});
							if (curPage == 0 || curPage == 1)
								 mList.clear();
							if (orders != null && orders.size() > 0)
								mList.addAll(orders);
							mOrderRecordAdapter.notifyDataSetChanged();
							noDataStatus(orders);
							
							if(mode == OrderRecordActivity.REBATE){
								LightDBHelper.setFanliCount(mContext, 0);
								BroadCastHelper.sendRefreshMainBroadcast(mContext,GobalConstants.RefreshType.ALLNEW);
								((OrderRecordActivity)mContext).refreshNewest();
							}
							
					}
					if (curPage == 0 || curPage == 1)
						mListView.onRefreshComplete();
					else
						mListView.onLoadMoreComplete();
				}
			});
		}
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
