package dianyun.baobaowd.adapter;

import java.util.List;

import android.content.Context;
import android.graphics.Bitmap;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

import dianyun.baobaowd.data.Gift;
import dianyun.baobaowd.util.ConversionHelper;
import dianyun.baobaowd.util.Utils;
import dianyun.zqcheap.R;

public class GoldListAdapter extends BaseAdapter {

    private DisplayImageOptions mOptions;
    private Context mContext;
    private List<Gift> mDataList;
    private int mWidth = 0;
    private View mCurrentActivityView;
    private RefreshCallback mCallback;

    public GoldListAdapter(Context context, List<Gift> datalist) {
        mContext = context;
        mDataList = datalist;
        mWidth = (mContext.getResources().getDisplayMetrics().widthPixels - ConversionHelper
                .dipToPx(30, mContext)) / 2;
        mOptions = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.defaultsmallimg)
                .showImageForEmptyUri(R.drawable.defaultsmallimg)
                .showImageOnFail(R.drawable.defaultsmallimg)
                .cacheInMemory(true).cacheOnDisk(true).considerExifParams(true)
                .bitmapConfig(Bitmap.Config.RGB_565).build();
    }

    public void setCurrentActivityRootView(View activityView) {
        mCurrentActivityView = activityView;
    }

    public void setDataSource(List<Gift> datalist) {
        mDataList = datalist;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        int res = 0;
        if (mDataList != null) {
            int count = mDataList.size();
            res = count / 2 + (count % 2 == 0 ? 0 : 1);
        }
        return res;
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder = null;
        if (convertView == null) {
            convertView = LayoutInflater.from(mContext).inflate(
                    R.layout.gold_list_item_lay, null);
            viewHolder = new ViewHolder();
            viewHolder.mGoodsTitleLay = convertView
                    .findViewById(R.id.gold_goods_txt_lay);
            viewHolder.mGoodsTitleLay2 = convertView
                    .findViewById(R.id.gold_goods_txt_lay2);
            viewHolder.mGoodsTitleTV = (TextView) convertView
                    .findViewById(R.id.goods_title);
            viewHolder.mGoodsBackFeesTV = (TextView) convertView
                    .findViewById(R.id.goods_backfees);
            viewHolder.mGoodsImageView = (ImageView) convertView
                    .findViewById(R.id.goods_image);
            viewHolder.mSlectedImageView1 = (RelativeLayout) convertView
                    .findViewById(R.id.goods_img_selected);
            final RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) viewHolder.mGoodsImageView
                    .getLayoutParams();
            lp.width = mWidth;
            lp.height = mWidth;

            final RelativeLayout.LayoutParams lp_selected = (RelativeLayout.LayoutParams) viewHolder.mSlectedImageView1
                    .getLayoutParams();
            lp_selected.width = mWidth;
            lp_selected.height = mWidth;

            viewHolder.mItemLay1 = (RelativeLayout) convertView
                    .findViewById(R.id.goods_item1);
            viewHolder.mGoodsTitleTV2 = (TextView) convertView
                    .findViewById(R.id.goods_title2);
            viewHolder.mGoodsBackFeesTV2 = (TextView) convertView
                    .findViewById(R.id.goods_backfees2);
            viewHolder.mGoodsImageView2 = (ImageView) convertView
                    .findViewById(R.id.goods_image2);
            viewHolder.mSlectedImageView2 = (RelativeLayout) convertView
                    .findViewById(R.id.goods_img_selected2);
            final RelativeLayout.LayoutParams lp2 = (RelativeLayout.LayoutParams) viewHolder.mGoodsImageView2
                    .getLayoutParams();
            lp2.width = mWidth;
            lp2.height = mWidth;


            final RelativeLayout.LayoutParams lp_selected2 = (RelativeLayout.LayoutParams) viewHolder.mSlectedImageView2
                    .getLayoutParams();
            lp_selected2.width = mWidth;
            lp_selected2.height = mWidth;

            viewHolder.mItemLay2 = (RelativeLayout) convertView
                    .findViewById(R.id.goods_item2);
            viewHolder.mGoodsImageView.setLayoutParams(lp);
            viewHolder.mGoodsImageView2.setLayoutParams(lp2);
            convertView.setTag(viewHolder);
        }
        viewHolder = (ViewHolder) convertView.getTag();
        if (2 * (1 + position) <= mDataList.size()) {
            // 意味着刚好是双数的data 集合
            viewHolder.mItemLay1.setVisibility(View.VISIBLE);
            viewHolder.mItemLay2.setVisibility(View.VISIBLE);
            viewHolder.mGoodsTitleLay.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    onItemClick(2 * position);
                }
            });
            viewHolder.mGoodsTitleLay2.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    onItemClick((2 * position) + 1);
                }
            });
            final RelativeLayout imageView1 = viewHolder.mSlectedImageView1;
            viewHolder.mGoodsImageView.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    onImageClick(2 * position, imageView1);
                }
            });
            final RelativeLayout imageView2 = viewHolder.mSlectedImageView2;
            viewHolder.mGoodsImageView2.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    onImageClick(2 * position + 1, imageView2);
                }
            });
            viewHolder.mSlectedImageView1.setVisibility(mDataList.get(2 * position).isSelected() ? View.VISIBLE : View.GONE);
            viewHolder.mSlectedImageView2.setVisibility(mDataList.get(2 * position + 1).isSelected() ? View.VISIBLE : View.GONE);

        } else {
            // 意味着肯定是单数的data 集合
            viewHolder.mItemLay1.setVisibility(View.VISIBLE);
            viewHolder.mItemLay2.setVisibility(View.INVISIBLE);
            viewHolder.mGoodsTitleLay.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    onItemClick(2 * position);
                }
            });
            final RelativeLayout imageView1 = viewHolder.mSlectedImageView1;

            viewHolder.mGoodsImageView.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    onImageClick(2 * position, imageView1);
                }
            });
            viewHolder.mSlectedImageView1.setVisibility(mDataList.get(2 * position).isSelected() ? View.VISIBLE : View.GONE);
            viewHolder.mSlectedImageView2.setVisibility(View.GONE);
            viewHolder.mGoodsTitleLay2.setOnClickListener(null);
        }
        // 初始化item1
        if (2 * position < mDataList.size()
                && mDataList.get(2 * position) != null) {
            ImageLoader.getInstance().displayImage(
                    mDataList.get(2 * position).getImgUrl(),
                    viewHolder.mGoodsImageView, mOptions);
            viewHolder.mGoodsTitleTV.setText(mDataList.get(2 * position).getGiftDesc());
            viewHolder.mGoodsBackFeesTV.setText(""
                    + (mDataList.get(2 * position).getFanliTaoPrice() == null ? 0
                    : mDataList.get(2 * position).getFanliTaoPrice()) + "金币");
            if (mDataList.get(2 * position).getFanliTaoPrice() == null
                    || mDataList.get(2 * position).getFanliTaoPrice() == 0) {
                viewHolder.mGoodsBackFeesTV.setVisibility(View.GONE);
            } else {
                viewHolder.mGoodsBackFeesTV.setVisibility(View.VISIBLE);
            }
        }
        // 初始化item2
        if (((2 * position) + 1) < mDataList.size()
                && mDataList.get(((2 * position) + 1)) != null) {
            ImageLoader.getInstance().displayImage(
                    mDataList.get(((2 * position) + 1)).getImgUrl(),
                    viewHolder.mGoodsImageView2, mOptions);
            viewHolder.mGoodsTitleTV2.setText(mDataList
                    .get(((2 * position) + 1)).getGiftDesc());
            if (mDataList.get((2 * position) + 1).getFanliTaoPrice() == null
                    || mDataList.get((2 * position) + 1).getFanliTaoPrice() == 0) {
                viewHolder.mGoodsBackFeesTV2.setVisibility(View.GONE);
            } else {
                viewHolder.mGoodsBackFeesTV2.setVisibility(View.VISIBLE);
            }
            viewHolder.mGoodsBackFeesTV2.setText(""
                    + (mDataList.get((2 * position) + 1).getFanliTaoPrice() == null ? 0
                    : mDataList.get((2 * position) + 1).getFanliTaoPrice()) + "金币");
        }
        return convertView;
    }

    public void onItemClick(int position) {
        // Log.d("onItemClick", ""+position);
        final Gift lGift = mDataList.get(position);
        if (!TextUtils.isEmpty(lGift.taobaoUrl)) {
            Utils.goHtmlActivity(
                    mContext,
                    lGift.getGiftDesc() == null ? "" : lGift
                            .getGiftDesc(),
                    lGift.taobaoUrl == null ? ""
                            : lGift.taobaoUrl);
        }
    }

    public interface RefreshCallback {

        public void refreshGift();
    }

    public void setRefreshCallback(RefreshCallback callback) {
        mCallback = callback;
    }

    public void onImageClick(final int position, final View imageView) {
        // Log.d("onItemClick", ""+position);
        final Gift item = mDataList.get(position);
        if (item != null) {
            if (item.isSelected()) {
                imageView.setVisibility(View.GONE);
            } else {
                imageView.setVisibility(View.VISIBLE);

            }
            item.setSelected(!item.isSelected());
            if (mCallback != null) {
                mCallback.refreshGift();
            }
        }
    }


    private class ViewHolder {

        public RelativeLayout mItemLay1;
        public RelativeLayout mSlectedImageView1;
        public ImageView mGoodsImageView;
        public TextView mGoodsTitleTV;
        public TextView mGoodsBackFeesTV;
        public RelativeLayout mItemLay2;
        public ImageView mGoodsImageView2;
        public TextView mGoodsTitleTV2;
        public RelativeLayout mSlectedImageView2;
        public TextView mGoodsBackFeesTV2;
        public View mGoodsTitleLay;
        public View mGoodsTitleLay2;

    }


}
