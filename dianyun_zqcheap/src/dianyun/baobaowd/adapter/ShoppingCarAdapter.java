package dianyun.baobaowd.adapter;

import java.io.Serializable;
import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import dianyun.baobaowd.data.Gift;
import dianyun.zqcheap.R;

public class ShoppingCarAdapter extends BaseAdapter
{

	private boolean downloading;
	private List<Gift> mDataList;
	private Context mContext;

	public ShoppingCarAdapter(List<Gift> pDataList, Context mContext)
	{
		super();
		this.mDataList = pDataList;
		this.mContext = mContext;
	}

	@Override
	public int getCount()
	{
		return mDataList.size();
	}

	@Override
	public Object getItem(int position)
	{
		return mDataList.get(position);
	}

	@Override
	public long getItemId(int position)
	{
		return position;
	}

	@Override
	public View getView(final int position, View view, ViewGroup parent)
	{
		final Gift lGift = mDataList.get(position);
		final HolderView lHolderView;
		if (null == view)
		{
			view = LayoutInflater.from(mContext).inflate(
					R.layout.shoppingcaradapter, null);
			lHolderView = new HolderView();
			lHolderView.setNameTv((TextView) view.findViewById(R.id.name_tv));
			lHolderView.setDesTv((TextView) view.findViewById(R.id.des_tv));
			lHolderView.setShippriceTv((TextView) view
					.findViewById(R.id.shipprice_tv));
			lHolderView.setPriceTv((TextView) view.findViewById(R.id.price_tv));
			lHolderView
					.setGoodsIv((ImageView) view.findViewById(R.id.goods_iv));
			lHolderView.setLeftIv((ImageView) view.findViewById(R.id.left_iv));
			lHolderView.setLeftLayout((RelativeLayout) view
					.findViewById(R.id.left_layout));
			view.setTag(lHolderView);
		}
		else
		{
			lHolderView = (HolderView) view.getTag();
		}
		if (lGift.isSelected())
		{
			lHolderView.getLeftIv().setImageResource(R.drawable.prize_selected);
		}
		else
		{
			lHolderView.getLeftIv().setImageResource(R.drawable.prize_normal);
		}
		return view;
	}

	public class HolderView implements Serializable
	{

		private static final long serialVersionUID = 1L;
		private RelativeLayout leftLayout;
		private ImageView leftIv;
		private ImageView goodsIv;
		private TextView nameTv;
		private TextView desTv;
		private TextView shippriceTv;
		private TextView priceTv;

		public RelativeLayout getLeftLayout()
		{
			return leftLayout;
		}

		public void setLeftLayout(RelativeLayout leftLayout)
		{
			this.leftLayout = leftLayout;
		}

		public ImageView getLeftIv()
		{
			return leftIv;
		}

		public void setLeftIv(ImageView leftIv)
		{
			this.leftIv = leftIv;
		}

		public ImageView getGoodsIv()
		{
			return goodsIv;
		}

		public void setGoodsIv(ImageView goodsIv)
		{
			this.goodsIv = goodsIv;
		}

		public TextView getNameTv()
		{
			return nameTv;
		}

		public void setNameTv(TextView nameTv)
		{
			this.nameTv = nameTv;
		}

		public TextView getDesTv()
		{
			return desTv;
		}

		public void setDesTv(TextView desTv)
		{
			this.desTv = desTv;
		}

		public TextView getShippriceTv()
		{
			return shippriceTv;
		}

		public void setShippriceTv(TextView shippriceTv)
		{
			this.shippriceTv = shippriceTv;
		}

		public TextView getPriceTv()
		{
			return priceTv;
		}

		public void setPriceTv(TextView priceTv)
		{
			this.priceTv = priceTv;
		}
	}
}
