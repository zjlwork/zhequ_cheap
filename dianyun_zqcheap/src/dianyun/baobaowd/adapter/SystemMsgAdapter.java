package dianyun.baobaowd.adapter;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;
import com.nostra13.universalimageloader.core.download.ImageDownloader.Scheme;

import dianyun.baobaowd.data.Message;
import dianyun.baobaowd.util.DateHelper;
import dianyun.zqcheap.R;

public class SystemMsgAdapter extends BaseAdapter
{

	private List<Message> mDataList;
	private Context mContext;
	DisplayImageOptions mOptions;
	public SystemMsgAdapter(List<Message> pDataList, Context mContext)
	{
		super();
		this.mDataList = pDataList;
		this.mContext = mContext;
		mOptions = new DisplayImageOptions.Builder()
		.showImageOnLoading(R.drawable.logo)
		.showImageForEmptyUri(R.drawable.logo)
		.showImageOnFail(R.drawable.logo)
		.cacheInMemory(true).cacheOnDisk(true)
		.considerExifParams(true).imageScaleType(ImageScaleType.EXACTLY_STRETCHED)
		.displayer(new RoundedBitmapDisplayer(360))
		.bitmapConfig(Bitmap.Config.RGB_565).build();
	}

	@Override
	public int getCount()
	{
		return mDataList.size();
	}

	@Override
	public Object getItem(int position)
	{
		return mDataList.get(position);
	}

	@Override
	public long getItemId(int position)
	{
		return position;
	}

	@Override
	public View getView(final int position, View view, ViewGroup parent)
	{
		final Message lMessage = mDataList.get(position);
		final HolderView lHolderView;
		if (null == view)
		{
			view = LayoutInflater.from(mContext).inflate(
					R.layout.systemmsgadapter, null);
			lHolderView = new HolderView();
			lHolderView.setValueTv((TextView) view.findViewById(R.id.value_tv));
			lHolderView.setAvatarIv((ImageView) view
					.findViewById(R.id.avatar_iv));
			lHolderView.setTimeTv((TextView) view.findViewById(R.id.time_tv));
			view.setTag(lHolderView);
		}
		else
		{
			lHolderView = (HolderView) view.getTag();
		}
		lHolderView.getValueTv().setText(lMessage.getContentObject());
	
		
	
		
		ImageLoader.getInstance().displayImage(
				Scheme.DRAWABLE.wrap("" + R.drawable.logo),
				lHolderView.getAvatarIv(),mOptions);
		
		Date date = new Date(lMessage.getTimestamp());
		lHolderView.getTimeTv().setText(
				DateHelper.getRoleTime(DateHelper.getTextByDate(date,
						DateHelper.YYYY_MM_DD_HH_MM_SS)));
		return view;
	}

	public class HolderView implements Serializable
	{

		private static final long serialVersionUID = 1L;
		private ImageView avatarIv;
		private TextView valueTv;
		private TextView timeTv;

		public ImageView getAvatarIv()
		{
			return avatarIv;
		}

		public void setAvatarIv(ImageView avatarIv)
		{
			this.avatarIv = avatarIv;
		}

		public TextView getValueTv()
		{
			return valueTv;
		}

		public void setValueTv(TextView valueTv)
		{
			this.valueTv = valueTv;
		}

		public TextView getTimeTv()
		{
			return timeTv;
		}

		public void setTimeTv(TextView timeTv)
		{
			this.timeTv = timeTv;
		}
	}
}
