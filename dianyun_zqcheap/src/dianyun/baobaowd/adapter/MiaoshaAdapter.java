package dianyun.baobaowd.adapter;

import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.nostra13.universalimageloader.core.ImageLoader;

import dianyun.baobaowd.defineview.CountDownTimeView;
import dianyun.baobaowd.defineview.CountDownTimeView.EndCallBack;
import dianyun.baobaowd.dto.ResultDTO;
import dianyun.baobaowd.entity.CateItem;
import dianyun.baobaowd.entity.SecKill;
import dianyun.baobaowd.help.ShopHttpHelper;
import dianyun.baobaowd.help.ShopHttpHelper.MiaoShaCallback;
import dianyun.baobaowd.util.DateHelper;
import dianyun.baobaowd.util.TaeSdkUtil;
import dianyun.baobaowd.util.ToastHelper;
import dianyun.baobaowd.util.UserHelper;
import dianyun.zqcheap.R;
import dianyun.zqcheap.application.BaoBaoWDApplication;

public class MiaoshaAdapter extends BaseAdapter {
	private Context mContext;
	private List<SecKill> mList;

	public MiaoshaAdapter(Context context, List<SecKill> mSecKillList) {
		this.mContext = context;
		this.mList = mSecKillList;
		// new CountDownTimerThread().start();
	}

	@Override
	public int getCount() {
		return mList.size();
	}

	@Override
	public Object getItem(int position) {
		return mList.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		final SecKill mItem = mList.get(position);
		ViewHolder mHolder = null;
		if (convertView == null) {
			convertView = LayoutInflater.from(mContext).inflate(
					R.layout.miaoshaadapter, null);
			mHolder = new ViewHolder(convertView);
			convertView.setTag(mHolder);
		} else {
			mHolder = (ViewHolder) convertView.getTag();
		}
		if (mItem == null) {
			return convertView;
		}
		 System.out.println("getView   mSecKill=="+mItem.startDate+"position="+position+"   "+mItem.isStart+"  "+mItem.startLeftTime+"  "+mItem.endLeftTime);
		try {
			if (mItem.isStart) {
				mHolder.btime_layout
						.setBackgroundResource(R.drawable.ms_begintitle);
				mHolder.bthint_tv.setText(mContext
						.getString(R.string.miaosha_qgz));
				
				mHolder.ethint_tv.setText(mContext
						.getString(R.string.miaosha_endtime_hint));

			} else {
				mHolder.btime_layout
						.setBackgroundResource(R.drawable.ms_normaltitle);
				mHolder.bthint_tv.setText(mContext
						.getString(R.string.miaosha_wks));
				mHolder.ethint_tv.setText(mContext
						.getString(R.string.miaosha_begintime_hint));

			}
//			sethms(mItem, mHolder.hour_tv, mHolder.minute_tv, mHolder.sec_tv);
			// new CountDownTimerThread(mItem, mHolder.hour_tv,
			// mHolder.minute_tv, mHolder.sec_tv).start();
			
			mHolder.counttime_view.initData(mItem);
			mHolder.counttime_view.setEndCallBack(new EndCallBack() {
				
				@Override
				public void end(SecKill pSecKill) {
					mList.remove(pSecKill);
					notifyDataSetChanged();
				}
			});
			
			String startDate = DateHelper.getDateText(mItem.startDate,
					DateHelper.YYYY_MM_DD_HH_MM_SS, DateHelper.HH_MM);
			mHolder.begintime_tv.setText(startDate);
			mHolder.part2_layout.removeAllViewsInLayout();
			addPart2(mHolder.part2_layout, mItem);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return convertView;
	}

	private void sethms(SecKill secKill, TextView hour_tv, TextView minute_tv,
			TextView sec_tv) {
		long time = 0L;
		if (secKill.isStart) {
			time = secKill.endLeftTime;
		} else {
			time = secKill.startLeftTime;
		}
		int hour = geth(time);
		int minute = getm(time % (60 * 60 ));
		int second = gets(time % 60 );
		hour_tv.setText(getStringInt(hour));
		minute_tv.setText(getStringInt(minute));
		sec_tv.setText(getStringInt(second));
	}

	private String getStringInt(int temp) {
		if (temp >= 10)
			return String.valueOf(temp);
		else
			return "0" + temp;
	}

	private int geth(long ys) {
		int hour = 0;
		if (ys >  60 * 60) {
			hour = (int) (ys / ( 60 * 60));
		}
		return hour;
	}

	private int getm(long ys) {
		int minute = 0;
		if (ys > 60 ) {
			minute = (int) (ys / 60 );
		}
		return minute;

	}

	private int gets(long ys) {
		int second = (int) ys;
//		if (ys > 1000) {
//			second = (int) (ys / 1000);
//		}
		return second;

	}

	private void addPart2(LinearLayout part2_layout, final SecKill lSecKill) {
		List<CateItem> cateItemList = lSecKill.itemList;
		if (cateItemList != null && cateItemList.size() > 0) {
			for (int i = 0; i < cateItemList.size(); i++) {
				final CateItem mItem = cateItemList.get(i);
				View child = LayoutInflater.from(mContext).inflate(
						R.layout.miaosha_listitem, null);
				RelativeLayout item_layout = (RelativeLayout) child
						.findViewById(R.id.item_layout);
				ImageView goods_image = (ImageView) child
						.findViewById(R.id.goods_image);
				TextView goods_title_tv = (TextView) child
						.findViewById(R.id.goods_title_tv);
				TextView goods_detail_tv = (TextView) child
						.findViewById(R.id.goods_detail_tv);
				TextView goods_backfeesPrice = (TextView) child
						.findViewById(R.id.goods_backfeesPrice);
				TextView goods_currentPrice = (TextView) child
						.findViewById(R.id.goods_currentPrice);
				final Button shop_product_item_go_buy = (Button) child
						.findViewById(R.id.shop_product_item_go_buy);
				ImageView tianmo_iv = (ImageView) child
						.findViewById(R.id.tianmo_iv);
				if (mItem.itemType != null
						&& mItem.itemType == CateItem.ITEMTYPE_STORE)
					tianmo_iv.setVisibility(View.VISIBLE);
				else
					tianmo_iv.setVisibility(View.GONE);
				if (lSecKill.isStart) {

					if (mItem.amount == 0) {
						shop_product_item_go_buy.setSelected(true);
						shop_product_item_go_buy.setEnabled(false);
						shop_product_item_go_buy.setText(mContext
								.getString(R.string.miaosha_yqw));

					} else {
						shop_product_item_go_buy.setSelected(false);
						shop_product_item_go_buy.setEnabled(true);
						shop_product_item_go_buy.setText(mContext
								.getString(R.string.miaosha_ljq));
					}

				} else {
					shop_product_item_go_buy.setSelected(true);
					shop_product_item_go_buy.setEnabled(false);
					shop_product_item_go_buy.setText(mContext
							.getString(R.string.miaosha_wks));
				}
				
				
				

				shop_product_item_go_buy
						.setOnClickListener(new OnClickListener() {

							@Override
							public void onClick(View v) {

								if (UserHelper.isGusetUser(mContext)) {
									UserHelper.gusestUserGo(mContext);
									return;
								}

								
								if(mItem.type==CateItem.TYPE_ZKMS){
									TaeSdkUtil.showTAEItemDetail((Activity) mContext,
											mItem.tbItemId, mItem.taobaoPid, mItem.isTk == 1,
											mItem.itemType, null,null);
								}else{
									shop_product_item_go_buy.setEnabled(false);
									ShopHttpHelper.miaoSha(mContext, true,
											UserHelper.getUser().getUid(),
											mItem.timeId, new MiaoShaCallback() {

												@Override
												public void result(ResultDTO result) {
													shop_product_item_go_buy.setEnabled(true);
													if(result!=null){
										    			if(result.getCode().equals("0")){
										    				Toast.makeText(mContext, result.getResult(), Toast.LENGTH_SHORT).show();
										    				TaeSdkUtil.showTAEItemDetail((Activity) mContext,
																	mItem.tbItemId, mItem.taobaoPid, mItem.isTk == 1,
																	mItem.itemType, null,null);
										    			}else {
										    				Toast.makeText(mContext, result.getErrorMsg(), Toast.LENGTH_SHORT).show();
										    				if(result.getCode().equals("-510")){
										    					mItem.amount =0;
										    					notifyDataSetChanged();
										    				}
										    			}
										    		}else{
										    			Toast.makeText(mContext, mContext.getString(R.string.requestfailed), Toast.LENGTH_SHORT).show();
										    		}
												}
											});
									
								}
					
							}
						});
				item_layout
				.setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View v) {
						
						 String status="";
						if (lSecKill.isStart) {
							if (mItem.amount == 0) 
								status = mContext.getString(R.string.miaosha_status_qgl);
							 else 
								status = mContext.getString(R.string.miaosha_status_jxz);
						} else {
							status = mContext
									.getString(R.string.miaosha_status_wks);
						}
						ToastHelper.showMaioShaItemDetailDialog(mContext, mItem, status);
					}
				});

				ImageLoader.getInstance().displayImage(
						mItem.pics == null ? "" : mItem.pics.get(0),
						goods_image, BaoBaoWDApplication.mOptions);
				goods_title_tv.setText(mItem.title);
				
				if (lSecKill.isStart) {
					goods_detail_tv.setText(mContext.getString(R.string.miaosha_begin_hint));
				} else {
					goods_detail_tv.setText(String.format(mContext.getString(R.string.miaosha_notbegin_hint), mItem.amount));
				}
				
				
				goods_backfeesPrice.setText(String.valueOf((float)mItem.coins/100));
				goods_currentPrice.setText(String.valueOf(mItem.umpPrice));
				part2_layout.addView(child);
			}

		}
	}

	private class ViewHolder {

		public TextView begintime_tv;
//		public TextView hour_tv;
//		public TextView minute_tv;
//		public TextView sec_tv;
		public LinearLayout part2_layout;
		public RelativeLayout btime_layout;
		public TextView bthint_tv;
		public TextView ethint_tv;
		public CountDownTimeView counttime_view;

		public ViewHolder(View view) {

			begintime_tv = (TextView) view.findViewById(R.id.begintime_tv);
//			hour_tv = (TextView) view.findViewById(R.id.hour_tv);
//			minute_tv = (TextView) view.findViewById(R.id.minute_tv);
//			sec_tv = (TextView) view.findViewById(R.id.sec_tv);
			part2_layout = (LinearLayout) view.findViewById(R.id.part2_layout);
			btime_layout = (RelativeLayout) view
					.findViewById(R.id.btime_layout);
			bthint_tv = (TextView) view.findViewById(R.id.bthint_tv);
			ethint_tv = (TextView) view.findViewById(R.id.ethint_tv);
			counttime_view = (CountDownTimeView) view.findViewById(R.id.counttime_view);

		}

	}

	// class CountDownTimerThread extends Thread {
	//
	// private Handler handler;
	// public CountDownTimerThread() {
	// handler = new Handler();
	// }
	//
	// @Override
	// public void run() {
	// while (true) {
	// try {
	// Thread.sleep(1000);
	//
	// for(int i=0;i<mList.size();i++){
	// SecKill lSecKill =mList.get(i);
	// long startLeftTime = lSecKill.startLeftTime;
	// long endLeftTime = lSecKill.endLeftTime;
	// if(startLeftTime>1000){
	// lSecKill.startLeftTime-=1000;
	// }else{
	// if(!lSecKill.isStart)
	// lSecKill.isStart=true;
	// }
	// if(endLeftTime>1000){
	// lSecKill.endLeftTime-=1000;
	// }
	// }
	//
	// } catch (InterruptedException e) {
	// e.printStackTrace();
	// }
	// handler.post(new Runnable() {
	//
	// @Override
	// public void run() {
	// notifyDataSetChanged();
	// }
	// });
	// }
	// }
	// }

	// class CountDownTimerThread extends Thread {
	//
	// private Handler handler;
	// private long time ;
	// SecKill secKill;
	// public TextView hour_tv;
	// public TextView minute_tv;
	// public TextView sec_tv;
	//
	// public CountDownTimerThread(SecKill secKill,TextView hour_tv,TextView
	// minute_tv,TextView sec_tv) {
	// handler = new Handler();
	// this.hour_tv = hour_tv;
	// this.minute_tv = minute_tv;
	// this.sec_tv = sec_tv;
	// this.secKill = secKill;
	// if(secKill.isStart){
	// time= secKill.endLeftTime;
	// }else{
	// time= secKill.startLeftTime;
	// }
	// sethms(secKill, hour_tv, minute_tv, sec_tv);
	// }
	//
	// @Override
	// public void run() {
	// while (time > 0) {
	// try {
	// Thread.sleep(1000);
	// time=time-1000;
	// } catch (InterruptedException e) {
	// // TODO Auto-generated catch block
	// e.printStackTrace();
	// }
	// handler.post(new Runnable() {
	//
	// @Override
	// public void run() {
	// if (time == 0) {
	//
	// if(!secKill.isStart)
	// secKill.isStart= true;
	// notifyDataSetChanged();
	// }else{
	// if(secKill.isStart){
	// secKill.endLeftTime = time;
	// }else{
	// secKill.startLeftTime = time;
	// }
	// sethms(secKill, hour_tv, minute_tv, sec_tv);
	// }
	//
	// }
	// });
	// }
	// }
	// }

}
