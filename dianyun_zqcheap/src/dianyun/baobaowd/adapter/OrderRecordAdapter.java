package dianyun.baobaowd.adapter;

import java.io.Serializable;
import java.util.List;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Paint;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;

import dianyun.baobaowd.data.Order;
import dianyun.baobaowd.util.ConversionHelper;
import dianyun.zqcheap.R;
import dianyun.zqcheap.activity.OrderRecordActivity;

public class OrderRecordAdapter extends BaseAdapter {

	private List<Order> mDataList;
	private Context mContext;
	DisplayImageOptions mOptions;
	private boolean rebateStatus;

	public OrderRecordAdapter(List<Order> pDataList, Context mContext) {
		super();
		this.mDataList = pDataList;
		this.mContext = mContext;
		mOptions = new DisplayImageOptions.Builder()
				.showImageOnLoading(R.drawable.defaultsmallimg)
				.showImageForEmptyUri(R.drawable.defaultsmallimg)
				.showImageOnFail(R.drawable.defaultsmallimg).cacheInMemory(true)
				.cacheOnDisk(true).considerExifParams(true)
				.imageScaleType(ImageScaleType.EXACTLY_STRETCHED)
				.bitmapConfig(Bitmap.Config.RGB_565).build();
	}

	public boolean isRebateStatus() {
		return rebateStatus;
	}

	public void setRebateStatus(boolean rebateStatus) {
		this.rebateStatus = rebateStatus;
	}

	@Override
	public int getCount() {
		return mDataList.size();
	}

	@Override
	public Object getItem(int position) {
		return mDataList.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(final int position, View view, ViewGroup parent) {
		final Order lOrder = mDataList.get(position);
		final HolderView lHolderView;
		if (null == view) {
			view = LayoutInflater.from(mContext).inflate(
					R.layout.orderrecordadapter, null);
			lHolderView = new HolderView();
			lHolderView
					.setGoodsIv((ImageView) view.findViewById(R.id.goods_iv));
			lHolderView.setRebatestatus_tv((TextView) view
					.findViewById(R.id.rebatestatus_tv));
			lHolderView.setRebategold_tv((TextView) view
					.findViewById(R.id.rebategold_tv));
			lHolderView.setDesTv((TextView) view.findViewById(R.id.des_tv));
			lHolderView.setPriceTv((TextView) view.findViewById(R.id.price_tv));
			lHolderView.setOldpriceTv((TextView) view
					.findViewById(R.id.oldprice_tv));
			lHolderView.setPercent_tv((TextView) view
					.findViewById(R.id.percent_tv));
			lHolderView.setTime_tv((TextView) view.findViewById(R.id.time_tv));
			lHolderView.setCountdown_layout((RelativeLayout) view
					.findViewById(R.id.countdown_layout));
			lHolderView.setProgress_v(view.findViewById(R.id.progress_v));
			lHolderView.setCount_tv((TextView)view.findViewById(R.id.count_tv));
			lHolderView.setOrdershare_layout((RelativeLayout)view.findViewById(R.id.ordershare_layout));

			view.setTag(lHolderView);
		} else {
			lHolderView = (HolderView) view.getTag();
		}
		

		lHolderView.getDesTv().setText(lOrder.getTbItemTitle());
		if (!TextUtils.isEmpty(lOrder.getItemUpmPrice()))
			lHolderView.getPriceTv().setText("" + lOrder.getItemUpmPrice());
		if (!TextUtils.isEmpty(lOrder.getItemPrice()))
			lHolderView.getOldpriceTv().setText("" + lOrder.getItemPrice());
		lHolderView.getPriceTv().getPaint()
		.setFakeBoldText(true);
		
//		textView.setTypeface(Typeface.defaultFromStyle(Typeface.BOLD));//加粗
//
//		textView.getPaint().setFakeBoldText(true);//加粗
		lHolderView.getOldpriceTv().getPaint()
				.setFlags(Paint.STRIKE_THRU_TEXT_FLAG);
		lHolderView.getRebategold_tv().setText(
				String.format(mContext.getString(R.string.returngold),
						lOrder.getItemCoins()));

		if (!TextUtils.isEmpty(lOrder.getPicUrl())) {
			ImageLoader.getInstance().displayImage(lOrder.getPicUrl(),
					lHolderView.getGoodsIv(), mOptions);
		}

		if (!rebateStatus) {
			lHolderView.getCountdown_layout().setVisibility(View.VISIBLE);
			lHolderView.getRebatestatus_tv().setVisibility(View.GONE);
			lHolderView.getOrdershare_layout().setVisibility(View.VISIBLE);
		} else {
			lHolderView.getOrdershare_layout().setVisibility(View.VISIBLE);
			if(lOrder.getSendStatus()==Order.SENDSTATUS_REBATESUCCESS)
				lHolderView.getRebatestatus_tv().setVisibility(View.GONE);
			else if(lOrder.getSendStatus()==Order.SENDSTATUS_AUDITING)
				lHolderView.getRebatestatus_tv().setVisibility(View.VISIBLE);
			lHolderView.getCountdown_layout().setVisibility(View.GONE);
		}
		String countdowntime ="";
		if(lOrder.getOrderStatusCode()==Order.ORDERSTATUSCODE_NOTRECEIVE){
			countdowntime = mContext.getString(R.string.rebate_time2);
		}else if(lOrder.getOrderStatusCode()==Order.ORDERSTATUSCODE_RECEIVE){
			countdowntime = String.format(
					mContext.getString(R.string.rebate_time),
					lOrder.getYcoinsReturnDays());
		}
		
		lHolderView.getTime_tv().setText(countdowntime);
		lHolderView.getPercent_tv().setText(lOrder.getYcoinsReturnPer() + "%");
		lHolderView.getProgress_v().getLayoutParams().width = getProgressPercent(lOrder
				.getYcoinsReturnPer());
		
		
		if(lOrder.getItemAmount()>1){
			lHolderView.getCount_tv().setVisibility(View.VISIBLE);
			lHolderView.getCount_tv().setText("X"+lOrder.getItemAmount());
		}else{
			lHolderView.getCount_tv().setVisibility(View.GONE);
		}
		lHolderView.getOrdershare_layout().setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				((OrderRecordActivity)mContext).handleOnShareOnClick((float)lOrder.getItemCoins()/100,lOrder.getSeqId(),rebateStatus);
			}
		});

		return view;

	}

	private int getProgressPercent(int per) {
		int width = ConversionHelper.dipToPx(100, mContext);
		width = width * per / 100;
		return width;
	}

	public class HolderView implements Serializable {

		private static final long serialVersionUID = 1L;
		private ImageView goodsIv;
		private TextView desTv;
		private TextView priceTv;
		private TextView oldpriceTv;
		private TextView percent_tv;
		private TextView time_tv;
		private TextView rebategold_tv;
		private TextView count_tv;
		private TextView rebatestatus_tv;
		private RelativeLayout countdown_layout;
		private RelativeLayout ordershare_layout;
		private View progress_v;

		
		
		
		public RelativeLayout getOrdershare_layout() {
			return ordershare_layout;
		}

		public void setOrdershare_layout(RelativeLayout ordershare_layout) {
			this.ordershare_layout = ordershare_layout;
		}

		public TextView getCount_tv() {
			return count_tv;
		}

		public void setCount_tv(TextView count_tv) {
			this.count_tv = count_tv;
		}

		public TextView getPercent_tv() {
			return percent_tv;
		}

		public void setPercent_tv(TextView percent_tv) {
			this.percent_tv = percent_tv;
		}

		public TextView getTime_tv() {
			return time_tv;
		}

		public void setTime_tv(TextView time_tv) {
			this.time_tv = time_tv;
		}

		public RelativeLayout getCountdown_layout() {
			return countdown_layout;
		}

		public void setCountdown_layout(RelativeLayout countdown_layout) {
			this.countdown_layout = countdown_layout;
		}

		public View getProgress_v() {
			return progress_v;
		}

		public void setProgress_v(View progress_v) {
			this.progress_v = progress_v;
		}

		public ImageView getGoodsIv() {
			return goodsIv;
		}

		public void setGoodsIv(ImageView goodsIv) {
			this.goodsIv = goodsIv;
		}

		public TextView getRebategold_tv() {
			return rebategold_tv;
		}

		public void setRebategold_tv(TextView rebategold_tv) {
			this.rebategold_tv = rebategold_tv;
		}

		public TextView getRebatestatus_tv() {
			return rebatestatus_tv;
		}

		public void setRebatestatus_tv(TextView rebatestatus_tv) {
			this.rebatestatus_tv = rebatestatus_tv;
		}

		public TextView getDesTv() {
			return desTv;
		}

		public void setDesTv(TextView desTv) {
			this.desTv = desTv;
		}

		public TextView getPriceTv() {
			return priceTv;
		}

		public void setPriceTv(TextView priceTv) {
			this.priceTv = priceTv;
		}

		public TextView getOldpriceTv() {
			return oldpriceTv;
		}

		public void setOldpriceTv(TextView oldpriceTv) {
			this.oldpriceTv = oldpriceTv;
		}
	}
}
