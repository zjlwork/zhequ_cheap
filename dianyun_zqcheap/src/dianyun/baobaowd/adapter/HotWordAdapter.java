package dianyun.baobaowd.adapter;

import java.io.Serializable;
import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import dianyun.baobaowd.data.HotWord;
import dianyun.zqcheap.R;

public class HotWordAdapter extends BaseAdapter {

	private List<HotWord> mHotWordList;
	private Context mContext;

	public HotWordAdapter(Context mContext, List<HotWord> mHotWordList) {
		super();
		this.mContext = mContext;
		this.mHotWordList = mHotWordList;
	}

	@Override
	public int getCount() {
		return mHotWordList.size();
	}

	@Override
	public Object getItem(int position) {
		return mHotWordList.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(final int position, View view, ViewGroup parent) {
		final HotWord hotWord = mHotWordList.get(position);
		final HolderView lHolderView;
		if (null == view) {
			view = LayoutInflater.from(mContext).inflate(
					R.layout.hotwordadapter, null);
			lHolderView = new HolderView();
			lHolderView.setWordTv((TextView) view.findViewById(R.id.word_tv));
			view.setTag(lHolderView);
		} else {
			lHolderView = (HolderView) view.getTag();
		}
		lHolderView.getWordTv().setText(hotWord.getWord());

		return view;
	}

	public class HolderView implements Serializable {

		private static final long serialVersionUID = 1L;
		private TextView wordTv;

		public TextView getWordTv() {
			return wordTv;
		}

		public void setWordTv(TextView wordTv) {
			this.wordTv = wordTv;
		}

	}

}
