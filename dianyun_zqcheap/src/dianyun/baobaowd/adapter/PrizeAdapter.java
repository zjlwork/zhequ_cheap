package dianyun.baobaowd.adapter;

import java.io.Serializable;
import java.util.List;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

import dianyun.baobaowd.data.Gift;
import dianyun.baobaowd.util.Utils;
import dianyun.zqcheap.R;

public class PrizeAdapter extends BaseAdapter
{

	private boolean downloading;
	private List<Gift> mDataList;
	private Context mContext;
	private Bitmap mBitmap;
	private DisplayImageOptions mOptions;
	private RefreshCallback mCallback;

	public PrizeAdapter(List<Gift> pDataList, Context mContext)
	{
		super();
		this.mDataList = pDataList;
		this.mContext = mContext;
		mBitmap = BitmapFactory.decodeResource(mContext.getResources(),
				R.drawable.defaultsmallimg);
		mOptions = new DisplayImageOptions.Builder()
				.showImageOnLoading(R.drawable.defaultsmallimg)
				.showImageForEmptyUri(R.drawable.defaultsmallimg)
				.showImageOnFail(R.drawable.defaultsmallimg)
				.cacheInMemory(true).cacheOnDisk(true).considerExifParams(true)
				.bitmapConfig(Bitmap.Config.RGB_565).build();
	}

	public interface RefreshCallback
	{

		public void refreshGift();
	}

	public void setRefreshCallback(RefreshCallback callback)
	{
		mCallback = callback;
	}

	@Override
	public int getCount()
	{
		return mDataList.size();
	}

	@Override
	public Object getItem(int position)
	{
		return mDataList.get(position);
	}

	@Override
	public long getItemId(int position)
	{
		return position;
	}

	@Override
	public View getView(final int position, View view, ViewGroup parent)
	{
		final Gift lGift = mDataList.get(position);
		final HolderView lHolderView;
		if (null == view)
		{
			view = LayoutInflater.from(mContext).inflate(R.layout.prizeadapter,
					null);
			lHolderView = new HolderView();
			lHolderView.setNeedGoldTv((TextView) view
					.findViewById(R.id.needgold_tv));
			lHolderView.setSubjectTv((TextView) view
					.findViewById(R.id.subject_tv));
			lHolderView.setGiftIv((ImageView) view.findViewById(R.id.gift_iv));
			lHolderView.setLeftIv((ImageView) view.findViewById(R.id.left_iv));
			lHolderView.mGift_ImageView_Lay = (RelativeLayout) view
					.findViewById(R.id.gift_iv_lay);
			lHolderView.mGift_lookdetail_Lay = (LinearLayout) view
					.findViewById(R.id.gift_look_detail_lay);
			lHolderView.setLeftLayout((RelativeLayout) view
					.findViewById(R.id.left_layout));
			view.setTag(lHolderView);
		}
		else
		{
			lHolderView = (HolderView) view.getTag();
		}
		if (TextUtils.isEmpty(lGift.taobaoUrl))
		{
			lHolderView.mGift_lookdetail_Lay.setVisibility(View.INVISIBLE);
		}
		else
		{
			lHolderView.mGift_lookdetail_Lay.setVisibility(View.VISIBLE);
		}
		lHolderView.getSubjectTv().setText(lGift.getGiftDesc());
		lHolderView.getNeedGoldTv().setText("" + lGift.getPrice());
		lHolderView.mGift_ImageView_Lay
				.setOnClickListener(new OnClickListener()
				{

					@Override
					public void onClick(View v)
					{
						// 跳转详情
						if (!TextUtils.isEmpty(lGift.taobaoUrl))
						{
							Utils.goHtmlActivity(
									mContext,
									lGift.getGiftDesc() == null ? "" : lGift
											.getGiftDesc(),
									lGift.taobaoUrl == null ? ""
											: lGift.taobaoUrl);
						}
					}
				});
		if (lGift.getImgUrl() != null && !lGift.getImgUrl().equals(""))
		{
			lHolderView.getGiftIv().setImageResource(R.drawable.defaultsmallimg);
			ImageLoader.getInstance().displayImage(lGift.getImgUrl(),
					lHolderView.getGiftIv(), mOptions);
		}
		else
		{
			lHolderView.getGiftIv().setImageResource(R.drawable.defaultsmallimg);
		}
		if (lGift.isSelected())
		{
			lHolderView.getLeftIv().setImageResource(R.drawable.prize_selected);
		}
		else
		{
			lHolderView.getLeftIv().setImageResource(R.drawable.prize_normal);
		}
		return view;
	}

	public class HolderView implements Serializable
	{

		private static final long serialVersionUID = 1L;
		private TextView subjectTv;
		private TextView needGoldTv;
		private RelativeLayout leftLayout;
		private ImageView leftIv;
		private ImageView giftIv;
		public RelativeLayout mGift_ImageView_Lay;
		public LinearLayout mGift_lookdetail_Lay;

		public RelativeLayout getLeftLayout()
		{
			return leftLayout;
		}

		public void setLeftLayout(RelativeLayout leftLayout)
		{
			this.leftLayout = leftLayout;
		}

		public TextView getSubjectTv()
		{
			return subjectTv;
		}

		public void setSubjectTv(TextView subjectTv)
		{
			this.subjectTv = subjectTv;
		}

		public TextView getNeedGoldTv()
		{
			return needGoldTv;
		}

		public void setNeedGoldTv(TextView needGoldTv)
		{
			this.needGoldTv = needGoldTv;
		}

		public ImageView getLeftIv()
		{
			return leftIv;
		}

		public void setLeftIv(ImageView leftIv)
		{
			this.leftIv = leftIv;
		}

		public ImageView getGiftIv()
		{
			return giftIv;
		}

		public void setGiftIv(ImageView giftIv)
		{
			this.giftIv = giftIv;
		}
	}
}
