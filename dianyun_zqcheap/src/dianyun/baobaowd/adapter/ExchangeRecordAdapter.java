package dianyun.baobaowd.adapter;

import java.io.Serializable;
import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import dianyun.baobaowd.data.YcoinPay;
import dianyun.zqcheap.R;

public class ExchangeRecordAdapter extends BaseAdapter
{

	private List<YcoinPay> mDataList;
	private Context mContext;

	public ExchangeRecordAdapter(List<YcoinPay> pDataList, Context mContext)
	{
		super();
		this.mDataList = pDataList;
		this.mContext = mContext;
	}

	@Override
	public int getCount()
	{
		return mDataList.size();
	}

	@Override
	public Object getItem(int position)
	{
		return mDataList.get(position);
	}

	@Override
	public long getItemId(int position)
	{
		return position;
	}

	@Override
	public View getView(final int position, View view, ViewGroup parent)
	{
		final YcoinPay lYcoinPay = mDataList.get(position);
		final HolderView lHolderView;
		if (null == view)
		{
			view = LayoutInflater.from(mContext).inflate(R.layout.exchangerecordadapter,
					null);
			lHolderView = new HolderView();
			lHolderView.setExcountTv((TextView) view.findViewById(R.id.excount_tv));
			lHolderView.setUnitTv((TextView) view.findViewById(R.id.unit_tv));
			lHolderView.setStatusTv((TextView) view.findViewById(R.id.status_tv));
			lHolderView.setDesTv((TextView) view.findViewById(R.id.des_tv));
			lHolderView.setUnitTv((TextView) view.findViewById(R.id.unit_tv));
		
			view.setTag(lHolderView);
		}
		else
		{
			lHolderView = (HolderView) view.getTag();
		}
		
		
		
		
		if(lYcoinPay.getPayType()==YcoinPay.PAYTYPE_MONDY){
			lHolderView.getExcountTv().setText(String.valueOf(lYcoinPay.getPayAmount()/*/100*/));
//			lHolderView.getUnitTv().setText(mContext.getString(R.string.unit_yuan));
			if(lYcoinPay.getPayStatus()==YcoinPay.PAYSTAUTS_FAILED){
				lHolderView.getStatusTv().setText(mContext.getString(R.string.paystatus_moneyfailed));
			}else if(lYcoinPay.getPayStatus()==YcoinPay.PAYSTAUTS_AUDIT){
				lHolderView.getStatusTv().setText(mContext.getString(R.string.paystatus_audit));
			}else if(lYcoinPay.getPayStatus()==YcoinPay.PAYSTAUTS_SUCCESS){
				lHolderView.getStatusTv().setText(mContext.getString(R.string.paystatus_moneysuccess));
			}
		}/*else if(lYcoinPay.getPayType()==YcoinPay.PAYTYPE_GOLD){
			lHolderView.getExcountTv().setText(String.valueOf(lYcoinPay.getPayAmount()));
			lHolderView.getUnitTv().setText(mContext.getString(R.string.unit_gold));
			if(lYcoinPay.getPayStatus()==YcoinPay.PAYSTAUTS_FAILED){
				lHolderView.getStatusTv().setText(mContext.getString(R.string.paystatus_goldfailed));
			}else if(lYcoinPay.getPayStatus()==YcoinPay.PAYSTAUTS_AUDIT){
				lHolderView.getStatusTv().setText(mContext.getString(R.string.paystatus_audit));
			}else if(lYcoinPay.getPayStatus()==YcoinPay.PAYSTAUTS_SUCCESS){
				lHolderView.getStatusTv().setText(mContext.getString(R.string.paystatus_goldsuccess));
			}
		}*/
		lHolderView.getDesTv().setText(lYcoinPay.getCreateTime());
		
		
		
		if(lYcoinPay.getPayStatus()==YcoinPay.PAYSTAUTS_FAILED){
			lHolderView.getStatusTv().setTextColor(mContext.getResources().getColor(R.color.paystatus_failed));
		}else if(lYcoinPay.getPayStatus()==YcoinPay.PAYSTAUTS_AUDIT){
			lHolderView.getStatusTv().setTextColor(mContext.getResources().getColor(R.color.paystatus_audit));
		}else if(lYcoinPay.getPayStatus()==YcoinPay.PAYSTAUTS_SUCCESS){
			lHolderView.getStatusTv().setTextColor(mContext.getResources().getColor(R.color.paystatus_success));
		}
		
		
	
		
		return view;
	}

	
	
	
	
	public class HolderView implements Serializable
	{

		private static final long serialVersionUID = 1L;
		private TextView excountTv;
		private TextView unitTv;
		private TextView statusTv;
		private TextView desTv;
		
		
		
		
	

		public TextView getExcountTv()
		{
			return excountTv;
		}
		
		public void setExcountTv(TextView excountTv)
		{
			this.excountTv = excountTv;
		}
		
		public TextView getUnitTv()
		{
			return unitTv;
		}
		
		public void setUnitTv(TextView unitTv)
		{
			this.unitTv = unitTv;
		}
		
		public TextView getStatusTv()
		{
			return statusTv;
		}
		
		public void setStatusTv(TextView statusTv)
		{
			this.statusTv = statusTv;
		}
		
		public TextView getDesTv()
		{
			return desTv;
		}
		
		public void setDesTv(TextView desTv)
		{
			this.desTv = desTv;
		}

		
	}
}
