package dianyun.baobaowd.adapter;

import java.util.List;

import android.content.Context;
import android.graphics.Paint;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;

import dianyun.baobaowd.entity.CateItem;
import dianyun.zqcheap.R;
import dianyun.zqcheap.application.BaoBaoWDApplication;

public class MAdapter extends BaseAdapter {
    private Context mContext;
   private List<CateItem> mCateItemList;

    public MAdapter(Context context, List<CateItem> mCateItemList) {
        this.mContext = context;
        this.mCateItemList = mCateItemList;
    }

    @Override
    public int getCount() {
        return mCateItemList.size();
    }

    @Override
    public Object getItem(int position) {
        return mCateItemList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final CateItem mItem = mCateItemList.get(position);
        ViewHolder mHolder = null;
        if (convertView == null) {
        	System.out.println("convertView == null   "+position);
            convertView = LayoutInflater.from(mContext).inflate(
                    R.layout.dianyun_product_item_lay, null);
            mHolder = new ViewHolder(convertView);
            convertView.setTag(mHolder);
        } else {
            mHolder = (ViewHolder) convertView.getTag();
            System.out.println("convertView != null   "+position);
        }
        if (mItem == null) {
            return convertView;
        }
        // 初始化
        ImageLoader.getInstance(). 
                displayImage(
                        mItem.pics == null ? "" : mItem.pics.get(0), mHolder.mGoodsImageView,
                        BaoBaoWDApplication.mOptions);
        mHolder.mGoodsTitleTV.setText(mItem.title);
        mHolder.mCurrentPriceTV.setText("" + mItem.umpPrice);
        mHolder.mPriceTV.setText("" + mItem.price);
        if(mItem.itemType!=null&&mItem.itemType==CateItem.ITEMTYPE_STORE)
        	mHolder.mTianmoIv.setVisibility(View.VISIBLE);
        else
        	mHolder.mTianmoIv.setVisibility(View.GONE);
        
        if (mItem.coins == null || mItem.coins == 0)

        {
            mHolder.mGoodsBackFeesTV.setVisibility(View.VISIBLE);
            mHolder.mGoodsBackFeesTV.setText(mContext.getResources()
                    .getString(R.string.no_back_fee_string));
        } else

        {
            mHolder.mGoodsBackFeesTV.setVisibility(View.VISIBLE);
            mHolder.mGoodsBackFeesTV.setText(String.format(mContext.getResources()
                            .getString(R.string.shop_back_fee_string),
                    (mItem.coins == null ? 0 : mItem.coins)));
        }
        
        String detail ="";
        if(!TextUtils.isEmpty(mItem.monthlySales))detail+=mItem.monthlySales+mContext.getString(R.string.monthlysales)+"     ";
        if(!TextUtils.isEmpty(mItem.location))detail+=mItem.location+"     ";
        if(!TextUtils.isEmpty(mItem.sellerNick))detail+=mItem.sellerNick;
        mHolder.mDetailTv.setText(detail);
        
//        mHolder.mTotalLayout.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                goShopping(mItem, mContext, mListView);
//            }
//        });
//        mHolder.mBtGoBuy.setOnClickListener(new View.OnClickListener() {
//        	@Override
//        	public void onClick(View v) {
//        		goShopping(mItem, mContext, mListView);
//        	}
//        });
        return convertView;
    }

    private class ViewHolder {
        public ImageView mGoodsImageView;
        public TextView mGoodsBackFeesTV;
        public TextView mGoodsTitleTV;
        public TextView mPriceTV;
        public TextView mCurrentPriceTV;
        public TextView mDetailTv;
        public Button mBtGoBuy;
        public ImageView mTianmoIv;
        public RelativeLayout mTotalLayout;
        

        public ViewHolder(View view) {

        	mTotalLayout = (RelativeLayout) view.findViewById(R.id.shop_scan_result_total);
        	mTianmoIv = (ImageView) view.findViewById(R.id.tianmo_iv);
//            mBtGoBuy = (Button) view.findViewById(R.id.shop_product_item_go_buy);
            mCurrentPriceTV = (TextView) view.findViewById(R.id.goods_currentPrice);
            mDetailTv = (TextView) view.findViewById(R.id.goods_detail_tv);
            mPriceTV = (TextView) view.findViewById(R.id.goods_Price);
            mPriceTV.getPaint().setFlags(Paint.STRIKE_THRU_TEXT_FLAG); // 中划线
            mGoodsTitleTV = (TextView) view.findViewById(R.id.goods_title);
            mGoodsBackFeesTV = (TextView) view.findViewById(R.id.goods_backfees);
            mGoodsImageView = (ImageView) view.findViewById(R.id.goods_image);
        }
    }
}
