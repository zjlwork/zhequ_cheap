package dianyun.baobaowd.adapter;

import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;

import dianyun.baobaowd.db.LightDBHelper;
import dianyun.baobaowd.entity.CateItem;
import dianyun.baobaowd.interfaces.DialogCallBack;
import dianyun.baobaowd.util.ConversionHelper;
import dianyun.baobaowd.util.GobalConstants;
import dianyun.baobaowd.util.TaeSdkUtil;
import dianyun.baobaowd.util.ToastHelper;
import dianyun.baobaowd.util.UserHelper;
import dianyun.zqcheap.R;
import dianyun.zqcheap.activity.HtmlActivity;
import dianyun.zqcheap.application.BaoBaoWDApplication;

public class GoodsListAdapter extends BaseAdapter
{

	private Context mContext;
	private List<CateItem> mDataList;
	private int mWidth = 0;
	private View mCurrentActivityView;

	public GoodsListAdapter(Context context, List<CateItem> datalist)
	{
		mContext = context;
		mDataList = datalist;
		mWidth = (mContext.getResources().getDisplayMetrics().widthPixels - ConversionHelper
				.dipToPx(30, mContext)) / 2;
	}

	public void setCurrentActivityRootView(View activityView)
	{
		mCurrentActivityView = activityView;
	}
	public void setDataSource(List<CateItem> datalist)
	{
		mDataList = datalist;
		notifyDataSetChanged();
	}

	@Override
	public int getCount()
	{
		int res = 0;
		if (mDataList != null)
		{
			int count = mDataList.size();
			res = count / 2 + (count % 2 == 0 ? 0 : 1);
		}
		return res;
	}

	@Override
	public Object getItem(int position)
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getItemId(int position)
	{
		return position;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent)
	{
		 ViewHolder viewHolder = null;
		if (convertView == null)
		{
			convertView = LayoutInflater.from(mContext).inflate(
					R.layout.goods_list_item_lay, null);
			viewHolder = new ViewHolder();
			viewHolder.mCurrentPriceTV = (TextView) convertView
					.findViewById(R.id.goods_currentPrice);
			viewHolder.mPriceTV = (TextView) convertView
					.findViewById(R.id.goods_Price);
			viewHolder.mPriceTV.getPaint()
					.setFlags(Paint.STRIKE_THRU_TEXT_FLAG); // 中划线
			viewHolder.mGoodsTitleTV = (TextView) convertView
					.findViewById(R.id.goods_title);
			viewHolder.mGoodsBackFeesTV = (TextView) convertView
					.findViewById(R.id.goods_backfees);
			viewHolder.mGoodsImageView = (ImageView) convertView
					.findViewById(R.id.goods_image);
			final RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) viewHolder.mGoodsImageView
					.getLayoutParams();
			lp.width = mWidth;
			lp.height = mWidth;
			
			viewHolder.mItemLay1 = (RelativeLayout) convertView
					.findViewById(R.id.goods_item1);
			viewHolder.mCurrentPriceTV2 = (TextView) convertView
					.findViewById(R.id.goods_currentPrice2);
			viewHolder.mPriceTV2 = (TextView) convertView
					.findViewById(R.id.goods_Price2);
			viewHolder.mPriceTV2.getPaint().setFlags(
					Paint.STRIKE_THRU_TEXT_FLAG); // 中划线
			viewHolder.mGoodsTitleTV2 = (TextView) convertView
					.findViewById(R.id.goods_title2);
			viewHolder.mGoodsBackFeesTV2 = (TextView) convertView
					.findViewById(R.id.goods_backfees2);
			viewHolder.mGoodsImageView2 = (ImageView) convertView
					.findViewById(R.id.goods_image2);
			final RelativeLayout.LayoutParams lp2 = (RelativeLayout.LayoutParams) viewHolder.mGoodsImageView2
					.getLayoutParams();
			lp2.width = mWidth;
			lp2.height = mWidth;
			
			viewHolder.mItemLay2 = (RelativeLayout) convertView
					.findViewById(R.id.goods_item2);
			viewHolder.mGoodsImageView.setLayoutParams(lp);
			viewHolder.mGoodsImageView2.setLayoutParams(lp2);
			convertView.setTag(viewHolder);
		}
		viewHolder = (ViewHolder) convertView.getTag();
		if (2 * (1 + position) <= mDataList.size())
		{
			// 意味着刚好是双数的data 集合
			viewHolder.mItemLay1.setVisibility(View.VISIBLE);
			viewHolder.mItemLay2.setVisibility(View.VISIBLE);
			viewHolder.mItemLay1.setOnClickListener(new OnClickListener()
			{

				@Override
				public void onClick(View v)
				{
					onItemClick(2 * position);
				}
			});
			viewHolder.mItemLay2.setOnClickListener(new OnClickListener()
			{

				@Override
				public void onClick(View v)
				{
					onItemClick((2 * position) + 1);
				}
			});
		}
		else
		{
			// 意味着肯定是单数的data 集合
			viewHolder.mItemLay1.setVisibility(View.VISIBLE);
			viewHolder.mItemLay2.setVisibility(View.INVISIBLE);
			viewHolder.mItemLay1.setOnClickListener(new OnClickListener()
			{

				@Override
				public void onClick(View v)
				{
					onItemClick(2 * position);
				}
			});
			viewHolder.mItemLay2.setOnClickListener(null);
		}
		// 初始化item1
		if (2 * position < mDataList.size()
				&& mDataList.get(2 * position) != null)
		{
			
			
			ImageLoader.getInstance().displayImage(
					mDataList.get(2 * position).pics == null ? ""
							: mDataList.get(2 * position).pics.get(0),
					viewHolder.mGoodsImageView, BaoBaoWDApplication.mOptions);
			viewHolder.mGoodsTitleTV.setText(mDataList.get(2 * position).title);
			viewHolder.mCurrentPriceTV.setText(""
					+ mDataList.get(2 * position).umpPrice);
			viewHolder.mPriceTV
					.setText("" + mDataList.get(2 * position).price);
			
			String coin = String.format(mContext.getResources()
                    .getString(R.string.shop_back_fee_string),
                    (mDataList.get(2 * position).coins == null ? 0
							: mDataList.get(2 * position).coins));
			viewHolder.mGoodsBackFeesTV.setText(coin);
			
			
			
			if (mDataList.get(2 * position).coins == null
					|| mDataList.get(2 * position).coins == 0)
			{
				viewHolder.mGoodsBackFeesTV.setVisibility(View.GONE);
			}
			else
			{
				viewHolder.mGoodsBackFeesTV.setVisibility(View.VISIBLE);
			}
		}
		// 初始化item2
		if (((2 * position) + 1) < mDataList.size()
				&& mDataList.get(((2 * position) + 1)) != null)
		{
			ImageLoader.getInstance().displayImage(
					mDataList.get(((2 * position) + 1)).pics == null ? ""
							: mDataList.get(((2 * position) + 1)).pics.get(0),
					viewHolder.mGoodsImageView2, BaoBaoWDApplication.mOptions);
			viewHolder.mGoodsTitleTV2.setText(mDataList
					.get(((2 * position) + 1)).title);
			viewHolder.mCurrentPriceTV2.setText(""
					+ mDataList.get(((2 * position) + 1)).umpPrice);
			viewHolder.mPriceTV2.setText(""
					+ mDataList.get(((2 * position) + 1)).price);
			if (mDataList.get((2 * position) + 1).coins == null
					|| mDataList.get((2 * position) + 1).coins == 0)
			{
				viewHolder.mGoodsBackFeesTV2.setVisibility(View.GONE);
			}
			else
			{
				viewHolder.mGoodsBackFeesTV2.setVisibility(View.VISIBLE);
			}
			String coin = String.format(mContext.getResources()
                    .getString(R.string.shop_back_fee_string),
                    (mDataList.get((2 * position) + 1).coins == null ? 0
							: mDataList.get((2 * position) + 1).coins));
			
			
			viewHolder.mGoodsBackFeesTV2.setText(coin);
		}
		return convertView;
	}

	public void onItemClick(int position)
	{
		// Log.d("onItemClick", ""+position);
		final CateItem item = mDataList.get(position);
		if (item != null)
		{
			if (item.clickType == 4)
			{
				// 跳转到网页上的单品
				Intent itn = new Intent(mContext, HtmlActivity.class);
				itn.putExtra(GobalConstants.Data.URL,
						item.clickUrl == null ? "" : item.clickUrl);
				itn.putExtra(GobalConstants.Data.TITLE, item.pageTitle == null ? ""
						: item.pageTitle);
				mContext.startActivity(itn);
			}
			else
			{
				// 如果是访客用户 提示 登录 是否需要返利
				if (!LightDBHelper.getIsNotTipLoginOrNotFees(mContext)
						&& UserHelper.getUser().getIsGuest() == GobalConstants.UserType.ISSELF&&mCurrentActivityView!=null)
				{
//					ToastHelper.showTipLoginDialogWhenShop(mContext,
//							mCurrentActivityView,
//							new ToastHelper.dialogCancelCallback()
//							{
//
//								@Override
//								public void onCancle(boolean isNeverTip)
//								{
//									LightDBHelper.setIsNotTipLoginOrNotFees(
//											mContext, isNeverTip);
//									TaeSdkUtil.showTAEItemDetail(
//											(Activity) mContext, item.tbItemId,
//											item.taobaoPid, item.isTk == 1,
//											item.itemType, null,mCurrentActivityView);
//								}
//							});
					
					
					ToastHelper.showGoLoginDialog(mContext, new DialogCallBack() {
						
						@Override
						public void clickSure() {
							
						}
						
						@Override
						public void clickCancel() {
							TaeSdkUtil.showTAEItemDetail(
									(Activity) mContext, item.tbItemId,
									item.taobaoPid, item.isTk == 1,
									item.itemType, null,mCurrentActivityView);
						}
					});
					
					
					
					
					
					
				}
				else
				{
					TaeSdkUtil.showTAEItemDetail((Activity) mContext,
							item.tbItemId, item.taobaoPid, item.isTk == 1,
							item.itemType, null,mCurrentActivityView);
				}
			}
		}
	}

	private class ViewHolder
	{

		public RelativeLayout mItemLay1;
		public TextView mCurrentPriceTV;
		public TextView mPriceTV;
		public ImageView mGoodsImageView;
		public TextView mGoodsTitleTV;
		public TextView mGoodsBackFeesTV;
		public RelativeLayout mItemLay2;
		public TextView mCurrentPriceTV2;
		public TextView mPriceTV2;
		public ImageView mGoodsImageView2;
		public TextView mGoodsTitleTV2;
		public TextView mGoodsBackFeesTV2;
	}
}
