package dianyun.baobaowd.db;

import java.util.ArrayList;
import java.util.List;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.text.TextUtils;
import dianyun.baobaowd.data.User;
import dianyun.baobaowd.util.GobalConstants;

public class UserDBHelper extends BaseDBHelper
{

	private static final String[] userallColumn = new String[] {
			TableConstants.UserColumn.UID, TableConstants.UserColumn.NICKNAME,
			TableConstants.UserColumn.GENDER,
			TableConstants.UserColumn.PERINATAL,
			TableConstants.UserColumn.BABYBIRTHDAY,
			TableConstants.UserColumn.BABYGENDER,
			TableConstants.UserColumn.PROFILEIMAGE,
			TableConstants.UserColumn.EMAIL,
			TableConstants.UserColumn.PASSWORD,
			TableConstants.UserColumn.REGTIME, TableConstants.UserColumn.TOKEN,
			TableConstants.UserColumn.DEVICETOKEN,
			TableConstants.UserColumn.COINS, TableConstants.UserColumn.CREDITS,
			TableConstants.UserColumn.ISSHARED,
			TableConstants.UserColumn.ISSELF,
			TableConstants.UserColumn.QUESTIONCOUNT,
			TableConstants.UserColumn.ANSWERCOUNT,
			TableConstants.UserColumn.APPRECIATIONCOUNT,
			TableConstants.UserColumn.STATUS,
			TableConstants.UserColumn.BGIMAGEURL,
			TableConstants.UserColumn.ISCOUNSELLOR,
			TableConstants.UserColumn.LEVEL,
			TableConstants.UserColumn.DEVICEID,
			TableConstants.UserColumn.ACCINLASTWEEK,
			TableConstants.UserColumn.COINSYESTERDAY,
			TableConstants.UserColumn.FOLLOWSTATUS,
			TableConstants.UserColumn.FANSCOUNT,
			TableConstants.UserColumn.ATTENTIONCOUNT,
			TableConstants.UserColumn.ISGUEST, TableConstants.UserColumn.CITY,
			TableConstants.UserColumn.MEDALIDLISTSTR,TableConstants.UserColumn.YCOINS,
			TableConstants.UserColumn.FREEZEYCOINS
			};

	public UserDBHelper()
	{
		super();
	}

	public UserDBHelper(Context context, String table)
	{
		super(context, table);
	}

	protected UserDBHelper(Context context, String table, ContentValues values)
	{
		super(context, table, values);
	}

	public static User newUser(Cursor lCursor,int add)
	{
		return new User(lCursor.getLong(0+add), lCursor.getString(1+add),
				(byte) lCursor.getInt(2+add), lCursor.getString(3+add),
				lCursor.getString(4+add), (byte) lCursor.getInt(5+add),
				lCursor.getString(6+add), lCursor.getString(7+add),
				lCursor.getString(8+add), lCursor.getString(9+add),
				lCursor.getString(10+add), lCursor.getString(11+add),
				lCursor.getInt(12+add), lCursor.getInt(13+add),
				(byte) lCursor.getInt(14+add), (byte) lCursor.getInt(15+add),
				lCursor.getInt(16+add), lCursor.getInt(17+add), lCursor.getInt(18+add),
				(byte) lCursor.getInt(19+add), lCursor.getString(20+add),
				(byte) lCursor.getInt(21+add), lCursor.getInt(22+add),
				lCursor.getString(23+add), lCursor.getInt(24+add), lCursor.getInt(25+add),
				(byte) lCursor.getInt(26+add), lCursor.getInt(27+add),
				lCursor.getInt(28+add), (byte) lCursor.getInt(29+add),
				lCursor.getString(30+add), lCursor.getString(31+add),lCursor.getInt(32+add),lCursor.getInt(33+add));
	}

	// 获取基本信息 不包含 关联关系
	private User getBaseUser(String selection, String[] selectionArgs)
	{
		if (null == mDBHelper)
			mDBHelper = DBHelper.getInstance(mContext);
		Cursor lCursor = mDBHelper.getDatabase().query(mTable, userallColumn,
				selection, selectionArgs, null, null, null);
		User user = null;
		if (lCursor.moveToFirst())
		{
			user = newUser(lCursor,0);
		}
		lCursor.close();
		return user;
	}
	private List<User> getBaseListUser(String selection, String[] selectionArgs)
	{
		if (null == mDBHelper)
			mDBHelper = DBHelper.getInstance(mContext);
		Cursor lCursor = mDBHelper.getDatabase().query(mTable, userallColumn,
				selection, selectionArgs, null, null, null);
		List<User> userList = null;
		if (lCursor.moveToFirst())
		{
			userList = new ArrayList<User>();
			do{
				userList.add(newUser(lCursor,0)) ;
			}
			while (lCursor.moveToNext());
		}
		lCursor.close();
		return userList;
	}

	// 获取全部信息 包含 关联关系
	private User getFullUser(Context context, String selection,
			String[] selectionArgs)
	{
		User user = getBaseUser(selection, selectionArgs);
		return user;
	}

	public User getMeUser(Context context)
	{
		return getFullUser(context, "isSelf = "
				+ GobalConstants.UserType.ISSELF, null);
	}

	public User getGuestUser(Context context)
	{
		return getFullUser(context, "isGuest = "
				+ GobalConstants.AllStatusYesOrNo.YES, null);
	}

	public User getBaseUserByUid(long uid)
	{
		return getBaseUser("uid = ? ", new String[] { String.valueOf(uid) });
	}
	public List<User> getUserListByUid(String uids)
	{
		String selection = null ;
		String[] selectionArgs = null;
		if(!TextUtils.isEmpty(uids)){
			String[] ids = uids.split(",");
			selection = "uid in(";
			selectionArgs = new String[ids.length];
			for(int i=0;i<ids.length;i++){
				if(i==ids.length-1)selection += "?";
				else selection += "?,";
				selectionArgs[i]=ids[i];
			}
			selection += ")";
		}else{
			return null;
		}
		return getBaseListUser(selection,selectionArgs);
	}

	public long insert(User user)
	{
		mValues = ContentValuesUtil.convertUser(user);
		return insertDB();
	}

	public long update(User user)
	{
		mValues = ContentValuesUtil.convertUser(user);
		mWhereClaus = TableConstants.UserColumn.UID + "=?";
		mWhereArgs = new String[] { String.valueOf(user.getUid()) };
		return updateDB();
	}

	public long delete(User user)
	{
		mWhereClaus = TableConstants.UserColumn.UID + "=?";
		mWhereArgs = new String[] { String.valueOf(user.getUid()) };
		return delDB();
	}

	public boolean isExist(long uid)
	{
		return isExist(TableConstants.UserColumn.UID, String.valueOf(uid));
	}
}
