package dianyun.baobaowd.db;

import java.util.ArrayList;
import java.util.List;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import dianyun.baobaowd.data.XiaoMiPushData;
import dianyun.baobaowd.util.GobalConstants;

public class XiaoMiPushDataDBHelper extends BaseDBHelper
{

	private static final String[] allColumn = new String[] {
			TableConstants.XiaoMiPushDataColumn.NAME,
			TableConstants.XiaoMiPushDataColumn.STAUTS,
			TableConstants.XiaoMiPushDataColumn.UNSTATUS,
			TableConstants.XiaoMiPushDataColumn.TYPE };

	public XiaoMiPushDataDBHelper()
	{
		super();
	}

	public XiaoMiPushDataDBHelper(Context context, String table)
	{
		super(context, table);
	}

	protected XiaoMiPushDataDBHelper(Context context, String table,
			ContentValues values)
	{
		super(context, table, values);
	}

	public XiaoMiPushData getPushDataByName(String name)
	{
		if (null == mDBHelper)
			mDBHelper = DBHelper.getInstance(mContext);
		Cursor lCursor = mDBHelper.getDatabase().query(mTable, allColumn,
				"name = ?", new String[] { name }, null, null, null);
		XiaoMiPushData xiaoMiPushData = null;
		if (lCursor.moveToFirst())
		{
			xiaoMiPushData = new XiaoMiPushData(lCursor.getString(0),
					lCursor.getInt(1), lCursor.getInt(2), lCursor.getInt(3));
		}
		lCursor.close();
		return xiaoMiPushData;
	}

	public List<XiaoMiPushData> getPushDataList()
	{
		if (null == mDBHelper)
			mDBHelper = DBHelper.getInstance(mContext);
		Cursor lCursor = mDBHelper.getDatabase().query(mTable, allColumn, null,
				null, null, null, null);
		List<XiaoMiPushData> pushDatalist = null;
		if (lCursor.moveToFirst())
		{
			pushDatalist = new ArrayList<XiaoMiPushData>();
			do
			{
				XiaoMiPushData xiaoMiPushData = new XiaoMiPushData(
						lCursor.getString(0), lCursor.getInt(1),
						lCursor.getInt(2), lCursor.getInt(3));
				pushDatalist.add(xiaoMiPushData);
			}
			while (lCursor.moveToNext());
		}
		lCursor.close();
		return pushDatalist;
	}

	public List<XiaoMiPushData> getUnRegisterFailedPushDataList()
	{
		if (null == mDBHelper)
			mDBHelper = DBHelper.getInstance(mContext);
		Cursor lCursor = mDBHelper.getDatabase().query(
				mTable,
				allColumn,
				"unStatus = ?",
				new String[] { String
						.valueOf(GobalConstants.PushDataStatus.FAILED) }, null,
				null, null);
		List<XiaoMiPushData> pushDatalist = null;
		if (lCursor.moveToFirst())
		{
			pushDatalist = new ArrayList<XiaoMiPushData>();
			do
			{
				XiaoMiPushData xiaoMiPushData = new XiaoMiPushData(
						lCursor.getString(0), lCursor.getInt(1),
						lCursor.getInt(2), lCursor.getInt(3));
				pushDatalist.add(xiaoMiPushData);
			}
			while (lCursor.moveToNext());
		}
		lCursor.close();
		return pushDatalist;
	}

	public long insert(XiaoMiPushData xiaoMiPushData)
	{
		mValues = ContentValuesUtil.convertXiaoMiPushData(xiaoMiPushData);
		return insertDB();
	}

	public long update(XiaoMiPushData xiaoMiPushData)
	{
		mValues = ContentValuesUtil.convertXiaoMiPushData(xiaoMiPushData);
		mWhereClaus = TableConstants.XiaoMiPushDataColumn.NAME + "=?";
		mWhereArgs = new String[] { xiaoMiPushData.getName() };
		return updateDB();
	}

	public long deleteByType(XiaoMiPushData xiaoMiPushData)
	{
		mWhereClaus = TableConstants.XiaoMiPushDataColumn.TYPE + "=?";
		mWhereArgs = new String[] { String
				.valueOf(GobalConstants.XiaoMiPushDataType.TOPIC) };
		return delDB();
	}

	public long deleteByName(String name)
	{
		mWhereClaus = TableConstants.XiaoMiPushDataColumn.NAME + "=?";
		mWhereArgs = new String[] { name };
		return delDB();
	}

	@Override
	public long deleteAll()
	{
		mWhereClaus = "1=1";
		mWhereArgs = null;
		return delDB();
	}
}
