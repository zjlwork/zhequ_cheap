package dianyun.baobaowd.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import dianyun.baobaowd.data.Drafts;

public class DraftsDBHelper extends BaseDBHelper
{

	private static final String[] allColumn = new String[] {
			TableConstants.DraftsColumn.DRAFTSID,
			TableConstants.DraftsColumn.TITLE,
			TableConstants.DraftsColumn.CONTENT,
			TableConstants.DraftsColumn.TYPE
			};

	public DraftsDBHelper()
	{
		super();
	}

	public DraftsDBHelper(Context context, String table)
	{
		super(context, table);
	}

	protected DraftsDBHelper(Context context, String table, ContentValues values)
	{
		super(context, table, values);
	}

	public Drafts getDraftsByType(int type)
	{
		if (null == mDBHelper)
			mDBHelper = DBHelper.getInstance(mContext);
		Cursor lCursor = mDBHelper.getDatabase().query(mTable, allColumn,
				"type = ?", new String[]{String.valueOf(type)}, null, null, null);
		Drafts lDrafts = null;
		if (lCursor.moveToFirst())
		{
			lDrafts = new Drafts(lCursor.getString(0), lCursor.getString(1),
					lCursor.getString(2),lCursor.getInt(3));
			
		}
		lCursor.close();
		return lDrafts;
	}

	

	public long insert(Drafts drafts)
	{
		mValues = ContentValuesUtil.convertDrafts(drafts);
		return insertDB();
	}

	public long update(Drafts drafts)
	{
		mValues = ContentValuesUtil.convertDrafts(drafts);
		mWhereClaus = TableConstants.DraftsColumn.TYPE + "=?";
		mWhereArgs = new String[] { String.valueOf(drafts.getType())};
		return updateDB();
	}
//	public long delete(Drafts drafts)
//	{
//		mWhereClaus = TableConstants.DraftsColumn.DRAFTSID + "=?";
//		mWhereArgs = new String[] { drafts.getDraftsId() };
//		return delDB();
//	}

	public long deleteByType(int type)
	{
		mWhereClaus = TableConstants.DraftsColumn.TYPE + "=?";
		mWhereArgs = new String[] { String.valueOf(type) };
		return delDB();
	}

	
}
