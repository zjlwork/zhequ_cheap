package dianyun.baobaowd.db;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.database.Cursor;
import android.text.TextUtils;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import dianyun.baobaowd.data.ViewPicture;
import dianyun.baobaowd.entity.CateItem;
import dianyun.baobaowd.entity.LocalMenu;
import dianyun.baobaowd.entity.Menu;

public class ShopDBHelper
{

	private static DBHelper mDBHelper;
	
	
	
	public static final int MENULEVEL1=1;
	public static final int MENULEVEL2=2;
	
	
	//新逻辑
	
//	public static List<Menu> getMenu(Context context,int menuLevel ){
//
//		List<Menu> res = null;
//		if (mDBHelper == null)
//		{
//			mDBHelper = DBHelper.getInstance(context);
//		}
//		String queryString = "select id,name ,picUrl,parentId from "
//				+ TableConstants.TABLE_SHOP_MENU ;
//		
//		if(menuLevel==MENULEVEL1){
//			queryString+=" where parentId == 0";
//		}else if(menuLevel==MENULEVEL2){
//			queryString+=" where parentId != 0";
//		}
//		
//				
//		Cursor lCursor = mDBHelper.getDatabase().rawQuery(queryString, null);
//		if (lCursor.moveToFirst())
//		{
//			res = new ArrayList<Menu>();
//			do
//			{
//				Menu menu = new Menu();
//				menu.cateId = lCursor.getLong(0);
//				menu.name = lCursor.getString(1);
//				menu.pic = lCursor.getString(2);
//				res.add(menu);
//			}
//			while (lCursor.moveToNext());
//		}
//		lCursor.close();
//		return res;
//	
//	}
//	
//	
//	public static boolean insertMenu(Context context, List<Menu> menuList,int menuLevel )
//	{
//		boolean res = false;
//		if (menuList == null)
//		{
//			return res;
//		}
//		if (mDBHelper == null)
//		{
//			mDBHelper = DBHelper.getInstance(context);
//		}
//		String queryString = "delete from " + TableConstants.TABLE_SHOP_MENU;
//		if(menuLevel==MENULEVEL1){
//			queryString+=" where parentId == 0";
//		}else if(menuLevel==MENULEVEL2){
//			queryString+=" where parentId != 0";
//		}
//		
//		
//		try
//		{
//			mDBHelper.getDatabase().beginTransaction();
//			mDBHelper.getDatabase().execSQL(queryString);
//			for (Menu menu : menuList)
//			{
//				queryString = "insert into " + TableConstants.TABLE_SHOP_MENU
//						+ "(id,name,picUrl,parentId) values(" + menu.cateId + ",'"
//						+ menu.name + "','" + menu.pic+ "','" + menu.parentId + "')";
//				mDBHelper.getDatabase().execSQL(queryString);
//			}
//			mDBHelper.getDatabase().setTransactionSuccessful();
//		}
//		finally
//		{
//			mDBHelper.getDatabase().endTransaction();
//		}
//		res = true;
//		return res;
//	}
//	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
//	老逻辑

	public static List<LocalMenu> getShopMenu(Context context)
	{
		List<LocalMenu> res = null;
		if (mDBHelper == null)
		{
			mDBHelper = DBHelper.getInstance(context);
		}
		String queryString = "select a.id,a.name ,a.picUrl,b.childData from "
				+ TableConstants.TABLE_SHOP_MENU + " a left join "
				+ TableConstants.TABLE_SHOP_MENU_CHILDS + " b on a.id=b.menuid";
		Cursor lCursor = mDBHelper.getDatabase().rawQuery(queryString, null);
		if (lCursor.moveToFirst())
		{
			res = new ArrayList<LocalMenu>();
			do
			{
				LocalMenu menu = new LocalMenu();
				menu.cateId = lCursor.getLong(0);
				menu.name = lCursor.getString(1);
				menu.pic = lCursor.getString(2);
				String dataString = lCursor.getString(3);
				if (!TextUtils.isEmpty(dataString))
				{
					Gson gson = new Gson();
					menu.subDataList = gson.fromJson(dataString,
							new TypeToken<List<CateItem>>()
							{
							}.getType());
				}
				res.add(menu);
			}
			while (lCursor.moveToNext());
		}
		lCursor.close();
		return res;
	}

	public static List<CateItem> getShopMenu_CatentByCategory(Context context,
			Long menuID, Long cateID)
	{
		List<CateItem> res = null;
		if (mDBHelper == null)
		{
			mDBHelper = DBHelper.getInstance(context);
		}
		String queryString = "select * from "
				+ TableConstants.TABLE_SHOP_CONTENT + "  where menuId="
				+ menuID + " and cateId=" + cateID + "";
		Cursor lCursor = mDBHelper.getDatabase().rawQuery(queryString, null);
		if (lCursor.moveToFirst())
		{
			String dataString = lCursor.getString(2);
			if (!TextUtils.isEmpty(dataString))
			{
				Gson gson = new Gson();
				res = (gson.fromJson(dataString,
						new TypeToken<List<CateItem>>()
						{
						}.getType()));
			}
		}
		lCursor.close();
		return res;
	}

    public static List<CateItem> getHighelestBackFee(Context context)
    {
        List<CateItem> res = null;
        if (mDBHelper == null)
        {
            mDBHelper = DBHelper.getInstance(context);
        }
        String queryString = "select * from "
                + TableConstants.TABLE_MAIN_HIGHLEST_BACKFEE;
        Cursor lCursor = mDBHelper.getDatabase().rawQuery(queryString, null);
        if (lCursor.moveToFirst())
        {
            String dataString = lCursor.getString(0);
            if (!TextUtils.isEmpty(dataString))
            {
                Gson gson = new Gson();
                res = (gson.fromJson(dataString,
                        new TypeToken<List<CateItem>>()
                        {
                        }.getType()));
            }
        }
        lCursor.close();
        return res;
    }
    public static List<CateItem> getFocusPicture(Context context)
    {
    	List<CateItem> res = null;
    	if (mDBHelper == null)
    	{
    		mDBHelper = DBHelper.getInstance(context);
    	}
    	String queryString = "select * from "
    			+ TableConstants.TABLE_FACUSPICTURE;
    	Cursor lCursor = mDBHelper.getDatabase().rawQuery(queryString, null);
    	if (lCursor.moveToFirst())
    	{
    		String dataString = lCursor.getString(0);
    		if (!TextUtils.isEmpty(dataString))
    		{
    			Gson gson = new Gson();
    			res = (gson.fromJson(dataString,
    					new TypeToken<List<CateItem>>()
    					{
    					}.getType()));
    		}
    	}
    	lCursor.close();
    	return res;
    }
    public static List<ViewPicture> getMiaoshaPicture(Context context)
    {
    	List<ViewPicture> res = null;
    	if (mDBHelper == null)
    	{
    		mDBHelper = DBHelper.getInstance(context);
    	}
    	String queryString = "select * from "
    			+ TableConstants.TABLE_MIAOSHAPICTURE;
    	Cursor lCursor = mDBHelper.getDatabase().rawQuery(queryString, null);
    	if (lCursor.moveToFirst())
    	{
    		String dataString = lCursor.getString(0);
    		if (!TextUtils.isEmpty(dataString))
    		{
    			Gson gson = new Gson();
    			res = (gson.fromJson(dataString,
    					new TypeToken<List<ViewPicture>>()
    					{
    					}.getType()));
    		}
    	}
    	lCursor.close();
    	return res;
    }

	public static boolean insertShopMenu(Context context, List<Menu> menuList)
	{
		boolean res = false;
		if (menuList == null)
		{
			return res;
		}
		if (mDBHelper == null)
		{
			mDBHelper = DBHelper.getInstance(context);
		}
		String queryString = "delete from " + TableConstants.TABLE_SHOP_MENU;
		try
		{
			mDBHelper.getDatabase().beginTransaction();
			mDBHelper.getDatabase().execSQL(queryString);
			for (Menu menu : menuList)
			{
				queryString = "insert into " + TableConstants.TABLE_SHOP_MENU
						+ "(id,name,picUrl) values(" + menu.cateId + ",'"
						+ menu.name + "','" + menu.pic + "')";
				mDBHelper.getDatabase().execSQL(queryString);
			}
			mDBHelper.getDatabase().setTransactionSuccessful();
		}
		finally
		{
			mDBHelper.getDatabase().endTransaction();
		}
		res = true;
		return res;
	}

	public static boolean insertShopMenuChildData(Context context,
			String menuID, String childString)
	{
		boolean res = false;
		if (TextUtils.isEmpty(childString) == true)
		{
			return res;
		}
		if (mDBHelper == null)
		{
			mDBHelper = DBHelper.getInstance(context);
		}
		String queryString = "delete from "
				+ TableConstants.TABLE_SHOP_MENU_CHILDS + "  where menuid="
				+ menuID + "";
		try
		{
			mDBHelper.getDatabase().beginTransaction();
			mDBHelper.getDatabase().execSQL(queryString);
			queryString = "insert into "
					+ TableConstants.TABLE_SHOP_MENU_CHILDS + " values("
					+ menuID + ",'" + childString + "')";
			mDBHelper.getDatabase().execSQL(queryString);
			mDBHelper.getDatabase().setTransactionSuccessful();
		}
		finally
		{
			mDBHelper.getDatabase().endTransaction();
		}
		res = true;
		return res;
	}

    public static boolean insertHighelestData(Context context,String childString)
    {
        boolean res = false;
        if (TextUtils.isEmpty(childString) == true)
        {
            return res;
        }
        if (mDBHelper == null)
        {
            mDBHelper = DBHelper.getInstance(context);
        }
        String queryString = "delete from "
                + TableConstants.TABLE_MAIN_HIGHLEST_BACKFEE;
        try
        {
            mDBHelper.getDatabase().beginTransaction();
            mDBHelper.getDatabase().execSQL(queryString);
            queryString = "insert into "
                    + TableConstants.TABLE_MAIN_HIGHLEST_BACKFEE + " values('" + childString + "')";
            mDBHelper.getDatabase().execSQL(queryString);
            mDBHelper.getDatabase().setTransactionSuccessful();
        }
        finally
        {
            mDBHelper.getDatabase().endTransaction();
        }
        res = true;
        return res;
    }
    public static boolean insertFocusPictureData(Context context,String childString)
    {
    	boolean res = false;
    	if (TextUtils.isEmpty(childString) == true)
    	{
    		return res;
    	}
    	if (mDBHelper == null)
    	{
    		mDBHelper = DBHelper.getInstance(context);
    	}
    	String queryString = "delete from "
    			+ TableConstants.TABLE_FACUSPICTURE;
    	try
    	{
//    		mDBHelper.getDatabase().beginTransaction();
    		mDBHelper.getDatabase().execSQL(queryString);
    		queryString = "insert into "
    				+ TableConstants.TABLE_FACUSPICTURE + " values('" + childString + "')";
    		mDBHelper.getDatabase().execSQL(queryString);
//    		mDBHelper.getDatabase().setTransactionSuccessful();
    	}catch(Exception e){
    		e.printStackTrace();
    	}
    	finally
    	{
//    		mDBHelper.getDatabase().endTransaction();
    	}
    	res = true;
    	return res;
    }
    public static boolean insertMiaoshaPictureData(Context context,String childString)
    {
    	boolean res = false;
    	if (TextUtils.isEmpty(childString) == true)
    	{
    		return res;
    	}
    	if (mDBHelper == null)
    	{
    		mDBHelper = DBHelper.getInstance(context);
    	}
    	String queryString = "delete from "
    			+ TableConstants.TABLE_MIAOSHAPICTURE;
    	try
    	{
//    		mDBHelper.getDatabase().beginTransaction();
    		mDBHelper.getDatabase().execSQL(queryString);
    		queryString = "insert into "
    				+ TableConstants.TABLE_MIAOSHAPICTURE + " values('" + childString + "')";
    		mDBHelper.getDatabase().execSQL(queryString);
//    		mDBHelper.getDatabase().setTransactionSuccessful();
    	}catch(Exception e){
    		e.printStackTrace();
    	}
    	finally
    	{
//    		mDBHelper.getDatabase().endTransaction();
    	}
    	res = true;
    	return res;
    }

	public static boolean updateShopMenuChilds(Context context, String menuID,
			String categoryListString)
	{
		boolean res = false;
		if (mDBHelper == null)
		{
			mDBHelper = DBHelper.getInstance(context);
		}
		String querString = "update " + TableConstants.TABLE_SHOP_MENU_CHILDS
				+ " set childData='" + categoryListString + "'  where menuid="
				+ menuID + "";
		mDBHelper.getDatabase().execSQL(querString);
		res = true;
		return res;
	}

	public static boolean saveShopMenu_CategoryItemByCateID(Context context,
			String menuID, String cateID, String cateChildItemList)
	{
		boolean res = false;
		if (mDBHelper == null)
		{
			mDBHelper = DBHelper.getInstance(context);
		}
		String queryString = "delete from " + TableConstants.TABLE_SHOP_CONTENT
				+ "  where menuId=" + menuID + " and cateId=" + cateID + "";
		try
		{
			mDBHelper.getDatabase().beginTransaction();
			mDBHelper.getDatabase().execSQL(queryString);
			queryString = "insert into " + TableConstants.TABLE_SHOP_CONTENT
					+ "(menuId,cateId,childData) values(" + menuID + ","
					+ cateID + ",'" + cateChildItemList + "')";
			mDBHelper.getDatabase().execSQL(queryString);
			mDBHelper.getDatabase().setTransactionSuccessful();
		}
		finally
		{
			mDBHelper.getDatabase().endTransaction();
		}
		res = true;
		return res;
	}
}
