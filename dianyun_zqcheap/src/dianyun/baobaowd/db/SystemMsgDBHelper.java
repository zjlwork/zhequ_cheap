package dianyun.baobaowd.db;

import java.util.ArrayList;
import java.util.List;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import dianyun.baobaowd.data.Message;

public class SystemMsgDBHelper extends BaseDBHelper
{

	private static final String[] allColumn = new String[] {
			TableConstants.MessageColumn.SEQID,
			TableConstants.MessageColumn.MESSAGEID,
			TableConstants.MessageColumn.MESSAGETYPE,
			TableConstants.MessageColumn.TIMESTAMP,
			TableConstants.MessageColumn.UID,
			TableConstants.MessageColumn.CONTENTOBJECT };

	public SystemMsgDBHelper()
	{
		super();
	}

	public SystemMsgDBHelper(Context context, String table)
	{
		super(context, table);
	}

	protected SystemMsgDBHelper(Context context, String table,
			ContentValues values)
	{
		super(context, table, values);
	}

	public long getSystemmsgMaxSeqId()
	{
		if (null == mDBHelper)
			mDBHelper = DBHelper.getInstance(mContext);
		Cursor lCursor = mDBHelper.getDatabase().rawQuery(
				"select  max(seqId) from systemmsg ", null);
		long maxSeqId = 0;
		if (lCursor.moveToFirst())
		{
			maxSeqId = lCursor.getLong(0);
		}
		lCursor.close();
		return maxSeqId;
	}

	public List<Message> getMessageList()
	{
		if (null == mDBHelper)
			mDBHelper = DBHelper.getInstance(mContext);
		Cursor lCursor = mDBHelper.getDatabase().query(mTable, allColumn, null,
				null, null, null, "seqId desc");
		List<Message> messageList = null;
		if (lCursor.moveToFirst())
		{
			messageList = new ArrayList<Message>();
			do
			{
				Message message = new Message(lCursor.getLong(0),
						lCursor.getString(1), lCursor.getInt(2),
						lCursor.getLong(3), lCursor.getLong(4),
						lCursor.getString(5));
				messageList.add(message);
			}
			while (lCursor.moveToNext());
		}
		lCursor.close();
		return messageList;
	}

	public long insert(Message message)
	{
		mValues = ContentValuesUtil.convertMessage(message);
		return insertDB();
	}

	public long update(Message message)
	{
		mValues = ContentValuesUtil.convertMessage(message);
		mWhereClaus = TableConstants.MessageColumn.MESSAGEID + "=?";
		mWhereArgs = new String[] { message.getMessageId() };
		return updateDB();
	}

	public long delete(Message message)
	{
		mWhereClaus = TableConstants.MessageColumn.MESSAGEID + "=?";
		mWhereArgs = new String[] { message.getMessageId() };
		return delDB();
	}

	public boolean isExist(long seqId)
	{
		return isExist(TableConstants.MessageColumn.SEQID,
				String.valueOf(seqId));
	}
}
