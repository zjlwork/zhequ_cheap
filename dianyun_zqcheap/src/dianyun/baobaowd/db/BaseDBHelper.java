package dianyun.baobaowd.db;

import java.util.List;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import dianyun.baobaowd.help.LogFile;

/**
 * 数据库操作抽象类
 * 
 * @author piers.xie
 */
public class BaseDBHelper
{

	protected static final byte[] baselock = new byte[0];
	// protected static int lock = 0;
	protected DBHelper mDBHelper;
	protected String mTable;
	protected ContentValues mValues;
	protected String mWhereClaus = null;
	protected String mWhereArgs[] = null;
	protected Context mContext;
	protected Cursor mCursor;

	public BaseDBHelper()
	{
		super();
	}

	public BaseDBHelper(Context context, String table)
	{
		super();
		mContext = context;
		mDBHelper = DBHelper.getInstance(context);
		this.mTable = table;
	}

	public BaseDBHelper(Context context, String table, ContentValues values)
	{
		super();
		mDBHelper = DBHelper.getInstance(context);
		this.mTable = table;
		this.mValues = values;
	}

	protected String getTable()
	{
		return mTable;
	}

	protected void setTable(String mTable)
	{
		this.mTable = mTable;
	}

	protected ContentValues getValues()
	{
		return mValues;
	}

	protected void setValues(ContentValues mValues)
	{
		this.mValues = mValues;
	}

	protected String getWhereClaus()
	{
		return mWhereClaus;
	}

	protected void setWhereClaus(String mWhereClaus)
	{
		this.mWhereClaus = mWhereClaus;
	}

	protected String[] getWhereArgs()
	{
		return mWhereArgs;
	}

	protected void setWhereArgs(String[] mWhereArgs)
	{
		this.mWhereArgs = mWhereArgs;
	}

	/**
	 * 插入�?��记录到数据库
	 * 
	 * @return
	 */
	public synchronized long insertDB()
	{
		// synchronized(baselock){
		if (null != mTable && null != mValues && (mDBHelper != null))
		{
			long result = mDBHelper.getDatabase().insert(mTable, null, mValues);
			// if(result<0)
			LogFile.SaveLog("mTable=" + mTable + "     mValues=" + mValues
					+ "   insertDB=" + result);
			return result;
		}
		else
		{
			LogFile.SaveLog("重要 insert 数据库失------mTable=" + mTable
					+ "     mValues=" + mValues);
			return -1;
		}
		// }
	}

	/**
	 * 更新�?��记录到数据库
	 * 
	 * @return
	 */
	public synchronized long updateDB()
	{
		// synchronized(baselock){
		if (null != mTable && null != mValues)
		{
			long result = mDBHelper.getDatabase().update(mTable, mValues,
					mWhereClaus, mWhereArgs);
			// if(result<0)
			LogFile.SaveLog("mTable=" + mTable + "     mValues=" + mValues
					+ "   mWhereClaus=" + mWhereClaus + " updateDB=" + result);
			return result;
		}
		else
		{
			LogFile.SaveLog("重要 update 数据库失�?------mTable=" + mTable
					+ "     mValues=" + mValues + "   mWhereClaus="
					+ mWhereClaus);
			return -1;
		}
		// }
	}

	/**
	 * 删除�?��记录到数据库
	 * 
	 * @return
	 */
	public synchronized long delDB()
	{
		// synchronized(baselock){
		if (null != mTable && (mDBHelper != null))
		{
			long result = mDBHelper.getDatabase().delete(mTable, mWhereClaus,
					mWhereArgs);
			// if(result<0)
			LogFile.SaveLog("mTable=" + mTable + "     mWhereClaus="
					+ mWhereClaus + "  mWhereArgs=" + mWhereArgs + "  delDB="
					+ result);
			return result;
		}
		else
		{
			LogFile.SaveLog("重要 delete 数据库失�?------mTable=" + mTable
					+ "     mWhereClaus=" + mWhereClaus + "  mWhereArgs="
					+ mWhereArgs);
			return -1;
		}
		// }
	}

	/**
	 * 删除�?��数据库记�?
	 * 
	 * @return
	 */
	// public long deleteAll(String username) {
	// // //synchronized(baselock){
	//
	// return delDB();
	// // }
	// }
	public synchronized long deleteAll()
	{
		mWhereClaus = "1=1";
		mWhereArgs = null;
		// mWhereArgs = new String[] {String.valueOf(1)};
		return delDB();
	}

	/**
	 * 判断是否存在ID的记�?
	 * 
	 * @return
	 */
	public synchronized boolean isExist(String column, String value)
	{
		// synchronized(baselock){
		boolean isExist = false;
		if (mDBHelper != null)
		{
			Cursor lCursor = mDBHelper.getDatabase().query(mTable,
					new String[] { column }, column + "=?",
					new String[] { value }, null, null, null);
			if (lCursor.moveToFirst())
			{
				isExist = true;
			}
			lCursor.close();
		}
		return isExist;
		// }
	}

	/**
	 * 判断是否存在记录
	 * 
	 * @return
	 */
	public synchronized boolean isExist(String column, String column2,
			String value, String value2)
	{
		// synchronized(baselock){
		boolean isExist = false;
		if (mDBHelper != null)
		{
			Cursor lCursor = mDBHelper.getDatabase().query(mTable,
					new String[] { column, column2 },
					column + "=? and " + column2 + "=?",
					new String[] { value, value2 }, null, null, null);
			if (lCursor.moveToFirst())
			{
				isExist = true;
			}
			lCursor.close();
		}
		return isExist;
		// }
	}

	public void beginTransaciton()
	{
		synchronized (baselock)
		{
			if (mDBHelper != null)
			{
				mDBHelper.getDatabase().beginTransaction();
			}
		}
	}

	public void setTransactionSuccessful()
	{
		synchronized (baselock)
		{
			if (mDBHelper != null)
			{
				mDBHelper.getDatabase().setTransactionSuccessful();
			}
		}
	}

	public void endTransaction()
	{
		synchronized (baselock)
		{
			if (mDBHelper != null)
			{
				mDBHelper.getDatabase().endTransaction();
			}
		}
	}

	public String getOrWhereSting(String columeName, List<String> selection)
	{
		StringBuilder where = new StringBuilder();
		for (int i = 0; i < selection.size(); i++)
		{
			where.append(columeName);
			where.append("='");
			where.append(selection.get(i));
			where.append("'");
			if (i < selection.size() - 1)
				where.append(" OR ");
		}
		return where.toString();
	}

	public synchronized void closeDB()
	{
	}
}
