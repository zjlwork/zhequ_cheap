package dianyun.baobaowd.db;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import dianyun.baobaowd.data.WelcomePicture;

public class WelcomePicDBHelper extends BaseDBHelper
{

//	public static String SEQID = "seqId";
//	public static String FILEURL = "fileUrl";
//	public static String LOCALPATH = "localPath";
//	public static String JUMPTYPE = "jumpType";
//	public static String JUMPVALUE = "jumpValue";
	private static final String[] allColumn = new String[] {
			TableConstants.WelcomePicColumn.SEQID, TableConstants.WelcomePicColumn.FILEURL,
			TableConstants.WelcomePicColumn.LOCALPATH,
			TableConstants.WelcomePicColumn.JUMPTYPE,TableConstants.WelcomePicColumn.JUMPVALUE
			};

	public WelcomePicDBHelper()
	{
		super();
	}

	public WelcomePicDBHelper(Context context, String table)
	{
		super(context, table);
	}

	protected WelcomePicDBHelper(Context context, String table, ContentValues values)
	{
		super(context, table, values);
	}
	
	
	
//	private List<WelcomePicture> getFirstStairWelcomePicture()
//	{
//		if (null == mDBHelper)
//			mDBHelper = DBHelper.getInstance(mContext);
//		Cursor lCursor = mDBHelper.getDatabase().query(mTable, allColumn,
//				TableConstants.WelcomePictureColumn.PARENTID+" = 0", null, null, null, null);
//		List<WelcomePicture> WelcomePictureList = null;
//		if (lCursor.moveToFirst())
//		{
//			WelcomePictureList = new ArrayList<WelcomePicture>();
//			do
//			{
//				
//				WelcomePicture WelcomePicture = new WelcomePicture(lCursor.getLong(0),lCursor.getString(1),lCursor.getString(2),lCursor.getLong(3));
//				WelcomePictureList.add(WelcomePicture);
//			}
//			while (lCursor.moveToNext());
//			
//		}
//		lCursor.close();
//		return WelcomePictureList;
//	}
	

	public WelcomePicture getWelcomePicture()
	{
		Cursor lCursor = mDBHelper.getDatabase().query(mTable,
				allColumn, null,
				null, null, null, null);
		WelcomePicture welcomePicture= null;
		if (lCursor.moveToFirst())
		{
			welcomePicture = new WelcomePicture(lCursor.getInt(0),lCursor.getString(1),
						lCursor.getString(2),lCursor.getInt(3),lCursor.getString(4));
			
		}
		lCursor.close();
		return welcomePicture;
	}

	public long deleteWelcomePicture(){
		mWhereClaus =  "1=1";
		return delDB();
	}
	public long insert(WelcomePicture WelcomePicture)
	{
		mValues = ContentValuesUtil.convertWelcomePicture(WelcomePicture);
		return insertDB();
	}

	
	
	

	
}
