package dianyun.baobaowd.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import dianyun.baobaowd.data.UserShareUrl;

public class UserShareUrlDBHelper extends BaseDBHelper
{

//	String urlKey, Integer appId, String urlValue,
//	String urlIcon, String urlTitle, String urlDesc, String urlContent
	private static final String[] allColumn = new String[] {
			TableConstants.UserShareUrlColumn.URLKEY, TableConstants.UserShareUrlColumn.APPID,
			TableConstants.UserShareUrlColumn.URLVALUE,
			TableConstants.UserShareUrlColumn.URLICON,
			TableConstants.UserShareUrlColumn.URLTITLE,
			TableConstants.UserShareUrlColumn.URLDESC, TableConstants.UserShareUrlColumn.URLCONTENT
			};

	public UserShareUrlDBHelper()
	{
		super();
	}

	public UserShareUrlDBHelper(Context context, String table)
	{
		super(context, table);
	}

	protected UserShareUrlDBHelper(Context context, String table, ContentValues values)
	{
		super(context, table, values);
	}
	
	
	
	
	

	public UserShareUrl getUserShareUrlByKey(String key)
	{
		if (null == mDBHelper)
			mDBHelper = DBHelper.getInstance(mContext);
		Cursor lCursor = mDBHelper.getDatabase().query(mTable, allColumn,
				"urlKey = ?", new String[] { key }, null, null, null);
		UserShareUrl lUserShareUrl = null;
		if (lCursor.moveToFirst())
		{
			lUserShareUrl = new UserShareUrl(lCursor.getString(0), lCursor.getInt(1),
						lCursor.getString(2), lCursor.getString(3),
						lCursor.getString(4), lCursor.getString(5),
						lCursor.getString(6));
		}
		lCursor.close();
		return lUserShareUrl;
	}
	
	

	public long insert(UserShareUrl userShareUrl)
	{
		mValues = ContentValuesUtil.convertUserShareUrl(userShareUrl);
		return insertDB();
	}

	public long update(UserShareUrl userShareUrl)
	{
		mValues = ContentValuesUtil.convertUserShareUrl(userShareUrl);
		mWhereClaus = TableConstants.UserShareUrlColumn.URLKEY + "=?";
		mWhereArgs = new String[] {userShareUrl.getUrlKey()};
		return updateDB();
	}

	

	
}
