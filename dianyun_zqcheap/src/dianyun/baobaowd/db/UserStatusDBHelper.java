package dianyun.baobaowd.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import dianyun.baobaowd.data.UserStatus;

public class UserStatusDBHelper extends BaseDBHelper
{

	// String userStatusId,int type, String time, int period, int cycle,byte
	// babyGender
	private static final String[] userstatusallColumn = new String[] {
			TableConstants.UserStatusColumn.USERSTATUSID,
			TableConstants.UserStatusColumn.TYPE,
			TableConstants.UserStatusColumn.TIME,
			TableConstants.UserStatusColumn.PERIOD,
			TableConstants.UserStatusColumn.CYCLE,
			TableConstants.UserStatusColumn.BABYGENDER };

	public UserStatusDBHelper()
	{
		super();
	}

	public UserStatusDBHelper(Context context, String table)
	{
		super(context, table);
	}

	protected UserStatusDBHelper(Context context, String table,
			ContentValues values)
	{
		super(context, table, values);
	}

	public UserStatus getUserStatus()
	{
		if (null == mDBHelper)
			mDBHelper = DBHelper.getInstance(mContext);
		Cursor lCursor = mDBHelper.getDatabase().query(mTable,
				userstatusallColumn, null, null, null, null, null);
		UserStatus lUserStatus = null;
		if (lCursor.moveToFirst())
		{
			lUserStatus = new UserStatus(lCursor.getString(0),
					lCursor.getInt(1), lCursor.getString(2), lCursor.getInt(3),
					lCursor.getInt(4), (byte) lCursor.getInt(5));
		}
		lCursor.close();
		return lUserStatus;
	}

	public long insert(UserStatus userStatus)
	{
		mValues = ContentValuesUtil.convertUserStatus(userStatus);
		return insertDB();
	}

	public long update(UserStatus userStatus)
	{
		mValues = ContentValuesUtil.convertUserStatus(userStatus);
		mWhereClaus = TableConstants.UserStatusColumn.USERSTATUSID + "=?";
		mWhereArgs = new String[] { userStatus.getUserStatusId() };
		return updateDB();
	}

	public long delete(UserStatus userStatus)
	{
		mWhereClaus = TableConstants.UserStatusColumn.USERSTATUSID + "=?";
		mWhereArgs = new String[] { userStatus.getUserStatusId() };
		return delDB();
	}

	@Override
	public long deleteAll()
	{
		mWhereClaus = "1=1";
		mWhereArgs = null;
		// mWhereArgs = new String[] {String.valueOf(1)};
		return delDB();
	}
	/*
	 * public boolean isExist(long uid){ return
	 * isExist(TableConstants.UserColumn.UID, String.valueOf(uid)); }
	 */
}
