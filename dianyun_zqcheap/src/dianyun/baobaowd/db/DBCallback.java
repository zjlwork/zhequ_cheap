package dianyun.baobaowd.db;

public interface DBCallback
{

	public Object execInsert(BaseDBHelper pBaseDBHelper);

	public Object execUpdate(BaseDBHelper pBaseDBHelper);

	public Object execDel(BaseDBHelper pBaseDBHelper);
}
