package dianyun.baobaowd.db;

import java.util.ArrayList;
import java.util.List;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import dianyun.baobaowd.data.NewestToast;

public class NewestToastDBHelper extends BaseDBHelper
{

	private static final String[] allColumn = new String[] {
			TableConstants.NewestToastColumn.TOASTID,
			TableConstants.NewestToastColumn.TOASTCONTENT,
			TableConstants.NewestToastColumn.TOASTTIME,
			TableConstants.NewestToastColumn.ISREAD };

	public NewestToastDBHelper()
	{
		super();
	}

	public NewestToastDBHelper(Context context, String table)
	{
		super(context, table);
	}

	protected NewestToastDBHelper(Context context, String table,
			ContentValues values)
	{
		super(context, table, values);
	}

	public List<NewestToast> getNewestToastList()
	{
		if (null == mDBHelper)
			mDBHelper = DBHelper.getInstance(mContext);
		Cursor lCursor = mDBHelper.getDatabase().query(mTable, allColumn, null,
				null, null, null, null);
		List<NewestToast> newestToastList = null;
		if (lCursor.moveToFirst())
		{
			newestToastList = new ArrayList<NewestToast>();
			do
			{
				NewestToast newestToast = new NewestToast(lCursor.getInt(0),
						lCursor.getString(1), lCursor.getString(2),
						(byte) lCursor.getInt(3));
				newestToastList.add(newestToast);
			}
			while (lCursor.moveToNext());
		}
		lCursor.close();
		return newestToastList;
	}

	public NewestToast getNewestToastById(int newestToastId)
	{
		if (null == mDBHelper)
			mDBHelper = DBHelper.getInstance(mContext);
		Cursor lCursor = mDBHelper.getDatabase().query(mTable, allColumn,
				"toastId = ?", new String[] { String.valueOf(newestToastId) },
				null, null, null);
		NewestToast newestToast = null;
		if (lCursor.moveToFirst())
		{
			newestToast = new NewestToast(lCursor.getInt(0),
					lCursor.getString(1), lCursor.getString(2),
					(byte) lCursor.getInt(3));
		}
		lCursor.close();
		return newestToast;
	}

	public long insert(NewestToast newestToast)
	{
		mValues = ContentValuesUtil.convertNewestToast(newestToast);
		return insertDB();
	}

	public long update(NewestToast newestToast)
	{
		mValues = ContentValuesUtil.convertNewestToast(newestToast);
		mWhereClaus = TableConstants.NewestToastColumn.TOASTID + "=?";
		mWhereArgs = new String[] { String.valueOf(newestToast.getToastId()) };
		return updateDB();
	}

	public long delete(NewestToast newestToast)
	{
		mWhereClaus = TableConstants.NewestToastColumn.TOASTID + "=?";
		mWhereArgs = new String[] { String.valueOf(newestToast.getToastId()) };
		return delDB();
	}

	public boolean isExist(long seqId)
	{
		return isExist(TableConstants.MessageColumn.SEQID,
				String.valueOf(seqId));
	}
}
