package dianyun.baobaowd.db;

import android.content.ContentValues;
import dianyun.baobaowd.data.Attachment;
import dianyun.baobaowd.data.Drafts;
import dianyun.baobaowd.data.Message;
import dianyun.baobaowd.data.NewestToast;
import dianyun.baobaowd.data.Portal;
import dianyun.baobaowd.data.Post;
import dianyun.baobaowd.data.Question;
import dianyun.baobaowd.data.Relation;
import dianyun.baobaowd.data.Topic;
import dianyun.baobaowd.data.User;
import dianyun.baobaowd.data.UserShareUrl;
import dianyun.baobaowd.data.UserStatus;
import dianyun.baobaowd.data.ViewPicture;
import dianyun.baobaowd.data.WelcomePicture;
import dianyun.baobaowd.data.XiaoMiPushData;
import dianyun.baobaowd.entity.Menu;

public class ContentValuesUtil
{

	
	public static ContentValues convertUserShareUrl(UserShareUrl userShareUrl)
	{
		ContentValues lContentValues = null;
		if (null != userShareUrl)
		{
			lContentValues = new ContentValues();
			if (null != userShareUrl.getUrlKey())
				lContentValues.put(TableConstants.UserShareUrlColumn.URLKEY,
						userShareUrl.getUrlKey());
			if (null != userShareUrl.getAppId())
				lContentValues.put(TableConstants.UserShareUrlColumn.APPID,
						userShareUrl.getAppId());
			if (null != userShareUrl.getUrlValue())
				lContentValues.put(TableConstants.UserShareUrlColumn.URLVALUE,
						userShareUrl.getUrlValue());
			if (null != userShareUrl.getUrlIcon())
				lContentValues.put(TableConstants.UserShareUrlColumn.URLICON,
						userShareUrl.getUrlIcon());
			if (null != userShareUrl.getUrlTitle())
				lContentValues.put(TableConstants.UserShareUrlColumn.URLTITLE,
						userShareUrl.getUrlTitle());
			if (null != userShareUrl.getUrlDesc())
				lContentValues.put(
						TableConstants.UserShareUrlColumn.URLDESC,
						userShareUrl.getUrlDesc());
			if (null != userShareUrl.getUrlContent())
				lContentValues.put(
						TableConstants.UserShareUrlColumn.URLCONTENT,
						userShareUrl.getUrlContent());
		}
		return lContentValues;
	}
	public static ContentValues convertWelcomePicture(WelcomePicture welcomePicture)
	{
//		int seqId,String fileUrl,String localPath,int jumpType,String jumpValue 
		ContentValues lContentValues = null;
		if (null != welcomePicture)
		{
			lContentValues = new ContentValues();
				lContentValues.put(TableConstants.WelcomePicColumn.SEQID,
						welcomePicture.getSeqId());
			if (null != welcomePicture.getFileUrl())
				lContentValues.put(TableConstants.WelcomePicColumn.FILEURL,
						welcomePicture.getFileUrl());
			if (null != welcomePicture.getLocalPath())
				lContentValues.put(TableConstants.WelcomePicColumn.LOCALPATH,
						welcomePicture.getLocalPath());
				lContentValues.put(TableConstants.WelcomePicColumn.JUMPTYPE,
						welcomePicture.getJumpType());
			if (null != welcomePicture.getJumpValue())
				lContentValues.put(TableConstants.WelcomePicColumn.JUMPVALUE,
						welcomePicture.getJumpValue());
		}
		return lContentValues;
	}


	
	

	public static ContentValues convertRelation(Relation relation)
	{
		ContentValues lContentValues = null;
		if (null != relation)
		{
			lContentValues = new ContentValues();
			if (null != relation.getRelationId())
				lContentValues.put(TableConstants.RelationColumn.RELATIONID,
						relation.getRelationId());
			if (null != relation.getSubjectId())
				lContentValues.put(TableConstants.RelationColumn.SUBJECTID,
						relation.getSubjectId());
			if (null != relation.getTopicId())
				lContentValues.put(TableConstants.RelationColumn.TOPICID,
						relation.getTopicId());
			if (null != relation.getAlbumId())
				lContentValues.put(TableConstants.RelationColumn.ALBUMID,
						relation.getAlbumId());
			if (null != relation.getMusicId())
				lContentValues.put(TableConstants.RelationColumn.MUSICID,
						relation.getMusicId());
		}
		return lContentValues;
	}
	public static ContentValues convertMenu(Menu menu)
	{
		ContentValues lContentValues = null;
		if (null != menu)
		{
			lContentValues = new ContentValues();
				lContentValues.put(TableConstants.MenuColumn.CATEID,
						menu.cateId);
			if(menu.name!=null)
				lContentValues.put(TableConstants.MenuColumn.NAME,
						menu.name);
			if (null != menu.pic)
				lContentValues.put(TableConstants.MenuColumn.PICURL,
						menu.pic);
				lContentValues.put(TableConstants.MenuColumn.PARENTID,
						menu.parentId);
		}
		return lContentValues;
	}

	public static ContentValues convertNewestToast(NewestToast newestToast)
	{
		ContentValues lContentValues = null;
		if (null != newestToast)
		{
			lContentValues = new ContentValues();
			lContentValues.put(TableConstants.NewestToastColumn.TOASTID,
					newestToast.getToastId());
			if (null != newestToast.getToastContent())
				lContentValues.put(
						TableConstants.NewestToastColumn.TOASTCONTENT,
						newestToast.getToastContent());
			if (null != newestToast.getToastTime())
				lContentValues.put(TableConstants.NewestToastColumn.TOASTTIME,
						newestToast.getToastTime());
			lContentValues.put(TableConstants.NewestToastColumn.ISREAD,
					newestToast.getIsRead());
		}
		return lContentValues;
	}

	public static ContentValues convertViewPicture(ViewPicture viewPicture)
	{
		// Long seqId, Integer viewType, Integer jumpType,
		// String jumpValue, String title, String noteReplaceRule
		ContentValues lContentValues = null;
		if (null != viewPicture)
		{
			lContentValues = new ContentValues();
			if (null != viewPicture.getSeqId())
				lContentValues.put(TableConstants.ViewPictureColumn.SEQID,
						viewPicture.getSeqId());
			if (null != viewPicture.getViewType())
				lContentValues.put(TableConstants.ViewPictureColumn.VIEWTYPE,
						viewPicture.getViewType());
			if (null != viewPicture.getJumpType())
				lContentValues.put(TableConstants.ViewPictureColumn.JUMPTYPE,
						viewPicture.getJumpType());
			if (null != viewPicture.getJumpValue())
				lContentValues.put(TableConstants.ViewPictureColumn.JUMPVALUE,
						viewPicture.getJumpValue());
			if (null != viewPicture.getTitle())
				lContentValues.put(TableConstants.ViewPictureColumn.TITLE,
						viewPicture.getTitle());
			if (null != viewPicture.getNoteReplaceRule())
				lContentValues.put(
						TableConstants.ViewPictureColumn.NOTEREPLACERULE,
						viewPicture.getNoteReplaceRule());
			if (null != viewPicture.getImgUrl())
				lContentValues.put(
						TableConstants.ViewPictureColumn.IMGURL,
						viewPicture.getImgUrl());
		}
		return lContentValues;
	}

	// private Long seqId;
	// private String messageId;
	// private int messageType;
	// private Long timestamp;
	// private Long uid;
	// private Object contentObject;
	public static ContentValues convertMessage(Message message)
	{
		ContentValues lContentValues = null;
		if (null != message)
		{
			lContentValues = new ContentValues();
			if (null != message.getSeqId())
				lContentValues.put(TableConstants.MessageColumn.SEQID,
						message.getSeqId());
			if (null != message.getMessageId())
				lContentValues.put(TableConstants.MessageColumn.MESSAGEID,
						message.getMessageId());
			lContentValues.put(TableConstants.MessageColumn.MESSAGETYPE,
					message.getMessageType());
			if (null != message.getTimestamp())
				lContentValues.put(TableConstants.MessageColumn.TIMESTAMP,
						message.getTimestamp());
			if (null != message.getUid())
				lContentValues.put(TableConstants.MessageColumn.UID,
						message.getUid());
			if (null != message.getContentObject())
				lContentValues.put(TableConstants.MessageColumn.CONTENTOBJECT,
						message.getContentObject());
		}
		return lContentValues;
	}

	public static ContentValues convertQuestion(Question question)
	{
		ContentValues lContentValues = null;
		if (null != question)
		{
			lContentValues = new ContentValues();
			if (null != question.getSeqId())
				lContentValues.put(TableConstants.QuestionColumn.SEQID,
						question.getSeqId());
			if (null != question.getQuestionId())
				lContentValues.put(TableConstants.QuestionColumn.QUESTIONID,
						question.getQuestionId());
			if (null != question.getContent())
				lContentValues.put(TableConstants.QuestionColumn.CONTENT,
						question.getContent());
			if (null != question.getUid())
				lContentValues.put(TableConstants.QuestionColumn.UID,
						question.getUid());
			if (null != question.getPostTime())
				lContentValues.put(TableConstants.QuestionColumn.POSTTIME,
						question.getPostTime());
			lContentValues.put(TableConstants.QuestionColumn.VIEWCOUNT,
					question.getViewCount());
			lContentValues.put(TableConstants.QuestionColumn.COMMENTCOUNT,
					question.getCommentCount());
			if (null != question.getIsTop())
				lContentValues.put(TableConstants.QuestionColumn.ISTOP,
						question.getIsTop());
			if (null != question.getBestAnswerStatus())
				lContentValues.put(
						TableConstants.QuestionColumn.BESTANSWERSTATUS,
						question.getBestAnswerStatus());
			if (null != question.getIsFav())
				lContentValues.put(TableConstants.QuestionColumn.ISFAV,
						question.getIsFav());
			lContentValues.put(TableConstants.QuestionColumn.ISNEWEST,
					question.getIsNewest());
			lContentValues.put(TableConstants.QuestionColumn.ISRECOMMEND,
					question.getIsRecommend());
			lContentValues.put(TableConstants.QuestionColumn.ISRACE,
					question.getIsRace());
			lContentValues.put(TableConstants.QuestionColumn.ISMYQUESTION,
					question.getIsMyQuestion());
			lContentValues.put(TableConstants.QuestionColumn.MAXSEQID,
					question.getMaxSeqId());
			lContentValues.put(TableConstants.QuestionColumn.ISMYFAV,
					question.getIsMyFav());
			if (question.getIsHot() != null)
				lContentValues.put(TableConstants.QuestionColumn.ISHOT,
						question.getIsHot());
			if (question.getHotTime() != null)
				lContentValues.put(TableConstants.QuestionColumn.HOTTIME,
						question.getHotTime());
			if (question.getReward() != null)
				lContentValues.put(TableConstants.QuestionColumn.REWARD,
						question.getReward());
			if (question.getBoardId() != null)
				lContentValues.put(TableConstants.QuestionColumn.BOARDID,
						question.getBoardId());
			if (question.getCity() != null)
				lContentValues.put(TableConstants.QuestionColumn.CITY,
						question.getCity());
			if (question.getBbStatus() != null)
				lContentValues.put(TableConstants.QuestionColumn.BBSTATUS,
						question.getBbStatus());
			if (question.getBabyBirthday() != null)
				lContentValues.put(TableConstants.QuestionColumn.BABYBIRTHDAY,
						question.getBabyBirthday());
		}
		return lContentValues;
	}

	// String topicId, Long seqId, String title, String content,
	// Long boardId, User user, String postTime, Integer viewCount,
	// Integer commentCount, String username, String babyBirthday,
	// Byte postFrom, Byte isTop, Byte isRecommended, Byte isMainPageShow,
	// List<Attachment> attachmentList, Long lastReplyTime,Byte isFav
	// ,Long recommendedTime,Long mainPageShowTime
	public static ContentValues convertTopic(Topic topic)
	{
		ContentValues lContentValues = null;
		if (null != topic)
		{
			lContentValues = new ContentValues();
			if (null != topic.getTopicId())
				lContentValues.put(TableConstants.TopicColumn.TOPICID,
						topic.getTopicId());
			if (null != topic.getSeqId())
				lContentValues.put(TableConstants.TopicColumn.SEQID,
						topic.getSeqId());
			if (null != topic.getTitle())
				lContentValues.put(TableConstants.TopicColumn.TITLE,
						topic.getTitle());
			if (null != topic.getContent())
				lContentValues.put(TableConstants.TopicColumn.CONTENT,
						topic.getContent());
			if (null != topic.getBoardId())
				lContentValues.put(TableConstants.TopicColumn.BOARDID,
						topic.getBoardId());
			if (null != topic.getUid())
				lContentValues.put(TableConstants.TopicColumn.UID,
						topic.getUid());
			if (null != topic.getPostTime())
				lContentValues.put(TableConstants.TopicColumn.POSTTIME,
						topic.getPostTime());
			if (null != topic.getViewCount())
				lContentValues.put(TableConstants.TopicColumn.VIEWCOUNT,
						topic.getViewCount());
			if (null != topic.getCommentCount())
				lContentValues.put(TableConstants.TopicColumn.COMMENTCOUNT,
						topic.getCommentCount());
			if (null != topic.getUsername())
				lContentValues.put(TableConstants.TopicColumn.USERNAME,
						topic.getUsername());
			if (null != topic.getBabyBirthday())
				lContentValues.put(TableConstants.TopicColumn.BABYBIRTHDAY,
						topic.getBabyBirthday());
			if (null != topic.getPostFrom())
				lContentValues.put(TableConstants.TopicColumn.POSTFROM,
						topic.getPostFrom());
			if (null != topic.getIsTop())
				lContentValues.put(TableConstants.TopicColumn.ISTOP,
						topic.getIsTop());
			if (null != topic.getIsRecommended())
				lContentValues.put(TableConstants.TopicColumn.ISRECOMMENDED,
						topic.getIsRecommended());
			if (null != topic.getIsMainPageShow())
				lContentValues.put(TableConstants.TopicColumn.ISMAINPAGESHOW,
						topic.getIsMainPageShow());
			if (null != topic.getLastReplyTime())
				lContentValues.put(TableConstants.TopicColumn.LASTREPLYTIME,
						topic.getLastReplyTime());
			if (null != topic.getIsFav())
				lContentValues.put(TableConstants.TopicColumn.ISFAV,
						topic.getIsFav());
			if (null != topic.getRecommendedTime())
				lContentValues.put(TableConstants.TopicColumn.RECOMMENDEDTIME,
						topic.getRecommendedTime());
			if (null != topic.getMainPageShowTime())
				lContentValues.put(TableConstants.TopicColumn.MAINPAGESHOWTIME,
						topic.getMainPageShowTime());
			lContentValues.put(TableConstants.TopicColumn.ISNEWESTREPLY,
					topic.getIsNewestReply());
			lContentValues.put(TableConstants.TopicColumn.ISNEWESTTOPIC,
					topic.getIsNewestTopic());
			lContentValues.put(TableConstants.TopicColumn.ISMARROWTOPIC,
					topic.getIsMarrowTopic());
			lContentValues.put(TableConstants.TopicColumn.MAXPOSTSEQID,
					topic.getMaxPostSeqId());
			if (null != topic.getTopicType())
				lContentValues.put(TableConstants.TopicColumn.TOPICTYPE,
						topic.getTopicType());
			if (null != topic.getReferId())
				lContentValues.put(TableConstants.TopicColumn.REFREID,
						topic.getReferId());
			
			if (topic.getCity() != null)
				lContentValues.put(TableConstants.TopicColumn.CITY,
						topic.getCity());
			if (topic.getBbStatus() != null)
				lContentValues.put(TableConstants.TopicColumn.BBSTATUS,
						topic.getBbStatus());
			if (topic.getIsHot() != null)
				lContentValues.put(TableConstants.TopicColumn.ISHOT,
						topic.getIsHot());
			if (topic.getHotTime() != null)
				lContentValues.put(TableConstants.TopicColumn.HOTTIME,
						topic.getHotTime());

			
		}
		return lContentValues;
	}

	
	

	
	
	public static ContentValues convertPortal(Portal portal)
	{
		ContentValues lContentValues = null;
		if (null != portal)
		{
			lContentValues = new ContentValues();
			if (null != portal.getSeqId())
				lContentValues.put(TableConstants.PortalColumn.SEQID,
						portal.getSeqId());
			if (null != portal.getTitle())
				lContentValues.put(TableConstants.PortalColumn.TITLE,
						portal.getTitle());
			if (null != portal.getPicUrl())
				lContentValues.put(TableConstants.PortalColumn.PICURL,
						portal.getPicUrl());
			if (null != portal.getPostTime())
				lContentValues.put(TableConstants.PortalColumn.POSTTIME,
						portal.getPostTime());
			if (null != portal.getPortalType())
				lContentValues.put(TableConstants.PortalColumn.PROTALTYPE,
						portal.getPortalType());
			if (null != portal.getReferId())
				lContentValues.put(TableConstants.PortalColumn.REFREID,
						portal.getReferId());
		}
		return lContentValues;
	}

	public static ContentValues convertXiaoMiPushData(
			XiaoMiPushData xiaoMiPushData)
	{
		ContentValues lContentValues = null;
		if (null != xiaoMiPushData)
		{
			lContentValues = new ContentValues();
			if (null != xiaoMiPushData.getName())
				lContentValues.put(TableConstants.XiaoMiPushDataColumn.NAME,
						xiaoMiPushData.getName());
			lContentValues.put(TableConstants.XiaoMiPushDataColumn.STAUTS,
					xiaoMiPushData.getStatus());
			lContentValues.put(TableConstants.XiaoMiPushDataColumn.UNSTATUS,
					xiaoMiPushData.getUnStatus());
			lContentValues.put(TableConstants.XiaoMiPushDataColumn.TYPE,
					xiaoMiPushData.getType());
		}
		return lContentValues;
	}

	public static ContentValues convertPost(Post post)
	{
		ContentValues lContentValues = null;
		if (null != post)
		{
			lContentValues = new ContentValues();
			if (null != post.getPostId())
				lContentValues.put(TableConstants.PostColumn.POSTID,
						post.getPostId());
			if (null != post.getSeqId())
				lContentValues.put(TableConstants.PostColumn.SEQID,
						post.getSeqId());
			if (null != post.getTopicId())
				lContentValues.put(TableConstants.PostColumn.TOPICID,
						post.getTopicId());
			if (null != post.getPostTime())
				lContentValues.put(TableConstants.PostColumn.POSTTIME,
						post.getPostTime());
			if (null != post.getReferPostId())
				lContentValues.put(TableConstants.PostColumn.REFERPOSTID,
						post.getReferPostId());
			if (null != post.getContent())
				lContentValues.put(TableConstants.PostColumn.CONTENT,
						post.getContent());
			lContentValues.put(TableConstants.PostColumn.UID, post.getUid());
			if (null != post.getTopicContent())
				lContentValues.put(TableConstants.PostColumn.TOPICCONTENT,
						post.getTopicContent());
			if (null != post.getTopicNickname())
				lContentValues.put(TableConstants.PostColumn.TOPICNICKNAME,
						post.getTopicNickname());
			if (null != post.getReferPostContent())
				lContentValues.put(TableConstants.PostColumn.REFERPOSTCONTENT,
						post.getReferPostContent());
			lContentValues.put(TableConstants.PostColumn.ISMYRECEIVEPOST,
					post.getIsMyReceivePost());
			lContentValues.put(TableConstants.PostColumn.ISTOPICREPLY,
					post.getIsTopicReply());
		}
		return lContentValues;
	}

	
	public static ContentValues convertDrafts(Drafts drafts)
	{
		ContentValues lContentValues = null;
		if (null != drafts)
		{
			lContentValues = new ContentValues();
			if (null != drafts.getDraftsId())
				lContentValues.put(TableConstants.DraftsColumn.DRAFTSID,
						drafts.getDraftsId());
			if (null != drafts.getTitle())
				lContentValues.put(TableConstants.DraftsColumn.TITLE,
						drafts.getTitle());
			if (null != drafts.getContent())
				lContentValues.put(TableConstants.DraftsColumn.CONTENT,
						drafts.getContent());
				lContentValues.put(TableConstants.DraftsColumn.TYPE,
						drafts.getType());
		}
		return lContentValues;
	}

	
	public static ContentValues convertAttachment(Attachment attachment)
	{
		ContentValues lContentValues = null;
		if (null != attachment)
		{
			lContentValues = new ContentValues();
			if (null != attachment.getAttId())
				lContentValues.put(TableConstants.AttachmentColumn.ATTID,
						attachment.getAttId());
			if (null != attachment.getFileUrl())
				lContentValues.put(TableConstants.AttachmentColumn.FILEURL,
						attachment.getFileUrl());
			if (null != attachment.getAttSize())
				lContentValues.put(TableConstants.AttachmentColumn.ATTSIZE,
						attachment.getAttSize());
			if (null != attachment.getFileName())
				lContentValues.put(TableConstants.AttachmentColumn.FILENAME,
						attachment.getFileName());
			if (null != attachment.getQuestionId())
				lContentValues.put(TableConstants.AttachmentColumn.QUESTIONID,
						attachment.getQuestionId());
			if (null != attachment.getAnswerId())
				lContentValues.put(TableConstants.AttachmentColumn.ANSWERID,
						attachment.getAnswerId());
			if (null != attachment.getFileLocalPath())
				lContentValues.put(
						TableConstants.AttachmentColumn.FILELOCALPATH,
						attachment.getFileLocalPath());
			if (null != attachment.getTopicId())
				lContentValues.put(TableConstants.AttachmentColumn.TOPICID,
						attachment.getTopicId());
			if (null != attachment.getPostId())
				lContentValues.put(TableConstants.AttachmentColumn.POSTID,
						attachment.getPostId());
			if (null != attachment.getSubjectId())
				lContentValues.put(TableConstants.AttachmentColumn.SUBJECTID,
						attachment.getSubjectId());
			if (null != attachment.getBoardId())
				lContentValues.put(TableConstants.AttachmentColumn.BOARDID,
						attachment.getBoardId());
		}
		return lContentValues;
	}

	public static ContentValues convertUser(User user)
	{
		ContentValues lContentValues = null;
		if (null != user)
		{
			lContentValues = new ContentValues();
			if (user.getUid() != null)
				lContentValues
						.put(TableConstants.UserColumn.UID, user.getUid());
			if (user.getNickname() != null)
				lContentValues.put(TableConstants.UserColumn.NICKNAME,
						user.getNickname());
			if (user.getGender() != null)
				lContentValues.put(TableConstants.UserColumn.GENDER,
						user.getGender());
			if (user.getPerinatal() != null)
				lContentValues.put(TableConstants.UserColumn.PERINATAL,
						user.getPerinatal());
			if (user.getBabyBirthday() != null)
				lContentValues.put(TableConstants.UserColumn.BABYBIRTHDAY,
						user.getBabyBirthday());
			else
				lContentValues.put(TableConstants.UserColumn.BABYBIRTHDAY, "");
			if (user.getBabyGender() != null)
				lContentValues.put(TableConstants.UserColumn.BABYGENDER,
						user.getBabyGender());
			if (user.getProfileImage() != null)
				lContentValues.put(TableConstants.UserColumn.PROFILEIMAGE,
						user.getProfileImage());
			if (user.getEmail() != null)
				lContentValues.put(TableConstants.UserColumn.EMAIL,
						user.getEmail());
			if (user.getPassword() != null)
				lContentValues.put(TableConstants.UserColumn.PASSWORD,
						user.getPassword());
			if (user.getRegTime() != null)
				lContentValues.put(TableConstants.UserColumn.REGTIME,
						user.getRegTime());
			if (user.getToken() != null)
				lContentValues.put(TableConstants.UserColumn.TOKEN,
						user.getToken());
			if (user.getDeviceToken() != null)
				lContentValues.put(TableConstants.UserColumn.DEVICETOKEN,
						user.getDeviceToken());
//			lContentValues
//					.put(TableConstants.UserColumn.COINS, user.getCoins());
			lContentValues.put(TableConstants.UserColumn.CREDITS,
					user.getCredits());
			if (user.getIsShared() != null)
				lContentValues.put(TableConstants.UserColumn.ISSHARED,
						user.getIsShared());
			if (user.getIsSelf() != null)
				lContentValues.put(TableConstants.UserColumn.ISSELF,
						user.getIsSelf());
			lContentValues.put(TableConstants.UserColumn.QUESTIONCOUNT,
					user.getQuestionCount());
			lContentValues.put(TableConstants.UserColumn.ANSWERCOUNT,
					user.getAnswerCount());
			lContentValues.put(TableConstants.UserColumn.APPRECIATIONCOUNT,
					user.getAppreciationCount());
			if (user.getStatus() != null)
				lContentValues.put(TableConstants.UserColumn.STATUS,
						user.getStatus());
			if (user.getBgImageUrl() != null)
				lContentValues.put(TableConstants.UserColumn.BGIMAGEURL,
						user.getBgImageUrl());
			if (user.getIsCounsellor() != null)
				lContentValues.put(TableConstants.UserColumn.ISCOUNSELLOR,
						user.getIsCounsellor());
			lContentValues
					.put(TableConstants.UserColumn.LEVEL, user.getLevel());
			if (user.getDeviceId() != null)
				lContentValues.put(TableConstants.UserColumn.DEVICEID,
						user.getDeviceId());
			lContentValues.put(TableConstants.UserColumn.ACCINLASTWEEK,
					user.getAccInLastWeek());
			lContentValues.put(TableConstants.UserColumn.COINSYESTERDAY,
					user.getCoinsYesterday());
			if (user.getFollowStatus() != null)
				lContentValues.put(TableConstants.UserColumn.FOLLOWSTATUS,
						user.getFollowStatus());
			lContentValues.put(TableConstants.UserColumn.FANSCOUNT,
					user.getFansCount());
			lContentValues.put(TableConstants.UserColumn.ATTENTIONCOUNT,
					user.getAttentionCount());
			lContentValues.put(TableConstants.UserColumn.ISGUEST,
					user.getIsGuest());
			lContentValues.put(TableConstants.UserColumn.CITY, user.getCity());
			lContentValues.put(TableConstants.UserColumn.MEDALIDLISTSTR,
					user.getMedalIdListStr());
			if(user.getYcoins()!=null)
			lContentValues.put(TableConstants.UserColumn.YCOINS,
					user.getYcoins());
			lContentValues.put(TableConstants.UserColumn.FREEZEYCOINS,
					user.getFreezeYcoins());
		}
		return lContentValues;
	}

	public static ContentValues convertUserStatus(UserStatus userStatus)
	{
		ContentValues lContentValues = null;
		if (null != userStatus)
		{
			lContentValues = new ContentValues();
			if (userStatus.getUserStatusId() != null)
				lContentValues.put(
						TableConstants.UserStatusColumn.USERSTATUSID,
						userStatus.getUserStatusId());
			lContentValues.put(TableConstants.UserStatusColumn.TYPE,
					userStatus.getType());
			if (userStatus.getTime() != null)
				lContentValues.put(TableConstants.UserStatusColumn.TIME,
						userStatus.getTime());
			lContentValues.put(TableConstants.UserStatusColumn.PERIOD,
					userStatus.getPeriod());
			lContentValues.put(TableConstants.UserStatusColumn.CYCLE,
					userStatus.getCycle());
			lContentValues.put(TableConstants.UserStatusColumn.BABYGENDER,
					userStatus.getBabyGender());
		}
		return lContentValues;
	}
}
