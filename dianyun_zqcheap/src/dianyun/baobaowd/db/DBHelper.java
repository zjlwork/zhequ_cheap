package dianyun.baobaowd.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import dianyun.baobaowd.help.LogFile;

public class DBHelper extends SQLiteOpenHelper
{

	private static final byte[] dblock = new byte[0];
	private static int lock = 0;
	private static final String DATABASE_NAME = "baobaowd.db";
	// private static final String DATABASE_NAME = android.os.Environment
	// .getExternalStorageDirectory().getAbsolutePath()
	// + "/"
	// + "baobaowd.db";
	private static final int DATABASE_VERSION = 62;
	
	private static final String SQL_ATTACHMENT = "CREATE TABLE "
			+ TableConstants.TABLE_ATTACHMENT
			+ " (attId  TEXT PRIMARY KEY  NOT NULL, fileUrl TEXT NOT NULL, attSize LONG, fileName TEXT"
			+ ",fileLocalPath TEXT, questionId TEXT, answerId TEXT,topicId TEXT,postId TEXT,subjectId TEXT,boardId LONG )";
	private static final String SQL_USER = "CREATE TABLE "
			+ TableConstants.TABLE_USER
			+ " (uid LONG PRIMARY KEY NOT NULL, nickname TEXT ,gender INTEGER,"
			+ "perinatal TEXT,babyBirthday TEXT,babyGender INTEGER,profileImage TEXT,email TEXT,password TEXT,regTime TEXT,token TEXT,deviceToken TEXT,"
			+ "coins INTEGER,credits INTEGER,isShared INTEGER,isSelf INTEGER,questionCount INTEGER,answerCount INTEGER,appreciationCount INTEGER,status INTEGER,bgImageUrl TEXT,"
			+ "isCounsellor INTEGER,level INTEGER,deviceId TEXT,accInLastWeek INTEGER, coinsYesterday INTEGER,followStatus INTEGER,"
			+ "fansCount INTEGER,attentionCount INTEGER,isGuest INTEGER,city TEXT,medalIdListStr TEXT,ycoins INTEGER,freezeYcoins INTEGER)";
	
	private static final String SQL_USERSTATUS = "CREATE TABLE "
			+ TableConstants.TABLE_USERSTATUS
			+ " (userStatusId TEXT PRIMARY KEY NOT NULL,type INTEGER ,time TEXT,"
			+ " period INTEGER, cycle INTEGER, babyGender  INTEGER )";
	
	
	
	private static final String SQL_SYSTEMMSG = "CREATE TABLE "
			+ TableConstants.TABLE_SYSTEMMSG
			+ " (seqId LONG PRIMARY KEY NOT NULL, messageId TEXT ,messageType INTEGER,"
			+ "timestamp LONG, uid LONG, contentObject TEXT)";
	
	
	private static final String SQL_XIAOMIPUSHDATA = "CREATE TABLE "
			+ TableConstants.TABLE_XIAOMIPUSHDATA
			+ " (name TEXT PRIMARY KEY NOT NULL, status INTEGER ,unStatus INTEGER,type INTEGER)";
	




	/**
	 * 创建电子商城的Tab页面的Menu 表
	 */
	private static final String CREATE_TABLE_SHOP_MENU = "CREATE TABLE "
			+ TableConstants.TABLE_SHOP_MENU
			+ " (cateId long  PRIMARY  KEY NOT NULL,name varchar(20) NOT NULL,picUrl text,parentId long  NOT NULL)";
	
//	private static final String CREATE_TABLE_SHOP_MENU_CHILDDATA = "CREATE TABLE "
//			+ TableConstants.TABLE_SHOP_MENU_CHILDS
//			+ " (menuid long  PRIMARY  KEY NOT NULL,childData text)";
	/**
	 * 创建菜单子类的内容数据表
	 */
//	private static final String CREATE_TABLE_SHOP_MENU_CHILDADTA_CONTENT = "CREATE TABLE "
//			+ TableConstants.TABLE_SHOP_CONTENT
//			+ " (menuId long  NOT NULL,cateId long  NOT NULL,childData text)";
	
	private static final String CREATE_HIGHLEST_BACK_TABLE="CREATE TABLE "
            + TableConstants.TABLE_MAIN_HIGHLEST_BACKFEE
            + " (childData text)";
	private static final String CREATE_FACUSPICTURE_TABLE="CREATE TABLE "
			+ TableConstants.TABLE_FACUSPICTURE
			+ " (childData text)";
	private static final String CREATE_MIAOSHAPICTURE_TABLE="CREATE TABLE "
			+ TableConstants.TABLE_MIAOSHAPICTURE
			+ " (childData text)";
	
	private static final  String SQL_USERSHAREURL="CREATE TABLE "
			+ TableConstants.TABLE_USERSHAREURL
			+ " (urlKey TEXT PRIMARY KEY NOT NULL,appId INTEGER,urlValue TEXT,urlIcon TEXT, urlTitle TEXT" +
			",urlDesc TEXT,urlContent TEXT)";
	private static final  String SQL_WELCOMEPICTURE="CREATE TABLE "
			+ TableConstants.TABLE_WELCOMEPICTURE
			+ " (seqId INTEGER PRIMARY KEY NOT NULL,fileUrl Text,localPath TEXT,jumpType INTEGER, jumpValue TEXT)";
	
//	String urlKey, Integer appId, String urlValue,
//	String urlIcon, String urlTitle, String urlDesc, String urlContent
	
	
	private static DBHelper mDBHelper = null;
	private static SQLiteDatabase mSQLiteWriter;

	// private static SQLiteDatabase mSQLiteReader;
	public DBHelper(Context context)
	{
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	public static DBHelper getInstance(Context context)
	{
		synchronized (dblock)
		{
			if (mDBHelper == null)
			{
				mDBHelper = new DBHelper(context);
			}
			return mDBHelper;
		}
	}

	/**
	 * 当数据库首次创建时执行该方法，一般将创建表等初始化操作放在该方法中执�? 重写onCreate方法，调用execSQL方法创建�?
	 * */
	@Override
	public void onCreate(SQLiteDatabase db)
	{
		System.out.println("db onCreate-------------");
		
		db.execSQL(SQL_USER);
		db.execSQL(SQL_USERSTATUS);
		db.execSQL(SQL_ATTACHMENT);
		
		db.execSQL(SQL_SYSTEMMSG);
		db.execSQL(SQL_XIAOMIPUSHDATA);
		
		db.execSQL(CREATE_HIGHLEST_BACK_TABLE);
		db.execSQL(CREATE_FACUSPICTURE_TABLE);
		db.execSQL(CREATE_MIAOSHAPICTURE_TABLE);
		
		db.execSQL(CREATE_TABLE_SHOP_MENU);
//		db.execSQL(CREATE_TABLE_SHOP_MENU_CHILDDATA);
//		db.execSQL(CREATE_TABLE_SHOP_MENU_CHILDADTA_CONTENT);
		db.execSQL(SQL_USERSHAREURL);
		db.execSQL(SQL_WELCOMEPICTURE);
	}

	/**
	 * 当打�?��据库时传入的版本号与当前的版本号不同时会调用该方�?
	 */
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion)
	{
		System.out.println("db onUpgrade-------------");
		try
		{
			// System.out.println("开始迁移--------" + System.currentTimeMillis());
			db.beginTransaction();
			if (oldVersion < newVersion)
			{
				db.execSQL("DROP TABLE IF EXISTS "
						+ TableConstants.TABLE_USERSTATUS);
				
				db.execSQL("DROP TABLE IF EXISTS "
						+ TableConstants.TABLE_SYSTEMMSG);

                db.execSQL("DROP TABLE IF EXISTS "
                        + TableConstants.TABLE_ATTACHMENT);
			
				
				db.execSQL("DROP TABLE IF EXISTS "
						+ TableConstants.TABLE_XIAOMIPUSHDATA);
                db.execSQL("DROP TABLE IF EXISTS "
                        + TableConstants.TABLE_MAIN_HIGHLEST_BACKFEE);
                db.execSQL("DROP TABLE IF EXISTS "
                		+ TableConstants.TABLE_FACUSPICTURE);
                db.execSQL("DROP TABLE IF EXISTS "
                		+ TableConstants.TABLE_MIAOSHAPICTURE);
				
				db.execSQL("DROP TABLE IF EXISTS "
						+ TableConstants.TABLE_SHOP_MENU);
				db.execSQL("DROP TABLE IF EXISTS "
						+ TableConstants.TABLE_SHOP_MENU_CHILDS);
				db.execSQL("DROP TABLE IF EXISTS "
						+ TableConstants.TABLE_SHOP_CONTENT);
				db.execSQL("DROP TABLE IF EXISTS "
						+ TableConstants.TABLE_USERSHAREURL);
				db.execSQL("DROP TABLE IF EXISTS "
						+ TableConstants.TABLE_WELCOMEPICTURE);
				
				
				db.execSQL(SQL_USERSTATUS);
				db.execSQL(SQL_ATTACHMENT);
				db.execSQL(SQL_SYSTEMMSG);
				db.execSQL(SQL_XIAOMIPUSHDATA);
				db.execSQL(CREATE_TABLE_SHOP_MENU);
//				db.execSQL(CREATE_TABLE_SHOP_MENU_CHILDADTA_CONTENT);
//				db.execSQL(CREATE_TABLE_SHOP_MENU_CHILDDATA);
                db.execSQL(CREATE_HIGHLEST_BACK_TABLE);
                db.execSQL(CREATE_FACUSPICTURE_TABLE);
                db.execSQL(CREATE_MIAOSHAPICTURE_TABLE);
                db.execSQL(SQL_USERSHAREURL);
                db.execSQL(SQL_WELCOMEPICTURE);
                
                
				migrationUserDB(db, oldVersion);
			}
			db.setTransactionSuccessful();
			// System.out.println("结束迁移--------" + System.currentTimeMillis());
		}
		catch (Exception e)
		{
			LogFile.SaveExceptionLog(e);
		}
		finally
		{
			db.endTransaction();
		}
	}

	private void migrationUserDB(SQLiteDatabase db, int oldVersion)
	{
//		if (oldVersion >= 37)
//		{
//			db.execSQL("ALTER TABLE user RENAME TO temp_user");
//			db.execSQL(SQL_USER);
//			db.execSQL("INSERT INTO user SELECT uid,nickname ,gender,perinatal,babyBirthday,babyGender, profileImage,email,password,regTime,token,deviceToken,coins,"
//					+ "credits,isShared,isSelf,questionCount,answerCount,appreciationCount,status,bgImageUrl,isCounsellor,level,deviceId,accInLastWeek,coinsYesterday,followStatus,fansCount,attentionCount,isGuest,'','' FROM temp_user");
//			db.execSQL("DROP TABLE temp_user");
//		}
//		else
//		{
			db.execSQL("ALTER TABLE user RENAME TO temp_user");
			db.execSQL(SQL_USER);
			db.execSQL("INSERT INTO user SELECT uid,nickname ,gender,perinatal,babyBirthday,babyGender, profileImage,email,password,regTime,token,deviceToken,coins,"
					+ "credits,isShared,isSelf,questionCount,answerCount,appreciationCount,status,bgImageUrl,isCounsellor,level,deviceId,accInLastWeek,coinsYesterday," +
					"followStatus,fansCount,attentionCount,isGuest,city,medalIdListStr,'','' FROM temp_user");
			db.execSQL("DROP TABLE temp_user");
//		}
	}

	public SQLiteDatabase getDatabase()
	{
		synchronized (dblock)
		{
			if (mSQLiteWriter == null || !mSQLiteWriter.isOpen())
				mSQLiteWriter = mDBHelper.getWritableDatabase();
			return mSQLiteWriter;
		}
	}

	public void deleteAllData()
	{
		getDatabase();

		mSQLiteWriter.delete(TableConstants.TABLE_USER, "isGuest!=1", null);
		mSQLiteWriter.delete(TableConstants.TABLE_SYSTEMMSG, "1=1", null);
		mSQLiteWriter.delete(TableConstants.TABLE_USERSHAREURL, "1=1", null);

		closeDB();
	}

	/**
	 * 关闭数据�?
	 */
	public void closeDB()
	{
		// System.out.println("closeDB"+this);
		if (null != mSQLiteWriter)
		{
			mSQLiteWriter.close();
			mSQLiteWriter = null;
		}
		if (mDBHelper != null)
			mDBHelper.close();
		mDBHelper = null;
	}
}
