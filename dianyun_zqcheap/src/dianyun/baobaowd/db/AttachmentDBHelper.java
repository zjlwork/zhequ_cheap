package dianyun.baobaowd.db;

import java.util.ArrayList;
import java.util.List;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import dianyun.baobaowd.data.Attachment;

public class AttachmentDBHelper extends BaseDBHelper
{


	private static final String[] attachmentallColumn = new String[] {
			TableConstants.AttachmentColumn.ATTID,
			TableConstants.AttachmentColumn.FILEURL,
			TableConstants.AttachmentColumn.ATTSIZE,
			TableConstants.AttachmentColumn.FILENAME,
			TableConstants.AttachmentColumn.FILELOCALPATH,
			TableConstants.AttachmentColumn.QUESTIONID,
			TableConstants.AttachmentColumn.ANSWERID,
			TableConstants.AttachmentColumn.TOPICID,
			TableConstants.AttachmentColumn.POSTID,
			TableConstants.AttachmentColumn.SUBJECTID,
			TableConstants.AttachmentColumn.BOARDID
			
			};

	public AttachmentDBHelper()
	{
		super();
	}

	public AttachmentDBHelper(Context context, String table)
	{
		super(context, table);
	}

	protected AttachmentDBHelper(Context context, String table,
			ContentValues values)
	{
		super(context, table, values);
	}

	public List<Attachment> getAttachmentsByQuestionId(String questionId)
	{
		if (null == mDBHelper)
			mDBHelper = DBHelper.getInstance(mContext);
		Cursor lCursor = mDBHelper.getDatabase().query(mTable,
				attachmentallColumn, "questionId = ?",
				new String[] { questionId }, null, null, null);
		List<Attachment> attachmentList = null;
		if (lCursor.moveToFirst())
		{
			attachmentList = new ArrayList<Attachment>();
			do
			{
				Attachment attachment = new Attachment(lCursor.getString(0),
						lCursor.getString(1), lCursor.getLong(2),
						lCursor.getString(3), lCursor.getString(4));
				attachment.setQuestionId(lCursor.getString(5));
				attachmentList.add(attachment);
			}
			while (lCursor.moveToNext());
		}
		lCursor.close();
		return attachmentList;
	}
	

	public List<Attachment> getAttachmentsByBoardId(long boardId)
	{
		if (null == mDBHelper)
			mDBHelper = DBHelper.getInstance(mContext);
		Cursor lCursor = mDBHelper.getDatabase().query(mTable,
				attachmentallColumn, "boardId = ?",
				new String[] { String.valueOf(boardId) }, null, null, null);
		List<Attachment> attachmentList = null;
		if (lCursor.moveToFirst())
		{
			attachmentList = new ArrayList<Attachment>();
			do
			{
				Attachment attachment = new Attachment(lCursor.getString(0),
						lCursor.getString(1), lCursor.getLong(2),
						lCursor.getString(3), lCursor.getString(4));
				attachment.setBoardId(lCursor.getLong(10));
				attachmentList.add(attachment);
			}
			while (lCursor.moveToNext());
		}
		lCursor.close();
		return attachmentList;
	}


	public List<Attachment> getAttachmentsByTopicId(String topicId)
	{
		if (null == mDBHelper)
			mDBHelper = DBHelper.getInstance(mContext);
		Cursor lCursor = mDBHelper.getDatabase().query(mTable,
				attachmentallColumn, "topicId = ?", new String[] { topicId },
				null, null, null);
		List<Attachment> attachmentList = null;
		if (lCursor.moveToFirst())
		{
			attachmentList = new ArrayList<Attachment>();
			do
			{
				Attachment attachment = new Attachment(lCursor.getString(0),
						lCursor.getString(1), lCursor.getLong(2),
						lCursor.getString(3), lCursor.getString(4));
				attachment.setTopicId(lCursor.getString(7));
				attachmentList.add(attachment);
			}
			while (lCursor.moveToNext());
		}
		lCursor.close();
		return attachmentList;
	}

	public List<Attachment> getAttachmentsBySubjectId(String subjectId)
	{
		if (null == mDBHelper)
			mDBHelper = DBHelper.getInstance(mContext);
		Cursor lCursor = mDBHelper.getDatabase().query(mTable,
				attachmentallColumn, "subjectId = ?",
				new String[] { subjectId }, null, null, null);
		List<Attachment> attachmentList = null;
		if (lCursor.moveToFirst())
		{
			attachmentList = new ArrayList<Attachment>();
			do
			{
				Attachment attachment = new Attachment(lCursor.getString(0),
						lCursor.getString(1), lCursor.getLong(2),
						lCursor.getString(3), lCursor.getString(4));
				attachment.setSubjectId(lCursor.getString(9));
				attachmentList.add(attachment);
			}
			while (lCursor.moveToNext());
		}
		lCursor.close();
		return attachmentList;
	}

	public List<Attachment> getAttachmentsByPostId(String postId)
	{
		if (null == mDBHelper)
			mDBHelper = DBHelper.getInstance(mContext);
		Cursor lCursor = mDBHelper.getDatabase().query(mTable,
				attachmentallColumn, "postId = ?", new String[] { postId },
				null, null, null);
		List<Attachment> attachmentList = null;
		if (lCursor.moveToFirst())
		{
			attachmentList = new ArrayList<Attachment>();
			do
			{
				Attachment attachment = new Attachment(lCursor.getString(0),
						lCursor.getString(1), lCursor.getLong(2),
						lCursor.getString(3), lCursor.getString(4));
				attachment.setPostId(lCursor.getString(8));
				attachmentList.add(attachment);
			}
			while (lCursor.moveToNext());
		}
		lCursor.close();
		return attachmentList;
	}

	public List<Attachment> getAttachmentsByAnswerId(String answerId)
	{
		if (null == mDBHelper)
			mDBHelper = DBHelper.getInstance(mContext);
		Cursor lCursor = mDBHelper.getDatabase().query(mTable,
				attachmentallColumn, "answerId = ?", new String[] { answerId },
				null, null, null);
		List<Attachment> attachmentList = null;
		if (lCursor.moveToFirst())
		{
			attachmentList = new ArrayList<Attachment>();
			do
			{
				Attachment attachment = new Attachment(lCursor.getString(0),
						lCursor.getString(1), lCursor.getLong(2),
						lCursor.getString(3), lCursor.getString(4));
				attachment.setAnswerId(lCursor.getString(6));
				attachmentList.add(attachment);
			}
			while (lCursor.moveToNext());
		}
		lCursor.close();
		return attachmentList;
	}
	
	

	
	
	
	

	public long insert(Attachment attachment)
	{
		mValues = ContentValuesUtil.convertAttachment(attachment);
		return insertDB();
	}

	public long update(Attachment attachment)
	{
		mValues = ContentValuesUtil.convertAttachment(attachment);
		mWhereClaus = TableConstants.AttachmentColumn.ATTID + "=?";
		mWhereArgs = new String[] { attachment.getAttId() };
		return updateDB();
	}

	public long delete(Attachment attachment)
	{
		mWhereClaus = TableConstants.AttachmentColumn.ATTID + "=?";
		mWhereArgs = new String[] { attachment.getAttId() };
		return delDB();
	}
	public long deleteByQuestionId(String questionId)
	{
		mWhereClaus = TableConstants.AttachmentColumn.QUESTIONID + "=?";
		mWhereArgs = new String[] {questionId };
		return delDB();
	}
	public long deleteByAnswerId(String answerId)
	{
		mWhereClaus = TableConstants.AttachmentColumn.ANSWERID + "=?";
		mWhereArgs = new String[] {answerId };
		return delDB();
	}
	public long deleteByTopicId(String topicId)
	{
		mWhereClaus = TableConstants.AttachmentColumn.TOPICID + "=?";
		mWhereArgs = new String[] {topicId };
		return delDB();
	}
	public long deleteByPostId(String postId)
	{
		mWhereClaus = TableConstants.AttachmentColumn.POSTID + "=?";
		mWhereArgs = new String[] {postId };
		return delDB();
	}
	public long deleteBySubjectId(String subjectId)
	{
		mWhereClaus = TableConstants.AttachmentColumn.SUBJECTID + "=?";
		mWhereArgs = new String[] {subjectId };
		return delDB();
	}
	

	public long deleteBoardAttachment()
	{
		mWhereClaus = TableConstants.AttachmentColumn.BOARDID + ">?";
		mWhereArgs = new String[] { String.valueOf(0) };
		return delDB();
	}

	public boolean isExist(String attId)
	{
		return isExist(TableConstants.AttachmentColumn.ATTID, attId);
	}
}
