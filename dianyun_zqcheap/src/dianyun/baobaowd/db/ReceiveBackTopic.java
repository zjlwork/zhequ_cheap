package dianyun.baobaowd.db;

public class ReceiveBackTopic
{

	private String topicId;
	private String msg;

	ReceiveBackTopic()
	{
	}

	public String getTopicId()
	{
		return topicId;
	}

	public void setTopicId(String topicId)
	{
		this.topicId = topicId;
	}

	public String getMsg()
	{
		return msg;
	}

	public void setMsg(String msg)
	{
		this.msg = msg;
	}
}
