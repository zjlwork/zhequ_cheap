package dianyun.baobaowd.db;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import dianyun.baobaowd.entity.Menu;

public class MenuDBHelper extends BaseDBHelper
{


	private static final String[] allColumn = new String[] {
			TableConstants.MenuColumn.CATEID, TableConstants.MenuColumn.NAME,
			TableConstants.MenuColumn.PICURL,
			TableConstants.MenuColumn.PARENTID
			};

	public MenuDBHelper()
	{
		super();
	}

	public MenuDBHelper(Context context, String table)
	{
		super(context, table);
	}

	protected MenuDBHelper(Context context, String table, ContentValues values)
	{
		super(context, table, values);
	}
	
	
	
//	private List<Menu> getFirstStairMenu()
//	{
//		if (null == mDBHelper)
//			mDBHelper = DBHelper.getInstance(mContext);
//		Cursor lCursor = mDBHelper.getDatabase().query(mTable, allColumn,
//				TableConstants.MenuColumn.PARENTID+" = 0", null, null, null, null);
//		List<Menu> menuList = null;
//		if (lCursor.moveToFirst())
//		{
//			menuList = new ArrayList<Menu>();
//			do
//			{
//				
//				Menu menu = new Menu(lCursor.getLong(0),lCursor.getString(1),lCursor.getString(2),lCursor.getLong(3));
//				menuList.add(menu);
//			}
//			while (lCursor.moveToNext());
//			
//		}
//		lCursor.close();
//		return menuList;
//	}
	

	public List<Menu> getMenuByParentId(long parntId)
	{
		Cursor lCursor = mDBHelper.getDatabase().query(mTable,
				allColumn, "parentId = ?",
				new String[] { String.valueOf(parntId) }, null, null, null);
		List<Menu> menuList = null;
		if (lCursor.moveToFirst())
		{
			menuList = new ArrayList<Menu>();
			do
			{
				Menu menu = new Menu(lCursor.getLong(0),lCursor.getString(1),lCursor.getString(2),lCursor.getLong(3));
				menuList.add(menu);
			}
			while (lCursor.moveToNext());
			
		}
		lCursor.close();
		return menuList;
	}

	public long deleteMenuByParentId(long parntId){
		mWhereClaus = TableConstants.MenuColumn.PARENTID + "=?";
		mWhereArgs = new String[] { String.valueOf(parntId) };
		return delDB();
	}
	public long insert(Menu menu)
	{
		mValues = ContentValuesUtil.convertMenu(menu);
		return insertDB();
	}

	public long update(Menu menu)
	{
		mValues = ContentValuesUtil.convertMenu(menu);
		mWhereClaus = TableConstants.MenuColumn.CATEID + "=?";
		mWhereArgs = new String[] { String.valueOf(menu.cateId) };
		return updateDB();
	}

	
	public long delete(Menu menu)
	{
		mWhereClaus = TableConstants.MenuColumn.CATEID + "=?";
		mWhereArgs = new String[] { String.valueOf(menu.cateId) };
		return delDB();
	}

	public boolean isExist(long seqId)
	{
		return isExist(TableConstants.MenuColumn.CATEID, String.valueOf(seqId));
	}
}
