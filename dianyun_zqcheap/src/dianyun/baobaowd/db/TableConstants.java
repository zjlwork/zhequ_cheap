package dianyun.baobaowd.db;

public class TableConstants
{

	/**
	 * 电子商城的Menu表
	 */
	public static String TABLE_SHOP_MENU = "shopMenu";
	public static String TABLE_SHOP_MENU_CHILDS = "shopMenuChilds";

	/**
	 * 电子商城的Menu的内容表
	 */
	public static String TABLE_SHOP_CONTENT = "shopMenuChildsContent";

    /**
     * 首页的最高返利
     */
    public static String TABLE_MAIN_HIGHLEST_BACKFEE = "mainFragmentHiglestBackFees";
    
    public static String TABLE_FACUSPICTURE = "mainfragmentfocuspicture";
    public static String TABLE_MIAOSHAPICTURE = "mainfragmentmiaoshapicture";


    public static String TABLE_ATTACHMENT = "attachment";
	public static String TABLE_USER = "user";
	public static String TABLE_USERSHAREURL = "UserShareUrl";
	public static String TABLE_USERSTATUS = "userStatus";
	public static String TABLE_SYSTEMMSG = "systemmsg";
	public static String TABLE_XIAOMIPUSHDATA = "xiaoMiPushData";
	public static String TABLE_WELCOMEPICTURE = "welcomePicture";

	public static class AttentionUserColumn
	{

		public static String TOUID = "toUid";
		public static String STATUS = "status";
	}

	// String relationId, long uid, int medalId
	public static class UserMedalColumn
	{

		public static String RELATIONID = "relationId";
		public static String UID = "uid";
		public static String MEDALID = "medalId";
	}

	public static class MedalColumn
	{

		// Integer medalId, String title, String medalDesc,
		// String smallPic, String bigPic, byte isHad
		public static String MEDALID = "medalId";
		public static String TITLE = "title";
		public static String MEDALDESC = "medalDesc";
		public static String SMALLPIC = "smallPic";
		public static String BIGPIC = "bigPic";
		public static String ISHAD = "isHad";
	}
	public static class MenuColumn
	{
		
//		Long cateId,String name,String picUrl,Long parentId
		public static String CATEID = "cateId";
		public static String NAME = "name";
		public static String PICURL = "picUrl";
		public static String PARENTID = "parentId";
	}
//	int seqId,String fileUrl,String localPath,int jumpType,String jumpValue 
	public static class WelcomePicColumn
	{
		
//		Long cateId,String name,String picUrl,Long parentId
		public static String SEQID = "seqId";
		public static String FILEURL = "fileUrl";
		public static String LOCALPATH = "localPath";
		public static String JUMPTYPE = "jumpType";
		public static String JUMPVALUE = "jumpValue";
	}

	public static class XiaoMiPushDataColumn
	{

		public static String NAME = "name";
		public static String STAUTS = "status";
		public static String UNSTATUS = "unStatus";
		public static String TYPE = "type";
	}

	// Long seqId, String title, String picUrl, String postTime
	public static class PortalColumn
	{

		public static String SEQID = "seqId";
		public static String TITLE = "title";
		public static String PICURL = "picUrl";
		public static String POSTTIME = "postTime";
		public static String PROTALTYPE = "portalType";
		public static String REFREID = "referId";
	}

	public static class QuestionColumn
	{

		public static String QUESTIONID = "questionId";
		public static String SEQID = "seqId";
		public static String CONTENT = "content";
		public static String UID = "uid";
		public static String POSTTIME = "postTime";
		public static String VIEWCOUNT = "viewCount";
		public static String COMMENTCOUNT = "commentCount";
		public static String ISTOP = "isTop";
		public static String BESTANSWERSTATUS = "bestAnswerStatus";
		public static String ISFAV = "isFav";
		public static String ISNEWEST = "isNewest";
		public static String ISRECOMMEND = "isRecommend";
		public static String ISRACE = "isRace";
		public static String ISMYQUESTION = "isMyQuestion";
		public static String MAXSEQID = "maxSeqId";
		public static String ISMYFAV = "isMyFav";
		public static String ISHOT = "isHot";
		public static String HOTTIME = "hotTime";
		public static String REWARD = "reward";
		public static String BOARDID = "boardId";
		public static String CITY = "city";
		public static String BBSTATUS = "bbStatus";
		public static String BABYBIRTHDAY = "babyBirthday";
	}

	public static class TopicColumn
	{

		public static String TOPICID = "topicId";
		public static String SEQID = "seqId";
		public static String TITLE = "title";
		public static String CONTENT = "content";
		public static String BOARDID = "boardId";
		public static String UID = "uid";
		public static String POSTTIME = "postTime";
		public static String VIEWCOUNT = "viewCount";
		public static String COMMENTCOUNT = "commentCount";
		public static String USERNAME = "username";
		public static String BABYBIRTHDAY = "babyBirthday";
		public static String POSTFROM = "postFrom";
		public static String ISTOP = "isTop";
		public static String ISRECOMMENDED = "isRecommended";
		public static String ISMAINPAGESHOW = "isMainPageShow";
		public static String LASTREPLYTIME = "lastReplyTime";
		public static String ISFAV = "isFav";
		public static String RECOMMENDEDTIME = "recommendedTime";
		public static String MAINPAGESHOWTIME = "mainPageShowTime";
		public static String ISNEWESTREPLY = "isNewestReply";
		public static String ISNEWESTTOPIC = "isNewestTopic";
		public static String ISMARROWTOPIC = "isMarrowTopic";
		public static String MAXPOSTSEQID = "maxPostSeqId";
		public static String TOPICTYPE = "topicType";
		public static String REFREID = "referId";
		public static String CITY = "city";
		public static String BBSTATUS = "bbStatus";
		public static String ISHOT = "isHot";
		public static String HOTTIME = "hotTime";
	}

	// String subjectId, String title, Integer viewCount,
	// Byte babyStatus, Integer babyBeginDay, Integer babyEndDay,
	// String postTime, String createTime, String subjectDesc,
	// List<Attachment> attachmentList,List<Topic> topicList
	public static class SubjectColumn
	{

		public static String SEQID = "seqId";
		public static String SUBJECTID = "subjectId";
		public static String TITLE = "title";
		public static String VIEWCOUNT = "viewCount";
		public static String BABYSTATUS = "babyStatus";
		public static String BABYBEGINDAY = "babyBeginDay";
		public static String BABYENDDAY = "babyEndDay";
		public static String POSTTIME = "postTime";
		public static String CREATETIME = "createTime";
		public static String SUBJECTDESC = "subjectDesc";
		public static String FROMTYPE = "fromType";
	}

	// Long seqId, String albumId, String createTime,
	// String postTime, String title, String descText
	public static class AlbumColumn
	{

		public static String SEQID = "seqId";
		public static String ALBUMID = "albumId";
		public static String CREATETIME = "createTime";
		public static String POSTTIME = "postTime";
		public static String TITLE = "title";
		public static String DESCTEXT = "descText";
	}

	// Long seqId, String musicId, String title, String author,
	// String songTime, String musicUrl, String fileUrl,String imgUrl
	public static class MusicColumn
	{

		public static String SEQID = "seqId";
		public static String MUSICID = "musicId";
		public static String TITLE = "title";
		public static String AUTHOR = "author";
		public static String SONGTIME = "songTime";
		public static String MUSICURL = "musicUrl";
		public static String FILEURL = "fileUrl";
		public static String IMGURL = "imgUrl";
		public static String DOWNLOADSTATUS = "downloadStatus";
		public static String PLAYFINISH = "playFinish";
	}

	// private Long seqId;
	// private String answerId;
	// private String questionId;
	// private String postTime;
	// private String referAnswerId;
	// private Integer appreciationCount;
	// private String content;
	// private User user;
	// private List<Attachment> attachmentList;
	public static class AnswerColumn
	{

		public static String SEQID = "seqId";
		public static String ANSWERID = "answerId";
		public static String QUESTIONID = "questionId";
		public static String POSTTIME = "postTime";
		public static String REFERANSWERID = "referAnswerId";
		public static String REFERANSWERCONTENT = "referAnswerContent";
		public static String APPRECIATIONCOUNT = "appreciationCount";
		public static String CONTENT = "content";
		public static String UID = "uid";
		public static String QUESTIONCONTENT = "questionContent";
		public static String QUESTIONNICKNAME = "questionNickname";
		public static String OTHERREPLYME = "OtherReplyMe";
		public static String MEREPLYOTHER = "MeReplyOther";
		public static String ISBEST = "isBest";
		public static String ISQUESTIONANSWER = "isQuestionAnswer";
	}

	public static class PostColumn
	{

		public static String POSTID = "postId";
		public static String SEQID = "seqId";
		public static String TOPICID = "topicId";
		public static String POSTTIME = "postTime";
		public static String REFERPOSTID = "referPostId";
		public static String CONTENT = "content";
		public static String UID = "uid";
		public static String TOPICCONTENT = "topicContent";
		public static String TOPICNICKNAME = "topicNickname";
		public static String REFERPOSTCONTENT = "referPostContent";
		public static String ISMYRECEIVEPOST = "isMyReceivePost";
		public static String ISTOPICREPLY = "isTopicReply";
	}

	// private String attId;
	// private String fileUrl;
	// private Long attSize;
	// private String fileName;
	public static class AttachmentColumn
	{

		public static String ATTID = "attId";
		public static String FILEURL = "fileUrl";
		public static String ATTSIZE = "attSize";
		public static String FILENAME = "fileName";
		public static String QUESTIONID = "questionId";
		public static String ANSWERID = "answerId";
		public static String FILELOCALPATH = "fileLocalPath";
		public static String TOPICID = "topicId";
		public static String POSTID = "postId";
		public static String SUBJECTID = "subjectId";
		public static String BOARDID = "boardId";
		// public static String VIEWPIC_SEQID = "viewpic_seqId";
	}

	// Long uid, String answerContent, String postTime
	public static class AppreciationColumn
	{

		public static String SEQID = "seqId";
		public static String UID = "uid";
		public static String ANSWERCONTENT = "answerContent";
		public static String POSTTIME = "postTime";
		public static String QUESTIONID = "questionId";
	}

	public static class AcceptedAnswerColumn
	{

		public static String SEQID = "seqId";
		public static String UID = "uid";
		public static String ANSWERID = "answerId";
		public static String ANSWERCONTENT = "answerContent";
		public static String POSTTIME = "postTime";
		public static String QUESTIONID = "questionId";
	}

	// Long seqId, String messageId, int messageType,
	// Long timestamp, Long uid, Object contentObject
	public static class MessageColumn
	{

		public static String SEQID = "seqId";
		public static String MESSAGEID = "messageId";
		public static String MESSAGETYPE = "messageType";
		public static String TIMESTAMP = "timestamp";
		public static String UID = "uid";
		public static String CONTENTOBJECT = "contentObject";
	}

	public static class FollowerColumn
	{

		public static String SEQID = "seqId";
		public static String UID = "uid";
		public static String TOUID = "toUid";
		public static String FOLLOWTIME = "followTime";
		public static String STATUS = "status";
		public static String LASTMESSAGETIME = "lastMessageTime";
		public static String LASTMESSAGECONTENT = "lastMessageContent";
		// public static String ISLISTDATA = "isListData";
		// public static String ISDETAILDATA = "isDetailData";
	}
//	String urlKey, Integer appId, String urlValue,
//	String urlIcon, String urlTitle, String urlDesc, String urlContent
	public static class UserShareUrlColumn
	{
		
		public static String URLKEY = "urlKey";
		public static String APPID = "appId";
		public static String URLVALUE = "urlValue";
		public static String URLICON = "urlIcon";
		public static String URLTITLE = "urlTitle";
		public static String URLDESC = "urlDesc";
		public static String URLCONTENT = "urlContent";

	}

	public static class MailColumn
	{

		public static String MAILID = "mailId";
		public static String FROMUID = "fromUid";
		public static String TOUID = "toUid";
		public static String POSTTIME = "postTime";
		public static String CONTENT = "content";
		public static String FOLLOWERID = "followerId";
	}

	// Long feedId, Long uid, Byte feedType,
	// String referId,String postTime, String content, String title, int
	// viewCount,
	// int commentCount
	public static class FeedColumn
	{

		public static String FEEDID = "feedId";
		public static String UID = "uid";
		public static String FEEDTYPE = "feedType";
		public static String REFERID = "referId";
		public static String POSTTIME = "postTime";
		public static String CONTENT = "content";
		public static String TITLE = "title";
		public static String VIEWCOUNT = "viewCount";
		public static String COMMENTCOUNT = "commentCount";
		public static String CITY = "city";
		public static String BABYBIRTHDAY = "babyBirthday";
	}

	public static class RelationColumn
	{

		public static String RELATIONID = "relationId";
		public static String SUBJECTID = "subjectId";
		public static String TOPICID = "topicId";
		public static String ALBUMID = "albumId";
		public static String MUSICID = "musicId";
	}

	public static class NewestToastColumn
	{

		public static String TOASTID = "toastId";
		public static String TOASTCONTENT = "toastContent";
		public static String TOASTTIME = "toastTime";
		public static String ISREAD = "isRead";
	}

	// Long seqId, Integer viewType, Integer jumpType,
	// String jumpValue, String title, String noteReplaceRule
	public static class ViewPictureColumn
	{

		public static String SEQID = "seqId";
		public static String VIEWTYPE = "viewType";
		public static String JUMPTYPE = "jumpType";
		public static String JUMPVALUE = "jumpValue";
		public static String TITLE = "title";
		public static String NOTEREPLACERULE = "noteReplaceRule";
		public static String IMGURL = "imgUrl";
	}

	// private Long uid;
	// private String nickname;
	// private Byte gender;
	// private String perinatal;
	// private String babyBirthday;
	// private Byte babyGender;
	// private String profileImage;
	// private String email;
	// private String password;
	// private String regTime;
	// private String token;
	// private String deviceToken;
	// private int coins;
	// private int credits;
	// private Byte isShared;
	// int questionCount,int answerCount ,int appreciationCount
	// Byte status,String bgImageUrl
	// ,Byte isCounsellor,int level,String deviceId
	public static class UserColumn
	{

		public static String UID = "uid";
		public static String NICKNAME = "nickname";
		public static String GENDER = "gender";
		public static String PERINATAL = "perinatal";
		public static String BABYBIRTHDAY = "babyBirthday";
		public static String BABYGENDER = "babyGender";
		public static String PROFILEIMAGE = "profileImage";
		public static String EMAIL = "email";
		public static String PASSWORD = "password";
		public static String REGTIME = "regTime";
		public static String TOKEN = "token";
		public static String DEVICETOKEN = "deviceToken";
		public static String COINS = "coins";
		public static String CREDITS = "credits";
		public static String ISSHARED = "isShared";
		public static String ISSELF = "isSelf";
		public static String QUESTIONCOUNT = "questionCount";
		public static String ANSWERCOUNT = "answerCount";
		public static String APPRECIATIONCOUNT = "appreciationCount";
		public static String STATUS = "status";
		public static String BGIMAGEURL = "bgImageUrl";
		public static String ISCOUNSELLOR = "isCounsellor";
		public static String LEVEL = "level";
		public static String DEVICEID = "deviceId";
		public static String ACCINLASTWEEK = "accInLastWeek";
		public static String COINSYESTERDAY = "coinsYesterday";
		public static String FOLLOWSTATUS = "followStatus";
		public static String FANSCOUNT = "fansCount";
		public static String ATTENTIONCOUNT = "attentionCount";
		public static String ISGUEST = "isGuest";
		public static String CITY = "city";
		public static String MEDALIDLISTSTR = "medalIdListStr";
		public static String YCOINS = "ycoins";
		public static String FREEZEYCOINS = "freezeYcoins";
	}

	public static class UserStatusColumn
	{

		public static String USERSTATUSID = "userStatusId";
		public static String TYPE = "type";
		public static String TIME = "time";
		public static String PERIOD = "period";
		public static String CYCLE = "cycle";
		public static String BABYGENDER = "babyGender";
	}

	public static class RequestFailed
	{

		public static String ID = "id";
		public static String FAILEDTYPE = "failedType";
		public static String DATAID = "dataId";
		public static String CONNECTCOUNT = "connectCount";
		public static String SUCCESSORNOT = "successOrNot";
	}

	//
	// String boardUUId, Long boardId, String boardName,
	// String boardDesc, String lastTopicTime, String lastReplyTime,
	// String iconUrl
	public static class BoardColumn
	{

		public static String BOARDUUID = "boardUUId";
		public static String BOARDID = "boardId";
		public static String BOARDNAME = "boardName";
		public static String BOARDDESC = "boardDesc";
		public static String LASTTOPICTIME = "lastTopicTime";
		public static String LASTREPLYTIME = "lastReplyTime";
		public static String ICONURL = "iconUrl";
		public static String BOARDORDER = "boardOrder";
		public static String ATTENTION = "attention";
		public static String MODERATORIDS = "moderatorIds";
		public static String FEEDCOUNT = "feedCount";
	}

	public static class BoardMaxSeqIdColumn
	{

		public static String DESC = "desc";
		public static String MAXQUESTIONSEQID = "maxSeqId";
	}

	// String draftsId, String title, String content, int type
	public static class DraftsColumn
	{

		public static String DRAFTSID = "draftsId";
		public static String TITLE = "title";
		public static String CONTENT = "content";
		public static String TYPE = "type";
	}
}
