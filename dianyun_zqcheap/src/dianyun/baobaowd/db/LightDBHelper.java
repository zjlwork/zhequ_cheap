package dianyun.baobaowd.db;

import android.content.Context;
import dianyun.baobaowd.util.GobalConstants;

public class LightDBHelper
{

//	public static void setStartIvUrl(Context context, String url)
//	{
//		SharedPreferenceData.writeStraightStringSp(context,
//				GobalConstants.SharedPreference.KEY_STARTIVURL, url,
//				GobalConstants.SharedPreference.sharedpreference_name);
//	}
//	
//	public static String getStartIvUrl(Context context)
//	{
//		return SharedPreferenceData.getStraightStringSp(context,
//				GobalConstants.SharedPreference.KEY_STARTIVURL,
//				GobalConstants.SharedPreference.sharedpreference_name);
//	}
	
	
	
//	public static void setInviteUserListUrl(Context context, String url)
//	{
//		SharedPreferenceData.writeStraightStringSp(context,
//				GobalConstants.SharedPreference.KEY_FANLIINVITEUSERLIST, url,
//				GobalConstants.SharedPreference.sharedpreference_name);
//	}
//	
//	public static String getInviteUserListUrl(Context context)
//	{
//		return SharedPreferenceData.getStraightStringSp(context,
//				GobalConstants.SharedPreference.KEY_FANLIINVITEUSERLIST,
//				GobalConstants.SharedPreference.sharedpreference_name);
//	}
//	public static void setProfileAppUrl(Context context, String url)
//	{
//		SharedPreferenceData.writeStraightStringSp(context,
//				GobalConstants.SharedPreference.KEY_FANLIPROFILEAPP, url,
//				GobalConstants.SharedPreference.sharedpreference_name);
//	}
//	
//	public static String getProfileAppUrl(Context context)
//	{
//		return SharedPreferenceData.getStraightStringSp(context,
//				GobalConstants.SharedPreference.KEY_FANLIPROFILEAPP,
//				GobalConstants.SharedPreference.sharedpreference_name);
//	}
	public static void setInviteUid(Context context, long  uid)
	{
		SharedPreferenceData.writeStraightLongSp(context,
				GobalConstants.SharedPreference.KEY_INVITEUID, uid,
				GobalConstants.SharedPreference.sharedpreference_name);
	}
	
	public static long getInviteUid(Context context)
	{
		return SharedPreferenceData.getStraightLongSp(context,
				GobalConstants.SharedPreference.KEY_INVITEUID,
				GobalConstants.SharedPreference.sharedpreference_name,-1);
	}
//	public static void setStartIvLocalPath(Context context, String localPath)
//	{
//		SharedPreferenceData.writeStraightStringSp(context,
//				GobalConstants.SharedPreference.KEY_STARTIVLOCALPATH, localPath,
//				GobalConstants.SharedPreference.sharedpreference_name);
//	}
//	
//	public static String getStartIvLocalPath(Context context)
//	{
//		return SharedPreferenceData.getStraightStringSp(context,
//				GobalConstants.SharedPreference.KEY_STARTIVLOCALPATH,
//				GobalConstants.SharedPreference.sharedpreference_name);
//	}
	
	
	
	public static void setLoginType(Context context, int loginType)
	{
		SharedPreferenceData.writeStraightIntSp(context,
				GobalConstants.SharedPreference.KEY_LOGINTYPE, loginType,
				GobalConstants.SharedPreference.sharedpreference_name);
	}

	public static int getLoginType(Context context)
	{
		return SharedPreferenceData.getStraightIntSp(context,
				GobalConstants.SharedPreference.KEY_LOGINTYPE,
				GobalConstants.SharedPreference.sharedpreference_name);
	}

	public static void setMainBg(Context context, String mainbg)
	{
		SharedPreferenceData.writeStraightStringSp(context,
				GobalConstants.SharedPreference.KEY_MAINBG, mainbg,
				GobalConstants.SharedPreference.sharedpreference_name);
	}

	public static String getMainBg(Context context)
	{
		return SharedPreferenceData.getStraightStringSp(context,
				GobalConstants.SharedPreference.KEY_MAINBG,
				GobalConstants.SharedPreference.sharedpreference_name);
	}

	public static void setXiaoMiUser(Context context, String xiaomijson)
	{
		SharedPreferenceData.writeStraightStringSp(context,
				GobalConstants.SharedPreference.KEY_XIAOMIJSON, xiaomijson,
				GobalConstants.SharedPreference.sharedpreference_name);
	}

	public static String getXiaoMiUser(Context context)
	{
		return SharedPreferenceData.getStraightStringSp(context,
				GobalConstants.SharedPreference.KEY_XIAOMIJSON,
				GobalConstants.SharedPreference.sharedpreference_name);
	}

	public static void setWeatherJson(Context context, String weatherJson)
	{
		SharedPreferenceData.writeStraightStringSp(context,
				GobalConstants.SharedPreference.KEY_WEATHERJSON, weatherJson,
				GobalConstants.SharedPreference.sharedpreference_name);
	}

	public static String getWeatherJson(Context context)
	{
		return SharedPreferenceData.getStraightStringSp(context,
				GobalConstants.SharedPreference.KEY_WEATHERJSON,
				GobalConstants.SharedPreference.sharedpreference_name);
	}

	public static void setTodayMood(Context context, String todayMood)
	{
		SharedPreferenceData.writeStraightStringSp(context,
				GobalConstants.SharedPreference.KEY_TODAYMOOD, todayMood,
				GobalConstants.SharedPreference.sharedpreference_name);
	}

	public static String getTodayMood(Context context)
	{
		return SharedPreferenceData.getStraightStringSp(context,
				GobalConstants.SharedPreference.KEY_TODAYMOOD,
				GobalConstants.SharedPreference.sharedpreference_name);
	}

	public static void setLastRefreshTime(String lastRefreshTime,
			Context context)
	{
		SharedPreferenceData.writeStraightStringSp(context,
				GobalConstants.SharedPreference.KEY_LASTREFRESHTIME,
				lastRefreshTime,
				GobalConstants.SharedPreference.sharedpreference_name);
	}

	public static String getLastRefreshTime(Context context)
	{
		return SharedPreferenceData.getStraightStringSp(context,
				GobalConstants.SharedPreference.KEY_LASTREFRESHTIME,
				GobalConstants.SharedPreference.sharedpreference_name);
	}
	public static void setBindPhone(String phone,
			Context context)
	{
		SharedPreferenceData.writeStraightStringSp(context,
				GobalConstants.SharedPreference.KEY_BINDPHONE,
				phone,
				GobalConstants.SharedPreference.sharedpreference_name);
	}
	
	public static String getBindPhone(Context context)
	{
		return SharedPreferenceData.getStraightStringSp(context,
				GobalConstants.SharedPreference.KEY_BINDPHONE,
				GobalConstants.SharedPreference.sharedpreference_name);
	}
	
	public static void setBindPay(String pay,
			Context context)
	{
		SharedPreferenceData.writeStraightStringSp(context,
				GobalConstants.SharedPreference.KEY_BINDPAY,
				pay,
				GobalConstants.SharedPreference.sharedpreference_name);
	}
	
	public static String getBindPay(Context context)
	{
		return SharedPreferenceData.getStraightStringSp(context,
				GobalConstants.SharedPreference.KEY_BINDPAY,
				GobalConstants.SharedPreference.sharedpreference_name);
	}
	
	
	
	
	
	
	

	public static void setOnlineUserCountTime(String time, Context context)
	{
		SharedPreferenceData.writeStraightStringSp(context,
				GobalConstants.SharedPreference.KEY_ONLINEUSERTIME, time,
				GobalConstants.SharedPreference.sharedpreference_name);
	}

	public static String getOnlineUserCountTime(Context context)
	{
		return SharedPreferenceData.getStraightStringSp(context,
				GobalConstants.SharedPreference.KEY_ONLINEUSERTIME,
				GobalConstants.SharedPreference.sharedpreference_name);
	}

	public static void setOnlineUserCount(Context context, int loginType)
	{
		SharedPreferenceData.writeStraightIntSp(context,
				GobalConstants.SharedPreference.KEY_ONLINEUSERCOUNT, loginType,
				GobalConstants.SharedPreference.sharedpreference_name);
	}
	

	public static int getOnlineUserCount(Context context)
	{
		return SharedPreferenceData.getStraightIntSp(context,
				GobalConstants.SharedPreference.KEY_ONLINEUSERCOUNT,
				GobalConstants.SharedPreference.sharedpreference_name);
	}

	public static void setOnlineUserCountRatio(Context context, float ratio)
	{
		SharedPreferenceData.writeStraightFloatSp(context,
				GobalConstants.SharedPreference.KEY_ONLINEUSERCOUNTRATIO,
				ratio, GobalConstants.SharedPreference.sharedpreference_name);
	}

	public static float getOnlineUserCountRatio(Context context)
	{
		return SharedPreferenceData.getStraightFloatSp(context,
				GobalConstants.SharedPreference.KEY_ONLINEUSERCOUNTRATIO,
				GobalConstants.SharedPreference.sharedpreference_name);
	}

	public static void setCheckInTime(String checkInTime, Context context)
	{
		SharedPreferenceData.writeStraightStringSp(context,
				GobalConstants.SharedPreference.KEY_CHECKINTIME, checkInTime,
				GobalConstants.SharedPreference.sharedpreference_name);
	}

	public static String getCheckInTime(Context context)
	{
		return SharedPreferenceData.getStraightStringSp(context,
				GobalConstants.SharedPreference.KEY_CHECKINTIME,
				GobalConstants.SharedPreference.sharedpreference_name);
	}

	public static void setSearchWords(String searchWords, Context context)
	{
		SharedPreferenceData.writeStraightStringSp(context,
				GobalConstants.SharedPreference.KEY_SEARCHWORDS, searchWords,
				GobalConstants.SharedPreference.sharedpreference_name);
	}

	public static String getSearchWords(Context context)
	{
		return SharedPreferenceData.getStraightStringSp(context,
				GobalConstants.SharedPreference.KEY_SEARCHWORDS,
				GobalConstants.SharedPreference.sharedpreference_name);
	}

	/*
	 * public static void setStartUpSuccessTime(String checkInTime,Context
	 * context){ SharedPreferenceData.writeStraightStringSp(context,
	 * GobalConstants.SharedPreference.KEY_STARTUPSUCCESSTIME, checkInTime,
	 * GobalConstants.SharedPreference.sharedpreference_name); } public static
	 * String getStartUpSuccessTime(Context context){ return
	 * SharedPreferenceData.getStraightStringSp(context,
	 * GobalConstants.SharedPreference.KEY_STARTUPSUCCESSTIME,
	 * GobalConstants.SharedPreference.sharedpreference_name); }
	 */
	public static void setRewardLevel(Context context, int level)
	{
		SharedPreferenceData.writeStraightIntSp(context,
				GobalConstants.SharedPreference.KEY_REWARDLEVEL, level,
				GobalConstants.SharedPreference.sharedpreference_name);
	}

	public static int getRewardLevel(Context context)
	{
		return SharedPreferenceData.getStraightIntSp(context,
				GobalConstants.SharedPreference.KEY_REWARDLEVEL,
				GobalConstants.SharedPreference.sharedpreference_name);
	}

	public static void setTopicDetailPrefix(String topicDetailPrefix,
			Context context)
	{
		SharedPreferenceData.writeStraightStringSp(context,
				GobalConstants.SharedPreference.KEY_TOPICDETAILPREFIX,
				topicDetailPrefix,
				GobalConstants.SharedPreference.sharedpreference_name);
	}

	public static String getTopicDetailPrefix(Context context)
	{
		return SharedPreferenceData.getStraightStringSp(context,
				GobalConstants.SharedPreference.KEY_TOPICDETAILPREFIX,
				GobalConstants.SharedPreference.sharedpreference_name);
	}
	public static void setRegGiveTips(String topicDetailPrefix,
			Context context)
	{
		SharedPreferenceData.writeStraightStringSp(context,
				GobalConstants.SharedPreference.KEY_REGIVETIPS,
				topicDetailPrefix,
				GobalConstants.SharedPreference.sharedpreference_name);
	}
	
	public static String getRegGiveTips(Context context)
	{
		return SharedPreferenceData.getStraightStringSp(context,
				GobalConstants.SharedPreference.KEY_REGIVETIPS,
				GobalConstants.SharedPreference.sharedpreference_name);
	}

	/*
	 * 设置商城的 Url 前缀头部
	 */
	public static void setShopUrlPrefix(String topicDetailPrefix,
			Context context)
	{
		SharedPreferenceData.writeStraightStringSp(context,
				GobalConstants.SharedPreference.KEY_SHOPPREFIX,
				topicDetailPrefix,
				GobalConstants.SharedPreference.sharedpreference_name);
	}

	/*
	 * 获取商城的 Url 前缀头部
	 */
	public static String getShopUrlPrefix(Context context)
	{
		String res = SharedPreferenceData.getStraightStringSp(context,
				GobalConstants.SharedPreference.KEY_SHOPPREFIX,
				GobalConstants.SharedPreference.sharedpreference_name);
		res = res.equals("") ? GobalConstants.URL.SHOP_DEFAULT_PREFIX : res;
		System.out.println("res======"+res);
		return res;
		
	}

	/**
	 * 获取扫一扫的http头前缀
	 * 
	 * @param context
	 * @return
	 */
	public static String getScanCheckUrlPrefix(Context context)
	{
		String res = SharedPreferenceData.getStraightStringSp(context,
				GobalConstants.SharedPreference.KEY_SCAN_CHECK_PREFIX,
				GobalConstants.SharedPreference.sharedpreference_name);
		res = res.equals("") ? GobalConstants.URL.SCAN_CHECK_DEFAULT_PREFIX
				: res;
		return res;
	}

	/**
	 * 设置扫一扫分析的http头前缀
	 * 
	 * @param context
	 * @param url
	 */
	public static void setScanCheckUrlPrefix(Context context, String url)
	{
		SharedPreferenceData.writeStraightStringSp(context,
				GobalConstants.SharedPreference.KEY_SCAN_CHECK_PREFIX, url,
				GobalConstants.SharedPreference.sharedpreference_name);
	}

    /**
     * 获取扫一扫的http头前缀
     *
     * @param context
     * @return
     */
    public static String getFetchCheckUrlPrefix(Context context)
    {
        String res = SharedPreferenceData.getStraightStringSp(context,
                GobalConstants.SharedPreference.KEY_FETCH_CHECK_PREFIX,
                GobalConstants.SharedPreference.sharedpreference_name);
        res = res.equals("") ? GobalConstants.URL.GET_CHECK_DEFAULT_PREFIX
                : res;
        return res;
    }

    /**
     * 设置扫一扫分析的http头前缀
     *
     * @param context
     * @param url
     */
    public static void setFetchCheckUrlPrefix(Context context, String url)
    {
        SharedPreferenceData.writeStraightStringSp(context,
                GobalConstants.SharedPreference.KEY_FETCH_CHECK_PREFIX, url,
                GobalConstants.SharedPreference.sharedpreference_name);
    }

    /**
     * 获取搜索关键字的http头前缀
     *
     * @param context
     * @return
     */
    public static String getSearchResultCheckUrlPrefix(Context context)
    {
        String res = SharedPreferenceData.getStraightStringSp(context,
                GobalConstants.SharedPreference.KEY_SEARCH_RESULT_WITH_CHECK_PREFIX,
                GobalConstants.SharedPreference.sharedpreference_name);
        res = res.equals("") ? GobalConstants.URL.GET_SEARCH_RESULT_WITH_PRIMARYKEY_URL_DEFAULT_PREFIX
                : res;
        return res;
    }

    /**
     * 设置扫一扫分析的http头前缀
     *
     * @param context
     * @param url
     */
    public static void setSearchResultCheckUrlPrefix(Context context, String url)
    {
        SharedPreferenceData.writeStraightStringSp(context,
                GobalConstants.SharedPreference.KEY_SEARCH_RESULT_WITH_CHECK_PREFIX, url,
                GobalConstants.SharedPreference.sharedpreference_name);
    }

    /**
     * 获取检验URL的http头前缀
     *
     * @param context
     * @return
     */
    public static String getPrimaryWithWordsUrlPrefix(Context context)
    {
        String res = SharedPreferenceData.getStraightStringSp(context,
                GobalConstants.SharedPreference.KEY_GET_PRIMARY_WITH_WORDS_PREFIX,
                GobalConstants.SharedPreference.sharedpreference_name);
        res = res.equals("") ? GobalConstants.URL.GET_FETCH_PRIMARY_WORDS_WITH_URL_DEFAULT_PREFIX
                : res;
        return res;
    }

    /**
     * 设置检验URL的http头前缀
     *
     * @param context
     * @param url
     */
    public static void setPrimaryWithWordsUrlPrefix(Context context, String url)
    {
        SharedPreferenceData.writeStraightStringSp(context,
                GobalConstants.SharedPreference.KEY_GET_PRIMARY_WITH_WORDS_PREFIX, url,
                GobalConstants.SharedPreference.sharedpreference_name);
    }

	public static void setAskDetailPrefix(String askDetailPrefix,
			Context context)
	{
		SharedPreferenceData.writeStraightStringSp(context,
				GobalConstants.SharedPreference.KEY_ASKDETAILPREFIX,
				askDetailPrefix,
				GobalConstants.SharedPreference.sharedpreference_name);
	}

	public static String getAskDetailPrefix(Context context)
	{
		return SharedPreferenceData.getStraightStringSp(context,
				GobalConstants.SharedPreference.KEY_ASKDETAILPREFIX,
				GobalConstants.SharedPreference.sharedpreference_name);
	}
	public static void setJfqSwitch(String switchstr,
			Context context)
	{
		SharedPreferenceData.writeStraightStringSp(context,
				GobalConstants.SharedPreference.KEY_JFQSWITCH,
				switchstr,
				GobalConstants.SharedPreference.sharedpreference_name);
	}
	
	public static String getJfqSwitch(Context context)
	{
		return SharedPreferenceData.getStraightStringSp(context,
				GobalConstants.SharedPreference.KEY_JFQSWITCH,
				GobalConstants.SharedPreference.sharedpreference_name);
	}

	public static void setBecomeUserSuccessTime(String checkInTime,
			Context context)
	{
		SharedPreferenceData.writeStraightStringSp(context,
				GobalConstants.SharedPreference.KEY_BECOMEUSERSUCCESSTIME,
				checkInTime,
				GobalConstants.SharedPreference.sharedpreference_name);
	}

	public static String getBecomeUserSuccessTime(Context context)
	{
		return SharedPreferenceData.getStraightStringSp(context,
				GobalConstants.SharedPreference.KEY_BECOMEUSERSUCCESSTIME,
				GobalConstants.SharedPreference.sharedpreference_name);
	}

	public static void setShareArticleTime(String shareArticleTime,
			Context context)
	{
		SharedPreferenceData.writeStraightStringSp(context,
				GobalConstants.SharedPreference.KEY_SHAREARTICLETIME,
				shareArticleTime,
				GobalConstants.SharedPreference.sharedpreference_name);
	}

	public static String getShareArticleTime(Context context)
	{
		return SharedPreferenceData.getStraightStringSp(context,
				GobalConstants.SharedPreference.KEY_SHAREARTICLETIME,
				GobalConstants.SharedPreference.sharedpreference_name);
	}

	public static void setIsDownloadApk(Context context, boolean isDownload)
	{
		SharedPreferenceData.writeStraightBooleanSp(context,
				GobalConstants.SharedPreference.KEY_ISDOWNLOADAPK, isDownload,
				GobalConstants.SharedPreference.sharedpreference_name);
	}

	public static boolean getIsDownloadApk(Context context)
	{
		return SharedPreferenceData.getStraightBooleanSp(context,
				GobalConstants.SharedPreference.KEY_ISDOWNLOADAPK,
				GobalConstants.SharedPreference.sharedpreference_name);
	}
	public static void setNotNoticeAgain(Context context, boolean notice)
	{
		SharedPreferenceData.writeStraightBooleanSp(context,
				GobalConstants.SharedPreference.KEY_NOTNOTICEAGAIN, notice,
				GobalConstants.SharedPreference.sharedpreference_name);
	}
	
	public static boolean getNotNoticeAgain(Context context)
	{
		return SharedPreferenceData.getStraightBooleanSp(context,
				GobalConstants.SharedPreference.KEY_NOTNOTICEAGAIN,
				GobalConstants.SharedPreference.sharedpreference_name);
	}
	public static void setGoldUseGuide(Context context, boolean guide)
	{
		SharedPreferenceData.writeStraightBooleanSp(context,
				GobalConstants.SharedPreference.KEY_GOLDUSEGUIDE, guide,
				GobalConstants.SharedPreference.sharedpreference_name);
	}
	
	public static boolean getGoldUseGuide(Context context)
	{
		return SharedPreferenceData.getStraightBooleanSp(context,
				GobalConstants.SharedPreference.KEY_GOLDUSEGUIDE,
				GobalConstants.SharedPreference.sharedpreference_name);
	}
	public static void setFirstGuide(Context context, boolean guide)
	{
		SharedPreferenceData.writeStraightBooleanSp(context,
				GobalConstants.SharedPreference.KEY_FIRSTGUIDE, guide,
				GobalConstants.SharedPreference.sharedpreference_name);
	}
	
	public static boolean getFirstGuide(Context context)
	{
		return SharedPreferenceData.getStraightBooleanSp(context,
				GobalConstants.SharedPreference.KEY_FIRSTGUIDE,
				GobalConstants.SharedPreference.sharedpreference_name);
	}
	public static void setSecondGuide(Context context, boolean guide)
	{
		SharedPreferenceData.writeStraightBooleanSp(context,
				GobalConstants.SharedPreference.KEY_SECONDGUIDE, guide,
				GobalConstants.SharedPreference.sharedpreference_name);
	}
	
	public static boolean getSecondGuide(Context context)
	{
		return SharedPreferenceData.getStraightBooleanSp(context,
				GobalConstants.SharedPreference.KEY_SECONDGUIDE,
				GobalConstants.SharedPreference.sharedpreference_name);
	}

	public static void setIsBind(Context context, boolean isBind)
	{
		SharedPreferenceData.writeStraightBooleanSp(context,
				GobalConstants.SharedPreference.KEY_ISBINDTAOBAO, isBind,
				GobalConstants.SharedPreference.sharedpreference_name);
	}

	public static boolean getIsBind(Context context)
	{
		return SharedPreferenceData.getStraightBooleanSp2(context,
				GobalConstants.SharedPreference.KEY_ISBINDTAOBAO,
				GobalConstants.SharedPreference.sharedpreference_name);
	}

	public static void setIsRegister(Context context, boolean status)
	{
		SharedPreferenceData.writeStraightBooleanSp(context,
				GobalConstants.SharedPreference.KEY_ISREGISTER, status,
				GobalConstants.SharedPreference.sharedpreference_name);
	}

	public static boolean getIsRegister(Context context)
	{
		return SharedPreferenceData.getStraightBooleanSp(context,
				GobalConstants.SharedPreference.KEY_ISREGISTER,
				GobalConstants.SharedPreference.sharedpreference_name);
	}



	public static void setIsNotTipLoginOrNotFees(Context context, boolean isTip)
	{
		SharedPreferenceData.writeStraightBooleanSp(context,
				GobalConstants.SharedPreference.KEY_ISTIPORNOTFESS, isTip,
				GobalConstants.SharedPreference.sharedpreference_name);
	}

	public static boolean getIsNotTipLoginOrNotFees(Context context)
	{
		return SharedPreferenceData.getStraightBooleanSp(context,
				GobalConstants.SharedPreference.KEY_ISTIPORNOTFESS,
				GobalConstants.SharedPreference.sharedpreference_name);
	}

    /**
     * 设置在点击购物车时候是否不再显示对话框
     * @param context
     * @param isTip
     */
    public static void setIsNotTipGoShopOrNotFees(Context context, boolean isTip)
    {
        SharedPreferenceData.writeStraightBooleanSp(context,
                GobalConstants.SharedPreference.KEY_ISGOSHOPTIPORNOTFESS, isTip,
                GobalConstants.SharedPreference.sharedpreference_name);
    }

    /**
     * 获取点击购物车时候是否需要显示对话框
     * @param context
     * @return
     */
    public static boolean getIsNotTipGoShopOrNotFees(Context context)
    {
        return SharedPreferenceData.getStraightBooleanSp(context,
                GobalConstants.SharedPreference.KEY_ISGOSHOPTIPORNOTFESS,
                GobalConstants.SharedPreference.sharedpreference_name);
    }

	public static void setShortcut(Context context, boolean status)
	{
		SharedPreferenceData.writeStraightBooleanSp(context,
				GobalConstants.SharedPreference.KEY_SHORTCUT, status,
				GobalConstants.SharedPreference.sharedpreference_name);
	}

	public static boolean getShortcut(Context context)
	{
		return SharedPreferenceData.getStraightBooleanSp(context,
				GobalConstants.SharedPreference.KEY_SHORTCUT,
				GobalConstants.SharedPreference.sharedpreference_name);
	}

	public static void setIsMainActivityAlive(Context context, boolean alive)
	{
		SharedPreferenceData.writeStraightBooleanSp(context,
				GobalConstants.SharedPreference.KEY_ISMAINACTIVITYALIVE, alive,
				GobalConstants.SharedPreference.sharedpreference_name);
	}

	public static boolean getIsMainActivityAlive(Context context)
	{
		return SharedPreferenceData.getStraightBooleanSp(context,
				GobalConstants.SharedPreference.KEY_ISMAINACTIVITYALIVE,
				GobalConstants.SharedPreference.sharedpreference_name);
	}

	public static void setReceiveAddress(String receiveAddress, Context context)
	{
		SharedPreferenceData.writeStraightStringSp(context,
				GobalConstants.SharedPreference.KEY_RECEIVEADDRESS,
				receiveAddress,
				GobalConstants.SharedPreference.sharedpreference_name);
	}

	public static String getReceiveAddress(Context context)
	{
		return SharedPreferenceData.getStraightStringSp(context,
				GobalConstants.SharedPreference.KEY_RECEIVEADDRESS,
				GobalConstants.SharedPreference.sharedpreference_name);
	}

	/*
	 * public static void setGettingMoreNew(Context context,boolean status){
	 * SharedPreferenceData.writeStraightBooleanSp(context,
	 * GobalConstants.SharedPreference.KEY_ISGETTINGMORENEW, status,
	 * GobalConstants.SharedPreference.sharedpreference_name); } public static
	 * boolean isGettingMoreNew(Context context){ return
	 * SharedPreferenceData.getStraightBooleanSp
	 * (context,GobalConstants.SharedPreference.KEY_ISGETTINGMORENEW,
	 * GobalConstants.SharedPreference.sharedpreference_name); } public static
	 * void setGettingMoreOld(Context context,boolean status){
	 * SharedPreferenceData.writeStraightBooleanSp(context,
	 * GobalConstants.SharedPreference.KEY_ISGETTINGMOREOLD, status,
	 * GobalConstants.SharedPreference.sharedpreference_name); } public static
	 * boolean isGettingMoreOld(Context context){ return
	 * SharedPreferenceData.getStraightBooleanSp
	 * (context,GobalConstants.SharedPreference.KEY_ISGETTINGMOREOLD,
	 * GobalConstants.SharedPreference.sharedpreference_name); }
	 */
	/*
	 * public static void setHadNewReply(Context context,boolean had){
	 * SharedPreferenceData.writeStraightBooleanSp(context,
	 * GobalConstants.SharedPreference.KEY_HADNEWREPLY, had,
	 * GobalConstants.SharedPreference.sharedpreference_name); } public static
	 * boolean getHadNewReply(Context context){ return
	 * SharedPreferenceData.getStraightBooleanSp
	 * (context,GobalConstants.SharedPreference.KEY_HADNEWREPLY,
	 * GobalConstants.SharedPreference.sharedpreference_name); } public static
	 * void setHadNewAppreciation(Context context,boolean had){
	 * SharedPreferenceData.writeStraightBooleanSp(context,
	 * GobalConstants.SharedPreference.KEY_HADNEWAPPRECIATION, had,
	 * GobalConstants.SharedPreference.sharedpreference_name); } public static
	 * boolean getHadNewAppreciation(Context context){ return
	 * SharedPreferenceData
	 * .getStraightBooleanSp(context,GobalConstants.SharedPreference
	 * .KEY_HADNEWAPPRECIATION,
	 * GobalConstants.SharedPreference.sharedpreference_name); }
	 */
	public static void setShareTime(String shareTime, Context context)
	{
		SharedPreferenceData.writeStraightStringSp(context,
				GobalConstants.SharedPreference.KEY_SHARETIME, shareTime,
				GobalConstants.SharedPreference.sharedpreference_name);
	}

	public static String getShareTime(Context context)
	{
		return SharedPreferenceData.getStraightStringSp(context,
				GobalConstants.SharedPreference.KEY_SHARETIME,
				GobalConstants.SharedPreference.sharedpreference_name);
	}

	public static void setCheckVersionTime(String shareTime, Context context)
	{
		SharedPreferenceData.writeStraightStringSp(context,
				GobalConstants.SharedPreference.KEY_CHECKVERSIONTIME,
				shareTime,
				GobalConstants.SharedPreference.sharedpreference_name);
	}

	public static String getCheckVersionTime(Context context)
	{
		return SharedPreferenceData.getStraightStringSp(context,
				GobalConstants.SharedPreference.KEY_CHECKVERSIONTIME,
				GobalConstants.SharedPreference.sharedpreference_name);
	}

	public static void setShareCount(Context context, int count)
	{
		SharedPreferenceData.writeStraightIntSp(context,
				GobalConstants.SharedPreference.KEY_SHARECOUNT, count,
				GobalConstants.SharedPreference.sharedpreference_name);
	}

	public static int getShareCount(Context context)
	{
		return SharedPreferenceData.getStraightIntSp(context,
				GobalConstants.SharedPreference.KEY_SHARECOUNT,
				GobalConstants.SharedPreference.sharedpreference_name);
	}


    public static void setMainBgIndex(Context context, int index)
    {
        SharedPreferenceData.writeStraightIntSp(context,
                GobalConstants.SharedPreference.KEY_MAINBGINDEX, index,
                GobalConstants.SharedPreference.sharedpreference_name);
    }

    public static int getMainBgIndex(Context context)
    {
       return SharedPreferenceData.getStraightIntSp(context,
                GobalConstants.SharedPreference.KEY_MAINBGINDEX,
                GobalConstants.SharedPreference.sharedpreference_name);

    }

    public static void setQiandaoMessage(Context context, String message)
    {
        SharedPreferenceData.writeStraightStringSp(context,
                GobalConstants.SharedPreference.KEY_QIANDAOMESSAGE, message,
                GobalConstants.SharedPreference.sharedpreference_name);
    }

    public static String getQiandaoMessage(Context context)
    {
        return SharedPreferenceData.getStraightStringSp(context,
                GobalConstants.SharedPreference.KEY_QIANDAOMESSAGE,
                GobalConstants.SharedPreference.sharedpreference_name);

    }

    public static void setMainNotifyMessage(Context context, String message)
    {
        SharedPreferenceData.writeStraightStringSp(context,
                GobalConstants.SharedPreference.KEY_MAINNOTIFYMESSAGE, message,
                GobalConstants.SharedPreference.sharedpreference_name);
    }

    public static String getMainNotifyMessage(Context context)
    {
        return SharedPreferenceData.getStraightStringSp(context,
                GobalConstants.SharedPreference.KEY_MAINNOTIFYMESSAGE,
                GobalConstants.SharedPreference.sharedpreference_name);

    }

	public static void setInviteCount(Context context, int count)
	{
		SharedPreferenceData.writeStraightIntSp(context,
				GobalConstants.SharedPreference.KEY_INVITECOUNT, count,
				GobalConstants.SharedPreference.sharedpreference_name);
	}

	public static int getInviteCount(Context context)
	{
		return SharedPreferenceData.getStraightIntSp(context,
				GobalConstants.SharedPreference.KEY_INVITECOUNT,
				GobalConstants.SharedPreference.sharedpreference_name);
	}

	public static void setLastBoardPosition(Context context, int position)
	{
		SharedPreferenceData
				.writeStraightIntSp(context,
						GobalConstants.SharedPreference.KEY_LASTBORADPOSITION,
						position,
						GobalConstants.SharedPreference.sharedpreference_name);
	}

	public static int getLastBoardPosition(Context context)
	{
		return SharedPreferenceData.getStraightIntSp(context,
				GobalConstants.SharedPreference.KEY_LASTBORADPOSITION,
				GobalConstants.SharedPreference.sharedpreference_name);
	}

	/*public static void setNewReplyCount(Context context, int count)
	{
		SharedPreferenceData.writeStraightIntSp(context,
				GobalConstants.SharedPreference.KEY_NEWREPLYCOUNT, count,
				GobalConstants.SharedPreference.sharedpreference_name);
	}

	public static int getNewReplyCount(Context context)
	{
		return SharedPreferenceData.getStraightIntSp(context,
				GobalConstants.SharedPreference.KEY_NEWREPLYCOUNT,
				GobalConstants.SharedPreference.sharedpreference_name);
	}

	public static void setNewAppreciationCount(Context context, int count)
	{
		SharedPreferenceData.writeStraightIntSp(context,
				GobalConstants.SharedPreference.KEY_NEWAPPRECIATIONCOUNT,
				count, GobalConstants.SharedPreference.sharedpreference_name);
	}

	public static int getNewAppreciationCount(Context context)
	{
		return SharedPreferenceData.getStraightIntSp(context,
				GobalConstants.SharedPreference.KEY_NEWAPPRECIATIONCOUNT,
				GobalConstants.SharedPreference.sharedpreference_name);
	}

	public static int getNewPostCount(Context context)
	{
		return SharedPreferenceData.getStraightIntSp(context,
				GobalConstants.SharedPreference.KEY_NEWPOSTCOUNT,
				GobalConstants.SharedPreference.sharedpreference_name);
	}

	public static void setNewPostCount(Context context, int count)
	{
		SharedPreferenceData.writeStraightIntSp(context,
				GobalConstants.SharedPreference.KEY_NEWPOSTCOUNT, count,
				GobalConstants.SharedPreference.sharedpreference_name);
	}

	public static int getNewMailCount(Context context)
	{
		return SharedPreferenceData.getStraightIntSp(context,
				GobalConstants.SharedPreference.KEY_NEWMAILCOUNT,
				GobalConstants.SharedPreference.sharedpreference_name);
	}

	public static void setNewMailCount(Context context, int count)
	{
		SharedPreferenceData.writeStraightIntSp(context,
				GobalConstants.SharedPreference.KEY_NEWMAILCOUNT, count,
				GobalConstants.SharedPreference.sharedpreference_name);
	}

	public static int getNewFeedCount(Context context)
	{
		return SharedPreferenceData.getStraightIntSp(context,
				GobalConstants.SharedPreference.KEY_NEWFEEDCOUNT,
				GobalConstants.SharedPreference.sharedpreference_name);
	}

	public static void setNewFeedCount(Context context, int count)
	{
		SharedPreferenceData.writeStraightIntSp(context,
				GobalConstants.SharedPreference.KEY_NEWFEEDCOUNT, count,
				GobalConstants.SharedPreference.sharedpreference_name);
	}

	public static void setNewQuestionCount(Context context, int count)
	{
		SharedPreferenceData.writeStraightIntSp(context,
				GobalConstants.SharedPreference.KEY_NEWQUESTIONCOUNT, count,
				GobalConstants.SharedPreference.sharedpreference_name);
	}

	public static int getNewQuestionCount(Context context)
	{
		return SharedPreferenceData.getStraightIntSp(context,
				GobalConstants.SharedPreference.KEY_NEWQUESTIONCOUNT,
				GobalConstants.SharedPreference.sharedpreference_name);
	}

	public static void setBestAnswerCount(Context context, int count)
	{
		SharedPreferenceData.writeStraightIntSp(context,
				GobalConstants.SharedPreference.KEY_BESTANSWERCOUNT, count,
				GobalConstants.SharedPreference.sharedpreference_name);
	}

	public static int getBestAnswerCount(Context context)
	{
		return SharedPreferenceData.getStraightIntSp(context,
				GobalConstants.SharedPreference.KEY_BESTANSWERCOUNT,
				GobalConstants.SharedPreference.sharedpreference_name);
	}*/

	public static void setSystemMsgCount(Context context, int count)
	{
		SharedPreferenceData.writeStraightIntSp(context,
				GobalConstants.SharedPreference.KEY_SYSTEMMSGCOUNT, count,
				GobalConstants.SharedPreference.sharedpreference_name);
	}

	public static int getSystemMsgCount(Context context)
	{
		return SharedPreferenceData.getStraightIntSp(context,
				GobalConstants.SharedPreference.KEY_SYSTEMMSGCOUNT,
				GobalConstants.SharedPreference.sharedpreference_name);
	}
	
//	最新返利数：fanliCount
//	最新提现数：cashCount
	public static void setFanliCount(Context context, int count)
	{
		SharedPreferenceData.writeStraightIntSp(context,
				GobalConstants.SharedPreference.KEY_FANLICOUNT, count,
				GobalConstants.SharedPreference.sharedpreference_name);
	}
	
	public static int getFanliCount(Context context)
	{
		return SharedPreferenceData.getStraightIntSp(context,
				GobalConstants.SharedPreference.KEY_FANLICOUNT,
				GobalConstants.SharedPreference.sharedpreference_name);
	}
	public static void setCashCount(Context context, int count)
	{
		SharedPreferenceData.writeStraightIntSp(context,
				GobalConstants.SharedPreference.KEY_CASHCOUNT, count,
				GobalConstants.SharedPreference.sharedpreference_name);
	}
	
	public static int getCashCount(Context context)
	{
		return SharedPreferenceData.getStraightIntSp(context,
				GobalConstants.SharedPreference.KEY_CASHCOUNT,
				GobalConstants.SharedPreference.sharedpreference_name);
	}

	/*public static void setQuestionMaxSeqId(Context context, long maxSeqId)
	{
		SharedPreferenceData.writeStraightLongSp(context,
				GobalConstants.SharedPreference.KEY_QUESTIONMAXSEQID, maxSeqId,
				GobalConstants.SharedPreference.sharedpreference_name);
	}

	public static long getQuestionMaxSeqId(Context context)
	{
		return SharedPreferenceData.getStraightLongSp(context,
				GobalConstants.SharedPreference.KEY_QUESTIONMAXSEQID,
				GobalConstants.SharedPreference.sharedpreference_name);
	}

	public static void setPostMaxSeqId(Context context, long maxSeqId)
	{
		SharedPreferenceData.writeStraightLongSp(context,
				GobalConstants.SharedPreference.KEY_POSTMAXSEQID, maxSeqId,
				GobalConstants.SharedPreference.sharedpreference_name);
	}

	public static long getPostMaxSeqId(Context context)
	{
		return SharedPreferenceData.getStraightLongSp(context,
				GobalConstants.SharedPreference.KEY_POSTMAXSEQID,
				GobalConstants.SharedPreference.sharedpreference_name);
	}

	public static void setMailMaxSeqId(Context context, long maxSeqId)
	{
		SharedPreferenceData.writeStraightLongSp(context,
				GobalConstants.SharedPreference.KEY_MAILMAXSEQID, maxSeqId,
				GobalConstants.SharedPreference.sharedpreference_name);
	}

	public static long getMailMaxSeqId(Context context)
	{
		return SharedPreferenceData.getStraightLongSp(context,
				GobalConstants.SharedPreference.KEY_MAILMAXSEQID,
				GobalConstants.SharedPreference.sharedpreference_name);
	}

	public static void setFeedMaxSeqId(Context context, long maxSeqId)
	{
		SharedPreferenceData.writeStraightLongSp(context,
				GobalConstants.SharedPreference.KEY_FEEDMAXSEQID, maxSeqId,
				GobalConstants.SharedPreference.sharedpreference_name);
	}

	public static long getFeedMaxSeqId(Context context)
	{
		return SharedPreferenceData.getStraightLongSp(context,
				GobalConstants.SharedPreference.KEY_FEEDMAXSEQID,
				GobalConstants.SharedPreference.sharedpreference_name);
	}

	public static void setArticleMaxSeqId(Context context, long maxSeqId)
	{
		SharedPreferenceData.writeStraightLongSp(context,
				GobalConstants.SharedPreference.KEY_ARTICLEMAXSEQID, maxSeqId,
				GobalConstants.SharedPreference.sharedpreference_name);
	}

	public static long getArticleMaxSeqId(Context context)
	{
		return SharedPreferenceData.getStraightLongSp(context,
				GobalConstants.SharedPreference.KEY_ARTICLEMAXSEQID,
				GobalConstants.SharedPreference.sharedpreference_name);
	}*/

	/*public static void setInviteUid(Context context, long uid)
	{
		SharedPreferenceData.writeStraightLongSp(context,
				GobalConstants.SharedPreference.KEY_INVITEUID, uid,
				GobalConstants.SharedPreference.sharedpreference_name);
	}
	
	public static long getInviteUid(Context context)
	{
		return SharedPreferenceData.getStraightLongSp(context,
				GobalConstants.SharedPreference.KEY_INVITEUID,
				GobalConstants.SharedPreference.sharedpreference_name,-1);
	}*/
	/*public static void setAskShare(Context context, boolean status)
	{
		SharedPreferenceData.writeStraightBooleanSp(context,
				GobalConstants.SharedPreference.KEY_ASKSHARE, status,
				GobalConstants.SharedPreference.sharedpreference_name);
	}

	public static boolean getAskShare(Context context)
	{
		return SharedPreferenceData.getStraightBooleanSp(context,
				GobalConstants.SharedPreference.KEY_ASKSHARE,
				GobalConstants.SharedPreference.sharedpreference_name);
	}

	public static void setAnswerShare(Context context, boolean status)
	{
		SharedPreferenceData.writeStraightBooleanSp(context,
				GobalConstants.SharedPreference.KEY_ANSWERSHARE, status,
				GobalConstants.SharedPreference.sharedpreference_name);
	}

	public static boolean getAnswerShare(Context context)
	{
		return SharedPreferenceData.getStraightBooleanSp(context,
				GobalConstants.SharedPreference.KEY_ANSWERSHARE,
				GobalConstants.SharedPreference.sharedpreference_name);
	}

	public static void setThankShare(Context context, boolean status)
	{
		SharedPreferenceData.writeStraightBooleanSp(context,
				GobalConstants.SharedPreference.KEY_THANKSHARE, status,
				GobalConstants.SharedPreference.sharedpreference_name);
	}

	public static boolean getThankShare(Context context)
	{
		return SharedPreferenceData.getStraightBooleanSp(context,
				GobalConstants.SharedPreference.KEY_THANKSHARE,
				GobalConstants.SharedPreference.sharedpreference_name);
	}

	public static void setBestAnswerShare(Context context, boolean status)
	{
		SharedPreferenceData.writeStraightBooleanSp(context,
				GobalConstants.SharedPreference.KEY_BESTANSWERSHARE, status,
				GobalConstants.SharedPreference.sharedpreference_name);
	}

	public static boolean getBestAnswerShare(Context context)
	{
		return SharedPreferenceData.getStraightBooleanSp(context,
				GobalConstants.SharedPreference.KEY_BESTANSWERSHARE,
				GobalConstants.SharedPreference.sharedpreference_name);
	}
*/
	public static void setPushShare(Context context, boolean status)
	{
		SharedPreferenceData.writeStraightBooleanSp(context,
				GobalConstants.SharedPreference.KEY_PUSHSHARE, status,
				GobalConstants.SharedPreference.sharedpreference_name);
	}

	public static boolean getPushShare(Context context)
	{
		return SharedPreferenceData.getStraightBooleanSp(context,
				GobalConstants.SharedPreference.KEY_PUSHSHARE,
				GobalConstants.SharedPreference.sharedpreference_name);
	}

	public static void setLastPrivateVer(Context context, boolean lastPrivateVer)
	{
		SharedPreferenceData.writeStraightBooleanSp(context,
				GobalConstants.SharedPreference.KEY_LASTPRIVATEVER,
				lastPrivateVer,
				GobalConstants.SharedPreference.sharedpreference_name);
	}

	public static int getLastPrivateVer(Context context)
	{
		return SharedPreferenceData.getStraightIntSp(context,
				GobalConstants.SharedPreference.KEY_LASTPRIVATEVER,
				GobalConstants.SharedPreference.sharedpreference_name);
	}

	public static void setQQBound(Context context, boolean bound)
	{
		SharedPreferenceData.writeStraightBooleanSp(context,
				GobalConstants.SharedPreference.KEY_QQBOUND, bound,
				GobalConstants.SharedPreference.sharedpreference_name);
	}

	public static boolean getQQBound(Context context)
	{
		return SharedPreferenceData.getStraightBooleanSp(context,
				GobalConstants.SharedPreference.KEY_QQBOUND,
				GobalConstants.SharedPreference.sharedpreference_name);
	}

	public static void setWeixinBound(Context context, boolean bound)
	{
		SharedPreferenceData.writeStraightBooleanSp(context,
				GobalConstants.SharedPreference.KEY_WEIXINBOUND, bound,
				GobalConstants.SharedPreference.sharedpreference_name);
	}

	public static boolean getWeixinBound(Context context)
	{
		return SharedPreferenceData.getStraightBooleanSp(context,
				GobalConstants.SharedPreference.KEY_WEIXINBOUND,
				GobalConstants.SharedPreference.sharedpreference_name);
	}
}
