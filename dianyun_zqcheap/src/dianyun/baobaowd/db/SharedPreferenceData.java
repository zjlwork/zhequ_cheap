package dianyun.baobaowd.db;

import android.content.Context;
import android.content.SharedPreferences;

public class SharedPreferenceData
{

	public static SharedPreferences getSharedPreferenceWithKey(Context context,
			int strId)
	{
		String name = context.getResources().getString(strId);
		SharedPreferences sp = context.getSharedPreferences(name, 0);
		return sp;
	}

	public static SharedPreferences getSharedPreferenceWithKey(Context context,
			String spName)
	{
		SharedPreferences sp = context.getSharedPreferences(spName, 0);
		return sp;
	}

	public static int getIntSp(Context context, int strId,
			String sharedpreference_name)
	{
		String key = context.getResources().getString(strId);
		SharedPreferences sp = getSharedPreferenceWithKey(context,
				sharedpreference_name);
		int value = sp.getInt(key, 0);
		return value;
	}

	public static int getStraightIntSp(Context context, String key,
			String sharedpreference_name)
	{
		SharedPreferences sp = getSharedPreferenceWithKey(context,
				sharedpreference_name);
		int value = 0;
		if (sp != null)
		{
			value = sp.getInt(key, 0);
		}
		return value;
	}

	public static void writeIntSp(Context context, int strId, int value,
			String sharedpreference_name)
	{
		String key = context.getResources().getString(strId);
		SharedPreferences sp = getSharedPreferenceWithKey(context,
				sharedpreference_name);
		SharedPreferences.Editor editor = sp.edit();
		editor.putInt(key, value);
		editor.commit();
	}

	public static void writeStraightIntSp(Context context, String key,
			int value, String sharedpreference_name)
	{
		SharedPreferences sp = getSharedPreferenceWithKey(context,
				sharedpreference_name);
		SharedPreferences.Editor editor = sp.edit();
		editor.putInt(key, value);
		editor.commit();
	}

	public static String getStringSp(Context context, int strId,
			String sharedpreference_name)
	{
		String key = context.getResources().getString(strId);
		SharedPreferences sp = getSharedPreferenceWithKey(context,
				sharedpreference_name);
		String value = sp.getString(key, "");
		return value;
	}

	public static String getStraightStringSp(Context context, String key,
			String sharedpreference_name)
	{
		SharedPreferences sp = getSharedPreferenceWithKey(context,
				sharedpreference_name);
		String value = sp.getString(key, "");
		return value;
	}

	public static void writeStringSp(Context context, int strId, String value,
			String sharedpreference_name)
	{
		String key = context.getResources().getString(strId);
		SharedPreferences sp = getSharedPreferenceWithKey(context,
				sharedpreference_name);
		SharedPreferences.Editor editor = sp.edit();
		editor.putString(key, value);
		editor.commit();
	}

	public static void writeStraightStringSp(Context context, String key,
			String value, String sharedpreference_name)
	{
		SharedPreferences sp = getSharedPreferenceWithKey(context,
				sharedpreference_name);
		SharedPreferences.Editor editor = sp.edit();
		editor.putString(key, value);
		editor.commit();
	}

	public static boolean getBooleanSp(Context context, int strId,
			String sharedpreference_name)
	{
		String key = context.getResources().getString(strId);
		SharedPreferences sp = getSharedPreferenceWithKey(context,
				sharedpreference_name);
		boolean value = sp.getBoolean(key, false);
		return value;
	}

	public static void writeBooleanSp(Context context, int strId,
			boolean value, String sharedpreference_name)
	{
		String key = context.getResources().getString(strId);
		SharedPreferences sp = getSharedPreferenceWithKey(context,
				sharedpreference_name);
		SharedPreferences.Editor editor = sp.edit();
		editor.putBoolean(key, value);
		editor.commit();
	}

	public static float getFloatSp(Context context, int strId,
			String sharedpreference_name)
	{
		String key = context.getResources().getString(strId);
		SharedPreferences sp = getSharedPreferenceWithKey(context,
				sharedpreference_name);
		float value = sp.getFloat(key, 0);
		return value;
	}

	public static void writeFloatSp(Context context, int strId, float value,
			String sharedpreference_name)
	{
		String key = context.getResources().getString(strId);
		SharedPreferences sp = getSharedPreferenceWithKey(context,
				sharedpreference_name);
		SharedPreferences.Editor editor = sp.edit();
		editor.putFloat(key, value);
		editor.commit();
	}

	public static long getLongSp(Context context, int strId,
			String sharedpreference_name)
	{
		String key = context.getResources().getString(strId);
		SharedPreferences sp = getSharedPreferenceWithKey(context,
				sharedpreference_name);
		long value = sp.getLong(key, 0);
		return value;
	}

	public static void writeLongSp(Context context, int strId, long value,
			String sharedpreference_name)
	{
		String key = context.getResources().getString(strId);
		SharedPreferences sp = getSharedPreferenceWithKey(context,
				sharedpreference_name);
		SharedPreferences.Editor editor = sp.edit();
		editor.putLong(key, value);
		editor.commit();
	}

	public static long getStraightLongSp(Context context, String key,
			String sharedpreference_name)
	{
		SharedPreferences sp = getSharedPreferenceWithKey(context,
				sharedpreference_name);
		long value = sp.getLong(key, 0);
		return value;
	}
	public static long getStraightLongSp(Context context, String key,
			String sharedpreference_name,long defaultValue)
	{
		SharedPreferences sp = getSharedPreferenceWithKey(context,
				sharedpreference_name);
		long value = sp.getLong(key, defaultValue);
		return value;
	}

	public static void writeStraightLongSp(Context context, String key,
			long value, String sharedpreference_name)
	{
		SharedPreferences sp = getSharedPreferenceWithKey(context,
				sharedpreference_name);
		SharedPreferences.Editor editor = sp.edit();
		editor.putLong(key, value);
		editor.commit();
	}

	public static void writeStraightFloatSp(Context context, String key,
			float value, String sharedpreference_name)
	{
		SharedPreferences sp = getSharedPreferenceWithKey(context,
				sharedpreference_name);
		SharedPreferences.Editor editor = sp.edit();
		editor.putFloat(key, value);
		editor.commit();
	}

	public static float getStraightFloatSp(Context context, String key,
			String sharedpreference_name)
	{
		SharedPreferences sp = getSharedPreferenceWithKey(context,
				sharedpreference_name);
		return sp.getFloat(key, 0);
	}

	public static boolean getStraightBooleanSp(Context context, String key,
			String sharedpreference_name)
	{
		SharedPreferences sp = getSharedPreferenceWithKey(context,
				sharedpreference_name);
		boolean value = sp.getBoolean(key, false);
		return value;
	}

	public static boolean getStraightBooleanSp2(Context context, String key,
			String sharedpreference_name)
	{
		SharedPreferences sp = getSharedPreferenceWithKey(context,
				sharedpreference_name);
		synchronized (sp)
		{
			boolean value = sp.getBoolean(key, false);
			return value;
		}
	}

	public static void writeStraightBooleanSp(Context context, String key,
			boolean value, String sharedpreference_name)
	{
		SharedPreferences sp = getSharedPreferenceWithKey(context,
				sharedpreference_name);
		SharedPreferences.Editor editor = sp.edit();
		editor.putBoolean(key, value);
		editor.commit();
	}

	public static void writeIsTipLoginOrNotBackFeesBooleanSp(Context context,
			String key, boolean value, String sharedpreference_name)
	{
		SharedPreferences sp = getSharedPreferenceWithKey(context,
				sharedpreference_name);
		SharedPreferences.Editor editor = sp.edit();
		editor.putBoolean(key, value);
		editor.commit();
	}
}
