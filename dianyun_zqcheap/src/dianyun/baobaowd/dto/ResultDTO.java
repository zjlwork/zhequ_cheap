package dianyun.baobaowd.dto;

public class ResultDTO
{

	private String code;
	private String resultStr;
	private String errorMsg;
    public String keywords="";

	public String getCode()
	{
		return code;
	}

	public void setCode(String code)
	{
		this.code = code;
	}

	public String getResult()
	{
		return resultStr;
	}

	public void setResult(String result)
	{
		this.resultStr = result;
	}

	public String getErrorMsg()
	{
		return errorMsg;
	}

	public void setErrorMsg(String errorMsg)
	{
		this.errorMsg = errorMsg;
	}

	public ResultDTO(String code, String result, String errorMsg)
	{
		super();
		this.code = code;
		this.resultStr = result;
		this.errorMsg = errorMsg;
	}

	public ResultDTO()
	{
		super();
	}
}
