package dianyun.baobaowd.qq;

public class QQLoginResult
{

	// {"ret":0,"pay_token":"1737147D3FA59FE6D51A697395DC289A",
	// "pf":"openmobile_android","md5str":null,
	// "expires_in":"7776000","openid":"47F8383CC1427CD3683FC67C9DE00F31",
	// "pfkey":"4f52ea61ad781aa936041cb09de30e13","msg":"sucess","access_token":"E3F6B1D6DA95AFA8711C3B94EFE8CD72"}
	// {"appid":"100462532","pay_token":"6D168ECF71B62F6319CEE039601DFE48","openid":"22902BD0507941846495A2126393E048",
	// "expires_in":"7776000","pf":"openmobile_android","pfkey":"8c819f872cd77cf0053bb0d79d51a877","access_token":"554BFF285EA4D0A7EFDF56AB289F064C"}
	QQLoginResult()
	{
	}
	private long expires_in;
	private String access_token;
	private String openid;

	public String getAccess_token()
	{
		return access_token;
	}

	public void setAccess_token(String access_token)
	{
		this.access_token = access_token;
	}

	public String getOpenid()
	{
		return openid;
	}

	public void setOpenid(String openid)
	{
		this.openid = openid;
	}

	public long getExpires_in()
	{
		return expires_in;
	}

	public void setExpires_in(long expires_in)
	{
		this.expires_in = expires_in;
	}
}
