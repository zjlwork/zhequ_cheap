package dianyun.baobaowd.qq;

public class QQConstants
{

	// public static final String QQSCOPE = "all";
	// public static final String QQSCOPE =
	// "get_user_info,get_simple_userinfo,get_user_profile,get_app_friends,"
	// +
	// "add_share,add_topic,list_album,upload_pic,add_album,set_user_face,get_vip_info,get_vip_rich_info,get_intimate_friends_weibo,match_nick_tips_weibo";
	public static final String QQSCOPE = "get_user_info,get_simple_userinfo,add_share";
	// public static final String QQSCOPE = "get_user_info";
	public static final String ACCESS_TOKEN = "access_token";
	public static final String OPENID = "openid";
	public static final String EXPIRES_IN = "expires_in";
	public static final String USERINFOJSON = "userinfo_json";
}
