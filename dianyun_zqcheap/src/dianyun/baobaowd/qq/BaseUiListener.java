package dianyun.baobaowd.qq;

import org.json.JSONObject;
import com.tencent.tauth.IUiListener;
import com.tencent.tauth.UiError;

public class BaseUiListener implements IUiListener
{

	@Override
	public void onComplete(Object response)
	{
		doComplete((JSONObject) response);
	}

	protected void doComplete(JSONObject values)
	{
	}

	@Override
	public void onError(UiError e)
	{
		System.out.println("e.errorMessage===" + e.errorMessage);
	}

	@Override
	public void onCancel()
	{
	}
}