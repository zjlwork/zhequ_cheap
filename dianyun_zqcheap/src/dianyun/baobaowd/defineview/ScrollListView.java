package dianyun.baobaowd.defineview;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import dianyun.baobaowd.entity.Menu;
import dianyun.zqcheap.R;

public class ScrollListView extends LinearLayout 
{

	private Context mContext;
	private List<Menu> mMenuList = new ArrayList<Menu>() ;
	private LinearLayout parent_layout;
	private ScrollView scrollview;
	
	public ScrollListView(Context context, AttributeSet attrs,
			int defStyle)
	{
		super(context, attrs, defStyle);
		initView(context);
	}

	public ScrollListView(Context context, AttributeSet attrs)
	{
		super(context, attrs);
		initView(context);
	}

	public ScrollListView(Context context)
	{
		super(context);
		initView(context);
	}

	private void initView(Context context)
	{
		mContext = context;
		View view = LayoutInflater.from(mContext).inflate(
				R.layout.scrolllistview_lay, this, false);

		parent_layout = (LinearLayout) view
				.findViewById(R.id.parent_layout);
		scrollview = (ScrollView) view
				.findViewById(R.id.scrollview);
		addView(view);
	}
	private int mSelectedPosition=-1;
	public Menu getSelectedMenu(){
		if(mSelectedPosition==-1) return null;
		return mMenuList.get(mSelectedPosition);
	}
	public List<Menu> getMenuList(){
		return mMenuList;
	}
	private  ItemSelectedCallback mItemSelectedCallback;
	
	
	
	private void changeSelected(int position){
		for(int i=0;i<parent_layout.getChildCount();i++){
			View child = parent_layout.getChildAt(i);
			View itemLayout = child.findViewById(R.id.item_layout);
			View selectedV = child.findViewById(R.id.selected_v);
			TextView titleTv = (TextView)child.findViewById(R.id.title_tv);
			if(position==i){
				selectedV.setVisibility(View.VISIBLE);
				itemLayout.setSelected(true);
				titleTv.setSelected(true);
				mSelectedPosition = position;
			}else{
				selectedV.setVisibility(View.GONE);
				itemLayout.setSelected(false);
				titleTv.setSelected(false);
			}
		}
	}
	
	
	private void notifyData(){
		parent_layout.removeAllViewsInLayout();
		if(mMenuList.size()>0){
			for(int i=0;i<mMenuList.size();i++){ 
				final Menu menu =mMenuList.get(i) ;
				View view =LayoutInflater.from(mContext).inflate(R.layout.scrolllistview_item, null);
				View itemLayout = view.findViewById(R.id.item_layout);
				final int height = itemLayout.getLayoutParams().height;
				final int position = i;
				view.setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View v) {
						int scrollSize =	position*height;
	//					System.out.println("scrollSize======="+scrollSize);
						scrollview.smoothScrollTo(0, scrollSize);
						changeSelected(position);
						if(mItemSelectedCallback!=null){
							mItemSelectedCallback.itemSelected(menu);
						}
						
					}
				});
				TextView titleTv = (TextView)view.findViewById(R.id.title_tv);
				titleTv.setText(menu.name);
				parent_layout.addView(view);
			}
			changeSelected(0);
		}
	}
	
	public void  setItemSelectedCallback(ItemSelectedCallback mItemSelectedCallback){
		this.mItemSelectedCallback = mItemSelectedCallback;
	}
	public void setDataSource(List<Menu> menuList)
	{
		if(menuList!=null&&menuList.size()>0){
			mMenuList.clear();
			mMenuList.addAll(menuList);
			notifyData();
		}
	}
	
	
	
	

	

//	private void initData()
//	{
//		mDataList = ShopDBHelper.getShopMenu_CatentByCategory(mContext,
//				mMenuID, mCategory.cateId);
//		if (mDataList == null || mDataList.size() == 0)
//		{
//			// 请求服务器
//			ShopHttpHelper.getChildData(mContext, false,
//					String.valueOf(mMenuID), String.valueOf(mCategory.cateId),
//					false, true, new ShopHttpHelper.ShopDataCallback()
//					{
//
//						@Override
//						public void getMenu(List<Menu> result)
//						{
//							// TODO Auto-generated method stub
//						}
//
//						@Override
//						public void getChildsData(List<CateItem> result)
//						{
//							mDataList = result;
//							refreshData();
//						}
//					});
//		}
//		else
//		{
//			for (int i = 0; i < mImageList.size(); i++)
//			{
//				if (mDataList.size() > i)
//				{
//					CateItem item = mDataList.get(i);
//					if (item != null)
//					{
//						String url = "";
//						if (item.xType != 3)
//						{
//							url = (item.pic == null ? "" : item.pic);
//						}
//						else
//						{
//							url = (item.pics == null ? "" : item.pics.get(0));
//						}
//						ImageLoader.getInstance()
//								.displayImage(url, mImageList.get(i),
//										BaoBaoWDApplication.mOptions);
//					}
//				}
//			}
//			if (Utils.isNetAvailable(mContext))
//			{
//				// 请求服务器
//				ShopHttpHelper.getChildData(mContext, false,
//						String.valueOf(mMenuID),
//						String.valueOf(mCategory.cateId), false, true,
//						new ShopHttpHelper.ShopDataCallback()
//						{
//
//							@Override
//							public void getMenu(List<Menu> result)
//							{
//								// TODO Auto-generated method stub
//							}
//
//							@Override
//							public void getChildsData(List<CateItem> result)
//							{
//								if (mDataList != null && result != null)
//								{
//									mDataList = result;
//									refreshData();
//								}
//							}
//						});
//			}
//		}
//	}

	
	
	public interface ItemSelectedCallback{
		public void itemSelected(Menu Menu);
		
	}
	
	
}
