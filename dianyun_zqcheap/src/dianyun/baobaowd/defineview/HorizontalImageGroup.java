package dianyun.baobaowd.defineview;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;

import dianyun.baobaowd.db.LightDBHelper;
import dianyun.baobaowd.db.ShopDBHelper;
import dianyun.baobaowd.entity.CateItem;
import dianyun.baobaowd.entity.Constants;
import dianyun.baobaowd.entity.LocalMenu;
import dianyun.baobaowd.help.ShopHttpHelper;
import dianyun.baobaowd.interfaces.DialogCallBack;
import dianyun.baobaowd.util.GobalConstants;
import dianyun.baobaowd.util.TaeSdkUtil;
import dianyun.baobaowd.util.ToastHelper;
import dianyun.baobaowd.util.UserHelper;
import dianyun.baobaowd.util.Utils;
import dianyun.zqcheap.R;
import dianyun.zqcheap.activity.GoodsListActivity;
import dianyun.zqcheap.activity.HtmlActivity;
import dianyun.zqcheap.activity.PrizesActivity;
import dianyun.zqcheap.activity.ShopSpecialTopicDetailActivity;
import dianyun.zqcheap.activity.ShopSpecialTopicListActivity;
import dianyun.zqcheap.application.BaoBaoWDApplication;

public class HorizontalImageGroup extends LinearLayout implements
		OnClickListener
{

	private Context mContext;
	private List<ImageView> mImageList = new ArrayList<ImageView>();
	private TextView mMoreTV;
	private LinearLayout mImageContainer;
	private CateItem mCategory;
	private long mMenuID;
	private List<CateItem> mDataList;
	private View mCurrentActivityView;
	public HorizontalImageGroup(Context context, AttributeSet attrs,
			int defStyle)
	{
		super(context, attrs, defStyle);
		initView(context);
	}

	public HorizontalImageGroup(Context context, AttributeSet attrs)
	{
		super(context, attrs);
		initView(context);
	}

	public HorizontalImageGroup(Context context)
	{
		super(context);
		initView(context);
		// TODO Auto-generated constructor stub
	}

	private void initView(Context context)
	{
		mContext = context;
		View view = LayoutInflater.from(mContext).inflate(
				R.layout.horizontalimagegroup_lay, this, false);
		mMoreTV = (TextView) view
				.findViewById(R.id.horizontal_image_group_more_horizontal);
		mImageContainer = (LinearLayout) view
				.findViewById(R.id.horizontal_image_group_img_lay);
		if (mImageContainer != null)
		{
			ImageView img0 = (ImageView) mImageContainer
					.findViewById(R.id.horizontal_image_group_image1);
			ImageView img1 = (ImageView) mImageContainer
					.findViewById(R.id.horizontal_image_group_image2);
			ImageView img2 = (ImageView) mImageContainer
					.findViewById(R.id.horizontal_image_group_image3);
			img0.setOnClickListener(this);
			img1.setOnClickListener(this);
			img2.setOnClickListener(this);
			mImageList.add(img0);
			mImageList.add(img1);
			mImageList.add(img2);
		}
		addView(view);
	}
	public void setCurrentActivityRootView(View activityView)
	{
		mCurrentActivityView = activityView;
	}
	public void setDataSource(CateItem category, long menuID)
	{
		mCategory = category;
		mMenuID = menuID;
		initData();
	}

	private void goTargetActivity(final CateItem item)
	{
		if (item != null && item.clickType != null && item.clickType != 0)
		{
			int xtype = item.xType;
			int clickType = item.clickType;
			if (item == null || item.clickType == null)
			{
				return;
			}
			Intent itn = null;
			// 点击类型，1：专辑列表，2：专辑详情，3：商品分类列表，4：webview
			switch (item.clickType)
			{
			case 1:
				itn = new Intent(mContext, ShopSpecialTopicListActivity.class);
				itn.putExtra(Constants.OBJECT_EXTRA_NAME,
						item.cateId == null ? 0L : item.cateId);
				itn.putExtra(Constants.EXTRA_NAME, item.pageTitle == null ? ""
						: item.pageTitle);
				break;
			case 2:
				itn = new Intent(mContext, ShopSpecialTopicDetailActivity.class);
				itn.putExtra(Constants.OBJECT_EXTRA_NAME,
						item.cateId == null ? 0L : item.cateId);
				itn.putExtra(Constants.EXTRA_NAME, item.pageTitle == null ? ""
						: item.pageTitle);
				break;
			case 3:
				itn = new Intent(mContext, GoodsListActivity.class);
				itn.putExtra(Constants.OBJECT_EXTRA_NAME,
						item.cateId == null ? 0L : item.cateId);
				itn.putExtra(Constants.EXTRA_NAME, item.pageTitle == null ? ""
						: item.pageTitle);
				break;
			case 4:
				itn = new Intent(mContext, HtmlActivity.class);
				itn.putExtra(GobalConstants.Data.URL,
						item.clickUrl == null ? "" : item.clickUrl);
				itn.putExtra(GobalConstants.Data.TITLE, item.pageTitle == null ? ""
						: item.pageTitle);
				break;
			case 5:
				if (xtype == 3 && item.tbItemId != null)
				{
					// 如果是单品的话
					if (!LightDBHelper.getIsNotTipLoginOrNotFees(mContext)
							&& UserHelper.getUser().getIsGuest() == GobalConstants.UserType.ISSELF&&mCurrentActivityView!=null)
					{
//						ToastHelper.showTipLoginDialogWhenShop(mContext,mCurrentActivityView,
//								new ToastHelper.dialogCancelCallback()
//								{
//
//									@Override
//									public void onCancle(boolean isNeverTip)
//									{
//										LightDBHelper
//												.setIsNotTipLoginOrNotFees(
//														mContext, isNeverTip);
//										TaeSdkUtil.showTAEItemDetail((Activity) mContext,
//												item.tbItemId,item.taobaoPid,item.isTk==1, item.itemType, null,mCurrentActivityView);
//									}
//								});
						
						ToastHelper.showGoLoginDialog(mContext, new DialogCallBack() {
							
							@Override
							public void clickSure() {
								
							}
							
							@Override
							public void clickCancel() {
								TaeSdkUtil.showTAEItemDetail((Activity) mContext,
										item.tbItemId,item.taobaoPid,item.isTk==1, item.itemType, null,mCurrentActivityView);
							}
						});
						
						
					}
					else
					{
						TaeSdkUtil.showTAEItemDetail((Activity) mContext,
								item.tbItemId,item.taobaoPid,item.isTk==1, item.itemType, null,mCurrentActivityView);
					}
				}
			case 6:
				// 金币商城
				itn = new Intent(mContext, PrizesActivity.class);
				break;
			default:
				break;
			}
			if (itn != null)
			{
				mContext.startActivity(itn);
			}
		}
	}

	private void initData()
	{
		mDataList = ShopDBHelper.getShopMenu_CatentByCategory(mContext,
				mMenuID, mCategory.cateId);
		if (mDataList == null || mDataList.size() == 0)
		{
			// 请求服务器
			ShopHttpHelper.getChildData(mContext, false,
					String.valueOf(mMenuID), String.valueOf(mCategory.cateId),
					false, true, new ShopHttpHelper.ShopDataCallback()
					{

						@Override
						public void getMenu(List<LocalMenu> result)
						{
							// TODO Auto-generated method stub
						}

						@Override
						public void getChildsData(List<CateItem> result)
						{
							mDataList = result;
							refreshData();
						}
					});
		}
		else
		{
			for (int i = 0; i < mImageList.size(); i++)
			{
				if (mDataList.size() > i)
				{
					CateItem item = mDataList.get(i);
					if (item != null)
					{
						String url = "";
						if (item.xType != 3)
						{
							url = (item.pic == null ? "" : item.pic);
						}
						else
						{
							url = (item.pics == null ? "" : item.pics.get(0));
						}
						ImageLoader.getInstance()
								.displayImage(url, mImageList.get(i),
										BaoBaoWDApplication.mOptions);
					}
				}
			}
			if (Utils.isNetAvailable(mContext))
			{
				// 请求服务器
				ShopHttpHelper.getChildData(mContext, false,
						String.valueOf(mMenuID),
						String.valueOf(mCategory.cateId), false, true,
						new ShopHttpHelper.ShopDataCallback()
						{

							@Override
							public void getMenu(List<LocalMenu> result)
							{
								// TODO Auto-generated method stub
							}

							@Override
							public void getChildsData(List<CateItem> result)
							{
								if (mDataList != null && result != null)
								{
									mDataList = result;
									refreshData();
								}
							}
						});
			}
		}
	}

	private void refreshData()
	{
		if (mDataList != null)
		{
			for (int i = 0; i < mImageList.size(); i++)
			{
				if (mDataList.size() > i)
				{
					CateItem item = mDataList.get(i);
					String url = "";
					if (item.xType != 3)
					{
						url = (item.pic == null ? "" : item.pic);
					}
					else
					{
						url = (item.pics == null ? "" : item.pics.get(0));
					}
					ImageLoader.getInstance().displayImage(url,
							mImageList.get(i), BaoBaoWDApplication.mOptions);
				}
			}
		}
	}

	@Override
	public void onClick(View v)
	{
		switch (v.getId())
		{
		case R.id.horizontal_image_group_image1:
		case R.id.horizontal_image_group_image2:
		case R.id.horizontal_image_group_image3:
			int index = mImageList.indexOf((ImageView) v);
			if (mDataList != null && index >= 0 && index < mDataList.size())
			{
				goTargetActivity(mDataList.get(index));
			}
			break;
		}
	}
}
