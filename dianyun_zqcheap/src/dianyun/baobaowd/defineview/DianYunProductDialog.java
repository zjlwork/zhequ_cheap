package dianyun.baobaowd.defineview;

import android.app.Activity;
import android.content.Context;
import android.graphics.Paint;
import android.graphics.drawable.BitmapDrawable;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.PopupWindow.OnDismissListener;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;

import dianyun.baobaowd.db.LightDBHelper;
import dianyun.baobaowd.entity.CateItem;
import dianyun.baobaowd.interfaces.DialogCallBack;
import dianyun.baobaowd.util.GobalConstants;
import dianyun.baobaowd.util.TaeSdkUtil;
import dianyun.baobaowd.util.ToastHelper;
import dianyun.baobaowd.util.UserHelper;
import dianyun.zqcheap.R;
import dianyun.zqcheap.application.BaoBaoWDApplication;

public class DianYunProductDialog extends PopupWindow implements
		OnClickListener, OnDismissListener
{

	private Context mContext;
	private CateItem mItem;
	private TextView mCurrentPriceTV;
	private TextView mPriceTV;
	private TextView mGoodsTitleTV;
	private TextView mGoodsBackFeesTV;
	private ImageView mGoodsImageView;
	private View mParent;
	private RelativeLayout ll_popup;
	private Button mBtn_ReScan;
	private Button mBtn_GoBuy;
	private RestartScanInterface mRestartScanListener;
	private String mOriginUrl = "";

	public DianYunProductDialog(Context context, CateItem item, View parent,
			String originUrl)
	{
		super(context);
		mContext = context;
		mItem = item;
		mParent = parent;
		mOriginUrl = originUrl;
		View view = LayoutInflater.from(mContext).inflate(
				R.layout.dianyun_product_dialog_lay, null);
		view.startAnimation(AnimationUtils.loadAnimation(mContext,
				R.anim.fade_ins));
		ll_popup = (RelativeLayout) view
				.findViewById(R.id.shop_scan_result_total);
		ll_popup.startAnimation(AnimationUtils.loadAnimation(mContext,
				R.anim.push_bottom_in_2));
		setWidth(LayoutParams.FILL_PARENT);
		setHeight(LayoutParams.FILL_PARENT);
		setBackgroundDrawable(new BitmapDrawable());
		setOutsideTouchable(true);
		setContentView(view);
		update();
		initView(view);
	}

	private void initView(View view)
	{
		mCurrentPriceTV = (TextView) view.findViewById(R.id.goods_currentPrice);
		mPriceTV = (TextView) view.findViewById(R.id.goods_Price);
		mPriceTV.getPaint().setFlags(Paint.STRIKE_THRU_TEXT_FLAG); // 中划线
		mGoodsTitleTV = (TextView) view.findViewById(R.id.goods_title);
		mGoodsBackFeesTV = (TextView) view.findViewById(R.id.goods_backfees);
		mGoodsImageView = (ImageView) view.findViewById(R.id.goods_image);
		mBtn_ReScan = (Button) view.findViewById(R.id.scan_btn_restart);
		mBtn_GoBuy = (Button) view.findViewById(R.id.scan_btn_buy);
		mBtn_GoBuy.setOnClickListener(this);
		mBtn_ReScan.setOnClickListener(this);
		// 初始化
		ImageLoader.getInstance().displayImage(
				mItem.pics == null ? "" : mItem.pics.get(0), mGoodsImageView,
				BaoBaoWDApplication.mOptions);
		mGoodsTitleTV.setText(mItem.title);
		mCurrentPriceTV.setText("" + mItem.umpPrice);
		mPriceTV.setText("" + mItem.price);
		if (mItem.coins == null || mItem.coins == 0)
		{
			mGoodsBackFeesTV.setVisibility(View.VISIBLE);
			mGoodsBackFeesTV.setText("无返利");
		}
		else
		{
			mGoodsBackFeesTV.setVisibility(View.VISIBLE);
			mGoodsBackFeesTV.setText("返"
					+ (mItem.coins == null ? 0 : mItem.coins) + "金币");
		}
	}

	public void setRescanText(String string)
	{
		if (mBtn_ReScan != null)
		{
			mBtn_ReScan.setText(string);
		}
	}

	@Override
	public void onClick(View v)
	{
		switch (v.getId())
		{
		case R.id.scan_btn_restart:
			if (mRestartScanListener != null)
			{
				mRestartScanListener.restartScan();
			}
			break;
		case R.id.scan_btn_buy:
			goShopping();
			break;
		}
	}

	public void show()
	{
		if (mParent != null)
			showAtLocation(mParent, Gravity.BOTTOM, 0, 0);
	}

	public void setOnRestartOnClickListener(RestartScanInterface listener)
	{
		mRestartScanListener = listener;
	}

	public interface RestartScanInterface
	{

		public void restartScan();

		public void goBuy();

		public void onDimiss();
	}

	private void goShopping()
	{
		if (mItem != null)
		{
			// 如果是单品的话 并且有返利的话
			if (mItem.coins != null && mItem.coins > 0)
			{
				if (!LightDBHelper.getIsNotTipLoginOrNotFees(mContext)
						&& UserHelper.getUser().getIsGuest() == GobalConstants.UserType.ISSELF
						&& mParent != null)
				{
//					ToastHelper.showTipLoginDialogWhenShop(mContext, mParent,
//							new ToastHelper.dialogCancelCallback()
//							{
//
//								@Override
//								public void onCancle(boolean isNeverTip)
//								{
//									LightDBHelper.setIsNotTipLoginOrNotFees(
//											mContext, isNeverTip);
//									TaeSdkUtil.showTAEItemDetail(
//											(Activity) mContext,
//											mItem.tbItemId, mItem.taobaoPid,
//											mItem.isTk == 1, mItem.itemType,
//											null,mParent);
//								}
//							}, null);
					

					ToastHelper.showGoLoginDialog(mContext, new DialogCallBack() {
						
						@Override
						public void clickSure() {
							
						}
						
						@Override
						public void clickCancel() {
							TaeSdkUtil.showTAEItemDetail(
									(Activity) mContext,
									mItem.tbItemId, mItem.taobaoPid,
									mItem.isTk == 1, mItem.itemType,
									null,mParent);
						}
					});
					
					
					
					
				}
				else
				{
					if (mRestartScanListener != null)
					{
						mRestartScanListener.goBuy();
					}
					TaeSdkUtil.showTAEItemDetail((Activity) mContext,
							mItem.tbItemId, mItem.taobaoPid, mItem.isTk == 1,
							mItem.itemType, null,mParent);
					dismiss();
				}
			}
			else
			{
				// 假如没有返利 直接showpage
				TaeSdkUtil.showTAEItemDetail((Activity) mContext,
						mItem.tbItemId, mItem.taobaoPid, mItem.isTk == 1,
						mItem.itemType, null,mParent);
			}
		}
	}

	@Override
	public void onDismiss()
	{
		if (mRestartScanListener != null)
		{
			mRestartScanListener.onDimiss();
		}
	}
}
