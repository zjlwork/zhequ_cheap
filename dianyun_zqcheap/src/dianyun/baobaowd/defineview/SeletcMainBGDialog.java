package dianyun.baobaowd.defineview;

import java.util.ArrayList;
import java.util.List;

import android.app.Dialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import dianyun.baobaowd.db.LightDBHelper;
import dianyun.zqcheap.R;

/**
 * Created by pc on 2015/4/11.
 */
public class SeletcMainBGDialog extends Dialog implements View.OnClickListener {

    private Context mContext;
    private List<TextView> mBgNameTVList = new ArrayList<TextView>();
    private List<ImageView> mBgChooseImgList = new ArrayList<ImageView>();
    private List<View> mBgParentList = new ArrayList<View>();
    private View mRootView;
    private int mCuurentIndex = 0;
    private selectedCallback mCallback;


    public SeletcMainBGDialog(Context context) {
        super(context);
        initView(context);

    }

    public SeletcMainBGDialog(Context context, int theme) {
        super(context, theme);
        initView(context);
    }

    private void initView(Context context) {
        mContext = context;
        mRootView = LayoutInflater.from(mContext).inflate(R.layout.select_main_bg_dialog_lay, null);
        mBgNameTVList.add((TextView) mRootView.findViewById(R.id.select_bg_name1));
        mBgNameTVList.add((TextView) mRootView.findViewById(R.id.select_bg_name2));
        mBgNameTVList.add((TextView) mRootView.findViewById(R.id.select_bg_name3));
        mBgNameTVList.add((TextView) mRootView.findViewById(R.id.select_bg_name4));


        mBgChooseImgList.add((ImageView) mRootView.findViewById(R.id.select_bg_choose1));
        mBgChooseImgList.add((ImageView) mRootView.findViewById(R.id.select_bg_choose2));
        mBgChooseImgList.add((ImageView) mRootView.findViewById(R.id.select_bg_choose3));
        mBgChooseImgList.add((ImageView) mRootView.findViewById(R.id.select_bg_choose4));

        mBgParentList.add(mRootView.findViewById(R.id.select_bg_lay1));
        mBgParentList.add(mRootView.findViewById(R.id.select_bg_lay2));
        mBgParentList.add(mRootView.findViewById(R.id.select_bg_lay3));
        mBgParentList.add(mRootView.findViewById(R.id.select_bg_lay4));

        mRootView.findViewById(R.id.select_bg_image1).setOnClickListener(this);
        mRootView.findViewById(R.id.select_bg_image2).setOnClickListener(this);
        mRootView.findViewById(R.id.select_bg_image3).setOnClickListener(this);
        mRootView.findViewById(R.id.select_bg_image4).setOnClickListener(this);
        setContentView(mRootView);
        setCanceledOnTouchOutside(true);
        setCancelable(true);



    }

    @Override
    public void onAttachedToWindow() {
        initData();
        super.onAttachedToWindow();
    }

    private void initData() {
        mCuurentIndex = LightDBHelper.getMainBgIndex(mContext);
        for (int i = 0; i < mBgChooseImgList.size(); i++) {
            if (i == mCuurentIndex) {
                mBgChooseImgList.get(i).setVisibility(View.VISIBLE);
                mBgNameTVList.get(i).setTextAppearance(mContext, R.style.main_fragment_select_background_name_selected_text_style);
                mBgParentList.get(i).setBackgroundColor(mContext.getResources().getColor(R.color.select_bg_selected_color));
            } else {
                mBgChooseImgList.get(i).setVisibility(View.GONE);
                mBgNameTVList.get(i).setTextAppearance(mContext, R.style.main_fragment_select_background_name_noraml_text_style);
                mBgParentList.get(i).setBackgroundColor(mContext.getResources().getColor(R.color.transparent));

            }
        }
    }

    @Override
    public void onClick(View v) {
        int index = 0;
        switch (v.getId()) {
            case R.id.select_bg_image1:
                index = 0;
                break;
            case R.id.select_bg_image2:
                index = 1;
                break;
            case R.id.select_bg_image3:
                index = 2;
                break;
            case R.id.select_bg_image4:
                index = 3;
                break;

        }
        mCuurentIndex = index;
        LightDBHelper.setMainBgIndex(mContext, mCuurentIndex);
        if (mCallback != null) {
            mCallback.onSelected(mCuurentIndex);
        }
        dismiss();

    }

    public void setSelectedCallback(selectedCallback callback) {
        mCallback = callback;
    }

    public interface selectedCallback {
        public void onSelected(int index);
    }
}
