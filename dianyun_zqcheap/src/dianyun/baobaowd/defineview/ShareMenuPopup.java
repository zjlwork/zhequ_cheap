package dianyun.baobaowd.defineview;

import android.content.Context;
import android.graphics.drawable.BitmapDrawable;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import dianyun.zqcheap.R;

public class ShareMenuPopup extends PopupWindow implements OnClickListener
{

	Context mContext = null;
	ShareMenuPopupClickCallback mClickListener = null;
	View mParent = null;
	LinearLayout ll_popup = null;
	View  mShareQQLay, mShareQQZoneLay, mShareSinaLay,
			mShareSinaImg, mShareWeixinLay, mShareWeixinSpaceLay,
			mCancelImg,mShareCopyLayout,mShareQQIv,mShareQQZoneIv,mShareWeixinIv,mShareWeixinSpaceIv,mShareCopyIv;

	public ShareMenuPopup(Context context, View parent)
	{
		super(context);
		mContext = context;
		mParent = parent;
		View view = LayoutInflater.from(mContext).inflate(
				R.layout.sharemenupopup, null);
		view.startAnimation(AnimationUtils.loadAnimation(mContext,
				R.anim.fade_ins));
		ll_popup = (LinearLayout) view.findViewById(R.id.ll_popup);
		ll_popup.startAnimation(AnimationUtils.loadAnimation(mContext,
				R.anim.push_bottom_in_2));
		setWidth(LayoutParams.FILL_PARENT);
		setHeight(LayoutParams.FILL_PARENT);
		setBackgroundDrawable(new BitmapDrawable());
		setOutsideTouchable(true);
		setContentView(view);
		update();
		initViews(view);
	}
	public void hideQQWx(){
		mShareQQLay.setVisibility(View.GONE);
		mShareWeixinLay.setVisibility(View.GONE);
		mShareCopyLayout.setVisibility(View.GONE);
	}

	private void initViews(View view)
	{

		// 新浪分享
		mShareSinaLay = view.findViewById(R.id.share_sinaweibo_lay);
		mShareSinaImg = view.findViewById(R.id.weibologin_iv);
		mShareSinaImg.setOnClickListener(this);
		// qq分享
		mShareQQLay = view.findViewById(R.id.share_qq_lay);
		mShareQQIv= view.findViewById(R.id.qqlogin_iv);
		mShareQQIv.setOnClickListener(this);
		// qqzone 分享
		mShareQQZoneLay = view.findViewById(R.id.share_qqzone_lay);
		mShareQQZoneIv= view.findViewById(R.id.share_qqzone_img);
		mShareQQZoneIv.setOnClickListener(this);
		// 微信分享
		mShareWeixinLay = view.findViewById(R.id.share_weixin_lay);
		mShareWeixinIv = view.findViewById(R.id.share_weixin_img);
		mShareWeixinIv.setOnClickListener(this);
		// 微信朋友圈 分享
		mShareWeixinSpaceLay = view.findViewById(R.id.share_weixinspace_lay);
		mShareWeixinSpaceIv= view.findViewById(R.id.share_weixin_friends);
		mShareWeixinSpaceIv.setOnClickListener(this);
		// 复制
		mShareCopyLayout = view.findViewById(R.id.share_copy_lay);
		mShareCopyIv = view.findViewById(R.id.share_copy_iv);
		mShareCopyIv.setOnClickListener(this);
		
		// 取消
		mCancelImg = view.findViewById(R.id.item_popupwindows_cancel);
		mCancelImg.setOnClickListener(this);
		
		
		mShareQQLay.setVisibility(View.GONE);
		mShareQQZoneLay.setVisibility(View.GONE);
		
	}

	public void setShareListener(ShareMenuPopupClickCallback callback)
	{
		mClickListener = callback;
	}

	public interface ShareMenuPopupClickCallback
	{

		public void handleShareOnClick(int i);

		public void handleFavorOnClick();

		public void handleSwitchPageOnClick();
	}

	public void show(boolean isSinaLogin,
			boolean isCanSwitchPage)
	{
		
		// sina login
		if (isSinaLogin)
		{
			mShareSinaLay.setVisibility(View.VISIBLE);
		}
		else
		{
			mShareSinaLay.setVisibility(View.GONE);
		}
		
		setFocusable(true);
		ll_popup.startAnimation(AnimationUtils.loadAnimation(mContext,
				R.anim.push_bottom_in_2));
		if (mParent != null)
			showAtLocation(mParent, Gravity.BOTTOM, 0, 0);
	}

	@Override
	public void onClick(View v)
	{
		switch (v.getId())
		{
		case R.id.item_popupwindows_cancel:
			// 取消
			dismiss();
			break;
		
		case R.id.qqlogin_iv:
			// qq
			handleOnClick(2);
			break;
		case R.id.share_qqzone_img:
			// qqzone
			handleOnClick(3);
			break;
		case R.id.weibologin_iv:
			// 微博
			handleOnClick(4);
			break;
		case R.id.share_weixin_img:
			// weixin
			handleOnClick(5);
			break;
		case R.id.share_weixin_friends:
			// weixin 朋友圈
			handleOnClick(6);
			break;
		case R.id.share_copy_iv:
			handleOnClick(7);
			break;
		}
	}

	private void handleOnClick(int i)
	{
		if (mClickListener != null)
		{
			if (i == 0)
			{
				mClickListener.handleFavorOnClick();
			}
			else if (i == 1)
			{
				mClickListener.handleSwitchPageOnClick();
			}
			else
			{
				mClickListener.handleShareOnClick(i);
			}
		}
		dismiss();
	}
}
