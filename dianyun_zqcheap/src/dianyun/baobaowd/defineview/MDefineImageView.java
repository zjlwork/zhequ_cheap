package dianyun.baobaowd.defineview;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Movie;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import dianyun.baobaowd.defineview.ximageview.IPhotoView;
import dianyun.baobaowd.defineview.ximageview.PhotoViewAttacher;
import dianyun.baobaowd.defineview.ximageview.PhotoViewAttacher.OnMatrixChangedListener;
import dianyun.baobaowd.defineview.ximageview.PhotoViewAttacher.OnPhotoTapListener;
import dianyun.baobaowd.defineview.ximageview.PhotoViewAttacher.OnViewTapListener;

/**
 * This is a View class that wraps Android {@link Movie} object and displays it.
 * You can set GIF as a Movie object or as a resource id from XML or by calling
 * {@link #setMovie(Movie)} or {@link #setMovieResource(int)}.
 * <p>
 * You can pause and resume GIF animation by calling {@link #setPaused(boolean)}.
 * <p>
 * The animation is drawn in the center inside of the measured view bounds.
 * 
 * @author Sergey Bakhtiarov
 */
public class MDefineImageView extends ImageView implements IPhotoView
{

	private static final int DEFAULT_MOVIEW_DURATION = 1000;
	private int mMovieResourceId;
	private Movie mMovie;
	private long mMovieStart;
	private int mCurrentAnimationTime = 0;
	/**
	 * Position for drawing animation frames in the center of the view.
	 */
	private float mLeft;
	private float mTop;
	/**
	 * Scaling factor to fit the animation within view bounds.
	 */
	private float mScale;
	/**
	 * Scaled movie frames width and height.
	 */
	private int mMeasuredMovieWidth;
	private int mMeasuredMovieHeight;
	private volatile boolean mPaused = false;
	private boolean mVisible = true;
	private PhotoViewAttacher mAttacher;
	private ScaleType mPendingScaleType;
	private ScaleType mScaleType;

	public MDefineImageView(Context context)
	{
		this(context, null);
		mAttacher = new PhotoViewAttacher(this);
	}

	public MDefineImageView(Context context, AttributeSet attrs)
	{
		this(context, attrs, 0);
		mAttacher = new PhotoViewAttacher(this);
	}

	public MDefineImageView(Context context, AttributeSet attrs, int defStyle)
	{
		super(context, attrs, defStyle);
		super.setScaleType(ScaleType.MATRIX);
		mAttacher = new PhotoViewAttacher(this);
		if (null != mPendingScaleType)
		{
			setScaleType(mPendingScaleType);
			mPendingScaleType = null;
		}
		// setViewAttributes(context, attrs, defStyle);
	}

	public void setOnSingleTap(OnViewTapListener clickListener)
	{
		mAttacher.setOnViewTapListener(clickListener);
	}

	// @SuppressLint("NewApi")
	// private void setViewAttributes(Context context, AttributeSet attrs,
	// int defStyle)
	// {
	// /**
	// * Starting from HONEYCOMB have to turn off HW acceleration to draw
	// * Movie on Canvas.
	// */
	// if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)
	// {
	// setLayerType(View.LAYER_TYPE_SOFTWARE, null);
	// }
	// final TypedArray array = context.obtainStyledAttributes(attrs,
	// R.styleable.GifMoviewView, defStyle,
	// R.style.Widget_GifMoviewView);
	// mMovieResourceId = array.getResourceId(R.styleable.GifMoviewView_gif,
	// -1);
	// mPaused = array.getBoolean(R.styleable.GifMoviewView_paused, false);
	// array.recycle();
	// if (mMovieResourceId != -1)
	// {
	// mMovie = Movie.decodeStream(getResources().openRawResource(
	// mMovieResourceId));
	// }
	// }
	public void setMovieResource(int movieResId)
	{
		this.mMovieResourceId = movieResId;
		mMovie = Movie.decodeStream(getResources().openRawResource(
				mMovieResourceId));
		requestLayout();
	}

	public void setMovie(Movie movie)
	{
		this.mMovie = movie;
		requestLayout();
	}

	public Movie getMovie()
	{
		return mMovie;
	}

	public void setMovieTime(int time)
	{
		mCurrentAnimationTime = time;
		invalidate();
	}

	public void setPaused(boolean paused)
	{
		this.mPaused = paused;
		/**
		 * Calculate new movie start time, so that it resumes from the same
		 * frame.
		 */
		if (!paused)
		{
			mMovieStart = android.os.SystemClock.uptimeMillis()
					- mCurrentAnimationTime;
		}
		invalidate();
	}

	public boolean isPaused()
	{
		return this.mPaused;
	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec)
	{
		if (mMovie != null)
		{
			int movieWidth = mMovie.width();
			int movieHeight = mMovie.height();
			/*
			 * Calculate horizontal scaling
			 */
			float scaleH = 1f;
			int measureModeWidth = MeasureSpec.getMode(widthMeasureSpec);
			if (measureModeWidth != MeasureSpec.UNSPECIFIED)
			{
				int maximumWidth = MeasureSpec.getSize(widthMeasureSpec);
				// if (movieWidth > maximumWidth)
				// {
				scaleH = (float) movieWidth / (float) maximumWidth;
				// }
			}
			/*
			 * calculate vertical scaling
			 */
			float scaleW = 1f;
			int measureModeHeight = MeasureSpec.getMode(heightMeasureSpec);
			if (measureModeHeight != MeasureSpec.UNSPECIFIED)
			{
				int maximumHeight = MeasureSpec.getSize(heightMeasureSpec);
				// if (movieHeight > maximumHeight)
				// {
				scaleW = (float) movieHeight / (float) maximumHeight;
				// }
			}
			/*
			 * calculate overall scale
			 */
			mScale = Math.max(scaleH, scaleW);
			mMeasuredMovieWidth = (int) (movieWidth / mScale);
			mMeasuredMovieHeight = (int) (movieHeight / mScale);
			setMeasuredDimension(mMeasuredMovieWidth, mMeasuredMovieHeight);
		}
		else
		{
			/*
			 * No movie set, just set minimum available size.
			 */
			setMeasuredDimension(measureWidth(widthMeasureSpec),
					measureHeight(heightMeasureSpec));
		}
	}

	/**
	 * Determines the width of this view
	 * 
	 * @param measureSpec
	 *            A measureSpec packed into an int
	 * @return The width of the view, honoring constraints from measureSpec
	 */
	private int measureWidth(int measureSpec)
	{
		int result = 0;
		int specMode = MeasureSpec.getMode(measureSpec);
		int specSize = MeasureSpec.getSize(measureSpec);
		if (specMode == MeasureSpec.EXACTLY)
		{
			// We were told how big to be
			result = specSize;
		}
		else
		{
			// Measure the text
			result = getPaddingLeft() + getPaddingRight();
			if (specMode == MeasureSpec.AT_MOST)
			{
				// Respect AT_MOST value if that was what is called for by
				// measureSpec
				result = Math.min(result, specSize);
			}
		}
		return result;
	}

	/**
	 * Determines the height of this view
	 * 
	 * @param measureSpec
	 *            A measureSpec packed into an int
	 * @return The height of the view, honoring constraints from measureSpec
	 */
	private int measureHeight(int measureSpec)
	{
		int result = 0;
		int specMode = MeasureSpec.getMode(measureSpec);
		int specSize = MeasureSpec.getSize(measureSpec);
		if (specMode == MeasureSpec.EXACTLY)
		{
			// We were told how big to be
			result = specSize;
		}
		else
		{
			// Measure the text (beware: ascent is a negative number)
			result = getPaddingTop() + getPaddingBottom();
			if (specMode == MeasureSpec.AT_MOST)
			{
				// Respect AT_MOST value if that was what is called for by
				// measureSpec
				result = Math.min(result, specSize);
			}
		}
		return result;
	}

	@Override
	protected void onLayout(boolean changed, int l, int t, int r, int b)
	{
		super.onLayout(changed, l, t, r, b);
		/*
		 * Calculate left / top for drawing in center
		 */
		mLeft = (getWidth() - mMeasuredMovieWidth) / 2f;
		mTop = (getHeight() - mMeasuredMovieHeight) / 2f;
		mVisible = getVisibility() == View.VISIBLE;
	}

	@Override
	protected void onDraw(Canvas canvas)
	{
		if (mMovie != null)
		{
			if (!mPaused)
			{
				updateAnimationTime();
				drawMovieFrame(canvas);
				invalidate();
			}
			else
			{
				drawMovieFrame(canvas);
			}
		}
		else
		{
			synchronized (canvas)
			{
				Drawable mDrawable = getDrawable();
				if (mDrawable != null)
				{
					int saveCount = canvas.getSaveCount();
					canvas.save();
					Matrix mDrawMatrix = mAttacher.getDisplayMatrix();
					if (mDrawMatrix != null)
					{
						canvas.concat(mDrawMatrix);
					}
					mDrawable.draw(canvas);
					canvas.restoreToCount(saveCount);
				}
			}
		}
	}

	/**
	 * Invalidates view only if it is visible. <br>
	 * {@link #postInvalidateOnAnimation()} is used for Jelly Bean and higher.
	 * 
	 */
	// @SuppressLint("NewApi")
	// private void invalidateView() {
	// if(mVisible) {
	// if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
	// postInvalidateOnAnimation();
	// } else {
	// invalidate();
	// }
	// }
	// }
	/**
	 * Calculate current animation time
	 */
	private void updateAnimationTime()
	{
		long now = android.os.SystemClock.uptimeMillis();
		if (mMovieStart == 0)
		{
			mMovieStart = now;
		}
		int dur = mMovie.duration();
		if (dur == 0)
		{
			dur = DEFAULT_MOVIEW_DURATION;
		}
		mCurrentAnimationTime = (int) ((now - mMovieStart) % dur);
	}

	/**
	 * Draw current GIF frame
	 */
	private void drawMovieFrame(Canvas canvas)
	{
		synchronized (canvas)
		{
			mMovie.setTime(mCurrentAnimationTime);
			int saveCnt = canvas.save(Canvas.MATRIX_SAVE_FLAG);
			// Log.d("photoview2",
			// "drawMovieFrame:" + mScale + "---" + mAttacher.getScale()
			// + "---left:" + mLeft + "---top:" + mTop);
			if (mScale == 0)
			{
				canvas.scale(1, 1);
			}
			else
			{
				canvas.scale(1 / mScale, 1 / mScale);
				// canvas.scale(1 * mAttacher.getScale() / mScale,
				// 1 * mAttacher.getScale() / mScale);
			}
			// canvas.translate(0, 0 + 200);
			mMovie.draw(canvas, 0, 0);
			// mMovie.draw(canvas, 0, 200);
			canvas.restore();
		}
	}

	private float[] applyScaleType(Canvas canvas)
	{
		// Get the current dimensions of the view and the gif
		float vWidth = getWidth();
		float vHeight = getHeight();
		float gWidth = mMovie.width() * mScale;
		float gHeight = mMovie.height() * mScale;
		// Disable the default scaling, it can mess things up
		if (mScaleType == null)
		{
			mScaleType = getScaleType();
			setScaleType(ScaleType.MATRIX);
		}
		float x = 0;
		float y = 0;
		float s = 1;
		switch (mScaleType)
		{
		case CENTER:
			/* Center the image in the view, but perform no scaling. */
			x = (vWidth - gWidth) / 2 / mScale;
			y = (vHeight - gHeight) / 2 / mScale;
			break;
		case CENTER_CROP:
			/*
			 * Scale the image uniformly (maintain the image's aspect ratio) so
			 * that both dimensions (width and height) of the image will be
			 * equal to or larger than the corresponding dimension of the view
			 * (minus padding). The image is then centered in the view.
			 */
			float minDimensionCenterCrop = Math.min(gWidth, gHeight);
			if (minDimensionCenterCrop == gWidth)
			{
				s = vWidth / gWidth;
			}
			else
			{
				s = vHeight / gHeight;
			}
			x = (vWidth - gWidth * s) / 2 / (s * mScale);
			y = (vHeight - gHeight * s) / 2 / (s * mScale);
			canvas.scale(s, s);
			break;
		case CENTER_INSIDE:
			/*
			 * Scale the image uniformly (maintain the image's aspect ratio) so
			 * that both dimensions (width and height) of the image will be
			 * equal to or less than the corresponding dimension of the view
			 * (minus padding). The image is then centered in the view.
			 */
			// Scaling only applies if the gif is larger than the container!
			if (gWidth > vWidth || gHeight > vHeight)
			{
				float maxDimensionCenterInside = Math.max(gWidth, gHeight);
				if (maxDimensionCenterInside == gWidth)
				{
					s = vWidth / gWidth;
				}
				else
				{
					s = vHeight / gHeight;
				}
			}
			x = (vWidth - gWidth * s) / 2 / (s * mScale);
			y = (vHeight - gHeight * s) / 2 / (s * mScale);
			canvas.scale(s, s);
			break;
		case FIT_CENTER:
			/*
			 * Compute a scale that will maintain the original src aspect ratio,
			 * but will also ensure that src fits entirely inside dst. At least
			 * one axis (X or Y) will fit exactly. The result is centered inside
			 * dst.
			 */
			// This scale type always scales the gif to the exact dimension of
			// the View
			float maxDimensionFitCenter = Math.max(gWidth, gHeight);
			if (maxDimensionFitCenter == gWidth)
			{
				s = vWidth / gWidth;
			}
			else
			{
				s = vHeight / gHeight;
			}
			x = (vWidth - gWidth * s) / 2 / (s * mScale);
			y = (vHeight - gHeight * s) / 2 / (s * mScale);
			canvas.scale(s, s);
			break;
		case FIT_START:
			/*
			 * Compute a scale that will maintain the original src aspect ratio,
			 * but will also ensure that src fits entirely inside dst. At least
			 * one axis (X or Y) will fit exactly. The result is centered inside
			 * dst.
			 */
			// This scale type always scales the gif to the exact dimension of
			// the View
			float maxDimensionFitStart = Math.max(gWidth, gHeight);
			if (maxDimensionFitStart == gWidth)
			{
				s = vWidth / gWidth;
			}
			else
			{
				s = vHeight / gHeight;
			}
			x = 0;
			y = 0;
			canvas.scale(s, s);
			break;
		case FIT_END:
			/*
			 * Compute a scale that will maintain the original src aspect ratio,
			 * but will also ensure that src fits entirely inside dst. At least
			 * one axis (X or Y) will fit exactly. END aligns the result to the
			 * right and bottom edges of dst.
			 */
			// This scale type always scales the gif to the exact dimension of
			// the View
			float maxDimensionFitEnd = Math.max(gWidth, gHeight);
			if (maxDimensionFitEnd == gWidth)
			{
				s = vWidth / gWidth;
			}
			else
			{
				s = vHeight / gHeight;
			}
			x = (vWidth - gWidth * s) / mScale / s;
			y = (vHeight - gHeight * s) / mScale / s;
			canvas.scale(s, s);
			break;
		case FIT_XY:
			/*
			 * Scale in X and Y independently, so that src matches dst exactly.
			 * This may change the aspect ratio of the src.
			 */
			float sFitX = vWidth / gWidth;
			s = vHeight / gHeight;
			x = 0;
			y = 0;
			canvas.scale(sFitX, s);
			break;
		default:
			break;
		}
		return new float[] { x, y, s };
	}

	// @SuppressLint("NewApi")
	// @Override
	// public void onScreenStateChanged(int screenState) {
	// // super.onScreenStateChanged(screenState);
	// // mVisible = screenState == SCREEN_STATE_ON;
	// // invalidateView();
	// }
	// @SuppressLint("NewApi")
	// @Override
	// protected void onVisibilityChanged(View changedView, int visibility)
	// {
	// super.onVisibilityChanged(changedView, visibility);
	// mVisible = visibility == View.VISIBLE;
	// invalidate();
	// }
	@Override
	protected void onWindowVisibilityChanged(int visibility)
	{
		super.onWindowVisibilityChanged(visibility);
		mVisible = visibility == View.VISIBLE;
		invalidate();
	}

	@Override
	public boolean canZoom()
	{
		return mAttacher.canZoom();
	}

	@Override
	public RectF getDisplayRect()
	{
		return mAttacher.getDisplayRect();
	}

	@Override
	public float getMinScale()
	{
		return mAttacher.getMinScale();
	}

	@Override
	public float getMidScale()
	{
		return mAttacher.getMidScale();
	}

	@Override
	public float getMaxScale()
	{
		return mAttacher.getMaxScale();
	}

	@Override
	public float getScale()
	{
		return mAttacher.getScale();
	}

	@Override
	public ScaleType getScaleType()
	{
		if (mAttacher == null)
			return ScaleType.FIT_XY;
		return mAttacher.getScaleType();
	}

	@Override
	public void setAllowParentInterceptOnEdge(boolean allow)
	{
		mAttacher.setAllowParentInterceptOnEdge(allow);
	}

	@Override
	public void setMinScale(float minScale)
	{
		mAttacher.setMinScale(minScale);
	}

	@Override
	public void setMidScale(float midScale)
	{
		mAttacher.setMidScale(midScale);
	}

	@Override
	public void setMaxScale(float maxScale)
	{
		mAttacher.setMaxScale(maxScale);
	}

	@Override
	// setImageBitmap calls through to this method
	public void setImageDrawable(Drawable drawable)
	{
		super.setImageDrawable(drawable);
		if (null != mAttacher)
		{
			mAttacher.update();
		}
	}

	@Override
	public void setImageResource(int resId)
	{
		super.setImageResource(resId);
		if (null != mAttacher)
		{
			mAttacher.update();
		}
	}

	@Override
	public void setImageURI(Uri uri)
	{
		super.setImageURI(uri);
		if (null != mAttacher)
		{
			mAttacher.update();
		}
	}

	@Override
	public void setOnMatrixChangeListener(OnMatrixChangedListener listener)
	{
		mAttacher.setOnMatrixChangeListener(listener);
	}

	@Override
	public void setOnLongClickListener(OnLongClickListener l)
	{
		mAttacher.setOnLongClickListener(l);
	}

	@Override
	public void setOnPhotoTapListener(OnPhotoTapListener listener)
	{
		mAttacher.setOnPhotoTapListener(listener);
	}

	@Override
	public void setOnViewTapListener(OnViewTapListener listener)
	{
		mAttacher.setOnViewTapListener(listener);
	}

	@Override
	public void setScaleType(ScaleType scaleType)
	{
		if (null != mAttacher)
		{
			mAttacher.setScaleType(scaleType);
		}
		else
		{
			mPendingScaleType = scaleType;
		}
	}

	@Override
	public void setZoomable(boolean zoomable)
	{
		mAttacher.setZoomable(zoomable);
	}

	@Override
	public void zoomTo(float scale, float focalX, float focalY)
	{
		mScale = scale;
		mAttacher.zoomTo(scale, focalX, focalY);
	}

	@Override
	protected void onDetachedFromWindow()
	{
		mAttacher.cleanup();
		super.onDetachedFromWindow();
	}
}
