package dianyun.baobaowd.defineview;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Paint;
import android.os.Handler;
import android.os.Handler.Callback;
import android.os.Message;
import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

import dianyun.baobaowd.data.User;
import dianyun.baobaowd.db.LightDBHelper;
import dianyun.baobaowd.db.ShopDBHelper;
import dianyun.baobaowd.defineview.ChildViewPager.OnSingleTouchListener;
import dianyun.baobaowd.entity.CateItem;
import dianyun.baobaowd.entity.Constants;
import dianyun.baobaowd.entity.LocalMenu;
import dianyun.baobaowd.help.ShopHttpHelper;
import dianyun.baobaowd.interfaces.DialogCallBack;
import dianyun.baobaowd.util.ConversionHelper;
import dianyun.baobaowd.util.GobalConstants;
import dianyun.baobaowd.util.TaeSdkUtil;
import dianyun.baobaowd.util.ToastHelper;
import dianyun.baobaowd.util.UserHelper;
import dianyun.zqcheap.R;
import dianyun.zqcheap.activity.GoodsListActivity;
import dianyun.zqcheap.activity.HtmlActivity;
import dianyun.zqcheap.activity.InviteFriendActivity;
import dianyun.zqcheap.activity.PrizesActivity;
import dianyun.zqcheap.activity.ShopSpecialTopicDetailActivity;
import dianyun.zqcheap.activity.ShopSpecialTopicListActivity;

public class HotPicView extends LinearLayout implements OnSingleTouchListener,
		OnPageChangeListener, Callback
{

	private List<CateItem> mDataList;
	private ChildViewPager mViewPager;
	private Context mContext;
	private LinearLayout mDotLayout;
	private HotViewPicPagerAdapter mAdapter;
	private Handler mHandler;
	private int mStopMills = 0;
	private final int MAXSTOPTIME = 5;
	private String TAG = "HotView";
	private View mCurrentActivityView;
	private boolean mIsHasSetUpData = false;

	public void setDataList(CateItem category, long menuID)
	{
//		mCategory = category;
//		mMenuID = menuID;
		// 显示小圆点
		// 初始化adapter
		initData();
	}

	public HotPicView(Context context, AttributeSet attrs, int defStyle)
	{
		super(context, attrs, defStyle);
		mContext = context;
		initView();
	}

	public HotPicView(Context context, AttributeSet attrs)
	{
		super(context, attrs);
		mContext = context;
		initView();
	}

	public HotPicView(Context context)
	{
		super(context);
		mContext = context;
		initView();
	}

	private void initView()
	{
		LayoutInflater.from(mContext).inflate(R.layout.shop_hotpic_lay, this,
				true);
		mDotLayout = (LinearLayout) findViewById(R.id.dotlayout);
		mViewPager = (ChildViewPager) findViewById(R.id.viewpager);
		mViewPager.setOnSingleTouchListener(this);
		mViewPager.setOnPageChangeListener(this);
		mHandler = new Handler(this);
	}

	public void stopPlay()
	{
		mHandler.removeMessages(0);
	}

	public void initData()
	{
		mDataList = ShopDBHelper.getFocusPicture(mContext);
		
		if(mDataList!=null&&mDataList.size()>0){
			if (!mIsHasSetUpData)
			{
				refreshData();
			}
		}
		System.out.println("mContext========="+mContext);
		ShopHttpHelper.getFocusPictureData(mContext, false,
				 new ShopHttpHelper.ShopDataCallback()
				{

					@Override
					public void getMenu(List<LocalMenu> result)
					{
						// TODO Auto-generated method stub
					}

					@Override
					public void getChildsData(List<CateItem> result)
					{
//						Log.d("success", "get hot data:"+result.size());
						mDataList = result;
						refreshData();
					}
				});
	}

	private void refreshData()
	{
		if (mDataList != null)
		{
			if (mAdapter == null)
			{
				mAdapter = new HotViewPicPagerAdapter(mDataList, mContext);
				mViewPager.setAdapter(mAdapter);
			}
			else
			{
				mAdapter.setDataSource(mDataList);
			}
			mIsHasSetUpData = true;
			changeDotLayout(mContext, 0);
			mHandler.sendEmptyMessage(0);
		}
	}

	private void changeDotLayout(Context context, int currtItem)
	{
		if (mDataList == null)
		{
			return;
		}
		int size = mDataList.size();
		mDotLayout.removeAllViewsInLayout();
		int left = ConversionHelper.dipToPx(3, context);
		int right = ConversionHelper.dipToPx(9, context);
		int layoutsize = ConversionHelper.dipToPx(12, context);
		for (int i = 0; i < size; i++)
		{
			DrawView lDrawView = null;
			if (i == currtItem)
			{
				lDrawView = new DrawView(context, left, left, right, right,
						context.getResources().getColor(
								R.color.boarddotselected));
				lDrawView.setStyle(Paint.Style.FILL);
				lDrawView.setSweep(360);
				LinearLayout.LayoutParams layoutParam = new LinearLayout.LayoutParams(
						layoutsize, layoutsize);
				layoutParam.gravity = Gravity.BOTTOM;
				lDrawView.setLayoutParams(layoutParam);
				mDotLayout.addView(lDrawView);
			}
			else
			{
				lDrawView = new DrawView(context, left, left, right, right,
						context.getResources().getColor(
								R.color.boarddotselected));
				lDrawView.setStyle(Paint.Style.STROKE);
				lDrawView.setSweep(360);
				LinearLayout.LayoutParams layoutParam = new LinearLayout.LayoutParams(
						layoutsize, layoutsize);
				layoutParam.gravity = Gravity.BOTTOM;
				lDrawView.setLayoutParams(layoutParam);
				mDotLayout.addView(lDrawView);
			}
		}
		mDotLayout.invalidate();
	}

	// @Override
	// protected void onLayout(boolean changed, int l, int t, int r, int b)
	// {
	//
	// }
	//
	// @Override
	// protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec)
	// {
	// super.onMeasure(widthMeasureSpec, heightMeasureSpec);
	// }
	@Override
	public void onSingleTouch()
	{
		if (mDataList != null && mDataList.size() > 0
				&& mDataList.get(0).clickType != 0)
		{
			CateItem item = mDataList.get(mViewPager.getCurrentItem());
			if (item != null && item.clickType != null)
			{
				goTargetActivity(item);
			}
		}
	}

	public void setCurrentActivityRootView(View activityView)
	{
		mCurrentActivityView = activityView;
	}

	private void goTargetActivity(final CateItem mCategory)
	{
		Intent itn = null;
		int xtype = mCategory.xType;
		int clickType = mCategory.clickType;
		if (mCategory == null || mCategory.clickType == null)
		{
			return;
		}
		// 点击类型，1：专辑列表，2：专辑详情，3：商品分类列表，4：webview
		switch (clickType)
		{
		case 1:
			itn = new Intent(mContext, ShopSpecialTopicListActivity.class);
			itn.putExtra(Constants.OBJECT_EXTRA_NAME,
					mCategory.cateId == null ? 0L : mCategory.cateId);
			itn.putExtra(Constants.EXTRA_NAME, mCategory.pageTitle == null ? ""
					: mCategory.pageTitle);
			break;
		case 2:
			itn = new Intent(mContext, ShopSpecialTopicDetailActivity.class);
			itn.putExtra(Constants.OBJECT_EXTRA_NAME,
					mCategory.cateId == null ? 0L : mCategory.cateId);
			itn.putExtra(Constants.EXTRA_NAME, mCategory.pageTitle == null ? ""
					: mCategory.pageTitle);
			break;
		case 3:
			itn = new Intent(mContext, GoodsListActivity.class);
			itn.putExtra(Constants.OBJECT_EXTRA_NAME,
					mCategory.cateId == null ? 0L : mCategory.cateId);
			itn.putExtra(Constants.EXTRA_NAME, mCategory.pageTitle == null ? ""
					: mCategory.pageTitle);
			break;
		case 4:
			itn = new Intent(mContext, HtmlActivity.class);
			itn.putExtra(GobalConstants.Data.URL,
					mCategory.clickUrl == null ? "" : mCategory.clickUrl);
			itn.putExtra(GobalConstants.Data.TITLE,
					mCategory.pageTitle == null ? "" : mCategory.pageTitle);
			itn.putExtra(GobalConstants.Data.SHAREIMAGEURL,
					mCategory.pic == null ? "": mCategory.pic);
			
			break;
		case 5:
			if (xtype == 3 && mCategory.tbItemId != null)
			{
				// 如果是单品的话
				if (!LightDBHelper.getIsNotTipLoginOrNotFees(mContext)
						&& UserHelper.getUser().getIsGuest() == GobalConstants.UserType.ISSELF
						&& mCurrentActivityView != null)
				{
//					ToastHelper.showTipLoginDialogWhenShop(mContext,
//							mCurrentActivityView,
//							new ToastHelper.dialogCancelCallback()
//							{
//
//								@Override
//								public void onCancle(boolean isNeverTip)
//								{
//									LightDBHelper.setIsNotTipLoginOrNotFees(
//											mContext, isNeverTip);
//									TaeSdkUtil.showTAEItemDetail(
//											(Activity) mContext,
//											mCategory.tbItemId,
//											mCategory.taobaoPid,
//											mCategory.isTk == 1,
//											mCategory.itemType, null,mCurrentActivityView);
//								}
//							});
					ToastHelper.showGoLoginDialog(mContext, new DialogCallBack() {
						
						@Override
						public void clickSure() {
							
						}
						
						@Override
						public void clickCancel() {
							TaeSdkUtil.showTAEItemDetail(
									(Activity) mContext,
									mCategory.tbItemId,
									mCategory.taobaoPid,
									mCategory.isTk == 1,
									mCategory.itemType, null,mCurrentActivityView);
						}
					});
				}
				else
				{
					TaeSdkUtil.showTAEItemDetail((Activity) mContext,
							mCategory.tbItemId, mCategory.taobaoPid,
							mCategory.isTk == 1, mCategory.itemType, null,mCurrentActivityView);
				}
			}
			break;
		case 6:
			// 金币商城
			itn = new Intent(mContext, PrizesActivity.class);
			break;
		case 7:
			if (UserHelper.isGusetUser(mContext)) {
				UserHelper.gusestUserGo(mContext);
			} else {
				// 金币商城
				itn = new Intent(mContext, InviteFriendActivity.class);
			}
			break;
		default:
			break;
		}
		if (itn != null)
		{
			mContext.startActivity(itn);
		}
	}

	private class HotViewPicPagerAdapter extends PagerAdapter
	{

		private List<CateItem> mList;
		private List<View> mImageList = new ArrayList<View>();
		private Context mContext;
		private int randomCount;
		private User mUser;
		private DisplayImageOptions mOptions;

		public HotViewPicPagerAdapter(List<CateItem> pList, Context pContext)
		{
			this.mContext = pContext;
			this.mList = pList;
			mUser = UserHelper.getUser();
			mOptions = new DisplayImageOptions.Builder()
					.showImageOnLoading(R.drawable.foucspicture_default)
					.showImageForEmptyUri(R.drawable.foucspicture_default)
					.showImageOnFail(R.drawable.foucspicture_default)
					// 设置图片在下载前是否重置，复位
					.cacheInMemory(true).cacheOnDisk(true)
					.considerExifParams(true)
					.imageScaleType(ImageScaleType.IN_SAMPLE_POWER_OF_2)
					.bitmapConfig(Bitmap.Config.RGB_565).build();
			initImageList();
		}

		public void setDataSource(List<CateItem> pList)
		{
			mList = pList;
			initImageList();
			notifyDataSetChanged();
		}

		@Override
		public int getCount()
		{
			return mList.size();
		}

		private void initImageList()
		{
			if (mList != null && mList.size() > 0)
			{
				mImageList.clear();
				for (int i = 0; i < mList.size(); i++)
				{
					View infalte = LayoutInflater.from(mContext).inflate(
							R.layout.hotpicpageradapter, null);
					final ImageView iv = (ImageView) infalte
							.findViewById(R.id.iv);
					final CateItem lViewPicture = mList.get(i);
					if (lViewPicture == null)
					{
						continue;
					}
					String url = "";
					if (lViewPicture.xType != 3)
					{
						url = lViewPicture.pic == null ? "" : lViewPicture.pic;
					}
					else
					{
						url = (lViewPicture.pics == null ? ""
								: lViewPicture.pics.get(0));
					}
					ImageLoader.getInstance().displayImage(url, iv,mOptions,
							new ImageLoadingListener()
							{

								@Override
								public void onLoadingStarted(String arg0,
										View arg1)
								{
									// TODO Auto-generated method stub
								}

								@Override
								public void onLoadingFailed(String arg0,
										View arg1, FailReason arg2)
								{
									Log.w(TAG, "onLoadingFailed:" + arg0);
								}

								@Override
								public void onLoadingComplete(String arg0,
										View arg1, Bitmap bitmap)
								{
									Log.w(TAG, "onLoadingComplete:" + arg0);
									iv.setImageBitmap(bitmap);
								}

								@Override
								public void onLoadingCancelled(String arg0,
										View arg1)
								{
									Log.w(TAG, "onLoadingCancelled:" + arg0);
								}
							});
					if (mImageList == null)
					{
						mImageList = new ArrayList<View>();
					}
					mImageList.add(infalte);
				}
			}
		}

		public View initItem(int position)
		{
			View infalte = LayoutInflater.from(mContext).inflate(
					R.layout.hotpicpageradapter, null);
			ImageView iv = (ImageView) infalte.findViewById(R.id.iv);
			final CateItem lViewPicture = mList.get(position);
			if (lViewPicture == null)
			{
				return infalte;
			}
			String url = "";
			if (lViewPicture.xType != 3)
			{
				url = lViewPicture.pic == null ? "" : lViewPicture.pic;
			}
			else
			{
				url = (lViewPicture.pics == null ? "" : lViewPicture.pics
						.get(0));
			}
			ImageLoader.getInstance().displayImage(url, iv);
			return infalte;
		}

		@Override
		public Object instantiateItem(View collection, final int position)
		{
			Log.i(TAG, "instantiateItem position start:" + position);
			View lView = mImageList.get(position);// initItem(position);
			((ViewPager) collection).addView(lView, 0);
			Log.i(TAG, "instantiateItem position end:" + position);
			return lView;
		}

		@Override
		public void destroyItem(View collection, int position, Object view)
		{
			((ViewPager) collection).removeView((View) view);
			Log.i(TAG, "destory position:" + position);
		}

		@Override
		public boolean isViewFromObject(View view, Object object)
		{
			return view == (object);
		}

		@Override
		public void finishUpdate(View arg0)
		{
		}

		@Override
		public void restoreState(Parcelable arg0, ClassLoader arg1)
		{
		}

		@Override
		public Parcelable saveState()
		{
			return null;
		}

		@Override
		public void startUpdate(View arg0)
		{
		}

		@Override
		public int getItemPosition(Object object)
		{
			return POSITION_NONE;
		}
	}

	@Override
	public void onPageScrollStateChanged(int arg0)
	{
		// TODO Auto-generated method stub
	}

	@Override
	public void onPageScrolled(int arg0, float arg1, int arg2)
	{
		// TODO Auto-generated method stub
	}

	@Override
	public void onPageSelected(int index)
	{
		mStopMills = 0;
		mHandler.removeMessages(0);
		mHandler.sendEmptyMessage(0);
		changeDotLayout(mContext, index);
	}

	@Override
	public boolean handleMessage(Message msg)
	{
		switch (msg.what)
		{
		case 0:
			Log.d(TAG,
					"count:" + mStopMills + "---" + mViewPager.getCurrentItem());
			mStopMills++;
			if (mStopMills == MAXSTOPTIME)
			{
				mHandler.removeMessages(0);
				int pageIndex = mViewPager.getCurrentItem();
				if(mDataList!=null){
					if (pageIndex == mDataList.size() - 1)
					{
						pageIndex = 0;
					}
					else
					{
						pageIndex++;
					}
				}
				mViewPager.setCurrentItem(pageIndex);
			}
			else
			{
				mHandler.sendEmptyMessageDelayed(0, 1000);
			}
			break;
		}
		return false;
	}
}
