
package dianyun.baobaowd.defineview;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.View;
import android.view.WindowManager;
import dianyun.baobaowd.util.ConversionHelper;
import dianyun.zqcheap.activity.ClipPictureActivity;

public class ClipView extends View
{

	private Context mContext;
	private int needHeight;

	public ClipView(Context context)
	{
		super(context);
		mContext = context;
	}

	public ClipView(Context context, AttributeSet attrs)
	{
		super(context, attrs);
		mContext = context;
		needHeight = ConversionHelper.dipToPx(200, mContext);
	}

	public ClipView(Context context, AttributeSet attrs, int defStyle)
	{
		super(context, attrs, defStyle);
		mContext = context;
		needHeight = ConversionHelper.dipToPx(200, mContext);
	}

	@Override
	protected void onDraw(Canvas canvas)
	{
		super.onDraw(canvas);
		int width = this.getWidth();
		int height = this.getHeight();
		/*
		 * Paint paint = new Paint(); paint.setColor(0xaa000000);
		 * 
		 * //top canvas.drawRect(0, 0, width, height/3, paint); //left
		 * canvas.drawRect(0, height/3, (width - height/3)/2, height*2/3,
		 * paint); //right canvas.drawRect((width + height/3)/2, height/3, width
		 * , height*2/3, paint); //bottom canvas.drawRect(0, height*2/3, width,
		 * height, paint);
		 */
		// DisplayMetrics dm = new DisplayMetrics();
		// ((ClipPictureActivity)mContext).getWindowManager().getDefaultDisplay().getMetrics(dm);
		// int screenWidth = dm.widthPixels;
		// int screenHeight = dm.heightPixels;
		Paint paint2 = new Paint();
		paint2.setColor(Color.WHITE);
		paint2.setStrokeWidth(3);
		WindowManager wm = (WindowManager) mContext
				.getSystemService(Context.WINDOW_SERVICE);
		int screenWidth = wm.getDefaultDisplay().getWidth();// 屏幕宽度
		int screenHeight = wm.getDefaultDisplay().getHeight();// 屏幕高度
		// top
		int startX = ConversionHelper.dipToPx(0, mContext);
		int stopX = screenWidth - ConversionHelper.dipToPx(0, mContext);
		int startY = (screenHeight - getBarHeight() - needHeight) / 2;
		int stopY = startY;
		canvas.drawLine(startX, startY, stopX, stopY, paint2);
		// left
		int startX2 = ConversionHelper.dipToPx(0, mContext);
		int stopX2 = ConversionHelper.dipToPx(0, mContext);
		int startY2 = (screenHeight - getBarHeight() - needHeight) / 2;
		int stopY2 = startY2 + needHeight;
		canvas.drawLine(startX2, startY2, stopX2, stopY2, paint2);
		// right
		int startX3 = screenWidth - ConversionHelper.dipToPx(0, mContext);
		int stopX3 = screenWidth - ConversionHelper.dipToPx(0, mContext);
		int startY3 = (screenHeight - getBarHeight() - needHeight) / 2;
		int stopY3 = startY3 + needHeight;
		canvas.drawLine(startX3, startY3, stopX3, stopY3, paint2);
		// bottom
		int startX4 = ConversionHelper.dipToPx(0, mContext);
		int stopX4 = screenWidth - ConversionHelper.dipToPx(0, mContext);
		int startY4 = (screenHeight - getBarHeight() - needHeight) / 2
				+ needHeight;
		int stopY4 = startY4;
		canvas.drawLine(startX4, startY4, stopX4, stopY4, paint2);
	}

	private int getBarHeight()
	{
		// 获取状态栏高度
		Rect frame = new Rect();
		((ClipPictureActivity) mContext).getWindow().getDecorView()
				.getWindowVisibleDisplayFrame(frame);
		int statusBarHeight = frame.top;
		//
		// int contenttop = ((ClipPictureActivity)mContext).getWindow()
		// .findViewById(Window.ID_ANDROID_CONTENT).getTop();
		// statusBarHeight是上面所求的状态栏的高度
		// titleBarHeight = contenttop - statusBarHeight;
		return statusBarHeight;
	}
}
