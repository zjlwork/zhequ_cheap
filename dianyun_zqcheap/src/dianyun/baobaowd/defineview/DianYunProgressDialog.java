package dianyun.baobaowd.defineview;

import android.app.Dialog;
import android.content.Context;
import android.view.Window;
import android.widget.TextView;
import dianyun.zqcheap.R;

;
public class DianYunProgressDialog extends Dialog
{

	private TextView mLoadingTipView;
	private Context mContext;

	public DianYunProgressDialog(Context context)
	{
		super(context);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		mContext = context;
		initView();
	}

	public DianYunProgressDialog(Context context, int theme)
	{
		super(context, theme);
		mContext = context;
		initView();
	}

	private void initView()
	{
		setCanceledOnTouchOutside(false);
		setContentView(R.layout.dianyun_progressdialog_lay);
		mLoadingTipView = (TextView) findViewById(R.id.progress_tip);
	}

	@Override
	public void dismiss()
	{
		// if (mContext != null)
		// {
		// ((Activity) mContext).finish();
		// }
		super.dismiss();
	}
}
