package dianyun.baobaowd.defineview;

import android.content.Context;
import android.util.AttributeSet;
import android.view.ViewGroup;

public class MessageCenterPullRefreshView extends ViewGroup
{

	public MessageCenterPullRefreshView(Context context, AttributeSet attrs)
	{
		super(context, attrs);
		// TODO Auto-generated constructor stub
	}/*

	private Scroller mScroller;
	private int mScrollY = 0;
	private int mCurrentScreen = 0;
	private Context mContext;
	private float mLastMotionX;
	private float mLastMotionY;
	float downpos = 0;
	float dist = 0;
	private RotateAnimation mFlipAnimation;
	private RotateAnimation mReverseFlipAnimation;
	int newY;
	private int pullHeight;
	private ImageView mPdRefreshImageView;
	private ProgressBar mPdProgressBar;
	private RelativeLayout mPdRefreshLayout;
	private RelativeLayout mPdRefreshErrorLayout;
	private int mPdRefreshLayoutHeight;
	private TextView mPdNotifyTv;
	private TextView mPdNotifyTimeTv;
	private TextView mPdErrorTv;
	private ListView mListView;
	boolean isUpStart = false;
	boolean isDownStart = false;
	boolean isLoading = false;
	private User mUser;
	// private int mTouchSlop = 0;
	// pullup
	private ImageView mPuRefreshImageView;
	private ProgressBar mPuProgressBar;
	private RelativeLayout mPuRefreshLayout;
	private RelativeLayout mPuRefreshErrorLayout;
	private TextView mPuNotifyTv;
	private TextView mPuNotifyTimeTv;
	private TextView mPuErrorTv;
	boolean isPullUpUpStart = false;
	boolean isPullUpDownStart = false;
	private int pullType;

	public MessageCenterPullRefreshView(Context context, AttributeSet attrs)
	{
		super(context, attrs);
		this.mContext = context;
		mScroller = new Scroller(context);
		// mTouchSlop =
		// ViewConfiguration.get(getContext()).getScaledTouchSlop();
		mFlipAnimation = new RotateAnimation(0, -180,
				Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF,
				0.5f);
		// mFlipAnimation.setInterpolator(new LinearInterpolator());
		mFlipAnimation.setDuration(200);
		mFlipAnimation.setFillAfter(true);
		mReverseFlipAnimation = new RotateAnimation(-180, 0,
				Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF,
				0.5f);
		// mReverseFlipAnimation.setInterpolator(new LinearInterpolator());
		mReverseFlipAnimation.setDuration(200);
		mReverseFlipAnimation.setFillAfter(true);
		mCurrentScreen = 1;
		this.setLayoutParams(new ViewGroup.LayoutParams(
				ViewGroup.LayoutParams.WRAP_CONTENT,
				ViewGroup.LayoutParams.FILL_PARENT));
		mUser = ((BaoBaoWDApplication) mContext.getApplicationContext())
				.getUser();
		// System.out.println("ReplyMePullRefreshView=="+getHeight());
	}

	@Override
	public boolean onInterceptTouchEvent(MotionEvent ev)
	{
		// System.out.println("PullDownRefreshView     onInterceptTouchEvent............");
		int deltaY = 0;
		initialize();
		final int action = ev.getAction();
		final float x = ev.getX();
		final float y = ev.getY();
		switch (action)
		{
		case MotionEvent.ACTION_MOVE:
			break;
		case MotionEvent.ACTION_DOWN:
			mLastMotionX = x;
			mLastMotionY = y;
			downpos = y;
			break;
		case MotionEvent.ACTION_CANCEL:
		case MotionEvent.ACTION_UP:
			break;
		}
		boolean temp = false;
		// listview 显示为实际第一项 且第一项 getTop()==0
		if (mListView != null && mListView.getChildAt(0) != null
				&& mListView.getChildAt(0).getTop() == 0
				&& mListView.getFirstVisiblePosition() == 0
				&& mLastMotionY - y < 0 && Math.abs(mLastMotionY - y) > 10)
		{
			temp = true;
			// listview 显示为实际最后一项 且最后一项 getBottom()<=mListView.getBottom()
			// 且list已加载完数据库中数据
		}
		else if (mListView != null
				&& mListView.getChildAt(0) != null
				&& mListView.getChildAt(mListView.getChildCount() - 1)
						.getBottom() <= mListView.getBottom()
				&& mListView.getLastVisiblePosition() == mListView.getAdapter()
						.getCount() - 1 && mLastMotionY - y > 0
				&& Math.abs(mLastMotionY - y) > 10)
		{
			temp = true;
		}
		// false 返回子类事件处理 子类没有事件则会调用自身onTouchEvent true 调用自身onTouchEvent
		return temp;
	}

	@Override
	public boolean onTouchEvent(MotionEvent event)
	{
		// System.out.println("PullDownRefreshView     onTouchEvent...........");
		if (!isLoading)
		{
			final int action = event.getAction();
			final float x = event.getX();
			final float y = event.getY();
			switch (action)
			{
			case MotionEvent.ACTION_DOWN:
				mLastMotionX = x;
				mLastMotionY = y;
				downpos = y;
				break;
			case MotionEvent.ACTION_MOVE:
				final int deltaX = (int) (mLastMotionX - x);
				final int deltaY = (int) (mLastMotionY - y);
				mLastMotionX = x;
				mLastMotionY = y;
				dist = downpos - y;
				// 向下
				if (getScrollY() < getHeight())
				{
					if (getScrollY() + deltaY > getHeight())
					{
						scrollBy(0, getHeight() - getScrollY());
					}
					else
					{
						if (getScrollY() + deltaY > getHeight()
								- ConversionHelper.dipToPx(120, mContext))
						{
							scrollBy(0, deltaY);
						}
						else
						{
							scrollBy(
									0,
									getHeight()
											- ConversionHelper.dipToPx(120,
													mContext) - getScrollY());
						}
					}
					// pullHeight 70dp
					if (getScrollY() >= getHeight() - pullHeight)
					{
						if (!isUpStart)
						{
							isUpStart = true;
							mPdRefreshImageView.clearAnimation();
							mPdRefreshImageView
									.startAnimation(mReverseFlipAnimation);
							isDownStart = false;
						}
					}
					else
					{
						if (!isDownStart)
						{
							isDownStart = true;
							mPdRefreshImageView.clearAnimation();
							mPdRefreshImageView.startAnimation(mFlipAnimation);
							isUpStart = false;
						}
					}
				}
				else
				{
					// 向上
					if (getScrollY() + deltaY < getHeight()
							+ ConversionHelper.dipToPx(120, mContext))
					{
						scrollBy(0, deltaY);
					}
					else
					{
						scrollBy(
								0,
								getHeight()
										+ ConversionHelper.dipToPx(120,
												mContext) - getScrollY());
					}
					// pullHeight 70dp
					if (getScrollY() < getHeight() + pullHeight)
					{
						if (!isPullUpDownStart)
						{
							isPullUpDownStart = true;
							mPuRefreshImageView.clearAnimation();
							mPuRefreshImageView.startAnimation(mFlipAnimation);
							isPullUpUpStart = false;
						}
					}
					else
					{
						if (!isPullUpUpStart)
						{
							isPullUpUpStart = true;
							mPuRefreshImageView.clearAnimation();
							mPuRefreshImageView
									.startAnimation(mReverseFlipAnimation);
							isPullUpDownStart = false;
						}
					}
				}
				break;
			case MotionEvent.ACTION_UP:
				dist = downpos - y;
				if (dist < 0)
				{
					if (getScrollY() <= getHeight() - pullHeight)
					{
						int delty = (newY - mPdRefreshLayoutHeight)
								- getScrollY();
						mScroller.startScroll(0, mScrollY, 0, delty, 500);
						mPdRefreshImageView.clearAnimation();
						mPdRefreshImageView.setVisibility(View.GONE);
						mPdProgressBar.setVisibility(View.VISIBLE);
						mPdNotifyTv.setText(mContext
								.getString(R.string.load_more_topic));
						LightDBHelper
								.setLastRefreshTime(
										DateHelper.getRefreshTime(new Date()),
										mContext);
						isLoading = true;
						if (NetworkStatus.getNetWorkStatus(mContext) < 1)
						{
							mPdRefreshLayout.setVisibility(View.GONE);
							mPdRefreshErrorLayout.setVisibility(View.VISIBLE);
							mPdErrorTv.setText("!"
									+ mContext.getResources().getString(
											R.string.no_network));
							new GoneThread(GobalConstants.PullRefreshType.DOWN)
									.start();
						}
						else
						{
							mPdRefreshLayout.setVisibility(View.VISIBLE);
							mPdRefreshErrorLayout.setVisibility(View.GONE);
							int type = ((MessageCenterActivity) mContext)
									.getType();
							if (type == GobalConstants.MessageCenterType.REPLY)
								new GetMyReceiveReplyThread(type, 0, 0, 0, 0,
										false).start();
							else if (type == GobalConstants.MessageCenterType.APPRECIATION)
								new GetMyReceiveThanksThread(type, 0, 0, 0, 0,
										false).start();
							else if (type == GobalConstants.MessageCenterType.BEST)
								new GetMyBestAnswersThread(type, 0, 0, 0, 0,
										false).start();
							else if (type == GobalConstants.MessageCenterType.SYSTEM)
								new GetSystemMsgThread(type, 0, 0, 0, 0, false)
										.start();
							else if (type == GobalConstants.MessageCenterType.POST)
								new GetMyReceiveTopicReplysThread(type, 0, 0,
										0, 0, false).start();
						}
						invalidate();
					}
					else
					{
						mScroller.startScroll(0, newY, 0, 0, 10);
						invalidate();
					}
				}
				else
				{
					if (getScrollY() >= getHeight() + pullHeight)
					{
						int delty = (newY + mPdRefreshLayoutHeight)
								- getScrollY();
						mScroller.startScroll(0, mScrollY, 0, delty, 500);
						mPuRefreshImageView.clearAnimation();
						mPuRefreshImageView.setVisibility(View.GONE);
						mPuProgressBar.setVisibility(View.VISIBLE);
						mPuNotifyTv.setText(mContext
								.getString(R.string.load_more_topic));
						LightDBHelper
								.setLastRefreshTime(
										DateHelper.getRefreshTime(new Date()),
										mContext);
						isLoading = true;
						if (NetworkStatus.getNetWorkStatus(mContext) < 1)
						{
							mPuRefreshLayout.setVisibility(View.GONE);
							mPuRefreshErrorLayout.setVisibility(View.VISIBLE);
							mPuErrorTv.setText("!"
									+ mContext.getResources().getString(
											R.string.no_network));
							new GoneThread(GobalConstants.PullRefreshType.UP)
									.start();
						}
						else
						{
							mPuRefreshLayout.setVisibility(View.VISIBLE);
							mPuRefreshErrorLayout.setVisibility(View.GONE);
							int type = ((MessageCenterActivity) mContext)
									.getType();
							if (type == GobalConstants.MessageCenterType.REPLY)
							{
								long maxSeqId = ((MessageCenterActivity) mContext)
										.getReplyMaxSeqId();
								new GetMyReceiveReplyThread(type, 0, 0, 0,
										maxSeqId, true).start();
							}
							else if (type == GobalConstants.MessageCenterType.APPRECIATION)
							{
								long maxSeqId = ((MessageCenterActivity) mContext)
										.getAppreciationMaxSeqId();
								new GetMyReceiveThanksThread(type, 0, 0, 0,
										maxSeqId, true).start();
							}
							else if (type == GobalConstants.MessageCenterType.BEST)
							{
								long maxSeqId = ((MessageCenterActivity) mContext)
										.getBestMaxSeqId();
								new GetMyBestAnswersThread(type, 0, 0, 0,
										maxSeqId, true).start();
							}
							else if (type == GobalConstants.MessageCenterType.SYSTEM)
							{
								long maxSeqId = ((MessageCenterActivity) mContext)
										.getSystemMsgMaxSeqId();
								new GetSystemMsgThread(type, 0, 0, 0, maxSeqId,
										true).start();
							}
							else if (type == GobalConstants.MessageCenterType.POST)
							{
								long maxSeqId = ((MessageCenterActivity) mContext)
										.getTopicReplyMaxSeqId();
								new GetMyReceiveTopicReplysThread(type, 0, 0,
										0, maxSeqId, true).start();
							}
						}
						invalidate();
					}
					else
					{
						mScroller.startScroll(0, newY, 0, 0, 10);
						invalidate();
					}
				}
				break;
			case MotionEvent.ACTION_CANCEL:
			}
			mScrollY = this.getScrollY();
		}
		return true;
	}

	public void setToScreen(int whichScreen)
	{
		mCurrentScreen = whichScreen;
		final int newX = whichScreen * getWidth();
		mScroller.startScroll(newX, 0, 0, 0, 10);
		invalidate();
	}

	@Override
	protected void onLayout(boolean changed, int l, int t, int r, int b)
	{
		int childTop = 0;
		final int count = getChildCount();
		for (int i = 0; i < count; i++)
		{
			final View child = getChildAt(i);
			if (child.getVisibility() != View.GONE)
			{
				final int childHeight = child.getMeasuredHeight();
				child.layout(0, childTop, child.getMeasuredWidth(), childTop
						+ childHeight);
				childTop += childHeight;
			}
		}
		// System.out.println("onLayout=="+getHeight());
	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec)
	{
		super.onMeasure(widthMeasureSpec, heightMeasureSpec);
		final int width = MeasureSpec.getSize(widthMeasureSpec);
		final int height = MeasureSpec.getSize(heightMeasureSpec);
		final int widthMode = MeasureSpec.getMode(widthMeasureSpec);
		if (widthMode != MeasureSpec.EXACTLY)
		{
			throw new IllegalStateException("error mode.");
		}
		final int heightMode = MeasureSpec.getMode(heightMeasureSpec);
		if (heightMode != MeasureSpec.EXACTLY)
		{
			throw new IllegalStateException("error mode.");
		}
		// The children are given the same width and height as the workspace
		final int count = getChildCount();
		for (int i = 0; i < count; i++)
		{
			getChildAt(i).measure(widthMeasureSpec, heightMeasureSpec);
		}
		scrollTo(0, mCurrentScreen * height);
		postInvalidate();
		// System.out.println("onMeasure=="+getHeight());
	}

	@Override
	public void computeScroll()
	{
		if (mScroller.computeScrollOffset())
		{
			mScrollY = mScroller.getCurrY();
			scrollTo(0, mScrollY);
			postInvalidate();
		}
	}

	public int getCurScreen()
	{
		return mCurrentScreen;
	}

	public boolean isLoading()
	{
		return isLoading;
	}

	public void setLoading(boolean isLoading)
	{
		this.isLoading = isLoading;
	}

	public void setType(int pullType)
	{
		this.pullType = pullType;
	}

	public void initialize()
	{
		// pull down
		if (mPdRefreshLayout == null)
		{
			System.out.println("initialize    等于null=" + mPdRefreshLayout);
			mPdRefreshLayout = (RelativeLayout) findViewById(R.id.uppart);
		}
		mPdRefreshErrorLayout = (RelativeLayout) findViewById(R.id.upparterror);
		mPdErrorTv = (TextView) findViewById(R.id.uperror_tv);
		pullHeight = ConversionHelper.dipToPx(70, mContext);
		mPdProgressBar = (ProgressBar) findViewById(R.id.uprefresh_pb);
		mPdRefreshImageView = (ImageView) findViewById(R.id.uprefresh_iv);
		mPdNotifyTv = (TextView) findViewById(R.id.upnotify_tv);
		mPdNotifyTimeTv = (TextView) findViewById(R.id.upnotifytime_tv);
		mListView = (ListView) findViewById(R.id.listview);
		newY = getHeight();
		mPdRefreshLayoutHeight = mPdRefreshLayout.getLayoutParams().height;
		// pull up
		mPuRefreshErrorLayout = (RelativeLayout) findViewById(R.id.bottomerror);
		mPuErrorTv = (TextView) findViewById(R.id.bottomerror_tv);
		mPuRefreshLayout = (RelativeLayout) findViewById(R.id.bottom);
		mPuProgressBar = (ProgressBar) findViewById(R.id.bottomrefresh_pb);
		mPuRefreshImageView = (ImageView) findViewById(R.id.bottomrefresh_iv);
		mPuNotifyTv = (TextView) findViewById(R.id.bottomnotify_tv);
		mPuNotifyTimeTv = (TextView) findViewById(R.id.bottomnotifytime_tv);
		if (!isLoading)
		{
			// pull down
			mPdRefreshLayout.setVisibility(View.VISIBLE);
			mPdRefreshErrorLayout.setVisibility(View.GONE);
			mPdRefreshImageView.setVisibility(View.VISIBLE);
			mPdProgressBar.setVisibility(View.GONE);
			mPdNotifyTv.setText(mContext.getString(R.string.pull_down_refresh));
			mPdNotifyTimeTv.setText(mContext.getString(R.string.last_update)
					+ LightDBHelper.getLastRefreshTime(mContext));
			// pull up
			mPuRefreshLayout.setVisibility(View.VISIBLE);
			mPuRefreshErrorLayout.setVisibility(View.GONE);
			mPuRefreshImageView.setVisibility(View.VISIBLE);
			mPuProgressBar.setVisibility(View.GONE);
			mPuNotifyTv.setText(mContext.getString(R.string.next_10_room));
			mPuNotifyTimeTv.setText(mContext.getString(R.string.last_load)
					+ LightDBHelper.getLastRefreshTime(mContext));
		}
	}

	public void refreshGetNew()
	{
		initialize();
		// int newY =
		// ToastHelper.getScreenHeight(mContext)-ToastHelper.getBarHeight(mContext)-ConversionHelper.dipToPx(46,
		// mContext);
		mScroller.startScroll(0, newY, 0, -mPdRefreshLayoutHeight, 200);
		mPdRefreshImageView.clearAnimation();
		mPdRefreshImageView.setVisibility(View.GONE);
		mPdProgressBar.setVisibility(View.VISIBLE);
		mPdNotifyTv.setText(mContext.getString(R.string.load_more_topic));
		LightDBHelper.setLastRefreshTime(DateHelper.getRefreshTime(new Date()),
				mContext);
		isLoading = true;
		if (NetworkStatus.getNetWorkStatus(mContext) < 1)
		{
			mPdRefreshLayout.setVisibility(View.GONE);
			mPdRefreshErrorLayout.setVisibility(View.VISIBLE);
			mPdErrorTv.setText("!"
					+ mContext.getResources().getString(R.string.no_network));
			new GoneThread(GobalConstants.PullRefreshType.DOWN).start();
		}
		else
		{
			mPdRefreshLayout.setVisibility(View.VISIBLE);
			mPdRefreshErrorLayout.setVisibility(View.GONE);
			int type = ((MessageCenterActivity) mContext).getType();
			if (type == GobalConstants.MessageCenterType.REPLY)
				new GetMyReceiveReplyThread(type, 0, 0, 0, 0, false).start();
			else if (type == GobalConstants.MessageCenterType.APPRECIATION)
				new GetMyReceiveThanksThread(type, 0, 0, 0, 0, false).start();
			else if (type == GobalConstants.MessageCenterType.BEST)
				new GetMyBestAnswersThread(type, 0, 0, 0, 0, false).start();
			else if (type == GobalConstants.MessageCenterType.SYSTEM)
				new GetSystemMsgThread(type, 0, 0, 0, 0, false).start();
			else if (type == GobalConstants.MessageCenterType.POST)
				new GetMyReceiveTopicReplysThread(type, 0, 0, 0, 0, false)
						.start();
		}
		invalidate();
	}

	class GetSystemMsgThread extends Thread
	{

		private Handler handler;
		private ResultDTO resultDTO;
		private int pagesize;
		private int curPage;
		private long minSeqId;
		private long maxSeqId;
		private List<Message> msgList;
		private int type;
		private boolean old;

		public GetSystemMsgThread(int type, int pagesize, int curPage,
				long minSeqId, long maxSeqId, boolean old)
		{
			handler = new Handler();
			this.pagesize = pagesize;
			this.curPage = curPage;
			this.minSeqId = minSeqId;
			this.maxSeqId = maxSeqId;
			this.type = type;
			this.old = old;
		}

		@Override
		public void run()
		{
			resultDTO = new GetMessageList(mUser.getUid(), mUser.getToken(),
					pagesize, curPage, minSeqId, maxSeqId).getConnect();
			handler.post(new Runnable()
			{

				@Override
				public void run()
				{
					if (resultDTO != null && resultDTO.getCode().equals("0"))
					{
						msgList = GsonHelper.gsonToObj(resultDTO.getResult(),
								new TypeToken<List<Message>>()
								{
								});
						if (msgList != null && msgList.size() > 0)
						{
							SystemMsgHelper.addMessageList(mContext, msgList);
						}
					}
					int lType = ((MessageCenterActivity) mContext).getType();
					if (old)
					{
						if (lType == type && msgList != null
								&& msgList.size() > 0)
						{
							((MessageCenterActivity) mContext)
									.refreshOldSystemMsg(msgList);
						}
						mScroller.startScroll(0, newY + mPdRefreshLayoutHeight,
								0, -mPdRefreshLayoutHeight, 800);
						invalidate();
						isLoading = false;
					}
					else
					{
						if (lType == type)
						{
							((MessageCenterActivity) mContext)
									.refreshSystemMsg(msgList);
						}
						LightDBHelper.setSystemMsgCount(mContext, 0);
						// BroadCastHelper.sendRefreshAllNewBroadcast(mContext);
						BroadCastHelper.sendRefreshMainBroadcast(mContext,
								GobalConstants.RefreshType.ALLNEW);
						mScroller.startScroll(0, newY - mPdRefreshLayoutHeight,
								0, mPdRefreshLayoutHeight, 800);
						mPdProgressBar.setVisibility(View.GONE);
						invalidate();
						isLoading = false;
					}
				}
			});
		}
	}

	class GetMyReceiveReplyThread extends Thread
	{

		private Handler handler;
		private ResultDTO resultDTO;
		private int pagesize;
		private int curPage;
		private long minSeqId;
		private long maxSeqId;
		private List<Answer> answerList;
		private int type;
		private boolean old;

		public GetMyReceiveReplyThread(int type, int pagesize, int curPage,
				long minSeqId, long maxSeqId, boolean old)
		{
			handler = new Handler();
			this.pagesize = pagesize;
			this.curPage = curPage;
			this.minSeqId = minSeqId;
			this.maxSeqId = maxSeqId;
			this.type = type;
			this.old = old;
		}

		@Override
		public void run()
		{
			resultDTO = new GetMyReceiveReply(mUser.getUid(), mUser.getToken(),
					pagesize, curPage, minSeqId, maxSeqId).getConnect();
			handler.post(new Runnable()
			{

				@Override
				public void run()
				{
					if (resultDTO != null && resultDTO.getCode().equals("0"))
					{
						answerList = GsonHelper.gsonToObj(
								resultDTO.getResult(),
								new TypeToken<List<Answer>>()
								{
								});
						if (answerList != null && answerList.size() > 0
								&& mContext != null)
						{
							if (!old)
								AnswerHelper
										.changeOtherReplyMeAnswersNo(mContext);
							AnswerHelper.addOtherReplyMeList(mContext,
									answerList);
						}
						else
						{
							LogFile.SaveErrorLog("mContext=null");
						}
					}
					if (answerList != null && answerList.size() > 0)
					{
						int lType = ((MessageCenterActivity) mContext)
								.getType();
						if (old)
						{
							if (lType == type)
								((MessageCenterActivity) mContext)
										.refreshOldReply(answerList);
						}
						else
						{
							if (lType == type)
								((MessageCenterActivity) mContext)
										.refreshNewReply(answerList);
							LightDBHelper.setNewReplyCount(mContext, 0);
							// BroadCastHelper.sendRefreshAllNewBroadcast(mContext);
							BroadCastHelper.sendRefreshMainBroadcast(mContext,
									GobalConstants.RefreshType.ALLNEW);
						}
					}
					if (old)
					{
						mScroller.startScroll(0, newY + mPdRefreshLayoutHeight,
								0, -mPdRefreshLayoutHeight, 800);
						invalidate();
						isLoading = false;
					}
					else
					{
						mScroller.startScroll(0, newY - mPdRefreshLayoutHeight,
								0, mPdRefreshLayoutHeight, 800);
						mPdProgressBar.setVisibility(View.GONE);
						invalidate();
						isLoading = false;
					}
				}
			});
		}
	}

	class GetMyBestAnswersThread extends Thread
	{

		private Handler handler;
		private ResultDTO resultDTO;
		private int pagesize;
		private int curPage;
		private long minSeqId;
		private long maxSeqId;
		private List<AcceptedAnswer> acceptedAnswerList;
		private int type;
		private boolean old;

		public GetMyBestAnswersThread(int type, int pagesize, int curPage,
				long minSeqId, long maxSeqId, boolean old)
		{
			handler = new Handler();
			this.pagesize = pagesize;
			this.curPage = curPage;
			this.minSeqId = minSeqId;
			this.maxSeqId = maxSeqId;
			this.type = type;
			this.old = old;
		}

		@Override
		public void run()
		{
			resultDTO = new GetMyBestAnswers(mUser.getUid(), mUser.getToken(),
					pagesize, curPage, minSeqId, maxSeqId).getConnect();
			handler.post(new Runnable()
			{

				@Override
				public void run()
				{
					if (resultDTO != null && resultDTO.getCode().equals("0"))
					{
						acceptedAnswerList = GsonHelper.gsonToObj(
								resultDTO.getResult(),
								new TypeToken<List<AcceptedAnswer>>()
								{
								});
						if (acceptedAnswerList != null
								&& acceptedAnswerList.size() > 0)
						{
							// AnswerHelper.changeOtherReplyMeAnswersNo(mContext);
							if (!old)
								AcceptedAnswerHelper
										.deleteAcceptedAnswers(mContext);
							AcceptedAnswerHelper.addAcceptedAnswerList(
									mContext, acceptedAnswerList);
							// AnswerHelper.addBestList(mContext,
							// answerList,mUser);
						}
					}
					if (acceptedAnswerList != null
							&& acceptedAnswerList.size() > 0)
					{
						int lType = ((MessageCenterActivity) mContext)
								.getType();
						if (old)
						{
							if (lType == type)
								((MessageCenterActivity) mContext)
										.refreshOldBest(acceptedAnswerList);
						}
						else
						{
							if (lType == type)
								((MessageCenterActivity) mContext)
										.refreshNewBest(acceptedAnswerList);
							LightDBHelper.setBestAnswerCount(mContext, 0);
							BroadCastHelper.sendRefreshMainBroadcast(mContext,
									GobalConstants.RefreshType.ALLNEW);
						}
					}
					if (old)
					{
						mScroller.startScroll(0, newY + mPdRefreshLayoutHeight,
								0, -mPdRefreshLayoutHeight, 800);
						invalidate();
						isLoading = false;
					}
					else
					{
						mScroller.startScroll(0, newY - mPdRefreshLayoutHeight,
								0, mPdRefreshLayoutHeight, 800);
						mPdProgressBar.setVisibility(View.GONE);
						invalidate();
						isLoading = false;
					}
				}
			});
		}
	}

	class GetMyReceiveTopicReplysThread extends Thread
	{

		private Handler handler;
		private ResultDTO resultDTO;
		private int pagesize;
		private int curPage;
		private List<Post> postList;
		private long minSeqId;
		private long maxSeqId;
		private boolean old;
		private int type;

		public GetMyReceiveTopicReplysThread(int type, int pagesize,
				int curPage, long minSeqId, long maxSeqId, boolean old)
		{
			handler = new Handler();
			this.pagesize = pagesize;
			this.curPage = curPage;
			this.minSeqId = minSeqId;
			this.maxSeqId = maxSeqId;
			this.old = old;
			this.type = type;
		}

		@Override
		public void run()
		{
			resultDTO = new GetMyReceiveTopicReplys(mUser.getUid(),
					mUser.getToken(), pagesize, curPage, minSeqId, maxSeqId)
					.getConnect();
			handler.post(new Runnable()
			{

				@Override
				public void run()
				{
					if (resultDTO != null && resultDTO.getCode().equals("0"))
					{
						postList = GsonHelper.gsonToObj(resultDTO.getResult(),
								new TypeToken<List<Post>>()
								{
								});
						if (!old)
							PostHelper.deleteReceivePosts(mContext);
						PostHelper.addReceivePostsList(mContext, postList);
					}
					if (postList != null && postList.size() > 0)
					{
						int lType = ((MessageCenterActivity) mContext)
								.getType();
						if (old)
						{
							if (lType == type)
								((MessageCenterActivity) mContext)
										.refreshOldTopicReply(postList);
						}
						else
						{
							LightDBHelper.setPostMaxSeqId(mContext, postList
									.get(0).getSeqId());
							LightDBHelper.setNewPostCount(mContext, 0);
							BroadCastHelper.sendRefreshMainBroadcast(mContext,
									GobalConstants.RefreshType.ALLNEW);
							if (lType == type)
								((MessageCenterActivity) mContext)
										.refreshNewTopicReply(postList);
						}
					}
					if (old)
					{
						mScroller.startScroll(0, newY + mPdRefreshLayoutHeight,
								0, -mPdRefreshLayoutHeight, 800);
						invalidate();
						isLoading = false;
					}
					else
					{
						mScroller.startScroll(0, newY - mPdRefreshLayoutHeight,
								0, mPdRefreshLayoutHeight, 800);
						mPdProgressBar.setVisibility(View.GONE);
						invalidate();
						isLoading = false;
					}
					invalidate();
					isLoading = false;
				}
			});
		}
	}

	class GetMyReceiveThanksThread extends Thread
	{

		private Handler handler;
		private ResultDTO resultDTO;
		private int pagesize;
		private int curPage;
		private List<Appreciation> appreciationList;
		private long minSeqId;
		private long maxSeqId;
		private int type;
		private boolean old;

		public GetMyReceiveThanksThread(int type, int pagesize, int curPage,
				long minSeqId, long maxSeqId, boolean old)
		{
			handler = new Handler();
			this.pagesize = pagesize;
			this.curPage = curPage;
			this.minSeqId = minSeqId;
			this.maxSeqId = maxSeqId;
			this.type = type;
			this.old = old;
		}

		@Override
		public void run()
		{
			resultDTO = new GetMyReceiveThanks(mUser.getUid(),
					mUser.getToken(), pagesize, curPage, minSeqId, maxSeqId)
					.getConnect();
			handler.post(new Runnable()
			{

				@Override
				public void run()
				{
					if (resultDTO != null && resultDTO.getCode().equals("0"))
					{
						appreciationList = GsonHelper.gsonToObj(
								resultDTO.getResult(),
								new TypeToken<List<Appreciation>>()
								{
								});
						if (appreciationList != null
								&& appreciationList.size() > 0)
						{
							if (!old)
								AppreciationHelper
										.deleteAppreciations(mContext);
							AppreciationHelper.addAppreciationList(mContext,
									appreciationList);
						}
					}
					if (appreciationList != null && appreciationList.size() > 0)
					{
						int lType = ((MessageCenterActivity) mContext)
								.getType();
						if (old)
						{
							if (lType == type)
								((MessageCenterActivity) mContext)
										.refreshOldAppreciation(appreciationList);
						}
						else
						{
							if (lType == type)
								((MessageCenterActivity) mContext)
										.refreshNewAppreciation(appreciationList);
							LightDBHelper.setNewAppreciationCount(mContext, 0);
							BroadCastHelper.sendRefreshMainBroadcast(mContext,
									GobalConstants.RefreshType.ALLNEW);
						}
					}
					if (old)
					{
						mScroller.startScroll(0, newY + mPdRefreshLayoutHeight,
								0, -mPdRefreshLayoutHeight, 800);
						invalidate();
						isLoading = false;
					}
					else
					{
						mScroller.startScroll(0, newY - mPdRefreshLayoutHeight,
								0, mPdRefreshLayoutHeight, 800);
						mPdProgressBar.setVisibility(View.GONE);
						invalidate();
						isLoading = false;
					}
				}
			});
		}
	}

	class GoneThread extends Thread
	{

		Handler handler;
		private int type;

		public GoneThread(int type)
		{
			handler = new Handler();
			this.type = type;
		}

		@Override
		public void run()
		{
			try
			{
				Thread.sleep(2000);
			}
			catch (InterruptedException e)
			{
				e.printStackTrace();
			}
			handler.post(new Runnable()
			{

				@Override
				public void run()
				{
					if (isLoading)
					{
						if (type == GobalConstants.PullRefreshType.DOWN)
						{
							mScroller.startScroll(0, newY
									- mPdRefreshLayoutHeight, 0,
									mPdRefreshLayoutHeight, 800);
							mPdProgressBar.setVisibility(View.GONE);
						}
						else if (type == GobalConstants.PullRefreshType.UP)
						{
							mScroller.startScroll(0, newY
									+ mPdRefreshLayoutHeight, 0,
									-mPdRefreshLayoutHeight, 800);
						}
						invalidate();
					}
					isLoading = false;
				}
			});
		}
	}
*/

	@Override
	protected void onLayout(boolean changed, int l, int t, int r, int b)
	{
		// TODO Auto-generated method stub
		
	}}