package dianyun.baobaowd.defineview;

import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;

import dianyun.baobaowd.db.LightDBHelper;
import dianyun.baobaowd.entity.CateItem;
import dianyun.baobaowd.entity.Constants;
import dianyun.baobaowd.entity.LocalMenu;
import dianyun.baobaowd.help.ShopHttpHelper;
import dianyun.baobaowd.interfaces.DialogCallBack;
import dianyun.baobaowd.util.GobalConstants;
import dianyun.baobaowd.util.TaeSdkUtil;
import dianyun.baobaowd.util.ToastHelper;
import dianyun.baobaowd.util.UserHelper;
import dianyun.baobaowd.util.Utils;
import dianyun.zqcheap.R;
import dianyun.zqcheap.activity.GoodsListActivity;
import dianyun.zqcheap.activity.HtmlActivity;
import dianyun.zqcheap.activity.PrizesActivity;
import dianyun.zqcheap.activity.ShopSpecialTopicDetailActivity;
import dianyun.zqcheap.activity.ShopSpecialTopicListActivity;
import dianyun.zqcheap.application.BaoBaoWDApplication;

public class SpecialTopicView2 extends LinearLayout implements OnClickListener
{

	private Context mContext;
	private ImageView mImageViewLog = null;
	private TextView mMoreTV;
	private TextView mMoreCountTV;
	private LinearLayout mMoreLay;
	private TextView mLastTimeTV;
	private TextView mDetailTV;
	private ImageView mImageContainer;
	private CateItem mCategory = null;
	private RelativeLayout mLastTimeLay;
	private LinearLayout mSpecialContentLay;
	private long mMenuID = -1L;
	private List<CateItem> mDataList;
	private DisplayImageOptions mOptions;
	private View mCurrentActivityView;
	private RelativeLayout mImageLay;
	private RelativeLayout mSpecialDescribleBottomLay = null;
	private int mWidth;
	private int mHeight;

	public SpecialTopicView2(Context context, AttributeSet attrs, int defStyle)
	{
		super(context, attrs, defStyle);
		initView(context);
	}

	public SpecialTopicView2(Context context, AttributeSet attrs)
	{
		super(context, attrs);
		initView(context);
	}

	public SpecialTopicView2(Context context)
	{
		super(context);
		initView(context);
		// TODO Auto-generated constructor stub
	}

	public void setCurrentActivity(Activity activity)
	{
		mContext = activity;// .getApplicationContext();
	}

	private void initView(Context context)
	{
		mContext = context;
		View view = LayoutInflater.from(mContext).inflate(
				R.layout.specialtopic_item_lay2, this, false);
		mImageLay = (RelativeLayout) view.findViewById(R.id.img_lay);
		mSpecialDescribleBottomLay = (RelativeLayout) view
				.findViewById(R.id.special_bottom_describle_lay);
		mImageContainer = (ImageView) view.findViewById(R.id.special_image);
		mImageContainer.setScaleType(ScaleType.CENTER_CROP);
		mImageViewLog = (ImageView) view.findViewById(R.id.special_logo);
		mImageViewLog.setScaleType(ScaleType.CENTER_CROP);
		mLastTimeLay = (RelativeLayout) view.findViewById(R.id.last_time_lay);
		mMoreLay = (LinearLayout) view.findViewById(R.id.more_horizontal_lay);
		mMoreTV = (TextView) view.findViewById(R.id.more_horizontal);
		mMoreCountTV = (TextView) view.findViewById(R.id.more_horizontal_count);
		mLastTimeTV = (TextView) view.findViewById(R.id.last_time);
		mDetailTV = (TextView) view.findViewById(R.id.special_describle);
		mSpecialContentLay = (LinearLayout) view
				.findViewById(R.id.special_content_lay);
		mSpecialContentLay.setOnClickListener(this);
		mMoreLay.setOnClickListener(this);
		mOptions = new DisplayImageOptions.Builder()
				.showImageOnLoading(R.drawable.defaultsmallimg)
				.showImageForEmptyUri(R.drawable.defaultsmallimg)
				.showImageOnFail(R.drawable.defaultsmallimg)
				// 设置图片在下载前是否重置，复位
				.cacheInMemory(true).cacheOnDisk(true).considerExifParams(true)
				.imageScaleType(ImageScaleType.IN_SAMPLE_POWER_OF_2)
				.bitmapConfig(Bitmap.Config.RGB_565).build();
		resetLayoutParams();
		addView(view);
	}

	private void resetLayoutParams()
	{
		mWidth = getResources().getDisplayMetrics().widthPixels
				- (int) (0 * getResources().getDimension(
						R.dimen.speical_padding));
		mHeight = mWidth * 7 / 15;
		LinearLayout.LayoutParams lp = (LinearLayout.LayoutParams) mImageLay
				.getLayoutParams();
		lp.width = mWidth;
		lp.height = mHeight;
		mImageLay.setLayoutParams(lp);
	}

	public void setMargin(int marginDX)
	{
		LinearLayout.LayoutParams lp = (LinearLayout.LayoutParams) getLayoutParams();
		lp.setMargins(marginDX, marginDX, marginDX, marginDX);
		setLayoutParams(lp);
		setPadding(0, 0, 0, 0);
	}

	public void setDataSource(CateItem category, long menuID)
	{
		mCategory = category;
		mMenuID = menuID;
		initData();
	}

	public void setDataSource(List<CateItem> categoryList)
	{
		mCategory = new CateItem();
		mDataList = categoryList;
		if (mDataList == null || mDataList.size() == 0)
		{
			// // 请求服务器
			// ShopHttpHelper.getChildData(mContext, false,
			// String.valueOf(mMenuID), String.valueOf(mCategory.cateId),
			// false, true, new ShopHttpHelper.ShopDataCallback()
			// {
			//
			// @Override
			// public void getMenu(List<LocalMenu> result)
			// {
			// // TODO Auto-generated method stub
			// }
			//
			// @Override
			// public void getChildsData(List<CateItem> result)
			// {
			// mDataList = result;
			// refreshData();
			// }
			// });
		}
		else
		{
			refreshData();
		}
	}

	private void initData()
	{
		if (mCategory == null)
		{
			return;
		}
		// mDataList = ShopDBHelper.getShopMenu_CatentByCategory(mContext,
		// mMenuID, mCategory.cateId);
		if (mDataList == null || mDataList.size() == 0)
		{
			// 请求服务器
			ShopHttpHelper.getChildData(mContext, false,
					String.valueOf(mMenuID), String.valueOf(mCategory.cateId),
					false, true, new ShopHttpHelper.ShopDataCallback()
					{

						@Override
						public void getMenu(List<LocalMenu> result)
						{
							// TODO Auto-generated method stub
						}

						@Override
						public void getChildsData(List<CateItem> result)
						{
							mDataList = result;
							refreshData();
						}
					});
		}
		else
		{
			if (Utils.isNetAvailable(mContext))
			{
				// 请求服务器
				ShopHttpHelper.getChildData(mContext, false,
						String.valueOf(mMenuID),
						String.valueOf(mCategory.cateId), false, true,
						new ShopHttpHelper.ShopDataCallback()
						{

							@Override
							public void getMenu(List<LocalMenu> result)
							{
								// TODO Auto-generated method stub
							}

							@Override
							public void getChildsData(List<CateItem> result)
							{
								mDataList = result;
								refreshData();
							}
						});
			}
			else
			{
				ImageLoader.getInstance().displayImage(
						mDataList.get(0).pics == null ? ""
								: mDataList.get(0).pics.get(0),
						mImageContainer, mOptions);
				mDetailTV.setText(mDataList.get(0).name == null ? ""
						: mDataList.get(0).name);
				if (mDataList.get(0) != null
						&& mDataList.get(0).leftTime != null
						&& mDataList.get(0).leftTime > 0)
				{
					mLastTimeLay.setVisibility(View.VISIBLE);
				}
				else
				{
					mLastTimeLay.setVisibility(View.GONE);
				}
			}
			// mImageViewLog.
		}
	}

	private void refreshData()
	{
		if (mDataList != null && mDataList.size() > 0)
		{
			CateItem item = mDataList.get(0);
			if (item != null)
			{
				String url = "";
				if (item.xType != 3)
				{
					url = item.pic == null ? "" : item.pic;
				}
				else
				{
					url = (item.pics == null ? "" : item.pics.get(0));
				}
				ImageLoader.getInstance().displayImage(url, mImageContainer,
						mOptions);
				if (TextUtils.isEmpty(item.logoPic))
				{
					mImageViewLog.setVisibility(View.GONE);
				}
				else
				{
					mImageViewLog.setVisibility(View.VISIBLE);
					ImageLoader.getInstance().displayImage(item.logoPic,
							mImageViewLog, BaoBaoWDApplication.mOptions);
				}
				if (TextUtils.isEmpty(item.name) && item.leftTime == null)
				{
					mSpecialDescribleBottomLay.setVisibility(View.INVISIBLE);
				}
				else
				{
					mSpecialDescribleBottomLay.setVisibility(View.VISIBLE);
				}
				mDetailTV.setText(item.name);
				if (item.leftTime != null)
				{
					mLastTimeLay.setVisibility(View.VISIBLE);
					mLastTimeTV.setText(Utils.convertTimeToDay(mContext,item.leftTime));
				}
				else
				{
					mLastTimeLay.setVisibility(View.GONE);
				}
			}
			else
			{
				mSpecialContentLay.setVisibility(View.GONE);
			}
		}
		// 显示更多
		if (mDataList.size() > 1 && mDataList.get(1) != null)
		{
			// 显示文字
			if (!TextUtils.isEmpty(mDataList.get(1).name))
			{
				mMoreTV.setText(mDataList.get(1).name);
			}
			// 展示数量
			if (mDataList.get(1).containsNum == null
					|| mDataList.get(1).containsNum == 0)
			{
				mMoreCountTV.setVisibility(View.GONE);
			}
			else
			{
				mMoreCountTV.setVisibility(View.GONE);
				int count = mDataList.get(1).containsNum;
				if (count < 100)
				{
					mMoreCountTV.setText("" + count);
				}
				else
				{
					mMoreCountTV.setText("99+");
				}
			}
			mMoreLay.setVisibility(View.VISIBLE);
		}
		else
		{
			mMoreLay.setVisibility(View.GONE);
		}
	}

	

	@Override
	public void onClick(View v)
	{
		switch (v.getId())
		{
		case R.id.more_horizontal_lay:
			if (mDataList != null && mDataList.size() > 1
					&& mDataList.get(1) != null)
			{
				goTargetActivity(mDataList.get(1));
			} // goSpecialTopicListActivity();
			break;
		case R.id.special_content_lay:
			if (mDataList != null && mDataList.size() > 0
					&& mDataList.get(0) != null
					&& mDataList.get(0).clickType != 0)
			{
				goTargetActivity(mDataList.get(0));
			}
			// goSingleSpecialTopicDetailActivity();
			break;
		}
	}

	public void setCurrentActivityRootView(View activityView)
	{
		mCurrentActivityView = activityView;
	}

	private void goTargetActivity(final CateItem item)
	{
		if (item != null && item.clickType != null)
		{
			int xtype = item.xType;
			int clickType = item.clickType;
			if (item == null || item.clickType == null)
			{
				return;
			}
			Intent itn = null;
			// 点击类型，1：专辑列表，2：专辑详情，3：商品分类列表，4：webview
			switch (item.clickType)
			{
			case 1:
				itn = new Intent(mContext, ShopSpecialTopicListActivity.class);
				itn.putExtra(Constants.OBJECT_EXTRA_NAME,
						item.cateId == null ? 0L : item.cateId);
				itn.putExtra(Constants.EXTRA_NAME, item.pageTitle == null ? ""
						: item.pageTitle);
				break;
			case 2:
				itn = new Intent(mContext, ShopSpecialTopicDetailActivity.class);
				itn.putExtra(Constants.OBJECT_EXTRA_NAME,
						item.cateId == null ? 0L : item.cateId);
				itn.putExtra(Constants.EXTRA_NAME, item.pageTitle == null ? ""
						: item.pageTitle);
				break;
			case 3:
				itn = new Intent(mContext, GoodsListActivity.class);
				itn.putExtra(Constants.OBJECT_EXTRA_NAME,
						item.cateId == null ? 0L : item.cateId);
				itn.putExtra(Constants.EXTRA_NAME, item.pageTitle == null ? ""
						: item.pageTitle);
				break;
			case 4:
				itn = new Intent(mContext, HtmlActivity.class);
				itn.putExtra(GobalConstants.Data.URL,
						item.clickUrl == null ? "" : item.clickUrl);
				itn.putExtra(GobalConstants.Data.TITLE,
						item.pageTitle == null ? "" : item.pageTitle);
				break;
			case 5:
				if (xtype == 3 && item.tbItemId != null)
				{
					// 如果是单品的话
					if (!LightDBHelper.getIsNotTipLoginOrNotFees(mContext)
							&& UserHelper.getUser().getIsGuest() == GobalConstants.UserType.ISSELF
							&& mCurrentActivityView != null)
					{
//						ToastHelper.showTipLoginDialogWhenShop(mContext,
//								mCurrentActivityView,
//								new ToastHelper.dialogCancelCallback()
//								{
//
//									@Override
//									public void onCancle(boolean isNeverTip)
//									{
//										LightDBHelper
//												.setIsNotTipLoginOrNotFees(
//														mContext, isNeverTip);
//										TaeSdkUtil.showTAEItemDetail(
//												(Activity) mContext,
//												item.tbItemId, item.taobaoPid,
//												item.isTk == 1, item.itemType,
//												null,mCurrentActivityView);
//									}
//								});
						
						ToastHelper.showGoLoginDialog(mContext, new DialogCallBack() {
							
							@Override
							public void clickSure() {
								
							}
							
							@Override
							public void clickCancel() {
								TaeSdkUtil.showTAEItemDetail(
										(Activity) mContext,
										item.tbItemId, item.taobaoPid,
										item.isTk == 1, item.itemType,
										null,mCurrentActivityView);
							}
						});
						
						
						
					}
					else
					{
						TaeSdkUtil.showTAEItemDetail((Activity) mContext,
								item.tbItemId, item.taobaoPid, item.isTk == 1,
								item.itemType, null,mCurrentActivityView);
					}
				}
			case 6:
				// 金币商城
				itn = new Intent(mContext, PrizesActivity.class);
				break;
			default:
				break;
			}
			if (itn != null)
			{
				mContext.startActivity(itn);
			}
		}
	}
}
