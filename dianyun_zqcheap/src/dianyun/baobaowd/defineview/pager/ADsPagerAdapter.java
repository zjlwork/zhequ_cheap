package dianyun.baobaowd.defineview.pager;

import java.util.List;

import android.content.Context;
import android.os.Parcelable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;
import dianyun.baobaowd.data.Notice;
import dianyun.zqcheap.R;

public class ADsPagerAdapter extends PagerAdapter
{

	private Context mContext;
	private List<Notice> mList;

	public ADsPagerAdapter(
			List<Notice> mList,
			Context pContext)
	{
		this.mContext = pContext;
		this.mList = mList;
	}

	@Override
	public int getCount()
	{
		return mList.size();
	}

	@Override
	public Object instantiateItem(View collection, final int position)
	{
		final Notice lNotice =mList.get(position); 
		View lView = LayoutInflater.from(mContext).inflate(
				R.layout.adtv_layout, null);
		 TextView notice_tv = (TextView)lView.findViewById(R.id.notice_tv);
		 notice_tv.setText(lNotice.getContent());
		 notice_tv.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
//				Toast.makeText(mContext, str, Toast.LENGTH_SHORT).show();
				
			}
		});
		 ((VerticalViewPager) collection).addView(lView);
		return lView;
	}
	

	@Override
	public void destroyItem(View collection, int position, Object view)
	{
		((VerticalViewPager) collection).removeView((View) view);
//		System.out.println("destroyItem========="+position);
	}

	@Override
	public boolean isViewFromObject(View view, Object object)
	{
		return view == (object);
	}

	@Override
	public void finishUpdate(View arg0)
	{
	}

	@Override
	public void restoreState(Parcelable arg0, ClassLoader arg1)
	{
	}

	@Override
	public Parcelable saveState()
	{
		return null;
	}

	@Override
	public void startUpdate(View arg0)
	{
	}

	@Override
	public int getItemPosition(Object object)
	{
		return POSITION_NONE;
	}
}