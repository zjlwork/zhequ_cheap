package dianyun.baobaowd.defineview;

import android.content.Context;
import android.os.Handler;
import android.os.Handler.Callback;
import android.os.Message;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.LinearLayout;
import android.widget.TextView;
import dianyun.baobaowd.entity.SecKill;
import dianyun.zqcheap.R;

public class CountDownTimeView extends LinearLayout implements Callback {

	private Context mContext;
	private SecKill mSecKill;
	private Handler mHandler;

	public EndCallBack mEndCallBack;

	boolean start;

	public CountDownTimeView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		mContext = context;
		initView();
		System.out
				.println("CountDownTimeView(Context context, AttributeSet attrs, int defStyle)");
	}

	public CountDownTimeView(Context context, AttributeSet attrs) {
		super(context, attrs);
		mContext = context;
		initView();
		System.out
				.println("CountDownTimeView(Context context, AttributeSet attrs)");
	}

	public CountDownTimeView(Context context) {
		super(context);
		mContext = context;
		initView();
		System.out.println("CountDownTimeView(Context context)");
	}

	public void setEndCallBack(EndCallBack pEndCallBack) {
		this.mEndCallBack = pEndCallBack;
	}

	TextView hour_tv;
	TextView minute_tv;
	TextView sec_tv;

	private void initView() {
		LayoutInflater.from(mContext).inflate(R.layout.miaosha_sfm, this, true);
		hour_tv = (TextView) findViewById(R.id.hour_tv);
		minute_tv = (TextView) findViewById(R.id.minute_tv);
		sec_tv = (TextView) findViewById(R.id.sec_tv);
		mHandler = new Handler(this);
	}

	public void initData(SecKill pSecKill) {
		this.mSecKill = pSecKill;
		if (!start) {
			start = true;
			System.out.println("initData   mSecKill==" + mSecKill.startDate
					+ "  " + mSecKill.isStart + "  " + mSecKill.startLeftTime
					+ "  " + mSecKill.endLeftTime);
			mHandler.sendEmptyMessage(REFRESH);
		}

	}

	private void sethms() {
		long time = 0L;
		if (mSecKill.isStart) {
			time = mSecKill.endLeftTime;
		} else {
			time = mSecKill.startLeftTime;
		}
		int hour = geth(time);
		int minute = getm(time % (60 * 60));
		int second = gets(time % 60);
		hour_tv.setText(getStringInt(hour));
		minute_tv.setText(getStringInt(minute));
		sec_tv.setText(getStringInt(second));
	}

	private String getStringInt(int temp) {
		if (temp >= 10)
			return String.valueOf(temp);
		else
			return "0" + temp;
	}

	private int geth(long ys) {
		int hour = 0;
		if (ys > 60 * 60) {
			hour = (int) (ys / (60 * 60));
		}
		return hour;
	}

	private int getm(long ys) {
		int minute = 0;
		if (ys > 60) {
			minute = (int) (ys / 60);
		}
		return minute;

	}

	private int gets(long ys) {
		int second = (int) ys;
		// if (ys > 1000) {
		// second = (int) (ys / 1000);
		// }
		return second;

	}

	public static final int REFRESH = 3;

	@Override
	public boolean handleMessage(Message msg) {
		switch (msg.what) {
		case REFRESH:
			boolean isTimeEnd = false;
			long startLeftTime = mSecKill.startLeftTime;
			long endLeftTime = mSecKill.endLeftTime;

			if (mSecKill.isStart) {
				if (endLeftTime > 0) {
					mSecKill.endLeftTime -= 1;
				} else {
					if (mSecKill.isStart)
						isTimeEnd = true;
				}
			} else {
				if (startLeftTime > 0) {
					mSecKill.startLeftTime -= 1;
				} else {
					if (!mSecKill.isStart)
						mSecKill.isStart = true;
				}
			}

			if (isTimeEnd) {
				if (mEndCallBack != null)
					mEndCallBack.end(mSecKill);
				mHandler.removeMessages(REFRESH);
				start = false;
			} else {
				sethms();
				mHandler.sendEmptyMessageDelayed(REFRESH, 1000);
			}
			break;
		}
		return false;
	}

	public interface EndCallBack {
		void end(SecKill pSecKill);
	}

}
