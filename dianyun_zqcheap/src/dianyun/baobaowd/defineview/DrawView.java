package dianyun.baobaowd.defineview;

import dianyun.baobaowd.util.ConversionHelper;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.RectF;
import android.view.View;

public class DrawView extends View
{

	private Paint mPaint;
	private Canvas mCanvas;
	int sweep;
	private float left;
	private float top;
	private float right;
	private float bottom;
	int color;
	Style style = Paint.Style.FILL;
	private Context mContext;

	public DrawView(Context context)
	{
		super(context);
		mPaint = new Paint();
		mContext = context;
	}

	public DrawView(Context context, float left, float top, float right,
			float bottom, int color)
	{
		super(context);
		mPaint = new Paint();
		this.left = left;
		this.top = top;
		this.right = right;
		this.bottom = bottom;
		this.color = color;
		mContext = context;
	}

	@Override
	protected void onDraw(Canvas canvas)
	{
		// System.out.println("onDraw------------");
		mCanvas = canvas;
		// 设置画布为黑色背
		// 取消锯齿
		mPaint.setAntiAlias(true);
		// 设置画笔风格为
		mPaint.setStyle(style);
		mPaint.setStrokeWidth(ConversionHelper.dipToPx(0.5f, mContext));
		mPaint.setColor(color);
		// mPaint.setStrokeWidth(20);
		// mCanvas.drawArc(new RectF(90,134,120,168 ),-90, sweep, true, mPaint);
		mCanvas.drawArc(new RectF(left, top, right, bottom), -90, sweep, true,
				mPaint);
		super.onDraw(canvas);
	}

	public void setStyle(Style style)
	{
		this.style = style;
	}

	public void setSweep(int sweep)
	{
		this.sweep = sweep;
	}

	public void setRectF(float left, float top, float right, float bottom)
	{
		this.left = left;
		this.top = top;
		this.right = right;
		this.bottom = bottom;
	}
}
