package dianyun.baobaowd.defineview;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.BitmapDrawable;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import dianyun.baobaowd.util.ToastHelper.dialogCancelCallback;
import dianyun.zqcheap.R;
import dianyun.zqcheap.activity.LoginRegisterActivity;

public class TipBackFeesPopup extends PopupWindow
{

	Context mContext = null;
	dialogCancelCallback mClickListener = null;
	View mParent = null;
	LinearLayout ll_popup = null;

	public TipBackFeesPopup(Context context, View parent)
	{
		super(context);
		mContext = context;
		mParent = parent;
		View view = LayoutInflater.from(mContext).inflate(
				R.layout.tip_back_fees_lay, null);
		view.startAnimation(AnimationUtils.loadAnimation(mContext,
				R.anim.fade_ins));
		ll_popup = (LinearLayout) view.findViewById(R.id.ll_popup);
		ll_popup.startAnimation(AnimationUtils.loadAnimation(mContext,
				R.anim.push_bottom_in_2));
		setWidth(LayoutParams.FILL_PARENT);
		setHeight(LayoutParams.FILL_PARENT);
		setBackgroundDrawable(new BitmapDrawable());
		setOutsideTouchable(true);
		setContentView(view);
		update();
		Button bt1 = (Button) view.findViewById(R.id.item_popupwindows_camera);
		Button bt2 = (Button) view.findViewById(R.id.item_popupwindows_Photo);
		Button bt3 = (Button) view.findViewById(R.id.item_popupwindows_cancel);
		bt1.setOnClickListener(new OnClickListener()
		{

			public void onClick(View v)
			{
				if (mClickListener != null)
				{
					mClickListener.onCancle(false);
				}
				dismiss();
			}
		});
		bt2.setOnClickListener(new OnClickListener()
		{

			public void onClick(View v)
			{
				if (mClickListener != null)
				{
					mClickListener.onCancle(true);
				}
				dismiss();
			}
		});
		bt3.setOnClickListener(new OnClickListener()
		{

			public void onClick(View v)
			{
				dismiss();
				Intent lIntent = new Intent(mContext,
						LoginRegisterActivity.class);
				mContext.startActivity(lIntent);
			}
		});
	}

	public void setOnClickListener(dialogCancelCallback callback)
	{
		mClickListener = callback;
	}
	

	public interface PopupClickCallback
	{

		public void handleCameraOnClick();

		public void handleGalleryOnClick();
	}

	public void show()
	{
		setFocusable(true);
		ll_popup.startAnimation(AnimationUtils.loadAnimation(mContext,
				R.anim.push_bottom_in_2));
		if (mParent != null)
			showAtLocation(mParent, Gravity.BOTTOM, 0, 0);
	}
}
