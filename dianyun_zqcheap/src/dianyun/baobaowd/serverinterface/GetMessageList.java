package dianyun.baobaowd.serverinterface;

public class GetMessageList extends HttpAppInterface
{

	public GetMessageList(long uid, String token, int pagesize, int curPage,
			long minSeqId, long maxSeqId)
	{
		super(null);
		url = GETMESSAGELIST_URL + "?uid=" + uid + "&token=" + token
				+ "&pagesize=" + pagesize + "&curPage=" + curPage
				+ "&minSeqId=" + minSeqId + "&maxSeqId=" + maxSeqId;
	}
}
