package dianyun.baobaowd.serverinterface;

import org.apache.http.message.BasicNameValuePair;
import android.content.Context;

public class VerifyPhoto extends HttpAppInterface
{

	public VerifyPhoto(Context context, String phoneNumber, String code)
	{
		super(context);
		url = VERIFYPHOTO_URL;
		lNameValuePairs.add(new BasicNameValuePair("phoneNumber", phoneNumber));
		lNameValuePairs.add(new BasicNameValuePair("code", code));
	}
}
