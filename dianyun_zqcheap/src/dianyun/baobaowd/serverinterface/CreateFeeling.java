package dianyun.baobaowd.serverinterface;

import org.apache.http.message.BasicNameValuePair;
import android.content.Context;

public class CreateFeeling extends HttpAppInterface
{

	public CreateFeeling(Context context, long uid, String token,
			String content, byte picId, String weather, String weatherCode)
	{
		super(context);
		url = CREATEFEELING_URL;
		lNameValuePairs.add(new BasicNameValuePair("uid", String.valueOf(uid)));
		lNameValuePairs.add(new BasicNameValuePair("token", token));
		lNameValuePairs.add(new BasicNameValuePair("content", content));
		lNameValuePairs.add(new BasicNameValuePair("picId", String
				.valueOf(picId)));
		lNameValuePairs.add(new BasicNameValuePair("weather", weather));
		lNameValuePairs.add(new BasicNameValuePair("weatherCode", weatherCode));
	}
}
