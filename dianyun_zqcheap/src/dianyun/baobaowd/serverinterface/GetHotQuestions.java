package dianyun.baobaowd.serverinterface;

public class GetHotQuestions extends HttpAppInterface
{

	public GetHotQuestions(long uid, String token, int pagesize, int curPage,
			long minSeqId, long maxSeqId)
	{
		super(null);
		url = GETHOTQUESTIONS_URL + "?uid=" + uid + "&token=" + token
				+ "&pagesize=" + pagesize + "&curPage=" + curPage
				+ "&minSeqId=" + minSeqId + "&maxSeqId=" + maxSeqId;
	}
	/*
	 * public GetHotQuestions(long uid,String token,int pagesize,long
	 * hotTime,byte actionType) { super(null); url =
	 * GETHOTQUESTIONS_URL+"?uid="+
	 * uid+"&token="+token+"&pagesize="+pagesize+"&hotTime="
	 * +hotTime+"&actionType="+actionType;
	 * 
	 * }
	 */
}
