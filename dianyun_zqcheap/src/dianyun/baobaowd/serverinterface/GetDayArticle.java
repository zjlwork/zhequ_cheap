package dianyun.baobaowd.serverinterface;

import android.content.Context;

public class GetDayArticle extends HttpAppInterface
{

	public GetDayArticle(Context context, long uid, String token, int dayType,
			int dayCount)
	{
		super(context);
		url = GETDAYARTICE_URL + "?uid=" + uid + "&token=" + token
				+ "&dayType=" + dayType + "&dayCount=" + dayCount;
	}
}
