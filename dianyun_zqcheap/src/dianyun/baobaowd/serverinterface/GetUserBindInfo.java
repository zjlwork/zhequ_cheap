package dianyun.baobaowd.serverinterface;

public class GetUserBindInfo extends HttpAppInterface
{

	public GetUserBindInfo(long uid, String token)
	{
		super(null);
		url = GETUSERBINDINFO_URL + "?uid=" + uid + "&token=" + token;
	}
}
