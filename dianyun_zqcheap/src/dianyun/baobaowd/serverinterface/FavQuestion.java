package dianyun.baobaowd.serverinterface;

import org.apache.http.message.BasicNameValuePair;

public class FavQuestion extends HttpAppInterface
{

	public FavQuestion(long uid, String token, String questionId)
	{
		super(null);
		url = FAVQUESTION_URL;
		lNameValuePairs.add(new BasicNameValuePair("uid", String.valueOf(uid)));
		lNameValuePairs.add(new BasicNameValuePair("token", token));
		lNameValuePairs.add(new BasicNameValuePair("questionId", questionId));
	}
}
