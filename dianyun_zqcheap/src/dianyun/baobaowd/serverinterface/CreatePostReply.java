package dianyun.baobaowd.serverinterface;

import org.apache.http.message.BasicNameValuePair;

public class CreatePostReply extends HttpAppInterface
{

	public CreatePostReply(long uid, String token, String jsonContent)
	{
		super(null);
		url = CREATEPOSTREPLY_URL;
		lNameValuePairs.add(new BasicNameValuePair("uid", String.valueOf(uid)));
		lNameValuePairs.add(new BasicNameValuePair("token", token));
		lNameValuePairs.add(new BasicNameValuePair("jsonContent", jsonContent));
	}
}
