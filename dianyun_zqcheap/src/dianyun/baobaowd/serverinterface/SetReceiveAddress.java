package dianyun.baobaowd.serverinterface;

import org.apache.http.message.BasicNameValuePair;

public class SetReceiveAddress extends HttpAppInterface
{

	public SetReceiveAddress(long uid, String token, String jsonContent)
	{
		super(null);
		url = SETRECEIVERADDRESS_URL;
		lNameValuePairs.add(new BasicNameValuePair("uid", String.valueOf(uid)));
		lNameValuePairs.add(new BasicNameValuePair("token", token));
		lNameValuePairs.add(new BasicNameValuePair("jsonContent", jsonContent));
	}
}
