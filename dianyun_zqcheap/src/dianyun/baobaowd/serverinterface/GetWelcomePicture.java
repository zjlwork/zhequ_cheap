package dianyun.baobaowd.serverinterface;

public class GetWelcomePicture extends HttpAppInterface
{

	public GetWelcomePicture(long uid, String token, byte screenType)
	{
		super(null);
		url = GETWELCOMEPICTURE_URL + "?uid=" + uid + "&token=" + token + "&screenType="
				+ screenType ;
	}
}
