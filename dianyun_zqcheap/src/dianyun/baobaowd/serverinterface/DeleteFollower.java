package dianyun.baobaowd.serverinterface;

import org.apache.http.message.BasicNameValuePair;

public class DeleteFollower extends HttpAppInterface
{

	public DeleteFollower(long uid, String token, long toUid)
	{
		super(null);
		url = DELETEFOLLOW_URL;
		lNameValuePairs.add(new BasicNameValuePair("uid", String.valueOf(uid)));
		lNameValuePairs.add(new BasicNameValuePair("token", token));
		lNameValuePairs.add(new BasicNameValuePair("toUid", String
				.valueOf(toUid)));
	}
}
