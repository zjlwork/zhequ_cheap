package dianyun.baobaowd.serverinterface;

public class GetViewPictures extends HttpAppInterface
{

	public GetViewPictures(long uid, String token, int viewType,
			int babyStatus, long babyDay)
	{
		super(null);
		url = GETVIEWPICTURES_URL + "?uid=" + uid + "&token=" + token
				+ "&viewType=" + viewType + "&babyStatus=" + babyStatus
				+ "&babyDay=" + babyDay;
	}
}
