package dianyun.baobaowd.serverinterface;


import org.apache.http.message.BasicNameValuePair;

import android.content.Context;
import dianyun.baobaowd.db.LightDBHelper;
import dianyun.zqcheap.application.BaoBaoWDApplication;

public class BBLogin extends HttpAppInterface
{

	public BBLogin(Context context, String email, String password,
			String deviceId)
	{
		super(context);
		url = BBLOGIN_URL;
		lNameValuePairs.add(new BasicNameValuePair("username", email));
		lNameValuePairs.add(new BasicNameValuePair("password", password));
		lNameValuePairs.add(new BasicNameValuePair("deviceId", deviceId));
		long inviteUid = LightDBHelper.getInviteUid(BaoBaoWDApplication.context);
		if(inviteUid!=0&&inviteUid!=-1)
		lNameValuePairs.add(new BasicNameValuePair("inviteUid", 
				String.valueOf(inviteUid)));
	}
}
