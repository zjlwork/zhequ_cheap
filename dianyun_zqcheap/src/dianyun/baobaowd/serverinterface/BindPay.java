package dianyun.baobaowd.serverinterface;

import org.apache.http.message.BasicNameValuePair;
import android.content.Context;

public class BindPay extends HttpAppInterface
{

	public BindPay(Context context, long uid, String token,
			byte accountType,String account,String accountName,String codeStr)
	{
		super(context);
		url = BINDPAY_URL;
		lNameValuePairs.add(new BasicNameValuePair("uid",  String.valueOf(uid)));
		lNameValuePairs.add(new BasicNameValuePair("token", token));
		lNameValuePairs.add(new BasicNameValuePair("accountType", String.valueOf(accountType)));
		lNameValuePairs.add(new BasicNameValuePair("account", account));
		lNameValuePairs.add(new BasicNameValuePair("accountName", accountName));
		lNameValuePairs.add(new BasicNameValuePair("codeStr", codeStr));
	}
}
