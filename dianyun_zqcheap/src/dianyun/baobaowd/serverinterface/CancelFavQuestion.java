package dianyun.baobaowd.serverinterface;

import org.apache.http.message.BasicNameValuePair;

public class CancelFavQuestion extends HttpAppInterface
{

	public CancelFavQuestion(long uid, String token, String questionId)
	{
		super(null);
		url = CANCELFAVQUESTION_URL;
		lNameValuePairs.add(new BasicNameValuePair("uid", String.valueOf(uid)));
		lNameValuePairs.add(new BasicNameValuePair("token", token));
		lNameValuePairs.add(new BasicNameValuePair("questionId", questionId));
	}
}
