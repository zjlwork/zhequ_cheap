package dianyun.baobaowd.serverinterface;

import org.apache.http.message.BasicNameValuePair;
import android.content.Context;

public class BindPhone extends HttpAppInterface
{

	public BindPhone(Context context, long uid, String token,
			String phone,String codeStr)
	{
		super(context);
		url = BINDPHONE_URL;
		lNameValuePairs.add(new BasicNameValuePair("uid", String.valueOf(uid)));
		lNameValuePairs.add(new BasicNameValuePair("token", token));
		lNameValuePairs.add(new BasicNameValuePair("phone", phone));
		lNameValuePairs.add(new BasicNameValuePair("codeStr", codeStr));
	}
}
