package dianyun.baobaowd.serverinterface;

import org.apache.http.message.BasicNameValuePair;

import dianyun.baobaowd.db.LightDBHelper;
import dianyun.zqcheap.application.BaoBaoWDApplication;

import android.content.Context;

public class ShareOrder extends HttpAppInterface
{

	public ShareOrder( long uid, String token,
			String seqId,boolean rebate)
	{
		super(null);
		url = rebate?SHAREORDER_URL:String.format(ShopHttpRequest.UNREBATESHAREORDER,
    			LightDBHelper.getShopUrlPrefix(BaoBaoWDApplication.context));
		
		lNameValuePairs.add(new BasicNameValuePair("uid", String.valueOf(uid)));
		lNameValuePairs.add(new BasicNameValuePair("token", token));
		lNameValuePairs.add(new BasicNameValuePair("seqId", seqId));
		
	}
}
