package dianyun.baobaowd.serverinterface;

import org.apache.http.message.BasicNameValuePair;

public class CancelFavArticle extends HttpAppInterface
{

	public CancelFavArticle(long uid, String token, String articleId)
	{
		super(null);
		url = CANCELFAVARTICLE_URL;
		lNameValuePairs.add(new BasicNameValuePair("uid", String.valueOf(uid)));
		lNameValuePairs.add(new BasicNameValuePair("token", token));
		lNameValuePairs.add(new BasicNameValuePair("articleId", articleId));
	}
}
