package dianyun.baobaowd.serverinterface;

public class GetHuaFei extends HttpAppInterface
{

	public GetHuaFei(long uid, String token, int pagesize, int curPage)
	{
		super(null);
		url = GETHUAFEI_URL + "?uid=" + uid + "&token=" + token + "&pagesize="
				+ pagesize + "&curPage=" + curPage;
	}
}
