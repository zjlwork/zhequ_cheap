package dianyun.baobaowd.serverinterface;

public class GetUserInfo extends HttpAppInterface
{

	public GetUserInfo(long uid, String token, long queryUid)
	{
		super(null);
		url = GETUSERINFO_URL + "?uid=" + uid + "&token=" + token
				+ "&queryUid=" + queryUid;
	}
}
