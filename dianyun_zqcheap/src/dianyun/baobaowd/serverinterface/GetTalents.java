package dianyun.baobaowd.serverinterface;

public class GetTalents extends HttpAppInterface
{

	public GetTalents(long uid, String token, int pagesize, long minSeqId)
	{
		super(null);
		url = GETTALENTS_URL + "?uid=" + uid + "&token=" + token + "&pagesize="
				+ pagesize + "&minSeqId=" + minSeqId;
	}
}
