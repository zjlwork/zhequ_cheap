package dianyun.baobaowd.serverinterface;

import org.apache.http.message.BasicNameValuePair;

public class DeleteAnswer extends HttpAppInterface
{

	public DeleteAnswer(long uid, String token, String answerId)
	{
		super(null);
		url = DELETEANSWER_URL;
		// openid String QQ开发平台的用户唯一标识必填 *
		// jsonContent String QQ用户信息的json字符串必填 *
		lNameValuePairs.add(new BasicNameValuePair("uid", String.valueOf(uid)));
		lNameValuePairs.add(new BasicNameValuePair("token", token));
		lNameValuePairs.add(new BasicNameValuePair("answerId", answerId));
	}
}
