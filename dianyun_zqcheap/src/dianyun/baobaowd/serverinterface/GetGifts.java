package dianyun.baobaowd.serverinterface;

public class GetGifts extends HttpAppInterface
{

	public GetGifts(long uid, String token, int pagesize, int curPage)
	{
		super(null);
		url = GETGIFTS_URL + "?uid=" + uid + "&token=" + token + "&pagesize="
				+ pagesize + "&curPage=" + curPage;
	}
}
