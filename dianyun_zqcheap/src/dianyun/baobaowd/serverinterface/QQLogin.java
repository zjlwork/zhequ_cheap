
package dianyun.baobaowd.serverinterface;

import org.apache.http.message.BasicNameValuePair;

import android.content.Context;
import dianyun.baobaowd.db.LightDBHelper;
import dianyun.zqcheap.application.BaoBaoWDApplication;

public class QQLogin extends HttpAppInterface
{

	public QQLogin(Context context, String openid, String jsonContent,
			String deviceId)
	{
		super(context);
		url = QQLOGIN_URL;
		// openid String QQ开发平台的用户唯一标识必填 *
		// jsonContent String QQ用户信息的json字符串必填 *
		lNameValuePairs.add(new BasicNameValuePair("openid", openid));
		lNameValuePairs.add(new BasicNameValuePair("jsonContent", jsonContent));
		lNameValuePairs.add(new BasicNameValuePair("deviceId", deviceId));
		long inviteUid = LightDBHelper.getInviteUid(BaoBaoWDApplication.context);
		if(inviteUid!=0&&inviteUid!=-1)
		lNameValuePairs.add(new BasicNameValuePair("inviteUid", 
				String.valueOf(inviteUid)));
	}
}
