package dianyun.baobaowd.serverinterface;


import org.apache.http.message.BasicNameValuePair;

import android.content.Context;
import dianyun.baobaowd.db.LightDBHelper;
import dianyun.zqcheap.application.BaoBaoWDApplication;

public class TaobaoLogin extends HttpAppInterface
{

	public TaobaoLogin(Context context, String taoUserId, String avatarUrl,
			String nick)
	{
		super(context);
		url = TAOBAOLOGIN_URL;
		lNameValuePairs.add(new BasicNameValuePair("taoUserId", taoUserId));
		lNameValuePairs.add(new BasicNameValuePair("avatarUrl", avatarUrl));
		lNameValuePairs.add(new BasicNameValuePair("nick", nick));
		long inviteUid = LightDBHelper.getInviteUid(BaoBaoWDApplication.context);
		if(inviteUid!=0&&inviteUid!=-1)
		lNameValuePairs.add(new BasicNameValuePair("inviteUid", 
				String.valueOf(inviteUid)));
	}
}
