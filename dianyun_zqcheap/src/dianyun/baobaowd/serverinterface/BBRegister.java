package dianyun.baobaowd.serverinterface;

import org.apache.http.message.BasicNameValuePair;
import android.content.Context;

public class BBRegister extends HttpAppInterface
{

	public BBRegister(Context context, String email, String password,
			String nickname, String deviceId)
	{
		super(context);
		url = BBREGISTER_URL;
		lNameValuePairs.add(new BasicNameValuePair("email", email));
		lNameValuePairs.add(new BasicNameValuePair("password", password));
		lNameValuePairs.add(new BasicNameValuePair("nickname", nickname));
		lNameValuePairs.add(new BasicNameValuePair("deviceId", deviceId));
	}
}
