package dianyun.baobaowd.serverinterface;

import org.apache.http.message.BasicNameValuePair;
import android.content.Context;

public class AppAward extends HttpAppInterface
{

	public AppAward(Context context, long uid, String token, String appId)
	{
		super(context);
		url = APPAWARD_URL;
		lNameValuePairs.add(new BasicNameValuePair("uid", String.valueOf(uid)));
		lNameValuePairs.add(new BasicNameValuePair("token", token));
		lNameValuePairs.add(new BasicNameValuePair("appId", appId));
	}
}
