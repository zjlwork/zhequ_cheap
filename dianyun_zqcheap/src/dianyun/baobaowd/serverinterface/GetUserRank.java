package dianyun.baobaowd.serverinterface;

public class GetUserRank extends HttpAppInterface
{

	public GetUserRank(long uid, String token, int pagesize)
	{
		super(null);
		url = GETUSERRANK_URL + "?uid=" + uid + "&token=" + token
				+ "&pagesize=" + pagesize;
	}
}
