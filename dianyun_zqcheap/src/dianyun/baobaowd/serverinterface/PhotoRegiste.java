
package dianyun.baobaowd.serverinterface;

import org.apache.http.message.BasicNameValuePair;

import android.content.Context;
import dianyun.baobaowd.db.LightDBHelper;
import dianyun.zqcheap.application.BaoBaoWDApplication;

public class PhotoRegiste extends HttpAppInterface
{

	public PhotoRegiste(Context context, String phoneNumber, String password,
			String authcode, String deviceId)
	{
		super(context);
		url = PHOTOREGISTE_URL;
		lNameValuePairs.add(new BasicNameValuePair("phoneNumber", phoneNumber));
		lNameValuePairs.add(new BasicNameValuePair("password", password));
		lNameValuePairs.add(new BasicNameValuePair("authcode", authcode));
		lNameValuePairs.add(new BasicNameValuePair("deviceId", deviceId));
		long inviteUid = LightDBHelper.getInviteUid(BaoBaoWDApplication.context);
		if(inviteUid!=0&&inviteUid!=-1)
		lNameValuePairs.add(new BasicNameValuePair("inviteUid", 
				String.valueOf(inviteUid)));
	}
}
