package dianyun.baobaowd.serverinterface;

public class GetLZTopicReply extends HttpAppInterface
{

	public GetLZTopicReply(long uid, String token, String topicId,
			int pagesize, int curPage, long minSeqId, long maxSeqId)
	{
		super(null);
		url = GETLZTOPICREPLY_URL + "?uid=" + uid + "&token=" + token
				+ "&topicId=" + topicId + "&pagesize=" + pagesize + "&curPage="
				+ curPage + "&minSeqId=" + minSeqId + "&maxSeqId=" + maxSeqId;
	}
}
