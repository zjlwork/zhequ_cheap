package dianyun.baobaowd.serverinterface;

public class GetOneQuestion extends HttpAppInterface
{

	public GetOneQuestion(long uid, String token, int pagesize)
	{
		super(null);
		url = GETONEQUESTION_URL + "?uid=" + uid + "&token=" + token
				+ "&pagesize=" + pagesize ;
	}
}
