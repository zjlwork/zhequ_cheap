package dianyun.baobaowd.serverinterface;

import org.apache.http.message.BasicNameValuePair;

public class ExchangeGift extends HttpAppInterface
{

	public ExchangeGift(long uid, String token, String giftIdStr,
			String phoneNumber, String receiverName, long addrId)
	{
		super(null);
		url = EXCHANGEGIFTS_URL;
		lNameValuePairs.add(new BasicNameValuePair("uid", String.valueOf(uid)));
		lNameValuePairs.add(new BasicNameValuePair("token", token));
		lNameValuePairs.add(new BasicNameValuePair("giftIdStr", giftIdStr));
		lNameValuePairs.add(new BasicNameValuePair("phoneNumber", phoneNumber));
		lNameValuePairs
				.add(new BasicNameValuePair("receiverName", receiverName));
		lNameValuePairs.add(new BasicNameValuePair("addrId", String
				.valueOf((addrId==0L?"":addrId))));
	}
}
