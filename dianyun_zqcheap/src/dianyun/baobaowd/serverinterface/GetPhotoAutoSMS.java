package dianyun.baobaowd.serverinterface;

import org.apache.http.message.BasicNameValuePair;
import android.content.Context;

public class GetPhotoAutoSMS extends HttpAppInterface
{

	public GetPhotoAutoSMS(Context context, String phoneNumber, String deviceId)
	{
		super(context);
		url = PHOTOAUTO_URL;
		lNameValuePairs.add(new BasicNameValuePair("phoneNumber", phoneNumber));
		lNameValuePairs.add(new BasicNameValuePair("deviceId", deviceId));
	}
}
