
package dianyun.baobaowd.serverinterface;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.zip.GZIPInputStream;

import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.json.JSONObject;

import android.content.Context;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;

import com.dianyun.netsdk.MD5Util;

import dianyun.baobaowd.data.User;
import dianyun.baobaowd.db.LightDBHelper;
import dianyun.baobaowd.dto.ResultDTO;
import dianyun.baobaowd.gson.GsonHelper;
import dianyun.baobaowd.help.LogFile;
import dianyun.baobaowd.util.GobalConstants;
import dianyun.baobaowd.util.UserHelper;
import dianyun.zqcheap.application.BaoBaoWDApplication;

public class ShopHttpRequest
{

    private final String SHOP_CHEAP_DETAIL = "detail?onlyItems=true&xid=%s&curPage=%s&pagesize=%s";
    private final String SHOP_HIGHLEST_DETAIL = "detail?onlyItems=true&xid=%s&curPage=%s&pagesize=%s";
    private final String SHOP_GET_MIAOSHA = "%sopenApi/seckill/list.json";
    private final String SHOP_MIAOSHA = "%sopenApi/seckill/kill.json";

    private final String SHOP_HOST = "%sopenApi/category/";
	private final String SHOP_HOST_ORDER = "%sopenApi/order/my";
	private final String SHOP_SCAN_CHECK = "%s?url=%s";
    private final String SHOP_GET_SCAN_CHECK = "%s?url=%s";
    private final String SHOP_GET_SEARCH_RESULT_LIST="%sitem/search.json";
    private final String SHOP_GET_FREEZEYOCOINS="%sopenApi/order/mytotal";
    private final String SHOP_GET_SECONDMENUS="%sopenApi/category/secondMenus";
    private final String SHOP_GET_FOCUSPICTURE="%sopenApi/category/focus";
    public static final String UNREBATESHAREORDER="%sshare/daily/shareOrder";
    
    

    // private final String SHOP_HOST =
	// "http://yoyo-1.wx.jaeapp.com/openApi/category/";
	private final String SHOP_MENU = "menus";
	private final String SHOP_DETAIL = "detail?xid=%s";
	private final String SHOP_MOREDETAIL = "detail?xid=%s&curPage=%s&pagesize=%s";
	protected Context context;
	protected DefaultHttpClient lClient;
	protected List<NameValuePair> lNameValuePairs;
	protected MultipartEntity reqEntity = null;

	// private static ShopHttpRequest mHelper;
	public ShopHttpRequest(Context context)
	{
		this.context = context;
		HttpParams httpParameters;
		int timeoutConnection = 60000;
		int timeoutSocket = 60000;
		httpParameters = new BasicHttpParams();// Set the timeout in
												// milliseconds until a
												// connection is established.
		HttpConnectionParams.setConnectionTimeout(httpParameters,
				timeoutConnection);// Set the default socket timeout
									// (SO_TIMEOUT) // in milliseconds which is
									// the timeout for waiting for data.
		HttpConnectionParams.setSoTimeout(httpParameters, timeoutSocket);
		lClient = new DefaultHttpClient(httpParameters);
		lNameValuePairs = new ArrayList<NameValuePair>();
		// TODO Auto-generated constructor stub
	}

	public static ShopHttpRequest getInstance(Context context)
	{
		// if (mHelper == null)
		// {
		// mHelper = new ShopHttpRequest(context);
		// }
		// return mHelper;
		return new ShopHttpRequest(context);
	}


	public ResultDTO getMenu()
	{
		return getConnect(String.format(SHOP_HOST,
				LightDBHelper.getShopUrlPrefix(context))
				+ SHOP_MENU);
	}
	
	public ResultDTO getSecondMenus(long parentId)
	{
		return getConnect(String.format(SHOP_GET_SECONDMENUS,
				LightDBHelper.getShopUrlPrefix(context))
				+ "?xid="+parentId);
	}
	public ResultDTO getFreenzeYoCoins(long uid)
	{
		return getConnect(String.format(SHOP_GET_FREEZEYOCOINS,
				LightDBHelper.getShopUrlPrefix(context))+"?uid="+uid);
	}

	public ResultDTO getMenuChilds(String menuID)
	{
		return getConnect(String.format(SHOP_HOST,
				LightDBHelper.getShopUrlPrefix(context))
				+ String.format(SHOP_DETAIL, menuID));
	}

	public ResultDTO getOrders(long uid, String token, int curPage, int pagesize)
	{
		 return getConnect(String.format(SHOP_HOST_ORDER,
		 LightDBHelper.getShopUrlPrefix(context))
		 +"?uid="+uid+"&token="+token+"&curPage="+curPage+"&pagesize="+pagesize);

	}

	public ResultDTO getMoreChilds(String parentID, int pageIndex, int pageSize)
	{
		return getConnect(String.format(SHOP_HOST,
				LightDBHelper.getShopUrlPrefix(context))
				+ String.format(SHOP_MOREDETAIL, parentID, pageIndex, pageSize));
	}

    public ResultDTO getCheapData(String parentID,int pageIndex, int pageSize)
    {
        return getConnect(String.format(SHOP_HOST,
                LightDBHelper.getShopUrlPrefix(context))
                + String.format(SHOP_CHEAP_DETAIL, parentID, pageIndex, pageSize));
    }
    public ResultDTO getMiaoShaData()
    {
    	return getConnect(String.format(SHOP_GET_MIAOSHA,
    			LightDBHelper.getShopUrlPrefix(context)));
    }
    public ResultDTO miaoSha(long uid,long timeId)
    {
    	return getConnect(String.format(SHOP_MIAOSHA,
    			LightDBHelper.getShopUrlPrefix(context))+"?uid="+uid+"&timeId="+timeId);
    }

    public ResultDTO getHighlestData(String parentID,int pageIndex, int pageSize)
    {
        return getConnect(String.format(SHOP_HOST,
                LightDBHelper.getShopUrlPrefix(context))
                + String.format(SHOP_HIGHLEST_DETAIL, parentID, pageIndex, pageSize));
    }
    public ResultDTO getFocusPictureData()
    {
    	
    	return getConnect(String.format(SHOP_GET_FOCUSPICTURE,
    			LightDBHelper.getShopUrlPrefix(context)));
    }
    public ResultDTO getMiaoShaPictureData(long uid,String token,int viewType)
    {
    	
    	return getConnect(GobalConstants.URL.DEFAULTPREFIX +
    			GobalConstants.URL.GETMIAOSHAPICTURES+"?uid="+uid+"&token="+token+"&viewType="+viewType);
    }


	public ResultDTO bindAccount(String uid, String token, String taoUserID,
			String taoUserActarUrl, String taoNickName)
	{
		List<NameValuePair> lNameValuePairs = new ArrayList<NameValuePair>();
		lNameValuePairs.add(new BasicNameValuePair("uid", String.valueOf(uid)));
		lNameValuePairs.add(new BasicNameValuePair("token", token));
		lNameValuePairs.add(new BasicNameValuePair("taoUserId", taoUserID));
		lNameValuePairs.add(new BasicNameValuePair("avatarUrl", String
				.valueOf(taoUserActarUrl)));
		lNameValuePairs.add(new BasicNameValuePair("nick", taoNickName));
		return postConnect(GobalConstants.URL.SHOP_BINDACCOUNT, lNameValuePairs);
	}

	public ResultDTO scanCheckUrl(String url)
	{
		String urlResult = String.format(SHOP_SCAN_CHECK,
				LightDBHelper.getScanCheckUrlPrefix(context),
				Base64.encodeToString(url.getBytes(), Base64.NO_WRAP));
		urlResult.replace(" ", "%20");
		return getConnect(urlResult);
	}

    /**
     * 获取Http短链
     * @param url
     * @return
     */
    public ResultDTO getHttpCheckUrl(String url)
    {
        String urlResult = String.format(SHOP_GET_SCAN_CHECK,LightDBHelper.getFetchCheckUrlPrefix(context),
                Base64.encodeToString(url.getBytes(), Base64.NO_WRAP));
        urlResult.replace(" ", "%20");
        return getConnect(urlResult);
    }

    /**
     * 获取Http短链以及文字信息
     * @param url
     * @return
     */
    public ResultDTO getHttpWithPrimaryWords(String url)
    {
        List<NameValuePair> lNameValuePairs = new ArrayList<NameValuePair>();
        lNameValuePairs.add(new BasicNameValuePair("content", Base64.encodeToString(url.getBytes(), Base64.NO_WRAP)));

        return postConnect(LightDBHelper.getPrimaryWithWordsUrlPrefix(context), lNameValuePairs);

    }

    /**
     * 获取关键字搜索的结果
     * @param url
     * @param keyWords
     * @param pagesize
     * @param pageIndex
     * @return
     */
    public ResultDTO getCateItemListByHttpWithPrimaryWords(String url,String keyWords,String order,
    		int pagesize,int pageIndex,int  startPrice,int endPrice,boolean isMall)
    {
        List<NameValuePair> lNameValuePairs = new ArrayList<NameValuePair>();
        if(TextUtils.isEmpty(url))
        {
            url="";
        }
        lNameValuePairs.add(new BasicNameValuePair("keywords",keyWords));
        lNameValuePairs.add(new BasicNameValuePair("order",order));
        lNameValuePairs.add(new BasicNameValuePair("url", Base64.encodeToString(url.getBytes(), Base64.NO_WRAP)));
        lNameValuePairs.add(new BasicNameValuePair("curPage", String.valueOf(pageIndex)));
        lNameValuePairs.add(new BasicNameValuePair("pagesize", String.valueOf(pagesize)));
		 lNameValuePairs.add(new BasicNameValuePair("isMall", String.valueOf(isMall)));
		 if(startPrice!=-1)
	     lNameValuePairs.add(new BasicNameValuePair("startPrice", String.valueOf(startPrice)));
		 if(endPrice!=-1)
	     lNameValuePairs.add(new BasicNameValuePair("endPrice", String.valueOf(endPrice)));
       
        return postConnect2(String.format(SHOP_GET_SEARCH_RESULT_LIST,LightDBHelper.getShopUrlPrefix(context)), lNameValuePairs);
    }
    public ResultDTO getHotWordList(long uid,String token)
    {
    	return getConnect(GobalConstants.URL.DEFAULTPREFIX+GobalConstants.URL.GETHOTWORDS+"?uid="+uid+"&token="+token);
    }


    public void saveConnectlog(String pUrl)
	{
		LogFile.SaveLog("saveConnectlog ===" + pUrl);
	}

	public void saveStatusCode(int statusCode, String pUrl)
	{
		LogFile.SaveLog("saveStatusCode = " + pUrl + " lNameValuePairs="
				+ lNameValuePairs + "statusCode ===" + statusCode);
	}
	
	public void saveResultlog(String result, String pUrl)
	{
		LogFile.SaveLog("saveResultlog = " + pUrl + " lNameValuePairs="
				+ lNameValuePairs + "result ===" + result);
	}
	
	

	private ResultDTO postConnect(String url,
			List<NameValuePair> lNameValuePairs)
	{
		saveConnectlog(url);
		StringBuilder s = new StringBuilder();
		try
		{
			HttpPost lHttpPost = new HttpPost(url);
			lHttpPost.addHeader("Accept-Encoding", "gzip");
			lHttpPost.addHeader("device", "android");
			User luser = UserHelper.getUser();
			String bbBirthday = "";
			String bbStatus = "";
			if (luser != null)
			{
				bbBirthday = luser.getBabyBirthday();
				bbStatus = "" + UserHelper.getBBStatus(bbBirthday);
			}
			lHttpPost.addHeader("bbBirthday", bbBirthday);
			lHttpPost.addHeader("bbStatus", bbStatus);
			lHttpPost.addHeader("X-APPID",
					String.valueOf(GobalConstants.AppID.DIANYUN_CHAIFENID));
			lHttpPost.addHeader("deviceId", BaoBaoWDApplication.deviceId);
			lHttpPost.addHeader("ver", BaoBaoWDApplication.versionName);
			lHttpPost.addHeader("X-VER", BaoBaoWDApplication.versionCode + "");
			lHttpPost.addHeader("channel", BaoBaoWDApplication.channel);
			String timeString = "" + Calendar.getInstance().getTimeInMillis();
			lHttpPost.addHeader("clientTime", timeString);
			lHttpPost.addHeader(
					"uuToken",
					""+ MD5Util.getInfoMD5(BaoBaoWDApplication.context,
									HttpAppInterface.getPostUid(lNameValuePairs)+ BaoBaoWDApplication.deviceId+ timeString));

			if (reqEntity != null)
				lHttpPost.setEntity(reqEntity);
			else
				lHttpPost.setEntity(new UrlEncodedFormEntity(lNameValuePairs,
						"UTF-8"));


			HttpResponse response = lClient.execute(lHttpPost);
			saveStatusCode(response.getStatusLine().getStatusCode(), url);
			if (200 == response.getStatusLine().getStatusCode())
			{
				InputStream instream = response.getEntity().getContent();
				Header contentEncoding = response
						.getFirstHeader("Content-Encoding");
				if (contentEncoding != null
						&& contentEncoding.getValue().equalsIgnoreCase("gzip"))
				{
					instream = new GZIPInputStream(instream);
				}
				BufferedReader reader = new BufferedReader(
						new InputStreamReader(instream, "UTF-8"));
				String sResponse;
				while ((sResponse = reader.readLine()) != null)
				{
					s = s.append(sResponse);
				}
				ResultDTO lResultDTO = GsonHelper.gsonToObj(s.toString(),
						ResultDTO.class);
				JSONObject jsonObject = new JSONObject(s.toString());
				lResultDTO.setResult(jsonObject.get("result").toString());
				return lResultDTO;
			}
		}
		catch (Exception e)
		{
			LogFile.SaveExceptionLog(e);
		}
		finally
		{
			saveResultlog(s.toString(), url);
		}
		return null;
	}

    private ResultDTO postConnect2(String url,
                                  List<NameValuePair> lNameValuePairs)
    {
        saveConnectlog(url);
        StringBuilder s = new StringBuilder();
        try
        {
            HttpPost lHttpPost = new HttpPost(url);
            lHttpPost.addHeader("Accept-Encoding", "gzip");
            lHttpPost.addHeader("device", "android");
            User luser = UserHelper.getUser();
            String bbBirthday = "";
            String bbStatus = "";
            if (luser != null)
            {
                bbBirthday = luser.getBabyBirthday();
                bbStatus = "" + UserHelper.getBBStatus(bbBirthday);
            }
            lHttpPost.addHeader("bbBirthday", bbBirthday);
            lHttpPost.addHeader("bbStatus", bbStatus);
            lHttpPost.addHeader("X-APPID",
                    String.valueOf(GobalConstants.AppID.DIANYUN_CHAIFENID));
            lHttpPost.addHeader("deviceId", BaoBaoWDApplication.deviceId);
            lHttpPost.addHeader("ver", BaoBaoWDApplication.versionName);
            lHttpPost.addHeader("X-VER", BaoBaoWDApplication.versionCode + "");
            lHttpPost.addHeader("channel", BaoBaoWDApplication.channel);
            String timeString = "" + Calendar.getInstance().getTimeInMillis();
            lHttpPost.addHeader("clientTime", timeString);

            lHttpPost.addHeader(
                    "uuToken",
                    ""+ MD5Util.getInfoMD5(BaoBaoWDApplication.context,
                    		HttpAppInterface.getPostUid(lNameValuePairs)+ BaoBaoWDApplication.deviceId+ timeString));
            if (reqEntity != null)
                lHttpPost.setEntity(reqEntity);
            else
                lHttpPost.setEntity(new UrlEncodedFormEntity(lNameValuePairs,
                        "UTF-8"));


            HttpResponse response = lClient.execute(lHttpPost);
            saveStatusCode(response.getStatusLine().getStatusCode(), url);
            if (200 == response.getStatusLine().getStatusCode())
            {
                InputStream instream = response.getEntity().getContent();
                Header contentEncoding = response
                        .getFirstHeader("Content-Encoding");
                if (contentEncoding != null
                        && contentEncoding.getValue().equalsIgnoreCase("gzip"))
                {
                    instream = new GZIPInputStream(instream);
                }
                BufferedReader reader = new BufferedReader(
                        new InputStreamReader(instream, "UTF-8"));
                String sResponse;
                while ((sResponse = reader.readLine()) != null)
                {
                    s = s.append(sResponse);
                }
                ResultDTO lResultDTO = GsonHelper.gsonToObj(s.toString(),
                        ResultDTO.class);
                JSONObject jsonObject = new JSONObject(s.toString());
                lResultDTO.setResult(jsonObject.get("result").toString());
                lResultDTO.keywords=jsonObject.get("keywords").toString();
                return lResultDTO;
            }
        }
        catch (Exception e)
        {
            LogFile.SaveExceptionLog(e);
        }
        finally
        {
            saveResultlog(s.toString(), url);
        }
        return null;
    }

	





	private ResultDTO getConnect(String url)
	{
		System.out.println("url======"+url);
		ResultDTO lResultDTO = null;
		saveConnectlog(url);
		StringBuilder s = new StringBuilder();
		try
		{
			HttpGet lHttpGet = new HttpGet(url);
			lHttpGet.addHeader("Accept-Encoding", "gzip");
			User luser = UserHelper.getUser();
			String bbBirthday = "";
			String bbStatus = "";
			if (luser != null)
			{
				bbBirthday = luser.getBabyBirthday();
				bbStatus = "" + UserHelper.getBBStatus(bbBirthday);
			}
			lHttpGet.addHeader("bbBirthday", bbBirthday);
			lHttpGet.addHeader("bbStatus", bbStatus);
			lHttpGet.addHeader("device", "android");
			lHttpGet.addHeader("X-APPID",
					String.valueOf(GobalConstants.AppID.DIANYUN_CHAIFENID));
			lHttpGet.addHeader("deviceId", BaoBaoWDApplication.deviceId);
			lHttpGet.addHeader("ver", BaoBaoWDApplication.versionName);
			lHttpGet.addHeader("X-VER", BaoBaoWDApplication.versionCode + "");
			lHttpGet.addHeader("channel", BaoBaoWDApplication.channel);
			String timeString = "" + Calendar.getInstance().getTimeInMillis();
			lHttpGet.addHeader("clientTime", timeString);
			lHttpGet.addHeader(
					"uuToken",
					""+ MD5Util.getInfoMD5(BaoBaoWDApplication.context,
							HttpAppInterface.getGetUid(url)+ BaoBaoWDApplication.deviceId+ timeString));
			Log.d("http2", "url:" + url + "---start");
			HttpResponse response = lClient.execute(lHttpGet);
			saveStatusCode(response.getStatusLine().getStatusCode(), url);
			Log.d("http2", "url:" + url + "---end:"
					+ response.getStatusLine().toString());
			if (200 == response.getStatusLine().getStatusCode())
			{
				InputStream instream = response.getEntity().getContent();
				Header contentEncoding = response
						.getFirstHeader("Content-Encoding");
				if (contentEncoding != null
						&& contentEncoding.getValue().equalsIgnoreCase("gzip"))
				{
					instream = new GZIPInputStream(instream);
				}
				BufferedReader reader = new BufferedReader(
						new InputStreamReader(instream, "UTF-8"));
				String sResponse;
				while ((sResponse = reader.readLine()) != null)
				{
					s = s.append(sResponse);
				}
				lResultDTO = GsonHelper
						.gsonToObj(s.toString(), ResultDTO.class);
				JSONObject jsonObject = new JSONObject(s.toString());
				lResultDTO.setResult(jsonObject.get("result").toString());
			}
		}
		catch (Exception e)
		{
			LogFile.SaveExceptionLog(e);
		}
		finally
		{
			saveResultlog(s.toString(), url);
		}
		return lResultDTO;
	}

	
	
	
	
	
	
	
	
	
}
