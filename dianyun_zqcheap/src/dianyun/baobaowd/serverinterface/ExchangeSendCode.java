package dianyun.baobaowd.serverinterface;

import org.apache.http.message.BasicNameValuePair;
import android.content.Context;

public class ExchangeSendCode extends HttpAppInterface
{

	public ExchangeSendCode( long uid, String token)
	{
		super(null);
		url = EXCHANGESENDCODE_URL;
		lNameValuePairs.add(new BasicNameValuePair("uid", String.valueOf(uid)));
		lNameValuePairs.add(new BasicNameValuePair("token", token));
	}
}
