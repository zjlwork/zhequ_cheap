package dianyun.baobaowd.serverinterface;

public class GetFans extends HttpAppInterface
{

	public GetFans(long uid, String token, long queryUid, int pagesize,
			long seqId)
	{
		super(null);
		url = GETFANS_URL + "?uid=" + uid + "&token=" + token + "&queryUid="
				+ queryUid + "&pagesize=" + pagesize + "&seqId=" + seqId;
	}
}
