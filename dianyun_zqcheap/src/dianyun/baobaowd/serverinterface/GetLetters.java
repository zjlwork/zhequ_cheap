package dianyun.baobaowd.serverinterface;

public class GetLetters extends HttpAppInterface
{

	public GetLetters(long uid, String token, int pagesize, long lastMessageTime)
	{
		super(null);
		url = GETLETTERS_URL + "?uid=" + uid + "&token=" + token + "&pagesize="
				+ pagesize + "&lastMessageTime=" + lastMessageTime;
	}
}
