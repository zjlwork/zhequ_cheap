package dianyun.baobaowd.serverinterface;

public class GetBoardQuestions extends HttpAppInterface
{

	public GetBoardQuestions(long uid, String token, int pagesize,
			int curPage, long minSeqId, long maxSeqId,long boardId,String city,int bbStatus,String babyBirthday)
	{
		super(null);
		url = GETBOARDQUESTIONS_URL + "?uid=" + uid + "&token=" + token
				+ "&pagesize=" + pagesize + "&curPage=" + curPage
				+ "&minSeqId=" + minSeqId + "&maxSeqId=" + maxSeqId
				+"&boardId=" + boardId 
				+"&city="+city+"&bbStatus="+bbStatus+"&babyBirthday="+babyBirthday;
	}
}
