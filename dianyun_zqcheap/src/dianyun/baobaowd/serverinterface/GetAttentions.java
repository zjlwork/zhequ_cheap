package dianyun.baobaowd.serverinterface;

public class GetAttentions extends HttpAppInterface
{

	public GetAttentions(long uid, String token, long queryUid, int pagesize,
			long seqId)
	{
		super(null);
		url = GETFOLLOWS_URL + "?uid=" + uid + "&token=" + token + "&queryUid="
				+ queryUid + "&pagesize=" + pagesize + "&seqId=" + seqId;
	}
}
