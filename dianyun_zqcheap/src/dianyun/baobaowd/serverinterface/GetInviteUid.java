package dianyun.baobaowd.serverinterface;

public class GetInviteUid extends HttpAppInterface
{

	public GetInviteUid(long uid, String token)
	{
		super(null);
		url = GETINVITEUID_URL + "?uid=" + uid + "&token=" + token;
	}
}
