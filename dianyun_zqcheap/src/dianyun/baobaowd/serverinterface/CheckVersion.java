package dianyun.baobaowd.serverinterface;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Calendar;
import java.util.zip.GZIPInputStream;

import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;

import android.content.Context;

import com.dianyun.netsdk.MD5Util;

import dianyun.baobaowd.data.VersionData;
import dianyun.baobaowd.gson.GsonHelper;
import dianyun.baobaowd.help.LogFile;
import dianyun.baobaowd.util.GobalConstants;
import dianyun.zqcheap.application.BaoBaoWDApplication;

public class CheckVersion extends HttpAppInterface
{

	private Context mContext;

	public CheckVersion(Context context)
	{
		super(context);
		this.mContext = context;
		url = GobalConstants.URL.YOYOBASE + GobalConstants.URL.CHECKVERSIONURL;
//		url = "http://garden.test.keep.im/yoyo/index.php?m=Versions&a=android";
	}

	public VersionData connect()
	{
		StringBuilder s = new StringBuilder();
		saveConnectlog(url);
		try
		{
			HttpGet lHttpGet = new HttpGet(url);
			
			lHttpGet.addHeader("Accept-Encoding", "gzip");
			lHttpGet.addHeader("device", "android");
			lHttpGet.addHeader("deviceId", BaoBaoWDApplication.deviceId);
			lHttpGet.addHeader("ver", BaoBaoWDApplication.versionName);
			lHttpGet.addHeader("X-VER",BaoBaoWDApplication.versionCode+"");
			lHttpGet.addHeader("channel", BaoBaoWDApplication.channel);
			lHttpGet.addHeader("X-APPID", String.valueOf(GobalConstants.AppID.DIANYUN_CHAIFENID));
			
			String timeString=""+Calendar.getInstance().getTimeInMillis();
			lHttpGet.addHeader("clientTime",timeString );
			
			lHttpGet.addHeader("uuToken", ""+MD5Util.getInfoMD5(BaoBaoWDApplication.context,getGetUid(url)+BaoBaoWDApplication.deviceId+timeString));
			
			HttpResponse response = lClient.execute(lHttpGet);
			saveStatusCode(response.getStatusLine().getStatusCode(), url);
			if (200 == response.getStatusLine().getStatusCode())
			{
				InputStream instream = response.getEntity().getContent();
				Header contentEncoding = response
						.getFirstHeader("Content-Encoding");
				if (contentEncoding != null
						&& contentEncoding.getValue().equalsIgnoreCase("gzip"))
				{
					instream = new GZIPInputStream(instream);
				}
				BufferedReader reader = new BufferedReader(
						new InputStreamReader(instream, "UTF-8"));
				String sResponse;
				while ((sResponse = reader.readLine()) != null)
				{
					s = s.append(sResponse);
				}
				return GsonHelper.gsonToObj(s.toString(), VersionData.class);
			}
		}
		catch (Exception e)
		{
			LogFile.SaveExceptionLog(e);
		}
		finally
		{
			saveResultlog(s.toString(), url);
		}
		return null;
	}
}
