package dianyun.baobaowd.serverinterface;

import org.apache.http.message.BasicNameValuePair;

public class FavTopic extends HttpAppInterface
{

	public FavTopic(long uid, String token, String topicId)
	{
		super(null);
		url = FAVTOPIC_URL;
		lNameValuePairs.add(new BasicNameValuePair("uid", String.valueOf(uid)));
		lNameValuePairs.add(new BasicNameValuePair("token", token));
		lNameValuePairs.add(new BasicNameValuePair("topicId", topicId));
	}
}
