package dianyun.baobaowd.serverinterface;

import java.io.ByteArrayOutputStream;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.ByteArrayBody;
import org.apache.http.entity.mime.content.StringBody;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import dianyun.baobaowd.help.LogFile;

public class UserAvatarSet extends HttpAppInterface
{

	public UserAvatarSet(long uid, String token, String newFile)
	{
		super(null);
		url = USERAVATARSET_URL;
		try
		{
			reqEntity = new MultipartEntity(
					HttpMultipartMode.BROWSER_COMPATIBLE, null,
					Charset.forName("UTF-8"));
			Bitmap bm = BitmapFactory.decodeFile(newFile);
			if (null != bm)
			{
				ByteArrayOutputStream bos = new ByteArrayOutputStream();
				bm.compress(CompressFormat.JPEG, 100, bos);
				byte[] data = bos.toByteArray();
				ByteArrayBody bab = new ByteArrayBody(data, newFile);
				reqEntity.addPart("newFile", bab);
			}
			reqEntity.addPart("uid", new StringBody(String.valueOf(uid),
					Charset.forName("UTF-8")));
			reqEntity.addPart("token",
					new StringBody(token, Charset.forName("UTF-8")));
		}
		catch (UnsupportedEncodingException e)
		{
			LogFile.SaveExceptionLog(e);
		}
	}
}
