package dianyun.baobaowd.serverinterface;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.ByteArrayBody;
import org.apache.http.entity.mime.content.StringBody;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.text.TextUtils;
import dianyun.baobaowd.help.LogFile;
import dianyun.baobaowd.util.BitmapCache;

public class UploadAttachment extends HttpAppInterface
{

	private byte[] getFileByteArray(String filePath)
	{
		byte[] res = null;
		if (TextUtils.isEmpty(filePath))
		{
			return res;
		}
		File file = new File(filePath);
		if (file.isFile() && file.exists())
		{
			FileInputStream ins;
			try
			{
				ins = new FileInputStream(file);
				res = new byte[ins.available()];
				ins.read(res);
			}
			catch (IOException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return res;
	}

	public UploadAttachment(long uid, String token, String filePath)
	{
		super(null);
		url = UPLOADATTACHMENT_URL;
		try
		{
			reqEntity = new MultipartEntity(
					HttpMultipartMode.BROWSER_COMPATIBLE, null,
					Charset.forName("UTF-8"));
			byte[] data = null;
			String fileName = String.valueOf(uid);
			if (filePath.toLowerCase().endsWith(".gif"))
			{
				// gif
				fileName += ".gif";
				data = getFileByteArray(filePath);
			}
			else
			{
				Bitmap bm = BitmapCache.revitionImageSize(filePath);
				if (null != bm)
				{
					ByteArrayOutputStream bos = new ByteArrayOutputStream();
					if (filePath.endsWith(".png") || filePath.endsWith(".PNG"))
					{
						bm.compress(CompressFormat.PNG, 100, bos);
					}
					else if (filePath.endsWith(".jpg")
							|| filePath.endsWith(".JPG"))
					{
						bm.compress(CompressFormat.JPEG, 100, bos);
					}
					else if (filePath.endsWith(".webp")
							|| filePath.endsWith(".WEBP"))
					{
						bm.compress(CompressFormat.PNG, 100, bos);
					}
					else
					{
						bm.compress(CompressFormat.PNG, 100, bos);
					}
					int index = filePath.lastIndexOf(".");
					if (index > 0)
					{
						// 说明有后缀名
						if (filePath.toLowerCase().endsWith(".png")
								|| filePath.toLowerCase().endsWith(".jpg")
								|| filePath.toLowerCase().endsWith(".jpeg")
								|| filePath.toLowerCase().endsWith(".bmp"))
						{
							fileName += filePath.substring(index);
						}
						else
						{
							// 除此之外 全部需要讲后缀名变换城 。jpg
							fileName += ".png";
						}
					}
					else
					{
						// 没有后缀名需要讲后缀名变换城 。jpg
						fileName += ".png";
					}
					data = bos.toByteArray();
				}
			}
			if (data != null && data.length > 0)
			{
				ByteArrayBody bab = new ByteArrayBody(data, fileName);
				reqEntity.addPart("newFile", bab);
			}
			reqEntity.addPart("uid", new StringBody(String.valueOf(uid),
					Charset.forName("UTF-8")));
			reqEntity.addPart("token",
					new StringBody(token, Charset.forName("UTF-8")));
		}
		catch (UnsupportedEncodingException e)
		{
			LogFile.SaveExceptionLog(e);
		}
	}
}
