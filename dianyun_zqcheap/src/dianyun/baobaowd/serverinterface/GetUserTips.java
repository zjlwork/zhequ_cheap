package dianyun.baobaowd.serverinterface;

public class GetUserTips extends HttpAppInterface
{

	public GetUserTips(long uid, String token, int pagesize, long minTipsId)
	{
		super(null);
		url = GETUSERTIPS_URL + "?uid=" + uid + "&token=" + token
				+ "&pagesize=" + pagesize + "&minTipsId=" + minTipsId;
	}
}
