package dianyun.baobaowd.serverinterface;

import org.apache.http.message.BasicNameValuePair;
import android.content.Context;

public class ShareArticle extends HttpAppInterface
{

	public ShareArticle(Context context, long uid, String token)
	{
		super(context);
		url = SHAREARTICLE_URL;
		lNameValuePairs.add(new BasicNameValuePair("uid", String.valueOf(uid)));
		lNameValuePairs.add(new BasicNameValuePair("token", token));
	}
}
