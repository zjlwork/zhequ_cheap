package dianyun.baobaowd.serverinterface;

import org.apache.http.message.BasicNameValuePair;

public class FavArticle extends HttpAppInterface
{

	public FavArticle(long uid, String token, String articleId)
	{
		super(null);
		url = FAVARTICLE_URL;
		lNameValuePairs.add(new BasicNameValuePair("uid", String.valueOf(uid)));
		lNameValuePairs.add(new BasicNameValuePair("token", token));
		lNameValuePairs.add(new BasicNameValuePair("articleId", articleId));
	}
}
