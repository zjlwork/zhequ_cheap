package dianyun.baobaowd.serverinterface;

import org.apache.http.message.BasicNameValuePair;
import android.content.Context;

public class CheckIn extends HttpAppInterface
{

	public CheckIn(Context context, long uid, String token)
	{
		super(context);
		url = CHECKIN_URL;
		lNameValuePairs.add(new BasicNameValuePair("uid", String.valueOf(uid)));
		lNameValuePairs.add(new BasicNameValuePair("token", token));
	}
}
