package dianyun.baobaowd.serverinterface;

import org.apache.http.message.BasicNameValuePair;

public class Report extends HttpAppInterface
{

	public Report(long uid, String token, long toUid, String content)
	{
		super(null);
		url = REPORT_URL;
		lNameValuePairs.add(new BasicNameValuePair("uid", String.valueOf(uid)));
		lNameValuePairs.add(new BasicNameValuePair("token", token));
		lNameValuePairs.add(new BasicNameValuePair("toUid", String
				.valueOf(toUid)));
		lNameValuePairs.add(new BasicNameValuePair("content", content));
	}
}
