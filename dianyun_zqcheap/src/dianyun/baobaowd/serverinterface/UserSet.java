package dianyun.baobaowd.serverinterface;

import org.apache.http.message.BasicNameValuePair;

public class UserSet extends HttpAppInterface
{

	public UserSet(long uid, String token, byte gender, byte babyGender,
			String babyBirthday, String nickname, String city)
	{
		super(null);
		url = USERSET_URL;
		lNameValuePairs.add(new BasicNameValuePair("uid", String.valueOf(uid)));
		lNameValuePairs.add(new BasicNameValuePair("token", token));
		lNameValuePairs.add(new BasicNameValuePair("gender", String
				.valueOf(gender)));
		lNameValuePairs.add(new BasicNameValuePair("babyGender", String
				.valueOf(babyGender)));
		lNameValuePairs
				.add(new BasicNameValuePair("babyBirthday", babyBirthday));
		lNameValuePairs.add(new BasicNameValuePair("nickname", nickname));
		lNameValuePairs.add(new BasicNameValuePair("city", city));
	}
}
