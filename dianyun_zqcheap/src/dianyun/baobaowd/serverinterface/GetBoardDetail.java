package dianyun.baobaowd.serverinterface;

public class GetBoardDetail extends HttpAppInterface
{

	public GetBoardDetail(long uid, String token, long boardId)
	{
		super(null);
		url = GETBOARDDETAIL_URL + "?uid=" + uid + "&token=" + token + "&boardId="
				+ boardId ;
	}
}
