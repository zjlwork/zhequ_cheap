package dianyun.baobaowd.serverinterface;

public class GetOnlineUserCount extends HttpAppInterface
{

	public GetOnlineUserCount(long uid, String token)
	{
		super(null);
		url = GETONLINEUSERCOUNT_URL + "?uid=" + uid + "&token=" + token;
	}
}
