package dianyun.baobaowd.serverinterface;

public class GetSearch extends HttpAppInterface
{

	public GetSearch(long uid, String token, String keywords, int curPage,int pagesize)
	{
		super(null);
		url = GETSEARCH_URL + "?uid=" + uid + "&token=" + token + "&keywords="
				+ keywords + "&curPage=" + curPage+"&pagesize=" + pagesize;
	}
}
