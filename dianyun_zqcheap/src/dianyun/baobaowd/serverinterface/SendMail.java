package dianyun.baobaowd.serverinterface;

import org.apache.http.message.BasicNameValuePair;

public class SendMail extends HttpAppInterface
{

	public SendMail(String email)
	{
		super(null);
		url = SENDMAIL_URL;
		lNameValuePairs.add(new BasicNameValuePair("email", email));
	}
}
