package dianyun.baobaowd.serverinterface;

public class GetInviteUserCount extends HttpAppInterface
{

	public GetInviteUserCount(long uid, String token)
	{
		super(null);
		url = GETINVITEUSERCOUNT_URL + "?uid=" + uid + "&token=" + token;
	}
}
