package dianyun.baobaowd.serverinterface;

import org.apache.http.message.BasicNameValuePair;

public class GetGuestUserInfo extends HttpAppInterface
{

	public GetGuestUserInfo(String deviceId)
	{
		super(null);
		url = GETGUESTUSERINFO_URL;
		lNameValuePairs.add(new BasicNameValuePair("deviceId", deviceId));
	}
}
