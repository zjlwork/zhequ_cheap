package dianyun.baobaowd.serverinterface;

public class GetNewestCount extends HttpAppInterface
{

	public GetNewestCount(long uid, String token, long answerMaxSeqId,
			long questionMaxSeqId, long appreciationMaxSeqId,
			long messageMaxSeqId, long accAnswerMaxSeqId, long postMaxSeqId,
			long mailMaxSeqId, long feedMaxSeqId)
	{
		super(null);
		url = GETNEWESTCOUNT_URL + "?uid=" + uid + "&token=" + token
				+ "&answerMaxSeqId=" + answerMaxSeqId + "&questionMaxSeqId="
				+ questionMaxSeqId + "&appreciationMaxSeqId="
				+ appreciationMaxSeqId + "&messageMaxSeqId=" + messageMaxSeqId
				+ "&accAnswerMaxSeqId=" + accAnswerMaxSeqId + "&postMaxSeqId="
				+ postMaxSeqId + "&mailMaxSeqId=" + mailMaxSeqId
				+ "&feedMaxSeqId=" + feedMaxSeqId;
	}
}
