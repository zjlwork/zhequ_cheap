package dianyun.baobaowd.serverinterface;

public class GetOrders extends HttpAppInterface
{

	public GetOrders(long uid, String token,int sendStatus, int curPage, int pagesize)
	{
		super(null);
		url = GETORDERS_URL + "?uid=" + uid + "&token=" + token+"&sendStatus=" + sendStatus + "&pagesize="
				+ pagesize + "&curPage=" + curPage;
	}
}
