
package dianyun.baobaowd.serverinterface;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.zip.GZIPInputStream;

import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.json.JSONObject;

import android.content.Context;
import android.net.Uri;
import android.text.TextUtils;

import com.dianyun.netsdk.MD5Util;

import dianyun.baobaowd.data.User;
import dianyun.baobaowd.dto.ResultDTO;
import dianyun.baobaowd.gson.GsonHelper;
import dianyun.baobaowd.help.LogFile;
import dianyun.baobaowd.util.GobalConstants;
import dianyun.baobaowd.util.UserHelper;
import dianyun.zqcheap.application.BaoBaoWDApplication;

public abstract class HttpAppInterface
{
	protected DefaultHttpClient lClient;
	protected List<NameValuePair> lNameValuePairs;
	protected MultipartEntity reqEntity = null;
	protected String url;
	protected Context context;
	private boolean isInTimer;

	public boolean isInTimer()
	{
		return isInTimer;
	}

	public void setInTimer(boolean isInTimer)
	{
		this.isInTimer = isInTimer;
	}

	public HttpAppInterface(Context context)
	{
		this.context = context;
		HttpParams httpParameters;
		int timeoutConnection = 60000;
		int timeoutSocket = 60000;
		httpParameters = new BasicHttpParams();// Set the timeout in
												// milliseconds until a
												// connection is established.
		HttpConnectionParams.setConnectionTimeout(httpParameters,
				timeoutConnection);// Set the default socket timeout
									// (SO_TIMEOUT) // in milliseconds which is
									// the timeout for waiting for data.
		HttpConnectionParams.setSoTimeout(httpParameters, timeoutSocket);
		lClient = new DefaultHttpClient(httpParameters);
		lNameValuePairs = new ArrayList<NameValuePair>();
	}
	
	
	
	
	
	private ResultDTO connect(boolean getConnect){
		try
		{
				String connectGetStr = connectGetStr(getConnect);
				ResultDTO lResultDTO = GsonHelper.gsonToObj(connectGetStr,
						ResultDTO.class);
				JSONObject jsonObject = new JSONObject(connectGetStr);
				lResultDTO.setResult(jsonObject.get("result").toString());
				return lResultDTO;
		}
		catch (Exception e)
		{
			LogFile.SaveExceptionLog(e);
		}
		return null;
	}
	public ResultDTO getConnect(){
		return connect(true);
	}
	public ResultDTO postConnect(){
		return connect(false);
	}
	
	public String getWeatherConnect(){
		return connectGetStr(true);
	}
	

	/*public ResultDTO getConnect()
	{
		saveConnectlog(url);
		StringBuilder s = new StringBuilder();
		try
		{
			HttpGet lHttpGet = new HttpGet(url);
			lHttpGet.addHeader("Accept-Encoding", "gzip");
			lHttpGet.addHeader("device", "android");
			lHttpGet.addHeader("deviceId", BaoBaoWDApplication.deviceId);
			lHttpGet.addHeader("ver", BaoBaoWDApplication.versionName);
			lHttpGet.addHeader("X-VER",BaoBaoWDApplication.versionCode+"");
			lHttpGet.addHeader("channel", BaoBaoWDApplication.channel);
			lHttpGet.addHeader("X-APPID", String.valueOf(GobalConstants.AppID.DIANYUNSHOP));
			String timeString=""+Calendar.getInstance().getTimeInMillis();
			lHttpGet.addHeader("clientTime",timeString );
			lHttpGet.addHeader("uuToken", ""+MD5Util.getInfoMD5(BaoBaoWDApplication.context,getGetUid(url)+BaoBaoWDApplication.deviceId+timeString));
			
			Log.d("http", "url:"+url+"---start");
			HttpResponse response = lClient.execute(lHttpGet);
			saveStatusCode(response.getStatusLine().getStatusCode(), url);
			Log.d("http", "url:"+url+"---end:"+response.getStatusLine().toString());
			if (200 == response.getStatusLine().getStatusCode())
			{
				InputStream instream = response.getEntity().getContent();
				Header contentEncoding = response
						.getFirstHeader("Content-Encoding");
				if (contentEncoding != null
						&& contentEncoding.getValue().equalsIgnoreCase("gzip"))
				{
					instream = new GZIPInputStream(instream);
				}
				BufferedReader reader = new BufferedReader(
						new InputStreamReader(instream, "UTF-8"));
				String sResponse;
				while ((sResponse = reader.readLine()) != null)
				{
					s = s.append(sResponse);
				}
				ResultDTO lResultDTO = GsonHelper.gsonToObj(s.toString(),
						ResultDTO.class);
				JSONObject jsonObject = new JSONObject(s.toString());
				lResultDTO.setResult(jsonObject.get("result").toString());
				return lResultDTO;
			}
		}
		catch (Exception e)
		{
			LogFile.SaveExceptionLog(e);
		}
		finally
		{
			saveResultlog(s.toString(), url);
		}
		return null;
	}
*/
	/*public String getWeatherConnect()
	{
		saveConnectlog(url);
		StringBuilder s = new StringBuilder();
		try
		{
			HttpGet lHttpGet = new HttpGet(url);
			lHttpGet.addHeader("Accept-Encoding", "gzip");
			lHttpGet.addHeader("device", "android");
			lHttpGet.addHeader("deviceId", BaoBaoWDApplication.deviceId);
			lHttpGet.addHeader("ver", BaoBaoWDApplication.versionName);
			lHttpGet.addHeader("X-VER",BaoBaoWDApplication.versionCode+"");
			lHttpGet.addHeader("channel", BaoBaoWDApplication.channel);
			lHttpGet.addHeader("X-APPID", String.valueOf(GobalConstants.AppID.DIANYUNSHOP));
			
			String timeString=""+Calendar.getInstance().getTimeInMillis();
			lHttpGet.addHeader("clientTime",timeString );
			
			lHttpGet.addHeader("uuToken", ""+MD5Util.getInfoMD5(BaoBaoWDApplication.context,getGetUid(url)+BaoBaoWDApplication.deviceId+timeString));
			
			
			// lHttpGet.addHeader("Accept-Encoding", "gzip");
			HttpResponse response = lClient.execute(lHttpGet);
			saveStatusCode(response.getStatusLine().getStatusCode(), url);
			if (200 == response.getStatusLine().getStatusCode())
			{
				InputStream instream = response.getEntity().getContent();
				// Header contentEncoding =
				// response.getFirstHeader("Content-Encoding");
				// if (contentEncoding != null &&
				// contentEncoding.getValue().equalsIgnoreCase("gzip")) {
				// instream = new GZIPInputStream(instream);
				// }
				BufferedReader reader = new BufferedReader(
						new InputStreamReader(instream, "UTF-8"));
				String sResponse;
				while ((sResponse = reader.readLine()) != null)
				{
					s = s.append(sResponse);
				}
				return s.toString();
			}
		}
		catch (Exception e)
		{
			LogFile.SaveExceptionLog(e);
		}
		finally
		{
			saveResultlog(s.toString(), url);
		}
		return null;
	}*/

	/*public ResultDTO postConnect()
	{
		saveConnectlog(url);
		StringBuilder s = new StringBuilder();
		try
		{
			HttpPost lHttpPost = new HttpPost(url);
			lHttpPost.addHeader("Accept-Encoding", "gzip");
			lHttpPost.addHeader("device", "android");
			lHttpPost.addHeader("deviceId", BaoBaoWDApplication.deviceId);
			lHttpPost.addHeader("ver", BaoBaoWDApplication.versionName);
			lHttpPost.addHeader("X-VER",BaoBaoWDApplication.versionCode+"");
			lHttpPost.addHeader("channel", BaoBaoWDApplication.channel);
			lHttpPost.addHeader("X-APPID", String.valueOf(GobalConstants.AppID.DIANYUNSHOP));
			String timeString=""+Calendar.getInstance().getTimeInMillis();
			lHttpPost.addHeader("clientTime",timeString );
			
			lHttpPost.addHeader("uuToken", ""+MD5Util.getInfoMD5(BaoBaoWDApplication.context,getPostUid(lNameValuePairs)+BaoBaoWDApplication.deviceId+timeString));
			if (reqEntity != null)
				lHttpPost.setEntity(reqEntity);
			else
				lHttpPost.setEntity(new UrlEncodedFormEntity(lNameValuePairs,
						"UTF-8"));
			HttpResponse response = lClient.execute(lHttpPost);
			saveStatusCode(response.getStatusLine().getStatusCode(), url);
			if (200 == response.getStatusLine().getStatusCode())
			{
				InputStream instream = response.getEntity().getContent();
				Header contentEncoding = response
						.getFirstHeader("Content-Encoding");
				if (contentEncoding != null
						&& contentEncoding.getValue().equalsIgnoreCase("gzip"))
				{
					instream = new GZIPInputStream(instream);
				}
				BufferedReader reader = new BufferedReader(
						new InputStreamReader(instream, "UTF-8"));
				String sResponse;
				while ((sResponse = reader.readLine()) != null)
				{
					s = s.append(sResponse);
				}
				ResultDTO lResultDTO = GsonHelper.gsonToObj(s.toString(),
						ResultDTO.class);
				JSONObject jsonObject = new JSONObject(s.toString());
				lResultDTO.setResult(jsonObject.get("result").toString());
				return lResultDTO;
			}
		}
		catch (Exception e)
		{
			LogFile.SaveExceptionLog(e);
		}
		finally
		{
			saveResultlog(s.toString(), url);
		}
		return null;
	}*/

	
	public void saveConnectlog(String pUrl)
	{
		LogFile.SaveLog("saveConnectlog ===" + pUrl + " lNameValuePairs="
				+ lNameValuePairs);
	}

	public void saveResultlog(String result, String pUrl)
	{
		LogFile.SaveLog("saveResultlog = " + pUrl + " lNameValuePairs="
				+ lNameValuePairs + "result ===" + result);
	}

	public void saveStatusCode(int statusCode, String pUrl)
	{
		LogFile.SaveLog("saveStatusCode = " + pUrl + " lNameValuePairs="
				+ lNameValuePairs + "statusCode ===" + statusCode);
	}
	protected final static String USERAVATARSET_URL = GobalConstants.URL.DEFAULTPREFIX
			+ GobalConstants.URL.USERAVATARSET;
	protected final static String USERBGSET_URL = GobalConstants.URL.DEFAULTPREFIX
			+ GobalConstants.URL.USERBGSET;
	protected final static String USERSET_URL = GobalConstants.URL.DEFAULTPREFIX
			+ GobalConstants.URL.USERSET;
	protected final static String QQLOGIN_URL = GobalConstants.URL.DEFAULTPREFIX
			+ GobalConstants.URL.QQLOGIN;
	protected final static String BBREGISTER_URL = GobalConstants.URL.DEFAULTPREFIX
			+ GobalConstants.URL.BBREGISTER;
	protected final static String BBLOGIN_URL = GobalConstants.URL.DEFAULTPREFIX
			+ GobalConstants.URL.BBLOGIN;
	protected final static String TAOBAOLOGIN_URL = GobalConstants.URL.DEFAULTPREFIX
			+ GobalConstants.URL.TAOBAOLOGIN;
	protected final static String CHECKIN_URL = GobalConstants.URL.DEFAULTPREFIX
			+ GobalConstants.URL.CHECKIN;
	protected final static String SHAREARTICLE_URL = GobalConstants.URL.DEFAULTPREFIX
			+ GobalConstants.URL.SHAREARTICLE;
	protected final static String SINAWEIBOLOGIN_URL = GobalConstants.URL.DEFAULTPREFIX
			+ GobalConstants.URL.SINAWEIBOLOGIN;
	protected final static String SENDQUESTION_URL = GobalConstants.URL.DEFAULTPREFIX
			+ GobalConstants.URL.SENDQUESTION;
	protected final static String SENDANSWER_URL = GobalConstants.URL.DEFAULTPREFIX
			+ GobalConstants.URL.SENDANSWER;
	protected final static String SENDTHANKS_URL = GobalConstants.URL.DEFAULTPREFIX
			+ GobalConstants.URL.SENDTHANKS;
	protected final static String SENDADOPT_URL = GobalConstants.URL.DEFAULTPREFIX
			+ GobalConstants.URL.SENDADOPT;
	protected final static String SENDMAIL_URL = GobalConstants.URL.DEFAULTPREFIX
			+ GobalConstants.URL.SENDMAIL;
	protected final static String SENDLETTER_URL = GobalConstants.URL.DEFAULTPREFIX
			+ GobalConstants.URL.SENDLETTER;
	protected final static String FAVQUESTION_URL = GobalConstants.URL.DEFAULTPREFIX
			+ GobalConstants.URL.FAVQUESTION;
	protected final static String FAVARTICLE_URL = GobalConstants.URL.DEFAULTPREFIX
			+ GobalConstants.URL.FAVARTICLE;
	protected final static String FAVTOPIC_URL = GobalConstants.URL.DEFAULTPREFIX
			+ GobalConstants.URL.FAVTOPIC;
	protected final static String CANCELFAVTOPIC_URL = GobalConstants.URL.DEFAULTPREFIX
			+ GobalConstants.URL.CANCELFAVTOPIC;
	protected final static String CANCELFAVARTICLE_URL = GobalConstants.URL.DEFAULTPREFIX
			+ GobalConstants.URL.CANCELFAVARTICLE;
	protected final static String READACTICLE_URL = GobalConstants.URL.DEFAULTPREFIX
			+ GobalConstants.URL.READACTICLE;
	protected final static String REPORT_URL = GobalConstants.URL.DEFAULTPREFIX
			+ GobalConstants.URL.REPORT;
	protected final static String CANCELFAVQUESTION_URL = GobalConstants.URL.DEFAULTPREFIX
			+ GobalConstants.URL.CANCELFAVQUESTION;
	protected final static String GETTODAYARTICE_URL = GobalConstants.URL.DEFAULTPREFIX
			+ GobalConstants.URL.GETTODAYARTICE;
	protected final static String GETDAYARTICE_URL = GobalConstants.URL.DEFAULTPREFIX
			+ GobalConstants.URL.GETDAYARTICE;
	protected final static String GETARTICES_URL = GobalConstants.URL.DEFAULTPREFIX
			+ GobalConstants.URL.GETARTICES;
	protected final static String GETVIEWPICTURES_URL = GobalConstants.URL.DEFAULTPREFIX
			+ GobalConstants.URL.GETVIEWPICTURES;
	protected final static String GETBOARDS_URL = GobalConstants.URL.DEFAULTPREFIX
			+ GobalConstants.URL.GETBOARDS;
	protected final static String GETFAVARTICES_URL = GobalConstants.URL.DEFAULTPREFIX
			+ GobalConstants.URL.GETFAVARTICES;
	protected final static String GETFOLLOWS_URL = GobalConstants.URL.DEFAULTPREFIX
			+ GobalConstants.URL.GETFOLLOWS;
	protected final static String GETFOLLOWCOUNT_URL = GobalConstants.URL.DEFAULTPREFIX
			+ GobalConstants.URL.GETFOLLOWCOUNT;
	protected final static String GETFANS_URL = GobalConstants.URL.DEFAULTPREFIX
			+ GobalConstants.URL.GETFANS;
	protected final static String GETLETTERS_URL = GobalConstants.URL.DEFAULTPREFIX
			+ GobalConstants.URL.GETLETTERS;
	protected final static String GETMAILS_URL = GobalConstants.URL.DEFAULTPREFIX
			+ GobalConstants.URL.GETMAILS;
	protected final static String GETUSERTIPS_URL = GobalConstants.URL.DEFAULTPREFIX
			+ GobalConstants.URL.GETUSERTIPS;
	protected final static String GETALlTIPS_URL = GobalConstants.URL.DEFAULTPREFIX
			+ GobalConstants.URL.GETALlTIPS;
	protected final static String GETSHARE_URL = GobalConstants.URL.DEFAULTPREFIX
			+ GobalConstants.URL.GETSHARE;
	
	protected final static String GETQUESTIONS_URL = GobalConstants.URL.DEFAULTPREFIX
			+ GobalConstants.URL.GETQUESTIONS;
	protected final static String GETRECOMMENDQUESTIONS_URL = GobalConstants.URL.DEFAULTPREFIX
			+ GobalConstants.URL.GETRECOMMENDQUESTIONS;
	protected final static String UPLOADATTACHMENT_URL = GobalConstants.URL.DEFAULTPREFIX
			+ GobalConstants.URL.UPLOADATTACHMENT;
	protected final static String GETUSERRANK_URL = GobalConstants.URL.DEFAULTPREFIX
			+ GobalConstants.URL.GETUSERRANK;
	protected final static String GETUSERYESTERDAYRANK_URL = GobalConstants.URL.DEFAULTPREFIX
			+ GobalConstants.URL.GETUSERYESTERDAYRANK;
	protected final static String GETANSWERS_URL = GobalConstants.URL.DEFAULTPREFIX
			+ GobalConstants.URL.GETANSWERS;
	protected final static String GETRACEQUESTIONS_URL = GobalConstants.URL.DEFAULTPREFIX
			+ GobalConstants.URL.GETRACEQUESTIONS;
	protected final static String GETHOTQUESTIONS_URL = GobalConstants.URL.DEFAULTPREFIX
			+ GobalConstants.URL.GETHOTQUESTIONS;
	protected final static String GETQUESTIONDETAIL_URL = GobalConstants.URL.DEFAULTPREFIX
			+ GobalConstants.URL.GETQUESTIONDETAIL;
	protected final static String GETGIFTS_URL = GobalConstants.URL.DEFAULTPREFIX
			+ GobalConstants.URL.GETGIFTS;
    /**
     * 话费的URL
     */
    protected final static String GETHUAFEI_URL = GobalConstants.URL.DEFAULTPREFIX
            + GobalConstants.URL.GETHUAFEILIST;

	protected final static String GETAPPS_URL = GobalConstants.URL.DEFAULTPREFIX
			+ GobalConstants.URL.GETAPPS;
	protected final static String GETHOTGIFTS_URL = GobalConstants.URL.DEFAULTPREFIX
			+ GobalConstants.URL.GETHOTGIFTS;
	protected final static String EXCHANGEGIFTS_URL = GobalConstants.URL.DEFAULTPREFIX
			+ GobalConstants.URL.EXCHANGEGIFTS;
	protected final static String GETMYQUESTIONS_URL = GobalConstants.URL.DEFAULTPREFIX
			+ GobalConstants.URL.GETMYQUESTIONS;
	protected final static String GETMYTOPICS_URL = GobalConstants.URL.DEFAULTPREFIX
			+ GobalConstants.URL.GETMYTOPICS;
	protected final static String GETMPTOPICS_URL = GobalConstants.URL.DEFAULTPREFIX
			+ GobalConstants.URL.GETMPTOPICS;
	protected final static String GETMYTOPICREPLYS_URL = GobalConstants.URL.DEFAULTPREFIX
			+ GobalConstants.URL.GETMYTOPICREPLYS;
	protected final static String GETMYRECEIVETOPICREPLYS_URL = GobalConstants.URL.DEFAULTPREFIX
			+ GobalConstants.URL.GETMYRECEIVETOPICREPLYS;
	protected final static String GETFAVTOPICS_URL = GobalConstants.URL.DEFAULTPREFIX
			+ GobalConstants.URL.GETFAVTOPICS;
	protected final static String GETFAVQUESTIONS_URL = GobalConstants.URL.DEFAULTPREFIX
			+ GobalConstants.URL.GETFAVQUESTIONS;
	protected final static String GETONEQUESTION_URL = GobalConstants.URL.DEFAULTPREFIX
			+ GobalConstants.URL.GETONEQUESTION;
	protected final static String GETCOUNSELLORS_URL = GobalConstants.URL.DEFAULTPREFIX
			+ GobalConstants.URL.GETCOUNSELLORS;
	protected final static String GETOTHERQUESTIONS_URL = GobalConstants.URL.DEFAULTPREFIX
			+ GobalConstants.URL.GETOTHERQUESTIONS;
	protected final static String GETOTHENOBESTRQUESTIONS_URL = GobalConstants.URL.DEFAULTPREFIX
			+ GobalConstants.URL.GETOTHENOBESTRQUESTIONS;
	protected final static String GETMYRECEIVEREPLY_URL = GobalConstants.URL.DEFAULTPREFIX
			+ GobalConstants.URL.GETMYRECEIVEREPLY;
	protected final static String GETMYBESTANSWER_URL = GobalConstants.URL.DEFAULTPREFIX
			+ GobalConstants.URL.GETMYBESTANSWER;
	protected final static String GETOTHERBESTANSWER_URL = GobalConstants.URL.DEFAULTPREFIX
			+ GobalConstants.URL.GETOTHERBESTANSWER;
	protected final static String GETMYANSWERS_URL = GobalConstants.URL.DEFAULTPREFIX
			+ GobalConstants.URL.GETMYANSWERS;
	protected final static String GETOTHERANSWERS_URL = GobalConstants.URL.DEFAULTPREFIX
			+ GobalConstants.URL.GETOTHERANSWERS;
	protected final static String GETMYRECEIVETHANKS_URL = GobalConstants.URL.DEFAULTPREFIX
			+ GobalConstants.URL.GETMYRECEIVETHANKS;
	protected final static String GETOTHERRECEIVETHANKS_URL = GobalConstants.URL.DEFAULTPREFIX
			+ GobalConstants.URL.GETOTHERRECEIVETHANKS;
	protected final static String GETUSERINFO_URL = GobalConstants.URL.DEFAULTPREFIX
			+ GobalConstants.URL.GETUSERINFO;
	protected final static String GETGUESTUSERINFO_URL = GobalConstants.URL.DEFAULTPREFIX
			+ GobalConstants.URL.GETGUESTUSERINFO;
	protected final static String GETUSERLASTWEEKRANK_URL = GobalConstants.URL.DEFAULTPREFIX
			+ GobalConstants.URL.GETUSERLASTWEEKRANK;
	protected final static String GETXMPPUSER_URL = GobalConstants.URL.DEFAULTPREFIX
			+ GobalConstants.URL.GETXMPPUSER;
	protected final static String DELETEQUESTION_URL = GobalConstants.URL.DEFAULTPREFIX
			+ GobalConstants.URL.DELETEQUESTION;
	protected final static String DELETEANSWER_URL = GobalConstants.URL.DEFAULTPREFIX
			+ GobalConstants.URL.DELETEANSWER;
	protected final static String GETNEWESTCOUNT_URL = GobalConstants.URL.DEFAULTPREFIX
			+ GobalConstants.URL.GETNEWESTCOUNT;
	protected final static String GETMESSAGELIST_URL = GobalConstants.URL.DEFAULTPREFIX
			+ GobalConstants.URL.GETMESSAGELIST;
	protected final static String GETRECEIVERADDRESS_URL = GobalConstants.URL.DEFAULTPREFIX
			+ GobalConstants.URL.GETRECEIVERADDRESS;
	protected final static String SETRECEIVERADDRESS_URL = GobalConstants.URL.DEFAULTPREFIX
			+ GobalConstants.URL.SETRECEIVERADDRESS;
	protected final static String GETINVITECOUNT_URL = GobalConstants.URL.DEFAULTPREFIX
			+ GobalConstants.URL.GETINVITECOUNT;
	protected final static String GETINVITEUID_URL = GobalConstants.URL.DEFAULTPREFIX
			+ GobalConstants.URL.GETINVITEUID;
	
	
	protected final static String GETINVITEUSERCOUNT_URL = GobalConstants.URL.DEFAULTPREFIX
			+ GobalConstants.URL.GETINVITEUSERCOUNT;
	protected final static String GETTOPICSBYREPLY_URL = GobalConstants.URL.DEFAULTPREFIX
			+ GobalConstants.URL.GETTOPICSBYREPLY;
	protected final static String GETTOPICSBYSEND_URL = GobalConstants.URL.DEFAULTPREFIX
			+ GobalConstants.URL.GETTOPICSBYSEND;
	protected final static String GETTOPICSBYMARROW_URL = GobalConstants.URL.DEFAULTPREFIX
			+ GobalConstants.URL.GETTOPICSBYMARROW;
	protected final static String CREATEFOLLOW_URL = GobalConstants.URL.DEFAULTPREFIX
			+ GobalConstants.URL.CREATEFOLLOW;
	protected final static String DELETEFOLLOW_URL = GobalConstants.URL.DEFAULTPREFIX
			+ GobalConstants.URL.DELETEFOLLOW;
	protected final static String CREATEPOSTTOPIC_URL = GobalConstants.URL.DEFAULTPREFIX
			+ GobalConstants.URL.CREATEPOSTTOPIC;
	protected final static String CREATEPOSTREPLY_URL = GobalConstants.URL.DEFAULTPREFIX
			+ GobalConstants.URL.CREATEPOSTREPLY;
	protected final static String GETTOPICREPLY_URL = GobalConstants.URL.DEFAULTPREFIX
			+ GobalConstants.URL.GETTOPICREPLY;
	protected final static String GETLZTOPICREPLY_URL = GobalConstants.URL.DEFAULTPREFIX
			+ GobalConstants.URL.GETLZTOPICREPLY;
	protected final static String GETTOPICDETAIL_URL = GobalConstants.URL.DEFAULTPREFIX
			+ GobalConstants.URL.GETTOPICDETAIL;
	protected final static String LOGACTIVE_URL = GobalConstants.URL.DEFAULTPREFIX
			+ GobalConstants.URL.LOGACTIVE;
	protected final static String GETTOPICSBYTIPS_URL = GobalConstants.URL.DEFAULTPREFIX
			+ GobalConstants.URL.GETTOPICSBYTIPS;
	protected final static String GETUSERADOPTCOUNT_URL = GobalConstants.URL.DEFAULTPREFIX
			+ GobalConstants.URL.GETUSERADOPTCOUNT;
	protected final static String DELETELETTER_URL = GobalConstants.URL.DEFAULTPREFIX
			+ GobalConstants.URL.DELETELETTER;
	protected final static String DELETEUSERALLLETTER_URL = GobalConstants.URL.DEFAULTPREFIX
			+ GobalConstants.URL.DELETEUSERALLLETTER;
	protected final static String SHIELDUSER_URL = GobalConstants.URL.DEFAULTPREFIX
			+ GobalConstants.URL.SHIELDUSER;
	protected final static String CANCELSHIELDUSER_URL = GobalConstants.URL.DEFAULTPREFIX
			+ GobalConstants.URL.CANCELSHIELDUSER;
	protected final static String GETSHIELDUSERS_URL = GobalConstants.URL.DEFAULTPREFIX
			+ GobalConstants.URL.GETSHIELDUSERS;
	protected final static String GETCONFIG_URL = GobalConstants.URL.DEFAULTPREFIX
			+ GobalConstants.URL.GETCONFIG;
	protected final static String GETONLINEUSERCOUNT_URL = GobalConstants.URL.DEFAULTPREFIX
			+ GobalConstants.URL.GETONLINEUSERCOUNT;
	protected final static String APPAWARD_URL = GobalConstants.URL.DEFAULTPREFIX
			+ GobalConstants.URL.APPAWARD;
	protected final static String PHOTOAUTO_URL = GobalConstants.URL.DEFAULTPREFIX
			+ GobalConstants.URL.PHOTOAUTO;
	protected final static String VERIFYPHOTO_URL = GobalConstants.URL.DEFAULTPREFIX
			+ GobalConstants.URL.VERIFYPHOTO;
	protected final static String PHOTOREGISTE_URL = GobalConstants.URL.DEFAULTPREFIX
			+ GobalConstants.URL.PHOTOREGISTE;
	protected final static String PHOTOCHANGEPW_URL = GobalConstants.URL.DEFAULTPREFIX
			+ GobalConstants.URL.PHOTOCHANGEPW;
	protected final static String GETSUBJECTS_URL = GobalConstants.URL.DEFAULTPREFIX
			+ GobalConstants.URL.GETSUBJECTS;
	protected final static String GETPORTALS_URL = GobalConstants.URL.DEFAULTPREFIX
			+ GobalConstants.URL.GETPORTALS;
	protected final static String GETSUBJECTDETAIL_URL = GobalConstants.URL.DEFAULTPREFIX
			+ GobalConstants.URL.GETSUBJECTDETAIL;
	protected final static String GETFEEDS_URL = GobalConstants.URL.DEFAULTPREFIX
			+ GobalConstants.URL.GETFEEDS;
	protected final static String GETMEDALS_URL = GobalConstants.URL.DEFAULTPREFIX
			+ GobalConstants.URL.GETMEDALS;
	protected final static String GETATTENTIONUSERS_URL = GobalConstants.URL.DEFAULTPREFIX
			+ GobalConstants.URL.GETATTENTIONUSERS;
	protected final static String GETSTARS_URL = GobalConstants.URL.DEFAULTPREFIX
			+ GobalConstants.URL.GETSTARS;
	protected final static String GETNEWSTARS_URL = GobalConstants.URL.DEFAULTPREFIX
			+ GobalConstants.URL.GETNEWSTARS;
	protected final static String GETUSERLEVEL_URL = GobalConstants.URL.DEFAULTPREFIX
			+ GobalConstants.URL.GETUSERLEVEL;
	protected final static String CREATEFEELING_URL = GobalConstants.URL.DEFAULTPREFIX
			+ GobalConstants.URL.CREATEFEELING;
	protected final static String GETFEELINGLIST_URL = GobalConstants.URL.DEFAULTPREFIX
			+ GobalConstants.URL.GETFEELINGLIST;
	protected final static String GETTALENTS_URL = GobalConstants.URL.DEFAULTPREFIX
			+ GobalConstants.URL.GETTALENTS;
	protected final static String GETMAXSEQID_URL = GobalConstants.URL.DEFAULTPREFIX
			+ GobalConstants.URL.GETMAXSEQID;
	protected final static String GETBOARDQUESTIONS_URL = GobalConstants.URL.DEFAULTPREFIX
			+ GobalConstants.URL.GETBOARDQUESTIONS;
	protected final static String GETBOARDDETAIL_URL = GobalConstants.URL.DEFAULTPREFIX
			+ GobalConstants.URL.GETBOARDDETAIL;
	
	protected final static String BINDPHONE_URL = GobalConstants.URL.DEFAULTPREFIX
			+ GobalConstants.URL.BINDPHONE;
	protected final static String BINDPHONESENDCODE_URL = GobalConstants.URL.DEFAULTPREFIX
			+ GobalConstants.URL.BINDPHONESENDCODE;
	
	protected final static String BINDPAY_URL = GobalConstants.URL.DEFAULTPREFIX
			+ GobalConstants.URL.BINDPAY;
	
	protected final static String BINDPAYSENDCODE_URL = GobalConstants.URL.DEFAULTPREFIX
			+ GobalConstants.URL.BINDPAYSENDCODE;
	protected final static String EXCHANGESENDCODE_URL = GobalConstants.URL.DEFAULTPREFIX
			+ GobalConstants.URL.EXCHANGESENDCODE;
	
	protected final static String GETUSERBINDINFO_URL = GobalConstants.URL.DEFAULTPREFIX
			+ GobalConstants.URL.GETUSERBINDINFO;
	
	protected final static String GETORDERS_URL = GobalConstants.URL.DEFAULTPREFIX
			+ GobalConstants.URL.GETORDERS;
	protected final static String EXCHANGEMONGY_URL = GobalConstants.URL.DEFAULTPREFIX
			+ GobalConstants.URL.EXCHANGEMONGY;
	protected final static String GETAPPLYLIST_URL = GobalConstants.URL.DEFAULTPREFIX
			+ GobalConstants.URL.GETAPPLYLIST;
	protected final static String GETAPPLYLIST_URL1 = GobalConstants.URL.DEFAULTPREFIX
			+ GobalConstants.URL.GETAPPLYLIST1;
	
	
	protected final static String GETWELCOMEPICTURE_URL = GobalConstants.URL.DEFAULTPREFIX
			+ GobalConstants.URL.GETWELCOMEPICTURE;
	protected final static String GETADS_URL = GobalConstants.URL.DEFAULTPREFIX
			+ GobalConstants.URL.GETADS;
	protected final static String GETRANK_URL = GobalConstants.URL.DEFAULTPREFIX
			+ GobalConstants.URL.GETRANK;
	protected final static String GETCHECKININFO_URL = GobalConstants.URL.DEFAULTPREFIX
			+ GobalConstants.URL.GETCHECKININFO;
	protected final static String GETDIGS_URL = GobalConstants.URL.DEFAULTPREFIX
			+ GobalConstants.URL.GETDIGS;
//	protected final static String GETMIAOSHAPICTURES_URL = GobalConstants.URL.DEFAULTPREFIX
//			+ GobalConstants.URL.GETMIAOSHAPICTURES;
	
	
	
	
	protected final static String GETSEARCH_URL = GobalConstants.URL.SEARCHBASE
			+ GobalConstants.URL.GETSEARCH;
	protected final static String GETSEARCHWORDS_URL = GobalConstants.URL.SEARCHBASE
			+ GobalConstants.URL.GETSEARCHWORDS;
	
	protected final static String SHAREORDER_URL = GobalConstants.URL.DEFAULTPREFIX
			+ GobalConstants.URL.SHAREORDER;
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	public static String getPostUid(List<NameValuePair> lNameValuePairs){
		if(lNameValuePairs!=null){
			for(int i=0;i<lNameValuePairs.size();i++){
				if(lNameValuePairs.get(i).getName().equals("uid")){
					return lNameValuePairs.get(i).getValue();
				}
			}
		}
		return "0";
	}
	public static String getGetUid(String url){
		if(TextUtils.isEmpty(url))return "0";
		Uri lUri = Uri.parse(url);
		String uid = lUri.getQueryParameter("uid");
		if(TextUtils.isEmpty(uid))
			uid = "0";
		return uid;
	}
	
	private HttpGet getHttpGet(){
		HttpGet lHttpGet = new HttpGet(url);
		User luser = UserHelper.getUser();
		String bbBirthday = "";
		String bbStatus = "";
		if (luser != null)
		{
			bbBirthday = luser.getBabyBirthday();
			bbStatus = "" + UserHelper.getBBStatus(bbBirthday);
		}
		lHttpGet.addHeader("bbBirthday", bbBirthday);
		lHttpGet.addHeader("bbStatus", bbStatus);
		lHttpGet.addHeader("Accept-Encoding", "gzip");
		lHttpGet.addHeader("device", "android");
		lHttpGet.addHeader("deviceId", BaoBaoWDApplication.deviceId);
		lHttpGet.addHeader("ver", BaoBaoWDApplication.versionName);
		lHttpGet.addHeader("X-VER",BaoBaoWDApplication.versionCode+"");
		lHttpGet.addHeader("channel", BaoBaoWDApplication.channel);
		lHttpGet.addHeader("X-APPID", String.valueOf(GobalConstants.AppID.DIANYUN_CHAIFENID));
		String timeString=""+Calendar.getInstance().getTimeInMillis();
		lHttpGet.addHeader("clientTime",timeString );
		lHttpGet.addHeader("uuToken", ""+MD5Util.getInfoMD5(BaoBaoWDApplication.context,getGetUid(url)+BaoBaoWDApplication.deviceId+timeString));
		return lHttpGet;
	}
	private HttpPost getHttpPost() throws UnsupportedEncodingException{
		HttpPost lHttpPost=null;
			lHttpPost = new HttpPost(url);
			User luser = UserHelper.getUser();
			String bbBirthday = "";
			String bbStatus = "";
			if (luser != null)
			{
				bbBirthday = luser.getBabyBirthday();
				bbStatus = "" + UserHelper.getBBStatus(bbBirthday);
			}
			lHttpPost.addHeader("bbBirthday", bbBirthday);
			lHttpPost.addHeader("bbStatus", bbStatus);
			lHttpPost.addHeader("Accept-Encoding", "gzip");
			lHttpPost.addHeader("device", "android");
			lHttpPost.addHeader("deviceId", BaoBaoWDApplication.deviceId);
			lHttpPost.addHeader("ver", BaoBaoWDApplication.versionName);
			lHttpPost.addHeader("X-VER",BaoBaoWDApplication.versionCode+"");
			lHttpPost.addHeader("channel", BaoBaoWDApplication.channel);
			lHttpPost.addHeader("X-APPID", String.valueOf(GobalConstants.AppID.DIANYUN_CHAIFENID));
			String timeString=""+Calendar.getInstance().getTimeInMillis();
			lHttpPost.addHeader("clientTime",timeString );
			lHttpPost.addHeader("uuToken", ""+MD5Util.getInfoMD5(BaoBaoWDApplication.context,getPostUid(lNameValuePairs)
					+BaoBaoWDApplication.deviceId+timeString));
			if (reqEntity != null)
				lHttpPost.setEntity(reqEntity);
			else
				lHttpPost.setEntity(new UrlEncodedFormEntity(lNameValuePairs,"UTF-8"));
		return lHttpPost;
	}
	
	
	//唯一核心
	private String connectGetStr(boolean getConnect){
		
		StringBuilder s = new StringBuilder();
		try
		{
			saveConnectlog(url);
			HttpResponse response = null;
			if(getConnect)
				response = lClient.execute(getHttpGet());
			else
				response = lClient.execute(getHttpPost());
			
			saveStatusCode(response.getStatusLine().getStatusCode(), url);
			if (200 == response.getStatusLine().getStatusCode())
			{
				InputStream instream = response.getEntity().getContent();
				Header contentEncoding = response
						.getFirstHeader("Content-Encoding");
				if (contentEncoding != null
						&& contentEncoding.getValue().equalsIgnoreCase("gzip"))
				{
					instream = new GZIPInputStream(instream);
				}
				BufferedReader reader = new BufferedReader(
						new InputStreamReader(instream, "UTF-8"));
				String sResponse;
				while ((sResponse = reader.readLine()) != null)
				{
					s = s.append(sResponse);
				}
				return s.toString();
			}
		}
		catch (Exception e)
		{
			LogFile.SaveExceptionLog(e);
		}
		finally
		{
			saveResultlog(s.toString(), url);
		}
		return null;
	
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
}
