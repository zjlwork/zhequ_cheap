package dianyun.baobaowd.serverinterface;

import org.apache.http.message.BasicNameValuePair;
import android.content.Context;

public class BindPaySendCode extends HttpAppInterface
{

	public BindPaySendCode(Context context, long uid, String token,
			String phone)
	{
		super(context);
		url = BINDPAYSENDCODE_URL;
		lNameValuePairs.add(new BasicNameValuePair("uid", String.valueOf(uid)));
		lNameValuePairs.add(new BasicNameValuePair("token", token));
		lNameValuePairs.add(new BasicNameValuePair("phone", phone));
	}
}
