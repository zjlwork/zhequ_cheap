package dianyun.baobaowd.serverinterface;

import org.apache.http.message.BasicNameValuePair;
import android.content.Context;

public class BindPhoneSendCode extends HttpAppInterface
{

	public BindPhoneSendCode(Context context, long uid, String token,
			String phone)
	{
		super(context);
		url = BINDPHONESENDCODE_URL;
		lNameValuePairs.add(new BasicNameValuePair("uid", String.valueOf(uid)));
		lNameValuePairs.add(new BasicNameValuePair("token", token));
		lNameValuePairs.add(new BasicNameValuePair("phone", phone));
	}
}
