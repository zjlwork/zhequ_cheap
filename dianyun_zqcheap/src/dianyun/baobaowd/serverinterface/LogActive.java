package dianyun.baobaowd.serverinterface;

import org.apache.http.message.BasicNameValuePair;

public class LogActive extends HttpAppInterface
{

	public LogActive(long uid, String deviceId)
	{
		super(null);
		url = LOGACTIVE_URL;
		lNameValuePairs.add(new BasicNameValuePair("uid", String.valueOf(uid)));
		lNameValuePairs.add(new BasicNameValuePair("deviceId", deviceId));
		System.out.println("LogActive----------------");
	}
}
