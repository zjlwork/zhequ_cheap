package dianyun.baobaowd.serverinterface;

import org.apache.http.message.BasicNameValuePair;

public class CancelFavTopic extends HttpAppInterface
{

	public CancelFavTopic(long uid, String token, String topicId)
	{
		super(null);
		url = CANCELFAVTOPIC_URL;
		lNameValuePairs.add(new BasicNameValuePair("uid", String.valueOf(uid)));
		lNameValuePairs.add(new BasicNameValuePair("token", token));
		lNameValuePairs.add(new BasicNameValuePair("topicId", topicId));
	}
}
