package dianyun.baobaowd.serverinterface;

public class GetInviteCount extends HttpAppInterface
{

	public GetInviteCount(long uid, String token)
	{
		super(null);
		url = GETINVITECOUNT_URL + "?uid=" + uid + "&token=" + token;
	}
}
