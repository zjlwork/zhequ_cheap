package dianyun.baobaowd.serverinterface;

public class GetDigs extends HttpAppInterface
{

	public GetDigs(long uid, String token, int pagesize, int  curPage,int digType)
	{
		super(null);
		url = GETDIGS_URL  +"?uid=" + uid + "&token=" + token + "&pagesize="
				+ pagesize + "&curPage=" + curPage+ "&digType=" + digType;
	}
}
