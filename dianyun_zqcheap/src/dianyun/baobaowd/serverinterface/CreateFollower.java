package dianyun.baobaowd.serverinterface;

import org.apache.http.message.BasicNameValuePair;

public class CreateFollower extends HttpAppInterface
{

	public CreateFollower(long uid, String token, long toUid)
	{
		super(null);
		url = CREATEFOLLOW_URL;
		lNameValuePairs.add(new BasicNameValuePair("uid", String.valueOf(uid)));
		lNameValuePairs.add(new BasicNameValuePair("token", token));
		lNameValuePairs.add(new BasicNameValuePair("toUid", String
				.valueOf(toUid)));
	}
}
