package dianyun.baobaowd.serverinterface;

public class GetXmppUser extends HttpAppInterface
{

	public GetXmppUser(long uid, String token)
	{
		super(null);
		url = GETXMPPUSER_URL + "?uid=" + uid + "&token=" + token;
	}
}
