package dianyun.baobaowd.entity;

import java.io.Serializable;
import java.util.List;

/**
 * Created by youlianjie on 14-12-17. cate和item必有一个为空
 */
public class CateItem implements Serializable
{

	// ----分类项----
	/**
	 * 分类id
	 */
	public Long cateId;
	/**
	 * 分类名称
	 */
	public String name;
	/**
	 * 分类图片，如果pic为空则显示name
	 */
	public String pic;
	/**
	 * 分类类型，1：轮播图，2：裤腰带，3:单品，4：专辑，5：更多，6：页面下部列表
	 */
	public Integer xType;
	// ----商品项----
	/**
	 * 淘宝商品混淆ID
	 */
	public String tbItemId;
	/**
	 * 卖家昵称
	 */
	public String sellerNick;
	/**
	 * 商品图片，目前为一张，考虑向后兼容，放在一个list里面
	 */
	public List<String> pics;
	/**
	 * 商品名称
	 */
	public String title;
	/**
	 * 商品原价
	 */
	public String price;
	/**
	 * 商品促销价
	 */
	public String umpPrice;
	/**
	 * 商品状态，0：商品下架，1：可用
	 */
	public Integer tbItemStatus;
	/**
	 * 商品剩余下架时间
	 */
	public Integer leftTime;
	/**
	 * 返金币数量
	 */
	public Integer coins;
	/**
	 * 排序字段
	 */
	public Integer sequence;
	/**
	 * 点击类型，1：专辑列表，2：专辑详情，3：商品分类列表，4：webview 7.邀请好友
	 */
	public Integer clickType;
	/**
	 * 只有clickType=4时才会用到
	 */
	public String clickUrl;
	/**
	 * logo图片
	 */
	public String logoPic;

	/**
	 * 即将跳转的页面的标题
	 */
	public String pageTitle;
	/**
	 * 是否为淘宝客商品，0：不是，1是
	 */
	public Integer isTk;
	/**
	 * 淘宝客pid
	 */
	public String taobaoPid;
	/**
	 * 更多项下面包含的专辑数量
	 */
    public Integer containsNum;
    
    //1：集市店，2：商城
    public Integer itemType=0;
    
    
    public String location;
    public String monthlySales;
    
    
    
    
  //时间场次编号
    public long timeId=0L;
    //类型，1:0元秒杀；2：折扣秒杀
    public int type=0;
    //数量
    public int amount=0;
    
    
    
    public static final int ITEMTYPE_NORMAL =1;
    public static final int ITEMTYPE_STORE =2;
    
    public static final int TYPE_LYMS =1;
    public static final int TYPE_ZKMS =2;


}
