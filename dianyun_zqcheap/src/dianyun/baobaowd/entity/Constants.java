package dianyun.baobaowd.entity;

public class Constants
{

	public static final String TAG="TAE";
	public static final int REQUEST_PICK_PHOTO = 100;
	public static final int REQUEST_TAKE_PHOTO = 110;
	public static final int PICK_PHOTO_MAX = 7;
	public static final String EXTRA_NAME = "data";
	public static final String OBJECT_EXTRA_NAME = "data1";

	public static final int REQUEST_LOGIN = 120;
	public static final int RESULT_LOGIN_SUCCESS = 121;
}
