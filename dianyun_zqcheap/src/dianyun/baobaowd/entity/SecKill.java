package dianyun.baobaowd.entity;

import java.util.List;

public class SecKill {
	
	 //本场秒杀是否已经开始
    public boolean isStart;
    //如果秒杀尚未开始，代表还有多少秒，本秒杀开始
    public long startLeftTime=0;
    //如果秒杀已经开始，代表还有多少秒，本秒杀结束
    public long endLeftTime=0;
    //秒杀开始时间
    public String startDate;
    public List<CateItem> itemList;
    
  //本地字段 （倒计时完成）
  	public boolean isEnd;
}
