package dianyun.baobaowd.interfaces;

public interface PopupReplyClickCallback
{

	public void handleOnImageOnClick();

	public void handleOnVoiceOnClick();
	
	public void handleOnDismiss();
	
	
}
