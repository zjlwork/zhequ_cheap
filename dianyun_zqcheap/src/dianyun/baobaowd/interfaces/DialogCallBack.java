package dianyun.baobaowd.interfaces;

public interface DialogCallBack
{

	void clickSure();

	void clickCancel();
}
