package dianyun.baobaowd.interfaces;

public interface ShareWindowCallBack
{

	void shareToQQFriend();

	void sinaweiboshare();

	void shareToQQSpace();

	void shareWeixinFriend();

	void shareWeixinSpace();
}
