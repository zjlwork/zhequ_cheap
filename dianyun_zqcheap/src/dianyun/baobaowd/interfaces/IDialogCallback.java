package dianyun.baobaowd.interfaces;

/**
 * Created by pc on 2015/5/7.
 */
public interface IDialogCallback {

    public void onConfirmOnClick();
    public void onCancelOnClick();
}
