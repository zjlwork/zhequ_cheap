package dianyun.baobaowd.interfaces;

public interface ReportShieldDeleteWindowCallBack
{

	void report();

	void shield();

	void delete();
}
