package dianyun.zqcheap.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import dianyun.baobaowd.entity.CateItem;
import dianyun.baobaowd.entity.Constants;
import dianyun.baobaowd.util.NotificationHelper;
import dianyun.zqcheap.activity.ShowTaeDetailActivity;


public class TaeReceiver extends BroadcastReceiver
{

	@Override
	public void onReceive(Context context, Intent intent)
	{

		if (intent.getAction().equals(
				NotificationHelper.ACTION_RECEIVE_GOODS))
		{
			Object obj = intent.getSerializableExtra(Constants.EXTRA_NAME);
			if (obj != null)
			{
				CateItem goodsItem = (CateItem) obj;
				if (goodsItem != null&&goodsItem.tbItemId!=null&&goodsItem.taobaoPid!=null&&goodsItem.isTk!=null&&goodsItem.itemType!=null)
				{
					Intent lIntent = new Intent(context,ShowTaeDetailActivity.class);
					lIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
							| Intent.FLAG_ACTIVITY_NEW_TASK);
					lIntent.putExtra("tbItemId", goodsItem.tbItemId);
					lIntent.putExtra("taobaoPid", goodsItem.taobaoPid);
					lIntent.putExtra("isTk", goodsItem.isTk);
					lIntent.putExtra("itemType", goodsItem.itemType);
					context.startActivity(lIntent);
				}
			}
		}
	}
	
}
