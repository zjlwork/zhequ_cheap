package dianyun.zqcheap.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.widget.Toast;
import dianyun.baobaowd.data.User;
import dianyun.baobaowd.dto.ResultDTO;
import dianyun.baobaowd.serverinterface.AppAward;
import dianyun.baobaowd.util.BroadCastHelper;
import dianyun.baobaowd.util.GobalConstants;
import dianyun.baobaowd.util.UserHelper;
import dianyun.zqcheap.R;

public class MyInstalledReceiver extends BroadcastReceiver
{

	Context mContext;

	@Override
	public void onReceive(Context context, Intent intent)
	{
		mContext = context;
		/*if (intent.getAction().equals("android.intent.action.PACKAGE_ADDED"))
		{
			String packageNameStr = intent.getDataString();
			String packageName = packageNameStr.substring(packageNameStr
					.lastIndexOf(":") + 1);
			File lFile = new File(FileHelper.getDownloadAppsPath()
					+ packageName + GobalConstants.Suffix.PIC_SUFFIX_APK);
			if (ListHelper.isExist(DownloadAppHelper.downloadAPkList,
					packageName) || lFile.isFile())
			{
				new AppAwardThread(packageName).start();
			}
		}*/
	}

	class AppAwardThread extends Thread
	{

		private Handler handler;
		private ResultDTO resultDTO;
		String appId;

		public AppAwardThread(String appId)
		{
			handler = new Handler();
			this.appId = appId;
		}

		@Override
		public void run()
		{
			User mUser =UserHelper.getUser();
			resultDTO = new AppAward(mContext, mUser.getUid(),
					mUser.getToken(), appId).postConnect();
			handler.post(new Runnable()
			{

				@Override
				public void run()
				{
					if (resultDTO != null && resultDTO.getCode().equals("0"))
					{
						int gold = Integer.parseInt(resultDTO.getResult()
								.toString());
						String toast = String.format(
								mContext.getString(R.string.awardgold), gold);
						Toast.makeText(mContext, toast, Toast.LENGTH_SHORT)
								.show();
						BroadCastHelper.sendRefreshAppBroadcast(mContext,
								appId, GobalConstants.AllStatusYesOrNo.YES);
						BroadCastHelper.sendRefreshUserGoldBroadcast(mContext,
								gold);
					}
					else
					{
						// BroadCastHelper.sendRefreshAppBroadcast(mContext,
						// appId,GobalConstants.AllStatusYesOrNo.NO);
					}
				}
			});
		}
	}
}