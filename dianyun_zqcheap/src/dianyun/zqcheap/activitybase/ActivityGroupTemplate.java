package dianyun.zqcheap.activitybase;

/**
 * ActivityGroup
 * 
 * @author piers.xie
 */
public class ActivityGroupTemplate implements ActivityGroupCallBack
{

	private static final String TAG = "ActivityE";
	private static final boolean isLog = true;

	@Override
	public void doInActivity(BaseActivity activity, int layoutResID)
	{
		activity.setContentView(layoutResID);
		try
		{
			activity.findView();
			activity.initData();
			activity.initListener();
		}
		catch (Exception e)
		{
			e.printStackTrace();
			// LogFile.SaveExceptionLog();
		}
	}
}
