package dianyun.zqcheap.activitybase;

public interface ActivityGroupCallBack
{

	void doInActivity(BaseActivity activity, int layoutResID);
}
