package dianyun.zqcheap.activitybase;

import java.util.ArrayList;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.text.TextUtils;
import android.widget.Toast;

import com.alibaba.sdk.android.callback.CallbackContext;
import com.tencent.connect.share.QzoneShare;
import com.tencent.mm.sdk.openapi.IWXAPI;
import com.tencent.mm.sdk.openapi.SendMessageToWX;
import com.tencent.mm.sdk.openapi.WXAPIFactory;
import com.tencent.tauth.IUiListener;
import com.tencent.tauth.Tencent;
import com.tencent.tauth.UiError;
import com.umeng.analytics.MobclickAgent;

import dianyun.baobaowd.qq.QQConstants;
import dianyun.baobaowd.qq.QQPreferenceUtil;
import dianyun.baobaowd.util.BroadCastHelper;
import dianyun.baobaowd.util.GobalConstants;
import dianyun.baobaowd.util.TencentHelper;
import dianyun.baobaowd.util.ThirdPartHelper;
import dianyun.baobaowd.util.ToastHelper;
import dianyun.zqcheap.R;
import dianyun.zqcheap.application.BaoBaoWDApplication;

public abstract class BaseActivity extends FragmentActivity /*
															 * implements
															 * IWXAPIEventHandler
															 */
{

	

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		initTencent();
		
	};

	@Override
	protected void onDestroy()
	{
		super.onDestroy();
	}

	/**
	 * 获取模板
	 * 
	 * @return
	 */
	protected ActivityGroupTemplate getTemplate()
	{
		return new ActivityGroupTemplate();
	}

	/**
	 * 获取组件对象
	 */
	public void findView()
	{
	};

	/**
	 * 初始化组件数
	 */
	public void initData()
	{
	};

	/**
	 * 初始化组件监听器
	 */
	public void initListener()
	{
	};

	@Override
	protected void onResume()
	{
		super.onResume();
		MobclickAgent.onResume(BaseActivity.this);
	}

	@Override
	protected void onPause()
	{
		super.onPause();
		MobclickAgent.onPause(BaseActivity.this);
	}

	public void startActivity(Class<?> clazz)
	{
		startActivity(new Intent(this, clazz));
	}

	public BaoBaoWDApplication getBaobaoApplication()
	{
		return (BaoBaoWDApplication) this.getApplicationContext();
	}
	public static Tencent mTencent;

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data)
	{
		super.onActivityResult(requestCode, resultCode, data);
		System.out
				.println("===================onActivityResult===========baseactivity = ");
//		// sso
//		if (requestCode == 32973)
//		{
//			SinaWeiboUtil.getInstance(BaseActivity.this).authCallBack(
//					requestCode, resultCode, data);
//		}
		if (mTencent != null)
		{
			mTencent.onActivityResult(requestCode, resultCode, data);
			
		}
//		 CallbackContext.onActivityResult(requestCode, resultCode, data);
	}

	

	
	public  boolean ready(Context context)
	{
		if (mTencent == null)
		{
			return false;
		}
		boolean ready = mTencent.isSessionValid()
				&& mTencent.getQQToken().getOpenId() != null;
		if (!ready)
			Toast.makeText(context, "login and get openId first, please!",
					Toast.LENGTH_SHORT).show();
		return ready;
	}
	
	
	
	public void initTencent()
	{
		
		if(mTencent==null)
		mTencent = Tencent.createInstance(ThirdPartHelper.QQAPP_ID,
				getApplicationContext());
		String access_token  = QQPreferenceUtil.getInstance(BaseActivity.this)
				.getString(QQConstants.ACCESS_TOKEN, "");
		long expires_in = QQPreferenceUtil.getInstance(BaseActivity.this)
				.getLong(QQConstants.EXPIRES_IN, 0);
		String openId = QQPreferenceUtil.getInstance(BaseActivity.this)
				.getString(QQConstants.OPENID, "");
		
		if(!TextUtils.isEmpty(access_token))
			mTencent.setAccessToken(access_token, String.valueOf(expires_in));
		if(!TextUtils.isEmpty(openId))
			mTencent.setOpenId(openId);
	}
	
	
	public void setAppIdToken(){
		if(mTencent!=null){
			mTencent.setAccessToken(QQPreferenceUtil.getInstance(BaseActivity.this)
			.getString(QQConstants.ACCESS_TOKEN, ""), String
			.valueOf(QQPreferenceUtil.getInstance(BaseActivity.this)
					.getLong(QQConstants.EXPIRES_IN, 0)));
			mTencent.setOpenId(QQPreferenceUtil.getInstance(BaseActivity.this)
			.getString(QQConstants.OPENID, ""));
		}
	}
	
	public void tencentLogout(){
		
		QQPreferenceUtil.getInstance(getApplicationContext())
		.saveString(QQConstants.ACCESS_TOKEN,"");
		QQPreferenceUtil.getInstance(getApplicationContext())
		.saveString(QQConstants.OPENID,"");
		QQPreferenceUtil.getInstance(getApplicationContext())
		.saveLong(QQConstants.EXPIRES_IN,0);
		
		if(mTencent!=null)
		mTencent.logout(getApplicationContext());
	}
	

	// ------------------------------------------------------------------------------------
	public boolean bshareWeixin(String title, String summaryTxt, String url,
			boolean toSpace)
	{
		if(title==null||summaryTxt==null||url==null)return false;
		IWXAPI api = WXAPIFactory.createWXAPI(BaseActivity.this, ThirdPartHelper.WEIXINAPP_ID,
				false);
		api.registerApp(ThirdPartHelper.WEIXINAPP_ID);
		if (summaryTxt.length() > 100)
			summaryTxt = summaryTxt.substring(0, 99);
		SendMessageToWX.Req req = TencentHelper.getShareWeixinReq(title,
				summaryTxt, url,
				BaseActivity.this, toSpace);
		
		boolean result = api.sendReq(req);
		return result;
	}
	
	
	
	
	
	

	public void bshareToQQFriend(String title, String summaryTxt, String url,String urlIcon)
	{
		if(title==null||summaryTxt==null||url==null||urlIcon==null)return ;
		Bundle bundle = TencentHelper.getShareToQQFriendBundle(title,
				urlIcon,
				url, summaryTxt,
				getString(R.string.app_name));
//		mTencent = TencentHelper.getTencent(BaseActivity.this);
		mTencent.shareToQQ(BaseActivity.this, bundle, new IUiListener()
		{

			@Override
			public void onError(UiError arg0)
			{
				System.out.println("sharefailed"+arg0);
			}

			@Override
			public void onComplete(Object arg0)
			{
				ToastHelper.show(BaseActivity.this,
						getString(R.string.sharesuccess));
				BroadCastHelper.sendRefreshMainBroadcast(BaseActivity.this, GobalConstants.RefreshType.SHAREORDERSUCCESS);
			}

			@Override
			public void onCancel()
			{
			}
		});
	}

	public void bshareToQQSpace(String title,String summaryTxt, String url,String urlIcon)
	{
//		mTencent = TencentHelper.getTencent(BaseActivity.this);
		if(title==null||summaryTxt==null||url==null||urlIcon==null)return ;
//		if (mTencent.isSessionValid() && mTencent.getOpenId() != null)
//		{
			final Bundle params = new Bundle();
			params.putInt(QzoneShare.SHARE_TO_QZONE_KEY_TYPE,
					QzoneShare.SHARE_TO_QZONE_TYPE_IMAGE_TEXT);
			params.putString(QzoneShare.SHARE_TO_QQ_TITLE,
					title);
			params.putString(QzoneShare.SHARE_TO_QQ_SUMMARY, summaryTxt);
			params.putString(QzoneShare.SHARE_TO_QQ_TARGET_URL,
					url);
			ArrayList<String> imageUrls = new ArrayList<String>();
			imageUrls.add(urlIcon);
			params.putStringArrayList(QzoneShare.SHARE_TO_QQ_IMAGE_URL,
					imageUrls);
			mTencent.shareToQzone(BaseActivity.this, params, new IUiListener()
			{

				@Override
				public void onError(UiError arg0)
				{
					System.out.println("sharefailed"+arg0);
				}

				@Override
				public void onComplete(Object arg0)
				{
					ToastHelper.show(BaseActivity.this,
							getString(R.string.sharesuccess));
					BroadCastHelper.sendRefreshMainBroadcast(BaseActivity.this, GobalConstants.RefreshType.SHAREORDERSUCCESS);
				}

				@Override
				public void onCancel()
				{
				}
			});
//		}
	}

	
}
