package dianyun.zqcheap.service;

import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.os.PowerManager;

import com.taobao.tae.sdk.TaeSDK;
import com.taobao.tae.sdk.model.Session;

import dianyun.baobaowd.data.User;
import dianyun.baobaowd.data.XiaoMiPushData;
import dianyun.baobaowd.db.LightDBHelper;
import dianyun.baobaowd.help.LogFile;
import dianyun.baobaowd.help.ShopHttpHelper;
import dianyun.baobaowd.util.ConfigHelper;
import dianyun.baobaowd.util.GobalConstants;
import dianyun.baobaowd.util.NetworkStatus;
import dianyun.baobaowd.util.UserHelper;
import dianyun.baobaowd.xiaomipush.RegisterPushHelper;

public class BaoBaoWDService extends Service
{

	private User mUser;
	/*
	 * private ConnectionChangeReceiver mConnReceiver; private void
	 * registerConnReciver() { IntentFilter filter = new
	 * IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION); mConnReceiver =
	 * new ConnectionChangeReceiver(); this.registerReceiver(mConnReceiver,
	 * filter); }
	 */
	private Timer mTimer = new Timer();
	private TimerTask mTask = new TimerTask()
	{

		@Override
		public void run()
		{
			try
			{
				// System.out.println(" 获取最新消息数目----------------------");
				mUser = UserHelper.getUser();
				// 检查guestuser是否是服务端获取的
				checkGuestUser();
				// 注册小米推送
				handlePushData();
				// 获取config 回答悬赏的判断条件
				getConfig();
				// 获取消息角标
				getAllNewReply();
				// 获取勋章列表
//				MedalHelper.cycleGetMedalListByNet(getApplicationContext(),
//						mUser);
				// 获取用户是否绑定淘宝
				checkIsBindTaoBao(BaoBaoWDService.this, mUser);
			}
			catch (Exception e)
			{
				LogFile.SaveExceptionLog(e);
			}
		}
	};

	private void checkIsBindTaoBao(final Context context, final User user)
	{
		if (NetworkStatus.getNetWorkStatus(getApplicationContext()) <= 0)
		{
			return;
		}
		if (user == null || user.getIsGuest() == 1)
		{
			return;
		}
		final Session session = TaeSDK.getSession();
		if (session == null || !session.isLogin())
		{
			return;
		}
		final com.taobao.tae.sdk.model.User taobaoUser = session.getUser();
		if (taobaoUser == null)
		{
			return;
		}
		if (LightDBHelper.getIsBind(context))
		{
			return;
		}
		new Thread()
		{

			@Override
			public void run()
			{
				ShopHttpHelper.bindTaoBaoAndYoYo_Only(context,
						"" + user.getUid(), user.getToken(),
						session.getUserId(), taobaoUser.avatarUrl,
						taobaoUser.nick);
				super.run();
			}
		}.start();
	}

	private void checkGuestUser()
	{
		if (NetworkStatus.getNetWorkStatus(getApplicationContext()) > 0)
		{
			User guestUser = UserHelper.getGuestUser(getApplicationContext());
			if (guestUser != null
					&& guestUser.getUid() == GobalConstants.GUESTUSERUID)
			{
				UserHelper.getNetGuestUser(getApplicationContext());
			}
		}
	}

	private void getAllNewReply()
	{
		if (NetworkStatus.getNetWorkStatus(getApplicationContext()) > 0)
		{
			PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
			boolean screen = pm.isScreenOn();
			boolean isActivityAlive = LightDBHelper
					.getIsMainActivityAlive(BaoBaoWDService.this);
			if (screen && isActivityAlive && mUser != null
					&& mUser.getIsSelf() == GobalConstants.AllStatusYesOrNo.YES)
				ReceiveHelper.getAllNewReply(BaoBaoWDService.this, mUser);
		}
	}

	private void getConfig()
	{
		if (NetworkStatus.getNetWorkStatus(getApplicationContext()) > 0)
		{
			if (LightDBHelper.getTopicDetailPrefix(getApplicationContext()) == null
					|| LightDBHelper.getTopicDetailPrefix(
							getApplicationContext()).equals(""))
				ConfigHelper.getConfigThread(getApplicationContext(), mUser,
						null);
		}
	}

	private void handlePushData()
	{
		if (NetworkStatus.getNetWorkStatus(getApplicationContext()) > 0)
		{
			List<XiaoMiPushData> xiaoMiPushDataList = RegisterPushHelper
					.getUnRegisterFailedPushDataList(getApplicationContext());
			if (xiaoMiPushDataList != null && xiaoMiPushDataList.size() > 0)
			{
				RegisterPushHelper.unSetAliasAndTopics(getApplicationContext());
			}
			else
			{
				RegisterPushHelper.registerAliasAndTopics(
						getApplicationContext(), mUser);
			}
		}
	}

	@Override
	public IBinder onBind(Intent intent)
	{
		return null;
	}

	@Override
	public void onCreate()
	{
		super.onCreate();
		mUser = UserHelper.getUser();
		System.out.println("BaoBaoWDService-----------------onCreate   muser="
				+ mUser);
		// if(mUser!=null&&mUser.getToken()!=null){
		// registerConnReciver();
		// }
		if (mUser != null
				&& mUser.getIsGuest() == GobalConstants.AllStatusYesOrNo.YES)
		{
			LightDBHelper.setPushShare(BaoBaoWDService.this, true);
		}
		try
		{
			mTimer.schedule(mTask, 15000, 15000);
		}
		catch (Exception e)
		{
			LogFile.SaveExceptionLog(e);
		}
		// 配置信息
		getConfig();
		// 获取勋章列表
		// MedalHelper.getMedalListByNet(getApplicationContext(), mUser,true);
	}

	@Override
	public void onDestroy()
	{
		super.onDestroy();
		// if(mConnReceiver!=null){
		// unregisterReceiver(mConnReceiver);
		// mConnReceiver=null;
		// }
		System.out.println("BaoBaoWDService-----------------onDestroy");
		if (mTimer != null)
			mTimer.cancel();
	}

	@Override
	public void onStart(Intent intent, int startId)
	{
		super.onStart(intent, startId);
		System.out.println("BaoBaoWDService-----------------onStart");
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId)
	{
		System.out.println("BaoBaoWDService-----------------onStartCommand");
		return super.onStartCommand(intent, flags, startId);
	}

	@Override
	public boolean onUnbind(Intent intent)
	{
		return super.onUnbind(intent);
	}
	/*
	 * class ConnectionChangeReceiver extends BroadcastReceiver {
	 * 
	 * @Override public void onReceive(final Context context, Intent intent) {
	 * 
	 * if (NetworkStatus.getNetWorkStatus(context) > 0) { //
	 * System.out.println("ConnectionChangeReceiver----------------true");
	 * 
	 * 
	 * } else {
	 * 
	 * } } }
	 */
}
