package dianyun.zqcheap.service;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import dianyun.baobaowd.data.NewestCountResult;
import dianyun.baobaowd.data.User;
import dianyun.baobaowd.db.LightDBHelper;
import dianyun.baobaowd.dto.ResultDTO;
import dianyun.baobaowd.entity.CateItem;
import dianyun.baobaowd.gson.GsonHelper;
import dianyun.baobaowd.serverinterface.GetNewestCount;
import dianyun.baobaowd.util.BroadCastHelper;
import dianyun.baobaowd.util.GobalConstants;
import dianyun.baobaowd.util.NotificationHelper;
import dianyun.baobaowd.util.Utils;
import dianyun.zqcheap.activity.HtmlActivity;
import dianyun.zqcheap.activity.MessageCenterActivity;
import dianyun.zqcheap.activity.OrderRecordActivity;
import dianyun.zqcheap.activity.RecordActivity;
import dianyun.zqcheap.activity.TaskActivity;

public class ReceiveHelper
{


	public static void receiveNewestToastType(Context context, String content,
			int messageType)
	{
		if (!LightDBHelper.getPushShare(context))
		{
			return;
		}
        String title="";
        boolean isShowNotify=true;
		if (messageType == GobalConstants.MessageType.SYSTEMMSG_GOODS)
		{
			// 该消息是 推荐商品的消息 处理逻辑
			// content 此时是一个 商品的对象
			if (!TextUtils.isEmpty(content))
			{
				try
				{
					JSONObject jsobj = new JSONObject(content);
					if (jsobj != null)
					{
						 title = jsobj.getString("title");
						String jsonContent = jsobj.getString("jsonContent");
						CateItem goodsItem = (CateItem) GsonHelper.gsonToObj(
								jsonContent, CateItem.class);
						NotificationHelper.showNotifyDefineSleftById(context,
								title, goodsItem,
								NotificationHelper.RECOMMENT_SHOP_NEWS++);
					}
				}
				catch (JSONException e)
				{
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		else
		{
			Intent lIntent = new Intent();
			if (messageType == GobalConstants.MessageType.SYSTEMMSG_FANLI)
			{
				lIntent.setClass(context, OrderRecordActivity.class);
				lIntent.putExtra("mode", OrderRecordActivity.REBATE);
			}
			else if (messageType == GobalConstants.MessageType.SYSTEMMSG_TIXIAN)
			{
				lIntent.setClass(context, RecordActivity.class);
			}
            else if(messageType==GobalConstants.MessageType.SYSTEMMSG_QIANDAO)
            {
                //qiaodao message
                isShowNotify=(Utils.isNeedToQianDao(context));
                lIntent.setClass(context,TaskActivity.class);
            }
            else if(messageType==GobalConstants.MessageType.SYSTEMMSG_XIADAN)
            {
                lIntent.setClass(context,OrderRecordActivity.class);
            }
            else if(messageType==GobalConstants.MessageType.SYSTEMMSG_ACTIVITY)
            {
                //跳转应用内的Activity 显示一个网页内容
                lIntent.setClass(context, HtmlActivity.class);
                JSONObject jsobj = null;
                try {
                    jsobj = new JSONObject(content);
                    if (jsobj != null) {
                         title = jsobj.getString("title");
                        content=jsobj.getString("content");
                        String jsonContentString = jsobj.getString("jsonContent");
                        JSONObject jsonContent=new JSONObject(jsonContentString);
                       String urlString= jsonContent.getString("url");
                       String pageTitleString= jsonContent.getString("pageTitle");
                        //设置跳转htmlActivity的title 和 跳转链接地址
                        lIntent.putExtra(GobalConstants.Data.URL,urlString);
                        lIntent.putExtra(GobalConstants.Data.TITLE,pageTitleString);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
			else
			{
				lIntent.setClass(context, MessageCenterActivity.class);
			}

			lIntent.putExtra(GobalConstants.Data.FROM,
					GobalConstants.Data.NOTIFY);
			lIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
					| Intent.FLAG_ACTIVITY_NEW_TASK);
            if(messageType!=GobalConstants.MessageType.SYSTEMMSG_ACTIVITY) {
                title = NotificationHelper.convert2MessTitleByMessID(
                        context, messageType);
            }
            if(isShowNotify) {
                NotificationHelper.showNotifyFullyById(context, title, content,
                        lIntent, NotificationHelper.SYSTEMID++);
            }
			// NotificationHelper.showNotifyById(context, content, lIntent,
			// NotificationHelper.SYSTEMID);
		}
	}

	public static void getAllNewReply(Context context, User user)
	{
		
		ResultDTO resultDTO = new GetNewestCount(user.getUid(),
				user.getToken(), 0, 0,0, 0,0, 0, 0,0).getConnect();
		if (resultDTO != null)
		{
			NewestCountResult lNewestCountResult = GsonHelper.gsonToObj(
					resultDTO.getResult(), NewestCountResult.class);


			LightDBHelper.setSystemMsgCount(context,
					lNewestCountResult.getMessageCount());
			LightDBHelper.setFanliCount(context, lNewestCountResult.getFanliCount());
			LightDBHelper.setCashCount(context, lNewestCountResult.getCashCount());
			BroadCastHelper.sendRefreshMainBroadcast(context,
					GobalConstants.RefreshType.ALLNEW);
		}
	}
	
	
	
	
	
	

	
}
