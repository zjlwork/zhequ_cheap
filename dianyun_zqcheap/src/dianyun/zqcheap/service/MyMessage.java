package dianyun.zqcheap.service;

public class MyMessage
{

	// {"jsonContent":"{\"noteFromNickname\":\"发个过\",\"noteFromUid\":38,\"noteText\":\"发个过发起配对\"}"
	// ,"messageId":"d54a6ebe-efc9-427c-8d0b-5ed6693c72fa","messageType":2,"timestamp":1364890694002,"uid":36}
	private String messageId;
	private int messageType;
	private long timestamp;
	private long uid;
	private Object contentObject;
	private Note note;

	
	
	public String show(){
		return "messageId="+messageId+"messageType="+messageType+" timestamp="+timestamp+" uid="+uid;
	}
	MyMessage()
	{
	}

	public Object getContentObject()
	{
		return contentObject;
	}

	public void setContentObject(Object contentObject)
	{
		this.contentObject = contentObject;
	}

	public Note getNote()
	{
		return note;
	}

	public void setNote(Note note)
	{
		this.note = note;
	}

	public String getMessageId()
	{
		return messageId;
	}

	public void setMessageId(String messageId)
	{
		this.messageId = messageId;
	}

	public int getMessageType()
	{
		return messageType;
	}

	public void setMessageType(int messageType)
	{
		this.messageType = messageType;
	}

	public long getTimestamp()
	{
		return timestamp;
	}

	public void setTimestamp(long timestamp)
	{
		this.timestamp = timestamp;
	}

	public long getUid()
	{
		return uid;
	}

	public void setUid(long uid)
	{
		this.uid = uid;
	}
}
