package dianyun.zqcheap.service;

import android.os.Parcel;
import android.os.Parcelable;

public class Note implements Parcelable
{

	private String noteText;
	private Long noteFromUid;
	private String noteFromNickname;

	Note()
	{
	}

	public Long getNoteFromUid()
	{
		return noteFromUid;
	}

	public void setNoteFromUid(Long noteFromUid)
	{
		this.noteFromUid = noteFromUid;
	}

	public String getNoteFromNickname()
	{
		return noteFromNickname;
	}

	public void setNoteFromNickname(String noteFromNickname)
	{
		this.noteFromNickname = noteFromNickname;
	}

	public String getNoteText()
	{
		return noteText;
	}

	public void setNoteText(String noteText)
	{
		this.noteText = noteText;
	}

	private void readFromParcel(Parcel in)
	{
		this.noteText = in.readString();
		this.noteFromUid = in.readLong();
		this.noteFromNickname = in.readString();
	}

	public Note(Parcel source)
	{
		readFromParcel(source);
	}
	public static final Parcelable.Creator<Note> CREATOR = new Parcelable.Creator<Note>()
	{

		@Override
		public Note[] newArray(int size)
		{
			return new Note[size];
		}

		@Override
		public Note createFromParcel(Parcel source)
		{
			return new Note(source);
		}
	};

	@Override
	public int describeContents()
	{
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags)
	{
		dest.writeString(noteText);
		dest.writeLong(noteFromUid);
		dest.writeString(noteFromNickname);
	}
}
