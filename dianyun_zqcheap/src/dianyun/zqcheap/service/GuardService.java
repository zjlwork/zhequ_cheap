package dianyun.zqcheap.service;

import java.util.Timer;
import java.util.TimerTask;
import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import dianyun.baobaowd.help.LogFile;

public class GuardService extends Service
{

	@Override
	public IBinder onBind(Intent intent)
	{
		return null;
	}

	@Override
	public void onCreate()
	{
		super.onCreate();
		System.out.println("GuardService-----------------onCreate");
		guard();
	}

	@Override
	@Deprecated
	public void onStart(Intent intent, int startId)
	{
		System.out.println("GuardService-----------------onStart=");
		super.onStart(intent, startId);
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId)
	{
		System.out.println("GuardService-----------------onStartCommand=");
		return super.onStartCommand(intent, flags, startId);
	}

	@Override
	public void onDestroy()
	{
		super.onDestroy();
		try
		{
			if (mTimer != null)
			{
				mTimer.cancel();
			}
			System.out.println("GuardService-----------------onDestroy=");
			Intent localIntent = new Intent();
			localIntent.setClass(this, GuardService.class); // 销毁时重新启动Service
			startService(localIntent);
		}
		catch (Exception e)
		{
			LogFile.SaveExceptionLog(e);
		}
	}

	private void guard()
	{
		try
		{
			mTimer.schedule(mTask, 50, 10);
		}
		catch (Exception e)
		{
			// LogFile.SaveExceptionLog(e);
		}
	}
	private Timer mTimer = new Timer();
	private TimerTask mTask = new TimerTask()
	{

		@Override
		public void run()
		{
			try
			{
				Intent localIntent = new Intent();
				localIntent.setClass(GuardService.this, BaoBaoWDService.class); // 销毁时重新启动Service
				startService(localIntent);
			}
			catch (Exception e)
			{
				LogFile.SaveExceptionLog(e);
			}
		}
	};
}
