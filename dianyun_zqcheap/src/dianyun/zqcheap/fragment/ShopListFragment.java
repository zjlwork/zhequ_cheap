
package dianyun.zqcheap.fragment;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.LinearLayout;
import dianyun.baobaowd.adapter.GoodsListAdapter;
import dianyun.baobaowd.db.LightDBHelper;
import dianyun.baobaowd.defineview.HorizontalImageGroup;
import dianyun.baobaowd.defineview.HotPicView;
import dianyun.baobaowd.defineview.SpecialTopicView;
import dianyun.baobaowd.defineview.SpecialTopicView2;
import dianyun.baobaowd.defineview.xlistview.CustomListView;
import dianyun.baobaowd.defineview.xlistview.CustomListView.OnLoadMoreListener;
import dianyun.baobaowd.entity.CateItem;
import dianyun.baobaowd.entity.LocalMenu;
import dianyun.baobaowd.help.ShopHttpHelper;
import dianyun.baobaowd.help.ShopHttpHelper.ShopDataCallback;
import dianyun.baobaowd.interfaces.DialogCallBack;
import dianyun.baobaowd.util.GobalConstants;
import dianyun.baobaowd.util.TaeSdkUtil;
import dianyun.baobaowd.util.ToastHelper;
import dianyun.baobaowd.util.UserHelper;
import dianyun.baobaowd.util.Utils;
import dianyun.zqcheap.R;
import dianyun.zqcheap.activity.HtmlActivity;

public class ShopListFragment extends Fragment implements OnItemClickListener,
		dianyun.baobaowd.defineview.xlistview.CustomListView.OnRefreshListener,
		OnLoadMoreListener
{

	private LocalMenu mMenuItem;
	private View mRoot;
	private View mListViewHeader;
	private LinearLayout mContainerView;
	private LinearLayout mSpecialContainerView;
	// private PullToRefreshScrollView mScrollView;
	private HotPicView mHotViewPager;
	private HorizontalImageGroup mHorizontalImageGroup;
	private List<SpecialTopicView2> mSpecialTopicViewList = new ArrayList<SpecialTopicView2>();
	private CustomListView mGoodsListView;
	private LayoutInflater mInflater;
	private GoodsListAdapter mGoodsAdapter;
	public boolean mIsInit = false;
	// private boolean mIsShowFocus = false;
	CateItem mfocusItem = null;
	CateItem mhorizontalItem = null;
	List<List<CateItem>> mspecialItem = null;
	CateItem mSpecialMoreItem;
	List<CateItem> mGoodsList = new ArrayList<CateItem>();
	private String TAG = "pull";
	private int mCurrentPageIndex = 1;
	private int mPageSize = 20;
	private final int MAXSPECIALSIZE = 10;
	private View mCurrentActivityView;
	private Activity mActivity;

	public ShopListFragment()
	{
	}

	public ShopListFragment(LocalMenu item, boolean isShowFocus)
	{
		mMenuItem = item;
		// mIsShowFocus = isShowFocus;
	}
	public Activity getActivity2()
	{
		return mActivity;
	}
	public String getTitle()
	{
		String res = "";
		if (mMenuItem != null)
		{
			res = mMenuItem.name;
		}
		return res;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState)
	{
		initData();
		super.onActivityCreated(savedInstanceState);
	}

	@Override
	public void onAttach(Activity activity)
	{
		mActivity = activity;
		super.onAttach(activity);
	}

	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState)
	{
		mInflater = inflater;
		if (null == mRoot)
		{
			mRoot = inflater.inflate(R.layout.shoplist_fragment_lay, container,
					false);
			mGoodsListView = (CustomListView) mRoot
					.findViewById(R.id.shoplist_listview);
			mGoodsListView.setHeadViewBg(getResources().getColor(
					R.color.allbgcolor));
			mGoodsListView.setCanLoadMore2(true);
			mListViewHeader = inflater.inflate(
					R.layout.shop_list_fragment_listheader, null, false);
			// mScrollView = (PullToRefreshScrollView) mRoot
			// .findViewById(R.id.shoplist_scrollview);
			// mScrollView.setMode(Mode.BOTH);
			mContainerView = (LinearLayout) mListViewHeader
					.findViewById(R.id.showlist_container);
			mSpecialContainerView = (LinearLayout) mListViewHeader
					.findViewById(R.id.special_total_lay);
			mHorizontalImageGroup = (HorizontalImageGroup) mListViewHeader
					.findViewById(R.id.shoplist_horizontal_imgcontainer);
			mHotViewPager = (HotPicView) mListViewHeader
					.findViewById(R.id.shoplist_viewpager);
			// mSpecialTopicView.setMargin(getActivity2().getResources().getDimensionPixelSize(R.dimen.speical_padding));
			mGoodsListView.addHeaderView(mListViewHeader);
			mGoodsListView.setOnRefreshListener(this);
			mGoodsListView.setOnLoadListener(this);
//			mGoodsListView.setOnItemClickListener(this);
		}
		ViewGroup vp = (ViewGroup) mRoot.getParent();
		if (vp != null)
		{
			vp.removeView(mRoot);
		}
		return mRoot;
	}

	public void setCurrentActivityRootView(View activityView)
	{
		mCurrentActivityView = activityView;
	}

	private void initSpecialView()
	{
		if (mSpecialContainerView != null)
		{
			mSpecialContainerView.removeAllViews();
			for (int i = 0; i < mspecialItem.size(); i++)
			{
				SpecialTopicView view = new SpecialTopicView(
						mSpecialContainerView.getContext());
				LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
						LinearLayout.LayoutParams.MATCH_PARENT,
						LinearLayout.LayoutParams.WRAP_CONTENT);
				view.setLayoutParams(lp);
				view.setDataSource(mspecialItem.get(i));
				mSpecialContainerView.addView(view);
			}
		}
	}

	@Override
	public void onDestroy()
	{
		// TODO Auto-generated method stub
		super.onDestroy();
	}

	public void initData()
	{
		if (mMenuItem == null)
		{
			return;
		}
		if (mMenuItem.subDataList != null && mMenuItem.subDataList.size() > 0&&mIsInit)
		{
			return;
		}
		// initHotFocusPicture(null);
		if (mMenuItem.subDataList != null && mMenuItem.subDataList.size() > 0)
		{
			doClassList(mMenuItem.subDataList);
			// 焦点图
			refreshInitData();
			// 需要再次安静的后台请求Menu获取子Category，然后重新刷新
			if (!Utils.isNetAvailable(getActivity2()))
			{
				return;
			}
			ShopHttpHelper.getChildData(getActivity2(), false,
					String.valueOf(mMenuItem.cateId), "", true, true,
					new ShopDataCallback()
					{

						@Override
						public void getMenu(List<LocalMenu> result)
						{
						}

						@Override
						public void getChildsData(List<CateItem> result)
						{
							// 比对数据是否一致，如果一致就不需要刷新
							if (!isSameToTarget(result))
							{
								mMenuItem.subDataList = result;
								doClassList(result);
								refreshInitData();
							}
						}
					});
		}
		else
		{
			if (!Utils.isNetAvailable(getActivity2()))
			{
				ToastHelper.show(getActivity2(), getActivity2().getResources()
						.getString(R.string.net_is_unable));
				return;
			}
			// 需要请求Menu获取子Category
			ShopHttpHelper.getChildData(getActivity2(), false,
					String.valueOf(mMenuItem.cateId), "", true, true,
					new ShopDataCallback()
					{

						@Override
						public void getMenu(List<LocalMenu> result)
						{
						}

						@Override
						public void getChildsData(List<CateItem> result)
						{
							mMenuItem.subDataList = result;
							doClassList(result);
							refreshInitData();
						}
					});
		}
		mIsInit = true;
	}

	public void reInitData()
	{
		if (mMenuItem == null)
		{
			return;
		}
		// initHotFocusPicture(null);
		if (mMenuItem.subDataList != null && mMenuItem.subDataList.size() > 0)
		{
			doClassList(mMenuItem.subDataList);
			// 焦点图
			refreshInitData();
			// 需要再次安静的后台请求Menu获取子Category，然后重新刷新
			if (!Utils.isNetAvailable(getActivity2()))
			{
				return;
			}
			ShopHttpHelper.getChildData(getActivity2(), false,
					String.valueOf(mMenuItem.cateId), "", true, true,
					new ShopDataCallback()
					{

						@Override
						public void getMenu(List<LocalMenu> result)
						{
						}

						@Override
						public void getChildsData(List<CateItem> result)
						{
							// 比对数据是否一致，如果一致就不需要刷新
							if (!isSameToTarget(result))
							{
								mMenuItem.subDataList = result;
								doClassList(result);
								refreshInitData();
							}
						}
					});
		}
		else
		{
			if (!Utils.isNetAvailable(getActivity2()))
			{
				ToastHelper.show(getActivity2(), getActivity2().getResources()
						.getString(R.string.net_is_unable));
				return;
			}
			// 需要请求Menu获取子Category
			ShopHttpHelper.getChildData(getActivity2(), false,
					String.valueOf(mMenuItem.cateId), "", true, true,
					new ShopDataCallback()
					{

						@Override
						public void getMenu(List<LocalMenu> result)
						{
						}

						@Override
						public void getChildsData(List<CateItem> result)
						{
							mMenuItem.subDataList = result;
							doClassList(result);
							refreshInitData();
						}
					});
		}
		mIsInit = true;
	}

	private boolean isSameToTarget(List<CateItem> result)
	{
		boolean res = true;
		if (result != null && mMenuItem.subDataList != null)
		{
			if (result.size() != mMenuItem.subDataList.size())
			{
				res = false;
			}
			else
			{
				for (int i = 0; i < result.size(); i++)
				{
					if (result.get(i) != mMenuItem.subDataList.get(i))
					{
						res = false;
						break;
					}
				}
			}
		}
		return res;
	}

	private void doClassList(List<CateItem> result)
	{
		if (result != null && result.size() > 0)
		{
			// 筛选出商品
			mCurrentPageIndex = 2;
			mGoodsList.clear();
			mfocusItem = null;
			mhorizontalItem = null;
			mspecialItem = null;
			mGoodsList.clear();
			mSpecialMoreItem = null;
			for (CateItem item : result)
			{
				if (item.xType == null)
				{
					continue;
				}
				if (item.xType == 3)
				{
					mGoodsList.add(item);
				}
				else if (item.xType == 1)
				{
					if (mfocusItem == null)
					{
						mfocusItem = item;
					}
				}
				else if (item.xType == 2)
				{
					if (mhorizontalItem == null)
					{
						mhorizontalItem = item;
					}
				}
				else if (item.xType == 4)
				{
					if (mspecialItem == null)
					{
						mspecialItem = new ArrayList<List<CateItem>>();
					}
					List<CateItem> singleItm = new ArrayList<CateItem>(2);
					singleItm.add(0, item);
					mspecialItem.add(singleItm);
				}
				else if (item.xType == 5)
				{
					mSpecialMoreItem = item;
				}
			}
		}
	}

	private void filterGoodsList(List<CateItem> result)
	{
		if (result != null)
		{
			mCurrentPageIndex++;
			if (mGoodsList == null)
			{
				mGoodsList = new ArrayList<CateItem>();
			}
			// 筛选出商品
			for (CateItem item : result)
			{
				if (item.xType == 3)
				{
					mGoodsList.add(item);
				}
			}
			if (result.size() < mPageSize)
			{
				mGoodsListView.setCanLoadMore2(false);
				// ToastHelper.show(getActivity2(), getActivity2().getResources()
				// .getString(R.string.no_moredata));
			}
		}
		else
		{
			// mGoodsListView.setCanLoadMore2(false);
		}
	}

	private void refreshInitData()
	{
		if (mfocusItem != null)
		{
			mHotViewPager.setVisibility(View.VISIBLE);
			mHotViewPager.stopPlay();
			mHotViewPager.setDataList(mfocusItem, mMenuItem.cateId);
		}
		else
		{
			mHotViewPager.setVisibility(View.GONE);
			mHotViewPager.stopPlay();
		}
		if (mhorizontalItem != null)
		{
			// 裤腰带
			mHorizontalImageGroup.setVisibility(View.VISIBLE);
			mHorizontalImageGroup.setDataSource(mhorizontalItem,
					mMenuItem.cateId);
		}
		else
		{
			mHorizontalImageGroup.setVisibility(View.GONE);
		}
		if (mspecialItem != null && mspecialItem.size() > 0)
		{
			mSpecialContainerView.setVisibility(View.VISIBLE);
			// 专题
			if (mSpecialMoreItem != null)
			{
				if (mspecialItem.size() > MAXSPECIALSIZE)
				{
					mspecialItem.get(MAXSPECIALSIZE - 1).add(1,
							mSpecialMoreItem);
				}
				else
				{
					mspecialItem.get(mspecialItem.size() - 1).add(1,
							mSpecialMoreItem);
				}
			}
			initSpecialView();
		}
		else
		{
			for (SpecialTopicView2 view : mSpecialTopicViewList)
			{
				view.setVisibility(View.GONE);
			}
			mSpecialContainerView.setVisibility(View.GONE);
		}
		if (null == mGoodsAdapter)
		{
			mGoodsAdapter = new GoodsListAdapter(getActivity2(), mGoodsList);
			mGoodsAdapter.setCurrentActivityRootView(mCurrentActivityView);
			mGoodsListView.setAdapter(mGoodsAdapter);
		}
		else
		{
			mGoodsAdapter.setDataSource(mGoodsList);
		}
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id)
	{
		position = position - 2;
		if (position >= 0 && mGoodsList != null && mGoodsList.size() > 0
				&& mGoodsList.size() > position)
		{
			final CateItem item = mGoodsList.get(position);
			if (item != null)
			{
				if (item.clickType == 4)
				{
					// 跳转到网页上的单品
					Intent itn = new Intent(getActivity2(), HtmlActivity.class);
					itn.putExtra(GobalConstants.Data.URL,
							item.clickUrl == null ? "" : item.clickUrl);
					itn.putExtra(GobalConstants.Data.TITLE,
							item.pageTitle == null ? "" : item.pageTitle);
					getActivity2().startActivity(itn);
				}
				else
				{
					if (!LightDBHelper.getIsNotTipLoginOrNotFees(getActivity2())
							&& UserHelper.getUser().getIsGuest() == GobalConstants.UserType.ISSELF
							&& mCurrentActivityView != null)
					{
//						ToastHelper.showTipLoginDialogWhenShop(getActivity2(),
//								mCurrentActivityView,
//								new ToastHelper.dialogCancelCallback()
//								{
//
//									@Override
//									public void onCancle(boolean isNeverTip)
//									{
//										LightDBHelper
//												.setIsNotTipLoginOrNotFees(
//														getActivity2(),
//														isNeverTip);
//										TaeSdkUtil.showTAEItemDetail(
//												getActivity2(), item.tbItemId,
//												item.taobaoPid, item.isTk == 1,
//												item.itemType, null,mCurrentActivityView);
//									}
//								});
						
						ToastHelper.showGoLoginDialog(getActivity2(), new DialogCallBack() {
    						
    						@Override
    						public void clickSure() {
    							
    						}
    						
    						@Override
    						public void clickCancel() {
    							TaeSdkUtil.showTAEItemDetail(
										getActivity2(), item.tbItemId,
										item.taobaoPid, item.isTk == 1,
										item.itemType, null,mCurrentActivityView);
    						}
    					});
						
						
						
						
					}
					else
					{
						TaeSdkUtil.showTAEItemDetail(getActivity2(),
								item.tbItemId, item.taobaoPid, item.isTk == 1,
								item.itemType, null,mCurrentActivityView);
					}
				}
			}
		}
	}

	@Override
	public void onRefresh()
	{
		if (!Utils.isNetAvailable(getActivity2()))
		{
			ToastHelper.show(getActivity2(), getActivity2().getResources()
					.getString(R.string.net_is_unable));
			mGoodsListView.onRefreshComplete();
			return;
		}
		mGoodsListView.setCanLoadMore2(true);
		ShopHttpHelper.getChildData(getActivity2(), false,
				String.valueOf(mMenuItem.cateId), "", true, true,
				new ShopHttpHelper.ShopDataCallback()
				{

					@Override
					public void getMenu(List<LocalMenu> result)
					{
					}

					@Override
					public void getChildsData(List<CateItem> result)
					{
						mMenuItem.subDataList = result;
						doClassList(result);
						refreshInitData();
						mGoodsListView.onRefreshComplete();
					}
				});
	}

	@Override
	public void onLoadMore()
	{
		if (!Utils.isNetAvailable(getActivity2()))
		{
			ToastHelper.show(getActivity2(), getActivity2().getResources()
					.getString(R.string.net_is_unable));
			mGoodsListView.onRefreshComplete();
			return;
		}
		ShopHttpHelper.getMoreChildData(getActivity2(), false,
				mCurrentPageIndex, mPageSize, String.valueOf(mMenuItem.cateId),
				new ShopHttpHelper.ShopDataCallback()
				{

					@Override
					public void getMenu(List<LocalMenu> result)
					{
					}

					@Override
					public void getChildsData(List<CateItem> result)
					{
						filterGoodsList(result);
						if (null == mGoodsAdapter)
						{
							mGoodsAdapter = new GoodsListAdapter(getActivity2(),
									mGoodsList);
							mGoodsAdapter.setCurrentActivityRootView(mCurrentActivityView);
							mGoodsListView.setAdapter(mGoodsAdapter);
						}
						else
						{
							mGoodsAdapter.setDataSource(mGoodsList);
						}
						mGoodsListView.onLoadMoreComplete();
					}
				});
	}
}
