package dianyun.zqcheap.fragment;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.GridView;
import android.widget.RelativeLayout;
import dianyun.baobaowd.adapter.GoodsWordGridAdapter;
import dianyun.baobaowd.defineview.ScrollListView;
import dianyun.baobaowd.defineview.ScrollListView.ItemSelectedCallback;
import dianyun.baobaowd.entity.Menu;
import dianyun.baobaowd.help.ShopHttpHelper;
import dianyun.baobaowd.help.ShopHttpHelper.GetMenuCallback;
import dianyun.baobaowd.util.ConversionHelper;
import dianyun.baobaowd.util.MenuHelper;
import dianyun.baobaowd.util.ToastHelper;
import dianyun.baobaowd.util.Utils;
import dianyun.zqcheap.R;
import dianyun.zqcheap.activity.ShowScanResultActivity;

public class ShopFragment2 extends Fragment implements OnClickListener {


    private boolean mInit = false;
    private View mCurrentActivityView;
    Button mActivityBackBt;

    private View mRoot;
    private Activity mActivity;
    
    private ScrollListView mScrollListView;
    private GridView mGridView;
    private GoodsWordGridAdapter mGoodsWordGridAdapter;
    private RelativeLayout mSearchLayout;
    private List<Menu> mChildMenuList = new ArrayList<Menu>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (null == mRoot) {
            mRoot = inflater.inflate(R.layout.shopfragment_lay2, container,
                    false);
//            initView();
        }
        ViewGroup vp = (ViewGroup) mRoot.getParent();
        if (vp != null) {
            vp.removeView(mRoot);
        }
        return mRoot;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
//        if (!mInit ) {
//            initData();
//            mInit = true;
//        }
    	initView();
    	initData();
        super.onActivityCreated(savedInstanceState);
    }

    public void setCurrentActivityRootView(View activityView) {
        mCurrentActivityView = activityView;
    }

    private int getGridItemImageWidth(){
    	int screenWidth = ToastHelper.getScreenWidth(getActivity2());
    	int paddingWidth = 4*ConversionHelper.dipToPx(10, getActivity2());
//    			+mGridView.getPaddingLeft()+mGridView.getPaddingRight();
    	return (screenWidth-paddingWidth-mScrollListView.getLayoutParams().width)/3;
    }
    public void initView() {
     
        mCurrentActivityView = mRoot.findViewById(R.id.root_view);
        mActivityBackBt = (Button) mRoot.findViewById(R.id.activityback_bt);
        mScrollListView = (ScrollListView) mRoot.findViewById(R.id.scrolllistview);
        mSearchLayout = (RelativeLayout) mRoot.findViewById(R.id.main_fragment_float_window);
        mGridView = (GridView) mRoot.findViewById(R.id.gridview);
        mGridView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				Menu menu = mChildMenuList.get(position);
				Intent lIntent = new Intent(getActivity2(),ShowScanResultActivity.class);
				lIntent.putExtra("menuName", menu.name);
				startActivity(lIntent);
			}
		});
        mScrollListView.setItemSelectedCallback(new ItemSelectedCallback() {
			
			@Override
			public void itemSelected(Menu Menu) {
				getChildMenu();
			}
		});
        mSearchLayout.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Utils.goActivity(getActivity2(), ShowScanResultActivity.class);
			}
		});
        mActivityBackBt.setOnClickListener(this);
    }


    @Override
    public void onAttach(Activity activity) {
        mActivity = activity;
        super.onAttach(activity);
    }

    public Activity getActivity2() {
        return mActivity;
    }


	
    @Override
    public void onResume() {
    	super.onResume();
    	
    }
    
    
    
    
    public void initData()
	{

      
    	 mGoodsWordGridAdapter = new GoodsWordGridAdapter(mChildMenuList,getActivity2(),getGridItemImageWidth());
		 mGridView.setAdapter(mGoodsWordGridAdapter);
    	
    	
//    	final List<Menu> mMenuList = ShopDBHelper.getShopMenu(getActivity());
    	
        final List<Menu> mMenuList = MenuHelper.getMenuListByparentId(getActivity2(), 0);
		if (mMenuList != null && mMenuList.size() > 0)
		{
			 mScrollListView.setDataSource(mMenuList);
			 refreshLocalChildMenu(mScrollListView.getSelectedMenu());
		}
		if (!Utils.isNetAvailable(getActivity()))
		{
			ToastHelper.show(getActivity(), getActivity().getResources()
					.getString(R.string.net_is_unable));
			return;
		}
		ShopHttpHelper.getMenu(getActivity(), mMenuList==null?true:false,0, new GetMenuCallback()
		{

			@Override
			public void getMenu(List<Menu> result,long parentId)
			{
				
					if(result!=null&&result.size()>0
							&&!isSameToTarget(result,mScrollListView.getMenuList())){
						mScrollListView.setDataSource(result);
						getChildMenu();
					}
			}

			
		});
		
		
		
		
		
	}
    
    
    private boolean refreshLocalChildMenu(Menu mSelectedMenu){
    	if(mSelectedMenu!=null){
			 final List<Menu> childMenuList = MenuHelper.getMenuListByparentId(getActivity2(), mSelectedMenu.cateId);
			 if(childMenuList!=null&&childMenuList.size()>0){
				 mChildMenuList.clear();
				 mChildMenuList.addAll(childMenuList);
				 mGoodsWordGridAdapter.notifyDataSetChanged();
			 }
			 return childMenuList==null?true:false;
    	}
    	return true;
    }
    
    private void getChildMenu(){
    	Menu mSelectedMenu = mScrollListView.getSelectedMenu();
    	if(mSelectedMenu!=null){
    		boolean showDialog = refreshLocalChildMenu(mSelectedMenu);
			
			 if (!Utils.isNetAvailable(getActivity()))
					return;
			ShopHttpHelper.getMenu(getActivity(), showDialog,mSelectedMenu.cateId, new GetMenuCallback()
			{

				@Override
				public void getMenu(List<Menu> result,long parentId)
				{
					if(result!=null&&result.size()>0&&parentId==mScrollListView.getSelectedMenu().cateId){
						 mChildMenuList.clear();
						 mChildMenuList.addAll(result);
						 mGoodsWordGridAdapter.notifyDataSetChanged();
					 }
				}
			});
		}
    	
    }
    
    
    
    

	private boolean isSameToTarget(List<Menu> result,List<Menu> mMenuList)
	{
		boolean res = true;
		if (result != null && mMenuList != null)
		{
			if (result.size() != mMenuList.size())
			{
				res = false;
			}
			else
			{
				for (int i = 0; i < result.size(); i++)
				{
					if (result.get(i).cateId != mMenuList.get(i).cateId)
					{
						res = false;
						break;
					}
				}
			}
		}
		return res;
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		
	}
    
    
    
    
    
    
    
    
    
    
    
    
    


}
