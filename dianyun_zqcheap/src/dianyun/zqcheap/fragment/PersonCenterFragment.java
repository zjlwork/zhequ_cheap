package dianyun.zqcheap.fragment;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.umeng.analytics.MobclickAgent;

import dianyun.baobaowd.data.InviteTotal;
import dianyun.baobaowd.data.User;
import dianyun.baobaowd.db.LightDBHelper;
import dianyun.baobaowd.dto.ResultDTO;
import dianyun.baobaowd.gson.GsonHelper;
import dianyun.baobaowd.help.ShopHttpHelper;
import dianyun.baobaowd.help.ShopHttpHelper.GetFreezeYoCoinsCallback;
import dianyun.baobaowd.serverinterface.GetInviteUserCount;
import dianyun.baobaowd.serverinterface.UserAvatarSet;
import dianyun.baobaowd.serverinterface.UserSet;
import dianyun.baobaowd.util.BindHelper;
import dianyun.baobaowd.util.BroadCastHelper;
import dianyun.baobaowd.util.CameraGalleryHelper;
import dianyun.baobaowd.util.DialogHelper;
import dianyun.baobaowd.util.GobalConstants;
import dianyun.baobaowd.util.NetworkStatus;
import dianyun.baobaowd.util.ServiceInterfaceHelper;
import dianyun.baobaowd.util.UserHelper;
import dianyun.baobaowd.util.Utils;
import dianyun.zqcheap.R;
import dianyun.zqcheap.activity.InviteFriendActivity;
import dianyun.zqcheap.activity.MessageCenterActivity;
import dianyun.zqcheap.activity.OrderRecordActivity;
import dianyun.zqcheap.activity.RecordActivity;
import dianyun.zqcheap.activity.SettingActivity;
import dianyun.zqcheap.application.BaoBaoWDApplication;

public class PersonCenterFragment extends Fragment implements OnClickListener {

	private View mRoot = null;
	private boolean mInit = false;

	private RelativeLayout mSysMsgLayout;

	private TextView mUsernameTv;
	private TextView mUidTv;
	private TextView mYoCoinsTv;;
	private TextView mTotalYoCoinsTv;
	
	
	private TextView mInviteCountTv;;
	private ImageView mUserAvatarIv;

	private RelativeLayout mPersonLayout;
	private LinearLayout mInviteFriendLayout;
	private RelativeLayout mOrderLayout;
	private RelativeLayout mRebateLayout;
	private RelativeLayout mRecordLayout;
	private RelativeLayout mHelpLayout;
	private RelativeLayout mSettingLayout;

	private User mUser;
	private String mTempImgPath;
	private String mAvatarLocalPath;
	private BindHelper mBindHelper;
	
	private TextView msgnewestcount_tv;;
	private TextView recordnewestcount_tv;;
	private TextView ordernewestcount_tv;;
	
	
	private LinearLayout total_yocoins_layout;
	private LinearLayout yocoins_layout;

	private void initUser() {
		mUser = UserHelper.getUser();
		if (getActivity() == null)
			return;
		UserHelper.showUserAvatar(getActivity(), mUser, mUserAvatarIv);
		mUsernameTv.setText(mUser.getNickname());
		mYoCoinsTv.setText(String.valueOf(mUser.getYcoins()));
		mTotalYoCoinsTv.setText(String.valueOf(mUser.getYcoins()+mUser.getFreezeYcoins()));
		mUidTv.setText("ID: "+String.valueOf(mUser.getUid()));
	}

	public boolean isLive() {
		return mInit;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		if (null == mRoot) {
			mRoot = inflater.inflate(R.layout.personcenterfragment, container,
					false);
		}
		ViewGroup vp = (ViewGroup) mRoot.getParent();
		if (vp != null) {
			vp.removeView(mRoot);
		}
		return mRoot;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		initData();
	}

	/**
	 * 设置Fragment 当激活时候是否要重新刷新数据
	 * 
	 * @param isToRefresh
	 */
	// public void setInitDataWhenResume(boolean isToRefresh)
	// {
	// mInit = isToRefresh;
	// }

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
	}

	@Override
	public void onResume() {
		super.onResume();
		MobclickAgent.onPageStart("personcenter");
		refreshNewest();
		initUser();
		
	}
	
	public void refreshNewest(){
		if(getActivity()==null)return;
		int msgnewestcount = LightDBHelper.getSystemMsgCount(BaoBaoWDApplication.context);
		int ordernewestcount = LightDBHelper.getFanliCount(BaoBaoWDApplication.context);
		int recordnewestcount = LightDBHelper.getCashCount(BaoBaoWDApplication.context);
		if(msgnewestcount>0){
			msgnewestcount_tv.setText(String.valueOf(msgnewestcount));
			msgnewestcount_tv.setVisibility(View.VISIBLE);
		}else{
			msgnewestcount_tv.setVisibility(View.GONE);
		}
		if(ordernewestcount>0){
			ordernewestcount_tv.setText(String.valueOf(ordernewestcount));
			ordernewestcount_tv.setVisibility(View.VISIBLE);
		}else{
			ordernewestcount_tv.setVisibility(View.GONE);
		}
		if(recordnewestcount>0){
			recordnewestcount_tv.setText(String.valueOf(recordnewestcount));
			recordnewestcount_tv.setVisibility(View.VISIBLE);
		}else{
			recordnewestcount_tv.setVisibility(View.GONE);
		}
		
		
		
	}
	

	@Override
	public void onPause() {
		super.onPause();
		MobclickAgent.onPageEnd("personcenter");
	}

	@Override
	public void onStart() {
		super.onStart();
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		mInit = false;
		System.out.println("foundfragment onDestroy========");
		if(mBindHelper!=null)mBindHelper.cancelJudge();
	}

	@Override
	public void onClick(View v) {
		if (v.getId() == R.id.person_layout) {
			if (NetworkStatus.getNetWorkStatus(getActivity()) > 0)
				showChangeNameDialog(mUser.getNickname());
			else
				Toast.makeText(getActivity(), getString(R.string.no_network),
						Toast.LENGTH_SHORT).show();
		} else if (v.getId() == R.id.order_layout) {
			Intent lIntent = new Intent(getActivity(),
					OrderRecordActivity.class);
			startActivity(lIntent);
		} else if (v.getId() == R.id.rebate_layout) {
			mBindHelper = new BindHelper(getActivity());
			mBindHelper.judgeBind();
		} else if (v.getId() == R.id.avatar_iv) {
			if (NetworkStatus.getNetWorkStatus(getActivity()) > 0)
				showChooseAvatarDialog();
			else
				Toast.makeText(getActivity(), getString(R.string.no_network),
						Toast.LENGTH_SHORT).show();

		} else if (v.getId() == R.id.msg_layout) {
			goMessageCenterActivity(GobalConstants.MessageCenterType.SYSTEM);

		} else if (v.getId() == R.id.record_layout) {
			Intent lIntent = new Intent(getActivity(), RecordActivity.class);
			getActivity().startActivity(lIntent);
			
		}else if (v.getId() == R.id.invitefriend_layout) {
//			if (NetworkStatus.getNetWorkStatus(getActivity()) > 0){
//				
//				
//				UserShareUrl userShareUrl1=	UserShareUrlHelper.getUserShareUrlByKey(BaoBaoWDApplication.context,
//						UserShareUrl.KEY_FANLIINVITEUSERLIST);
//				UserShareUrl userShareUrl2=	UserShareUrlHelper.getUserShareUrlByKey(BaoBaoWDApplication.context,
//						UserShareUrl.KEY_FANLIPROFILEAPP);
//				String inviteUserListUrl = userShareUrl1==null?"":userShareUrl1.getUrlValue();
//				String profileAppUrl = userShareUrl2==null?"":userShareUrl2.getUrlValue();
//				if(!TextUtils.isEmpty(inviteUserListUrl)&&!TextUtils.isEmpty(profileAppUrl)){
//					toInviteFriendActivity(inviteUserListUrl,profileAppUrl);
//				}else{
//					new GetShareUrlThread().start();
//				}
//			}else
//				Toast.makeText(getActivity(), getString(R.string.no_network),
//						Toast.LENGTH_SHORT).show();
			toInviteFriendActivity();
		}else if(v.getId() == R.id.total_yocoins_layout){
			Utils.goHtmlActivity(getActivity(), getResources().getString(R.string.doc_coin_des),
					GobalConstants.URL.YOYOBASE+GobalConstants.URL.DOC_COIN);
		}else if(v.getId() == R.id.yocoins_layout){
			Utils.goHtmlActivity(getActivity(), getResources().getString(R.string.doc_coin_des),
					GobalConstants.URL.YOYOBASE+GobalConstants.URL.DOC_COIN);
		}else if(v.getId() == R.id.setting_layout){
			Intent lIntent = new Intent(getActivity(),
					SettingActivity.class);
			getActivity().startActivity(lIntent);
		}else if(v.getId() == R.id.help_layout){
			Utils.goHtmlActivity(getActivity(), getResources().getString(R.string.helpcenter),
					GobalConstants.URL.YOYOBASE+GobalConstants.URL.HELPCENTER);
		}
	
	}

	private void initData() {
		mUser = UserHelper.getUser();
		msgnewestcount_tv = (TextView) mRoot.findViewById(R.id.msgnewestcount_tv);
		recordnewestcount_tv = (TextView) mRoot.findViewById(R.id.recordnewestcount_tv);
		ordernewestcount_tv = (TextView) mRoot.findViewById(R.id.ordernewestcount_tv);
		mHelpLayout = (RelativeLayout) mRoot.findViewById(R.id.help_layout);
		mSettingLayout = (RelativeLayout) mRoot.findViewById(R.id.setting_layout);
		mSettingLayout.setOnClickListener(this);
		mHelpLayout.setOnClickListener(this);
		mYoCoinsTv = (TextView) mRoot.findViewById(R.id.yocoins_tv);
		mTotalYoCoinsTv = (TextView) mRoot.findViewById(R.id.total_yocoins_tv);
		mInviteCountTv = (TextView) mRoot.findViewById(R.id.invitecount_tv);
		mInviteCountTv.setText(String.valueOf(mUser.getInviteUserCount()));
		mUsernameTv = (TextView) mRoot.findViewById(R.id.username_tv);
		mUidTv = (TextView) mRoot.findViewById(R.id.uid_tv);
		mUserAvatarIv = (ImageView) mRoot.findViewById(R.id.avatar_iv);
		mPersonLayout = (RelativeLayout) mRoot.findViewById(R.id.person_layout);
		mInviteFriendLayout = (LinearLayout) mRoot.findViewById(R.id.invitefriend_layout);
		total_yocoins_layout = (LinearLayout) mRoot.findViewById(R.id.total_yocoins_layout);
		yocoins_layout = (LinearLayout) mRoot.findViewById(R.id.yocoins_layout);
		mOrderLayout = (RelativeLayout) mRoot.findViewById(R.id.order_layout);
		mRebateLayout = (RelativeLayout) mRoot.findViewById(R.id.rebate_layout);
		mRecordLayout = (RelativeLayout) mRoot.findViewById(R.id.record_layout);
		mSysMsgLayout = (RelativeLayout) mRoot.findViewById(R.id.msg_layout);
		mPersonLayout.setOnClickListener(this);
		mInviteFriendLayout.setOnClickListener(this);
		total_yocoins_layout.setOnClickListener(this);
		yocoins_layout.setOnClickListener(this);
		mOrderLayout.setOnClickListener(this);
		mRebateLayout.setOnClickListener(this);
		mRecordLayout.setOnClickListener(this);
		mSysMsgLayout.setOnClickListener(this);
		mUserAvatarIv.setOnClickListener(this);
		initUser();
		if (NetworkStatus.getNetWorkStatus(getActivity()) > 0) {
			ServiceInterfaceHelper.getUserInfo(getActivity(),mUser.getUid(), mUser.getToken(), 
					mUser.getUid(),false, new ServiceInterfaceHelper.InterfaceCallBack(){
				@Override
				public void getResultDTO(ResultDTO resultDTO) {
					if (getActivity() == null)
						return;
					if (resultDTO != null && resultDTO.getCode().equals("0")) {
						User user = GsonHelper.gsonToObj(resultDTO.getResult(),
								User.class);
						if (user != null) {
							if (user.getUid().equals(mUser.getUid())) {
								user.setIsSelf(GobalConstants.AllStatusYesOrNo.YES);
								user.setToken(mUser.getToken());
								user.setInviteUserCount(mUser.getInviteUserCount());
								user.setFreezeYcoins(mUser.getFreezeYcoins());
							}
							UserHelper.setUser(user);
							BroadCastHelper.sendRefreshMainBroadcast(
									getActivity(),
									GobalConstants.RefreshType.USER);
							initUser();
						}
					}
				}
			});
			
			ShopHttpHelper.getFreezeYoCoins(getActivity(), mUser.getUid(), false, new GetFreezeYoCoinsCallback() {
				
				@Override
				public void getResult(Integer freezeYoCoins) {
					mUser.setFreezeYcoins(freezeYoCoins);
					UserHelper.setUser(mUser);
					initUser();
				}
			});
			new GetInviteUserCountThread().start();
		}

		mInit = true;
	}

	private void goMessageCenterActivity(int messageType) {
		Intent lIntent = new Intent(getActivity(), MessageCenterActivity.class);
		lIntent.putExtra(GobalConstants.Data.MESSAGETYPE, messageType);
		getActivity().startActivity(lIntent);
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		mAvatarLocalPath = CameraGalleryHelper.handleChangeAvatarResult(
				getActivity(), requestCode, resultCode, data, mTempImgPath,
				mUserAvatarIv, mAvatarLocalPath);
		if (!TextUtils.isEmpty(mAvatarLocalPath)) {
			new UserAvatarSetThread().start();
		}

	}

	private void showChooseAvatarDialog() {
		View pview = LayoutInflater.from(getActivity()).inflate(
				R.layout.chooseavatardialog, null);
		final Dialog dialog = new Dialog(getActivity(), R.style.dialognotitle);
		dialog.setContentView(pview);
		dialog.show();
		RelativeLayout cameraLayout = (RelativeLayout) pview
				.findViewById(R.id.camera_layout);
		RelativeLayout albumsLayout = (RelativeLayout) pview
				.findViewById(R.id.albums_layout);
		Button cancelBt = (Button) pview.findViewById(R.id.cancel_bt);
		cameraLayout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				dialog.cancel();
				mTempImgPath = CameraGalleryHelper.startCamera(getActivity());
			}
		});
		albumsLayout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				dialog.cancel();
				CameraGalleryHelper.startGallery(getActivity());
			}
		});
		cancelBt.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				dialog.cancel();
			}
		});
	}

	Dialog mProgressDialog;




	class UserAvatarSetThread extends Thread {

		private Handler handler;
		private ResultDTO avatarSetResultDTO;

		public UserAvatarSetThread() {
			handler = new Handler();
			mProgressDialog = DialogHelper.showProgressDialog(getActivity(),
					getString(R.string.uploadinguserinfo));
		}

		@Override
		public void run() {

			avatarSetResultDTO = new UserAvatarSet(mUser.getUid(),
					mUser.getToken(), mAvatarLocalPath).postConnect();

			handler.post(new Runnable() {

				@Override
				public void run() {
					DialogHelper.cancelProgressDialog(mProgressDialog);
					if (avatarSetResultDTO != null
							&& avatarSetResultDTO.getCode().equals("0")) {
						mUser.setProfileImage(avatarSetResultDTO.getResult());
						UserHelper.setUser(mUser);
						initUser();
						BroadCastHelper.sendRefreshMainBroadcast(getActivity(),
								GobalConstants.RefreshType.USER);
					} else {
						Toast.makeText(getActivity(),
								getString(R.string.uploadinguserinfofailed),
								Toast.LENGTH_SHORT).show();
					}
				}
			});
		}
	}

	class UserSetThread extends Thread {

		private Handler handler;
		private ResultDTO userSetResultDTO;
		private String nickname;

		public UserSetThread(String nickname) {
			handler = new Handler();
			this.nickname = nickname;
			mProgressDialog = DialogHelper.showProgressDialog(getActivity(),
					getString(R.string.uploadinguserinfo));
		}

		@Override
		public void run() {
			userSetResultDTO = new UserSet(mUser.getUid(), mUser.getToken(),
					(byte) 0, (byte) 0, "", nickname, "").postConnect();

			handler.post(new Runnable() {

				@Override
				public void run() {
					DialogHelper.cancelProgressDialog(mProgressDialog);
					if (userSetResultDTO != null
							&& userSetResultDTO.getCode().equals("0")) {

						mUser.setNickname(nickname);
						UserHelper.setUser(mUser);
						initUser();
						BroadCastHelper.sendRefreshMainBroadcast(getActivity(),
								GobalConstants.RefreshType.USER);
					} else {
						Toast.makeText(getActivity(),
								getString(R.string.uploadinguserinfofailed),
								Toast.LENGTH_SHORT).show();
					}
				}
			});
		}
	}

	String mNickName;

	private void showChangeNameDialog(String oldName) {
		View pview = LayoutInflater.from(getActivity()).inflate(
				R.layout.changenamedialog, null);
		final Dialog dialog = new Dialog(getActivity(), R.style.dialognotitle);
		dialog.setContentView(pview);
		dialog.show();
		final EditText edittext = (EditText) pview.findViewById(R.id.edittext);
		final TextView titleTv = (TextView) pview.findViewById(R.id.title_tv);
		if (oldName != null && !oldName.equals("")) {
			titleTv.setText(getString(R.string.changenickname));
			edittext.setText(oldName);
			edittext.setSelection(oldName.length());
		} else {
			titleTv.setText(getString(R.string.setnickname));
		}
		Button cancelBt = (Button) pview.findViewById(R.id.cancel_bt);
		Button sureBt = (Button) pview.findViewById(R.id.sure_bt);
		sureBt.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				dialog.cancel();
				mNickName = edittext.getText().toString();
				mUsernameTv.setText(mNickName);
				dialog.cancel();

				new UserSetThread(mNickName).start();

			}
		});
		cancelBt.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				dialog.cancel();
			}
		});

	}
	
	
	
	
//	class GetShareUrlThread extends Thread {
//
//		private Handler handler;
//		private ResultDTO resultDTO;
//
//		public GetShareUrlThread() {
//			handler = new Handler();
//			mProgressDialog = DialogHelper.showProgressDialog(getActivity(),
//					getString(R.string.loginloading));
//		}
//
//		@Override
//		public void run() {
//			resultDTO = new GetShareUrl(mUser.getUid(), mUser.getToken()).getConnect();
//
//			handler.post(new Runnable() {
//
//				@Override
//				public void run() {
//					DialogHelper.cancelProgressDialog(mProgressDialog);
//					
//					if (resultDTO != null && resultDTO.getCode().equals("0")) {
//						List<UserShareUrl> lShareUrlList= GsonHelper.gsonToObj(resultDTO.getResult(),
//								new TypeToken<List<UserShareUrl>>(){});
//						
//						UserShareUrlHelper.addUserShareUrlList(BaoBaoWDApplication.context, lShareUrlList);
//						if (getActivity() == null)
//							return;
//						if(lShareUrlList!=null&&lShareUrlList.size()>0){
//							Intent lIntent = new Intent(getActivity(),InviteFriendActivity.class);
//							startActivity(lIntent);
//						}
//						
//					}
//				}
//			});
//		}
//	}
	private void toInviteFriendActivity(){
		Intent lIntent = new Intent(getActivity(),InviteFriendActivity.class);
		startActivity(lIntent);
	}
	class GetInviteUserCountThread extends Thread {
		
		private Handler handler;
		private ResultDTO resultDTO;
		
		public GetInviteUserCountThread() {
			handler = new Handler();
		
		}
		
		@Override
		public void run() {
			resultDTO = new GetInviteUserCount(mUser.getUid(), mUser.getToken()).getConnect();
			
			handler.post(new Runnable() {
				
				@Override
				public void run() {
					DialogHelper.cancelProgressDialog(mProgressDialog);
					
					if (resultDTO != null && resultDTO.getCode().equals("0")) {
						InviteTotal  lInviteTotal= GsonHelper.gsonToObj(resultDTO.getResult(),
								InviteTotal.class);
						
						if(lInviteTotal!=null){
							int inviteUserCount = lInviteTotal.getUserCount();
							mUser.setInviteUserCount(inviteUserCount);
							UserHelper.setUser(mUser);
							if (getActivity() == null)
								return;
							mInviteCountTv.setText(String.valueOf(inviteUserCount));
						}
					}
				}
			});
		}
	}

	
	
	
	
	
	
	
	
	
	

}
