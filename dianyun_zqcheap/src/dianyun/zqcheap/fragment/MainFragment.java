package dianyun.zqcheap.fragment;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.reflect.TypeToken;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.download.ImageDownloader.Scheme;

import dianyun.baobaowd.adapter.GoodsListAdapter;
import dianyun.baobaowd.data.Notice;
import dianyun.baobaowd.data.User;
import dianyun.baobaowd.data.ViewPicture;
import dianyun.baobaowd.db.LightDBHelper;
import dianyun.baobaowd.db.ShopDBHelper;
import dianyun.baobaowd.defineview.HotPicView;
import dianyun.baobaowd.defineview.SeletcMainBGDialog;
import dianyun.baobaowd.defineview.pager.ADsPagerAdapter;
import dianyun.baobaowd.defineview.pager.VerticalViewPager;
import dianyun.baobaowd.defineview.xlistview.CustomListView;
import dianyun.baobaowd.dto.ResultDTO;
import dianyun.baobaowd.entity.CateItem;
import dianyun.baobaowd.entity.LocalMenu;
import dianyun.baobaowd.gson.GsonHelper;
import dianyun.baobaowd.help.ShopHttpHelper;
import dianyun.baobaowd.help.ShopHttpHelper.MiaoshaPictureCallback;
import dianyun.baobaowd.interfaces.DialogCallBack;
import dianyun.baobaowd.serverinterface.GetADs;
import dianyun.baobaowd.util.ADHelper;
import dianyun.baobaowd.util.BindHelper;
import dianyun.baobaowd.util.GobalConstants;
import dianyun.baobaowd.util.TaeSdkUtil;
import dianyun.baobaowd.util.ToastHelper;
import dianyun.baobaowd.util.UserHelper;
import dianyun.baobaowd.util.Utils;
import dianyun.zqcheap.R;
import dianyun.zqcheap.activity.CheapListActivity;
import dianyun.zqcheap.activity.GoldMallActivity;
import dianyun.zqcheap.activity.HtmlActivity;
import dianyun.zqcheap.activity.MiaoshaActivity;
import dianyun.zqcheap.activity.RankActivity;
import dianyun.zqcheap.activity.SflHintActivity;
import dianyun.zqcheap.activity.ShowScanResultActivity;
import dianyun.zqcheap.activity.TaskActivity;

public class MainFragment extends Fragment implements OnClickListener, CustomListView.OnRefreshListener, CustomListView.OnLoadMoreListener, SeletcMainBGDialog.selectedCallback, AdapterView.OnItemClickListener, AbsListView.OnScrollListener {


    private boolean mInit = false;
    private View mCurrentActivityView;
    private View mRoot;
    private CustomListView mGridListView;
    
    private List<CateItem> mGoodsList = new ArrayList<CateItem>();
    private GoodsListAdapter mGoodsAdapter;
    private int mCurrentPageIndex = 1;
    private int mPageSize = 20;
    
    

    List<Notice> mAdList;
    private ImageView mQianDaoNewDot;
    private ImageView mGoldMallNewDot;
    private ImageView mShopCarImageView;
    private VerticalViewPager mViewPager;
    private static Handler mHandler = getHandler();
    private ADsPagerAdapter mADsPagerAdapter;
    private TextView mNoticeTv;
    private User mUser;

    private Activity mActivity;
    private View mFloatSearchView;
    private View headerView;
    private RelativeLayout mBgLay;
    private SeletcMainBGDialog mSeletcMainBGDialog;
    private HotPicView mHotPicView;
    
    
    private TextView miaosha_hint1;
    private TextView miaosha_hint2;
    private ImageView miaosha_hintiv;
    
    private TextView gfl_hint1;
    private TextView gfl_hint2;
    private ImageView gfl_hintiv;
    private LinearLayout miaosha_layout;
    private RelativeLayout gfl_layout;
    
    

    public static Handler getHandler() {
        if (mHandler == null) {
            mHandler = new Handler();
        }
        return mHandler;
    }

    public static final int AD_START = 1;
    public static final int AD_CHANGETOFIRST = 2;

    public Handler mADHandler = new Handler() {
        public void handleMessage(android.os.Message msg) {

            switch (msg.what) {
                case AD_START:
                    mViewPager.setCurrentItem(
                            mViewPager.getCurrentItem() + 1, true);

                    if (mViewPager.getCurrentItem() == mAdList.size() - 1) {
                        mADHandler.sendEmptyMessageDelayed(AD_CHANGETOFIRST, 1000);
                    }
                    mADHandler.sendEmptyMessageDelayed(AD_START, 3000);
                    break;
                case AD_CHANGETOFIRST:
                    mViewPager.setCurrentItem(0, false);
                    break;
            }
        }

        ;
    };

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (null == mRoot) {
            mRoot = inflater.inflate(R.layout.mainfragment_lay, container,
                    false);
            initView();
        }
        ViewGroup vp = (ViewGroup) mRoot.getParent();
        if (vp != null) {
            vp.removeView(mRoot);
        }
        return mRoot;
    }

    public Activity getActivity2() {
        return mActivity;
    }

    @Override
    public void onAttach(Activity activity) {
        mActivity = activity;
        super.onAttach(activity);
    }

    @Override
    public void onResume() {
        refreshFragmentInfo();
        super.onResume();
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        if (!mInit) {
            initData();
            mInit = true;
        }
        super.onActivityCreated(savedInstanceState);
    }

    public void setCurrentActivityRootView(View activityView) {
        mCurrentActivityView = activityView;
        if(mGoodsAdapter!=null)
        	 mGoodsAdapter.setCurrentActivityRootView(mCurrentActivityView);
    }
    View shop_scan_pop_lay;
    View main_fragment_module_qiandao_lay;
    public void initView() {

        mShopCarImageView = (ImageView) mRoot.findViewById(R.id.shop_main_show_shoppingcar);
        mShopCarImageView.setOnClickListener(this);
        mFloatSearchView = mRoot.findViewById(R.id.main_fragment_float_window);
        mFloatSearchView.setOnClickListener(this);
        mGridListView = (CustomListView) mRoot.findViewById(R.id.main_fragment_listview);
        mGridListView.setOnRefreshListener(this);
        mGridListView.setOnLoadListener(this);
        mGridListView.setSeconScrollListener(this);
        mGridListView.setOnItemClickListener(this);
      
        
        
        headerView = LayoutInflater.from(getActivity2()).inflate(R.layout.main_listview_header_lay, null);
        shop_scan_pop_lay = headerView.findViewById(R.id.shop_scan_pop_lay);
        main_fragment_module_qiandao_lay = headerView.findViewById(R.id.main_fragment_module_qiandao_lay);
       
        
        headerView.findViewById(R.id.shop_scan_pop_lay).setOnClickListener(this);
        headerView.findViewById(R.id.main_fragment_module_qiandao_image).setOnClickListener(this);
        headerView.findViewById(R.id.main_fragment_module_tixian_lay).setOnClickListener(this);
        headerView.findViewById(R.id.main_fragment_module_rank_lay).setOnClickListener(this);
        headerView.findViewById(R.id.main_fragment_module_change_lay).setOnClickListener(this);
        headerView.findViewById(R.id.miaosha_layout).setOnClickListener(this);
        headerView.findViewById(R.id.sfl_layout).setOnClickListener(this);
        headerView.findViewById(R.id.gfl_layout).setOnClickListener(this);

         miaosha_hint1 =(TextView)headerView.findViewById(R.id.miaosha_hint1);
         miaosha_hint2 =(TextView)headerView.findViewById(R.id.miaosha_hint2);
         miaosha_hintiv =(ImageView)headerView.findViewById(R.id.miaosha_hintiv);
         miaosha_layout =(LinearLayout)headerView.findViewById(R.id.miaosha_layout);
         gfl_hint1 =(TextView)headerView.findViewById(R.id.gfl_hint1);
         gfl_hint2 =(TextView)headerView.findViewById(R.id.gfl_hint2);
         gfl_hintiv =(ImageView)headerView.findViewById(R.id.gfl_hintiv);
         gfl_layout =(RelativeLayout)headerView.findViewById(R.id.gfl_layout);
        
        
        
        mBgLay = (RelativeLayout)headerView.findViewById(R.id.main_fragment_header_bg_lay);
        mHotPicView =  (HotPicView )headerView.findViewById(R.id.shoplist_viewpager);
        int width = ToastHelper.getScreenWidth(getActivity2());
        int height = width/8*5;
        mHotPicView.setLayoutParams(new RelativeLayout.LayoutParams(width,height));
        mNoticeTv = (TextView) headerView.findViewById(R.id.notice_tv);
        mQianDaoNewDot = (ImageView) headerView.findViewById(R.id.main_fragment_module_qiandao_newdot);
        mGoldMallNewDot = (ImageView) headerView.findViewById(R.id.main_fragment_module_tixian_newdot);
        mViewPager = (VerticalViewPager) headerView.findViewById(R.id.verticalviewpager);
        mGridListView.addHeaderView(headerView);
        if (mGoodsAdapter == null) {
            mGoodsAdapter = new GoodsListAdapter(getActivity2(), mGoodsList);
            mGoodsAdapter.setCurrentActivityRootView(mCurrentActivityView);
            mGridListView.setAdapter(mGoodsAdapter);
        }
       
          	
        
        
        
    }

    
    public int[] getScanLocation(){
    	int[] location = new int[2];
    	shop_scan_pop_lay.getLocationOnScreen(location);
    	int[] newLocation = new int[4];
    	newLocation[0]=location[0];
    	newLocation[1]=location[1];
    	newLocation[2]= location[0]+shop_scan_pop_lay.getWidth();
    	newLocation[3]= location[1]+shop_scan_pop_lay.getHeight();
    	return newLocation;
    }
    public int[] getQiandaoLocation(){
    	int[] location = new int[2];
    	main_fragment_module_qiandao_lay.getLocationOnScreen(location);
    	int[] newLocation = new int[4];
    	newLocation[0]=location[0];
    	newLocation[1]=location[1];
    	newLocation[2]= location[0]+main_fragment_module_qiandao_lay.getWidth();
    	newLocation[3]= location[1]+main_fragment_module_qiandao_lay.getHeight();
    	return newLocation;
    }
    
    
    
    private void initViewPager() {
        mAdList = new ArrayList<Notice>();
        mADsPagerAdapter = new ADsPagerAdapter(mAdList, getActivity2());
        mViewPager.setAdapter(mADsPagerAdapter);
        refreshADs();
    }

    private void refreshFragmentInfo() {
        //refresh personal gold coins
        refreshPersonalCoins();
        //refresh bg image
        changeBg();
        //refresh qiandao new message
        refreshQiandaoMessage();
        mGridListView.requestFocus();
    }

    private void refreshPersonalCoins() {
//        if (UserHelper.isGusetUser(getActivity2())) {
//            mTotalGetTv.setText("0");
//        } else {
//            mTotalGetTv.setText((UserHelper.getUser().getYcoins()+UserHelper.getUser().getFreezeYcoins()) + "");
//        }
    }

    private void refreshADs() {
        mADHandler.removeMessages(AD_START);
        List<Notice> lNoticeList = ADHelper.getADList(getActivity2());
        if (lNoticeList != null && lNoticeList.size() > 0) {
            mAdList.clear();
            mAdList.addAll(lNoticeList);
            mADsPagerAdapter.notifyDataSetChanged();
        } else {
            if (Utils.isNetAvailable(getActivity2())) {
                new GetADsThread(0).start();
            }
        }
        if (mAdList.size() > 1) {
            Notice firstNotice = mAdList.get(0);
            mAdList.add(firstNotice);
            mADHandler.sendEmptyMessageDelayed(AD_START, 3000);
            mNoticeTv.setVisibility(View.VISIBLE);
        } else {
            mNoticeTv.setVisibility(View.GONE);
        }


    }

    private void refreshLayVisiablity(int refreshType) {
//        if (refreshType == 1) {
//            mCustomListView.setVisibility(View.GONE);
//            mShopErrorLay.setVisibility(View.VISIBLE);
//            mShopLoadingBar.setVisibility(View.VISIBLE);
//            mShopTipLoadingTV.setText(getResources().getString(
//                    R.string.shop_data_loading));
//        } else if (refreshType == 2) {
//            mCustomListView.setVisibility(View.GONE);
//            mShopErrorLay.setVisibility(View.VISIBLE);
//            mShopLoadingBar.setVisibility(View.GONE);
//            mShopTipLoadingTV.setText(getResources().getString(
//                    R.string.please_click_try));
//        } else if (refreshType == 3) {
//            mCustomListView.setVisibility(View.VISIBLE);
//            mShopErrorLay.setVisibility(View.GONE);
//        } else if (refreshType == 4) {
//            mCustomListView.setVisibility(View.GONE);
//            mShopErrorLay.setVisibility(View.VISIBLE);
//            mShopTipLoadingTV.setText(getResources().getString(
//                    R.string.shop_no_network));
//            mShopLoadingBar.setVisibility(View.GONE);
//        }
    }

    public void initData() {
        changeBg();
        initViewPager();
        List<ViewPicture> viewPictureList = ShopDBHelper.getMiaoshaPicture(getActivity2());
        refreshMiaoshaPicture(viewPictureList);
        List<CateItem> goodsList = ShopDBHelper.getHighelestBackFee(getActivity2());
        mGoodsList.clear();
        if(goodsList!=null&&goodsList.size()>0)
        	  mGoodsList.addAll(goodsList);
        refreshInitData();
        if (mGoodsList != null && mGoodsList.size() > 0) {
            refreshLayVisiablity(3);
        } else {
            if (!Utils.isNetAvailable(getActivity2())) {
                refreshLayVisiablity(4);
                ToastHelper.show(getActivity2(), getActivity2()
                        .getResources().getString(R.string.net_is_unable));
                return;
            }

        }

        if (Utils.isNetAvailable(getActivity2())) {
            ShopHttpHelper.getCheapData(getActivity2(), false, mCurrentPageIndex,
                    mPageSize,
                    new ShopHttpHelper.ShopDataCallback() {

                        @Override
                        public void getMenu(List<LocalMenu> result) {
                        }

                        @Override
                        public void getChildsData(List<CateItem> result) {
                        	if (result != null && result.size() > 0) {
                        		
	                            doClassList(result);
	                            ShopDBHelper.insertHighelestData(getActivity2(), GsonHelper.gsonTojson(mGoodsList));
	                            refreshInitData();
                        	}
                        }
                    });
            ShopHttpHelper.getMaoshaPictureData(getActivity2(), false,
    				new MiaoshaPictureCallback(){
    					@Override
    					public void getChildsData(List<ViewPicture> result) {
    						refreshMiaoshaPicture(result);
    					}
            });
            
        }
        
        mHotPicView.initData();
        refreshGoldUseDot();
    }


    private void refreshInitData() {


        if (null == mGoodsAdapter) {
            mGoodsAdapter = new GoodsListAdapter(getActivity2(), mGoodsList);
//            mGoodsAdapter.setCurrentActivityRootView(mCurrentActivityView);
            mGridListView.setAdapter(mGoodsAdapter);
        } else {
//            mGoodsAdapter.setDataSource(mGoodsList);
        	mGoodsAdapter.notifyDataSetChanged();
        }
    }

    private void doClassList(List<CateItem> result) {
        if (result != null && result.size() > 0) {
            // 筛选出商品
            if (mGoodsList == null) {
                mGoodsList = new ArrayList<CateItem>();
            }
            mGoodsList.clear();
            mCurrentPageIndex = 2;
            for (CateItem item : result) {
                if (item.xType != null && item.xType == 3) {
                    mGoodsList.add(item);
                }
            }
            if (result.size() < mPageSize) {
            	mGridListView.setCanLoadMore2(false);
            } else {
            	mGridListView.setCanLoadMore2(true);
            }
        } else {
            // mGridListView.setCanLoadMore(false);
        }
    }
    
   
    
    
    
    
    
    private void doAddClassList(List<CateItem> result) {
        if (result != null && result.size() > 0) {
            mCurrentPageIndex++;
            // 筛选出商品
            if (mGoodsList == null) {
                mGoodsList = new ArrayList<CateItem>();
            }
            for (CateItem item : result) {
                if (item.xType != null && item.xType == 3) {
                    mGoodsList.add(item);
                }
            }
            if (result.size() < mPageSize) {
                mGridListView.setCanLoadMore2(false);
            } else {
                mGridListView.setCanLoadMore2(true);
            }
        } else {
            mGridListView.setCanLoadMore2(false);
        }
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.shop_main_show_shoppingcar:
                TaeSdkUtil.showShopMarket(getActivity2());
                break;
//            case R.id.main_fragment_header_bg_lay:
//                handleBgClick();
//                break;
            case R.id.main_fragment_float_window:
                Utils.goActivity(getActivity2(), ShowScanResultActivity.class);
                break;
            case R.id.shop_try_tv:
                initData();
                break;
//            case R.id.main_fragment_listview_header_scan_help:
//                handleGoScanHelpClick();
//                break;
            case R.id.shop_scan_pop_lay:
                Utils.goActivity(getActivity2(), ShowScanResultActivity.class);
                break;
//            case R.id.main_fragment_listview_header_scan_imageview:
//                Utils.goActivity(getActivity2(), CaptureActivity.class);
//                break;
            case R.id.main_fragment_module_qiandao_image:
                Utils.goActivity(getActivity2(), TaskActivity.class);
                break;
            case R.id.main_fragment_module_tixian_lay:
            	LightDBHelper.setGoldUseGuide(getActivity2(), true);
            	refreshGoldUseDot();
                Utils.goActivity(getActivity2(), GoldMallActivity.class);
                break;
            case R.id.main_fragment_module_rank_lay:
                Utils.goActivity(getActivity2(), RankActivity.class);
                break;
            case R.id.main_fragment_module_change_lay:
            	mBindHelper = new BindHelper(getActivity2());
    			mBindHelper.judgeBind();
                break;
            case R.id.miaosha_layout:
            	Utils.goActivity(getActivity2(), MiaoshaActivity.class);
            	break;
            case R.id.sfl_layout:
            	Utils.goActivity(getActivity2(), SflHintActivity.class);
            	break;
            case R.id.gfl_layout:
            	Utils.goActivity(getActivity2(), CheapListActivity.class);
            	break;

        }
    }

    private void refreshGoldUseDot(){
    	if(!LightDBHelper.getGoldUseGuide(getActivity2())){
    		mGoldMallNewDot.setVisibility(View.VISIBLE);
    	}else{
    		mGoldMallNewDot.setVisibility(View.GONE);
    	}
    }
    
    
    
//    BindHelper mBindHelper;


    private void handleBgClick() {
        if (mSeletcMainBGDialog == null) {
            mSeletcMainBGDialog = new SeletcMainBGDialog(getActivity2(), R.style.dialognotitle);
            mSeletcMainBGDialog.setSelectedCallback(this);
        }
        mSeletcMainBGDialog.show();
    }

    private void refreshQiandaoMessage() {

        boolean isShow = true;
        if (!Utils.isNeedToQianDao(getActivity2())) {
            isShow = false;
        }
        mQianDaoNewDot.setVisibility(isShow ? View.VISIBLE : View.GONE);
    }

    private void handleGoScanHelpClick() {
        Intent itn = new Intent(getActivity2(), HtmlActivity.class);
        itn.putExtra(GobalConstants.Data.URL,
        		GobalConstants.URL.YOYOBASE+GobalConstants.URL.SHOP_SCAN_HELP_URL);
        itn.putExtra(GobalConstants.Data.TITLE, this
                .getResources().getString(R.string.scan_title_describle));
        startActivity(itn);
    }


    private void showInputDialog() {
        // ShopScanInputActivity dialog = new
        // ShopScanInputActivity(ShopActivity.this,R.style.dialognotitle);
        // dialog.show();
    }

    private void refreshNotice() {
        new GetADsThread(0).start();
    }

    @Override
    public void onLoadMore() {
        if (!Utils.isNetAvailable(getActivity2())) {
            ToastHelper.show(getActivity2(),
                    getResources().getString(R.string.net_is_unable));
            mGridListView.onLoadMoreComplete();
            return;
        }
        ShopHttpHelper.getCheapData(getActivity2(), false, mCurrentPageIndex,
                mPageSize,
                new ShopHttpHelper.ShopDataCallback() {

                    @Override
                    public void getMenu(List<LocalMenu> result) {
                        // TODO Auto-generated method stub
                    }

                    @Override
                    public void getChildsData(List<CateItem> result) {
                        doAddClassList(result);
                        mGridListView.onLoadMoreComplete();
                    }
                });
        
    }

    @Override
    public void onRefresh() {
        mCurrentPageIndex = 1;
        if (!Utils.isNetAvailable(getActivity2())) {
            ToastHelper.show(getActivity2(),
                    getResources().getString(R.string.net_is_unable));
            mGridListView.onRefreshComplete();
            return;
        }
        mGridListView.setCanLoadMore2(false);
        ShopHttpHelper.getCheapData(getActivity2(), false, mCurrentPageIndex,
                mPageSize,
                new ShopHttpHelper.ShopDataCallback() {

                    @Override
                    public void getMenu(List<LocalMenu> result) {
                        // TODO Auto-generated method stub
                    }

                    @Override
                    public void getChildsData(List<CateItem> result) {
                        doClassList(result);
                        mGridListView.onRefreshComplete();
                    }
                });
        mHotPicView.initData();
        
        ShopHttpHelper.getMaoshaPictureData(getActivity2(), false,
				new MiaoshaPictureCallback(){
					@Override
					public void getChildsData(List<ViewPicture> result) {
						refreshMiaoshaPicture(result);
					}
        });
        
        
    }
    
    
    private void refreshMiaoshaPicture(List<ViewPicture> result){
	
    	if(result!=null&&result.size()>0){
    		DisplayImageOptions mOptions = new DisplayImageOptions.Builder()
    		.cacheInMemory(true).cacheOnDisk(true).considerExifParams(true)
    		.imageScaleType(ImageScaleType.IN_SAMPLE_POWER_OF_2)
    		.bitmapConfig(Bitmap.Config.RGB_565)
    		.build();
    	    for(int i=0;i<result.size();i++){
    	    	final ViewPicture pic = result.get(i);
    	    	if(i==0){
    	    		miaosha_hint1.setText(pic.getTitle());
    	    		miaosha_hint2.setText(pic.getNoteReplaceRule());
    	    		if(pic.getAttachmentList()!=null&&pic.getAttachmentList().size()>0){
    	    			ImageLoader.getInstance().displayImage(pic.getAttachmentList().get(0).getFileUrl()
							,miaosha_hintiv,mOptions);
    	    		}
    	    		miaosha_layout.setOnClickListener(new OnClickListener() {
						
						@Override
						public void onClick(View v) {
							if(pic.getJumpType()==ViewPicture.JUMETYPE_MIAOSHA){
								if(pic.getJumpValue()!=null&&pic.getJumpValue().equals("1")){
									Utils.goActivity(getActivity2(), MiaoshaActivity.class);
								}else {
									Utils.goActivity(getActivity2(), CheapListActivity.class);
								}
							}
						}
					});
    	    		
    	    		
    	    	}else if(i==1){
    	    		gfl_hint1.setText(pic.getTitle());
    	    		gfl_hint2.setText(pic.getNoteReplaceRule());
    	    		if(pic.getAttachmentList()!=null&&pic.getAttachmentList().size()>0){
    	    			ImageLoader.getInstance().displayImage(pic.getAttachmentList().get(0).getFileUrl()
							,gfl_hintiv,mOptions);
    	    		}
    	    		gfl_layout.setOnClickListener(new OnClickListener() {
						
						@Override
						public void onClick(View v) {
							if(pic.getJumpType()==ViewPicture.JUMETYPE_MIAOSHA){
								if(pic.getJumpValue()!=null&&pic.getJumpValue().equals("1")){
									Utils.goActivity(getActivity2(), MiaoshaActivity.class);
								}else {
									Utils.goActivity(getActivity2(), CheapListActivity.class);
								}
							}
						}
					});
    	    	}
    	    }
    	    
    	}
    	
    }
    

    @Override
    public void onSelected(int index) {
        changeBg();
    }

    private void changeBg() {/*
        int drawid = R.drawable.main_fragment_bg1;
        switch (LightDBHelper.getMainBgIndex(getActivity2())) {
            case 0:
                drawid = R.drawable.main_fragment_bg1;
                break;
            case 1:
                drawid = R.drawable.main_fragment_bg2;
                break;
            case 2:
                drawid = R.drawable.main_fragment_bg3;
                break;
            case 3:
                drawid = R.drawable.main_fragment_bg4;
                break;
        }

        mBgLay.setBackgroundResource((drawid));
    */}

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        position = position - 2;
        if (position >= 0 && mGoodsList != null && mGoodsList.size() > 0
                && mGoodsList.size() > position) {
            final CateItem item = mGoodsList.get(position);
            if (item != null) {
                if (item.clickType == 4) {
                    // 跳转到网页上的单品
                    Intent itn = new Intent(getActivity2(), HtmlActivity.class);
                    itn.putExtra(GobalConstants.Data.URL,
                            item.clickUrl == null ? "" : item.clickUrl);
                    itn.putExtra(GobalConstants.Data.TITLE,
                            item.pageTitle == null ? "" : item.pageTitle);
                    getActivity2().startActivity(itn);
                } else {
                    if (!LightDBHelper.getIsNotTipLoginOrNotFees(getActivity2())
                            && UserHelper.getUser().getIsGuest() == GobalConstants.UserType.ISSELF
                            && mCurrentActivityView != null) {
                        
                        ToastHelper.showGoLoginDialog(getActivity2(), new DialogCallBack() {
    						
    						@Override
    						public void clickSure() {
    							
    						}
    						
    						@Override
    						public void clickCancel() {
    							TaeSdkUtil.showTAEItemDetail(
                                        getActivity2(), item.tbItemId,
                                        item.taobaoPid, item.isTk == 1,
                                        item.itemType, null,mCurrentActivityView);
    						}
    					});
    					
                        
                        
                        
                        
                    } else {
                        TaeSdkUtil.showTAEItemDetail(getActivity2(),
                                item.tbItemId, item.taobaoPid, item.isTk == 1,
                                item.itemType, null,mCurrentActivityView);
                    }
                }
            }
        }
    }

    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState) {
    }

    @Override
    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
        if (mGridListView != null && mGridListView.getChildCount() > 0) {
            if (headerView.getTop() < -getActivity2().getResources().getDimensionPixelOffset(R.dimen.main_fragment_header_top_height)) {
                mFloatSearchView.setVisibility(View.VISIBLE);
            } else {
                mFloatSearchView.setVisibility(View.GONE);
            }
        }
    }

    class GetADsThread extends Thread {

        private Handler handler;
        private ResultDTO resultDTO;
        int pagesize;

        public GetADsThread(int pagesize) {
            handler = new Handler();
            this.pagesize = pagesize;
        }

        @Override
        public void run() {
            resultDTO = new GetADs(pagesize).getConnect();
            handler.post(new Runnable() {

                @Override
                public void run() {

                    if (resultDTO != null && resultDTO.getCode().equals("0")) {
                        List<Notice> noticeList = GsonHelper.gsonToObj(
                                resultDTO.getResult(),
                                new TypeToken<List<Notice>>() {
                                });
                        if (noticeList != null && noticeList.size() > 0) {
                            ADHelper.setADList(getActivity2(), noticeList);
                            refreshADs();
                        }
                    }
                }
            });
        }
    }
   

    BindHelper mBindHelper;
    @Override
    public void onDestroy() {
        super.onDestroy();
//        if (mBindHelper != null) mBindHelper.cancelJudge();
        if(mHotPicView!=null)
        mHotPicView.stopPlay();
        if(mBindHelper!=null)mBindHelper.cancelJudge();
    }
}
