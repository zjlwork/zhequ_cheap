package dianyun.zqcheap.fragment;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.wps.mlibary.intertor.TabPageIndicator;

import dianyun.baobaowd.adapter.ShopItemAdapter;
import dianyun.baobaowd.db.ShopDBHelper;
import dianyun.baobaowd.entity.CateItem;
import dianyun.baobaowd.entity.LocalMenu;
import dianyun.baobaowd.help.ShopHttpHelper;
import dianyun.baobaowd.help.ShopHttpHelper.ShopDataCallback;
import dianyun.baobaowd.util.TaeSdkUtil;
import dianyun.baobaowd.util.ToastHelper;
import dianyun.baobaowd.util.Utils;
import dianyun.zqcheap.R;

public class ShopFragment extends Fragment implements OnClickListener {

    // private View mSearchLay = null;
    private TabPageIndicator mTabPager;
    private ViewPager mViewPager;
    private ShopItemAdapter mAdapter;
    private TextView mShopTipLoadingTV = null;
    private LinearLayout mShopContentLay = null;
    private RelativeLayout mShopErrorLay = null;
    private ProgressBar mShopLoadingBar = null;
    private List<ShopListFragment> mFragmentList = new ArrayList<ShopListFragment>();
    private List<LocalMenu> mMenuList;
    private boolean mInit = false;
    private View mCurrentActivityView;
    // private RelativeLayout mMainTitleTV;
    // private RelativeLayout mScanImageView;
    Button mActivityBackBt;
    private ImageView mShowShopCar;
    private View mRoot;
    private Activity mActivity;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (null == mRoot) {
            mRoot = inflater.inflate(R.layout.shopfragment_lay, container,
                    false);
            initView();
        }
        ViewGroup vp = (ViewGroup) mRoot.getParent();
        if (vp != null) {
            vp.removeView(mRoot);
        }
        return mRoot;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        if (!mInit || (mMenuList == null || mMenuList.size() <= 0)) {
            initData();
            mInit = true;
        }
        super.onActivityCreated(savedInstanceState);
    }

    public void setCurrentActivityRootView(View activityView) {
        mCurrentActivityView = activityView;
    }

    public void initView() {
        // mScanImageView = (RelativeLayout)
        // findViewById(R.id.shop_scan_image);
        // mMainTitleTV = (RelativeLayout) findViewById(R.id.top_tv);
        mShowShopCar = (ImageView) mRoot.findViewById(R.id.shop_main_show_shoppingcar);
        mShowShopCar.setOnClickListener(this);
        mCurrentActivityView = mRoot.findViewById(R.id.root_view);
        mTabPager = (TabPageIndicator) mRoot.findViewById(R.id.shopTab);
        mViewPager = (ViewPager) mRoot.findViewById(R.id.shopVPager);
        mShopContentLay = (LinearLayout) mRoot.findViewById(R.id.shop_content_lay);
        mShopErrorLay = (RelativeLayout) mRoot.findViewById(R.id.shop_error_lay);
        mShopLoadingBar = (ProgressBar) mRoot.findViewById(R.id.shop_loading_progress);
        mShopTipLoadingTV = (TextView) mRoot.findViewById(R.id.shop_try_tv);
        mActivityBackBt = (Button) mRoot.findViewById(R.id.activityback_bt);
        mActivityBackBt.setOnClickListener(this);
        // mSearchLay = findViewById(R.id.search_layout);
        // mSearchLay.setOnClickListener(this);
        mShopTipLoadingTV.setOnClickListener(this);
        // mMainTitleTV.setOnClickListener(this);
        // mScanImageView.setOnClickListener(this);
    }

    /**
     * 1：初始化 2：初始化失败 3：成功加载到数据 4:网络连接失败
     *
     * @param refreshType
     */
    private void refreshLayVisiablity(int refreshType) {
        if (refreshType == 1) {
            // 初始化
            mShopContentLay.setVisibility(View.GONE);
            mShopErrorLay.setVisibility(View.VISIBLE);
            mShopLoadingBar.setVisibility(View.VISIBLE);
            mShopTipLoadingTV.setText(getActivity2().getResources().getString(
                    R.string.shop_data_loading));
        } else if (refreshType == 2) {
            mShopContentLay.setVisibility(View.GONE);
            mShopErrorLay.setVisibility(View.VISIBLE);
            mShopLoadingBar.setVisibility(View.GONE);
            mShopTipLoadingTV.setText(getActivity2().getResources().getString(
                    R.string.please_click_try));
        } else if (refreshType == 3) {
            mShopContentLay.setVisibility(View.VISIBLE);
            mShopErrorLay.setVisibility(View.GONE);
        } else if (refreshType == 4) {
            mShopContentLay.setVisibility(View.GONE);
            mShopErrorLay.setVisibility(View.VISIBLE);
            mShopTipLoadingTV.setText(getActivity2().getResources().getString(
                    R.string.shop_no_network));
            mShopLoadingBar.setVisibility(View.GONE);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        mActivity = activity;
        super.onAttach(activity);
    }

    public Activity getActivity2() {
        return mActivity;
    }

    public void initData() {
        refreshLayVisiablity(1);
        mMenuList = ShopDBHelper.getShopMenu(getActivity2());
        if (mMenuList != null && mMenuList.size() > 0) {
            refreshLayVisiablity(3);
            for (LocalMenu menu : mMenuList) {
                ShopListFragment fragment = new ShopListFragment(menu,
                        mMenuList.indexOf(menu) == 0);
                fragment.setCurrentActivityRootView(mCurrentActivityView);
                if (null == mFragmentList) {
                    mFragmentList = new ArrayList<ShopListFragment>();
                }
                mFragmentList.add(fragment);
            }
            if (mMenuList.size() == 1) {
                mTabPager.setVisibility(View.GONE);
            } else {
                mTabPager.setVisibility(View.VISIBLE);
            }
            mAdapter = new ShopItemAdapter(getFragmentManager(),
                    mFragmentList);
            mViewPager.setAdapter(mAdapter);
            mTabPager.setViewPager(mViewPager, 0);
            if (!Utils.isNetAvailable(getActivity2())) {
                return;
            }
            ShopHttpHelper.getMenu(getActivity2(), true,
                    new ShopDataCallback() {

                        @Override
                        public void getMenu(List<LocalMenu> result) {
                            if (!isSameToTarget(result)) {
                                mMenuList = result;
                                refreshInitData();
                            }
                        }

                        @Override
                        public void getChildsData(List<CateItem> result) {
                            // TODO Auto-generated method stub
                        }
                    });
        } else {
            if (!Utils.isNetAvailable(getActivity2())) {
                refreshLayVisiablity(4);
                ToastHelper.show(getActivity2(), getActivity2()
                        .getResources().getString(R.string.net_is_unable));
                return;
            }
            ShopHttpHelper.getMenu(getActivity2(), true,
                    new ShopDataCallback() {

                        @Override
                        public void getMenu(List<LocalMenu> result) {
                            //
                            if (result == null || result.size() <= 0) {
                                ToastHelper
                                        .show(getActivity2(),
                                                getActivity2()
                                                        .getResources()
                                                        .getString(
                                                                R.string.not_get_shop_data));
                                refreshLayVisiablity(2);
                            } else {
                                refreshLayVisiablity(3);
                            }
                            mMenuList = result;
                            refreshInitData();
                        }

                        @Override
                        public void getChildsData(List<CateItem> result) {
                            // TODO Auto-generated method stub
                        }
                    });
        }
    }

    private boolean isSameToTarget(List<LocalMenu> result) {
        boolean res = true;
        if (result != null && mMenuList != null) {
            if (result.size() != mMenuList.size()) {
                res = false;
            } else {
                for (int i = 0; i < result.size(); i++) {
                    if (result.get(i).cateId != mMenuList.get(i).cateId) {
                        res = false;
                        break;
                    }
                }
            }
        }
        return res;
    }

    private void refreshInitData() {
        if (mMenuList != null) {
            if (mFragmentList != null) {
                mFragmentList.clear();
            }
            for (LocalMenu menu : mMenuList) {
                ShopListFragment fragment = new ShopListFragment(menu,
                        mMenuList.indexOf(menu) == 0);
                fragment.setCurrentActivityRootView(mCurrentActivityView);
                if (null == mFragmentList) {
                    mFragmentList = new ArrayList<ShopListFragment>();
                }
                mFragmentList.add(fragment);
            }
            if (mMenuList.size() == 1) {
                mTabPager.setVisibility(View.GONE);
            } else {
                mTabPager.setVisibility(View.VISIBLE);
            }
            mAdapter = new ShopItemAdapter(getFragmentManager(),
                    mFragmentList);
            mViewPager.setAdapter(mAdapter);
            mAdapter.notifyDataSetChanged();
            // mTabPager.removeAllViews();
            // mTabPager.requestLayout();
            mTabPager.setViewPager(mViewPager, 0);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.shop_try_tv:
                initData();
                break;
            case R.id.top_tv:
                // case R.id.search_layout:
                // goActivity(ShopScanInputActivity.class);
                // break;
                // case R.id.shop_scan_image:
                // goActivity(CaptureActivity.class);
                // break;
            case R.id.activityback_bt:
                getActivity2().finish();
                break;
            case R.id.shop_main_show_shoppingcar:
                TaeSdkUtil.showShopMarket(getActivity2());
                break;
        }
    }

    private void goActivity(Class cls) {
        Intent itn = new Intent(getActivity2(), cls);
        getActivity2().startActivity(itn);
    }

    private void showInputDialog() {
        // ShopScanInputActivity dialog = new
        // ShopScanInputActivity(ShopActivity.this,R.style.dialognotitle);
        // dialog.show();
    }


}
