package dianyun.zqcheap.activity;

import java.util.ArrayList;
import java.util.List;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.text.ClipboardManager;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import dianyun.baobaowd.adapter.OrdersPagerAdapter;
import dianyun.baobaowd.adapter.OrdersPagerViewHelper;
import dianyun.baobaowd.data.UserShareUrl;
import dianyun.baobaowd.db.LightDBHelper;
import dianyun.baobaowd.defineview.ShareMenuPopup;
import dianyun.baobaowd.defineview.ShareMenuPopup.ShareMenuPopupClickCallback;
import dianyun.baobaowd.help.LogFile;
import dianyun.baobaowd.sinaweibo.SinaWeiboHelper;
import dianyun.baobaowd.util.GobalConstants;
import dianyun.baobaowd.util.NetworkStatus;
import dianyun.baobaowd.util.UserShareUrlHelper;
import dianyun.baobaowd.util.UserShareUrlHelper.GetShareUrlCallBack;
import dianyun.zqcheap.R;
import dianyun.zqcheap.activitybase.BaseActivity;
import dianyun.zqcheap.application.BaoBaoWDApplication;

public class OrderRecordActivity extends BaseActivity implements
		ShareMenuPopupClickCallback {

	private Button mActivityBackBt;
	public static final int PAGESIZE = 20;
	private TextView mUnRebateTv;
	// private TextView mAuditTv;
	private TextView mRebateTv;
	private TextView newestcount_tv;
	private View mUnRebateV;
	// private View mAuditV;
	private View mRebateV;
	public final static int REBATE = 1;
	public final static int UNREBATE = 0;
	// public final static int AUDIT = 1;
	public int mMode;

	public static final int SENDSTATUS_REBATE = 2;
	private ViewPager mViewPager;
	private OrdersPagerAdapter mOrdersPagerAdapter;
	private List<OrdersPagerViewHelper> mOrdersPagerViewHelperList = new ArrayList<OrdersPagerViewHelper>();
	private ShareMenuPopup mSharePop;
	private String mRebateShareUrl;
	private String mUnRebateShareUrl;
	private String mShareUrl;
	
	private View mCurrentActivityView;
	private boolean hasLocalShareUrl(){
		UserShareUrl unRebateShare=	UserShareUrlHelper.getUserShareUrlByKey(BaoBaoWDApplication.context,
				UserShareUrl.KEY_FANLISHAREORDERUNREBATE);
		UserShareUrl rebateShare=	UserShareUrlHelper.getUserShareUrlByKey(BaoBaoWDApplication.context,
				UserShareUrl.KEY_FANLISHAREORDER);
		
		mUnRebateShareUrl = unRebateShare==null?"":unRebateShare.getUrlValue();
		mRebateShareUrl = rebateShare==null?"":rebateShare.getUrlValue();
		if(!TextUtils.isEmpty(mRebateShareUrl)&&!TextUtils.isEmpty(mUnRebateShareUrl)){
			return true;
		}
		return false;
	}
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getTemplate().doInActivity(this, R.layout.orderrecordactivity);

	}

	public void refreshNewest() {
		int count = LightDBHelper.getFanliCount(OrderRecordActivity.this);
		if (count > 0) {
			newestcount_tv.setText(String.valueOf(count));
			newestcount_tv.setVisibility(View.VISIBLE);
		} else {
			newestcount_tv.setVisibility(View.GONE);
		}
	}

	float mReturnYuan;
	long mSeqId=-1;
	boolean mRebate;

	public void handleOnShareOnClick(float returnyuan,long seqId,boolean rebate) {
		mRebateShareUrl =mRebateShareUrl.replace("{seqId}", String.valueOf(seqId)); 
		mUnRebateShareUrl =mUnRebateShareUrl.replace("{seqId}", String.valueOf(seqId)); 
		if(rebate)mShareUrl= mRebateShareUrl;
		else mShareUrl= mUnRebateShareUrl;
		mRebate = rebate;
		mReturnYuan = returnyuan;
		mSeqId =seqId;
		if (mSharePop == null) {
			mSharePop = new ShareMenuPopup(this, findViewById(R.id.root_view));
			mSharePop.hideQQWx();
			mSharePop.setShareListener(this);
		}
		boolean isSinaLogin = LightDBHelper.getLoginType(this) == GobalConstants.LoginType.SINAWEIBO;
		mSharePop.show(isSinaLogin, false);
	}

	@Override
	public void handleShareOnClick(int i) {
		if (i == 2) {
			// qq
			bshareToQQFriend(getString(R.string.scorewall_sharetitle),
					String.format(getString(R.string.scorewall_shareqqcontent),
							mReturnYuan), mShareUrl, GobalConstants.URL.SHAREIMAGEURL);
		} else if (i == 3) {
			// qqzone
			bshareToQQSpace(getString(R.string.scorewall_sharetitle),
					String.format(getString(R.string.scorewall_shareqqcontent),
							mReturnYuan), mShareUrl, GobalConstants.URL.SHAREIMAGEURL);
		} else if (i == 4) {
			// sina
			// shareSinaweiboshare();
//			bsinaweiboshare(
//					getString(R.string.scorewall_sharetitle)
//							+ String.format(
//									getString(R.string.scorewall_shareweibocontent),
//									mReturnYuan), mShareUrl);
			
			SinaWeiboHelper.share(OrderRecordActivity.this, getString(R.string.scorewall_sharetitle)
					+ String.format(
							getString(R.string.scorewall_shareweibocontent),
							mReturnYuan),mShareUrl);
			
			
		} else if (i == 5) {
			// weixin
			boolean result = bshareWeixin(
					getString(R.string.scorewall_sharetitle), String.format(
							getString(R.string.scorewall_shareqqcontent),
							mReturnYuan), mShareUrl, false);

			// ShareHelper.toShareWeixin(OrderRecordActivity.this,getString(R.string.scorewall_sharetitle),
			// String.format(getString(R.string.scorewall_shareqqcontent),
			// mReturnYuan), "",
			// false);

		} else if (i == 6) {
			// weixin 朋友圈
			boolean result = bshareWeixin(
					getString(R.string.scorewall_sharetitle), String.format(
							getString(R.string.scorewall_shareqqcontent),
							mReturnYuan), mShareUrl, true);

			// ShareHelper.toShareWeixin(OrderRecordActivity.this,getString(R.string.scorewall_sharetitle),
			// String.format(getString(R.string.scorewall_shareqqcontent),
			// mReturnYuan), "",
			// true);

			// if(result)
			// Toast.makeText(InviteFriendActivity.this,
			// getString(R.string.sharesuccess), Toast.LENGTH_SHORT).show();
		} else if (i == 7) {
			// 复制

			ClipboardManager c = (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);
			c.setText(getString(R.string.scorewall_sharetitle)
					+ String.format(
							getString(R.string.scorewall_shareqqcontent),
							mReturnYuan)+mShareUrl);
			Toast.makeText(OrderRecordActivity.this,
					getString(R.string.copysuccess), Toast.LENGTH_SHORT).show();
		}
	}

	@Override
	public void initData() {
		super.initData();
		mCurrentActivityView = findViewById(R.id.root_view);
		mActivityBackBt = (Button) findViewById(R.id.activityback_bt);
		mActivityBackBt.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				finish();
			}
		});
		mRebateTv = (TextView) findViewById(R.id.rebate_tv);
		mUnRebateTv = (TextView) findViewById(R.id.unrebate_tv);
		newestcount_tv = (TextView) findViewById(R.id.newestcount_tv);
		refreshNewest();
		// mAuditTv = (TextView) findViewById(R.id.audit_tv);
		mRebateV = (View) findViewById(R.id.rebate_v);
		mUnRebateV = (View) findViewById(R.id.unrebate_v);
		// mAuditV = (View) findViewById(R.id.audit_v);
		initPageAdapter();
		changeSelectedColor(mUnRebateTv);

		mUnRebateTv.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				mViewPager.setCurrentItem(UNREBATE);
			}
		});
		// mAuditTv.setOnClickListener(new OnClickListener()
		// {
		//
		// @Override
		// public void onClick(View v)
		// {
		// mViewPager.setCurrentItem(AUDIT);
		// }
		// });
		mRebateTv.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				mViewPager.setCurrentItem(REBATE);
			}
		});

		mMode = getIntent().getIntExtra("mode", 0);
		mViewPager.setCurrentItem(mMode);
		setReceiver();
		if(!hasLocalShareUrl()){
			if (NetworkStatus.getNetWorkStatus(OrderRecordActivity.this) > 0){
				UserShareUrlHelper.getShareUrl(OrderRecordActivity.this,true,new GetShareUrlCallBack() {
					
					@Override
					public void getResult() {
						hasLocalShareUrl();
					}
				});
			}else
				Toast.makeText(OrderRecordActivity.this, getString(R.string.no_network),
						Toast.LENGTH_SHORT).show();
		}
	}

	private void initPageAdapter() {
		mViewPager = (ViewPager) findViewById(R.id.viewpager);
		mViewPager.setOffscreenPageLimit(2);
		OrdersPagerViewHelper lOrdersPagerViewHelper = new OrdersPagerViewHelper(
				OrderRecordActivity.this, UNREBATE);
		lOrdersPagerViewHelper.setCurrentActivityRootView(mCurrentActivityView);
		OrdersPagerViewHelper lOrdersPagerViewHelper2 = new OrdersPagerViewHelper(
				OrderRecordActivity.this, REBATE);
		lOrdersPagerViewHelper.setCurrentActivityRootView(mCurrentActivityView);
		// OrdersPagerViewHelper lOrdersPagerViewHelper3 = new
		// OrdersPagerViewHelper(
		// OrderRecordActivity.this, REBATE);
		mOrdersPagerViewHelperList.add(lOrdersPagerViewHelper);
		mOrdersPagerViewHelperList.add(lOrdersPagerViewHelper2);
		// mOrdersPagerViewHelperList.add(lOrdersPagerViewHelper3);
		mOrdersPagerAdapter = new OrdersPagerAdapter(
				mOrdersPagerViewHelperList, OrderRecordActivity.this);
		mViewPager.setAdapter(mOrdersPagerAdapter);
		mViewPager.setOnPageChangeListener(new OnPageChangeListener() {

			@Override
			public void onPageSelected(int position) {
				changeMode(position);
			}

			@Override
			public void onPageScrolled(int arg0, float arg1, int arg2) {
			}

			@Override
			public void onPageScrollStateChanged(int arg0) {
			}
		});
	}

	private void changeMode(int mode) {
		if (mode == REBATE)
			changeSelectedColor(mRebateTv);
		else if (mode == UNREBATE)
			changeSelectedColor(mUnRebateTv);
		// else
		// if (mode == AUDIT)
		// changeSelectedColor(mAuditTv);
	}

	private void changeSelectedColor(TextView pTextView) {
		if (pTextView.getId() == R.id.rebate_tv) {
			mRebateTv.setSelected(true);
			mUnRebateTv.setSelected(false);
			// mAuditTv.setSelected(false);
			mRebateV.setVisibility(View.VISIBLE);
			mUnRebateV.setVisibility(View.GONE);
			// mAuditV.setVisibility(View.GONE);
		} else if (pTextView.getId() == R.id.unrebate_tv) {
			mRebateTv.setSelected(false);
			mUnRebateTv.setSelected(true);
			// mAuditTv.setSelected(false);
			mRebateV.setVisibility(View.GONE);
			mUnRebateV.setVisibility(View.VISIBLE);
			// mAuditV.setVisibility(View.GONE);
		}
		// else
		// if (pTextView.getId() == R.id.audit_tv)
		// {
		// mRebateTv.setSelected(false);
		// mUnRebateTv.setSelected(false);
		// mAuditTv.setSelected(true);
		// mRebateV.setVisibility(View.GONE);
		// mUnRebateV.setVisibility(View.GONE);
		// mAuditV.setVisibility(View.VISIBLE);
		// }
	}

	@Override
	public void handleFavorOnClick() {
		// TODO Auto-generated method stub

	}

	@Override
	public void handleSwitchPageOnClick() {
		// TODO Auto-generated method stub

	}

	RefreshReceiver mRefreshReceiver;

	public class RefreshReceiver extends BroadcastReceiver {

		@Override
		public void onReceive(Context context, Intent intent) {
			byte refreshType = intent.getByteExtra(
					GobalConstants.Data.REFRESHTYPE, (byte) 0);
			if (refreshType == GobalConstants.RefreshType.SHAREORDERSUCCESS) {
				if(mSeqId!=-1)
				UserShareUrlHelper.shareOrder(OrderRecordActivity.this, false,
						String.valueOf(mSeqId),mRebate);
			}
		}
	}

	private void setReceiver() {
		try {
			if (null == mRefreshReceiver) {
				// 房间状态
				mRefreshReceiver = new RefreshReceiver();
				registerReceiver(mRefreshReceiver, new IntentFilter(
						GobalConstants.IntentFilterAction.REFRESH));
			}
		} catch (Exception e) {
			LogFile.SaveExceptionLog(e);
		}
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		if (mRefreshReceiver != null) {
			unregisterReceiver(mRefreshReceiver);
			mRefreshReceiver = null;
		}
	}

}
