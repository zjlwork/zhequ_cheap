package dianyun.zqcheap.activity;

import java.util.ArrayList;
import java.util.List;

import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ListView;

import com.google.gson.reflect.TypeToken;

import dianyun.baobaowd.adapter.RankAdapter;
import dianyun.baobaowd.data.User;
import dianyun.baobaowd.dto.ResultDTO;
import dianyun.baobaowd.gson.GsonHelper;
import dianyun.baobaowd.serverinterface.GetRank;
import dianyun.baobaowd.util.NetworkStatus;
import dianyun.baobaowd.util.UserHelper;
import dianyun.zqcheap.R;
import dianyun.zqcheap.activitybase.BaseActivity;

public class RankActivity extends BaseActivity {

	private ListView mListView;
	private List<User> mList;
	private RankAdapter mRankAdapter;
	private User mUser;
	private Button mActivityBackBt;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getTemplate().doInActivity(this, R.layout.rankactivity);
	}

	@Override
	public void findView() {
		super.findView();
		mUser = UserHelper.getUser();
		mActivityBackBt = (Button) findViewById(R.id.activityback_bt);
		mActivityBackBt.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				finish();
			}
		});

		mListView = (ListView) findViewById(R.id.listview);
		mListView.setCacheColorHint(0);
		mList = new ArrayList<User>();
		mRankAdapter = new RankAdapter(RankActivity.this, mList);
		mListView.setAdapter(mRankAdapter);
		if (NetworkStatus.getNetWorkStatus(RankActivity.this) > 0) {
			new GetRankThread().start();
		}

	}

	@Override
	public void initListener() {
		super.initListener();
	}

	class GetRankThread extends Thread {

		private Handler handler;
		private ResultDTO resultDTO;

		public GetRankThread() {
			handler = new Handler();

		}

		@Override
		public void run() {
			resultDTO = new GetRank(mUser.getUid(), mUser.getToken())
					.getConnect();

			handler.post(new Runnable() {

				@Override
				public void run() {

					if (resultDTO != null && resultDTO.getCode().equals("0")) {
						List<User> userList = GsonHelper.gsonToObj(
								resultDTO.getResult(),
								new TypeToken<List<User>>() {
								});
						mList.clear();
						if (userList != null)
							mList.addAll(userList);
						mRankAdapter.notifyDataSetChanged();
					}
				}
			});
		}
	}

	@Override
	public void onResume() {
		super.onResume();
	}

	@Override
	public void onPause() {
		super.onPause();
	}

}
