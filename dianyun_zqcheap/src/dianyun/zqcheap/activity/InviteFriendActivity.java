package dianyun.zqcheap.activity;

import android.os.Bundle;
import android.text.ClipboardManager;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import dianyun.baobaowd.data.UserShareUrl;
import dianyun.baobaowd.db.LightDBHelper;
import dianyun.baobaowd.defineview.ShareMenuPopup;
import dianyun.baobaowd.defineview.ShareMenuPopup.ShareMenuPopupClickCallback;
import dianyun.baobaowd.sinaweibo.SinaWeiboHelper;
import dianyun.baobaowd.util.GobalConstants;
import dianyun.baobaowd.util.NetworkStatus;
import dianyun.baobaowd.util.UserHelper;
import dianyun.baobaowd.util.UserShareUrlHelper;
import dianyun.baobaowd.util.UserShareUrlHelper.GetShareUrlCallBack;
import dianyun.zqcheap.R;
import dianyun.zqcheap.activitybase.BaseActivity;
import dianyun.zqcheap.application.BaoBaoWDApplication;

public class InviteFriendActivity extends BaseActivity implements
		OnClickListener, ShareMenuPopupClickCallback {

	private TextView mTopTv;
	private WebView mWebView;
	private Button mActivityBackBt;
	private TextView mInviteTv;
	private ShareMenuPopup mSharePop;
	private UserShareUrl mUserShareUrl;
	private UserShareUrl mUrl;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getTemplate().doInActivity(this, R.layout.invitefriendactivity);
	}

	@Override
	public void findView() {
		super.findView();
		mTopTv = (TextView) findViewById(R.id.top_tv);
		mWebView = (WebView) findViewById(R.id.webview);
		mInviteTv = (TextView) findViewById(R.id.invite_tv);
		mInviteTv.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (UserHelper.isGusetUser(InviteFriendActivity.this)) {
					UserHelper.gusestUserGo(InviteFriendActivity.this);
				} else {
					handleOnShareOnClick();
				}
			}
		});

		mWebView.requestFocus();
		mWebView.setScrollBarStyle(View.SCROLLBARS_OUTSIDE_OVERLAY);
		WebSettings setting = mWebView.getSettings();
		setting.setUserAgentString(setting.getUserAgentString() + " yoyo360/"
				+ BaoBaoWDApplication.versionName + " (yoyo360-android)");
		setting.setJavaScriptEnabled(true);
		setting.setSupportZoom(false);
		setting.setBuiltInZoomControls(true);

		mWebView.setWebViewClient(new WebViewClient() {

			@Override
			public boolean shouldOverrideUrlLoading(WebView view, String url) {
				// System.out.println("shouldOverrideUrlLoading url=====" +
				// url);
				return super.shouldOverrideUrlLoading(view, url);
			}
		});
		mWebView.clearCache(true);
		mActivityBackBt = (Button) findViewById(R.id.activityback_bt);
		mActivityBackBt.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (mWebView.canGoBack()) {
					mWebView.goBack();
				} else {
					finish();
				}
			}
		});
		mTopTv.setText(getString(R.string.invitefriend_togetprize));
		if(!hasLocalShareUrl()){
			mInviteTv.setVisibility(View.GONE);
			if (NetworkStatus.getNetWorkStatus(InviteFriendActivity.this) > 0){
				UserShareUrlHelper.getShareUrl(InviteFriendActivity.this,true,new GetShareUrlCallBack() {
					
					@Override
					public void getResult() {
						hasLocalShareUrl();
					}
				});
			}else
				Toast.makeText(InviteFriendActivity.this, getString(R.string.no_network),
						Toast.LENGTH_SHORT).show();
		}
		
		
	}
	
	private boolean hasLocalShareUrl(){
		UserShareUrl userShareUrl1=	UserShareUrlHelper.getUserShareUrlByKey(BaoBaoWDApplication.context,
				UserShareUrl.KEY_FANLIINVITEUSERLIST);
		UserShareUrl userShareUrl2=	UserShareUrlHelper.getUserShareUrlByKey(BaoBaoWDApplication.context,
				UserShareUrl.KEY_FANLIPROFILEAPP);
		String inviteUserListUrl = userShareUrl1==null?"":userShareUrl1.getUrlValue();
		String profileAppUrl = userShareUrl2==null?"":userShareUrl2.getUrlValue();
		if(!TextUtils.isEmpty(inviteUserListUrl)&&!TextUtils.isEmpty(profileAppUrl)){
			mUserShareUrl = userShareUrl2;
			mUrl = userShareUrl1;
			mInviteTv.setVisibility(View.VISIBLE);
			mWebView.loadUrl(mUrl.getUrlValue());
			return true;
		}
		return false;
	}
	
	
	

	private void handleOnShareOnClick() {
		if (mSharePop == null) {
			mSharePop = new ShareMenuPopup(this, mWebView);
			mSharePop.setShareListener(this);
		}
		boolean isSinaLogin = LightDBHelper.getLoginType(this) == GobalConstants.LoginType.SINAWEIBO;
		mSharePop.show(isSinaLogin, false);
	}

	@Override
	public void handleShareOnClick(int i) {
		if (i == 2) {
			// qq
			bshareToQQFriend(mUserShareUrl.getUrlTitle(),
					mUserShareUrl.getUrlContent(), mUserShareUrl.getUrlValue(),mUserShareUrl.getUrlIcon());
		} else if (i == 3) {
			// qqzone
			bshareToQQSpace(mUserShareUrl.getUrlTitle(),mUserShareUrl.getUrlContent(),
					mUserShareUrl.getUrlValue(),mUserShareUrl.getUrlIcon());
		} else if (i == 4) {
			// sina
			// shareSinaweiboshare();
//			bsinaweiboshare(mUserShareUrl.getUrlContent(),
//					mUserShareUrl.getUrlValue());
			
			SinaWeiboHelper.share(InviteFriendActivity.this, mUserShareUrl.getUrlContent(), mUserShareUrl.getUrlValue());
			
		} else if (i == 5) {
			// weixin
			boolean result = bshareWeixin(mUserShareUrl.getUrlTitle(),
					mUserShareUrl.getUrlContent(), mUserShareUrl.getUrlValue(),
					false);
			
//			ShareHelper.toShareWeixin(InviteFriendActivity.this,mUserShareUrl.getUrlTitle(),
//					mUserShareUrl.getUrlContent(), mUserShareUrl.getUrlValue(),
//					false);
			
			
		} else if (i == 6) {
			// weixin 朋友圈
			boolean result = bshareWeixin(mUserShareUrl.getUrlTitle(),
					mUserShareUrl.getUrlContent(), mUserShareUrl.getUrlValue(),
					true);
			
//			ShareHelper.toShareWeixin(InviteFriendActivity.this,mUserShareUrl.getUrlTitle(),
//					mUserShareUrl.getUrlContent(), mUserShareUrl.getUrlValue(),
//					true);
			
			
//			if(result)
//			Toast.makeText(InviteFriendActivity.this, getString(R.string.sharesuccess), Toast.LENGTH_SHORT).show();
		}else if (i == 7) {
			//复制
			
			ClipboardManager c= (ClipboardManager)getSystemService(CLIPBOARD_SERVICE);
			c.setText(getString(R.string.invite_copyhint)+mUserShareUrl.getUrlValue());
			Toast.makeText(InviteFriendActivity.this, getString(R.string.copysuccess), Toast.LENGTH_SHORT).show();
		}
	}

	@Override
	public void initListener() {
		super.initListener();
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if ((keyCode == KeyEvent.KEYCODE_BACK) && mWebView.canGoBack()) {
			mWebView.goBack();
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}

	@Override
	public void handleFavorOnClick() {
		// TODO Auto-generated method stub

	}

	@Override
	public void handleSwitchPageOnClick() {
		// TODO Auto-generated method stub

	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.share_iv:
			handleOnShareOnClick();
			break;
		}

	}
	
//	
//	Dialog mProgressDialog;
//	class GetShareUrlThread extends Thread {
//
//		private Handler handler;
//		private ResultDTO resultDTO;
//
//		public GetShareUrlThread() {
//			handler = new Handler();
//			mProgressDialog = DialogHelper.showProgressDialog(InviteFriendActivity.this,
//					getString(R.string.loginloading));
//		}
//
//		@Override
//		public void run() {
//			User mUser = UserHelper.getUser();
//			resultDTO = new GetShareUrl(mUser.getUid(), mUser.getToken()).getConnect();
//
//			handler.post(new Runnable() {
//
//				@Override
//				public void run() {
//					DialogHelper.cancelProgressDialog(mProgressDialog);
//					
//					if (resultDTO != null && resultDTO.getCode().equals("0")) {
//						List<UserShareUrl> lShareUrlList= GsonHelper.gsonToObj(resultDTO.getResult(),
//								new TypeToken<List<UserShareUrl>>(){});
//						UserShareUrlHelper.addUserShareUrlList(BaoBaoWDApplication.context, lShareUrlList);
//						hasLocalShareUrl();
//					}
//				}
//			});
//		}
//	}
	
	
	

	
	
}
