package dianyun.zqcheap.activity;

import java.util.Date;
import java.util.List;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.xkeuops.DynamicSdkManager;
import com.android.xkeuops.OWManager;
import com.google.gson.reflect.TypeToken;
import com.taobao.tae.sdk.callback.CallbackContext;
import com.umeng.analytics.MobclickAgent;

import dianyun.baobaowd.data.Notice;
import dianyun.baobaowd.data.User;
import dianyun.baobaowd.db.DBHelper;
import dianyun.baobaowd.db.LightDBHelper;
import dianyun.baobaowd.defineview.ResizeLayout;
import dianyun.baobaowd.defineview.ResizeLayout.OnResizeListener;
import dianyun.baobaowd.dto.ResultDTO;
import dianyun.baobaowd.gson.GsonHelper;
import dianyun.baobaowd.handler.DownloadThread;
import dianyun.baobaowd.handler.NotifyHandler;
import dianyun.baobaowd.handler.NotifyThread;
import dianyun.baobaowd.help.FileHelper;
import dianyun.baobaowd.help.LogFile;
import dianyun.baobaowd.interfaces.DialogCallBack;
import dianyun.baobaowd.serverinterface.GetADs;
import dianyun.baobaowd.util.ADHelper;
import dianyun.baobaowd.util.ConfigHelper;
import dianyun.baobaowd.util.DateHelper;
import dianyun.baobaowd.util.GobalConstants;
import dianyun.baobaowd.util.InviteHelper;
import dianyun.baobaowd.util.NetworkStatus;
import dianyun.baobaowd.util.TaeSdkUtil;
import dianyun.baobaowd.util.ToastHelper;
import dianyun.baobaowd.util.UserHelper;
import dianyun.baobaowd.util.UserShareUrlHelper;
import dianyun.baobaowd.util.Utils;
import dianyun.zqcheap.R;
import dianyun.zqcheap.activitybase.BaseActivity;
import dianyun.zqcheap.application.BaoBaoWDApplication;
import dianyun.zqcheap.fragment.MainFragment;
import dianyun.zqcheap.fragment.PersonCenterFragment;
import dianyun.zqcheap.fragment.ShopFragment2;
import dianyun.zqcheap.service.BaoBaoWDService;

public class MainActivity extends BaseActivity implements OnClickListener {

    private int outSwitch = 0;
    private boolean mIsRefresh = false;
    private FrameLayout mFrameLayout;
    private FragmentManager mFragmentManager;
    private FragmentTransaction mFragmentTransaction;
    private MainFragment mMainFragment;
    private PersonCenterFragment mPersonCenterFragment;
    private ShopFragment2 mShopFragment2;
    private User mUser;
    private ResizeLayout mCurrentActivityRootView;
    private TextView mNewestToastCountTv;
    private TextView mTab_Home_Tv;
    private TextView mTab_ShopMarket_Tv;
    private TextView mTab_Shop_Tv;
    private TextView mTab_Person_Tv;
    private ImageView mTab_Home_ImageView;
    private ImageView mTab_Shop_ImageView;
    private ImageView mTab_ShopMarket_ImageView;
    private ImageView mTab_Person_ImageView;
    private RelativeLayout mTabHomeLayout;
    private RelativeLayout mTabShopLayout;
    private RelativeLayout mTabShopMarketLayout;
    private RelativeLayout mTabPersonLayout;
    private RelativeLayout secondguide_layout;
    private Button secondguide_bt;
    private static Handler mHandler = getHandler();
    private RefreshReceiver mRefreshReceiver;

    public static Handler getHandler() {
        if (mHandler == null) {
            mHandler = new Handler();
        }
        return mHandler;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mUser = UserHelper.getUser();
        LightDBHelper.setIsMainActivityAlive(MainActivity.this, true);
        MobclickAgent.openActivityDurationTrack(false);
        MobclickAgent.onEvent(MainActivity.this, "startCount");
        MobclickAgent.onEventBegin(MainActivity.this, "appDuration");
        getTemplate().doInActivity(this, R.layout.mainactivity);
        if (NetworkStatus.getNetWorkStatus(MainActivity.this) > 0) {
            Utils.checkVersion(MainActivity.this);
            // 每次启动重新拉去一遍Config配置信息
            ConfigHelper.getConfigThread(this, mUser, null);
            UserShareUrlHelper.getShareUrl(MainActivity.this,false,null);
            new GetADsThread(0).start();
            InviteHelper.getInviteUid();
        }
       
    }

    private void firstGuideShow(){
    	 if(windowFocus&&ToastHelper.getBarHeight(MainActivity.this)!=0
    			 &&!LightDBHelper.getFirstGuide(MainActivity.this)){
			 LightDBHelper.setFirstGuide(MainActivity.this, true);
        	 if(mMainFragment!=null){
        		 ToastHelper.showGuideDialog(MainActivity.this,mCurrentActivityRootView
        				 , mMainFragment.getScanLocation(),mMainFragment.getQiandaoLocation());
        	 }
        	   
         }
    }
    @Override
    public void findView() {
        super.findView();
        mNewestToastCountTv = (TextView)findViewById(R.id.msgcenternewestcount_tv);
        mCurrentActivityRootView = (ResizeLayout)findViewById(R.id.root_view);
        mCurrentActivityRootView.setOnResizeListener(new OnResizeListener() {
			
			@Override
			public void OnResize(int w, int h, int oldw, int oldh) {
//				 System.out.println("setOnResizeListener======="+ToastHelper.getBarHeight(MainActivity.this));
				firstGuideShow();
			}
		});
        secondguide_bt = (Button) findViewById(R.id.secondguide_bt);
        String regiveTips = LightDBHelper.getRegGiveTips(MainActivity.this);
        if(!TextUtils.isEmpty(regiveTips))
        secondguide_bt.setText(regiveTips);
        secondguide_layout = (RelativeLayout) findViewById(R.id.secondguide_layout);
        mTabHomeLayout = (RelativeLayout) findViewById(R.id.main_tab_home);
        mTabShopLayout = (RelativeLayout) findViewById(R.id.main_tab_shop);
        mTabShopMarketLayout = (RelativeLayout) findViewById(R.id.main_tab_shopmarket);
        mTabPersonLayout = (RelativeLayout) findViewById(R.id.main_tab_person);

        mTab_Home_ImageView = (ImageView) findViewById(R.id.main_tab_home_iv);
        mTab_Shop_ImageView = (ImageView) findViewById(R.id.main_tab_shop_iv);
        mTab_ShopMarket_ImageView = (ImageView) findViewById(R.id.main_tab_shopmarket_iv);
        mTab_Person_ImageView = (ImageView) findViewById(R.id.main_tab_person_iv);

        mTab_Home_Tv = (TextView) findViewById(R.id.main_tab_home_tv);
        mTab_ShopMarket_Tv = (TextView) findViewById(R.id.main_tab_shopmarket_tv);
        mTab_Shop_Tv = (TextView) findViewById(R.id.main_tab_shop_tv);
        mTab_Person_Tv = (TextView) findViewById(R.id.main_tab_person_tv);
        mFrameLayout = (FrameLayout) findViewById(R.id.framelayout);
        Intent lIntent = new Intent(MainActivity.this, BaoBaoWDService.class);
        startService(lIntent);
    }


    @Override
    public void initData() {
        super.initData();
        setReceiver();
        mFragmentManager = getSupportFragmentManager();
        mTabShopLayout.setOnClickListener(this);
        mTabPersonLayout.setOnClickListener(this);
        mTabHomeLayout.setOnClickListener(this);
        mTabShopMarketLayout.setOnClickListener(this);
        secondguide_bt.setOnClickListener(this);
        refreshAllNew();
        goMainFragment();
        refreshSecondGuide();
        
    }
    boolean windowFocus = false;
    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
//        System.out.println("onWindowFocusChanged======="+ToastHelper.getBarHeight(MainActivity.this)+"=="+hasFocus);
         if(hasFocus){
        	 windowFocus= true;
         }
         firstGuideShow();
    }


    private void goShopCar() {
        TaeSdkUtil.showShopMarket(this);
    }

    private void goShopFragment() {
        outSwitch = 0;
        if (mShopFragment2 == null) {
        	mShopFragment2 = new ShopFragment2();
        }
        if (!mShopFragment2.isAdded()) {
            mFragmentTransaction = mFragmentManager.beginTransaction();
            mFragmentTransaction.replace(R.id.framelayout, mShopFragment2);
            mFragmentTransaction.commit();
            changeSelectedColor(mTab_Shop_Tv);
        }
    }

    public void goMainFragment() {
        outSwitch = 0;
        if (mMainFragment == null) {
            mMainFragment = new MainFragment();
            mMainFragment.setCurrentActivityRootView(mCurrentActivityRootView);

        }
        if (!mMainFragment.isAdded()) {
            mFragmentTransaction = mFragmentManager.beginTransaction();
            mFragmentTransaction.replace(R.id.framelayout, mMainFragment);
            mFragmentTransaction.commit();
            changeSelectedColor(mTab_Home_Tv);
        }
    }

    public void goPersonCenterFragment() {
        if (UserHelper.isGusetUser(MainActivity.this)) {
            UserHelper.gusestUserGo(MainActivity.this);
            return;
        }
        outSwitch = 0;
        if (mPersonCenterFragment == null) {
            mPersonCenterFragment = new PersonCenterFragment();
        }
        if (!mPersonCenterFragment.isAdded()) {
            mFragmentTransaction = mFragmentManager.beginTransaction();
            mFragmentTransaction.replace(R.id.framelayout, mPersonCenterFragment);
            mFragmentTransaction.commit();
            changeSelectedColor(mTab_Person_Tv);
        }
    }


    @Override
    protected void onResume() {
//    	System.out.println("onResume======="+ToastHelper.getBarHeight(MainActivity.this));
        if (mIsRefresh) {
            goMainFragment();
            mainInitUserAndRefresh();
            mIsRefresh = false;
        }
        refreshAllNew();
        super.onResume();
        outSwitch = 0;
     
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            if (outSwitch == 0) {
                outSwitch++;
                Toast.makeText(MainActivity.this, getString(R.string.againout),
                        Toast.LENGTH_SHORT).show();
                return false;
            }
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        LightDBHelper.setIsMainActivityAlive(MainActivity.this, false);
        DBHelper.getInstance(this).closeDB();
        MobclickAgent.onEventEnd(MainActivity.this, "appDuration");
//        System.out.println("mainactivity   onDestroy------------");
        if (mRefreshReceiver != null) {
            unregisterReceiver(mRefreshReceiver);
            mRefreshReceiver = null;
        }
        
        //有米回收资源
        DynamicSdkManager.getInstance(this).onAppDestroy();
        OWManager.getInstance(this).releaseOfferWall();
    }


    private void setReceiver() {
        try {
            if (null == mRefreshReceiver) {
                // 房间状态
                mRefreshReceiver = new RefreshReceiver();
                registerReceiver(mRefreshReceiver, new IntentFilter(
                        GobalConstants.IntentFilterAction.REFRESH));
            }
        } catch (Exception e) {
            LogFile.SaveExceptionLog(e);
        }
    }

    private void mainInitUserAndRefresh() {/*
        if (mMainFragment != null)
		{
			if (!mMainFragment.isResumed())
			{
				mMainFragment.setInitDataWhenResume(true);
			}
			else
			{
				mMainFragment.initUserAndRefresh();
			}
		}

		if (mCommunityFragment != null)
		{
			mCommunityFragment.changeUserRefresh();
		}

	*/
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.main_tab_home:
                goMainFragment();
                break;
            case R.id.main_tab_shop:
                goShopFragment();
                break;
            case R.id.main_tab_shopmarket:
                goShopCar();
                break;
            case R.id.main_tab_person:
                goPersonCenterFragment();
//            	startActivity(BindAlipayActivity.class);
                break;
            case R.id.secondguide_bt:
//            	goPersonCenterFragment();
            	LightDBHelper.setSecondGuide(MainActivity.this, true);
            	refreshSecondGuide();
            	UserHelper.gusestUserGo(MainActivity.this);
            	break;
        }
    }
    private void refreshSecondGuide(){
    	if(LightDBHelper.getSecondGuide(MainActivity.this)
    			||!UserHelper.isGusetUser(MainActivity.this)){
    		secondguide_layout.setVisibility(View.GONE);
    	}else{
    		secondguide_layout.setVisibility(View.VISIBLE);
    	}
    }

    public class RefreshReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            byte refreshType = intent.getByteExtra(
                    GobalConstants.Data.REFRESHTYPE, (byte) 0);
            if (refreshType == GobalConstants.RefreshType.USER) {
                mUser = UserHelper.getUser();
                mainInitUserAndRefresh();
                ToastHelper.cancelPopup();
                refreshSecondGuide();
            } else if (refreshType == GobalConstants.RefreshType.CHANGEUSER) {
                mUser = UserHelper.getUser();
                mainInitUserAndRefresh();
                Intent lIntent = new Intent(MainActivity.this,
                        BaoBaoWDService.class);
                startService(lIntent);
                ToastHelper.cancelPopup();
                refreshSecondGuide();
            } else if (refreshType == GobalConstants.RefreshType.LOGOUT) {
                Log.e("time", "logout");
                mIsRefresh = true;
                mUser = UserHelper.getUser();
                Intent lIntent = new Intent(MainActivity.this,
                        BaoBaoWDService.class);
                startService(lIntent);
                mainInitUserAndRefresh();
            }else if (refreshType == GobalConstants.RefreshType.ALLNEW)
			{
				refreshAllNew();
				if(mPersonCenterFragment!=null)
					mPersonCenterFragment.refreshNewest();
				
			}else if (refreshType == GobalConstants.RefreshType.REGIVETIPS)
			{
				String regivetips = intent.getStringExtra("REGIVETIPS");
				if(!TextUtils.isEmpty(regivetips))
				secondguide_bt.setText(regivetips);
				
				
			}else if (refreshType == GobalConstants.RefreshType.UPDATEVERSION)
			{
				if (NetworkStatus.getNetWorkStatus(MainActivity.this) > 0 )
				{
					final String url = intent
							.getStringExtra(GobalConstants.Version.URL);
					final String note = intent
							.getStringExtra(GobalConstants.Version.NOTE);
					final int lastPrivateVer = intent
							.getIntExtra(GobalConstants.Version.LASTPRIVATEVER,0);
					
					
					ToastHelper.showUpdataVersionDialog(MainActivity.this,
							note, new DialogCallBack()
							{

								@Override
								public void clickSure()
								{
									if(!LightDBHelper.getIsDownloadApk(MainActivity.this)){
										LightDBHelper.setIsDownloadApk(
												MainActivity.this, true);
										NotifyHandler lNotifyHandler = new NotifyHandler(
												MainActivity.this);
										FileHelper.sendMsg(0, lNotifyHandler);
										new DownloadThread(MainActivity.this,
												lNotifyHandler, url).start();
										new NotifyThread(MainActivity.this,
												lNotifyHandler).start();
									}
									
									
									if(lastPrivateVer>=BaoBaoWDApplication.versionCode){
										finish();
									}else{
										LightDBHelper.setCheckVersionTime(DateHelper.getTextByDate(new Date(),
												DateHelper.YYYY_MM_DD),MainActivity.this);
									}
								}

								@Override
								public void clickCancel()
								{
									if(lastPrivateVer>=BaoBaoWDApplication.versionCode){
										LightDBHelper.setCheckVersionTime("",MainActivity.this);
										finish();
									}else{
										LightDBHelper.setCheckVersionTime(DateHelper.getTextByDate(new Date(),
												DateHelper.YYYY_MM_DD),MainActivity.this);
									}
								}
							},lastPrivateVer);
				}
			}

        }
    }
    public void refreshAllNew()
	{
		if (!UserHelper.isGusetUser(MainActivity.this)&&(LightDBHelper.getSystemMsgCount(MainActivity.this) != 0
				|| LightDBHelper.getFanliCount(MainActivity.this) != 0
				|| LightDBHelper.getCashCount(MainActivity.this) != 0))
		{
			mNewestToastCountTv.setVisibility(View.VISIBLE);
			int count =  LightDBHelper.getSystemMsgCount(MainActivity.this)
					+ LightDBHelper.getFanliCount(MainActivity.this)
					+ LightDBHelper.getCashCount(MainActivity.this);
			if (count > 99)
				mNewestToastCountTv.setText(String.valueOf(99));
			else
				mNewestToastCountTv.setText(String.valueOf(count));
		}
		else
		{
			mNewestToastCountTv.setVisibility(View.GONE);
		}
	}
    private void changeSelectedColor(TextView pTextView) {


        if (pTextView.getId() == R.id.main_tab_home_tv) {
            mTab_Home_ImageView.setImageResource(R.drawable.tabmainpress);
            mTab_Shop_ImageView.setImageResource(R.drawable.tabcommunitynormal);
            mTab_ShopMarket_ImageView.setImageResource(R.drawable.tabmsgnormal);
            mTab_Person_ImageView.setImageResource(R.drawable.tabpersonalcenternormal);

            mTab_Home_Tv.setSelected(true);
            mTab_Shop_Tv.setSelected(false);
            mTab_ShopMarket_Tv.setSelected(false);
            mTab_Person_Tv.setSelected(false);

        } else if (pTextView.getId() == R.id.main_tab_shop_tv) {
            mTab_Home_ImageView.setImageResource(R.drawable.tabmainnormal);
            mTab_Shop_ImageView.setImageResource(R.drawable.tabcommunitypress);
            mTab_ShopMarket_ImageView.setImageResource(R.drawable.tabmsgnormal);
            mTab_Person_ImageView.setImageResource(R.drawable.tabpersonalcenternormal);

            mTab_Home_Tv.setSelected(false);
            mTab_Shop_Tv.setSelected(true);
            mTab_ShopMarket_Tv.setSelected(false);
            mTab_Person_Tv.setSelected(false);
        } else if (pTextView.getId() == R.id.main_tab_shopmarket_tv) {
            mTab_Home_ImageView.setImageResource(R.drawable.tabmainnormal);
            mTab_Shop_ImageView.setImageResource(R.drawable.tabcommunitynormal);
            mTab_ShopMarket_ImageView.setImageResource(R.drawable.tabmsgpress);
            mTab_Person_ImageView.setImageResource(R.drawable.tabpersonalcenternormal);

            mTab_Home_Tv.setSelected(false);
            mTab_Shop_Tv.setSelected(false);
            mTab_ShopMarket_Tv.setSelected(true);
            mTab_Person_Tv.setSelected(false);
        } else if (pTextView.getId() == R.id.main_tab_person_tv) {
            mTab_Home_ImageView.setImageResource(R.drawable.tabmainnormal);
            mTab_Shop_ImageView.setImageResource(R.drawable.tabcommunitynormal);
            mTab_ShopMarket_ImageView.setImageResource(R.drawable.tabmsgnormal);
            mTab_Person_ImageView.setImageResource(R.drawable.tabpersonalcenterpress);

            mTab_Home_Tv.setSelected(false);
            mTab_Shop_Tv.setSelected(false);
            mTab_ShopMarket_Tv.setSelected(false);
            mTab_Person_Tv.setSelected(true);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        CallbackContext.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
        if (mPersonCenterFragment != null) {
            mPersonCenterFragment.onActivityResult(requestCode, resultCode,
                    data);
        }
    }

    class GetADsThread extends Thread {

        private Handler handler;
        private ResultDTO resultDTO;
        int pagesize;

        public GetADsThread(int pagesize) {
            handler = new Handler();
            this.pagesize = pagesize;
        }

        @Override
        public void run() {
            resultDTO = new GetADs(pagesize).getConnect();
            handler.post(new Runnable() {

                @Override
                public void run() {

                    if (resultDTO != null && resultDTO.getCode().equals("0")) {
                        List<Notice> noticeList = GsonHelper.gsonToObj(
                                resultDTO.getResult(),
                                new TypeToken<List<Notice>>() {
                                });
                        if (noticeList != null && noticeList.size() > 0) {
                            ADHelper.setADList(MainActivity.this,noticeList);
                        }
                    }
                }
            });
        }
    }

}