package dianyun.zqcheap.activity;

import java.util.ArrayList;
import java.util.List;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.reflect.TypeToken;
import com.umeng.analytics.MobclickAgent;

import dianyun.baobaowd.adapter.PrizeAdapter;
import dianyun.baobaowd.adapter.PrizeAdapter.RefreshCallback;
import dianyun.baobaowd.data.Gift;
import dianyun.baobaowd.data.User;
import dianyun.baobaowd.dto.ResultDTO;
import dianyun.baobaowd.gson.GsonHelper;
import dianyun.baobaowd.serverinterface.GetGifts;
import dianyun.baobaowd.serverinterface.GetUserInfo;
import dianyun.baobaowd.util.DialogHelper;
import dianyun.baobaowd.util.GobalConstants;
import dianyun.baobaowd.util.NetworkStatus;
import dianyun.baobaowd.util.UserHelper;
import dianyun.baobaowd.util.WebInsideHelper;
import dianyun.zqcheap.R;
import dianyun.zqcheap.activitybase.BaseActivity;

public class PrizesActivity extends BaseActivity implements RefreshCallback
{

	Dialog mProgressDialog;
	private RelativeLayout mBottomLayout;
	private ListView mListView;
	private List<Gift> mList;
	private PrizeAdapter mPrizeAdapter;
	private RelativeLayout mTotalLayout;
	private User mUser;
	private TextView mGoldTv;
	private TextView mScoreTv;
	private TextView mScoreDesTv;
	private Button mExchangeBt;
	private boolean isCoinsEnough;
	private Button mActivityBackBt;
	private long mGiftId;
	private RelativeLayout mRoleLayout;

	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		getTemplate().doInActivity(this, R.layout.prizesactivity);
	}

	@Override
	public void findView()
	{
		super.findView();
		mTotalLayout = (RelativeLayout) findViewById(R.id.total_layout);
		mTotalLayout.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View v)
			{
			}
		});
		mUser = UserHelper.getUser();
		mScoreTv = (TextView) findViewById(R.id.score_tv);
		mScoreDesTv = (TextView) findViewById(R.id.scoredes_tv);
		mExchangeBt = (Button) findViewById(R.id.exchange_bt);
		mGoldTv = (TextView) findViewById(R.id.gold_tv);
		mGoldTv.setText("" + mUser.getYcoins());
		mActivityBackBt = (Button) findViewById(R.id.activityback_bt);
		mActivityBackBt.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View v)
			{
				finish();
			}
		});
		mRoleLayout = (RelativeLayout) findViewById(R.id.role_layout);
		mRoleLayout.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View v)
			{
				WebInsideHelper.goWebInside(PrizesActivity.this,
						GobalConstants.URL.WEBHTMLBASE
								+ GobalConstants.URL.GOLD,
						getString(R.string.goldrole));
			}
		});
		mListView = (ListView) findViewById(R.id.listview);
		mListView.setCacheColorHint(0);
		mList = new ArrayList<Gift>();
		mPrizeAdapter = new PrizeAdapter(mList, PrizesActivity.this);
		// mPrizeAdapter.setRefreshCallback(this);
		mListView.setAdapter(mPrizeAdapter);
		mListView.setOnItemClickListener(new OnItemClickListener()
		{

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1,
					int position, long arg3)
			{
				Gift lGift = mList.get(position);
				if (lGift.isSelected())
					lGift.setSelected(false);
				else
					lGift.setSelected(true);
				mPrizeAdapter.notifyDataSetChanged();
				refreshScore();
			}
		});
		mExchangeBt.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View v)
			{
				if (NetworkStatus.getNetWorkStatus(PrizesActivity.this) > 0)
				{
					Intent lIntent = new Intent(PrizesActivity.this,
							ExchangeActivity.class);
					lIntent.putParcelableArrayListExtra(
							GobalConstants.Data.GIFTLIST, getSelectedGiftList());
					startActivity(lIntent);
					finish();
				}
				else
				{
					Toast.makeText(PrizesActivity.this,
							getString(R.string.no_network), Toast.LENGTH_SHORT)
							.show();
				}
			}
		});
		mGiftId = getIntent().getLongExtra(GobalConstants.Data.GIFTID, 0L);
		if (NetworkStatus.getNetWorkStatus(PrizesActivity.this) > 0)
		{
			new GetGiftsThread(1000, 1).start();
			if (mUser.getIsSelf() == GobalConstants.AllStatusYesOrNo.YES)
			{
				new GetUserInfoThread(mUser.getUid()).start();
			}
		}
		else
		{
			Toast.makeText(PrizesActivity.this, getString(R.string.no_network),
					Toast.LENGTH_SHORT).show();
		}
		refreshScore();
	}

	@Override
	public void initListener()
	{
		super.initListener();
	}

	private int getGiftPosition(long giftId)
	{
		for (int i = 0; i < mList.size(); i++)
		{
			Gift gift = mList.get(i);
			if (gift.getGiftId().equals(giftId))
				return i;
		}
		return 0;
	}

	private String getGiftIds()
	{
		String giftids = "";
		for (Gift gift : mList)
		{
			if (gift.isSelected())
				giftids += gift.getGiftId() + ",";
		}
		if (giftids.indexOf(",") != -1)
			return giftids.substring(0, giftids.lastIndexOf(","));
		return giftids;
	}

	private ArrayList<Gift> getSelectedGiftList()
	{
		ArrayList<Gift> giftList = new ArrayList<Gift>();
		for (Gift gift : mList)
		{
			if (gift.isSelected())
				giftList.add(gift);
		}
		return giftList;
	}

	private int getSelectedPrice()
	{
		int count = 0;
		for (Gift gift : mList)
		{
			if (gift.isSelected())
				count += gift.getPrice();
		}
		return count;
	}

	private int getSelectedCount()
	{
		int count = 0;
		for (Gift gift : mList)
		{
			if (gift.isSelected())
				count++;
		}
		return count;
	}

	private void clearSelected()
	{
		for (Gift gift : mList)
		{
			if (gift.isSelected())
				gift.setSelected(false);
		}
		mPrizeAdapter.notifyDataSetChanged();
		refreshScore();
	}

	public void refreshScore()
	{
		int price = getSelectedPrice();
		String scoreDes = String.format(getString(R.string.choosegiftcount),
				getSelectedCount());
		mScoreDesTv.setText(scoreDes);
		mScoreTv.setText(String.valueOf(price));
		if (price == 0)
		{
			mExchangeBt.setEnabled(false);
			mExchangeBt.setSelected(false);
		}
		else
		{
			int lack = price - mUser.getYcoins();
			if (lack > 0)
			{
				String lackStr = String.format(
						getString(R.string.lackgoldcount), lack);
				// mLackTv.setText(lackStr);
				isCoinsEnough = false;
				mExchangeBt.setEnabled(false);
				mExchangeBt.setSelected(false);
			}
			else
			{
				isCoinsEnough = true;
				mExchangeBt.setEnabled(true);
				mExchangeBt.setSelected(true);
			}
		}
	}

	class GetUserInfoThread extends Thread
	{

		private Handler handler;
		private ResultDTO resultDTO;
		private long queryUid;
		private User user;

		public GetUserInfoThread(long queryUid)
		{
			handler = new Handler();
			this.queryUid = queryUid;
		}

		@Override
		public void run()
		{
			resultDTO = new GetUserInfo(mUser.getUid(), mUser.getToken(),
					queryUid).getConnect();
			handler.post(new Runnable()
			{

				@Override
				public void run()
				{
					if (resultDTO != null && resultDTO.getCode().equals("0"))
					{
						user = GsonHelper.gsonToObj(resultDTO.getResult(),
								User.class);
						if (user != null)
						{
							mUser.setYcoins(user.getYcoins());
							mUser.setLevel(user.getLevel());
							UserHelper.setUser(mUser);
							// UserHelper.updateUser(PrizesActivity.this,
							// mUser);
							mGoldTv.setText(mUser.getYcoins() + "");
						}
					}
				}
			});
		}
	}

	class GetGiftsThread extends Thread
	{

		private Handler handler;
		private ResultDTO resultDTO;
		private int pagesize;
		private int curPage;
		private List<Gift> giftList;

		public GetGiftsThread(int pagesize, int curPage)
		{
			handler = new Handler();
			mProgressDialog = DialogHelper.showProgressDialog(
					PrizesActivity.this, getString(R.string.loginloading));
			this.pagesize = pagesize;
			this.curPage = curPage;
		}

		@Override
		public void run()
		{
			resultDTO = new GetGifts(mUser.getUid(), mUser.getToken(),
					pagesize, curPage).getConnect();
			if (resultDTO != null && resultDTO.getCode().equals("0"))
			{
				giftList = GsonHelper.gsonToObj(resultDTO.getResult(),
						new TypeToken<List<Gift>>()
						{
						});
			}
			handler.post(new Runnable()
			{

				@Override
				public void run()
				{
					DialogHelper.cancelProgressDialog(mProgressDialog);
					if (giftList != null && giftList.size() > 0)
					{
						mList.clear();
						mList.addAll(giftList);
						mPrizeAdapter.notifyDataSetChanged();
						if (mGiftId != 0)
						{
							int position = getGiftPosition(mGiftId);
							mListView.setSelection(position);
						}
					}
				}
			});
		}
	}

	@Override
	public void onResume()
	{
		super.onResume();
		MobclickAgent.onPageStart("兑奖中心");
	}

	@Override
	public void onPause()
	{
		super.onPause();
		MobclickAgent.onPageEnd("兑奖中心");
	}

	@Override
	public void refreshGift()
	{
		refreshScore();
	}
}
