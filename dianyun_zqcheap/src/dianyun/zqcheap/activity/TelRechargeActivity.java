package dianyun.zqcheap.activity;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import dianyun.baobaowd.data.Gift;
import dianyun.baobaowd.data.ReceiverAddress;
import dianyun.baobaowd.data.User;
import dianyun.baobaowd.db.LightDBHelper;
import dianyun.baobaowd.dto.ResultDTO;
import dianyun.baobaowd.gson.GsonFactory;
import dianyun.baobaowd.gson.GsonHelper;
import dianyun.baobaowd.serverinterface.ExchangeGift;
import dianyun.baobaowd.serverinterface.GetHuaFei;
import dianyun.baobaowd.serverinterface.SetReceiveAddress;
import dianyun.baobaowd.util.BroadCastHelper;
import dianyun.baobaowd.util.DialogHelper;
import dianyun.baobaowd.util.GobalConstants;
import dianyun.baobaowd.util.ToastHelper;
import dianyun.baobaowd.util.UserHelper;
import dianyun.baobaowd.util.Utils;
import dianyun.zqcheap.R;

/**
 * Created by pc on 2015/5/19.
 */
public class TelRechargeActivity extends Activity implements View.OnClickListener {

    private Dialog mProgressDialog;
    private EditText mTelPhoneEdit;
    private TextView mRechargeCountTv;
    private TextView mMyYoYoCountTV;
    private Button mBackButton;
    //    private Button mFetchVaildBt;
    private Button mSubmitRechargeBt;
    private User mUser;
    private List<Gift> mList = new ArrayList<Gift>();
    private String[] mHuanFeiStringlist = null;
    private Dialog mSelectHuaFeiDialog = null;
    //    private ArrayAdapter<String> mAdapter;
    private int mSelectedIndex = 0;

    private int mCurrentPageIndex = 1;
    private int mPageSize = 20;
    private ReceiverAddress mOldReceiverAddress;
    private ArrayList<ReceiverAddress> mReceiverAddressList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        initView();
        super.onCreate(savedInstanceState);
        initData();

    }

    @Override
    protected void onResume() {
        mUser = UserHelper.getUser();
        super.onResume();
    }

    private void initView() {
        setContentView(R.layout.tel_recharge_lay);
        mBackButton = (Button) findViewById(R.id.activityback_bt);
        mBackButton.setOnClickListener(this);
        mMyYoYoCountTV = (TextView) findViewById(R.id.yocoins_tv);
        mTelPhoneEdit = (EditText) findViewById(R.id.tel_recharge_telphone_edt);
        mRechargeCountTv = (TextView) findViewById(R.id.tel_recharge_telphone_count);
        mSubmitRechargeBt = (Button) findViewById(R.id.tel_recharge_ok_bt);
        mRechargeCountTv.setOnClickListener(this);
        mSubmitRechargeBt.setOnClickListener(this);
        mSubmitRechargeBt.setEnabled(false);
        mSubmitRechargeBt.setSelected(false);
        mRechargeCountTv.setEnabled(false);
    }

    private void initData() {
        mUser = UserHelper.getUser();
        if (UserHelper.isGusetUser(this)) {
            mMyYoYoCountTV.setText("0");
        } else {
            mMyYoYoCountTV.setText("" + mUser.getYcoins());
        }
        //开始请求
        if (!Utils.isNetAvailable(this)) {
            ToastHelper.show(this, getResources().getString(R.string.net_is_unable));
            return;
        }
        //获取充值面额
        fetchHuaFeiMianEr();

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tel_recharge_ok_bt:
                exchangeAddHuaFei();
                break;
            case R.id.tel_recharge_telphone_count:
                showDialog();
                break;
            case R.id.activityback_bt:
                finish();
                break;
        }
    }

    private void fetchHuaFeiMianEr() {
        new GetHuaFeilistThread(mPageSize, mCurrentPageIndex).start();
    }

    private void exchangeAddHuaFei() {

        if (UserHelper.isGusetUser(this)) {
            ToastHelper.show(this, getString(R.string.huanfei_telphone_go_to_login));
            Utils.goActivity(this, LoginRegisterActivity.class);
            return;
        }
        if (TextUtils.isEmpty(mTelPhoneEdit.getText().toString().toString().trim())) {
            ToastHelper.show(this, getString(R.string.huanfei_telphone_is_not_valid));
            mTelPhoneEdit.requestFocus();
            mTelPhoneEdit.selectAll();
            return;
        }
        if (!Utils.isMobileNO(mTelPhoneEdit.getText().toString().toString().trim())) {
            ToastHelper.show(this, getString(R.string.huanfei_telphone_is_not_valid));
            mTelPhoneEdit.requestFocus();
            mTelPhoneEdit.selectAll();
            return;
        }
        new ExchangeGiftThread(String.valueOf(mList.get(mSelectedIndex).getGiftId()),
                mTelPhoneEdit.getText().toString().trim(), "", 0L).start();
    }

    class SetReceiveAddressThread extends Thread {

        private Handler handler;
        private ResultDTO resultDTO;
        private ReceiverAddress receivrerAddress;
        // private Long addrId;
        private String giftId;

        public SetReceiveAddressThread(ReceiverAddress receivrerAddress,
                                       String giftId) {
            handler = new Handler();
            this.receivrerAddress = receivrerAddress;
            this.giftId = giftId;
        }

        @Override
        public void run() {
            Gson gson = GsonFactory.getGsonInstance();
            resultDTO = new SetReceiveAddress(mUser.getUid(), mUser.getToken(),
                    gson.toJson(receivrerAddress)).postConnect();
            handler.post(new Runnable() {

                @Override
                public void run() {
                    if (resultDTO != null && resultDTO.getCode().equals("0")) {
                        receivrerAddress.setAddrId(Long.parseLong(resultDTO
                                .getResult()));
                        if (mReceiverAddressList == null) {
                            mReceiverAddressList = new ArrayList<ReceiverAddress>();
                            mReceiverAddressList.add(receivrerAddress);
                        }
                        Gson gson = GsonFactory.getGsonInstance();
                        LightDBHelper.setReceiveAddress(
                                gson.toJson(mReceiverAddressList),
                                TelRechargeActivity.this);
                        new ExchangeGiftThread(giftId, receivrerAddress
                                .getPhone(), receivrerAddress.getName(),
                                receivrerAddress.getAddrId()).start();
                    } else {
                        //设置地址失败 重置回原始数据 用于和新地址比较
                        Toast.makeText(TelRechargeActivity.this,
                                getString(R.string.exchangefailed),
                                Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }
    }

    class GetHuaFeilistThread extends Thread {

        private Handler handler;
        private ResultDTO resultDTO;
        private int pagesize;
        private int curPage;
        private List<Gift> giftList;

        public GetHuaFeilistThread(int pagesize, int curPage) {
            handler = new Handler();
            mProgressDialog = DialogHelper.showProgressDialog(
                    TelRechargeActivity.this, getString(R.string.loginloading));
            this.pagesize = pagesize;
            this.curPage = curPage;
        }

        @Override
        public void run() {
            resultDTO = new GetHuaFei(mUser.getUid(), mUser.getToken(),
                    pagesize, curPage).getConnect();
            if (resultDTO != null && resultDTO.getCode().equals("0")) {
                giftList = GsonHelper.gsonToObj(resultDTO.getResult(),
                        new TypeToken<List<Gift>>() {
                        });
            }
            handler.post(new Runnable() {

                @Override
                public void run() {
                    DialogHelper.cancelProgressDialog(mProgressDialog);
                    if (giftList != null && giftList.size() > 0) {

                        mList.clear();
                        mList.addAll(giftList);
                        fillStringToHuaFei();

                    }
                }
            });
        }
    }


    class ExchangeGiftThread extends Thread {

        private Handler handler;
        private ResultDTO resultDTO;
        private String giftId;
        private String phoneNumber;
        private String receiverName = "";
        private long addrId = 0L;

        public ExchangeGiftThread(String giftIdStr, String phoneNumber,
                                  String receiverName, long addrId) {
            handler = new Handler();
            this.giftId = giftIdStr;
            this.phoneNumber = phoneNumber;
            this.receiverName = receiverName;
            this.addrId = addrId;
            mProgressDialog = DialogHelper.showProgressDialog(
                    TelRechargeActivity.this, getString(R.string.loginloading));
        }

        @Override
        public void run() {
            resultDTO = new ExchangeGift(mUser.getUid(), mUser.getToken(),
                    giftId, phoneNumber, receiverName, addrId).postConnect();
            handler.post(new Runnable() {

                @Override
                public void run() {
                    DialogHelper.cancelProgressDialog(mProgressDialog);
                    if (resultDTO != null && resultDTO.getCode().equals("0")) {
                        int price = mList.get(mSelectedIndex).getFanliTaoPrice();
                        mUser.setYcoins(mUser.getYcoins() - price);
                        UserHelper.setUser(mUser);
                        BroadCastHelper.sendRefreshMainBroadcast(
                                TelRechargeActivity.this,
                                GobalConstants.RefreshType.USER);
                        Toast.makeText(TelRechargeActivity.this,
                                getString(R.string.huanfeichongzhi_success),
                                Toast.LENGTH_SHORT).show();
                        finish();
                    } else {
                        String errmsg = resultDTO.getErrorMsg();
                        if (TextUtils.isEmpty(errmsg)) {
                            errmsg = "";
                        } else {
                            errmsg = "(" + errmsg + ")";
                        }
                        Toast.makeText(TelRechargeActivity.this,
                                getString(R.string.huanfeichongzhi_failed) + errmsg,
                                Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }
    }

    private void fillStringToHuaFei() {
        if (mList != null && mList.size() > 0) {
            mHuanFeiStringlist = null;
            mHuanFeiStringlist = new String[mList.size()];
            for (int i = 0; i < mList.size(); i++) {
                Gift gift = mList.get(i);
                mHuanFeiStringlist[i] = (String.format(getResources().getString(R.string.huanfeichongzhi_select_money),
                        gift.getFanliTaoPrice() / 100));
            }
            mSubmitRechargeBt.setEnabled(true);
            mSubmitRechargeBt.setSelected(true);
            mRechargeCountTv.setEnabled(true);
            mRechargeCountTv.setText((String.format(getResources().getString(R.string.huanfeichongzhi_select_money),
                    mList.get(0).getFanliTaoPrice() / 100)));
        }


    }

    private void showDialog() {
        mSelectHuaFeiDialog = null;
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getResources().getString(R.string.huanfeichongzhi_select_money_title));
        builder.setSingleChoiceItems(mHuanFeiStringlist, mSelectedIndex, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                mSelectedIndex = which;
                mRechargeCountTv.setText((String.format(getResources().getString(R.string.huanfeichongzhi_select_money),
                        mList.get(which).getFanliTaoPrice() / 100)));
                mSelectHuaFeiDialog.dismiss();
            }
        });
        mSelectHuaFeiDialog = builder.create();

        mSelectHuaFeiDialog.show();
    }
}
