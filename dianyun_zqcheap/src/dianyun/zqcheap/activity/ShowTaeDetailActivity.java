package dianyun.zqcheap.activity;

import android.os.Bundle;
import dianyun.baobaowd.util.TaeSdkUtil;
import dianyun.zqcheap.R;
import dianyun.zqcheap.activitybase.BaseActivity;

public class ShowTaeDetailActivity extends BaseActivity
{

	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		getTemplate().doInActivity(ShowTaeDetailActivity.this, R.layout.showtaedetailactivity);
		
		 if(getIntent()!=null&&getIntent().getStringExtra("tbItemId")!=null){
				String tbItemId = getIntent().getStringExtra("tbItemId");
				String taobaoPid = getIntent().getStringExtra("taobaoPid");
				int isTk = getIntent().getIntExtra("isTk", 0);
				int itemType = getIntent().getIntExtra("itemType", 0);
				TaeSdkUtil.showTAEItemDetail(ShowTaeDetailActivity.this,
	        			tbItemId, taobaoPid,
						isTk == 1, itemType, null,findViewById(R.id.root_view));
				finish();
	        }
		
	}

	
}
