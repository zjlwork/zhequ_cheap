
package dianyun.zqcheap.activity;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;

import com.taobao.tae.sdk.callback.CallbackContext;

import dianyun.baobaowd.adapter.GoodsListAdapter;
import dianyun.baobaowd.defineview.xlistview.CustomListView;
import dianyun.baobaowd.defineview.xlistview.CustomListView.OnLoadMoreListener;
import dianyun.baobaowd.entity.CateItem;
import dianyun.baobaowd.entity.Constants;
import dianyun.baobaowd.entity.LocalMenu;
import dianyun.baobaowd.help.ShopHttpHelper;
import dianyun.baobaowd.util.ToastHelper;
import dianyun.baobaowd.util.Utils;
import dianyun.zqcheap.R;

public class GoodsListActivity extends Activity implements OnClickListener,
		dianyun.baobaowd.defineview.xlistview.CustomListView.OnRefreshListener,
		OnLoadMoreListener
{

	// private PullToRefreshScrollView mScrollView;
	private CustomListView mGridListView;
	private ImageView mToTopView;
	private View mListViewHeader;
	// private SpecialTopicView mSpecialView;
	private TextView mTitleTV;
	private String mTitleString = "";
	private Long mParentID = 1L;
	private List<CateItem> mGoodsList = new ArrayList<CateItem>();
	private GoodsListAdapter mAdapter;
	private int mCurrentPageIndex = 1;
	private int mPageSize = 20;
	private View mCurrentActivityRootView;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		setContentView(R.layout.only_goods_list_lay);
		initView();
		initData();
		super.onCreate(savedInstanceState);
	}

	private void initView()
	{
		// mScrollView = (PullToRefreshScrollView)
		// findViewById(R.id.goodslist_scrollview);
		// mScrollView.setMode(Mode.BOTH);
		mCurrentActivityRootView = findViewById(R.id.root_view);
		mGridListView = (CustomListView) findViewById(R.id.goodslist_lv);
		mGridListView.setCanLoadMore(false);
		mGridListView.setHeadViewBg(getResources().getColor(R.color.white));
		// mSpecialView = (SpecialTopicView)
		// findViewById(R.id.specialtopic_lay);
		mToTopView = (ImageView) findViewById(R.id.goodslist_to_top);
		mTitleTV = (TextView) findViewById(R.id.top_tv);
		// mSpecialView.setVisibility(View.GONE);
		mGridListView.setOnRefreshListener(this);
		mGridListView.setOnLoadListener(this);
		// mGridListView.setOnItemClickListener(this);
		mAdapter = new GoodsListAdapter(this, mGoodsList);
		mAdapter.setCurrentActivityRootView(mCurrentActivityRootView);
		mGridListView.setAdapter(mAdapter);
	}

	private void initData()
	{
		Intent itn = getIntent();
		if (itn != null)
		{
			mTitleString = itn.getStringExtra(Constants.EXTRA_NAME);
			mParentID = itn.getLongExtra(Constants.OBJECT_EXTRA_NAME, 0L);
		}
		if (null != (mTitleString))
		{
			mTitleTV.setText(mTitleString);
		}
		if (!Utils.isNetAvailable(this))
		{
			ToastHelper.show(this,
					getResources().getString(R.string.net_is_unable));
			return;
		}
		ShopHttpHelper.getMoreChildData(this, true, mCurrentPageIndex,
				mPageSize, String.valueOf(mParentID),
				new ShopHttpHelper.ShopDataCallback()
				{

					@Override
					public void getMenu(List<LocalMenu> result)
					{
						// TODO Auto-generated method stub
					}

					@Override
					public void getChildsData(List<CateItem> result)
					{
						doClassList(result);
						mAdapter.setDataSource(mGoodsList);
					}
				});
	}

	private void doClassList(List<CateItem> result)
	{
		if (result != null && result.size() > 0)
		{
			// 筛选出商品
			mGoodsList.clear();
			mCurrentPageIndex = 2;
			for (CateItem item : result)
			{
				if (item.xType != null && item.xType == 3)
				{
					mGoodsList.add(item);
				}
			}
			if (result.size() < mPageSize)
			{
				mGridListView.setCanLoadMore2(false);
			}
			else
			{
				mGridListView.setCanLoadMore2(true);
			}
		}
		else
		{
			// mGridListView.setCanLoadMore(false);
		}
	}

	private void doAddClassList(List<CateItem> result)
	{
		if (result != null && result.size() > 0)
		{
			mCurrentPageIndex++;
			// 筛选出商品
			if (mGoodsList == null)
			{
				mGoodsList = new ArrayList<CateItem>();
			}
			for (CateItem item : result)
			{
				if (item.xType != null && item.xType == 3)
				{
					mGoodsList.add(item);
				}
			}
			if (result.size() < mPageSize)
			{
				// ToastHelper.show(this,
				// getResources().getString(R.string.no_moredata));
				mGridListView.setCanLoadMore2(false);
			}
			else
			{
				mGridListView.setCanLoadMore2(true);
			}
		}
		else
		{
			mGridListView.setCanLoadMore2(false);
			// ToastHelper.show(this,
			// getResources().getString(R.string.no_moredata));
		}
	}

	@Override
	public void onClick(View v)
	{
		switch (v.getId())
		{
		case R.id.activityback_bt:
			// back
			finish();
			break;
		case R.id.goodslist_to_top:
			mGridListView.smoothToTop();
			break;
		}
	}

	@Override
	public void onLoadMore()
	{
		if (!Utils.isNetAvailable(this))
		{
			ToastHelper.show(this,
					getResources().getString(R.string.net_is_unable));
			mGridListView.onLoadMoreComplete();
			return;
		}
		ShopHttpHelper.getMoreChildData(this, false, mCurrentPageIndex,
				mPageSize, String.valueOf(mParentID),
				new ShopHttpHelper.ShopDataCallback()
				{

					@Override
					public void getMenu(List<LocalMenu> result)
					{
						// TODO Auto-generated method stub
					}

					@Override
					public void getChildsData(List<CateItem> result)
					{
						doAddClassList(result);
						mAdapter.setDataSource(mGoodsList);
						mGridListView.onLoadMoreComplete();
					}
				});
	}

	@Override
	public void onRefresh()
	{
		mCurrentPageIndex = 1;
		if (!Utils.isNetAvailable(this))
		{
			ToastHelper.show(this,
					getResources().getString(R.string.net_is_unable));
			mGridListView.onRefreshComplete();
			return;
		}
		mGridListView.setCanLoadMore2(false);
		ShopHttpHelper.getMoreChildData(this, false, mCurrentPageIndex,
				mPageSize, String.valueOf(mParentID),
				new ShopHttpHelper.ShopDataCallback()
				{

					@Override
					public void getMenu(List<LocalMenu> result)
					{
						// TODO Auto-generated method stub
					}

					@Override
					public void getChildsData(List<CateItem> result)
					{
						doClassList(result);
						mAdapter.setDataSource(mGoodsList);
						mGridListView.onRefreshComplete();
					}
				});
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data)
	{
		CallbackContext.onActivityResult(requestCode, resultCode, data);
	}
}
