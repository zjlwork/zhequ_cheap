package dianyun.zqcheap.activity;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.telephony.TelephonyManager;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import dianyun.baobaowd.data.User;
import dianyun.baobaowd.db.LightDBHelper;
import dianyun.baobaowd.dto.ResultDTO;
import dianyun.baobaowd.entity.Constants;
import dianyun.baobaowd.gson.GsonHelper;
import dianyun.baobaowd.serverinterface.BBLogin;
import dianyun.baobaowd.util.ActivityManager;
import dianyun.baobaowd.util.BroadCastHelper;
import dianyun.baobaowd.util.DialogHelper;
import dianyun.baobaowd.util.GobalConstants;
import dianyun.baobaowd.util.NetworkStatus;
import dianyun.baobaowd.util.TaeSdkUtil;
import dianyun.baobaowd.util.UserHelper;
import dianyun.baobaowd.util.UserStatusHelper;
import dianyun.baobaowd.xiaomipush.RegisterPushHelper;
import dianyun.zqcheap.R;
import dianyun.zqcheap.activitybase.BaseActivity;
import dianyun.zqcheap.service.BaoBaoWDService;

public class PhoneLoginActivity extends BaseActivity {

	private Button mActivityBackBt;
	private Button mOkBt;
	private EditText mPhoneNumEt;
	private EditText mPwEt;
	private TextView mForgetPwTv;
	private final int BBLOGIN = 9;

	private Handler mHandler = new Handler() {
		@Override
		public void handleMessage(Message msg) {

			switch (msg.what) {
			case BBLOGIN:
				ResultDTO resultDTO = (ResultDTO) msg.obj;
				handLoginResult(resultDTO);

				break;
			}
			super.handleMessage(msg);
		}
	};

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getTemplate().doInActivity(this, R.layout.phoneloginactivity);
		ActivityManager.pushActivity(PhoneLoginActivity.this);
	}

	@Override
	public void initData() {
		super.initData();
		mActivityBackBt = (Button) findViewById(R.id.activityback_bt);
		mOkBt = (Button) findViewById(R.id.ok_bt);
		mPhoneNumEt = (EditText) findViewById(R.id.phonenum_et);
		mPwEt = (EditText) findViewById(R.id.pw_et);
		mForgetPwTv = (TextView) findViewById(R.id.forgetpw_tv);
		mActivityBackBt.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				finish();
			}
		});

		mForgetPwTv.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				Intent lIntent = new Intent(PhoneLoginActivity.this,
						PhoneRegisteActivity.class);
				lIntent.putExtra(GobalConstants.Data.PHONEOPERATE,
						GobalConstants.Data.PHONECHANGEPW);
				startActivity(lIntent);
			}
		});

		mOkBt.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				String phone = mPhoneNumEt.getText().toString().trim();
				String pw = mPwEt.getText().toString().trim();
				if (phone.equals("") || pw.equals("")) {
					Toast.makeText(PhoneLoginActivity.this,
							getString(R.string.mailpwcannotnull),
							Toast.LENGTH_SHORT).show();
					return;
				}
				if (phone.contains("@") || phone.length() == 11) {
					if (NetworkStatus.getNetWorkStatus(PhoneLoginActivity.this) > 0) {
						bbLoginThread(phone, pw);
					} else {
						Toast.makeText(PhoneLoginActivity.this,
								getString(R.string.no_network),
								Toast.LENGTH_SHORT).show();
					}
				} else {
					Toast.makeText(PhoneLoginActivity.this,
							getString(R.string.gopageinputerror),
							Toast.LENGTH_SHORT).show();
				}
			}
		});

	}

	Dialog mProgressDialog;

	private void bbLoginThread(final String email, final String password) {
		mProgressDialog = DialogHelper.showProgressDialog(
				PhoneLoginActivity.this, getString(R.string.logining));
		mOkBt.setEnabled(false);
		mOkBt.setSelected(true);
		TelephonyManager tm = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
		final String DEVICE_ID = tm.getDeviceId();
		new Thread() {

			@Override
			public void run() {
				ResultDTO resultDTO = new BBLogin(PhoneLoginActivity.this,
						email, password, DEVICE_ID).postConnect();
				Message msg = new Message();
				msg.what = BBLOGIN;
				msg.obj = resultDTO;
				mHandler.sendMessage(msg);
			}
		}.start();
	}

	private void handLoginResult(ResultDTO resultDTO) {
		DialogHelper.cancelProgressDialog(mProgressDialog);
		mOkBt.setEnabled(true);
		mOkBt.setSelected(false);
		if (resultDTO != null) {
			if (resultDTO.getCode().equals("0")) {
				LightDBHelper.setLoginType(PhoneLoginActivity.this,
						GobalConstants.LoginType.PHONE);
				User mUser = GsonHelper.gsonToObj(resultDTO.getResult(),
						User.class);
				mUser.setIsSelf(GobalConstants.AllStatusYesOrNo.YES);
				UserStatusHelper.saveUserStatus(PhoneLoginActivity.this, mUser);
				UserHelper.addGusetUser(PhoneLoginActivity.this);
				UserHelper.guestCopyUser(PhoneLoginActivity.this, mUser);
				UserHelper.setUser(mUser);

				LightDBHelper.setPushShare(PhoneLoginActivity.this, true);
				Intent lIntent = new Intent(PhoneLoginActivity.this,
						BaoBaoWDService.class);
				stopService(lIntent);
				RegisterPushHelper.logout(PhoneLoginActivity.this);
				BroadCastHelper.sendRefreshMainBroadcast(
						PhoneLoginActivity.this,
						GobalConstants.RefreshType.CHANGEUSER);
				TaeSdkUtil.bindAccount(PhoneLoginActivity.this, mUser.getUid());
				if (mUser.getRegTime() == null || mUser.getRegTime().equals("")) {
					Intent intent = new Intent(this, StatusActivity.class);
					intent.putExtra(Constants.EXTRA_NAME, "welcome");
					startActivity(intent);
					overridePendingTransition(R.anim.push_left_in,
							R.anim.push_right_out);
				}

				ActivityManager.popAllActivity();
				finish();
			} else if (resultDTO.getCode().equals("-1011")) {
				Toast.makeText(PhoneLoginActivity.this,
						getString(R.string.pwerror), Toast.LENGTH_SHORT).show();
				DialogHelper.cancelProgressDialog(mProgressDialog);
			} else if (resultDTO.getCode().equals("-1012")) {
				Toast.makeText(PhoneLoginActivity.this,
						getString(R.string.errorcode_1012_hint),
						Toast.LENGTH_SHORT).show();
				DialogHelper.cancelProgressDialog(mProgressDialog);
			} else if (resultDTO.getCode().equals("-1002")) {
				Toast.makeText(PhoneLoginActivity.this,
						getString(R.string.usernotexist), Toast.LENGTH_SHORT)
						.show();
				DialogHelper.cancelProgressDialog(mProgressDialog);
			} else {
				Toast.makeText(PhoneLoginActivity.this,
						getString(R.string.loginfailed), Toast.LENGTH_SHORT)
						.show();
				DialogHelper.cancelProgressDialog(mProgressDialog);
			}
		} else {
			Toast.makeText(PhoneLoginActivity.this,
					getString(R.string.loginfailed), Toast.LENGTH_SHORT).show();
			DialogHelper.cancelProgressDialog(mProgressDialog);
		}
	}

}
