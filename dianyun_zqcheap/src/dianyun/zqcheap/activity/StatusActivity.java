package dianyun.zqcheap.activity;

import java.util.Date;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.umeng.analytics.MobclickAgent;

import dianyun.baobaowd.data.User;
import dianyun.baobaowd.data.UserStatus;
import dianyun.baobaowd.dto.ResultDTO;
import dianyun.baobaowd.entity.Constants;
import dianyun.baobaowd.serverinterface.UserAvatarSet;
import dianyun.baobaowd.serverinterface.UserSet;
import dianyun.baobaowd.util.ActivityManager;
import dianyun.baobaowd.util.BroadCastHelper;
import dianyun.baobaowd.util.CameraGalleryHelper;
import dianyun.baobaowd.util.DateHelper;
import dianyun.baobaowd.util.DialogHelper;
import dianyun.baobaowd.util.DialogHelper.EditDialogCallBack;
import dianyun.baobaowd.util.GobalConstants;
import dianyun.baobaowd.util.NetworkStatus;
import dianyun.baobaowd.util.UserHelper;
import dianyun.baobaowd.util.UserStatusHelper;
import dianyun.zqcheap.R;
import dianyun.zqcheap.activitybase.BaseActivity;

public class StatusActivity extends BaseActivity {

	Dialog mProgressDialog;

	private ImageView mAvatarIv;
	private TextView mNameDesTv;
	private User mUser;
	private Button mSaveBt;
	private RelativeLayout mNameLayout;
	private RelativeLayout mAvatarLayout;

	private int mPeriod;
	private int mCycle;

	private byte mGender;
	private String mPerinatal;
	private byte mBabyGender;
	private String mTempImgPath;
	private String mAvatarLocalPath;
	private String mNickName;
	private byte mSataus;
	private Button mBackBt;

	public static boolean mIsFromWelcome = false;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getTemplate().doInActivity(this, R.layout.statusactivity);
		MobclickAgent.onEvent(StatusActivity.this, "statusPage");

		mBackBt = (Button) findViewById(R.id.activityback_bt);
		mNameLayout = (RelativeLayout) findViewById(R.id.nickname_layout);
		mAvatarLayout = (RelativeLayout) findViewById(R.id.avatar_layout);
		mAvatarIv = (ImageView) findViewById(R.id.avatar_iv);
		mNameDesTv = (TextView) findViewById(R.id.nicknamedes_tv);
		mSaveBt = (Button) findViewById(R.id.save_bt);

		mBackBt.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				finish();
			}
		});
		mSaveBt.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (mNickName == null || mNickName.equals("")) {
					Toast.makeText(StatusActivity.this,
							getString(R.string.nicknamecannotbenull),
							Toast.LENGTH_SHORT).show();
					return;
				}
				if (NetworkStatus.getNetWorkStatus(StatusActivity.this) > 0) {
					mProgressDialog = DialogHelper.showProgressDialog(
							StatusActivity.this,
							getString(R.string.uploadinguserinfo));

					new UserSetThread(mGender, (byte) 0, "", mNickName, "")
							.start();
				} else {
					Toast.makeText(StatusActivity.this,
							getString(R.string.no_network), Toast.LENGTH_SHORT)
							.show();
				}
			}
		});
		mNameLayout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				String title = "";
				if (mNickName != null && !mNickName.equals("")) {
					title = getString(R.string.changenickname);
				} else {
					title = getString(R.string.setnickname);
				}
				DialogHelper.showChangeTextDialog(StatusActivity.this, title,
						mNickName, new EditDialogCallBack() {

							@Override
							public void clickOk(String edStr) {
								mNickName = edStr;
								mNameDesTv.setText(edStr);
							}

							@Override
							public void clickCancel() {
							}
						});
			}
		});
		mAvatarLayout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				showChooseAvatarDialog();
			}
		});

		initUser();
		Intent itn = getIntent();
		if (itn != null && itn.getStringExtra(Constants.EXTRA_NAME) != null
				&& itn.getStringExtra(Constants.EXTRA_NAME).equals("welcome")) {
			mIsFromWelcome = true;
		}
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		mIsFromWelcome = false;
	}

	private void initUser() {
		mUser = UserHelper.getUser();
		UserStatus lUserStatus = UserStatusHelper
				.getUserStatus(StatusActivity.this);
		if (lUserStatus.getType() == GobalConstants.DayArticleStatus.PREPARE) {
			mPeriod = lUserStatus.getPeriod();
			mCycle = lUserStatus.getCycle();
		}
		mGender = mUser.getGender() == null ? GobalConstants.Gender.MOTHER
				: mUser.getGender();
		mBabyGender = mUser.getBabyGender() == null ? GobalConstants.Gender.MOTHER
				: mUser.getBabyGender();
		if (mUser.getBabyBirthday() != null
				&& !mUser.getBabyBirthday().equals(""))
			mPerinatal = mUser.getBabyBirthday();

		mNickName = mUser.getNickname();
		if (mNickName != null)
			mNameDesTv.setText(mNickName);
		else
			mNameDesTv.setText("");
		UserHelper.showUserAvatar(StatusActivity.this, mUser, mAvatarIv);
		if (mPerinatal == null || mPerinatal.equals("")) {
			mSataus = GobalConstants.Status.PREPARE;
		} else {
			String nowTime = DateHelper.getTextByDate(new Date(),
					DateHelper.YYYY_MM_DD);

		}
		if (mNickName == null || mNickName.equals("")) {
			showChangeNameDialog(mNickName);
		}
		mBackBt.setVisibility(View.VISIBLE);

	}

	private void showChooseAvatarDialog() {
		View pview = getLayoutInflater().inflate(R.layout.chooseavatardialog,
				null);
		final Dialog dialog = new Dialog(StatusActivity.this,
				R.style.dialognotitle);
		dialog.setContentView(pview);
		dialog.show();
		RelativeLayout cameraLayout = (RelativeLayout) pview
				.findViewById(R.id.camera_layout);
		RelativeLayout albumsLayout = (RelativeLayout) pview
				.findViewById(R.id.albums_layout);
		Button cancelBt = (Button) pview.findViewById(R.id.cancel_bt);
		cameraLayout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				dialog.cancel();
				mTempImgPath = CameraGalleryHelper
						.startCamera(StatusActivity.this);
			}
		});
		albumsLayout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				dialog.cancel();
				CameraGalleryHelper.startGallery(StatusActivity.this);
			}
		});
		cancelBt.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				dialog.cancel();
			}
		});
	}

	private void showChangeNameDialog(String oldName) {
		View pview = getLayoutInflater().inflate(R.layout.changenamedialog,
				null);
		final Dialog dialog = new Dialog(StatusActivity.this,
				R.style.dialognotitle);
		dialog.setContentView(pview);
		dialog.show();
		final EditText edittext = (EditText) pview.findViewById(R.id.edittext);
		final TextView titleTv = (TextView) pview.findViewById(R.id.title_tv);
		if (oldName != null && !oldName.equals("")) {
			titleTv.setText(getString(R.string.changenickname));
			edittext.setText(oldName);
			edittext.setSelection(oldName.length());
		} else {
			titleTv.setText(getString(R.string.setnickname));
		}
		Button cancelBt = (Button) pview.findViewById(R.id.cancel_bt);
		Button sureBt = (Button) pview.findViewById(R.id.sure_bt);
		sureBt.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				dialog.cancel();
				mNickName = edittext.getText().toString();
				mNameDesTv.setText(mNickName);
			}
		});
		cancelBt.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				dialog.cancel();
			}
		});
		new ShowThread(edittext).start();
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		mAvatarLocalPath = CameraGalleryHelper.handleChangeAvatarResult(
				StatusActivity.this, requestCode, resultCode, data,
				mTempImgPath, mAvatarIv, mAvatarLocalPath);
	}

	class ShowThread extends Thread {

		Handler handler;
		EditText edittext;

		public ShowThread(EditText edittext) {
			handler = new Handler();
			this.edittext = edittext;
		}

		@Override
		public void run() {
			try {
				Thread.sleep(200);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			handler.post(new Runnable() {

				@Override
				public void run() {
					edittext.requestFocus();
					InputMethodManager inputManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
					inputManager.showSoftInput(edittext, 0);
				}
			});
		}
	}

	class UserSetThread extends Thread {

		private Handler handler;
		private ResultDTO userSetResultDTO;
		private ResultDTO avatarSetResultDTO;
		String jsonContent;
		private byte gender;
		private byte babyGender;
		private String babyBirthday;
		private String nickname;
		private String city;

		public UserSetThread(byte gender, byte babyGender, String babyBirthday,
				String nickname, String city) {
			handler = new Handler();
			this.gender = gender;
			this.babyGender = babyGender;
			this.babyBirthday = babyBirthday;
			this.nickname = nickname;
			this.city = city;
			mSaveBt.setEnabled(false);
		}

		@Override
		public void run() {
			userSetResultDTO = new UserSet(mUser.getUid(), mUser.getToken(),
					gender, babyGender, babyBirthday, nickname, city)
					.postConnect();
			if (userSetResultDTO != null
					&& userSetResultDTO.getCode().equals("0")) {
				if (mAvatarLocalPath != null)
					avatarSetResultDTO = new UserAvatarSet(mUser.getUid(),
							mUser.getToken(), mAvatarLocalPath).postConnect();
			}
			handler.post(new Runnable() {

				@Override
				public void run() {
					mSaveBt.setEnabled(true);
					if (userSetResultDTO != null
							&& userSetResultDTO.getCode().equals("0")) {
						if (mAvatarLocalPath != null) {
							if (avatarSetResultDTO != null
									&& avatarSetResultDTO.getCode().equals("0")) {
								mUser.setProfileImage(avatarSetResultDTO
										.getResult());
							}
						}
						mUser.setGender(gender);
						mUser.setBabyGender(babyGender);
						mUser.setBabyBirthday(babyBirthday);
						mUser.setNickname(nickname);
						mUser.setCity(city);
						UserHelper.setUser(mUser);
						UserHelper.guestCopyUser(StatusActivity.this, mUser);
						DialogHelper.cancelProgressDialog(mProgressDialog);

						ActivityManager.popAllActivity();
						BroadCastHelper.sendRefreshMainBroadcast(
								StatusActivity.this,
								GobalConstants.RefreshType.CHANGEUSER);

						Intent itn = new Intent(StatusActivity.this,
								MainActivity.class);
						startActivity(itn);
						overridePendingTransition(R.anim.push_left_in,
								R.anim.push_right_out);

						finish();
					} else {
						Toast.makeText(StatusActivity.this,
								getString(R.string.uploadinguserinfofailed),
								Toast.LENGTH_SHORT).show();
						DialogHelper.cancelProgressDialog(mProgressDialog);
					}
				}
			});
		}
	}

}
