package dianyun.zqcheap.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import dianyun.baobaowd.defineview.DianYunProductDialog;
import dianyun.baobaowd.defineview.DianYunProductDialog.RestartScanInterface;
import dianyun.baobaowd.entity.CateItem;
import dianyun.baobaowd.help.ShopHttpHelper;
import dianyun.baobaowd.help.ShopHttpHelper.ScanCheckCallback;
import dianyun.baobaowd.util.GobalConstants;
import dianyun.baobaowd.util.ToastHelper;
import dianyun.zqcheap.R;

public class ShopScanInputActivity extends Activity implements
		android.view.View.OnClickListener, ScanCheckCallback,
		RestartScanInterface
{

	private EditText mInputTextView;
	private Button mCloseImage;
	private LinearLayout mConfirmBt;
	private LinearLayout mScanHelpLay;
	private String mOriginUrl;
	private DianYunProductDialog mProductDialog;
	private View mContainer;
	private InputMethodManager mInputManager;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		setContentView(R.layout.shop_scan_input_activity);
		initView();
		super.onCreate(savedInstanceState);
	}

	private void initView()
	{
		mCloseImage = (Button) findViewById(R.id.activityback_bt);
		mInputTextView = (EditText) findViewById(R.id.shop_scan_input_edittext);
		mConfirmBt = (LinearLayout) findViewById(R.id.shop_scan_input_confirm_button);
		mContainer = findViewById(R.id.shop_scan_input_total_lay);
		mScanHelpLay = (LinearLayout) findViewById(R.id.scan_help);
		mScanHelpLay.setOnClickListener(this);
		mCloseImage.setOnClickListener(this);
		mConfirmBt.setOnClickListener(this);
		mInputManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
		mInputTextView.setFocusable(true);
		mInputTextView.setFocusableInTouchMode(true);
		mInputTextView.requestFocus();
		mInputManager.showSoftInputFromInputMethod(mInputTextView.getWindowToken(), InputMethodManager.SHOW_FORCED );
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event)
	{
		boolean res = true;
		switch (keyCode)
		{
		case KeyEvent.KEYCODE_BACK:
			if (mProductDialog != null && mProductDialog.isShowing())
			{
				mProductDialog.dismiss();
				res = false;
			}
			else
			{
				finish();
			}
			break;
		}
		if (res)
		{
			return super.onKeyDown(keyCode, event);
		}
		else
		{
			return true;
		}
	}

	@Override
	public void onClick(View v)
	{
		switch (v.getId())
		{
		case R.id.shop_scan_input_confirm_button:
			checkUrl();
			break;
		case R.id.activityback_bt:
			finish();
			break;
		case R.id.scan_help:
			Intent itn = new Intent(this, HtmlActivity.class);
			itn.putExtra(GobalConstants.Data.URL,
					GobalConstants.URL.YOYOBASE+GobalConstants.URL.SHOP_SCAN_HELP_URL);
			itn.putExtra(GobalConstants.Data.TITLE, this.getResources()
					.getString(R.string.scan_title_describle));
			this.startActivity(itn);
			break;
		}
	}

	private void checkUrl()
	{
		if (mInputTextView != null)
		{
			if (TextUtils.isEmpty(mInputTextView.getEditableText().toString()
					.trim()))
			{
				ToastHelper.showByGravity(
						this,
						getResources().getString(
								R.string.shop_scan_can_not_empty),
						Gravity.CENTER);
				return;
			}
			else
			{
				mInputTextView.clearFocus();
				mInputManager.hideSoftInputFromWindow(
						mInputTextView.getWindowToken(), 0);
				mOriginUrl = getHttpUrl(mInputTextView.getEditableText()
						.toString().trim());
				ShopHttpHelper.requestCheckUrl(this, mOriginUrl, this);
			}
		}
	}

	private String getHttpUrl(String url)
	{
		String res = url+"";
//        if (!TextUtils.isEmpty(url)) {
//            int index = url.indexOf("http");
//            if (index >= 0) {
//                res = url.substring(index);
//            }
//        }
//        Pattern pattern = Pattern.compile("^(http|www|ftp|)?(://)?(\\w+(-\\w+)*)(\\.(\\w+(-\\w+)*))*((:\\d+)?)(/(\\w+(-\\w+)*))*(\\.?(\\w)*)(\\?)?(((\\w*%)*(\\w*\\?)*(\\w*:)*(\\w*\\+)*(\\w*\\.)*(\\w*&)*(\\w*-)*(\\w*=)*(\\w*%)*(\\w*\\?)*(\\w*:)*(\\w*\\+)*(\\w*\\.)*(\\w*&)*(\\w*-)*(\\w*=)*)*(\\w*)*)$", Pattern.CASE_INSENSITIVE);
//        Matcher matcher = pattern.matcher(res);
//        if (matcher.find()) {
//            res = matcher.group();
//        }
		return res;
	}

	private void showScanResultDialog(CateItem item)
	{
		mProductDialog = null;
		mProductDialog = new DianYunProductDialog(this, item, mContainer,
				mOriginUrl);
		mProductDialog.setRescanText(getResources().getString(
				R.string.shop_scan_input_restart));
		mProductDialog.setOnRestartOnClickListener(this);
		mProductDialog.show();
	}

	@Override
	public void getCheckResult(CateItem item, String errmsg)
	{
		if (item != null)
		{
			mInputTextView.clearFocus();
			mInputManager.hideSoftInputFromWindow(
					mInputTextView.getWindowToken(), 0);
			showScanResultDialog(item);
		}
		else
		{
			if (TextUtils.isEmpty(errmsg))
			{
				errmsg = getResources().getString(
						R.string.shop_scan_controll_failed);
			}
			ToastHelper.showByGravity(this, errmsg, Gravity.CENTER);
		}
	}

	@Override
	protected void onResume()
	{
		super.onResume();
	}

	@Override
	public void restartScan()
	{
		mInputTextView.setText("");
		mProductDialog.dismiss();
		mInputTextView.requestFocus();
		mInputManager.showSoftInputFromInputMethod(mInputTextView.getWindowToken(), InputMethodManager.SHOW_FORCED );
	}

	@Override
	public void goBuy()
	{
	}

	@Override
	public void onDimiss()
	{
		mInputTextView.requestFocus();
		mInputManager.showSoftInputFromInputMethod(mInputTextView.getWindowToken(), InputMethodManager.SHOW_FORCED );
//		mInputManager.showSoftInputFromInputMethod(
//				mInputTextView.getWindowToken(), 0);
	}
}
