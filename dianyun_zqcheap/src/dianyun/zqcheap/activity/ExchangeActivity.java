package dianyun.zqcheap.activity;

import java.util.ArrayList;
import java.util.List;

import android.app.Dialog;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.umeng.analytics.MobclickAgent;

import dianyun.baobaowd.data.Gift;
import dianyun.baobaowd.data.ReceiverAddress;
import dianyun.baobaowd.data.User;
import dianyun.baobaowd.db.LightDBHelper;
import dianyun.baobaowd.dto.ResultDTO;
import dianyun.baobaowd.gson.GsonFactory;
import dianyun.baobaowd.gson.GsonHelper;
import dianyun.baobaowd.serverinterface.ExchangeGift;
import dianyun.baobaowd.serverinterface.GetReceiveAddress;
import dianyun.baobaowd.serverinterface.SetReceiveAddress;
import dianyun.baobaowd.util.BroadCastHelper;
import dianyun.baobaowd.util.DialogHelper;
import dianyun.baobaowd.util.DialogHelper.EditDialogCallBack;
import dianyun.baobaowd.util.GobalConstants;
import dianyun.baobaowd.util.NetworkStatus;
import dianyun.baobaowd.util.ProvinceWheelHelper;
import dianyun.baobaowd.util.UserHelper;
import dianyun.zqcheap.R;
import dianyun.zqcheap.activitybase.BaseActivity;

public class ExchangeActivity extends BaseActivity
{

	private User mUser;
	private Button mActivityBackBt;
	private EditText mConsigneeEt;
	private EditText mPhoneNumberEt;
	private EditText mZipcodeEt;
	private EditText mDetailAddressEt;
	private LinearLayout mGiftLayout;
	private InputMethodManager inputMethodManager;
	private List<Gift> mSelectedGiftList;
	private boolean downloading;
	private List<ReceiverAddress> mReceiverAddressList;
	private ReceiverAddress mOldReceiverAddress;
	private Button mOkButton;
	private RelativeLayout mProvinceLayout;
	private TextView mProvinceTv;
//	private String mAddress = "";
	private String mOldProvince= "";
	private String mNewProvince= "";

	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		getTemplate().doInActivity(this, R.layout.exchangeactivity);
	}

	@Override
	public void onStart()
	{
		super.onStart();
	}

	@Override
	public void onDestroy()
	{
		super.onDestroy();
	}

	@Override
	public void onResume()
	{
		super.onResume();
		MobclickAgent.onPageStart("兑换页面");
	}

	@Override
	public void onPause()
	{
		super.onPause();
		MobclickAgent.onPageEnd("兑换页面");
	}

	@Override
	public void initData()
	{
		inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
		mUser = UserHelper.getUser();
		mActivityBackBt = (Button) findViewById(R.id.activityback_bt);
		mOkButton = (Button) findViewById(R.id.ok_bt);
		mConsigneeEt = (EditText) findViewById(R.id.consignee_et);
		mPhoneNumberEt = (EditText) findViewById(R.id.phonenumber_et);
		mZipcodeEt = (EditText) findViewById(R.id.zipcode_et);
		mDetailAddressEt = (EditText) findViewById(R.id.detailaddress_et);
		mGiftLayout = (LinearLayout) findViewById(R.id.gift_layout);
		mProvinceLayout = (RelativeLayout) findViewById(R.id.province_layout);
		mProvinceTv = (TextView) findViewById(R.id.province_tv);
		mProvinceLayout.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View v)
			{
				ProvinceWheelHelper lProvinceWheelHelper = new ProvinceWheelHelper(
						ExchangeActivity.this, mNewProvince);
				lProvinceWheelHelper.showProvincePicker(ExchangeActivity.this,
						new EditDialogCallBack()
						{

							@Override
							public void clickOk(String edStr)
							{
								mNewProvince = edStr;
								mProvinceTv.setText(mNewProvince);
							}

							@Override
							public void clickCancel()
							{
							}
						});
			}
		});
		mActivityBackBt.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View v)
			{
				finish();
			}
		});
		mOkButton.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View v)
			{
				if (NetworkStatus.getNetWorkStatus(ExchangeActivity.this) > 0)
				{
					String newConsignee = mConsigneeEt.getText().toString().trim();
					String newPhoneNumber = mPhoneNumberEt.getText().toString().trim();
					String newZipcode = mZipcodeEt.getText().toString().trim();
					String newDetailAddress = mDetailAddressEt.getText().toString().trim();
					if (!TextUtils.isEmpty(newConsignee) && !TextUtils.isEmpty(newPhoneNumber)
							&& !TextUtils.isEmpty(newZipcode)
							&& !TextUtils.isEmpty(newDetailAddress))
					{
						if (newPhoneNumber.length() != 11)
						{
							Toast.makeText(ExchangeActivity.this,
									getString(R.string.errorphonenumber),
									Toast.LENGTH_SHORT).show();
							return;
						}
						if (newZipcode.length() != 6)
						{
							Toast.makeText(ExchangeActivity.this,
									getString(R.string.zipcodeerror),
									Toast.LENGTH_SHORT).show();
							return;
						}
						if (TextUtils.isEmpty(mNewProvince))
						{
							Toast.makeText(ExchangeActivity.this,
									getString(R.string.provinceerror),
									Toast.LENGTH_SHORT).show();
							return;
						}
						
						
						if (mOldReceiverAddress != null)
						{
							if (!isChanged(newConsignee,newPhoneNumber,newZipcode,newDetailAddress,mNewProvince))
							{
								new ExchangeGiftThread(getGiftIds(),
										newPhoneNumber, newConsignee,
										mOldReceiverAddress.getAddrId())
										.start();
							}
							else
							{
								mOldReceiverAddress.setName(newConsignee);
								mOldReceiverAddress.setPhone(newPhoneNumber);
								mOldReceiverAddress.setZip(newZipcode);
								mOldReceiverAddress.setAddress(newDetailAddress);
								mOldReceiverAddress.setUid(mUser.getUid());
								if(!TextUtils.isEmpty(mNewProvince)){
									String[] strArray = mNewProvince.split("-");
									mOldReceiverAddress.setState(strArray[0]);
									mOldReceiverAddress.setCity(strArray[1]);
									mOldReceiverAddress.setDistrict(strArray[2]);
								}
								new SetReceiveAddressThread(
										mOldReceiverAddress, getGiftIds())
										.start();
							}
						}
						else
						{
							mOldReceiverAddress = new ReceiverAddress(mUser
									.getUid(), newConsignee, newPhoneNumber,
									newZipcode, newDetailAddress);
							if(!TextUtils.isEmpty(mNewProvince)){
								String[] strArray = mNewProvince.split("-");
								mOldReceiverAddress.setState(strArray[0]);
								mOldReceiverAddress.setCity(strArray[1]);
								mOldReceiverAddress.setDistrict(strArray[2]);
							}
							new SetReceiveAddressThread(mOldReceiverAddress,
									getGiftIds()).start();
						}
					}
					else
					{
						Toast.makeText(ExchangeActivity.this,
								getString(R.string.incompleteInfo),
								Toast.LENGTH_SHORT).show();
					}
				}
				else
				{
					Toast.makeText(ExchangeActivity.this,
							getString(R.string.no_network), Toast.LENGTH_SHORT)
							.show();
				}
			}
		});
		mSelectedGiftList = getIntent().getParcelableArrayListExtra(
				GobalConstants.Data.GIFTLIST);
		if (mSelectedGiftList != null && mSelectedGiftList.size() > 0)
		{
			for (int i = 0; i < mSelectedGiftList.size(); i++)
			{
				Gift gift = mSelectedGiftList.get(i);
				View view = LayoutInflater.from(ExchangeActivity.this).inflate(
						R.layout.giftitem, null);
				ImageView giftIv = (ImageView) view.findViewById(R.id.gift_iv);
				TextView subjectTv = (TextView) view
						.findViewById(R.id.subject_tv);
				TextView needGoldTv = (TextView) view
						.findViewById(R.id.needgold_tv);
				subjectTv.setText(gift.getGiftDesc());
				needGoldTv.setText("" + gift.getFanliTaoPrice());
				if (gift.getImgUrl() != null && !gift.getImgUrl().equals(""))
				{
					DisplayImageOptions options = new DisplayImageOptions.Builder()
							.showImageOnLoading(R.drawable.defaultsmallimg)
							.showImageForEmptyUri(R.drawable.defaultsmallimg)
							.showImageOnFail(R.drawable.defaultsmallimg)
							.cacheInMemory(true).cacheOnDisk(true)
							.considerExifParams(true)
							.bitmapConfig(Bitmap.Config.RGB_565).build();
					ImageLoader.getInstance().displayImage(gift.getImgUrl(),
							giftIv, options);
				}
				else
				{
					giftIv.setImageResource(R.drawable.defaultsmallimg);
				}
				mGiftLayout.addView(view);
			}
		}
		try
		{
			String receiveAddressListStr = LightDBHelper
					.getReceiveAddress(ExchangeActivity.this);
			if (receiveAddressListStr != null
					&& !receiveAddressListStr.equals(""))
			{
				mReceiverAddressList = GsonHelper.gsonToObj(
						receiveAddressListStr,
						new TypeToken<List<ReceiverAddress>>()
						{
						});
			}
			if (mReceiverAddressList != null && mReceiverAddressList.size() > 0)
			{
				mOldReceiverAddress = mReceiverAddressList.get(0);
				setTvValue(mOldReceiverAddress);
			}
			else
			{
				mProvinceTv.setText(getString(R.string.setprovincehint));
				if (NetworkStatus.getNetWorkStatus(ExchangeActivity.this) > 0)
					new GetReceiveAddressThread().start();
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
	
	
	private  boolean isChanged(String newConsignee,String newPhoneNumber,String newZipcode,String newDetailAddress,String province){
		
		if(!newConsignee.equals(mOldReceiverAddress.getName()))return true;
		if(!newPhoneNumber.equals(mOldReceiverAddress.getPhone()))return true;
		if(!newZipcode.equals(mOldReceiverAddress.getZip()))return true;
		if(!newDetailAddress.equals(mOldReceiverAddress.getAddress()))return true;
		if(!province.equals(mOldReceiverAddress.getProvince()))return true;
		return false;
	}
	
	
	

	private void refreshReceiverAddress()
	{
		try
		{
			String receiveAddressListStr = LightDBHelper
					.getReceiveAddress(ExchangeActivity.this);
			if (receiveAddressListStr != null
					&& !receiveAddressListStr.equals(""))
			{
				mReceiverAddressList = GsonHelper.gsonToObj(
						receiveAddressListStr,
						new TypeToken<List<ReceiverAddress>>()
						{
						});
			}
			if (mReceiverAddressList != null && mReceiverAddressList.size() > 0)
			{
				mOldReceiverAddress = mReceiverAddressList.get(0);
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
	Dialog mProgressDialog;

	class GetReceiveAddressThread extends Thread
	{

		private Handler handler;
		private ResultDTO resultDTO;
		private List<ReceiverAddress> addressList;

		public GetReceiveAddressThread()
		{
			handler = new Handler();
			mProgressDialog = DialogHelper.showProgressDialog(
					ExchangeActivity.this, getString(R.string.gettingaddress));
			mConsigneeEt.setEnabled(false);
			mPhoneNumberEt.setEnabled(false);
			mZipcodeEt.setEnabled(false);
			mDetailAddressEt.setEnabled(false);
		}

		@Override
		public void run()
		{
			resultDTO = new GetReceiveAddress(mUser.getUid(), mUser.getToken())
					.getConnect();
			if (resultDTO != null && resultDTO.getCode().equals("0"))
			{
				addressList = GsonHelper.gsonToObj(resultDTO.getResult(),
						new TypeToken<List<ReceiverAddress>>()
						{
						});
			}
			handler.post(new Runnable()
			{

				@Override
				public void run()
				{
					DialogHelper.cancelProgressDialog(mProgressDialog);
					mConsigneeEt.setEnabled(true);
					mPhoneNumberEt.setEnabled(true);
					mZipcodeEt.setEnabled(true);
					mDetailAddressEt.setEnabled(true);
					if (resultDTO != null && resultDTO.getCode().equals("0"))
					{
						if (addressList != null && addressList.size() > 0)
						{
							mOldReceiverAddress = addressList.get(0);
							Gson gson = GsonFactory.getGsonInstance();
							LightDBHelper.setReceiveAddress(
									gson.toJson(addressList),
									ExchangeActivity.this);
							setTvValue(mOldReceiverAddress);
						}
					}
				}
			});
		}
	}
	
	
	
	
	public void setTvValue(ReceiverAddress receiverAddress){
		mConsigneeEt.setText(receiverAddress.getName());
		mPhoneNumberEt.setText(receiverAddress.getPhone());
		mZipcodeEt.setText(receiverAddress.getZip());
		mDetailAddressEt.setText(receiverAddress.getAddress());
		if (!TextUtils.isEmpty(receiverAddress.getState())
				&& !receiverAddress.getState().equals("no"))
		{
			mOldProvince = receiverAddress.getState() + "-"
					+ receiverAddress.getCity() + "-"
					+ receiverAddress.getDistrict();
			mProvinceTv.setText(mOldProvince);
			mNewProvince = mProvinceTv.getText().toString();
		}
		else
		{
			mProvinceTv.setText(getString(R.string.setprovincehint));
		}
	}
	
	
	
	
	
	
	

	class SetReceiveAddressThread extends Thread
	{

		private Handler handler;
		private ResultDTO resultDTO;
		private ReceiverAddress receivrerAddress;
		// private Long addrId;
		private String giftId;

		public SetReceiveAddressThread(ReceiverAddress receivrerAddress,
				String giftId)
		{
			handler = new Handler();
			this.receivrerAddress = receivrerAddress;
			this.giftId = giftId;
		}

		@Override
		public void run()
		{
			Gson gson = GsonFactory.getGsonInstance();
			resultDTO = new SetReceiveAddress(mUser.getUid(), mUser.getToken(),
					gson.toJson(receivrerAddress)).postConnect();
			handler.post(new Runnable()
			{

				@Override
				public void run()
				{
					if (resultDTO != null && resultDTO.getCode().equals("0"))
					{
						receivrerAddress.setAddrId(Long.parseLong(resultDTO
								.getResult()));
						if (mReceiverAddressList == null)
						{
							mReceiverAddressList = new ArrayList<ReceiverAddress>();
							mReceiverAddressList.add(receivrerAddress);
						}
						Gson gson = GsonFactory.getGsonInstance();
						LightDBHelper.setReceiveAddress(
								gson.toJson(mReceiverAddressList),
								ExchangeActivity.this);
						new ExchangeGiftThread(giftId, receivrerAddress
								.getPhone(), receivrerAddress.getName(),
								receivrerAddress.getAddrId()).start();
					}
					else
					{
						//设置地址失败 重置回原始数据 用于和新地址比较
						refreshReceiverAddress();
						Toast.makeText(ExchangeActivity.this,
								getString(R.string.exchangefailed),
								Toast.LENGTH_SHORT).show();
					}
				}
			});
		}
	}

	class ExchangeGiftThread extends Thread
	{

		private Handler handler;
		private ResultDTO resultDTO;
		private String giftId;
		private String phoneNumber;
		private String receiverName;
		private long addrId;

		public ExchangeGiftThread(String giftIdStr, String phoneNumber,
				String receiverName, long addrId)
		{
			handler = new Handler();
			this.giftId = giftIdStr;
			this.phoneNumber = phoneNumber;
			this.receiverName = receiverName;
			this.addrId = addrId;
		}

		@Override
		public void run()
		{
			resultDTO = new ExchangeGift(mUser.getUid(), mUser.getToken(),
					giftId, phoneNumber, receiverName, addrId).postConnect();
			handler.post(new Runnable()
			{

				@Override
				public void run()
				{
					if (resultDTO != null && resultDTO.getCode().equals("0"))
					{
						int price = getSelectedPrice();
						mUser.setYcoins(mUser.getYcoins() - price);
						UserHelper.setUser(mUser);
						BroadCastHelper.sendRefreshMainBroadcast(
								ExchangeActivity.this,
								GobalConstants.RefreshType.USER);
						Toast.makeText(ExchangeActivity.this,
								getString(R.string.exchangesuccess),
								Toast.LENGTH_SHORT).show();
						finish();
					}
					else
					{
						Toast.makeText(ExchangeActivity.this,
								getString(R.string.exchangefailed),
								Toast.LENGTH_SHORT).show();
					}
				}
			});
		}
	}

	private int getSelectedPrice()
	{
		int count = 0;
		if (mSelectedGiftList != null)
		{
			for (Gift gift : mSelectedGiftList)
			{
				count += gift.getFanliTaoPrice();
			}
		}
		return count;
	}

	private String getGiftIds()
	{
		String giftids = "";
		if (mSelectedGiftList != null)
		{
			for (Gift gift : mSelectedGiftList)
			{
				giftids += gift.getGiftId() + ",";
			}
			if (giftids.indexOf(",") != -1)
				return giftids.substring(0, giftids.lastIndexOf(","));
		}
		return giftids;
	}
}
