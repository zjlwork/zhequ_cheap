package dianyun.zqcheap.activity;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import dianyun.baobaowd.data.User;
import dianyun.baobaowd.db.LightDBHelper;
import dianyun.baobaowd.dto.ResultDTO;
import dianyun.baobaowd.gson.GsonHelper;
import dianyun.baobaowd.serverinterface.GetPhotoAutoSMS;
import dianyun.baobaowd.serverinterface.PhotoChangePw;
import dianyun.baobaowd.serverinterface.PhotoRegiste;
import dianyun.baobaowd.util.ActivityManager;
import dianyun.baobaowd.util.BroadCastHelper;
import dianyun.baobaowd.util.DialogHelper;
import dianyun.baobaowd.util.GobalConstants;
import dianyun.baobaowd.util.NetworkStatus;
import dianyun.baobaowd.util.TaeSdkUtil;
import dianyun.baobaowd.util.ToastHelper;
import dianyun.baobaowd.util.UserHelper;
import dianyun.baobaowd.util.UserStatusHelper;
import dianyun.baobaowd.xiaomipush.RegisterPushHelper;
import dianyun.zqcheap.R;
import dianyun.zqcheap.activitybase.BaseActivity;
import dianyun.zqcheap.application.BaoBaoWDApplication;
import dianyun.zqcheap.service.BaoBaoWDService;

public class PhoneRegisteActivity extends BaseActivity {

	private Button mActivityBackBt;
	private Button mOkBt;
	private Button mGetCodeBt;
	private EditText mPhoneNumEt;
	private EditText mAuthCodeEt;
	private EditText mPwEt;
	private String mPhotoNumber;
	private String mOperate;
	private TextView titleTv;

	private static final int SENDCODE = 1;
	private static final int PHONEREGISTE = 2;

	private Handler mHandler = new Handler() {
		@Override
		public void handleMessage(Message msg) {

			switch (msg.what) {
			case SENDCODE:
				DialogHelper.cancelProgressDialog(mProgressDialog);

				ResultDTO resultDTO = (ResultDTO) msg.obj;
				if (resultDTO != null && resultDTO.getCode().equals("0")) {
					Toast.makeText(PhoneRegisteActivity.this,
							getString(R.string.autocodewillcome),
							Toast.LENGTH_SHORT).show();
					new CountDownTimerThread().start();
				} else {
					Toast.makeText(PhoneRegisteActivity.this,
							getString(R.string.getautocodefailed),
							Toast.LENGTH_SHORT).show();
					mGetCodeBt.setEnabled(true);
					mGetCodeBt.setSelected(false);
				}

				break;
			case PHONEREGISTE:
				ResultDTO resultDTO2 = (ResultDTO) msg.obj;
				handRegisteResult(resultDTO2);
				break;
			}
			super.handleMessage(msg);
		}
	};

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getTemplate().doInActivity(this, R.layout.phoneregisteactivity);
	}
	boolean justChangePw;
	@Override
	public void initData() {
		super.initData();
		mActivityBackBt = (Button) findViewById(R.id.activityback_bt);
		mOkBt = (Button) findViewById(R.id.ok_bt);
		mGetCodeBt = (Button) findViewById(R.id.getcode_bt);
		mPhoneNumEt = (EditText) findViewById(R.id.phonenum_et);
		mAuthCodeEt = (EditText) findViewById(R.id.authcode_et);
		mPwEt = (EditText) findViewById(R.id.pw_et);
		titleTv = (TextView) findViewById(R.id.title_tv);

		mActivityBackBt.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				finish();
			}
		});
		mGetCodeBt.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				String mPhone = mPhoneNumEt.getText().toString().trim();
				if (mPhone.length() != 11) {
					Toast.makeText(PhoneRegisteActivity.this,
							getString(R.string.errorphonenumber),
							Toast.LENGTH_SHORT).show();
					return;
				}
				sendCode(PhoneRegisteActivity.this);
			}
		});
		mOkBt.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				String password = mPwEt.getText().toString().trim();
				String authcode = mAuthCodeEt.getText().toString().trim();
				if (password.length() < 6 || password.length() > 16) {
					ToastHelper.show(PhoneRegisteActivity.this,
							getString(R.string.pwlengthrole));
					return;
				} else if (authcode.length() < 1) {
					ToastHelper.show(PhoneRegisteActivity.this,
							getString(R.string.autocoderole));
					return;
				}
				// if (NetworkStatus.getNetWorkStatus(PhoneRegisteActivity.this)
				// > 0)
				// {
				// new PhotoRegisteThread(mPhotoNumber, password, authcode)
				// .start();
				// }
				// else
				// {
				// ToastHelper.show(PhoneRegisteActivity.this,
				// getString(R.string.no_network));
				// }

				phoneRegiste(PhoneRegisteActivity.this);
			}
		});

		mPhotoNumber = getIntent().getStringExtra(
				GobalConstants.Data.PHONENUMBER);
		mOperate = getIntent().getStringExtra(GobalConstants.Data.PHONEOPERATE);
		if (mOperate != null
				&& mOperate.equals(GobalConstants.Data.PHONECHANGEPW)) {
			titleTv.setText(getString(R.string.phonesetnewpw));
		}else if(mOperate != null
				&& mOperate.equals(GobalConstants.Data.PHONEJUSTCHANGEPW)){
			titleTv.setText(getString(R.string.phonesetnewpw));
			justChangePw = true;
		}

	}

	Dialog mProgressDialog;

	private void sendCode(final Context context) {
		if (NetworkStatus.getNetWorkStatus(context) > 0) {
			mProgressDialog = DialogHelper.showProgressDialog(context,
					getString(R.string.getautocodeing));
			mGetCodeBt.setEnabled(false);
			mGetCodeBt.setSelected(true);
			new Thread() {
				public void run() {
					// ResultDTO lResultDTO =new BindPhoneSendCode(context,
					// mUser.getUid(), mUser.getToken(), mPhone).postConnect();
					String phone = mPhoneNumEt.getText().toString().trim();
					ResultDTO lResultDTO = new GetPhotoAutoSMS(
							PhoneRegisteActivity.this, phone,
							BaoBaoWDApplication.deviceId).postConnect();
					Message msg = new Message();
					msg.what = SENDCODE;
					msg.obj = lResultDTO;
					mHandler.sendMessage(msg);
				}
			}.start();
		} else {
			Toast.makeText(context, getString(R.string.no_network),
					Toast.LENGTH_SHORT).show();
		}
	}

	private void phoneRegiste(final Context context) {
		if (NetworkStatus.getNetWorkStatus(context) > 0) {
			if (mOperate == null || mOperate.equals(""))
				mProgressDialog = DialogHelper.showProgressDialog(
						PhoneRegisteActivity.this,
						getString(R.string.registering));
			else if (mOperate.equals(GobalConstants.Data.PHONECHANGEPW))
				mProgressDialog = DialogHelper.showProgressDialog(
						PhoneRegisteActivity.this,
						getString(R.string.changepwing));
			mGetCodeBt.setEnabled(false);
			mGetCodeBt.setSelected(true);
			new Thread() {
				public void run() {
					String phoneNumber = mPhoneNumEt.getText().toString()
							.trim();
					String password = mPwEt.getText().toString().trim();
					String authcode = mAuthCodeEt.getText().toString().trim();
					ResultDTO lResultDTO = null;
					if (mOperate == null || mOperate.equals(""))
						lResultDTO = new PhotoRegiste(
								PhoneRegisteActivity.this, phoneNumber,
								password, authcode,
								BaoBaoWDApplication.deviceId).postConnect();
					else if (mOperate.equals(GobalConstants.Data.PHONECHANGEPW)||
							mOperate.equals(GobalConstants.Data.PHONEJUSTCHANGEPW))
						lResultDTO = new PhotoChangePw(
								PhoneRegisteActivity.this, phoneNumber,
								password, authcode,
								BaoBaoWDApplication.deviceId).postConnect();

					Message msg = new Message();
					msg.what = PHONEREGISTE;
					msg.obj = lResultDTO;
					mHandler.sendMessage(msg);
				}
			}.start();
		} else {
			Toast.makeText(context, getString(R.string.no_network),
					Toast.LENGTH_SHORT).show();
		}
	}

	// class PhotoRegisteThread extends Thread
	// {
	//
	// private Handler handler;
	// private ResultDTO resultDTO;
	// String phoneNumber;
	// String password;
	// String authcode;
	//
	// public PhotoRegisteThread(String phoneNumber, String password,
	// String authcode)
	// {
	// handler = new Handler();
	// if (mOperate == null || mOperate.equals(""))
	// mProgressDialog = DialogHelper.showProgressDialog(
	// PhoneRegisteActivity.this,
	// getString(R.string.registering));
	// else
	// if (mOperate.equals(GobalConstants.Data.PHONECHANGEPW))
	// mProgressDialog = DialogHelper.showProgressDialog(
	// PhoneRegisteActivity.this,
	// getString(R.string.changepwing));
	// this.phoneNumber = phoneNumber;
	// this.password = password;
	// this.authcode = authcode;
	// }
	//
	// @Override
	// public void run()
	// {
	// if (mOperate == null || mOperate.equals(""))
	// resultDTO = new PhotoRegiste(PhoneRegisteActivity.this,
	// phoneNumber, password, authcode,
	// BaoBaoWDApplication.deviceId
	// ).postConnect();
	// else
	// if (mOperate.equals(GobalConstants.Data.PHONECHANGEPW))
	// resultDTO = new PhotoChangePw(PhoneRegisteActivity.this,
	// phoneNumber, password, authcode,
	// BaoBaoWDApplication.deviceId
	// ).postConnect();
	// handler.post(new Runnable()
	// {
	//
	// @Override
	// public void run()
	// {
	// handRegisteResult(resultDTO);
	// }
	// });
	// }
	// }

	class CountDownTimerThread extends Thread {

		private Handler handler;
		private int time = 60;

		public CountDownTimerThread() {
			handler = new Handler();
			mGetCodeBt.setEnabled(false);
			mGetCodeBt.setSelected(true);
		}

		@Override
		public void run() {
			while (time > 0) {
				try {
					Thread.sleep(1000);
					time--;
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				handler.post(new Runnable() {

					@Override
					public void run() {
						if (time == 0) {
							mGetCodeBt.setText(getString(R.string.resend));
							mGetCodeBt.setEnabled(true);
							mGetCodeBt.setSelected(false);
						} else {
							mGetCodeBt.setText(getString(R.string.resend) + "("
									+ time + ")");
							mGetCodeBt.setEnabled(false);
							mGetCodeBt.setSelected(true);
						}
					}
				});
			}
		}
	}

	// class GetSMSThread extends Thread
	// {
	//
	// private Handler handler;
	// private ResultDTO resultDTO;
	// String photoNumber;
	//
	// public GetSMSThread(String photoNumber)
	// {
	// handler = new Handler();
	// mProgressDialog = DialogHelper.showProgressDialog(
	// PhoneRegisteActivity.this,
	// getString(R.string.getautocodeing));
	// this.photoNumber = photoNumber;
	// mResendBt.setEnabled(false);
	// }
	//
	// @Override
	// public void run()
	// {
	// resultDTO = new GetPhotoAutoSMS(PhoneRegisteActivity.this,
	// photoNumber, BaoBaoWDApplication.deviceId).postConnect();
	// handler.post(new Runnable()
	// {
	//
	// @Override
	// public void run()
	// {
	// DialogHelper.cancelProgressDialog(mProgressDialog);
	// if (resultDTO != null && resultDTO.getCode().equals("0"))
	// {
	// ToastHelper.show(PhoneRegisteActivity.this,
	// getString(R.string.autocodewillcome));
	// }
	// }
	// });
	// }
	// }

	private void handRegisteResult(ResultDTO resultDTO) {
		DialogHelper.cancelProgressDialog(mProgressDialog);
		mOkBt.setEnabled(true);
		mOkBt.setSelected(false);
		if (resultDTO != null && resultDTO.getCode().equals("0")) {
			LightDBHelper.setLoginType(PhoneRegisteActivity.this,
					GobalConstants.LoginType.PHONE);
			if(justChangePw){
				Toast.makeText(PhoneRegisteActivity.this,
						getString(R.string.changepwsuccess),
						Toast.LENGTH_SHORT).show();
				finish();
				return;
			}
			User lUser = GsonHelper
					.gsonToObj(resultDTO.getResult(), User.class);
			lUser.setIsSelf(GobalConstants.AllStatusYesOrNo.YES);
			UserStatusHelper.saveUserStatus(PhoneRegisteActivity.this, lUser);
			UserHelper.addGusetUser(PhoneRegisteActivity.this);
			UserHelper.guestCopyUser(PhoneRegisteActivity.this, lUser);
			UserHelper.setUser(lUser);
			LightDBHelper.setPushShare(PhoneRegisteActivity.this, true);
			Intent lIntent = new Intent(PhoneRegisteActivity.this,
					BaoBaoWDService.class);
			stopService(lIntent);
			RegisterPushHelper.logout(PhoneRegisteActivity.this);

			TaeSdkUtil.bindAccount(PhoneRegisteActivity.this, lUser.getUid());
			BroadCastHelper.sendRefreshMainBroadcast(PhoneRegisteActivity.this,
					GobalConstants.RefreshType.CHANGEUSER);
			if (lUser.getRegTime() == null || lUser.getRegTime().equals("")) {
				startActivity(StatusActivity.class);
			}
			ActivityManager.popAllActivity();
			finish();
		} else if (resultDTO != null && resultDTO.getCode().equals("-1031")) {
			if (mOperate == null || mOperate.equals(""))
				Toast.makeText(PhoneRegisteActivity.this,
						getString(R.string.phonenumberregistealready),
						Toast.LENGTH_SHORT).show();
		} else if (resultDTO != null && resultDTO.getCode().equals("-1032")) {
			Toast.makeText(PhoneRegisteActivity.this,
					getString(R.string.autocodeerror), Toast.LENGTH_SHORT)
					.show();
		} else {
			if (mOperate == null || mOperate.equals("")) {
				Toast.makeText(PhoneRegisteActivity.this,
						getString(R.string.registerfailed), Toast.LENGTH_SHORT)
						.show();
			} else if (mOperate.equals(GobalConstants.Data.PHONECHANGEPW))
				Toast.makeText(PhoneRegisteActivity.this,
						getString(R.string.changepwerror), Toast.LENGTH_SHORT)
						.show();
		}
	}
}
