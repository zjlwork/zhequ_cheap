
package dianyun.zqcheap.activity;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.TextView;

import com.taobao.tae.sdk.callback.CallbackContext;

import dianyun.baobaowd.adapter.GoodsListAdapter;
import dianyun.baobaowd.db.LightDBHelper;
import dianyun.baobaowd.defineview.SpecialTopicView2;
import dianyun.baobaowd.defineview.xlistview.CustomListView;
import dianyun.baobaowd.defineview.xlistview.CustomListView.OnLoadMoreListener;
import dianyun.baobaowd.entity.CateItem;
import dianyun.baobaowd.entity.Constants;
import dianyun.baobaowd.entity.LocalMenu;
import dianyun.baobaowd.help.ShopHttpHelper;
import dianyun.baobaowd.interfaces.DialogCallBack;
import dianyun.baobaowd.util.GobalConstants;
import dianyun.baobaowd.util.TaeSdkUtil;
import dianyun.baobaowd.util.ToastHelper;
import dianyun.baobaowd.util.UserHelper;
import dianyun.baobaowd.util.Utils;
import dianyun.zqcheap.R;

public class ShopSpecialTopicDetailActivity extends Activity implements
		OnClickListener, OnItemClickListener,
		dianyun.baobaowd.defineview.xlistview.CustomListView.OnRefreshListener,
		OnLoadMoreListener
{

	// private PullToRefreshScrollView mScrollView;
	private CustomListView mGridListView;
	private SpecialTopicView2 mSpecialView;
	private ImageView mToTopView;
	private TextView mTitleTV;
	private String mTitleString = "";
	private Long mParentID = 1L;
	private List<CateItem> mSpecialCateItem;
	private View mListViewHeader;
	private List<CateItem> mGoodsList = new ArrayList<CateItem>();
	private GoodsListAdapter mAdapter;
	private int mCurrentPageIndex = 1;
	private int mPageSize = 20;
	private View mCurrentActivityRootView;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		setContentView(R.layout.goods_list_lay);
		initView();
		initData();
		super.onCreate(savedInstanceState);
	}

	private void initView()
	{
		// mScrollView = (PullToRefreshScrollView)
		// findViewById(R.id.goodslist_scrollview);
		mCurrentActivityRootView = findViewById(R.id.root_view);
		mGridListView = (CustomListView) findViewById(R.id.goodslist_lv);
		mGridListView.setCanLoadMore2(false);
		mGridListView.setHeadViewBg(getResources().getColor(R.color.white));
		mListViewHeader = LayoutInflater.from(this).inflate(
				R.layout.shop_goods_list_header, null);
		mSpecialView = (SpecialTopicView2) mListViewHeader
				.findViewById(R.id.specialtopic_lay);
		mSpecialView.setCurrentActivityRootView(mCurrentActivityRootView);
		mGridListView.addHeaderView(mListViewHeader);
		mToTopView = (ImageView) findViewById(R.id.goodslist_to_top);
		mTitleTV = (TextView) findViewById(R.id.top_tv);
		mToTopView.setVisibility(View.GONE);
		mGridListView.setOnRefreshListener(this);
		mGridListView.setOnLoadListener(this);
		// mGridListView.setOnItemClickListener(this);
		mAdapter = new GoodsListAdapter(this, mGoodsList);
		mAdapter.setCurrentActivityRootView(mCurrentActivityRootView);
		// mAdapter.setNoPadding();
		mGridListView.setAdapter(mAdapter);
	}

	private void initData()
	{
		Intent itn = getIntent();
		if (itn != null)
		{
			mTitleString = itn.getStringExtra(Constants.EXTRA_NAME);
			mParentID = itn.getLongExtra(Constants.OBJECT_EXTRA_NAME, 0L);
		}
		if (null != (mTitleString))
		{
			mTitleTV.setText(mTitleString);
		}
		if (!Utils.isNetAvailable(this))
		{
			ToastHelper.show(this,
					getResources().getString(R.string.net_is_unable));
			return;
		}
		mCurrentPageIndex = 1;
		ShopHttpHelper.getMoreChildData(this, true, mCurrentPageIndex,
				mPageSize, "" + mParentID,
				new ShopHttpHelper.ShopDataCallback()
				{

					@Override
					public void getMenu(List<LocalMenu> result)
					{
						// TODO Auto-generated method stub
					}

					@Override
					public void getChildsData(List<CateItem> result)
					{
						doClassList(result);
						mAdapter.setDataSource(mGoodsList);
						if (mSpecialCateItem == null
								|| mSpecialCateItem.size() <= 0)
						{
							mSpecialView.setVisibility(View.GONE);
						}
						else
						{
							mSpecialView.setDataSource(mSpecialCateItem);
						}
						mGridListView.onRefreshComplete();
					}
				});
	}

	private void doClassList(List<CateItem> result)
	{
		if (result != null && result.size() > 0)
		{
			if (result.size() < mPageSize)
			{
				if (mCurrentPageIndex > 1)
				{
					// ToastHelper.show(this,
					// getResources().getString(R.string.no_moredata));
				}
				mGridListView.setCanLoadMore2(false);
			}
			else
			{
				mGridListView.setCanLoadMore2(true);
			}
			// 筛选出商品
			mGoodsList.clear();
			mSpecialCateItem = null;
			for (CateItem item : result)
			{
				if (item.xType != null && item.xType == 3)
				{
					mGoodsList.add(item);
				}
				else if (item.xType != null && item.xType == 4)
				{
					if (mSpecialCateItem == null)
					{
						mSpecialCateItem = new ArrayList<CateItem>(2);
					}
					mSpecialCateItem.add(0, item);
					mSpecialCateItem.add(1, null);
				}
			}
			mCurrentPageIndex++;
		}
		else
		{
			mGridListView.setCanLoadMore2(false);
		}
	}

	private void doAddClassList(List<CateItem> result)
	{
		if (result != null && result.size() > 0)
		{
			mCurrentPageIndex++;
			// 筛选出商品
			for (CateItem item : result)
			{
				if (item.xType != null && item.xType == 3)
				{
					mGoodsList.add(item);
				}
			}
			if (result.size() < mPageSize)
			{
				// ToastHelper.show(this,
				// getResources().getString(R.string.no_moredata));
				mGridListView.setCanLoadMore2(false);
			}
		}
		else
		{
			mGridListView.setCanLoadMore2(true);
		}
	}

	@Override
	public void onClick(View v)
	{
		switch (v.getId())
		{
		case R.id.activityback_bt:
			// back
			finish();
			break;
		case R.id.goodslist_to_top:
			mGridListView.scrollTo(0, 0);
			break;
		}
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id)
	{
		position = position - 2;
		final CateItem item = mGoodsList.get(position);
		if (item != null)
		{
			if (item.clickType == 4)
			{
				// 跳转到网页上的单品
				Intent itn = new Intent(ShopSpecialTopicDetailActivity.this,
						HtmlActivity.class);
				itn.putExtra(GobalConstants.Data.URL,
						item.clickUrl == null ? "" : item.clickUrl);
				itn.putExtra(GobalConstants.Data.TITLE,
						item.pageTitle == null ? "" : item.pageTitle);
				ShopSpecialTopicDetailActivity.this.startActivity(itn);
			}
			else
			{
				if (!LightDBHelper
						.getIsNotTipLoginOrNotFees(ShopSpecialTopicDetailActivity.this)
						&& (UserHelper.getUser().getIsGuest() == GobalConstants.UserType.ISSELF))
				{
//					ToastHelper.showTipLoginDialogWhenShop(
//							ShopSpecialTopicDetailActivity.this,
//							mCurrentActivityRootView,
//							new ToastHelper.dialogCancelCallback()
//							{
//
//								@Override
//								public void onCancle(boolean isNeverTip)
//								{
//									LightDBHelper
//											.setIsNotTipLoginOrNotFees(
//													ShopSpecialTopicDetailActivity.this,
//													isNeverTip);
//									TaeSdkUtil
//											.showTAEItemDetail(
//													ShopSpecialTopicDetailActivity.this,
//													item.tbItemId,
//													item.taobaoPid,
//													item.isTk == 1,
//													item.itemType, null,mCurrentActivityRootView);
//								}
//							});
					
					ToastHelper.showGoLoginDialog(ShopSpecialTopicDetailActivity.this, new DialogCallBack() {
						
						@Override
						public void clickSure() {
							
						}
						
						@Override
						public void clickCancel() {
							TaeSdkUtil
							.showTAEItemDetail(
									ShopSpecialTopicDetailActivity.this,
									item.tbItemId,
									item.taobaoPid,
									item.isTk == 1,
									item.itemType, null,mCurrentActivityRootView);
						}
					});
					
					
					
					
					
					
					
					
					
				}
				else
				{
					TaeSdkUtil
							.showTAEItemDetail(
									ShopSpecialTopicDetailActivity.this,
									item.tbItemId, item.taobaoPid,
									item.isTk == 1, item.itemType, null,mCurrentActivityRootView);
				}
			}
		}
	}

	@Override
	public void onLoadMore()
	{
		if (!Utils.isNetAvailable(this))
		{
			ToastHelper.show(this,
					getResources().getString(R.string.net_is_unable));
			mGridListView.onLoadMoreComplete();
			return;
		}
		ShopHttpHelper.getMoreChildData(this, false, mCurrentPageIndex,
				mPageSize, "" + mParentID,
				new ShopHttpHelper.ShopDataCallback()
				{

					@Override
					public void getMenu(List<LocalMenu> result)
					{
						// TODO Auto-generated method stub
					}

					@Override
					public void getChildsData(List<CateItem> result)
					{
						doAddClassList(result);
						mAdapter.setDataSource(mGoodsList);
						mSpecialView.setDataSource(mSpecialCateItem);
						mGridListView.onLoadMoreComplete();
					}
				});
	}

	@Override
	public void onRefresh()
	{
		if (!Utils.isNetAvailable(this))
		{
			ToastHelper.show(this,
					getResources().getString(R.string.net_is_unable));
			mGridListView.onRefreshComplete();
			return;
		}
		mCurrentPageIndex = 1;
		mGridListView.setCanLoadMore2(false);
		ShopHttpHelper.getChildData(this, false, "", "" + mParentID, false,
				false, new ShopHttpHelper.ShopDataCallback()
				{

					@Override
					public void getMenu(List<LocalMenu> result)
					{
						// TODO Auto-generated method stub
					}

					@Override
					public void getChildsData(List<CateItem> result)
					{
						doClassList(result);
						mAdapter.setDataSource(mGoodsList);
						mSpecialView.setDataSource(mSpecialCateItem);
						mGridListView.onRefreshComplete();
					}
				});
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data)
	{
		CallbackContext.onActivityResult(requestCode, resultCode, data);
	}
}
