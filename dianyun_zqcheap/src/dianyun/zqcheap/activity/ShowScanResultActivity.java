package dianyun.zqcheap.activity;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.ClipboardManager;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.listener.PauseOnScrollListener;

import dianyun.baobaowd.adapter.HotTagAdapter;
import dianyun.baobaowd.adapter.MAdapter;
import dianyun.baobaowd.data.HotWord;
import dianyun.baobaowd.data.User;
import dianyun.baobaowd.db.LightDBHelper;
import dianyun.baobaowd.defineview.ResizeLayout;
import dianyun.baobaowd.defineview.xlistview.CustomListView;
import dianyun.baobaowd.entity.CateItem;
import dianyun.baobaowd.entity.Constants;
import dianyun.baobaowd.help.ShopHttpHelper;
import dianyun.baobaowd.interfaces.ChoosePriceWindowCallBack;
import dianyun.baobaowd.interfaces.DialogCallBack;
import dianyun.baobaowd.util.GobalConstants;
import dianyun.baobaowd.util.NetworkStatus;
import dianyun.baobaowd.util.TaeSdkUtil;
import dianyun.baobaowd.util.ToastHelper;
import dianyun.baobaowd.util.UserHelper;
import dianyun.baobaowd.util.Utils;
import dianyun.zqcheap.R;

/**
 * Created by pc on 2015/4/1.
 */
public class ShowScanResultActivity extends Activity implements
		ShopHttpHelper.PrimaryKeyWithUrlCallback, View.OnClickListener,
		ResizeLayout.OnResizeListener, View.OnTouchListener, TextWatcher,
		View.OnKeyListener, CustomListView.OnLoadMoreListener,
		ShopHttpHelper.HotWordCallback {

	private CustomListView mListView;
	// private CustomListView mKeyWordsListView;
	private List<HotWord> mHowWordList = new ArrayList<HotWord>();
	// private HotWordAdapter mHotWordAdapter;

	private GridView mKeyWordsGridView;
	private HotTagAdapter mHotTagAdapter;
	private LinearLayout mKeyWordsLayout;

	private TextView mFanliOrderTv;
	private TextView mSalesOrderTv;
	private TextView mPriceOrderTv;
	private TextView mChoosePriceTv;
	private RelativeLayout mPriceOrderLayout;
	private RelativeLayout mChoosePriceLayout;

	private List<CateItem> mCateItemList = new ArrayList<CateItem>();
	private View mParent;
	private MAdapter mAdapter;
	private RelativeLayout mDeleteImageView;
	private ResizeLayout mTotalLay;
	private Button mPasteButton;
	private EditText mUrlEditText;
	private TextView mCloseTxt;
	private android.view.inputmethod.InputMethodManager mInputManager;
	private String mOriginUrl;
	private String mPrimaryKeyWords = "";
	private String mHttpUrlString = "";
	private int mCurrentPageIndex = 1;
	private int mPageSize = 30;

	private RelativeLayout tbhint_lay;
	private TextView mShowResultTipTV;
	private boolean mIsPause = false;
	private String mOrderType = ORDERTYPE_DEFAULT;
	public static final String ORDERTYPE_DEFAULT = "";
	public static final int ORDERTYPE_PRICE_HTOL = 200;
	public static final int ORDERTYPE_PRICE_LTOH = 300;

	public static final String ORDERTYPE_HTOL = "price_desc";
	public static final String ORDERTYPE_LTOH = "price_asc";

	private static final String ORDERTYPE_FANLI = "commissionRate_desc";
	public static final String ORDERTYPE_SALES = "commissionNum_desc";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.shop_scan_result_lay);
		initViews();
		initData();
	}

	private void initViews() {
		mInputManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
		mShowResultTipTV = (TextView) findViewById(R.id.shop_scan_tip_txt);
		tbhint_lay = (RelativeLayout) findViewById(R.id.tbhint_lay);

		mFanliOrderTv = (TextView) findViewById(R.id.fanli_order_tv);
		mSalesOrderTv = (TextView) findViewById(R.id.sales_order_tv);
		mPriceOrderTv = (TextView) findViewById(R.id.price_order_tv);
		mChoosePriceTv = (TextView) findViewById(R.id.choose_price_tv);
		mPriceOrderLayout = (RelativeLayout) findViewById(R.id.price_order_layout);
		mChoosePriceLayout = (RelativeLayout) findViewById(R.id.choose_price_layout);

		mFanliOrderTv.setOnClickListener(this);
		mSalesOrderTv.setOnClickListener(this);
		mPriceOrderLayout.setOnClickListener(this);
		mChoosePriceLayout.setOnClickListener(this);
		tbhint_lay.setOnClickListener(this);

		mListView = (CustomListView) findViewById(R.id.shop_scan_result_listview);
		mKeyWordsGridView = (GridView) findViewById(R.id.gridview);
		mKeyWordsLayout = (LinearLayout) findViewById(R.id.shop_scan_keywords_layout);
		mKeyWordsLayout.setOnClickListener(this);
		// mKeyWordsListView = (CustomListView)
		// findViewById(R.id.shop_scan_keywords_listview);

		mListView.setOnLoadListener(this);
		mListView.setAutoLoadMore(true);
		mListView.setSeconScrollListener(new PauseOnScrollListener(ImageLoader
				.getInstance(), false, false));
		mListView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				if (position > 0) {
					CateItem lCateItem = mCateItemList.get(position - 1);
					goShopping(lCateItem, ShowScanResultActivity.this,
							mListView);
				}
			}
		});
		mCloseTxt = (TextView) findViewById(R.id.shop_scan_pop_close);
		mCloseTxt.setOnClickListener(this);
		mTotalLay = (ResizeLayout) findViewById(R.id.shop_scan_pop_total_lay);
		mTotalLay.setOnResizeListener(this);
		mDeleteImageView = (RelativeLayout) findViewById(R.id.shop_scan_pop_delete_img);
		mPasteButton = (Button) findViewById(R.id.shop_scan_paste_button);
		mPasteButton.setOnClickListener(this);
		mDeleteImageView.setOnClickListener(this);
		mUrlEditText = (EditText) findViewById(R.id.shop_scan_pop_edit);
		// mUrlEditText.setOnClickListener(this);
		mUrlEditText.setOnTouchListener(this);

		mUrlEditText.addTextChangedListener(this);
		mUrlEditText.setOnKeyListener(this);
		mPasteButton.setVisibility(View.VISIBLE);
		mShowResultTipTV.setVisibility(View.GONE);

	}

	ClipboardManager mClipboard;
	String mKeyWord;

	private void initData() {
		mHandler = new Handler();
		Intent itn = getIntent();
		if (itn != null) {
			List<CateItem> obj = (List<CateItem>) itn
					.getSerializableExtra(Constants.EXTRA_NAME);
			if (obj != null && obj.size() > 0) {
				mCateItemList.clear();
				mCateItemList.addAll(obj);
			}
		}
		if (mAdapter == null) {
			mAdapter = new MAdapter(this, mCateItemList);
			mListView.setAdapter(mAdapter);
		} else {
			mAdapter.notifyDataSetChanged();
		}
		mClipboard = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
		String menuName = getIntent().getStringExtra("menuName");
		if (!TextUtils.isEmpty(menuName)) {
			mKeyWord = menuName;
		} else if (mClipboard.getText() != null
				&& !TextUtils.isEmpty(mClipboard.getText().toString().trim())) {
			mKeyWord = mClipboard.getText().toString().trim();

		}
		if (mKeyWord == null) {
			initKeyWordListView();
			User mUser = UserHelper.getUser();
			if (NetworkStatus.getNetWorkStatus(ShowScanResultActivity.this) > 0) {
				ShopHttpHelper.requestHotWords(this, mUser.getUid(),
						mUser.getToken(), this);
			} else {
				Toast.makeText(ShowScanResultActivity.this,
						getString(R.string.no_network), Toast.LENGTH_SHORT)
						.show();
			}
		} else {
			mUrlEditText.setText(mKeyWord);
			mUrlEditText.setSelection(mKeyWord.length());
			handleCheckUrlClick();
		}

		changeOrderType(ORDERTYPE_DEFAULT);

	}

	private void initKeyWordListView() {
		// View header =
		// LayoutInflater.from(ShowScanResultActivity.this).inflate(R.layout.hotwordadapter,
		// null);
		// TextView word_tv = (TextView)header.findViewById(R.id.word_tv);
		// word_tv.setText(getString(R.string.hotsearchword));
		// word_tv.setTextColor(getResources().getColor(R.color.priceorderhintcolor));
		// mHotWordAdapter = new
		// HotWordAdapter(ShowScanResultActivity.this,mHowWordList);
		// mKeyWordsListView.addHeaderView(header);
		// mKeyWordsListView.setAdapter(mHotWordAdapter);
		// mKeyWordsListView.setOnItemClickListener(new OnItemClickListener() {
		//
		// @Override
		// public void onItemClick(AdapterView<?> parent, View view,
		// int position, long id) {
		//
		// if(position>1){
		// HotWord hotWord = mHowWordList.get(position-2);
		// mUrlEditText.setText(hotWord.getWord());
		// mUrlEditText.setSelection(hotWord.getWord().length());
		// handleCheckUrlClick();
		// }
		// }
		// });
		// mKeyWordsListView.setVisibility(View.VISIBLE);

		mHotTagAdapter = new HotTagAdapter(mHowWordList,
				ShowScanResultActivity.this);
		mKeyWordsGridView.setAdapter(mHotTagAdapter);
		mKeyWordsGridView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				HotWord hotWord = mHowWordList.get(position);
				mUrlEditText.setText(hotWord.getWord());
				mUrlEditText.setSelection(hotWord.getWord().length());
				handleCheckUrlClick();
			}
		});

	}

	public void hotWordClick(HotWord hotWord) {
		mUrlEditText.setText(hotWord.getWord());
		mUrlEditText.setSelection(hotWord.getWord().length());
		handleCheckUrlClick();
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		mClipboard.setText("");
	}

	boolean mIsTianmao = false;
	int mBeginPrice = -1;
	int mEndPrice = -1;
	int mHtolOrLtoh = -1;

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		// case R.id.shop_scan_pop_edit:
		// if (TextUtils.isEmpty(mUrlEditText.getText().toString().trim())) {
		// handlePaste();
		// handleCheckUrlClick();
		// }
		// break;
		case R.id.shop_scan_pop_close:
			handleCloseClick();
			break;

		case R.id.shop_scan_pop_delete_img:
			handleDelteClick();
			break;
		case R.id.shop_scan_paste_button:
			handlePaste();
			// handleCheckUrlClick();
			break;
		case R.id.fanli_order_tv:
			changeOrderType(ORDERTYPE_FANLI);
			handleCheckUrlClick();
			break;
		case R.id.sales_order_tv:
			changeOrderType(ORDERTYPE_SALES);
			handleCheckUrlClick();
			break;
		case R.id.shop_scan_keywords_layout:
			break;
		case R.id.price_order_layout:
			changeOrderType(ORDERTYPE_DEFAULT);
			handleCheckUrlClick();
			// ToastHelper.showPriceOrderWindow(ShowScanResultActivity.this,
			// mFanliOrderTv, mOrderType,
			// new PriceOrderWindowCallBack() {
			//
			// @Override
			// public void priceltohselected() {
			// changeOrderType(ORDERTYPE_PRICE_LTOH);
			// handleCheckUrlClick();
			// }
			//
			// @Override
			// public void pricehtolselected() {
			// changeOrderType(ORDERTYPE_PRICE_HTOL);
			// handleCheckUrlClick();
			// }
			//
			// @Override
			// public void pricedefaultselected() {
			// changeOrderType(ORDERTYPE_DEFAULT);
			// handleCheckUrlClick();
			// }
			// });
			break;
		case R.id.choose_price_layout:
			ToastHelper.showChoosePriceWindow(ShowScanResultActivity.this,
					mChoosePriceLayout, mIsTianmao, mBeginPrice, mEndPrice,
					mHtolOrLtoh, mOrderType, new ChoosePriceWindowCallBack() {

						@Override
						public void choose(boolean isTianmao, int beginPrice,
								int endPrice, int htolOrltoh) {
							mIsTianmao = isTianmao;
							mBeginPrice = beginPrice;
							mEndPrice = endPrice;
							mHtolOrLtoh = htolOrltoh;
							changeOrderType(mOrderType);
							handleCheckUrlClick();
						}
					});
			break;
		case R.id.tbhint_lay:
			Utils.goHtmlActivity(ShowScanResultActivity.this, getString(R.string.taobaolc), GobalConstants.URL.YOYOBASE+GobalConstants.URL.SHOP_SCAN_HELP_URL);
			break;

		}

	}

	private void changeOrderType(String order) {
		mOrderType = order;
		/*
		 * if(mOrderType.equals(ORDERTYPE_PRICE_LTOH)){
		 * mPriceOrderTv.setText(getString(R.string.order_price_ltoh));
		 * mPriceOrderTv.setCompoundDrawablesWithIntrinsicBounds(null, null,
		 * getResources().getDrawable(R.drawable.priceorder_downselected),
		 * null); mPriceOrderTv.setSelected(true);
		 * mSalesOrderTv.setSelected(false); mFanliOrderTv.setSelected(false);
		 * 
		 * }else if(mOrderType.equals(ORDERTYPE_PRICE_HTOL)){
		 * mPriceOrderTv.setText(getString(R.string.order_price_htol));
		 * mPriceOrderTv.setCompoundDrawablesWithIntrinsicBounds(null, null,
		 * getResources().getDrawable(R.drawable.priceorder_downselected),
		 * null); mPriceOrderTv.setSelected(true);
		 * mSalesOrderTv.setSelected(false); mFanliOrderTv.setSelected(false);
		 * }else
		 */

		if (mOrderType.equals(ORDERTYPE_DEFAULT)) {
			mPriceOrderTv.setText(getString(R.string.order_price_default));
			// mPriceOrderTv.setCompoundDrawablesWithIntrinsicBounds(null, null,
			// getResources().getDrawable(R.drawable.priceorder_downselected),
			// null);
			mPriceOrderTv.setSelected(true);
			mSalesOrderTv.setSelected(false);
			mFanliOrderTv.setSelected(false);

		} else if (mOrderType.equals(ORDERTYPE_FANLI)) {
			// mPriceOrderTv.setCompoundDrawablesWithIntrinsicBounds(null, null,
			// getResources().getDrawable(R.drawable.priceorder_downnormal),
			// null);
			mPriceOrderTv.setSelected(false);
			mSalesOrderTv.setSelected(false);
			mFanliOrderTv.setSelected(true);
			mHtolOrLtoh = -1;
		} else if (mOrderType.equals(ORDERTYPE_SALES)) {
			// mPriceOrderTv.setCompoundDrawablesWithIntrinsicBounds(null, null,
			// getResources().getDrawable(R.drawable.priceorder_downnormal),
			// null);
			mPriceOrderTv.setSelected(false);
			mSalesOrderTv.setSelected(true);
			mFanliOrderTv.setSelected(false);
			mHtolOrLtoh = -1;
		}

		if (mIsTianmao == false && mBeginPrice == -1 && mEndPrice == -1
				&& mHtolOrLtoh == -1) {
			mChoosePriceTv.setCompoundDrawablesWithIntrinsicBounds(null, null,
					getResources()
							.getDrawable(R.drawable.priceorder_downnormal),
					null);
			mChoosePriceTv.setSelected(false);
		} else {
			mChoosePriceTv.setCompoundDrawablesWithIntrinsicBounds(
					null,
					null,
					getResources().getDrawable(
							R.drawable.priceorder_downselected), null);
			mChoosePriceTv.setSelected(true);
		}

	}

	private void handleCloseClick() {
		String closeName = mCloseTxt.getText().toString().trim();
		if (closeName.equals(getResources().getString(R.string.search_string))) {
			handleCheckUrlClick();
		} else {
			finish();
		}
	}

	private void handlePaste() {
		// mPasteButton.setVisibility(View.VISIBLE);
		// ClipboardManager clipboard =
		// (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);

		if (mUrlEditText != null) {
			String pasteString = TextUtils.isEmpty(mClipboard.getText()) ? ""
					: mClipboard.getText().toString();
			mUrlEditText.setText(pasteString);
			mUrlEditText.setSelection(pasteString.length());
		}
	}

	private void handleDelteClick() {
		if (mUrlEditText != null) {
			mUrlEditText.setText("");
		}
	}

	private void handleCheckUrlClick() {
		mKeyWordsLayout.setVisibility(View.GONE);
		mPasteButton.setVisibility(View.GONE);
		if (mUrlEditText != null) {
			if (TextUtils.isEmpty(mUrlEditText.getEditableText().toString()
					.trim())) {
				ToastHelper.showByGravity(
						this,
						getResources().getString(
								R.string.shop_scan_can_not_empty),
						Gravity.CENTER);
				return;
			} else {
				mUrlEditText.clearFocus();
				mPasteButton.setVisibility(View.GONE);
				mInputManager.hideSoftInputFromWindow(
						mUrlEditText.getWindowToken(), 0);
				mOriginUrl = getHttpUrl(mUrlEditText.getEditableText()
						.toString().trim());
				mCurrentPageIndex = 1;
				mHttpUrlString = "";
				mPrimaryKeyWords = "";
				if (mHtolOrLtoh == ORDERTYPE_PRICE_HTOL)
					mOrderType = ORDERTYPE_HTOL;
				else if (mHtolOrLtoh == ORDERTYPE_PRICE_LTOH) {
					mOrderType = ORDERTYPE_LTOH;
				}

				ShopHttpHelper.requestPrimaryWithUrl(this, mOriginUrl,
						mPrimaryKeyWords, mOrderType, mHttpUrlString,
						mPageSize, mCurrentPageIndex, mBeginPrice, mEndPrice,
						mIsTianmao, this);
			}
		}

	}

	private String getHttpUrl(String url) {
		String res = url + "";
		// if (!TextUtils.isEmpty(url)) {
		// int index = url.indexOf("http");
		// if (index >= 0) {
		// res = url.substring(index);
		// }
		// }
		return res;
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		boolean res = true;
		switch (keyCode) {
		case KeyEvent.KEYCODE_BACK:
			finish();
			break;
		case KeyEvent.KEYCODE_ENTER:
			handleCheckUrlClick();
			break;
		}
		if (res) {
			return super.onKeyDown(keyCode, event);
		} else {
			return true;
		}
	}

	@Override
	protected void onPause() {
		mIsPause = true;
		super.onPause();
	}

	@Override
	public void OnResize(int w, int h, int oldw, int oldh) {
		if (oldh > h) {
			// if show keyboard
			mPasteButton.setVisibility(View.VISIBLE);
		} else {
			if (oldh == 0) {
				mPasteButton.setVisibility(View.VISIBLE);

			} else {
				if (!mIsPause) {
					mPasteButton.setVisibility(View.GONE);
				} else {
					mIsPause = false;
				}
			}
		}

	}

	@Override
	public boolean onTouch(View v, MotionEvent event) {

		mPasteButton.setVisibility(View.VISIBLE);
		// if (event.getAction() == MotionEvent.ACTION_DOWN) {
		// if (TextUtils.isEmpty(mUrlEditText.getText().toString().trim())) {
		// handlePaste();
		// handleCheckUrlClick();
		//
		// }
		// }
		return false;
	}

	@Override
	public void beforeTextChanged(CharSequence s, int start, int count,
			int after) {

	}

	@Override
	public void onTextChanged(CharSequence s, int start, int before, int count) {
		if (TextUtils.isEmpty(s)) {
			mDeleteImageView.setVisibility(View.INVISIBLE);
			mCloseTxt.setText(getResources().getString(R.string.cancel));
			mCloseTxt.setTextAppearance(ShowScanResultActivity.this,
					R.style.shop_scan_search_close_text_style);
			if (mHowWordList.size() > 0)
				mKeyWordsLayout.setVisibility(View.VISIBLE);

		} else {
			mDeleteImageView.setVisibility(View.VISIBLE);
			mCloseTxt.setText(getResources().getString(R.string.search_string));
			mCloseTxt.setTextAppearance(ShowScanResultActivity.this,
					R.style.shop_scan_search_close_search_text_style);
		}
	}

	@Override
	public void afterTextChanged(Editable s) {

	}

	private void getCheckResult(List<CateItem> item, String errmsg) {
		if (item != null && item.size() > 0) {
			mShowResultTipTV.setVisibility(View.VISIBLE);
			mListView.setVisibility(View.VISIBLE);
			mUrlEditText.clearFocus();
			// mInputManager.hideSoftInputFromWindow(
			// mUrlEditText.getWindowToken(), 0);
			if (getWindow().getAttributes().softInputMode == WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE) {
				mInputManager.toggleSoftInput(0,
						InputMethodManager.HIDE_NOT_ALWAYS);
			}
			mPasteButton.setVisibility(View.GONE);

		} else {
			mShowResultTipTV.setVisibility(View.GONE);
			mListView.setVisibility(View.GONE);
			if (TextUtils.isEmpty(errmsg)) {
				errmsg = getResources().getString(
						R.string.shop_scan_controll_failed);
			}
			if (getWindow().getAttributes().softInputMode != WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE) {
				mInputManager.toggleSoftInput(0,
						InputMethodManager.HIDE_NOT_ALWAYS);
			}
			mPasteButton.setVisibility(View.VISIBLE);
			ToastHelper.showByGravity(this, errmsg, Gravity.CENTER);
		}
		mListView.requestFocus();

	}

	@Override
	public boolean onKey(View v, int keyCode, KeyEvent event) {
		if ((event.getAction() == MotionEvent.ACTION_UP)
				&& (keyCode == KeyEvent.KEYCODE_ENTER)) {
			handleCheckUrlClick();
		}
		return false;
	}

	@Override
	public void getResult(List<CateItem> itemList, String descString,
			String httpString, String errmsg) {
		mPrimaryKeyWords = descString;
		mHttpUrlString = httpString;
		if (itemList != null && itemList.size() > 0) {
			if (mCurrentPageIndex <= 1) {

				mCateItemList.clear();
				mCateItemList.addAll(itemList);
			} else {
				mCateItemList.addAll(itemList);
			}
			if (itemList.size() < mPageSize) {
				mListView.setCanLoadMore2(false);
			} else {
				mListView.setCanLoadMore2(true);
			}
			mCurrentPageIndex++;
		} else {
			mListView.setCanLoadMore2(false);
			if (mCurrentPageIndex <= 1) {
				mCateItemList.clear();
			}
			mListView.setVisibility(View.GONE);
		}
		mListView.onLoadMoreComplete();
		mAdapter.notifyDataSetChanged();
		if (mCurrentPageIndex <= 2 && mCateItemList.size() > 0) {
			mListView.setSelection(0);
		}
		getCheckResult(mCateItemList, errmsg);
	}

	@Override
	public void onLoadMore() {
		if (!TextUtils.isEmpty(mHttpUrlString + mPrimaryKeyWords)) {
			ShopHttpHelper
					.requestPrimaryWithUrl(this, mOriginUrl, mPrimaryKeyWords,
							mOrderType, mHttpUrlString, mPageSize,
							mCurrentPageIndex, mBeginPrice, mEndPrice,
							mIsTianmao, this);
		}
	}

	

	private void goShopping(final CateItem mItem, final Context mContext,
			final View mParent) {
		if (mItem != null) {
			// 如果是单品的话 并且有返利的话
			if (mItem.coins != null && mItem.coins > 0) {
				if (!LightDBHelper.getIsNotTipLoginOrNotFees(mContext)
						&& UserHelper.getUser().getIsGuest() == GobalConstants.UserType.ISSELF
						&& mParent != null) {
//					ToastHelper.showTipLoginDialogWhenShop(mContext, mParent,
//							new ToastHelper.dialogCancelCallback() {
//
//								@Override
//								public void onCancle(boolean isNeverTip) {
//									LightDBHelper.setIsNotTipLoginOrNotFees(
//											mContext, isNeverTip);
//									TaeSdkUtil.showTAEItemDetail(
//											(Activity) mContext,
//											mItem.tbItemId, mItem.taobaoPid,
//											mItem.isTk == 1, mItem.itemType,
//											null,mTotalLay);
//								}
//							}, null);
					
					ToastHelper.showGoLoginDialog(mContext, new DialogCallBack() {
						
						@Override
						public void clickSure() {
							
						}
						
						@Override
						public void clickCancel() {
							TaeSdkUtil.showTAEItemDetail(
									(Activity) mContext,
									mItem.tbItemId, mItem.taobaoPid,
									mItem.isTk == 1, mItem.itemType,
									null,mTotalLay);
						}
					});
					
					
					
					
					
					
					
					
				} else {
					TaeSdkUtil.showTAEItemDetail((Activity) mContext,
							mItem.tbItemId, mItem.taobaoPid, mItem.isTk == 1,
							mItem.itemType, null,mTotalLay);

				}
			} else {
				// 假如没有返利 直接showpage
				TaeSdkUtil.showTAEItemDetail((Activity) mContext,
						mItem.tbItemId, mItem.taobaoPid, mItem.isTk == 1,
						mItem.itemType, null,mTotalLay);
			}
		}
	}

	private Handler mHandler;

	@Override
	public void getDeseStr(final String descString) {
		mHandler.post(new Runnable() {

			@Override
			public void run() {
				if (!TextUtils.isEmpty(descString)) {
					mUrlEditText.setText(descString);
					mUrlEditText.setSelection(descString.length());
				}
			}
		});

	}

	@Override
	public void getResult(List<HotWord> wordList) {
		if (wordList != null && wordList.size() > 0) {
			mKeyWordsLayout.setVisibility(View.VISIBLE);
			mHowWordList.clear();
			mHowWordList.addAll(wordList);
			mHotTagAdapter.notifyDataSetChanged();
		} else {
			mKeyWordsLayout.setVisibility(View.GONE);
		}

	}
}
