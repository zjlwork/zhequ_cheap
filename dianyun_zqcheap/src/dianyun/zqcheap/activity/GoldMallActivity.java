
package dianyun.zqcheap.activity;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.reflect.TypeToken;
import com.taobao.tae.sdk.callback.CallbackContext;

import dianyun.baobaowd.adapter.GoldListAdapter;
import dianyun.baobaowd.data.Gift;
import dianyun.baobaowd.data.User;
import dianyun.baobaowd.defineview.xlistview.CustomListView;
import dianyun.baobaowd.dto.ResultDTO;
import dianyun.baobaowd.gson.GsonHelper;
import dianyun.baobaowd.serverinterface.GetGifts;
import dianyun.baobaowd.util.BindHelper;
import dianyun.baobaowd.util.BroadCastHelper;
import dianyun.baobaowd.util.ConversionHelper;
import dianyun.baobaowd.util.DialogHelper;
import dianyun.baobaowd.util.GobalConstants;
import dianyun.baobaowd.util.NetworkStatus;
import dianyun.baobaowd.util.ServiceInterfaceHelper;
import dianyun.baobaowd.util.ToastHelper;
import dianyun.baobaowd.util.UserHelper;
import dianyun.baobaowd.util.Utils;
import dianyun.zqcheap.R;

public class GoldMallActivity extends Activity implements OnClickListener, GoldListAdapter.RefreshCallback {

    private Dialog mProgressDialog;
    // private PullToRefreshScrollView mScrollView;
    private CustomListView mGridListView;
    private ImageView mToTopView;
    private TextView mTitleTV;
    private String mTitleString = "";
    private Long mParentID = 1L;
    private GoldListAdapter mAdapter;
    private int mCurrentPageIndex = 1;
    private int mPageSize = 200;
    private View mCurrentActivityRootView;
    private View mHeaderView;
    private View mHeaderLeftLay;
    private View mHeaderRightLay;
    private TextView mBottomMyCountTv;
    private TextView mBottomNeedCountTv;
    private TextView mBottomHasSelectTv;
    private Button mBottomChargeBt;
//    private List<Integer> mSelectCZNumList = new ArrayList<Integer>();
//    private List<Button> mSelectCZNumBtList = new ArrayList<Button>();
    private int mCurrentSelectedIndex = 0;
    private int mHasSelectedCount = 0;
    private User mUser;
    private List<Gift> mList = new ArrayList<Gift>();
    private int mGiftId;
    private TextView mGoldUseGuideTv;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.gold_mall_lay);
        initView();
        initData();
        super.onCreate(savedInstanceState);
    }

    private void initView() {
        mHeaderView = LayoutInflater.from(this).inflate(R.layout.gold_mall_header_lay, null);
        mHeaderLeftLay = mHeaderView.findViewById(R.id.gold_mall_left_lay);
        mGoldUseGuideTv = (TextView)mHeaderView.findViewById(R.id.golduseguide_tv);
       int marginLeft= ConversionHelper
                .dipToPx(15, this);
       LinearLayout.LayoutParams layoutParams=(LinearLayout.LayoutParams) mHeaderLeftLay.getLayoutParams();
        layoutParams.setMargins(marginLeft,0,0,0);
        mHeaderLeftLay.setLayoutParams(layoutParams);
        mHeaderRightLay =  mHeaderView.findViewById(R.id.gold_mall_rigth_lay);
        LinearLayout.LayoutParams layoutParams2=(LinearLayout.LayoutParams) mHeaderRightLay.getLayoutParams();
        layoutParams2.setMargins(marginLeft,0,0,0);
        mHeaderRightLay.setLayoutParams(layoutParams2);

        mGoldUseGuideTv.setOnClickListener(this);
        mHeaderLeftLay.setOnClickListener(this);
        mHeaderRightLay.setOnClickListener(this);
        mBottomChargeBt = (Button) findViewById(R.id.gold_mall_bottom_charge);
        mBottomChargeBt.setOnClickListener(this);
        mBottomMyCountTv = (TextView) findViewById(R.id.gold_mall_bottom_my_gold_count);
        mBottomNeedCountTv = (TextView) findViewById(R.id.gold_mall_bottom_need_gold_count);
        mBottomHasSelectTv=(TextView)findViewById(R.id.gold_mall_bottom_selected_count);
        mCurrentActivityRootView = findViewById(R.id.root_view);
        mGridListView = (CustomListView) findViewById(R.id.goodslist_lv);
        mGridListView.setCanLoadMore(false);
        mGridListView.setHeadViewBg(getResources().getColor(R.color.white));
        mGridListView.addHeaderView(mHeaderView);
        mToTopView = (ImageView) findViewById(R.id.goodslist_to_top);
        mTitleTV = (TextView) findViewById(R.id.top_tv);
        mAdapter = new GoldListAdapter(this, mList);
        mAdapter.setRefreshCallback(this);
        mAdapter.setCurrentActivityRootView(mCurrentActivityRootView);
        mGridListView.setAdapter(mAdapter);
    }

    private void initData() {
//        mSelectCZNumList.add(10);
//        mSelectCZNumList.add(30);
//        mSelectCZNumList.add(50);
        mUser = UserHelper.getUser();
        if (UserHelper.isGusetUser(this)) {
            mBottomMyCountTv.setText("0");
        }
        else {
            mBottomMyCountTv.setText("" + mUser.getYcoins());
        }
        mBottomNeedCountTv.setText("0");
        mBottomHasSelectTv.setText(String.format(getResources().getString(R.string.gold_bottom_needgold), mHasSelectedCount));
        if (!Utils.isNetAvailable(this)) {
            ToastHelper.show(this,
                    getResources().getString(R.string.net_is_unable));
            return;
        }
        new GetGiftsThread(mPageSize, mCurrentPageIndex).start();
        if(mUser.getIsSelf()==GobalConstants.AllStatusYesOrNo.YES){
	        ServiceInterfaceHelper.getUserInfo(GoldMallActivity.this,mUser.getUid(), mUser.getToken(), mUser.getUid(),false ,
	        		new ServiceInterfaceHelper.InterfaceCallBack(){
	
						@Override
						public void getResultDTO(ResultDTO resultDTO) {
							
							if (resultDTO != null && resultDTO.getCode().equals("0")) {
								User user = GsonHelper.gsonToObj(resultDTO.getResult(),
										User.class);
								if (user != null) {
									mUser.setYcoins(user.getYcoins());
									 mBottomMyCountTv.setText("" + mUser.getYcoins());
									UserHelper.setUser(mUser);
									BroadCastHelper.sendRefreshMainBroadcast(
											GoldMallActivity.this,
											GobalConstants.RefreshType.USER);
								}
							}
						}
	        	
	        });
        }

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.gold_mall_left_lay:
            	BindHelper  	mBindHelper = new BindHelper(GoldMallActivity.this);
    			mBindHelper.judgeBind();
                break;
            case R.id.gold_mall_rigth_lay:
                Utils.goActivity(this,TelRechargeActivity.class);
                break;
            case R.id.activityback_bt:
                // back
                finish();
                break;
            case R.id.goodslist_to_top:
                mGridListView.smoothToTop();
                break;
//            case R.id.gold_mall_header_button:
//                handleCZBtOnClick();
//
//                break;
//            case R.id.gold_mall_select_num1:
//                mCurrentSelectedIndex = 0;
//                handleSelectBtOnClick(mCurrentSelectedIndex);
//                break;
//            case R.id.gold_mall_select_num2:
//                mCurrentSelectedIndex = 1;
//                handleSelectBtOnClick(mCurrentSelectedIndex);
//                break;
//            case R.id.gold_mall_select_num3:
//                mCurrentSelectedIndex = 2;
//                handleSelectBtOnClick(mCurrentSelectedIndex);
//                break;
            case R.id.gold_mall_bottom_charge:
                handleChargeOnClick();
                break;
            case R.id.golduseguide_tv:
            	Utils.goHtmlActivity(GoldMallActivity.this, getResources().getString(R.string.doc_coin_des),
    					GobalConstants.URL.YOYOBASE+GobalConstants.URL.DOC_COIN);
            	break;
        }
    }


    private void handleChargeOnClick()
    {
        if (UserHelper.isGusetUser(this))
        {
            Utils.goActivity(this,LoginRegisterActivity.class);
            return;
        }
        if(mUser==null) {
           mUser= UserHelper.getUser();
        }
        if(mUser==null||(mUser.getYcoins()-getSelectedPrice())<=0)
        {
            return;
        }
        if (NetworkStatus.getNetWorkStatus(this) > 0)
        {
            Intent lIntent = new Intent(this,
                    ExchangeActivity.class);
            lIntent.putParcelableArrayListExtra(
                    GobalConstants.Data.GIFTLIST, getSelectedGiftList());
            startActivity(lIntent);
            finish();
        }
        else
        {
            Toast.makeText(this,
                    getString(R.string.no_network), Toast.LENGTH_SHORT)
                    .show();
        }
    }
    private void handleSelectBtOnClick(int index) {
//        for (int i = 0; i < mSelectCZNumBtList.size(); i++) {
//            Button bt = mSelectCZNumBtList.get(i);
//            if (i == index) {
//                bt.setBackgroundResource(R.drawable.gold_mall_header_input_bg_selected);
//                bt.setTextColor(getResources().getColor(R.color.gold_mall_header_selected_color));
//            } else {
//                bt.setBackgroundResource(R.drawable.gold_mall_header_input_bg_normal);
//                bt.setTextColor(getResources().getColor(R.color.gold_mall_header_normal_color));
//            }
//        }

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        CallbackContext.onActivityResult(requestCode, resultCode, data);
    }

    private int getSelectedPrice()
    {
        int count = 0;
        for (Gift gift : mList)
        {
            if (gift.isSelected())
                count += gift.getFanliTaoPrice();
        }
        return count;
    }
    private ArrayList<Gift> getSelectedGiftList()
    {
        ArrayList<Gift> giftList = new ArrayList<Gift>();
        for (Gift gift : mList)
        {
            if (gift.isSelected())
                giftList.add(gift);
        }
        return giftList;
    }

    private int getSelectedCount()
    {
        int count = 0;
        for (Gift gift : mList)
        {
            if (gift.isSelected())
                count++;
        }
        return count;
    }

    private void clearSelected()
    {
        for (Gift gift : mList)
        {
            if (gift.isSelected())
                gift.setSelected(false);
        }
        mAdapter.notifyDataSetChanged();
        refreshScore();
    }

    public void refreshScore()
    {
        int price = getSelectedPrice();
        String scoreDes = String.format(getString(R.string.gold_bottom_needgold),
                getSelectedCount());
        mBottomHasSelectTv.setText(scoreDes);
        mBottomNeedCountTv.setText(String.valueOf(price));
        if (price == 0)
        {
//            mBottomChargeBt.setEnabled(false);
            mBottomChargeBt.setSelected(false);
        }
        else
        {
            int lack = price - mUser.getYcoins();
            if (lack > 0)
            {
                String lackStr = String.format(
                        getString(R.string.lackgoldcount), lack);
                // mLackTv.setText(lackStr);
                mBottomChargeBt.setEnabled(false);
                mBottomChargeBt.setSelected(false);
            }
            else
            {
                mBottomChargeBt.setEnabled(true);
                mBottomChargeBt.setSelected(true);
            }
        }
    }

    @Override
    public void refreshGift() {
        refreshScore();
    }


    class GetGiftsThread extends Thread {

        private Handler handler;
        private ResultDTO resultDTO;
        private int pagesize;
        private int curPage;
        private List<Gift> giftList;

        public GetGiftsThread(int pagesize, int curPage) {
            handler = new Handler();
            mProgressDialog = DialogHelper.showProgressDialog(
                    GoldMallActivity.this, getString(R.string.loginloading));
            this.pagesize = pagesize;
            this.curPage = curPage;
        }

        @Override
        public void run() {
            resultDTO = new GetGifts(mUser.getUid(), mUser.getToken(),
                    pagesize, curPage).getConnect();
            if (resultDTO != null && resultDTO.getCode().equals("0")) {
                giftList = GsonHelper.gsonToObj(resultDTO.getResult(),
                        new TypeToken<List<Gift>>() {
                        });
            }
            handler.post(new Runnable() {

                @Override
                public void run() {
                    DialogHelper.cancelProgressDialog(mProgressDialog);
                    if (giftList != null && giftList.size() > 0) {
                        mList.clear();
                        mList.addAll(giftList);
                        mAdapter.notifyDataSetChanged();
//                        if (mGiftId != 0)
//                        {
//                            int position = getGiftPosition(mGiftId);
//                            mListView.setSelection(position);
//                        }
                    }
                }
            });
        }
    }
}
