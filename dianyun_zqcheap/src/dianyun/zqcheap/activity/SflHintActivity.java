package dianyun.zqcheap.activity;

import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import dianyun.baobaowd.util.GobalConstants;
import dianyun.baobaowd.util.Utils;
import dianyun.zqcheap.R;
import dianyun.zqcheap.activitybase.BaseActivity;

public class SflHintActivity extends BaseActivity

{

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getTemplate().doInActivity(this, R.layout.sflhintactivity);
	}

	@Override
	public void findView() {
		super.findView();

		findViewById(R.id.shop_scan_pop_lay).setOnClickListener(
				new OnClickListener() {

					@Override
					public void onClick(View v) {

						Utils.goActivity(SflHintActivity.this,
								ShowScanResultActivity.class);

					}
				});
		findViewById(R.id.activityback_bt).setOnClickListener(
				new OnClickListener() {

					@Override
					public void onClick(View v) {

						finish();

					}
				});
		findViewById(R.id.role_tv).setOnClickListener(
				new OnClickListener() {
					
					@Override
					public void onClick(View v) {
						
						Utils.goHtmlActivity(SflHintActivity.this, getString(R.string.taobaolc),
								GobalConstants.URL.YOYOBASE+GobalConstants.URL.SHOP_SCAN_HELP_URL);
						
					}
				});
	}

}
