package dianyun.zqcheap.activity;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.alibaba.sdk.android.callback.CallbackContext;
import com.alibaba.sdk.android.session.model.Session;
import com.sina.weibo.sdk.auth.sso.SsoHandler;
import com.tencent.connect.UserInfo;
import com.tencent.tauth.IUiListener;
import com.tencent.tauth.UiError;
import com.umeng.analytics.MobclickAgent;

import dianyun.baobaowd.data.User;
import dianyun.baobaowd.db.LightDBHelper;
import dianyun.baobaowd.dto.ResultDTO;
import dianyun.baobaowd.entity.Constants;
import dianyun.baobaowd.gson.GsonHelper;
import dianyun.baobaowd.help.LogFile;
import dianyun.baobaowd.qq.QQConstants;
import dianyun.baobaowd.qq.QQLoginResult;
import dianyun.baobaowd.qq.QQPreferenceUtil;
import dianyun.baobaowd.serverinterface.QQLogin;
import dianyun.baobaowd.serverinterface.SinaWeiboLogin;
import dianyun.baobaowd.serverinterface.TaobaoLogin;
import dianyun.baobaowd.sinaweibo.AccessTokenKeeper;
import dianyun.baobaowd.sinaweibo.SinaWeiboHelper;
import dianyun.baobaowd.sinaweibo.SinaWeiboHelper.SinaLoginCallback;
import dianyun.baobaowd.util.ActivityManager;
import dianyun.baobaowd.util.BroadCastHelper;
import dianyun.baobaowd.util.DialogHelper;
import dianyun.baobaowd.util.GobalConstants;
import dianyun.baobaowd.util.NetworkStatus;
import dianyun.baobaowd.util.TaeSdkUtil;
import dianyun.baobaowd.util.ToastHelper;
import dianyun.baobaowd.util.UserHelper;
import dianyun.baobaowd.util.UserStatusHelper;
import dianyun.baobaowd.xiaomipush.RegisterPushHelper;
import dianyun.zqcheap.R;
import dianyun.zqcheap.activitybase.BaseActivity;
import dianyun.zqcheap.application.BaoBaoWDApplication;
import dianyun.zqcheap.service.BaoBaoWDService;
import dianyun.zqcheap.wxapi.WXHelper;

public class LoginRegisterActivity extends BaseActivity
{

	private LinearLayout mTaobaoLoginLayout;
	private LinearLayout mWeiboLoginLayout;
	private Button mPhoneRegisteLayout;
	private Button mPhoneLoginLayout;
	private ImageView mActivityBackBt;
	Dialog mProgressDialog;
	private final int SINAWEIBOAUTHSUCCESS = 1;
	private final int SINAWEIBOAUTHFAILED = 2;
	private final int QQAUTHSUCCESS = 3;
	private final int QQAUTHFAILED = 4;
//	private final int SENDMAIL = 5;
	private final int GETQQUSERINFOSUCCESS = 6;
	private final int GETQQUSERINFOFAILED = 7;
//	private final int BBLOGIN = 9;
	private final int QQLOGIN = 10;
	private final int SINAWEIBOLOGIN = 11;
	private final int TAELOGIN = 12;
	public static boolean mIsFromWelcome = false;
	private User mUser;
	Handler mHandler = new Handler()
	{

		@Override
		public void handleMessage(Message msg)
		{
			
			switch (msg.what)
			{
			case SINAWEIBOAUTHSUCCESS:
				String userinfo = AccessTokenKeeper.readUserJson(LoginRegisterActivity.this);
				MobclickAgent.onEvent(LoginRegisterActivity.this, "registerBy",
						"WeiBo");
				
				sinaWeiboLoginThread(userinfo, BaoBaoWDApplication.deviceId);
				break;
			case SINAWEIBOAUTHFAILED:
				cancelProgressDialog(mProgressDialog);
				Toast.makeText(LoginRegisterActivity.this,
						getString(R.string.loginfailed), Toast.LENGTH_SHORT)
						.show();
				break;
			case QQAUTHSUCCESS:
				try
				{
					QQLoginResult lQQLoginResult = GsonHelper.gsonToObj(
							msg.obj.toString(), QQLoginResult.class);
					QQPreferenceUtil.getInstance(LoginRegisterActivity.this)
							.saveString(QQConstants.ACCESS_TOKEN,
									lQQLoginResult.getAccess_token());
					QQPreferenceUtil.getInstance(LoginRegisterActivity.this)
							.saveString(QQConstants.OPENID,
									lQQLoginResult.getOpenid());
					QQPreferenceUtil.getInstance(LoginRegisterActivity.this)
							.saveLong(QQConstants.EXPIRES_IN,
									lQQLoginResult.getExpires_in());
					setAppIdToken();
					onClickUserInfo();
				}
				catch (Exception e)
				{
					LogFile.SaveExceptionLog(e);
				}
				break;
			case QQAUTHFAILED:
				cancelProgressDialog(mProgressDialog);
				Toast.makeText(LoginRegisterActivity.this,
						getString(R.string.loginfailed), Toast.LENGTH_SHORT)
						.show();
				break;
//			case SENDMAIL:
//				ResultDTO resultDTO2 = (ResultDTO) msg.obj;
//				if (resultDTO2 != null && resultDTO2.getCode().equals("0"))
//				{
//					Toast.makeText(LoginRegisterActivity.this,
//							getString(R.string.sendmailsuccess),
//							Toast.LENGTH_SHORT).show();
//				}
//				else
//				{
//					Toast.makeText(LoginRegisterActivity.this,
//							getString(R.string.sendmailfailed),
//							Toast.LENGTH_SHORT).show();
//				}
//				break;
			case GETQQUSERINFOSUCCESS:
				JSONObject jsonObject = (JSONObject) msg.obj;
				try
				{
					
				
					if (jsonObject.getString("ret").equals("0"))
					{
						MobclickAgent.onEvent(LoginRegisterActivity.this,
								"registerBy", "QQ");
						QQPreferenceUtil
								.getInstance(LoginRegisterActivity.this)
								.saveString(QQConstants.USERINFOJSON,
										jsonObject.toString());
						LightDBHelper.setLoginType(LoginRegisterActivity.this,
								GobalConstants.LoginType.QQ);
						String openId = QQPreferenceUtil.getInstance(
								LoginRegisterActivity.this).getString(
								QQConstants.OPENID, "");
						qqLoginThread(openId, jsonObject.toString(), BaoBaoWDApplication.deviceId);
					}
					else
					{
						ToastHelper.show(LoginRegisterActivity.this,
								getString(R.string.loginloadingfailed));
					}
				}
				catch (JSONException e)
				{
					LogFile.SaveExceptionLog(e);
				}
				break;
			case GETQQUSERINFOFAILED:
				cancelProgressDialog(mProgressDialog);
				ToastHelper.show(LoginRegisterActivity.this,
						getString(R.string.loginloadingfailed));
				break;
			
//			case BBLOGIN:
//				mLoginBt.setEnabled(true);
//				handLoginResult((ResultDTO) msg.obj);
//				break;
			case QQLOGIN:
				LightDBHelper.setLoginType(LoginRegisterActivity.this,
						GobalConstants.LoginType.QQ);
				handLoginResult((ResultDTO) msg.obj);
				break;
			case SINAWEIBOLOGIN:
				LightDBHelper.setLoginType(LoginRegisterActivity.this,
						GobalConstants.LoginType.SINAWEIBO);
				handLoginResult((ResultDTO) msg.obj);
				break;
			case TAELOGIN:
				LightDBHelper.setLoginType(LoginRegisterActivity.this,
						GobalConstants.LoginType.TAE);
				handLoginResult((ResultDTO) msg.obj);
				break;
			}
			super.handleMessage(msg);
		}
	};

	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		ActivityManager.pushActivity(LoginRegisterActivity.this);
		getTemplate().doInActivity(this, R.layout.loginregisteractivity);
		MobclickAgent.onEvent(LoginRegisterActivity.this, "loginregister");
		
		Intent itn = getIntent();
		if (itn != null && itn.getStringExtra(Constants.EXTRA_NAME) != null
				&& itn.getStringExtra(Constants.EXTRA_NAME).equals("welcome"))
		{
			mIsFromWelcome = true;
		}
	}

	@Override
	protected void onDestroy()
	{
		mIsFromWelcome = false;
		 if (mRefreshReceiver != null) {
	            unregisterReceiver(mRefreshReceiver);
	            mRefreshReceiver = null;
	        }
		super.onDestroy();
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event)
	{
		if (keyCode == event.KEYCODE_BACK)
		{
			finish();
			overridePendingTransition(0, R.anim.push_bottom_out);
		}
		return super.onKeyDown(keyCode, event);
	}

	@Override
	public void findView()
	{
		super.findView();
		mActivityBackBt = (ImageView) findViewById(R.id.welcome_closeimage);
		mTaobaoLoginLayout = (LinearLayout) findViewById(R.id.taobaologin_layout);
		mWeiboLoginLayout = (LinearLayout) findViewById(R.id.weibologin_layout);
		mPhoneRegisteLayout = (Button) findViewById(R.id.phoneregist_bt);
		mPhoneLoginLayout = (Button) findViewById(R.id.phonelogin_bt);
		mWeiboLoginLayout.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View v)
			{
				if (NetworkStatus.getNetWorkStatus(LoginRegisterActivity.this) > 0)
				{
					MobclickAgent.onEvent(LoginRegisterActivity.this,
							"registerClick", "WeiBo");
					mProgressDialog = DialogHelper.showProgressDialog(
							LoginRegisterActivity.this,
							getString(R.string.logining));
					
					mSsoHandler = SinaWeiboHelper.login(LoginRegisterActivity.this,new SinaLoginCallback() {
						
						@Override
						public void result(boolean success) {
							if (success)
							{
								mHandler.sendEmptyMessage(SINAWEIBOAUTHSUCCESS);
							}
							else
							{
								mHandler.sendEmptyMessage(SINAWEIBOAUTHFAILED);
							}
							
						}
					});
					
				}
				else
				{
					Toast.makeText(LoginRegisterActivity.this,
							getString(R.string.no_network), Toast.LENGTH_SHORT)
							.show();
				}
			}
		});
		mPhoneRegisteLayout.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View v)
			{
				startActivity(PhoneRegisteActivity.class);
			}
		});
		mPhoneLoginLayout.setOnClickListener(new OnClickListener()
		{
			
			@Override
			public void onClick(View v)
			{
				startActivity(PhoneLoginActivity.class);
//				WXHelper.login(LoginRegisterActivity.this);
				
				
				
			}
		});
		mTaobaoLoginLayout.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View v)
			{
				if (NetworkStatus.getNetWorkStatus(LoginRegisterActivity.this) > 0)
				{
					mProgressDialog = DialogHelper.showProgressDialog(
							LoginRegisterActivity.this,
							getString(R.string.logining));
					TaeSdkUtil.showLogin(LoginRegisterActivity.this,new TaeSdkUtil.TAELoginCallback(){

						@Override
						public void result(Session session) {
							if(session!=null){
								taeLoginThread(session, BaoBaoWDApplication.deviceId);
							}else{
								DialogHelper.cancelProgressDialog(mProgressDialog);
								Toast.makeText(LoginRegisterActivity.this,
										getString(R.string.loginfailed), Toast.LENGTH_SHORT).show();
							}
						}
					});
				}
				else
				{
					Toast.makeText(LoginRegisterActivity.this,
							getString(R.string.no_network), Toast.LENGTH_SHORT)
							.show();
				}
			}
		});
		mActivityBackBt.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View v)
			{
				finish();
				if (mIsFromWelcome)
				{
					overridePendingTransition(0, R.anim.push_bottom_out);
				}
			}
		});

		setReceiver();
	}

	private void onClickUserInfo()
	{
		mProgressDialog = DialogHelper.showProgressDialog(
				LoginRegisterActivity.this, getString(R.string.logining));
		if (ready(LoginRegisterActivity.this))
		{
			UserInfo mInfo = new UserInfo(this, mTencent.getQQToken());
			mInfo.getUserInfo(new GetUserInfoBaseUIListener(this,
					"get_simple_userinfo"));
		}
	}



	public class GetUserInfoBaseUIListener implements IUiListener
	{

		private String mScope;

		public GetUserInfoBaseUIListener(Context mContext, String mScope)
		{
			super();
			this.mScope = mScope;
		}

		@Override
		public void onComplete(Object response)
		{
			Message msg = new Message();
			msg.what = GETQQUSERINFOSUCCESS;
			msg.obj = response;
			mHandler.sendMessage(msg);
		}

		@Override
		public void onError(UiError e)
		{
			mHandler.sendEmptyMessage(GETQQUSERINFOFAILED);
		}

		@Override
		public void onCancel()
		{
		}
	}

	private void qqLoginThread(final String openid, final String jsonContent,
			final String deviceId)
	{
		new Thread()
		{

			@Override
			public void run()
			{
				ResultDTO resultDTO = new QQLogin(LoginRegisterActivity.this,
						openid, jsonContent, deviceId
						).postConnect();
				Message msg = new Message();
				msg.what = QQLOGIN;
				msg.obj = resultDTO;
				mHandler.sendMessage(msg);
			}
		}.start();
	}

	private void sinaWeiboLoginThread(final String jsonContent,
			final String deviceId)
	{
		new Thread()
		{

			@Override
			public void run()
			{
				ResultDTO resultDTO = new SinaWeiboLogin(
						LoginRegisterActivity.this, jsonContent, deviceId
						)
						.postConnect();
				Message msg = new Message();
				msg.what = SINAWEIBOLOGIN;
				msg.obj = resultDTO;
				mHandler.sendMessage(msg);
			}
		}.start();
	}
	private void taeLoginThread(final Session session,
			final String deviceId)
	{
		new Thread()
		{
			
			@Override
			public void run()
			{
				ResultDTO resultDTO = new TaobaoLogin(
						LoginRegisterActivity.this,session.getUserId(), session.getUser().avatarUrl,
						session.getUser().nick).postConnect();
				Message msg = new Message();
				msg.what = TAELOGIN;
				msg.obj = resultDTO;
				mHandler.sendMessage(msg);
			}
		}.start();
	}
	
	
	
	
	
	
	private void handLoginResult(ResultDTO resultDTO)
	{
		cancelProgressDialog(mProgressDialog);
		if (mUser != null)
			return;
		if (resultDTO != null)
		{
			if (resultDTO.getCode().equals("0"))
			{
				mUser = GsonHelper.gsonToObj(resultDTO.getResult(), User.class);
				mUser.setIsSelf(GobalConstants.AllStatusYesOrNo.YES);
				UserStatusHelper.saveUserStatus(LoginRegisterActivity.this,
						mUser);
				UserHelper.addGusetUser(LoginRegisterActivity.this);
				UserHelper.guestCopyUser(LoginRegisterActivity.this, mUser);
				UserHelper.setUser(mUser);
				LightDBHelper.setPushShare(LoginRegisterActivity.this, true);
				Intent lIntent = new Intent(LoginRegisterActivity.this,
						BaoBaoWDService.class);
				stopService(lIntent);
				RegisterPushHelper.logout(LoginRegisterActivity.this);
				BroadCastHelper.sendRefreshMainBroadcast(
						LoginRegisterActivity.this,
						GobalConstants.RefreshType.CHANGEUSER);
				TaeSdkUtil.bindAccount(LoginRegisterActivity.this, mUser.getUid());
				if (mUser.getRegTime() == null || mUser.getRegTime().equals(""))
				{
					Intent intent = new Intent(this, StatusActivity.class);
					intent.putExtra(Constants.EXTRA_NAME, "welcome");
					startActivity(intent);
					overridePendingTransition(R.anim.push_left_in,
							R.anim.push_right_out);
				}
				else if (mIsFromWelcome)
				{
					Intent itn = new Intent(this, MainActivity.class);
					startActivity(itn);
					overridePendingTransition(R.anim.push_left_in,
							R.anim.push_right_out);
				}
				ActivityManager.popAllActivity();
				finish();
			}
			else if (resultDTO.getCode().equals("-1011"))
			{
				Toast.makeText(LoginRegisterActivity.this,
						getString(R.string.pwerror), Toast.LENGTH_SHORT).show();
				cancelProgressDialog(mProgressDialog);
			}
			else if (resultDTO.getCode().equals("-1012"))
			{
				Toast.makeText(LoginRegisterActivity.this,
						getString(R.string.errorcode_1012_hint),
						Toast.LENGTH_SHORT).show();
				cancelProgressDialog(mProgressDialog);
			}
			else if (resultDTO.getCode().equals("-1002"))
			{
				Toast.makeText(LoginRegisterActivity.this,
						getString(R.string.usernotexist), Toast.LENGTH_SHORT)
						.show();
				cancelProgressDialog(mProgressDialog);
			}
			else
			{
				Toast.makeText(LoginRegisterActivity.this,
						getString(R.string.loginfailed), Toast.LENGTH_SHORT)
						.show();
				cancelProgressDialog(mProgressDialog);
			}
		}
		else
		{
			Toast.makeText(LoginRegisterActivity.this,
					getString(R.string.loginfailed), Toast.LENGTH_SHORT).show();
			cancelProgressDialog(mProgressDialog);
		}
	}
	
	
	
	public void cancelProgressDialog(Dialog progressDialog){
		if(progressDialog!=null){
			progressDialog.cancel();
			progressDialog=null;
		}
	}
	
	SsoHandler mSsoHandler;
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		
		if (mSsoHandler != null) {
			mSsoHandler.authorizeCallBack(requestCode, resultCode, data);
		}
		 
		  CallbackContext.onActivityResult(requestCode, resultCode, data);
	}
	RefreshReceiver mRefreshReceiver;
	
	public class RefreshReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            byte refreshType = intent.getByteExtra(
                    GobalConstants.Data.REFRESHTYPE, (byte) 0);
            if (refreshType == GobalConstants.RefreshType.WXLOGINSUCCESS) {
                WXHelper.getUserInfo(LoginRegisterActivity.this);
            } 
        }
    }
	
	
	private void setReceiver() {
        try {
            if (null == mRefreshReceiver) {
                // 房间状态
                mRefreshReceiver = new RefreshReceiver();
                registerReceiver(mRefreshReceiver, new IntentFilter(
                        GobalConstants.IntentFilterAction.REFRESH));
            }
        } catch (Exception e) {
            LogFile.SaveExceptionLog(e);
        }
    }
	
	
	
	
}

