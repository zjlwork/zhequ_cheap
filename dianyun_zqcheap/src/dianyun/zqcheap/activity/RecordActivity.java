package dianyun.zqcheap.activity;

import java.util.ArrayList;
import java.util.List;

import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import dianyun.baobaowd.adapter.ExchangeRecordPagerViewHelper;
import dianyun.baobaowd.adapter.viewpagerbase.MyViewPagerAdapter;
import dianyun.baobaowd.adapter.viewpagerbase.PagerViewHelper;
import dianyun.baobaowd.data.User;
import dianyun.baobaowd.defineview.xlistview.CustomListView;
import dianyun.baobaowd.util.UserHelper;
import dianyun.zqcheap.R;
import dianyun.zqcheap.activitybase.BaseActivity;

public class RecordActivity extends BaseActivity
{

	CustomListView mListView;
	private Button mActivityBackBt;
//	RelativeLayout mMyOrderLayout;
//	ExchangeRecordAdapter mExchangeRecordAdapter;
//	private List<YcoinPay> mApplyList;
	private User mUser;
	
	private MyViewPagerAdapter mMyViewPagerAdapter;
	private List<PagerViewHelper> mPagerViewHelperList = new ArrayList<PagerViewHelper>();
	private ViewPager mViewPager;
	
	public static final int RECORD_TIXIAN=0;
	public static final int RECORD_PRIZE=1;
	
	
//	private TextView tixiancount_tv;
//	private TextView prizecount_tv;
	private TextView tixian_tv;
	private TextView prize_tv;
	private View tixian_v;
	private View prize_v;

	
	
	
	
	
	
	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		getTemplate().doInActivity(this, R.layout.recordactivity);
	}

	@Override
	public void initData()
	{
		super.initData();
		mUser = UserHelper.getUser();
		mActivityBackBt = (Button) findViewById(R.id.activityback_bt);
		mActivityBackBt.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View v)
			{
				finish();
			}
		});
		
//		tixiancount_tv = (TextView) findViewById(R.id.tixiancount_tv);
//		prizecount_tv = (TextView) findViewById(R.id.prizecount_tv);
		tixian_tv = (TextView) findViewById(R.id.tixian_tv);
		prize_tv = (TextView) findViewById(R.id.prize_tv);
		tixian_v =  findViewById(R.id.tixian_v);
		prize_v = findViewById(R.id.prize_v);
		
		tixian_tv.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View v)
			{
				mViewPager.setCurrentItem(0);
			}
		});
		prize_tv.setOnClickListener(new OnClickListener()
		{
			
			@Override
			public void onClick(View v)
			{
				mViewPager.setCurrentItem(1);
			}
		});
		
//		mListView = (CustomListView) findViewById(R.id.listview);
//		View header = LayoutInflater.from(RecordActivity.this).inflate(
//				R.layout.rebateheader, null);
//		mListView.addHeaderView(header);
//		mListView.setDividerHeight(0);
//		mListView.setCacheColorHint(0);
//		mApplyList = new ArrayList<YcoinPay>();
//		mExchangeRecordAdapter = new ExchangeRecordAdapter(mApplyList,
//				RecordActivity.this);
//		mListView.setAdapter(mExchangeRecordAdapter);
		
		
//		mListView.setOnRefreshListener(new OnRefreshListener()
//		{
//
//			@Override
//			public void onRefresh()
//			{
//				if (NetworkStatus.getNetWorkStatus(RecordActivity.this) > 0)
//				{
//					new GetApplyListThread(1,OrderRecordActivity.PAGESIZE)
//							.start();
//				}
//				else
//				{
//					Toast.makeText(RecordActivity.this,
//							getString(R.string.no_network), Toast.LENGTH_SHORT)
//							.show();
//					mListView.onRefreshComplete();
//				}
//			}
//		});
//		mListView.setOnLoadListener(new OnLoadMoreListener()
//		{
//
//			@Override
//			public void onLoadMore()
//			{
//				if (NetworkStatus.getNetWorkStatus(RecordActivity.this) > 0)
//				{
//					int nextpage = getNextPage();
//					new GetApplyListThread(nextpage,OrderRecordActivity.PAGESIZE).start();
//				}
//				else
//				{
//					Toast.makeText(RecordActivity.this,
//							getString(R.string.no_network), Toast.LENGTH_SHORT)
//							.show();
//					mListView.onLoadMoreComplete();
//				}
//			}
//		});
//		noDataStatus(mApplyList);
//		mListView.refresh();
		
		initPageAdapter();
		
		
		
	}

	
	
	
	private void initPageAdapter()
	{
		mViewPager = (ViewPager) findViewById(R.id.viewpager);
		mViewPager.setOffscreenPageLimit(2);
		PagerViewHelper lPagerViewHelper = new ExchangeRecordPagerViewHelper(
				RecordActivity.this, RECORD_TIXIAN);
		
		PagerViewHelper lPagerViewHelper2 = new ExchangeRecordPagerViewHelper(
				RecordActivity.this,RECORD_PRIZE);
		
		mPagerViewHelperList.add(lPagerViewHelper);
		mPagerViewHelperList.add(lPagerViewHelper2);
		mMyViewPagerAdapter = new MyViewPagerAdapter(
				mPagerViewHelperList, RecordActivity.this);
		mViewPager.setAdapter(mMyViewPagerAdapter);
		mViewPager.setOnPageChangeListener(new OnPageChangeListener()
		{

			@Override
			public void onPageSelected(int position)
			{
				PagerViewHelper lMyAnswersPagerViewHelper = mPagerViewHelperList
						.get(position);
				refreshTypeLayout(lMyAnswersPagerViewHelper.getType());
			}

			@Override
			public void onPageScrolled(int arg0, float arg1, int arg2)
			{
			}

			@Override
			public void onPageScrollStateChanged(int arg0)
			{
			}
		});
		refreshTypeLayout(RECORD_TIXIAN);
	}
	
	
	private void refreshTypeLayout(int type)
	{
		if (type == RECORD_TIXIAN)
		{
			tixian_tv.setSelected(true);
			prize_tv.setSelected(false);
			tixian_v.setVisibility(View.VISIBLE);
			prize_v.setVisibility(View.GONE);
		}
		else if (type == RECORD_PRIZE)
		{
			tixian_tv.setSelected(false);
			prize_tv.setSelected(true);
			tixian_v.setVisibility(View.GONE);
			prize_v.setVisibility(View.VISIBLE);
		}
	}
	
	
	@Override
	protected void onResume()
	{
		super.onResume();
		
		mUser = UserHelper.getUser();
		
		
	}
	
	
	
//	private int getNextPage()
//	{
//		if (mApplyList == null || mApplyList.size() == 0)
//			return 1;
//		else
//			return mApplyList.size() / OrderRecordActivity.PAGESIZE + 1;
//	}
//
//	private void noDataStatus(List<YcoinPay> list)
//	{
//		if (list != null && list.size() > 0)
//		{
//			if (list.size() < 20)
//				mListView.setCanLoadMore(false);
//			else
//				mListView.setCanLoadMore(true);
//		}
//		else
//		{
//			mListView.setCanLoadMore(false);
//		}
//	}
//
//	
//	class GetApplyListThread extends Thread
//	{
//
//		private Handler handler;
//		private ResultDTO resultDTO;
//		int curPage;
//		int pagesize;
//
//		public GetApplyListThread(int curPage, int pagesize)
//		{
//			handler = new Handler();
//			this.curPage = curPage;
//			this.pagesize = pagesize;
//		}
//
//		@Override
//		public void run()
//		{
//			resultDTO = new GetApplyList(mUser.getUid(), mUser.getToken(),
//					curPage, pagesize).getConnect();
//			handler.post(new Runnable()
//			{
//
//				@Override
//				public void run()
//				{
//					if (resultDTO != null && resultDTO.getCode().equals("0"))
//					{
//						List<YcoinPay> applyList = GsonHelper.gsonToObj(
//								resultDTO.getResult(),
//								new TypeToken<List<YcoinPay>>()
//								{
//								});
//						if (curPage == 0 || curPage == 1)
//							mApplyList.clear();
//						if (applyList != null && applyList.size() > 0)
//							mApplyList.addAll(applyList);
//						mExchangeRecordAdapter.notifyDataSetChanged();
//						noDataStatus(applyList);
//						LightDBHelper.setCashCount(RecordActivity.this, 0);
//						BroadCastHelper.sendRefreshMainBroadcast(RecordActivity.this,GobalConstants.RefreshType.ALLNEW);
//					}
//					if (curPage == 0 || curPage == 1)
//						mListView.onRefreshComplete();
//					else
//						mListView.onLoadMoreComplete();
//				}
//			});
//		}
//	}
}
