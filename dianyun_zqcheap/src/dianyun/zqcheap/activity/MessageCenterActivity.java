package dianyun.zqcheap.activity;

import java.util.ArrayList;
import java.util.List;

import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.reflect.TypeToken;
import com.umeng.analytics.MobclickAgent;

import dianyun.baobaowd.adapter.SystemMsgAdapter;
import dianyun.baobaowd.data.Message;
import dianyun.baobaowd.data.User;
import dianyun.baobaowd.db.LightDBHelper;
import dianyun.baobaowd.defineview.xlistview.CustomListView;
import dianyun.baobaowd.defineview.xlistview.CustomListView.OnLoadMoreListener;
import dianyun.baobaowd.defineview.xlistview.CustomListView.OnRefreshListener;
import dianyun.baobaowd.dto.ResultDTO;
import dianyun.baobaowd.gson.GsonHelper;
import dianyun.baobaowd.serverinterface.GetMessageList;
import dianyun.baobaowd.util.BroadCastHelper;
import dianyun.baobaowd.util.GobalConstants;
import dianyun.baobaowd.util.NetworkStatus;
import dianyun.baobaowd.util.UserHelper;
import dianyun.zqcheap.R;
import dianyun.zqcheap.activitybase.BaseActivity;

public class MessageCenterActivity extends BaseActivity {

//	private int mType = GobalConstants.MessageCenterType.SYSTEM;
	private Button mActivityBackBt;
	private User mUser;
	private TextView mTitleTv;
	private CustomListView mListView;



	private SystemMsgAdapter mSystemMsgAdapter;
	private List<Message> mMessageList = new ArrayList<Message>();


	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getTemplate().doInActivity(this, R.layout.messagecenteractivity);
	}

	@Override
	public void onResume() {
		super.onResume();
		MobclickAgent.onPageStart("消息中心");
	}

	@Override
	public void onPause() {
		super.onPause();
		MobclickAgent.onPageEnd("消息中心");
	}

	@Override
	public void onStart() {
		super.onStart();
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
	}

	@Override
	public void initData() {
		mTitleTv = (TextView) findViewById(R.id.title_tv);
		mActivityBackBt = (Button) findViewById(R.id.activityback_bt);
		mActivityBackBt.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				finish();
			}
		});
		mListView = (CustomListView) findViewById(R.id.listview);
		mListView.setCacheColorHint(0);
		mUser = UserHelper.getUser();
	
		mSystemMsgAdapter = new SystemMsgAdapter(mMessageList,
				MessageCenterActivity.this);

//		mType = getIntent().getIntExtra(GobalConstants.Data.MESSAGETYPE, 0);
//		if (mType == GobalConstants.MessageCenterType.SYSTEM) {
			mTitleTv.setText(getString(R.string.tabtwo_msg));
			mListView.setAdapter(mSystemMsgAdapter);
//		}

		mListView.setOnRefreshListener(new OnRefreshListener() {

			@Override
			public void onRefresh() {
				if (NetworkStatus.getNetWorkStatus(MessageCenterActivity.this) > 0) {

//					if (mType == GobalConstants.MessageCenterType.SYSTEM)
						new GetSystemMsgThread( 0, 0, 0, 0, true)
								.start();

				} else {
					Toast.makeText(MessageCenterActivity.this,
							getString(R.string.no_network), Toast.LENGTH_SHORT)
							.show();
					mListView.onRefreshComplete();
				}
			}
		});
		mListView.setOnLoadListener(new OnLoadMoreListener() {

			@Override
			public void onLoadMore() {
				if (NetworkStatus.getNetWorkStatus(MessageCenterActivity.this) > 0) {
//					if (mType == GobalConstants.MessageCenterType.SYSTEM) {
						long maxSeqId = getSystemMsgMaxSeqId();
						new GetSystemMsgThread( 0, 0, 0, maxSeqId, false)
								.start();
//					}

				} else {
					Toast.makeText(MessageCenterActivity.this,
							getString(R.string.no_network), Toast.LENGTH_SHORT)
							.show();
					mListView.onLoadMoreComplete();
				}
			}
		});
		mListView.setCanRefresh(true);
//		getDatabyDataBase();
		mListView.refresh();
	}



	private void refreshLoadMore(List rempList) {
		if (rempList != null && rempList.size() > 0) {
			if (rempList.size() < 20)
				mListView.setCanLoadMore(false);
			else
				mListView.setCanLoadMore(true);
		} else {
			mListView.setCanLoadMore(false);
		}
	}


	private void refreshSystemMsg(List<Message> messageList, boolean getnew) {
		if (getnew)
			mMessageList.clear();
		if (messageList != null && messageList.size() > 0)
			mMessageList.addAll(messageList);
		mSystemMsgAdapter.notifyDataSetChanged();
	}

	public long getSystemMsgMaxSeqId() {
		if (mMessageList.size() > 0) {
			return mMessageList.get(mMessageList.size() - 1).getSeqId();
		}
		return 0;
	}

	public void getDatabyDataBase() {/*
		if (mType == GobalConstants.MessageCenterType.SYSTEM) {
			mListView.setAdapter(mSystemMsgAdapter);
			List<Message> msgList = SystemMsgHelper
					.getMessageList(MessageCenterActivity.this);
			if (msgList != null && msgList.size() > 0)
				refreshSystemMsg(msgList, true);
			refreshLoadMore(mMessageList);
		}

	*/};

	class GetSystemMsgThread extends Thread {

		private Handler handler;
		private ResultDTO resultDTO;
		private int pagesize;
		private int curPage;
		private long minSeqId;
		private long maxSeqId;
		private List<Message> msgList;
		private boolean getnew;

		public GetSystemMsgThread( int pagesize, int curPage,
				long minSeqId, long maxSeqId, boolean getnew) {
			handler = new Handler();
			this.pagesize = pagesize;
			this.curPage = curPage;
			this.minSeqId = minSeqId;
			this.maxSeqId = maxSeqId;
			this.getnew = getnew;
		}

		@Override
		public void run() {
			resultDTO = new GetMessageList(mUser.getUid(), mUser.getToken(),
					pagesize, curPage, minSeqId, maxSeqId).getConnect();
			handler.post(new Runnable() {

				@Override
				public void run() {
					if (resultDTO != null && resultDTO.getCode().equals("0")) {
						msgList = GsonHelper.gsonToObj(resultDTO.getResult(),
								new TypeToken<List<Message>>() {
								});
						
						refreshSystemMsg(msgList, getnew);
						refreshLoadMore(msgList);
						LightDBHelper.setSystemMsgCount(MessageCenterActivity.this, 0);
						BroadCastHelper.sendRefreshMainBroadcast(MessageCenterActivity.this,GobalConstants.RefreshType.ALLNEW);
					}
					if (getnew) mListView.onRefreshComplete();
					else mListView.onLoadMoreComplete();
						
					
				}
			});
		}
	}
}
