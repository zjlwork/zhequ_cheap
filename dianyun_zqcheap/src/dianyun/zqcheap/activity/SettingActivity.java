package dianyun.zqcheap.activity;

import java.io.File;
import java.util.Date;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager.NameNotFoundException;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.umeng.analytics.MobclickAgent;

import dianyun.baobaowd.data.User;
import dianyun.baobaowd.data.VersionData;
import dianyun.baobaowd.db.DBHelper;
import dianyun.baobaowd.db.LightDBHelper;
import dianyun.baobaowd.defineview.Switch;
import dianyun.baobaowd.handler.DownloadThread;
import dianyun.baobaowd.handler.NotifyHandler;
import dianyun.baobaowd.handler.NotifyThread;
import dianyun.baobaowd.help.FileHelper;
import dianyun.baobaowd.help.LogFile;
import dianyun.baobaowd.interfaces.DialogCallBack;
import dianyun.baobaowd.serverinterface.CheckVersion;
import dianyun.baobaowd.util.ActivityManager;
import dianyun.baobaowd.util.BroadCastHelper;
import dianyun.baobaowd.util.DateHelper;
import dianyun.baobaowd.util.DialogHelper;
import dianyun.baobaowd.util.GobalConstants;
import dianyun.baobaowd.util.NetworkStatus;
import dianyun.baobaowd.util.TaeSdkUtil;
import dianyun.baobaowd.util.ToastHelper;
import dianyun.baobaowd.util.UserHelper;
import dianyun.baobaowd.util.WebInsideHelper;
import dianyun.baobaowd.xiaomipush.RegisterPushHelper;
import dianyun.zqcheap.R;
import dianyun.zqcheap.activitybase.BaseActivity;
import dianyun.zqcheap.service.BaoBaoWDService;
public class SettingActivity extends BaseActivity
{

	private RelativeLayout mScoreLayout;

	private Switch mPushSwitch;
	private RelativeLayout mLogoutLayout;
	private RelativeLayout mClauseLayout;
	private RelativeLayout mUpdateLayout;
	private RelativeLayout mChangePwLayout;
	private LinearLayout mTotalLayout;
	private Button mActivityBackBt;
	private TextView mUpdateVersionTv;
	private TextView mUidTv;
	
	private User mUser;
	
	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		
		getTemplate().doInActivity(this, R.layout.settingactivity);
	}

	@Override
	public void initData()
	{
		mUser = UserHelper.getUser();
		mTotalLayout = (LinearLayout) findViewById(R.id.total_layout);
		mChangePwLayout = (RelativeLayout) findViewById(R.id.changepw_layout);
		mScoreLayout = (RelativeLayout) findViewById(R.id.score_layout);
		mTotalLayout.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View v)
			{
			}
		});
		if(LightDBHelper.getLoginType(SettingActivity.this)==GobalConstants.LoginType.PHONE){
			mChangePwLayout.setVisibility(View.VISIBLE);
		}else{
			mChangePwLayout.setVisibility(View.GONE);
		}
		mChangePwLayout.setOnClickListener(new OnClickListener()
		{
			
			@Override
			public void onClick(View v)
			{
				
				
				Intent lIntent = new Intent(SettingActivity.this,
						PhoneRegisteActivity.class);
				lIntent.putExtra(GobalConstants.Data.PHONEOPERATE,
						GobalConstants.Data.PHONEJUSTCHANGEPW);
				startActivity(lIntent);
			}
		});
		mClauseLayout = (RelativeLayout) findViewById(R.id.clause_layout);
		mLogoutLayout = (RelativeLayout) findViewById(R.id.logout_layout);
		mUpdateLayout = (RelativeLayout) findViewById(R.id.update_layout);
		mUpdateVersionTv = (TextView) findViewById(R.id.updateversion_tv);
		mPushSwitch = (Switch) findViewById(R.id.pushswitch);
		mUidTv = (TextView) findViewById(R.id.uid_tv);
		mUidTv.setText("uid:" + UserHelper.getUser().getUid());
		mActivityBackBt = (Button) findViewById(R.id.activityback_bt);
		mActivityBackBt.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View v)
			{
				finish();
			}
		});
		
		mUpdateLayout.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View v)
			{
				if (NetworkStatus.getNetWorkStatus(SettingActivity.this) > 0)
					new CheckVersionThread().start();
				else
					Toast.makeText(SettingActivity.this,
							getString(R.string.no_network), Toast.LENGTH_SHORT)
							.show();
			}
		});
		mClauseLayout.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View v)
			{
			
				WebInsideHelper.goWebInside(SettingActivity.this, GobalConstants.URL.WEBHTMLBASE
						+ GobalConstants.URL.TREATY, getString(R.string.clause));
			}
		});
		mScoreLayout.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View v)
			{
				try
				{
					Uri uri = Uri.parse("market://details?id="
							+ getPackageName());
					Intent lIntent = new Intent(Intent.ACTION_VIEW, uri);
					startActivity(lIntent);
				}
				catch (Exception e)
				{
					LogFile.SaveExceptionLog(e);
				}
			}
		});
		
		if (LightDBHelper.getPushShare(SettingActivity.this))
		{
			mPushSwitch.setChecked(true);
		}
		else
		{
			mPushSwitch.setChecked(false);
		}
		
		mPushSwitch.setOnCheckedChangeListener(new OnCheckedChangeListener()
		{

			@Override
			public void onCheckedChanged(CompoundButton buttonView,
					boolean isChecked)
			{
				LightDBHelper.setPushShare(SettingActivity.this, isChecked);
			}
		});
		mLogoutLayout.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View v)
			{
				Dialog alertDialog = new AlertDialog.Builder(
						SettingActivity.this)
						.setTitle(getString(R.string.confirmlogout))
						.setPositiveButton(getString(R.string.OkButton),
								new DialogInterface.OnClickListener()
								{

									@Override
									public void onClick(DialogInterface dialog,
											int which)
									{
										dialog.cancel();
										new LogoutThread().start();
									}
								})
						.setNegativeButton(getString(R.string.CancelButton),
								new DialogInterface.OnClickListener()
								{

									@Override
									public void onClick(DialogInterface dialog,
											int which)
									{
										dialog.cancel();
									}
								}).create();
				alertDialog.show();
			}
		});
		try
		{
			String versionname = getPackageManager().getPackageInfo(
					getPackageName(), 0).versionName;
			mUpdateVersionTv.setText(getString(R.string.updateversion) + "("
					+ versionname + ")");
		}
		catch (NameNotFoundException e)
		{
			e.printStackTrace();
		}
	}

	
	
	Dialog mDialog;
	class LogoutThread extends Thread
	{

		private Handler handler;
	
		public LogoutThread()
		{
			handler = new Handler();
			mDialog = DialogHelper.showProgressDialog(SettingActivity.this,getString(R.string.logouting));
			mDialog.setCanceledOnTouchOutside(false);
			mDialog.setCancelable(false);
			deleteImportent();
		}

		@Override
		public void run()
		{
			deleteUserData();
			handler.post(new Runnable()
			{
				
				@Override
				public void run()
				{
					UserHelper.setUser(null);
					if (mDialog != null)
					{
						mDialog.cancel();
						mDialog = null;
					}
					ActivityManager.popAllActivity();
					finish();
				}
			});
		}
	}
	
	
	
	private void deleteImportent(){
		try
		{
			tencentLogout();
//			
//			SinaWeiboUtil.getInstance(SettingActivity.this)
//					.endSession();
			TaeSdkUtil.unbindAccount(SettingActivity.this, TaeSdkUtil.ACCOUNTPREFIX+mUser.getUid());
			User mGuestUser = UserHelper.getGuestUser(SettingActivity.this);
			if(mGuestUser!=null)TaeSdkUtil.bindAccount(SettingActivity.this, mGuestUser.getUid());
			RegisterPushHelper.logout(SettingActivity.this);
			UserHelper.clearAttentionUsers();
		}catch (Exception e)
		{
			e.printStackTrace();
		}
	}
	
	
	
	
	
	private void deleteUserData()
	{
		try
		{
			if(getExternalCacheDir()!=null)
					
					FileHelper.recursionDeleteFile(new File(FileHelper
							.getDataPath()));
					FileHelper.recursionDeleteFile(new File(FileHelper
							.getSmallImgPath()));
					FileHelper.recursionDeleteFile(new File(FileHelper
							.getTempPath()));
					DBHelper.getInstance(SettingActivity.this).deleteAllData();
					resetLightDBData();
			
					Intent lIntent = new Intent(SettingActivity.this,
							BaoBaoWDService.class);
					stopService(lIntent);
					UserHelper.logoutUser();
					BroadCastHelper.sendRefreshMainBroadcast(
							SettingActivity.this,
							GobalConstants.RefreshType.LOGOUT);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	private void resetLightDBData()
	{
		LightDBHelper.setXiaoMiUser(SettingActivity.this, "");
		LightDBHelper.setShortcut(SettingActivity.this, false);
		LightDBHelper.setPushShare(SettingActivity.this, false);
		LightDBHelper.setShareCount(SettingActivity.this, 0);
		LightDBHelper.setShareTime(
				DateHelper.getTextByDate(new Date(), DateHelper.YYYY_MM_DD),
				SettingActivity.this);
		LightDBHelper.setCheckInTime("", SettingActivity.this);
		LightDBHelper.setShareArticleTime("", SettingActivity.this);
		LightDBHelper.setReceiveAddress("", SettingActivity.this);
		LightDBHelper.setCheckVersionTime("", SettingActivity.this);
		LightDBHelper.setIsDownloadApk(SettingActivity.this, false);
		
//		LightDBHelper.setInviteUserListUrl(SettingActivity.this, "");
//		LightDBHelper.setProfileAppUrl(SettingActivity.this, "");
		
		
		LightDBHelper.setFanliCount(SettingActivity.this, 0);
		LightDBHelper.setCashCount(SettingActivity.this, 0);
		
		LightDBHelper.setAskDetailPrefix("", SettingActivity.this);
		LightDBHelper.setTopicDetailPrefix("", SettingActivity.this);
		LightDBHelper.setOnlineUserCount(SettingActivity.this, 0);
		LightDBHelper.setOnlineUserCountTime("", SettingActivity.this);
		LightDBHelper.setOnlineUserCountRatio(SettingActivity.this, 0);
		//切换账户后恢复 UID和淘宝ID没有绑定的状态
		
		
		
		LightDBHelper.setIsBind(SettingActivity.this, false);
		LightDBHelper.setBindPhone("",SettingActivity.this);
		LightDBHelper.setBindPay("",SettingActivity.this);
		
		
		
	}

	class CheckVersionThread extends Thread
	{

		private Handler handler;
		private VersionData lVersionData;
		private int versionCode;

		public CheckVersionThread()
		{
			handler = new Handler();
		}

		@Override
		public void run()
		{
			try
			{
				if (SettingActivity.this != null)
				{
					lVersionData = new CheckVersion(SettingActivity.this)
							.connect();
					versionCode = getPackageManager().getPackageInfo(
							getPackageName(), 0).versionCode;
				}
			}
			catch (Exception e)
			{
				LogFile.SaveExceptionLog(e);
			}
			handler.post(new Runnable()
			{

				@Override
				public void run()
				{
					if (SettingActivity.this != null)
					{
						if (lVersionData != null
								&& versionCode < lVersionData.getPrivateVer())
						{
							if (NetworkStatus
									.getNetWorkStatus(SettingActivity.this) > 0
									&& !LightDBHelper
											.getIsDownloadApk(SettingActivity.this))
							{
//								final String url = GobalConstants.URL.DOWNLOADAPKURL
//										+ "?" + UUID.randomUUID().toString();
								final String url = lVersionData.getFilepath();
								final int lastPrivateVer = lVersionData.getLastPrivateVer();
								
								ToastHelper.showUpdataVersionDialog(
										SettingActivity.this,
										lVersionData.getNote(),
										new DialogCallBack()
										{

											@Override
											public void clickSure()
											{
												LightDBHelper.setIsDownloadApk(
														SettingActivity.this,
														true);
												NotifyHandler lNotifyHandler = new NotifyHandler(
														SettingActivity.this);
												FileHelper.sendMsg(0,
														lNotifyHandler);
												new DownloadThread(
														SettingActivity.this,
														lNotifyHandler, url)
														.start();
												new NotifyThread(
														SettingActivity.this,
														lNotifyHandler).start();
											}

											@Override
											public void clickCancel()
											{
											}
										},lastPrivateVer);
							}
						}
						else
						{
							Toast.makeText(SettingActivity.this,
									getString(R.string.versionnewest),
									Toast.LENGTH_SHORT).show();
						}
					}
				}
			});
		}
	}

	@Override
	public void onResume()
	{
		super.onResume();
		MobclickAgent.onPageStart("设置");
	}

	@Override
	public void onPause()
	{
		super.onPause();
		MobclickAgent.onPageEnd("设置");
	}

	@Override
	protected void onDestroy()
	{
		super.onDestroy();
	}
}
