package dianyun.zqcheap.activity;

import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.TextView;

import com.ta.utdid2.android.utils.StringUtils;
import com.umeng.analytics.MobclickAgent;

import dianyun.baobaowd.util.GobalConstants;
import dianyun.zqcheap.R;
import dianyun.zqcheap.activitybase.BaseActivity;
import dianyun.zqcheap.application.BaoBaoWDApplication;

public class WebHtmlActivity extends BaseActivity 

{

	private TextView mTopTv;
	private WebView mWebView;
	private String url;
	private Button mActivityBackBt;
	private String mTitle;

	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		getTemplate().doInActivity(this, R.layout.webhtmlactivity);
	}

	@Override
	public void findView()
	{
		super.findView();
		mTopTv = (TextView) findViewById(R.id.top_tv);
		mWebView = (WebView) findViewById(R.id.webview);
		mWebView.requestFocus();
		mWebView.setScrollBarStyle(View.SCROLLBARS_OUTSIDE_OVERLAY);
		WebSettings setting = mWebView.getSettings();
		setting.setUserAgentString(setting.getUserAgentString() + " yoyo360/"
				+ BaoBaoWDApplication.versionName + " (yoyo360-android)");
		setting.setJavaScriptEnabled(true);
		setting.setSupportZoom(true);
		setting.setBuiltInZoomControls(true);
		
		mWebView.setWebViewClient(new WebViewClient()
		{

			@Override
			public boolean shouldOverrideUrlLoading(WebView view, String url)
			{
//				System.out.println("shouldOverrideUrlLoading url=====" + url);
				return super.shouldOverrideUrlLoading(view, url);
			}
		});
		
		
		mWebView.clearCache(true);
		mActivityBackBt = (Button) findViewById(R.id.activityback_bt);
		mActivityBackBt.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View v)
			{
				back();
			}
		});
		if (getIntent() != null)
		{
			url = getIntent().getStringExtra(GobalConstants.Data.URL);
			mTitle = getIntent().getStringExtra(GobalConstants.Data.TITLE);
			mTopTv.setText(StringUtils.isEmpty(mTitle)?getString(R.string.detail):mTitle);
			mWebView.loadUrl(url);
		}
	}

	
	
	
	
	
	
	private void back(){
		
		if (mWebView.canGoBack())
		{
			mWebView.goBack();
		}
		else
		{
			finish();
		}
	}
	
	@Override
	public void initListener()
	{
		super.initListener();
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event)
	{
		if ((keyCode == KeyEvent.KEYCODE_BACK))
		{
			back();
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}

	@Override
	public void onResume()
	{
		super.onResume();
		MobclickAgent.onPageStart("金币规则");
	}

	@Override
	public void onPause()
	{
		super.onPause();
		MobclickAgent.onPageEnd("金币规则");
	}

	

	
}
