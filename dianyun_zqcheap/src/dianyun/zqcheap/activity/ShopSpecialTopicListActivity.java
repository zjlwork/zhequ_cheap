
package dianyun.zqcheap.activity;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;
import dianyun.baobaowd.adapter.SpecialTopicAdapter;
import dianyun.baobaowd.defineview.xlistview.CustomListView;
import dianyun.baobaowd.defineview.xlistview.CustomListView.OnLoadMoreListener;
import dianyun.baobaowd.entity.CateItem;
import dianyun.baobaowd.entity.Constants;
import dianyun.baobaowd.entity.LocalMenu;
import dianyun.baobaowd.help.ShopHttpHelper;
import dianyun.baobaowd.util.ToastHelper;
import dianyun.baobaowd.util.Utils;
import dianyun.zqcheap.R;

public class ShopSpecialTopicListActivity extends Activity implements
		OnClickListener,
		dianyun.baobaowd.defineview.xlistview.CustomListView.OnRefreshListener,
		OnLoadMoreListener
{

	// private PullToRefreshScrollView mScrollView;
	private CustomListView mGridListView;
	// private SpecialTopicView mSpecialView;
	private ImageView mToTopView;
	private TextView mTitleTV;
	private String mTitleString = "";
	private Long mParentID = 0L;
	private SpecialTopicAdapter mAdapter;
	private View mListViewHeader;
	private List<List<CateItem>> mGoodsList = new ArrayList<List<CateItem>>();
	private int mCurrentPageIndex = 1;
	private int mPageSize = 20;
	private View mCurrentActivityRootView;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		setContentView(R.layout.goods_list_lay);
		initView();
		initData();
		super.onCreate(savedInstanceState);
	}

	private void initView()
	{
		// mScrollView = (PullToRefreshScrollView)
		// findViewById(R.id.goodslist_scrollview);
		mCurrentActivityRootView = findViewById(R.id.root_view);
		mGridListView = (CustomListView) findViewById(R.id.goodslist_lv);
		mGridListView.setCanLoadMore(false);
		mGridListView
				.setHeadViewBg(getResources().getColor(R.color.allbgcolor));
		// mListViewHeader=LayoutInflater.from(this).inflate(R.layout.shop_goods_list_header,
		// null);
		//
		// mSpecialView = (SpecialTopicView)mListViewHeader.
		// findViewById(R.id.specialtopic_lay);
		// mSpecialView.setVisibility(View.GONE);
		mToTopView = (ImageView) findViewById(R.id.goodslist_to_top);
		mTitleTV = (TextView) findViewById(R.id.top_tv);
		mToTopView.setVisibility(View.GONE);
		mGridListView.setOnRefreshListener(this);
		mGridListView.setOnLoadListener(this);
	}

	private void initData()
	{
		Intent itn = getIntent();
		if (itn != null)
		{
			mTitleString = itn.getStringExtra(Constants.EXTRA_NAME);
			mParentID = itn.getLongExtra(Constants.OBJECT_EXTRA_NAME, 0L);
		}
		if (null != (mTitleString))
		{
			mTitleTV.setText(mTitleString);
		}
		if (!Utils.isNetAvailable(this))
		{
			ToastHelper.show(this,
					getResources().getString(R.string.net_is_unable));
			return;
		}
		ShopHttpHelper.getMoreChildData(this, mGoodsList.size() <= 0,
				mCurrentPageIndex, mPageSize, mParentID + "",
				new ShopHttpHelper.ShopDataCallback()
				{

					@Override
					public void getMenu(List<LocalMenu> result)
					{
						// TODO Auto-generated method stub
					}

					@Override
					public void getChildsData(List<CateItem> result)
					{
						doClassList(result);
						if (mAdapter == null)
						{
							mAdapter = new SpecialTopicAdapter(
									ShopSpecialTopicListActivity.this,
									mGoodsList);
							mAdapter.setCurrentActivityRootView(mCurrentActivityRootView);
							mGridListView.setAdapter(mAdapter);
						}
						else
						{
							mAdapter.setDataSource(mGoodsList);
						}
						mGridListView.onRefreshComplete();
					}
				});
	}

	private void doClassList(List<CateItem> result)
	{
		if (result != null && result.size() > 0)
		{
			// 筛选出商品
			mGoodsList.clear();
			for (CateItem item : result)
			{
				if (item.xType != null && item.xType == 4)
				{
					List<CateItem> tempItem = new ArrayList<CateItem>(2);
					tempItem.add(0, item);
					tempItem.add(1, null);
					mGoodsList.add(tempItem);
				}
			}
			if (result.size() < mPageSize)
			{
				mGridListView.setCanLoadMore2(false);
			}
			else
			{
				mGridListView.setCanLoadMore2(true);
				mCurrentPageIndex++;
			}
		}
		else
		{
			mGridListView.setCanLoadMore(false);
			// ToastHelper.show(this,
			// getResources().getString(R.string.no_moredata));
		}
	}

	private void doAddClassList(List<CateItem> result)
	{
		if (result != null && result.size() > 0)
		{
			mCurrentPageIndex++;
			// 筛选出商品
			for (CateItem item : result)
			{
				if (item.xType != null && item.xType == 4)
				{
					List<CateItem> tempItem = new ArrayList<CateItem>(2);
					tempItem.add(0, item);
					tempItem.add(1, null);
					mGoodsList.add(tempItem);
				}
			}
			if (result.size() < mPageSize)
			{
				mGridListView.setCanLoadMore2(false);
			}
			else
			{
				mGridListView.setCanLoadMore2(true);
			}
		}
		else
		{
			mGridListView.setCanLoadMore(false);
			// ToastHelper.show(this,
			// getResources().getString(R.string.no_moredata));
		}
	}

	@Override
	public void onClick(View v)
	{
		switch (v.getId())
		{
		case R.id.activityback_bt:
			// back
			finish();
			break;
		case R.id.goodslist_to_top:
			// mGridListView.scrollTo(0, 0);
			break;
		}
	}

	@Override
	public void onLoadMore()
	{
		if (!Utils.isNetAvailable(this))
		{
			ToastHelper.show(this,
					getResources().getString(R.string.net_is_unable));
			mGridListView.onLoadMoreComplete();
			return;
		}
		ShopHttpHelper.getMoreChildData(this, mGoodsList.size() <= 0,
				mCurrentPageIndex, mPageSize, mParentID + "",
				new ShopHttpHelper.ShopDataCallback()
				{

					@Override
					public void getMenu(List<LocalMenu> result)
					{
						// TODO Auto-generated method stub
					}

					@Override
					public void getChildsData(List<CateItem> result)
					{
						doAddClassList(result);
						if (mAdapter == null)
						{
							mAdapter = new SpecialTopicAdapter(
									ShopSpecialTopicListActivity.this,
									mGoodsList);
							mGridListView.setAdapter(mAdapter);
						}
						else
						{
							mAdapter.setDataSource(mGoodsList);
						}
						mGridListView.onLoadMoreComplete();
					}
				});
	}

	@Override
	public void onRefresh()
	{
		if (!Utils.isNetAvailable(this))
		{
			ToastHelper.show(this,
					getResources().getString(R.string.net_is_unable));
			mGridListView.onRefreshComplete();
			return;
		}
		mCurrentPageIndex = 1;
		mGridListView.setCanLoadMore2(false);
		ShopHttpHelper.getMoreChildData(this, mGoodsList.size() <= 0,
				mCurrentPageIndex, mPageSize, mParentID + "",
				new ShopHttpHelper.ShopDataCallback()
				{

					@Override
					public void getMenu(List<LocalMenu> result)
					{
						// TODO Auto-generated method stub
					}

					@Override
					public void getChildsData(List<CateItem> result)
					{
						doClassList(result);
						if (mAdapter == null)
						{
							mAdapter = new SpecialTopicAdapter(
									ShopSpecialTopicListActivity.this,
									mGoodsList);
							mGridListView.setAdapter(mAdapter);
						}
						else
						{
							mAdapter.setDataSource(mGoodsList);
						}
						mGridListView.onRefreshComplete();
					}
				});
	}
}
