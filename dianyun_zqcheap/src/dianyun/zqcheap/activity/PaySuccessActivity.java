package dianyun.zqcheap.activity;

import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import dianyun.zqcheap.R;
import dianyun.zqcheap.activitybase.BaseActivity;

public class PaySuccessActivity extends BaseActivity 
{

	
	private Button mActivityBackBt;
	private Button mSureBt;
	private boolean tixian;
	private TextView title_tv;
	private TextView content_tv;

	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		getTemplate().doInActivity(this, R.layout.paysuccessactivity);
	}

	@Override
	public void findView()
	{
		super.findView();
		
		title_tv = (TextView) findViewById(R.id.title_tv);
		content_tv = (TextView) findViewById(R.id.content_tv);
		mActivityBackBt = (Button) findViewById(R.id.activityback_bt);
		mActivityBackBt.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View v)
			{
				finish();
			}
		});
		mSureBt = (Button) findViewById(R.id.sure_bt);
		mSureBt.setOnClickListener(new OnClickListener()
		{
			
			@Override
			public void onClick(View v)
			{
				if(!tixian)
					startActivity(OrderRecordActivity.class);
				else
					startActivity(RecordActivity.class);
				finish();
			}
		});
		
		tixian = getIntent().getBooleanExtra("istixian", false);
		
		if(tixian){
			title_tv.setText(getString(R.string.paystatus_moneysuccess));
			content_tv.setText(getString(R.string.tixian_content));
			mSureBt.setText(getString(R.string.exchangerecord));
		}
		
		
	}

	@Override
	public void initListener()
	{
		super.initListener();
	}

	
}
