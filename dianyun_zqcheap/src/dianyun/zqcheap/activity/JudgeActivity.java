package dianyun.zqcheap.activity;

import java.io.File;
import java.util.Date;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.download.ImageDownloader.Scheme;
import com.zhequzheq.DevInit;

import dianyun.baobaowd.data.Attachment;
import dianyun.baobaowd.data.User;
import dianyun.baobaowd.data.UserStatus;
import dianyun.baobaowd.data.ViewPicture;
import dianyun.baobaowd.data.WelcomePicture;
import dianyun.baobaowd.db.LightDBHelper;
import dianyun.baobaowd.dto.ResultDTO;
import dianyun.baobaowd.gson.GsonHelper;
import dianyun.baobaowd.help.FileHelper;
import dianyun.baobaowd.serverinterface.GetWelcomePicture;
import dianyun.baobaowd.util.DateHelper;
import dianyun.baobaowd.util.GobalConstants;
import dianyun.baobaowd.util.NetworkStatus;
import dianyun.baobaowd.util.ShortCutHelper;
import dianyun.baobaowd.util.TaeSdkUtil;
import dianyun.baobaowd.util.ThirdPartHelper;
import dianyun.baobaowd.util.ToastHelper;
import dianyun.baobaowd.util.UserHelper;
import dianyun.baobaowd.util.UserStatusHelper;
import dianyun.baobaowd.util.WelcomePictureHelper;
import dianyun.zqcheap.R;
import dianyun.zqcheap.activitybase.BaseActivity;
import dianyun.zqcheap.application.BaoBaoWDApplication;

public class JudgeActivity extends BaseActivity {

	private User mGuestUser;
	private UserStatus mUserStatus;
	private User mUser;
	private ImageView mStartIv;
	private DisplayImageOptions mOptions;
	private boolean jumpAlready;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);// 去掉标题栏
		this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);// 去掉信息栏
		getTemplate().doInActivity(this, R.layout.judgeactivity);
		
		long begin = System.currentTimeMillis();
		System.out.println("onCreate= begin=="+begin);
		mStartIv = (ImageView) findViewById(R.id.start_iv);
		long getBegin = System.currentTimeMillis();
//		System.out.println("onCreate=get begin=="+getBegin+"       ");
		mGuestUser = UserHelper.getGuestUser(JudgeActivity.this);
		mUser = UserHelper.getMeUser(JudgeActivity.this);
		mUserStatus = UserStatusHelper.getUserStatus(JudgeActivity.this);
		long getEnd = System.currentTimeMillis();
//		System.out.println("onCreate=get end=="+getEnd+"       "+(getEnd-getBegin));

		if (!LightDBHelper.getShortcut(JudgeActivity.this)) {
			LightDBHelper.setShortcut(JudgeActivity.this, true);
			ShortCutHelper.addShortcut(JudgeActivity.this);
		}
		
		if (NetworkStatus.getNetWorkStatus(JudgeActivity.this) > 0)
			new GetWelcomePictureThread(GobalConstants.GUESTUSERUID,
					GobalConstants.GUESTUSERTOKEN, getScreenType()).start();
		setStartIv();
//		long initBegin = System.currentTimeMillis();
//		System.out.println("onCreate=init begin=="+initBegin);
		  DevInit.initGoogleContext(JudgeActivity.this, ThirdPartHelper.DIANLE_APP_ID,BaoBaoWDApplication.channel);
//		  long initCenter = System.currentTimeMillis();
//		  System.out.println("onCreate=init center=="+initCenter+"  "+(initCenter-initBegin));
//		  CommonManager.getInstance(JudgeActivity.this).init(ThirdPartHelper.YOUMI_APP_ID, ThirdPartHelper.YOUMI_APP_SECRET);
//		  OWManager.getInstance(JudgeActivity.this).initOfferWall();
//		  long initEnd = System.currentTimeMillis();
//		  System.out.println("onCreate=init end=="+initEnd+"   "+(initEnd-initBegin));
		if (mGuestUser == null) {
//			Intent lIntent = new Intent(JudgeActivity.this,
//					WelComeActivity.class);
//			startActivity(lIntent);
//			finish();
//			System.out.println("onCreate=set begin=="+System.currentTimeMillis());
			User mGuestUser = new User(GobalConstants.GUESTUSERUID,
					getString(R.string.guestnickname),
					GobalConstants.GUESTUSERTOKEN);
			mGuestUser.setIsGuest(GobalConstants.AllStatusYesOrNo.YES);
			UserHelper.setUser(mGuestUser);
			TaeSdkUtil.bindAccount(JudgeActivity.this, mGuestUser.getUid());
//			System.out.println("onCreate=set begin=="+System.currentTimeMillis());
			
			new GoneThread2().start();
			
			
		} else
			new GoneThread().start();

		System.out.println("onCreate= end=="+System.currentTimeMillis());
	}

	private void setStartIv() {
		mOptions = new DisplayImageOptions.Builder()
		.cacheInMemory(true).cacheOnDisk(true).considerExifParams(true)
		.imageScaleType(ImageScaleType.IN_SAMPLE_POWER_OF_2)
		.bitmapConfig(Bitmap.Config.RGB_565)
//		.showImageOnLoading(R.drawable.loginbg)
		.build();
		
		
//		String localPath = LightDBHelper
//				.getStartIvLocalPath(JudgeActivity.this);
		final WelcomePicture lWelcomePicture = WelcomePictureHelper.getWelcomePicture(BaoBaoWDApplication.context);
		if(lWelcomePicture!=null){
			if (!TextUtils.isEmpty(lWelcomePicture.getLocalPath())) {
				File lFile = new File(lWelcomePicture.getLocalPath());
				if (lFile.isFile()) {
					ImageLoader.getInstance().displayImage(
							Scheme.FILE.wrap(lWelcomePicture.getLocalPath()), mStartIv,mOptions);
				}
			}
			mStartIv.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					if (lWelcomePicture.getJumpType() == ViewPicture.JUMETYPE_WEBINSIDE){
						jumpAlready = true;
						Intent lIntent = new Intent(JudgeActivity.this,
								HtmlActivity.class);
						lIntent.putExtra(GobalConstants.Data.URL,lWelcomePicture.getJumpValue());
						lIntent.putExtra(GobalConstants.Data.FROM,"JudgeActivity");
						startActivity(lIntent);
						finish();
					}else if(lWelcomePicture.getJumpType() == ViewPicture.JUMETYPE_WEBOUTSIDE){
					  	Intent intent = new Intent();        
			            intent.setAction("android.intent.action.VIEW");    
			            Uri content_url = Uri.parse(lWelcomePicture.getJumpValue());   
			            intent.setData(content_url);  
			            startActivity(intent);
			            jumpAlready = true;
			            finish();
					}
				}
			});
		}
	}

	class GoneThread extends Thread {

		private Handler handler;

		public GoneThread() {
			handler = new Handler();
		}

		@Override
		public void run() {
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			handler.post(new Runnable() {

				@Override
				public void run() {
					if(jumpAlready)return;
					
					if (mUser != null && mUserStatus == null) {
						UserStatusHelper.saveUserStatus(JudgeActivity.this,
								mUser);
					} else if (mGuestUser != null && mUserStatus == null) {
						UserStatusHelper.saveUserStatus(JudgeActivity.this,
								mGuestUser);
					}
					startActivity(MainActivity.class);
					finish();
				}
			});
		}
	}
	class GoneThread2 extends Thread {
		
		private Handler handler;
		
		public GoneThread2() {
			handler = new Handler();
		}
		
		@Override
		public void run() {
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			handler.post(new Runnable() {
				
				@Override
				public void run() {
					if(jumpAlready)return;
					
					startActivity(MainActivity.class);
					finish();
				}
			});
		}
	}

	private byte getScreenType() {
		int width = ToastHelper.getScreenWidth(JudgeActivity.this);

		if (width >= 1080) {
			return 7;
		} else if (width >= 720) {
			return 5;
		} else if (width >= 640) {
			return 3;
		} else if (width >= 480) {
			return 1;
		}
		return 1;
	}

	class GetWelcomePictureThread extends Thread {

		private Handler handler;
		private ResultDTO resultDTO;
		byte screenType;
		long uid;
		String token;

		public GetWelcomePictureThread(long uid, String token, byte screenType) {
			handler = new Handler();
			this.uid = uid;
			this.token = token;
			this.screenType = screenType;

		}

		@Override
		public void run() {

			resultDTO = new GetWelcomePicture(uid, token, screenType)
					.getConnect();
			try {
				if (resultDTO != null && resultDTO.getCode().equals("0")) {
					WelcomePicture lWelcomePicture = GsonHelper.gsonToObj(
							resultDTO.getResult(), WelcomePicture.class);

					if (lWelcomePicture != null
							&& lWelcomePicture.getAttachmentList() != null
							&& lWelcomePicture.getAttachmentList().size() > 0) {

						Attachment attachment = lWelcomePicture
								.getAttachmentList().get(0);
//						String url = LightDBHelper
//								.getStartIvUrl(JudgeActivity.this);
						
						long nowTimeLong = new Date().getTime();
						long endTime = 0 ; 
						Date endDate = 	DateHelper.getDateByPattern(lWelcomePicture.getEndDate(), DateHelper.YYYY_MM_DD_HH_MM_SS) ;
						if(endDate!=null)endTime = endDate.getTime();
						if(nowTimeLong<endTime){
							WelcomePicture localWelcomePicture = WelcomePictureHelper.getWelcomePicture(BaoBaoWDApplication.context);
							String localPath =localWelcomePicture==null?"":localWelcomePicture.getLocalPath();
							if(localWelcomePicture==null||(localWelcomePicture!=null&&!attachment.getFileUrl().equals(localWelcomePicture.getFileUrl()))){
								 localPath = FileHelper
										.downloadWelcomePic(attachment.getFileUrl());
							}
							WelcomePictureHelper.deleteWelcomePicture(BaoBaoWDApplication.context);
							WelcomePictureHelper.addWelcomePicture(BaoBaoWDApplication.context, 
									new WelcomePicture(lWelcomePicture.getSeqId(), attachment.getFileUrl(), localPath,
											lWelcomePicture.getJumpType(), lWelcomePicture.getJumpValue()));
						}else{
							WelcomePictureHelper.deleteWelcomePicture(BaoBaoWDApplication.context);
							
						}
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}

			handler.post(new Runnable() {

				@Override
				public void run() {
					if (resultDTO != null && resultDTO.getCode().equals("0")) {
						setStartIv();
					}
				}
			});
		}
	}

}
