package dianyun.zqcheap.activity;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Interpolator;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Scroller;

import com.nineoldandroids.animation.Animator;
import com.nineoldandroids.animation.Animator.AnimatorListener;
import com.nineoldandroids.animation.AnimatorSet;
import com.nineoldandroids.animation.ObjectAnimator;

import dianyun.baobaowd.data.User;
import dianyun.baobaowd.entity.Constants;
import dianyun.baobaowd.util.ActivityManager;
import dianyun.baobaowd.util.GobalConstants;
import dianyun.baobaowd.util.TaeSdkUtil;
import dianyun.baobaowd.util.UserHelper;
import dianyun.zqcheap.R;
import dianyun.zqcheap.activitybase.BaseActivity;

public class WelComeActivity extends BaseActivity implements OnClickListener,
		OnPageChangeListener
{

	public static boolean mLoginSuccess = false;
	private View mWelcome_LoginLay = null;
	private View mWelcome_GuideLay = null;

//	private View mWelcome_Guide_Lay1 = null;
//	private View mWelcome_Guide_Lay2 = null;
//	private View mWelcome_Guide_Lay3 = null;
//	private View mWelcome_Guide_Lay4 = null;
//	private TextView mWelcome_BigTv1 = null;
//	private TextView mWelcome_BigTv2 = null;
//	private TextView mWelcome_BigTv3 = null;

	private List<ImageView> mDotImageList = new ArrayList<ImageView>();
//	private int[] mIsLoadPageList = new int[] { 0, 0, 0, 0 };
	private LinearLayout mDotLayout = null;
	private ViewPager mWelcomeVPager = null;
	private ImagePagerAdapter mAdapter = null;
	private int[] mImageRefrenceList = new int[] { R.drawable.dot3,
			R.drawable.dot3, R.drawable.dot3 };
	private Animator animator_appreance;
	private Animator animator_dismiss;
//	private String mFontPath = "IMPACT.TTF";
//	private Typeface mFontTP;

//	protected void onCreate(Bundle savedInstanceState)
//	{
//		setContentView(R.layout.welcome_layout);
//		initView();
//		ActivityManager.pushActivity(this);
//
//		super.onCreate(savedInstanceState);
//	}
//	
	
	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		getTemplate().doInActivity(this, R.layout.welcome_layout);
		initView();
		ActivityManager.pushActivity(this);
	}
	
	

	private void initView()
	{
//		mFontTP = Typeface.createFromAsset(getAssets(), mFontPath);
		mWelcome_GuideLay = findViewById(R.id.welcome_guid_lay);
//		mWelcome_LoginLay = findViewById(R.id.welcome_login_lay);
		mDotLayout = (LinearLayout) findViewById(R.id.dot_lay);
		mWelcomeVPager = (ViewPager) findViewById(R.id.welcome_viewpager);
		mWelcomeVPager.setOffscreenPageLimit(1);
		controlViewPagerSpeed();
		mWelcomeVPager.setOnPageChangeListener(this);
		for (int i = 0; i < mDotLayout.getChildCount(); i++)
		{
			mDotImageList.add((ImageView) mDotLayout.getChildAt(i));
		}
		// mWelcomeLoginBT=findViewById(R.id.welcome_login);
		// setonclickListener
		// mWelcomeLoginBT.setOnClickListener(this);
		mAdapter = new ImagePagerAdapter(this, mImageRefrenceList);
		mWelcomeVPager.setAdapter(mAdapter);
		mWelcomeVPager.setCurrentItem(0);
	}

	private void showLoginLay()
	{
		animator_appreance = ObjectAnimator.ofFloat(mWelcome_LoginLay,
				"translationY",
				-getResources().getDisplayMetrics().heightPixels, 0);
		// animator_appreance.setInterpolator(new DecelerateInterpolator(5f));
		animator_appreance.setDuration(1000);
		animator_appreance.addListener(new AnimatorListener()
		{

			@Override
			public void onAnimationStart(Animator arg0)
			{
				mWelcome_LoginLay.setVisibility(View.VISIBLE);
			}

			@Override
			public void onAnimationRepeat(Animator arg0)
			{
				// TODO Auto-generated method stub
			}

			@Override
			public void onAnimationEnd(Animator arg0)
			{
				// TODO Auto-generated method stub
			}

			@Override
			public void onAnimationCancel(Animator arg0)
			{
			}
		});
		Animator alpha = ObjectAnimator.ofFloat(mWelcome_GuideLay, "alpha", 1f,
				0f);
		alpha.setStartDelay(300);
		// alpha.setInterpolator(new AccelerateInterpolator(5f));
		alpha.addListener(new AnimatorListener()
		{

			@Override
			public void onAnimationStart(Animator arg0)
			{
			}

			@Override
			public void onAnimationRepeat(Animator arg0)
			{
				// TODO Auto-generated method stub
			}

			@Override
			public void onAnimationEnd(Animator arg0)
			{
				mWelcome_GuideLay.setVisibility(View.GONE);
			}

			@Override
			public void onAnimationCancel(Animator arg0)
			{
				// TODO Auto-generated method stub
			}
		});
		alpha.setDuration(1000);
		AnimatorSet mAnset = new AnimatorSet();
		mAnset.playTogether(alpha, animator_appreance);
		mAnset.start();
	}

	private void hideLoginLay()
	{
		animator_dismiss = ObjectAnimator.ofFloat(mWelcome_LoginLay,
				"translationY", 0,
				-getResources().getDisplayMetrics().heightPixels);
		animator_dismiss.setDuration(500);
		// animator_dismiss.setInterpolator(new DecelerateInterpolator(0f));
		animator_dismiss.addListener(new AnimatorListener()
		{

			@Override
			public void onAnimationStart(Animator arg0)
			{
			}

			@Override
			public void onAnimationRepeat(Animator arg0)
			{
				// TODO Auto-generated method stub
			}

			@Override
			public void onAnimationEnd(Animator arg0)
			{
				mWelcome_LoginLay.setVisibility(View.VISIBLE);
			}

			@Override
			public void onAnimationCancel(Animator arg0)
			{
				// TODO Auto-generated method stub
			}
		});
		Animator alpha = ObjectAnimator.ofFloat(mWelcome_GuideLay, "alpha", 0f,
				1f);
		// alpha.setInterpolator(new AccelerateInterpolator(4f));
		alpha.setStartDelay(100);
		alpha.addListener(new AnimatorListener()
		{

			@Override
			public void onAnimationStart(Animator arg0)
			{
				mWelcome_GuideLay.setVisibility(View.VISIBLE);
			}

			@Override
			public void onAnimationRepeat(Animator arg0)
			{
				// TODO Auto-generated method stub
			}

			@Override
			public void onAnimationEnd(Animator arg0)
			{
			}

			@Override
			public void onAnimationCancel(Animator arg0)
			{
				// TODO Auto-generated method stub
			}
		});
		alpha.setDuration(500);
		AnimatorSet mAnset = new AnimatorSet();
		mAnset.playTogether(alpha, animator_dismiss);
		mAnset.start();
	}

	private void controlViewPagerSpeed()
	{
		try
		{
			Field mField;
			mField = ViewPager.class.getDeclaredField("mScroller");
			mField.setAccessible(true);
			FixedSpeedScroller mScroller = new FixedSpeedScroller(this,
					new AccelerateInterpolator());
			mScroller.setmDuration(200); // 2000ms
			mField.set(mWelcomeVPager, mScroller);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	public class FixedSpeedScroller extends Scroller
	{

		private int mDuration = 100; // default time is 1500ms

		public FixedSpeedScroller(Context context)
		{
			super(context);
		}

		public FixedSpeedScroller(Context context, Interpolator interpolator)
		{
			super(context, interpolator);
		}

		@Override
		public void startScroll(int startX, int startY, int dx, int dy,
				int duration)
		{
			// Ignore received duration, use fixed one instead
			super.startScroll(startX, startY, dx, dy, mDuration);
		}

		@Override
		public void startScroll(int startX, int startY, int dx, int dy)
		{
			// Ignore received duration, use fixed one instead
			super.startScroll(startX, startY, dx, dy, mDuration);
		}

		/**
		 * set animation time
		 * 
		 * @param time
		 */
		public void setmDuration(int time)
		{
			mDuration = time;
		}

		/**
		 * get current animation time
		 * 
		 * @return
		 */
		public int getmDuration()
		{
			return mDuration;
		}
	}

	private class ImagePagerAdapter extends PagerAdapter
	{

		private Context mContext = null;
		private int[] mDrawableReferList;

		public ImagePagerAdapter(Context context, int[] drawableCollection)
		{
			mContext = context;
			mDrawableReferList = drawableCollection;
		}

		@Override
		public int getCount()
		{
			int res = 0;
			if (mDrawableReferList != null)
			{
				res = mDrawableReferList.length;
			}
			return res;
		}

		@Override
		public void destroyItem(View collection, int position, Object view)
		{
			((ViewPager) collection).removeView((View) view);
		}

		@Override
		public boolean isViewFromObject(View view, Object object)
		{
			return view == (object);
		}

		@Override
		public Object instantiateItem(View collection, final int position)
		{
			View lView = null;
			if (position == 0)
			{
				lView = View.inflate(mContext, R.layout.welcome_guide1_layout,
						null);
//				mWelcome_BigTv1 = (TextView) lView
//						.findViewById(R.id.welcome_guide_big_textview1);
//				mWelcome_BigTv1.setTypeface(mFontTP);
//				mWelcome_Guide_Lay1 = lView
//						.findViewById(R.id.welcome_guid1_lay);
			}
			else if (position == 1)
			{
				lView = View.inflate(mContext, R.layout.welcome_guide2_layout,
						null);
//				mWelcome_Guide_Lay2 = lView
//						.findViewById(R.id.welcome_guid2_lay);
//				mWelcome_BigTv2 = (TextView) lView
//						.findViewById(R.id.welcome_guide_big_textview2);
//				mWelcome_BigTv2.setTypeface(mFontTP);
			}
			else if (position == 2)
			{
				lView = View.inflate(mContext, R.layout.welcome_guide3_layout,
						null);
//				mWelcome_Guide_Lay3 = lView
//						.findViewById(R.id.welcome_guid3_lay);
//				mWelcome_BigTv3 = (TextView) lView
//						.findViewById(R.id.welcome_guide_big_textview3);
//				mWelcome_BigTv3.setTypeface(mFontTP);
				lView.setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View v) {
						handleGoDirectly();
						
					}
				});
			}
			/*else if (position == 3)
			{
				lView = View.inflate(mContext, R.layout.welcome_guide4_layout,
						null);
//				mWelcome_Guide_Lay4 = lView
//						.findViewById(R.id.welcome_guid4_lay);
				
				
				lView.setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View v) {
						handleGoDirectly();
						
					}
				});
				
			}*/
			
			
			
			
			
			
			((ViewPager) collection).addView(lView, 0);
			return lView;
		}
	}

	@Override
	public void onClick(View v)
	{
		switch (v.getId())
		{
//		case R.id.welcome_login:
//			handleGoLogin();
			// showLoginLay();
//			break;
//		case R.id.welcome_goindirectly:
//			handleGoDirectly();
//			break;
		case R.id.welcome_closeimage:
			hideLoginLay();
			break;
		}
	}

	@Override
	public void onPageScrollStateChanged(int arg0)
	{
		// TODO Auto-generated method stub
	}

	@Override
	public void onPageScrolled(int arg0, float arg1, int arg2)
	{
		// TODO Auto-generated method stub
	}

	@Override
	public void onPageSelected(int index)
	{
		if (index < mDotImageList.size())
		{
			for (int i = 0; i < mDotImageList.size(); i++)
			{
				if (index == i)
				{
					mDotImageList.get(i).setImageResource(
							R.drawable.circle_selected);
				}
				else
				{
					mDotImageList.get(i).setImageResource(
							R.drawable.circle_unselect);
				}
			}
		}
//		if (mIsLoadPageList[index] == 0)
//		{
//			View targetView = mWelcome_Guide_Lay1;
//			switch (index)
//			{
//			case 1:
//				targetView = mWelcome_Guide_Lay2;
//				break;
//			case 2:
//				targetView = mWelcome_Guide_Lay3;
//				break;
//			case 3:
//				targetView = mWelcome_Guide_Lay4;
//				break;
//			}
//			doAnmation(targetView);
//			mIsLoadPageList[index] = 1;
//		}
	}

//	private void doAnmation(View v)
//	{
//		AnimatorSet mAnset = new AnimatorSet();
//		mAnset.playTogether(ObjectAnimator.ofFloat(v, "translationX", 50, 0),
//				ObjectAnimator.ofFloat(v, "alpha", 0, 1f),
//				ObjectAnimator.ofFloat(v, "scaleX", 1f, 1f, 1f),
//				ObjectAnimator.ofFloat(v, "scaleY", 1f, 1f, 1f));
//		mAnset.setStartDelay(000);
//		mAnset.setDuration(800);
//		mAnset.start();
//	}

	private void handleGoDirectly()
	{
		User mGuestUser = new User(GobalConstants.GUESTUSERUID,
				getString(R.string.guestnickname),
				GobalConstants.GUESTUSERTOKEN);
		mGuestUser.setIsGuest(GobalConstants.AllStatusYesOrNo.YES);
		UserHelper.setUser(mGuestUser);
		TaeSdkUtil.bindAccount(WelComeActivity.this, mGuestUser.getUid());
		Intent lIntent = new Intent(this, MainActivity.class);
//		lIntent.putExtra(GobalConstants.Data.USER, mGuestUser);
		startActivity(lIntent);
		finish();
		overridePendingTransition(R.anim.push_bottom_in, R.anim.push_bottom_out);
	}



	private void handleGoLogin()
	{
		Intent lIntent = new Intent(this, LoginRegisterActivity.class);
		lIntent.putExtra(Constants.EXTRA_NAME, "welcome");
		startActivity(lIntent);
		overridePendingTransition(R.anim.push_bottom_in, R.anim.push_bottom_out);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data)
	{
//		if (requestCode == Constants.REQUEST_LOGIN)
//		{
//			if (mLoginSuccess)
//			{
//				Intent lIntent = new Intent(this, MainActivity.class);
//				overridePendingTransition(R.anim.push_right_in,
//						R.anim.push_right_out);
//				startActivity(lIntent);
//				finish();
//				mLoginSuccess = false;
//			}
//		}
		super.onActivityResult(requestCode, resultCode, data);
	}
}
