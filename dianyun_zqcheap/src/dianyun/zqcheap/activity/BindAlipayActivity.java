package dianyun.zqcheap.activity;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import dianyun.baobaowd.data.User;
import dianyun.baobaowd.db.LightDBHelper;
import dianyun.baobaowd.dto.ResultDTO;
import dianyun.baobaowd.serverinterface.BindPay;
import dianyun.baobaowd.util.DialogHelper;
import dianyun.baobaowd.util.GobalConstants;
import dianyun.baobaowd.util.NetworkStatus;
import dianyun.baobaowd.util.UserHelper;
import dianyun.zqcheap.R;
import dianyun.zqcheap.activitybase.BaseActivity;

public class BindAlipayActivity extends BaseActivity
{

//	Button mGetCodeBt;
	Button mBindBt;
	EditText mNameEt;
	EditText mAlipayEt;
//	EditText mCodeEt;
	private Button mActivityBackBt;
	VerifyTextWatcher mVerifyTextWatcher;
	User mUser;
//	private static final int BINDPAYSENDCODE = 1;
	private static final int BINDPAY = 2;
	private String mPhone;
	private String mCode="";
	private String mAccount;
	private String mAccountName;
	private Handler mHandler = new Handler()
	{

		@Override
		public void handleMessage(Message msg)
		{
			switch (msg.what)
			{
//			case BINDPAYSENDCODE:
//				DialogHelper.cancelProgressDialog(mProgressDialog);
//				
//				ResultDTO resultDTO = (ResultDTO) msg.obj;
//				if (resultDTO != null && resultDTO.getCode().equals("0"))
//				{
//					Toast.makeText(BindAlipayActivity.this,
//							getString(R.string.autocodewillcome),
//							Toast.LENGTH_SHORT).show();
////					new CountDownTimerThread().start();
//				}
//				else
//				{
//					Toast.makeText(BindAlipayActivity.this,
//							getString(R.string.getautocodefailed),
//							Toast.LENGTH_SHORT).show();
////					mGetCodeBt.setEnabled(true);
////					mGetCodeBt.setSelected(false);
//				}
//				break;
			case BINDPAY:
				DialogHelper.cancelProgressDialog(mProgressDialog);
				mBindBt.setEnabled(true);
				mBindBt.setSelected(false);
				ResultDTO resultDTO2 = (ResultDTO) msg.obj;
				if (resultDTO2 != null && resultDTO2.getCode().equals("0"))
				{
//					Toast.makeText(BindAlipayActivity.this,
//							getString(R.string.bindsuccess), Toast.LENGTH_SHORT)
//							.show();
					LightDBHelper.setBindPay(mAccount, BindAlipayActivity.this);
					startActivity(ExchangeMoneyActivity.class);
					finish();
				}
				else
				{
					Toast.makeText(BindAlipayActivity.this,
							getString(R.string.bindfailed), Toast.LENGTH_SHORT)
							.show();
				}
				break;
			}
			super.handleMessage(msg);
		}
	};

	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		getTemplate().doInActivity(this, R.layout.bindalipayactivity);
	}

	@Override
	public void initData()
	{
		super.initData();
		mUser = UserHelper.getUser();
		mActivityBackBt = (Button) findViewById(R.id.activityback_bt);
		mActivityBackBt.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View v)
			{
				finish();
			}
		});
		mNameEt = (EditText) findViewById(R.id.name_et);
		mAlipayEt = (EditText) findViewById(R.id.alipay_et);
//		mCodeEt = (EditText) findViewById(R.id.code_et);
//		mGetCodeBt = (Button) findViewById(R.id.getcode_bt);
		mBindBt = (Button) findViewById(R.id.bind_bt);
		mPhone = LightDBHelper.getBindPhone(BindAlipayActivity.this);
//		mGetCodeBt.setOnClickListener(new OnClickListener()
//		{
//
//			@Override
//			public void onClick(View v)
//			{
//				
//				bindAlipaySendCode(BindAlipayActivity.this);
//			}
//		});
		mBindBt.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View v)
			{
				// startActivity(ExchangeMoneyActivity.class);
				bindAlipay(BindAlipayActivity.this);
			}
		});
		mNameEt.requestFocus();
		mVerifyTextWatcher = new VerifyTextWatcher();
		mNameEt.addTextChangedListener(mVerifyTextWatcher);
		mAlipayEt.addTextChangedListener(mVerifyTextWatcher);
//		mCodeEt.addTextChangedListener(mVerifyTextWatcher);
		activateBt();
	}

	private class VerifyTextWatcher implements TextWatcher
	{

		@Override
		public void beforeTextChanged(CharSequence s, int start, int count,
				int after)
		{
			// TODO Auto-generated method stub
		}

		@Override
		public void onTextChanged(CharSequence s, int start, int before,
				int count)
		{
			// TODO Auto-generated method stub
		}

		@Override
		public void afterTextChanged(Editable s)
		{
			activateBt();
		}
	}

	private void activateBt()
	{
		if (!TextUtils.isEmpty(mNameEt.getText())
				&& !TextUtils.isEmpty(mAlipayEt.getText()))
		{
			mBindBt.setSelected(false);
			mBindBt.setEnabled(true);
		}
		else
		{
			mBindBt.setSelected(true);
			mBindBt.setEnabled(false);
		}
	}
	Dialog mProgressDialog;

//	private void bindAlipaySendCode(final Context context)
//	{
//		if (NetworkStatus.getNetWorkStatus(context) > 0)
//		{
//			mProgressDialog = DialogHelper.showProgressDialog(context,
//					getString(R.string.getautocodeing));
////			mGetCodeBt.setEnabled(false);
////			mGetCodeBt.setSelected(true);
//			new Thread()
//			{
//
//				public void run()
//				{
//					ResultDTO lResultDTO = new BindPaySendCode(context,
//							mUser.getUid(), mUser.getToken(), mPhone)
//							.postConnect();
//					Message msg = new Message();
//					msg.what = BINDPAYSENDCODE;
//					msg.obj = lResultDTO;
//					mHandler.sendMessage(msg);
//				}
//			}.start();
//		}
//		else
//		{
//			Toast.makeText(context, getString(R.string.no_network),
//					Toast.LENGTH_SHORT).show();
//		}
//	}

	private void bindAlipay(final Context context)
	{
		if (NetworkStatus.getNetWorkStatus(context) > 0)
		{
			mProgressDialog = DialogHelper.showProgressDialog(context,
					getString(R.string.binding));
			mBindBt.setEnabled(false);
			mBindBt.setSelected(true);
			new Thread()
			{

				public void run()
				{
//					mCode = mCodeEt.getText().toString().trim();
					mAccount = mAlipayEt.getText().toString().trim();
					mAccountName = mNameEt.getText().toString().trim();
					ResultDTO lResultDTO = new BindPay(context, mUser.getUid(),
							mUser.getToken(),
							GobalConstants.AccountType.ALIPAY, mAccount,
							mAccountName, mCode).postConnect();
					Message msg = new Message();
					msg.what = BINDPAY;
					msg.obj = lResultDTO;
					mHandler.sendMessage(msg);
				}
			}.start();
		}
		else
		{
			Toast.makeText(context, getString(R.string.no_network),
					Toast.LENGTH_SHORT).show();
		}
	}

	/*class CountDownTimerThread extends Thread
	{

		private Handler handler;
		private int time = 60;

		public CountDownTimerThread()
		{
			handler = new Handler();
			mGetCodeBt.setEnabled(false);
			mGetCodeBt.setSelected(true);
		}

		@Override
		public void run()
		{
			while (time > 0)
			{
				try
				{
					Thread.sleep(1000);
					time--;
				}
				catch (InterruptedException e)
				{
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				handler.post(new Runnable()
				{

					@Override
					public void run()
					{
						if (time == 0)
						{
							mGetCodeBt.setText(getString(R.string.resend));
							mGetCodeBt.setEnabled(true);
							mGetCodeBt.setSelected(false);
						}
						else
						{
							mGetCodeBt.setText(getString(R.string.resend) + "("
									+ time + ")");
							mGetCodeBt.setEnabled(false);
							mGetCodeBt.setSelected(true);
						}
					}
				});
			}
		}
	}*/
}
