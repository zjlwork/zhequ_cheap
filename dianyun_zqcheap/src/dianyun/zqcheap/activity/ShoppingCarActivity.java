package dianyun.zqcheap.activity;

import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

import com.umeng.analytics.MobclickAgent;

import dianyun.baobaowd.adapter.ShoppingCarAdapter;
import dianyun.zqcheap.R;
import dianyun.zqcheap.activitybase.BaseActivity;

public class ShoppingCarActivity extends BaseActivity
{

	private Button mActivityBackBt;
	private ShoppingCarAdapter mShoppingCarAdapter;

	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		getTemplate().doInActivity(this, R.layout.shoppingcaractivity);
	}

	@Override
	public void findView()
	{
		super.findView();
		mActivityBackBt = (Button) findViewById(R.id.activityback_bt);
		mActivityBackBt.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View v)
			{
				finish();
			}
		});
	}

	@Override
	public void initListener()
	{
		super.initListener();
	}

	@Override
	public void onResume()
	{
		super.onResume();
		MobclickAgent.onPageStart("商品详情");
	}

	@Override
	public void onPause()
	{
		super.onPause();
		MobclickAgent.onPageEnd("商品详情");
	}
}
