package dianyun.zqcheap.activity;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import dianyun.baobaowd.data.User;
import dianyun.baobaowd.db.LightDBHelper;
import dianyun.baobaowd.dto.ResultDTO;
import dianyun.baobaowd.serverinterface.BindPhone;
import dianyun.baobaowd.serverinterface.BindPhoneSendCode;
import dianyun.baobaowd.util.DialogHelper;
import dianyun.baobaowd.util.NetworkStatus;
import dianyun.baobaowd.util.UserHelper;
import dianyun.zqcheap.R;
import dianyun.zqcheap.activitybase.BaseActivity;

public class BindPhoneActivity extends BaseActivity 
{

	Button mGetCodeBt;
	Button mBindBt;
	EditText mPhoneNumEt;
	EditText mCodeEt;
	private Button mActivityBackBt;
	VerifyTextWatcher mVerifyTextWatcher;
	User mUser;
	private static final int BINDPHONESENDCODE =1;
	private static final int BINDPHONE =2;
//	private int mBindMode;
	
	private String mPhone;
	private String mCode;
	private Handler mHandler   = new Handler(){
		@Override
		public void handleMessage(Message msg)
		{
			
			
			switch (msg.what)
			{
			case BINDPHONESENDCODE:
				DialogHelper.cancelProgressDialog(mProgressDialog);
				
				ResultDTO resultDTO = (ResultDTO) msg.obj;
				if (resultDTO != null && resultDTO.getCode().equals("0"))
				{
					Toast.makeText(BindPhoneActivity.this,
							getString(R.string.autocodewillcome),
							Toast.LENGTH_SHORT).show();
					new CountDownTimerThread().start();
				}
				else
				{
					Toast.makeText(BindPhoneActivity.this,
							getString(R.string.getautocodefailed),
							Toast.LENGTH_SHORT).show();
					mGetCodeBt.setEnabled(true);
					mGetCodeBt.setSelected(false);
				}
				
				
				break;
			case BINDPHONE:
				DialogHelper.cancelProgressDialog(mProgressDialog);
				mBindBt.setEnabled(true);
				mBindBt.setSelected(false);
				ResultDTO resultDTO2 = (ResultDTO) msg.obj;
				if (resultDTO2 != null && resultDTO2.getCode().equals("0"))
				{
//					Toast.makeText(BindPhoneActivity.this,
//							getString(R.string.bindsuccess),
//							Toast.LENGTH_SHORT).show();
					LightDBHelper.setBindPhone(mPhone, BindPhoneActivity.this);
//					if(mBindMode == GobalConstants.BindMode.EXCHANGEMONEY)
						startActivity(BindAlipayActivity.class);
//					else if(mBindMode == GobalConstants.BindMode.EXCHANGEGOLD)
//						startActivity(ExchangeGoldActivity.class);
					finish();
				}
				else
				{
					Toast.makeText(BindPhoneActivity.this,
							getString(R.string.bindfailed),
							Toast.LENGTH_SHORT).show();
				}
				
				break;
			}
			
			
			
			super.handleMessage(msg);
		}
	};
	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		getTemplate().doInActivity(this, R.layout.bindphoneactivity);
			
	}
	
	
	@Override
	public void initData()
	{
		super.initData();
		mUser = UserHelper.getUser();
		mActivityBackBt = (Button) findViewById(R.id.activityback_bt);
		mActivityBackBt.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View v)
			{
				
					finish();
			}
		});
		
		mPhoneNumEt = (EditText)findViewById(R.id.phonenum_et);
		mCodeEt = (EditText)findViewById(R.id.code_et);
		mGetCodeBt = (Button)findViewById(R.id.getcode_bt);
		mBindBt = (Button)findViewById(R.id.bind_bt);
		
		
		mGetCodeBt.setOnClickListener(new OnClickListener()
		{
			
			@Override
			public void onClick(View v)
			{
				mPhone = mPhoneNumEt.getText().toString().trim();
				if(mPhone.length()!=11){
					Toast.makeText(BindPhoneActivity.this, getString(R.string.errorphonenumber), Toast.LENGTH_SHORT).show();
					return;
				}
				bindPhoneSendCode(BindPhoneActivity.this);
			}
		});
		mBindBt.setOnClickListener(new OnClickListener()
		{
			
			@Override
			public void onClick(View v)
			{
//				startActivity(BindAlipayActivity.class);
				bindPhone(BindPhoneActivity.this);
			}
		});
		mPhoneNumEt.requestFocus();
		mVerifyTextWatcher = new VerifyTextWatcher();
		mPhoneNumEt.addTextChangedListener(mVerifyTextWatcher);
		mCodeEt.addTextChangedListener(mVerifyTextWatcher);
		activateBt();
//		mBindMode = getIntent().getIntExtra(GobalConstants.Data.BINDMODE, 0);
	}
	
	
	private void activateBt(){
		if(!TextUtils.isEmpty(mPhoneNumEt.getText())&&!TextUtils.isEmpty(mCodeEt.getText())){
			mBindBt.setSelected(false);
			mBindBt.setEnabled(true);
		}else{
			mBindBt.setSelected(true);
			mBindBt.setEnabled(false);
		}
	}
	
	
	
	
	private class VerifyTextWatcher implements TextWatcher{

		@Override
		public void beforeTextChanged(CharSequence s, int start, int count,
				int after)
		{
			
		}

		@Override
		public void onTextChanged(CharSequence s, int start, int before,
				int count)
		{
			
		}

		@Override
		public void afterTextChanged(Editable s)
		{
			activateBt();
		}
		
	}
	
	Dialog  mProgressDialog;
	private void bindPhoneSendCode(final Context context){
		if(NetworkStatus.getNetWorkStatus(context)>0){
			mProgressDialog = DialogHelper.showProgressDialog(
					context, getString(R.string.getautocodeing));
			mGetCodeBt.setEnabled(false);
			mGetCodeBt.setSelected(true);
			new Thread(){
				public void run(){
					ResultDTO lResultDTO =new BindPhoneSendCode(context, mUser.getUid(), mUser.getToken(), mPhone).postConnect();
					Message msg = new Message();
					msg.what = BINDPHONESENDCODE;
					msg.obj = lResultDTO;
					mHandler.sendMessage(msg);
				}
			}.start();
		}else{
			Toast.makeText(context, getString(R.string.no_network), Toast.LENGTH_SHORT).show();
		}
	}
	private void bindPhone(final Context context){
		if(NetworkStatus.getNetWorkStatus(context)>0){
			mProgressDialog = DialogHelper.showProgressDialog(
					context, getString(R.string.binding));
			mBindBt.setEnabled(false);
			mBindBt.setSelected(true);
			new Thread(){
				public void run(){
					mCode = mCodeEt.getText().toString().trim();
					ResultDTO lResultDTO =new BindPhone(context, mUser.getUid(), mUser.getToken(), mPhone,mCode).postConnect();
					Message msg = new Message();
					msg.what = BINDPHONE;
					msg.obj = lResultDTO;
					mHandler.sendMessage(msg);
				}
			}.start();
		}else{
			Toast.makeText(context, getString(R.string.no_network), Toast.LENGTH_SHORT).show();
		}
	}
	
	
	
	
	
	
	
	class CountDownTimerThread extends Thread
	{

		private Handler handler;
		private int time = 60;

		public CountDownTimerThread()
		{
			handler = new Handler();
			mGetCodeBt.setEnabled(false);
			mGetCodeBt.setSelected(true);
		}

		@Override
		public void run()
		{
			while (time > 0)
			{
				try
				{
					Thread.sleep(1000);
					time--;
				}
				catch (InterruptedException e)
				{
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				handler.post(new Runnable()
				{

					@Override
					public void run()
					{
						if (time == 0)
						{
							mGetCodeBt.setText(getString(R.string.resend));
							mGetCodeBt.setEnabled(true);
							mGetCodeBt.setSelected(false);
						}
						else
						{
							mGetCodeBt.setText(getString(R.string.resend) + "("
									+ time + ")");
							mGetCodeBt.setEnabled(false);
							mGetCodeBt.setSelected(true);
						}
					}
				});
			}
		}
	}
	
	
	
	
	
	
}
