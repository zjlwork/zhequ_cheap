package dianyun.zqcheap.activity;

import java.util.ArrayList;
import java.util.List;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import cn.aow.android.DAOW;

import com.android.xkeuops.CommonManager;
import com.android.xkeuops.DynamicSdkManager;
import com.android.xkeuops.OWManager;
import com.google.gson.reflect.TypeToken;
import com.zhequzheq.DevInit;

import dianyun.baobaowd.data.Dig;
import dianyun.baobaowd.data.User;
import dianyun.baobaowd.db.LightDBHelper;
import dianyun.baobaowd.dto.ResultDTO;
import dianyun.baobaowd.gson.GsonHelper;
import dianyun.baobaowd.serverinterface.GetDigs;
import dianyun.baobaowd.util.DialogHelper;
import dianyun.baobaowd.util.NetworkStatus;
import dianyun.baobaowd.util.ThirdPartHelper;
import dianyun.baobaowd.util.ToastHelper;
import dianyun.baobaowd.util.UserHelper;
import dianyun.zqcheap.R;
import dianyun.zqcheap.activitybase.BaseActivity;

/**
 * Created by pc on 2015/6/10.
 */
public class ScoreActivity extends BaseActivity implements
		View.OnClickListener, AdapterView.OnItemClickListener {

	private ImageView mTopImageView;
	private ListView mListView;
	private Button mBackImageBT;
	private TextView mScoreDescTv;

	private scoreAdapter mAdapter;
	private List<Dig> mList = new ArrayList<Dig>();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getTemplate().doInActivity(this, R.layout.score_activity_lay);
	}

	@Override
	public void findView() {
		super.findView();
		DevInit.setCurrentUserID(this,
				String.valueOf(UserHelper.getUser().getUid()));

		mBackImageBT = (Button) findViewById(R.id.activityback_bt);
		mScoreDescTv = (TextView) findViewById(R.id.des_tv);
		mTopImageView = (ImageView) findViewById(R.id.score_activity_lay_top_image);
		mBackImageBT.setOnClickListener(this);
		mScoreDescTv.setOnClickListener(this);
		mListView = (ListView) findViewById(R.id.score_activity_lay_list);
		mListView.setOnItemClickListener(this);
	}

	public void initData() {

		if (mList == null) {
			mList = new ArrayList<Dig>();
		}
		mList.clear();
		if (mAdapter == null) {
			mAdapter = new scoreAdapter(this, mList);
			mListView.setAdapter(mAdapter);
		}
		if (NetworkStatus.getNetWorkStatus(ScoreActivity.this) > 0) {
			new GetDigsThread(Dig.DIGTYPE_GOLD, 100, 1).start();
		}
		if (!LightDBHelper.getNotNoticeAgain(ScoreActivity.this)) {
			ToastHelper.showScoreWallDialog(ScoreActivity.this);
		}
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.activityback_bt:
			finish();
			break;
		case R.id.des_tv:
			ToastHelper.showScoreWallDialog(ScoreActivity.this);
			break;

		}
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {

		try {
			Dig lDig = mList.get(position);
			if (lDig != null && lDig.digKey != null) {
				if (lDig.showType == Dig.SHOWTYPE_SDKOPEN
						&& lDig.digKey.equals(Dig.DIGKEY_DIANLE)) {
					DevInit.showOffers(this);
				} else if (lDig.showType == Dig.SHOWTYPE_SDKOPEN
						&& lDig.digKey.equals(Dig.DIGKEY_YOUMI)) {
					// true : 可以进行加载积分墙sdk
					// false : 不可以加载积分墙sdk
					// boolean flag
					// =DynamicSdkManager.getInstance(ScoreActivity.this).isSdkLoadComplete();
					// if(flag)OWManager.getInstance(ScoreActivity.this).showOfferWallActivity();
					CommonManager.getInstance(ScoreActivity.this).init(
							ThirdPartHelper.YOUMI_APP_ID,
							ThirdPartHelper.YOUMI_APP_SECRET);
					OWManager.getInstance(ScoreActivity.this).initOfferWall();
					boolean flag = DynamicSdkManager.getInstance(
							ScoreActivity.this).isSdkLoadComplete();
					if (flag) {
						OWManager.getInstance(ScoreActivity.this)
								.setCustomUserId(
										String.valueOf(UserHelper.getUser()
												.getUid()));
						// 告诉sdk采用服务器回调积分订单
						OWManager.getInstance(ScoreActivity.this)
								.setUsingServerCallBack(true);
						OWManager.getInstance(ScoreActivity.this)
								.setPointsLayoutVisibility(false);
						OWManager.getInstance(ScoreActivity.this)
								.showOfferWallActivity();
					}

				} else if (lDig.showType == Dig.SHOWTYPE_SDKOPEN
						&& lDig.digKey.equals(Dig.DIGKEY_DUOMENG)) {
					DAOW.getInstance(ScoreActivity.this).init(
							ThirdPartHelper.DUOMENG_APP_ID,
							String.valueOf(UserHelper.getUser().getUid()));
					DAOW.getInstance(ScoreActivity.this).show(
							ScoreActivity.this);

				} else {
					Toast.makeText(ScoreActivity.this,
							getString(R.string.pleaseupdateversion),
							Toast.LENGTH_SHORT).show();
				}
			}

		} catch (Exception e) {

		}

	}

	Dialog mProgressDialog;

	class GetDigsThread extends Thread {

		private Handler handler;
		private ResultDTO resultDTO;
		private int pagesize;
		private int curPage;
		private int digType;

		public GetDigsThread(int digType, int pagesize, int curPage) {
			handler = new Handler();
			this.pagesize = pagesize;
			this.curPage = curPage;
			this.digType = digType;
			mProgressDialog = DialogHelper.showProgressDialog(
					ScoreActivity.this, getString(R.string.loginloading));
		}

		@Override
		public void run() {
			User mUser = UserHelper.getUser();
			resultDTO = new GetDigs(mUser.getUid(), mUser.getToken(), pagesize,
					curPage, digType).getConnect();
			handler.post(new Runnable() {

				@Override
				public void run() {
					DialogHelper.cancelProgressDialog(mProgressDialog);
					if (resultDTO != null && resultDTO.getCode().equals("0")) {
						List<Dig> digs = GsonHelper.gsonToObj(
								resultDTO.getResult(),
								new TypeToken<List<Dig>>() {
								});
						mList.clear();
						if (digs != null)
							mList.addAll(digs);
						mAdapter.notifyDataSetChanged();
					}
				}
			});
		}
	}

	private class scoreAdapter extends BaseAdapter {

		private final Context mContext;
		private List<Dig> mDataList;

		public scoreAdapter(Context context, List<Dig> dataList) {
			mDataList = dataList;
			mContext = context;
		}

		@Override
		public int getCount() {
			int res = 0;
			if (mDataList != null) {
				res = mDataList.size();
			}
			return res;
		}

		@Override
		public Object getItem(int position) {
			Object res = null;
			if (mDataList != null) {
				mDataList.get(position);
			}
			return res;
		}

		@Override
		public long getItemId(int position) {
			return (long) position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			ViewHolder viewHolder = null;
			Dig dig = mDataList.get(position);
			if (convertView == null) {
				convertView = LayoutInflater.from(mContext).inflate(
						R.layout.score_list_item_lay, null);
				viewHolder = new ViewHolder(convertView);
				convertView.setTag(viewHolder);
			} else {
				viewHolder = (ViewHolder) convertView.getTag();
			}

			viewHolder.mAreaTv.setText(dig.title);
			if (!TextUtils.isEmpty(dig.flag)) {
				if (dig.flag.equals("0")) {
					viewHolder.mAreaIconImgV.setVisibility(View.GONE);
				} else if (dig.flag.equals("1")) {
					viewHolder.mAreaIconImgV.setVisibility(View.VISIBLE);
					viewHolder.mAreaIconImgV
							.setImageResource(R.drawable.score_list_new);

				} else if (dig.flag.equals("2")) {
					viewHolder.mAreaIconImgV.setVisibility(View.VISIBLE);
					viewHolder.mAreaIconImgV
							.setImageResource(R.drawable.score_list_hot);
				}
			}

			return convertView;
		}

		private class ViewHolder {
			public TextView mAreaTv;
			public ImageView mAreaIconImgV;
			public ImageView mAreaRigthICon;

			public ViewHolder(View view) {
				if (view == null) {
					return;
				}
				mAreaTv = (TextView) view
						.findViewById(R.id.score_list_area_name);
				mAreaIconImgV = (ImageView) view
						.findViewById(R.id.score_list_area_hot_icon);
				mAreaRigthICon = (ImageView) view
						.findViewById(R.id.score_list_area_right_arrow);
			}

		}
	}
}
