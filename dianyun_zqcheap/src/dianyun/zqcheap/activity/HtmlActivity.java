package dianyun.zqcheap.activity;

import java.util.UUID;

import android.os.Bundle;
import android.text.ClipboardManager;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.ta.utdid2.android.utils.StringUtils;
import com.umeng.analytics.MobclickAgent;

import dianyun.baobaowd.db.LightDBHelper;
import dianyun.baobaowd.defineview.ShareMenuPopup;
import dianyun.baobaowd.defineview.ShareMenuPopup.ShareMenuPopupClickCallback;
import dianyun.baobaowd.link.LinkHelper;
import dianyun.baobaowd.sinaweibo.SinaWeiboHelper;
import dianyun.baobaowd.util.GobalConstants;
import dianyun.baobaowd.util.UserHelper;
import dianyun.zqcheap.R;
import dianyun.zqcheap.activitybase.BaseActivity;
import dianyun.zqcheap.application.BaoBaoWDApplication;

public class HtmlActivity extends BaseActivity implements
OnClickListener, ShareMenuPopupClickCallback
{

	private TextView mTopTv;
	private WebView mWebView;
	private String url;
	// private byte type;
	private Button mActivityBackBt;
	private TextView mShareTv;
	private ShareMenuPopup  mSharePop;
	private String mTitle;
	private String mFrom;
	private String mShareImageUrl=GobalConstants.URL.SHAREIMAGEURL+"?"+UUID.randomUUID().toString();

	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		getTemplate().doInActivity(this, R.layout.htmlactivity);
	}

	@Override
	public void findView()
	{
		super.findView();
		mTopTv = (TextView) findViewById(R.id.top_tv);
		mWebView = (WebView) findViewById(R.id.webview);
		mShareTv = (TextView) findViewById(R.id.share_tv);
		
		
		mShareTv.setOnClickListener(new OnClickListener()
		{
			
			@Override
			public void onClick(View v)
			{
				
				if (UserHelper.isGusetUser(HtmlActivity.this))
				{
					UserHelper.gusestUserGo(HtmlActivity.this);
				}
				else
				{
					handleOnShareOnClick();
				}
			}
		});
		mWebView.requestFocus();
		mWebView.setScrollBarStyle(View.SCROLLBARS_OUTSIDE_OVERLAY);
		WebSettings setting = mWebView.getSettings();
		setting.setUserAgentString(setting.getUserAgentString() + " yoyo360/"
				+ BaoBaoWDApplication.versionName + " (yoyo360-android)");
		setting.setJavaScriptEnabled(true);
		setting.setSupportZoom(true);
		setting.setBuiltInZoomControls(true);
		
		mWebView.setWebViewClient(new WebViewClient()
		{

			@Override
			public boolean shouldOverrideUrlLoading(WebView view, String url)
			{
//				System.out.println("shouldOverrideUrlLoading url=====" + url);
				return super.shouldOverrideUrlLoading(view, url);
			}
		});
		
		
//		WebChromeClient wvcc = new WebChromeClient() {
//			@Override
//            public void onReceivedTitle(WebView view, String title) {
//                super.onReceivedTitle(view, title);
//                Log.d("ANDROID_LAB", "TITLE=" + title);
//                mTitle = title;
//                mTopTv.setText(title);
//            }
//		};
		// 设置setWebChromeClient对象
//		mWebView.setWebChromeClient(wvcc);
		mWebView.clearCache(true);
		mActivityBackBt = (Button) findViewById(R.id.activityback_bt);
		mActivityBackBt.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View v)
			{
				back();
			}
		});
		if (getIntent() != null)
		{
			url = getIntent().getStringExtra(GobalConstants.Data.URL);
			mFrom = getIntent().getStringExtra(GobalConstants.Data.FROM);
			
			
			String shareImageUrl = getIntent().getStringExtra(GobalConstants.Data.SHAREIMAGEURL);
			if(!TextUtils.isEmpty(shareImageUrl))
					mShareImageUrl = shareImageUrl;
			if(!TextUtils.isEmpty(url)){
				mTitle = getIntent().getStringExtra(GobalConstants.Data.TITLE);
				mTopTv.setText(StringUtils.isEmpty(mTitle)?getString(R.string.detail):mTitle);
			}else{
				url = LinkHelper.extractUidFromUri(getIntent());
				mTitle = getString(R.string.detailsharefromyoyo);
				mTopTv.setText(getString(R.string.detail));
				
			}
			mWebView.loadUrl(url);
//			if(TopicHelper.commodityidNeedAutoLink(url))
			mShareTv.setVisibility(View.VISIBLE);
		}
        mTopTv.setSelected(true);
	}

	
	private void handleOnShareOnClick()
	{
		if (mSharePop == null)
		{
			mSharePop = new ShareMenuPopup(this, mWebView);
			mSharePop.setShareListener(this);
		}
		boolean isSinaLogin = LightDBHelper.getLoginType(this) == GobalConstants.LoginType.SINAWEIBO;
		mSharePop.show(isSinaLogin, false);
	}
	
	
	
	@Override
	public void handleShareOnClick(int i)
	{
		if (i == 2)
		{
			// qq
			bshareToQQFriend(getString(R.string.app_name), mTitle==null?getString(R.string.app_name):mTitle,
					url,mShareImageUrl);
		}
		else if (i == 3)
		{
			// qqzone
			bshareToQQSpace(getString(R.string.app_name),mTitle==null?getString(R.string.app_name):mTitle, url,mShareImageUrl);
		}
		else if (i == 4)
		{
			// sina
//			shareSinaweiboshare();
			SinaWeiboHelper.share(HtmlActivity.this, mTitle, url);
		}
		else if (i == 5)
		{
			// weixin
			boolean result = bshareWeixin(getString(R.string.app_name), mTitle==null?getString(R.string.app_name):mTitle,
					url, false);
			
//			
//			ShareHelper.toShareWeixin(HtmlActivity.this,getString(R.string.app_name), mTitle==null?getString(R.string.app_name):mTitle,
//					url, false);
		}
		else if (i == 6)
		{
			// weixin 朋友圈
			boolean result = bshareWeixin(getString(R.string.app_name), mTitle==null?getString(R.string.app_name):mTitle,
					url, true);
			
//			ShareHelper.toShareWeixin(HtmlActivity.this,getString(R.string.app_name), mTitle==null?getString(R.string.app_name):mTitle,
//					url, true);
			
			
//			if(result)
//				Toast.makeText(HtmlActivity.this, getString(R.string.sharesuccess), Toast.LENGTH_SHORT).show();
		}else if (i == 7) {
			//复制
			
			ClipboardManager c= (ClipboardManager)getSystemService(CLIPBOARD_SERVICE);
			String title =mTitle==null?getString(R.string.app_name):mTitle;
			c.setText(title+url);
			Toast.makeText(HtmlActivity.this, getString(R.string.justcopysuccess), Toast.LENGTH_SHORT).show();
		}
	}

	
	
	private void back(){
		
		if (mWebView.canGoBack())
		{
			mWebView.goBack();
		}
		else
		{
			if(!TextUtils.isEmpty(mFrom)&&mFrom.equals("JudgeActivity"))
				startActivity(MainActivity.class);
			finish();
		}
	}
	
	@Override
	public void initListener()
	{
		super.initListener();
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event)
	{
		if ((keyCode == KeyEvent.KEYCODE_BACK))
		{
			back();
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}

	@Override
	public void onResume()
	{
		super.onResume();
		MobclickAgent.onPageStart("金币规则");
	}

	@Override
	public void onPause()
	{
		super.onPause();
		MobclickAgent.onPageEnd("金币规则");
	}

	@Override
	public void handleFavorOnClick()
	{
		// TODO Auto-generated method stub
		
	}

	@Override
	public void handleSwitchPageOnClick()
	{
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onClick(View v)
	{
		switch (v.getId())
		{
		case R.id.share_iv:
			handleOnShareOnClick();
			break;
		}
		
	}
}
