
package dianyun.zqcheap.activity;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.TextView;

import com.taobao.tae.sdk.callback.CallbackContext;

import dianyun.baobaowd.adapter.MAdapter;
import dianyun.baobaowd.db.LightDBHelper;
import dianyun.baobaowd.defineview.xlistview.CustomListView;
import dianyun.baobaowd.defineview.xlistview.CustomListView.OnLoadMoreListener;
import dianyun.baobaowd.entity.CateItem;
import dianyun.baobaowd.entity.Constants;
import dianyun.baobaowd.entity.LocalMenu;
import dianyun.baobaowd.help.ShopHttpHelper;
import dianyun.baobaowd.interfaces.DialogCallBack;
import dianyun.baobaowd.util.GobalConstants;
import dianyun.baobaowd.util.TaeSdkUtil;
import dianyun.baobaowd.util.ToastHelper;
import dianyun.baobaowd.util.UserHelper;
import dianyun.baobaowd.util.Utils;
import dianyun.zqcheap.R;

public class CheapListActivity extends Activity implements OnClickListener,
        CustomListView.OnRefreshListener,
        OnLoadMoreListener {

    private CustomListView mCustomListView;
    private ImageView mToTopView;
    private View mListViewHeader;
    private TextView mTitleTV;
    private String mTitleString = "";
    private List<CateItem> mGoodsList = new ArrayList<CateItem>();
    private MAdapter mAdapter;
    private int mCurrentPageIndex = 1;
    private int mPageSize = 20;
    private View mCurrentActivityRootView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.cheap_goods_list_lay);
        initView();
        initData();
        super.onCreate(savedInstanceState);
    }

    private void initView() {
        mCurrentActivityRootView = findViewById(R.id.root_view);
        mCustomListView = (CustomListView) findViewById(R.id.goodslist_lv);
        mCustomListView.setCanLoadMore(false);
        mCustomListView.setHeadViewBg(getResources().getColor(R.color.white));
        mToTopView = (ImageView) findViewById(R.id.goodslist_to_top);
        mTitleTV = (TextView) findViewById(R.id.top_tv);
        mCustomListView.setOnRefreshListener(this);
        mCustomListView.setOnLoadListener(this);
        mCustomListView.setCacheColorHint(0);
        mAdapter = new MAdapter(this, mGoodsList);
        mCustomListView.setAdapter(mAdapter);
        mCustomListView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int position,
					long arg3) {
				

		        position = position - 1;
		        if (position >= 0 && mGoodsList != null && mGoodsList.size() > 0
		                && mGoodsList.size() > position) {
		            final CateItem item = mGoodsList.get(position);
		            if (item != null) {
		                if (item.clickType == 4) {
		                    // 跳转到网页上的单品
		                    Intent itn = new Intent(CheapListActivity.this, HtmlActivity.class);
		                    itn.putExtra(GobalConstants.Data.URL,
		                            item.clickUrl == null ? "" : item.clickUrl);
		                    itn.putExtra(GobalConstants.Data.TITLE,
		                            item.pageTitle == null ? "" : item.pageTitle);
		                    startActivity(itn);
		                } else {
		                    if (!LightDBHelper.getIsNotTipLoginOrNotFees(CheapListActivity.this)
		                            && UserHelper.getUser().getIsGuest() == GobalConstants.UserType.ISSELF
		                            && mCurrentActivityRootView != null) {
//		                        ToastHelper.showTipLoginDialogWhenShop(getActivity2(),
//		                                mCurrentActivityView,
//		                                new ToastHelper.dialogCancelCallback() {
		//
//		                                    @Override
//		                                    public void onCancle(boolean isNeverTip) {
//		                                        LightDBHelper
//		                                                .setIsNotTipLoginOrNotFees(
//		                                                        getActivity2(),
//		                                                        isNeverTip);
//		                                        TaeSdkUtil.showTAEItemDetail(
//		                                                getActivity2(), item.tbItemId,
//		                                                item.taobaoPid, item.isTk == 1,
//		                                                item.itemType, null,mCurrentActivityView);
//		                                    }
//		                                });
//		                        
		                        
		                        ToastHelper.showGoLoginDialog(CheapListActivity.this, new DialogCallBack() {
		    						
		    						@Override
		    						public void clickSure() {
		    							
		    						}
		    						
		    						@Override
		    						public void clickCancel() {
		    							TaeSdkUtil.showTAEItemDetail(
		    									CheapListActivity.this, item.tbItemId,
		                                        item.taobaoPid, item.isTk == 1,
		                                        item.itemType, null,mCurrentActivityRootView);
		    						}
		    					});
		    					
		                        
		                        
		                        
		                        
		                    } else {
		                        TaeSdkUtil.showTAEItemDetail(CheapListActivity.this,
		                                item.tbItemId, item.taobaoPid, item.isTk == 1,
		                                item.itemType, null,mCurrentActivityRootView);
		                    }
		                }
		            }
		        }
		    
				
				
				
				
			}
		});
    }

    private void initData() {
        Intent itn = getIntent();
        if (itn != null) {
            mTitleString = itn.getStringExtra(Constants.EXTRA_NAME);
        }
        if (!Utils.isNetAvailable(this)) {
            ToastHelper.show(this,
                    getResources().getString(R.string.net_is_unable));
            return;
        }
        ShopHttpHelper.getHighlestData(this, true, mCurrentPageIndex,
                mPageSize,
                new ShopHttpHelper.ShopDataCallback() {

                    @Override
                    public void getMenu(List<LocalMenu> result) {
                    }

                    @Override
                    public void getChildsData(List<CateItem> result) {
                        doClassList(result);
                    }
                });
    }

    private void doClassList(List<CateItem> result) {
        if (result != null && result.size() > 0) {
            // 筛选出商品
            if (mGoodsList == null) {
                mGoodsList = new ArrayList<CateItem>();
            }
            mGoodsList.clear();
            mCurrentPageIndex = 2;
            for (CateItem item : result) {
                if (item.xType != null && item.xType == 3) {
                    mGoodsList.add(item);
                }
            }
            if (result.size() < mPageSize) {
                mCustomListView.setCanLoadMore2(false);
            } else {
                mCustomListView.setCanLoadMore2(true);
            }
        } else {
            // mGridListView.setCanLoadMore(false);
        }
        mAdapter.notifyDataSetChanged();
    }

    private void doAddClassList(List<CateItem> result) {
        if (result != null && result.size() > 0) {
            mCurrentPageIndex++;
            // 筛选出商品
            if (mGoodsList == null) {
                mGoodsList = new ArrayList<CateItem>();
            }
            for (CateItem item : result) {
                if (item.xType != null && item.xType == 3) {
                    mGoodsList.add(item);
                }
            }
            if (result.size() < mPageSize) {
                // ToastHelper.show(this,
                // getResources().getString(R.string.no_moredata));
                mCustomListView.setCanLoadMore2(false);
            } else {
                mCustomListView.setCanLoadMore2(true);
            }
        } else {
            mCustomListView.setCanLoadMore2(false);
        }
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.activityback_bt:
                finish();
                break;
            case R.id.goodslist_to_top:
                mCustomListView.smoothToTop();
                break;
        }
    }

    @Override
    public void onLoadMore() {
        if (!Utils.isNetAvailable(this)) {
            ToastHelper.show(this,
                    getResources().getString(R.string.net_is_unable));
            mCustomListView.onLoadMoreComplete();
            return;
        }
        ShopHttpHelper.getHighlestData(this, false, mCurrentPageIndex,
                mPageSize,
                new ShopHttpHelper.ShopDataCallback() {

                    @Override
                    public void getMenu(List<LocalMenu> result) {
                        // TODO Auto-generated method stub
                    }

                    @Override
                    public void getChildsData(List<CateItem> result) {
                        doAddClassList(result);
                        mCustomListView.onLoadMoreComplete();
                    }
                });
    }

    @Override
    public void onRefresh() {
        mCurrentPageIndex = 1;
        if (!Utils.isNetAvailable(this)) {
            ToastHelper.show(this,
                    getResources().getString(R.string.net_is_unable));
            mCustomListView.onRefreshComplete();
            return;
        }
        mCustomListView.setCanLoadMore2(false);
        ShopHttpHelper.getHighlestData(this, false, mCurrentPageIndex,
                mPageSize,
                new ShopHttpHelper.ShopDataCallback() {

                    @Override
                    public void getMenu(List<LocalMenu> result) {
                        // TODO Auto-generated method stub
                    }

                    @Override
                    public void getChildsData(List<CateItem> result) {
                        doClassList(result);
                        mCustomListView.onRefreshComplete();
                    }
                });
    }
    
    
    
   
    
    
    
    

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        CallbackContext.onActivityResult(requestCode, resultCode, data);
    }
}
