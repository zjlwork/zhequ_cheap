
package dianyun.zqcheap.activity;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.graphics.PointF;
import android.graphics.Rect;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.FloatMath;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import dianyun.baobaowd.defineview.ClipView;
import dianyun.baobaowd.util.ConversionHelper;
import dianyun.baobaowd.util.GobalConstants;
import dianyun.baobaowd.util.ImageHelper;
import dianyun.zqcheap.R;

/**
 * @author Administrator 整体思想是：截取屏幕的截图，然后截取矩形框里面的图片 代码未经优化，只是一个demo。
 */
public class ClipPictureActivity extends Activity implements OnTouchListener
{

	ImageView srcPic;
	Button sure;
	Button cancel;
	ClipView clipview;
	String filePath;
	String from;
	private int needHeight;
	// These matrices will be used to move and zoom image
	Matrix matrix = new Matrix();
	Matrix savedMatrix = new Matrix();
	// We can be in one of these 3 states
	static final int NONE = 0;
	static final int DRAG = 1;
	static final int ZOOM = 2;
	private static final String TAG = "11";
	int mode = NONE;
	// Remember some things for zooming
	PointF start = new PointF();
	PointF mid = new PointF();
	float oldDist = 1f;

	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.clippictureactivity);
		srcPic = (ImageView) this.findViewById(R.id.src_pic);
		srcPic.setOnTouchListener(this);
		sure = (Button) this.findViewById(R.id.sure);
		sure.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View v)
			{/*
				Intent intent = new Intent();
				intent.setClass(ClipPictureActivity.this,
							PersonCenterActivity.class);
				Bitmap fianBitmap = getBitmap();
				String imgFilePath = ImageHelper.saveImgData(fianBitmap);
				intent.putExtra(GobalConstants.Data.IMGFILEPATH, imgFilePath);
				setResult(RESULT_OK, intent);
				finish();
			*/}
		});
		cancel = (Button) this.findViewById(R.id.cancel);
		cancel.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View v)
			{
				finish();
			}
		});
		if (getIntent() != null
				&& getIntent().getStringExtra(GobalConstants.Data.IMGFILEPATH) != null)
		{
			String filepath = getIntent().getStringExtra(
					GobalConstants.Data.IMGFILEPATH);
			from = getIntent().getStringExtra(GobalConstants.Data.FROM);
			Bitmap bitmap = ImageHelper.getBitmapByMaxSize(filepath,
					ImageHelper.IMGMAXSIZE);
			Bitmap newBitmap = getBitmapByWidth(bitmap, getImageWidth());
			srcPic.setImageBitmap(newBitmap);
		}
		needHeight = ConversionHelper.dipToPx(200, ClipPictureActivity.this);
	}

	public Bitmap getBitmapByWidth(Bitmap bm, int newSize)
	{
		int width = bm.getWidth();
		int height = bm.getHeight();
		float scale = ((float) newSize) / width;
		// if(height*scale<newSize*2){
		Matrix matrix = new Matrix();
		matrix.postScale(scale, scale);
		Bitmap newbm = Bitmap.createBitmap(bm, 0, 0, width, height, matrix,
				true);
		return newbm;
		// }else{
		// return null;
		// }
	}

	private int getImageWidth()
	{
		DisplayMetrics dm = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(dm);
		return dm.widthPixels
				- ConversionHelper.dipToPx(0, ClipPictureActivity.this);
	}

	/* 这里实现了多点触摸放大缩小，和单点移动图片的功能，参考了论坛的代码 */
	@Override
	public boolean onTouch(View v, MotionEvent event)
	{
		ImageView view = (ImageView) v;
		// Handle touch events here...
		switch (event.getAction() & MotionEvent.ACTION_MASK)
		{
		case MotionEvent.ACTION_DOWN:
			savedMatrix.set(matrix);
			// 設置初始點位置
			start.set(event.getX(), event.getY());
			Log.d(TAG, "mode=DRAG");
			mode = DRAG;
			break;
		case MotionEvent.ACTION_POINTER_DOWN:
			oldDist = spacing(event);
			Log.d(TAG, "oldDist=" + oldDist);
			if (oldDist > 10f)
			{
				savedMatrix.set(matrix);
				midPoint(mid, event);
				mode = ZOOM;
				Log.d(TAG, "mode=ZOOM");
			}
			break;
		case MotionEvent.ACTION_UP:
		case MotionEvent.ACTION_POINTER_UP:
			mode = NONE;
			Log.d(TAG, "mode=NONE");
			break;
		case MotionEvent.ACTION_MOVE:
			if (mode == DRAG)
			{
				// ...
				matrix.set(savedMatrix);
				matrix.postTranslate(event.getX() - start.x, event.getY()
						- start.y);
			}
			else
				if (mode == ZOOM)
				{
					float newDist = spacing(event);
					Log.d(TAG, "newDist=" + newDist);
					if (newDist > 10f)
					{
						matrix.set(savedMatrix);
						float scale = newDist / oldDist;
						matrix.postScale(scale, scale, mid.x, mid.y);
					}
				}
			break;
		}
		view.setImageMatrix(matrix);
		return true; // indicate event was handled
	}

	/** Determine the space between the first two fingers */
	private float spacing(MotionEvent event)
	{
		float x = event.getX(0) - event.getX(1);
		float y = event.getY(0) - event.getY(1);
		return FloatMath.sqrt(x * x + y * y);
	}

	/** Calculate the mid point of the first two fingers */
	private void midPoint(PointF point, MotionEvent event)
	{
		float x = event.getX(0) + event.getX(1);
		float y = event.getY(0) + event.getY(1);
		point.set(x / 2, y / 2);
	}

	/*
	 * 点击进入预览 public void onClick(View v) {
	 * 
	 * 
	 * Bitmap fianBitmap = getBitmap(); ByteArrayOutputStream baos = new
	 * ByteArrayOutputStream(); fianBitmap.compress(Bitmap.CompressFormat.JPEG,
	 * 100, baos); byte[] bitmapByte = baos.toByteArray();
	 * 
	 * Intent intent = new Intent(); intent.setClass(getApplicationContext(),
	 * PreviewActivity.class); intent.putExtra("bitmap", bitmapByte);
	 * 
	 * startActivity(intent); }
	 */
	/* 获取矩形区域内的截图 */
	// private Bitmap getBitmap()
	// {
	// getBarHeight();
	// Bitmap screenShoot = takeScreenShot();
	//
	// clipview = (ClipView)this.findViewById(R.id.clipview);
	// int width = clipview.getWidth();
	// int height = clipview.getHeight();
	// Bitmap finalBitmap = Bitmap.createBitmap(screenShoot,
	// (width - height / 3) / 2, height / 3 + titleBarHeight + statusBarHeight,
	// height / 3, height / 3);
	// return finalBitmap;
	// }
	private Bitmap getBitmap()
	{
		int barHeight = getBarHeight();
		Bitmap screenShoot = takeScreenShot();
		// srcPic.setImageBitmap(screenShoot);
		// System.out.println(screenShoot.getWidth()+"          "+screenShoot.getHeight());
		// clipview = (ClipView)this.findViewById(R.id.clipview);
		// int width = clipview.getWidth();
		// int height = clipview.getHeight();
		WindowManager wm = (WindowManager) getSystemService(Context.WINDOW_SERVICE);
		int screenWidth = wm.getDefaultDisplay().getWidth();// 屏幕宽度
		int screenHeight = wm.getDefaultDisplay().getHeight();// 屏幕高度
		int startX = ConversionHelper.dipToPx(0, ClipPictureActivity.this);
		int startY = (screenHeight - getBarHeight() - needHeight) / 2;
		int width = screenWidth
				- ConversionHelper.dipToPx(0, ClipPictureActivity.this);
		int height = needHeight;
		Bitmap finalBitmap = Bitmap.createBitmap(screenShoot, startX + 3,
				startY + getBarHeight() + 3, width - 6, height - 6);
		return finalBitmap;
	}
	int statusBarHeight = 0;
	int titleBarHeight = 0;

	private int getBarHeight()
	{
		// 获取状态栏高度
		Rect frame = new Rect();
		this.getWindow().getDecorView().getWindowVisibleDisplayFrame(frame);
		statusBarHeight = frame.top;
		// int contenttop = this.getWindow()
		// .findViewById(Window.ID_ANDROID_CONTENT).getTop();
		// // statusBarHeight是上面所求的状态栏的高度
		// titleBarHeight = contenttop - statusBarHeight;
		Log.v(TAG, "statusBarHeight = " + statusBarHeight
				+ ", titleBarHeight = " + titleBarHeight);
		return statusBarHeight;
	}

	// 获取Activity的截屏
	private Bitmap takeScreenShot()
	{
		View view = this.getWindow().getDecorView();
		view.setDrawingCacheEnabled(true);
		view.buildDrawingCache();
		return view.getDrawingCache();
	}
}