
package dianyun.zqcheap.activity;

import java.util.ArrayList;
import java.util.List;

import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import dianyun.baobaowd.adapter.MiaoshaAdapter;
import dianyun.baobaowd.defineview.xlistview.CustomListView;
import dianyun.baobaowd.entity.SecKill;
import dianyun.baobaowd.help.ShopHttpHelper;
import dianyun.baobaowd.util.GobalConstants;
import dianyun.baobaowd.util.ToastHelper;
import dianyun.baobaowd.util.Utils;
import dianyun.zqcheap.R;
import dianyun.zqcheap.activitybase.BaseActivity;

public class MiaoshaActivity extends BaseActivity implements OnClickListener,
        CustomListView.OnRefreshListener {

    private CustomListView mGridListView;
    private ImageView mToTopView;
    private List<SecKill> mGoodsList = new ArrayList<SecKill>();
    private MiaoshaAdapter mAdapter;
//    public static final int REFRESH = 3;

//    public Handler mADHandler = new Handler() {
//        public void handleMessage(android.os.Message msg) {
//
//            switch (msg.what) {
//                case REFRESH:
//                	SecKill mEndSecKill =null;
//                    for(int i=0;i<mGoodsList.size();i++){
//						SecKill lSecKill =mGoodsList.get(i);
//						long startLeftTime = lSecKill.startLeftTime;
//						long endLeftTime = lSecKill.endLeftTime;
//						
//						if(lSecKill.isStart){
//							if(endLeftTime>0){
//								lSecKill.endLeftTime-=1;
//							}else{
//								if(lSecKill.isStart)
//									mEndSecKill = lSecKill;
//							}
//						}else{
//							if(startLeftTime>0){
//								lSecKill.startLeftTime-=1;
//							}else{
//								if(!lSecKill.isStart)
//									lSecKill.isStart=true;
//							}
//						}
//						
//					}
//                    if(mEndSecKill!=null)mGoodsList.remove(mEndSecKill);
//                    
//                    
//                    mAdapter.notifyDataSetChanged();
//                    mADHandler.sendEmptyMessageDelayed(REFRESH, 1000);
//                    break;
//            }
//        }
//    };
//    
    
    

    @Override
    protected void onCreate(Bundle savedInstanceState) {
    	
        super.onCreate(savedInstanceState);
        getTemplate().doInActivity(this, R.layout.miaoshaactivity);
    }

    public void findView() {
//        mCurrentActivityRootView = findViewById(R.id.root_view);
        mGridListView = (CustomListView) findViewById(R.id.goodslist_lv);
        mGridListView.setCanLoadMore(false);
        mGridListView.setHeadViewBg(getResources().getColor(R.color.white));
        mToTopView = (ImageView) findViewById(R.id.goodslist_to_top);
        mToTopView.setVisibility(View.GONE);
        mGridListView.setOnRefreshListener(this);
        mAdapter = new MiaoshaAdapter(this, mGoodsList);
//        mAdapter.setCurrentActivityRootView(mCurrentActivityRootView);
        mGridListView.setAdapter(mAdapter);
        mGridListView.setCacheColorHint(0);
        mGridListView.setCanLoadMore2(false);
        findViewById(R.id.role_tv).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Utils.goHtmlActivity(MiaoshaActivity.this, getString(R.string.miaosha_role), GobalConstants.URL.MIAOSHAROLE);
			}
		});
    }

    public void initData() {
    	mGridListView.refresh();
    }

   


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.activityback_bt:
                finish();
                break;
            case R.id.goodslist_to_top:
                mGridListView.smoothToTop();
                break;
        }
    }

   

    @Override
    public void onRefresh() {
        if (!Utils.isNetAvailable(this)) {
            ToastHelper.show(this,
                    getResources().getString(R.string.net_is_unable));
            mGridListView.onRefreshComplete();
            return;
        }
        
        ShopHttpHelper.getMiaoShaData(this, false, 
                new ShopHttpHelper.MiaoShaDataCallback() {
					
					@Override
					public void result(List<SecKill> result) {
						 mGridListView.onRefreshComplete();
						 if (result != null && result.size() > 0) {
					            // 筛选出商品
					            mGoodsList.clear();
					            mGoodsList.addAll(result);
					            mAdapter.notifyDataSetChanged();
//					            mADHandler.removeMessages(REFRESH);
//					            mADHandler.sendEmptyMessage(REFRESH);
					        } 
						
					}
				});
        
    }

}
