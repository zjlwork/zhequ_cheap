package dianyun.zqcheap.activity;

import java.util.ArrayList;
import java.util.List;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.GridView;

import com.umeng.analytics.MobclickAgent;

import dianyun.baobaowd.adapter.GoodsGroupAdapter;
import dianyun.baobaowd.data.Item;
import dianyun.zqcheap.R;
import dianyun.zqcheap.activitybase.BaseActivity;

public class GoodsGroupActivity extends BaseActivity
{

	private Button mActivityBackBt;
	private GridView mGridView;
	private List<Item> mGoodsList;
	private GoodsGroupAdapter mGoodsGroupAdapter;

	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		getTemplate().doInActivity(this, R.layout.goodsgroupactivity);
	}

	@Override
	public void findView()
	{
		super.findView();
		mGridView = (GridView) findViewById(R.id.gridview);
		mActivityBackBt = (Button) findViewById(R.id.activityback_bt);
		mActivityBackBt.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View v)
			{
				finish();
			}
		});
		mGoodsList = new ArrayList<Item>();
		mGoodsGroupAdapter = new GoodsGroupAdapter(mGoodsList,
				GoodsGroupActivity.this);
		mGridView.setAdapter(mGoodsGroupAdapter);
		mGridView.setOnItemClickListener(new OnItemClickListener()
		{

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id)
			{
				Intent lIntent = new Intent(GoodsGroupActivity.this,
						GoodsDetailActivity.class);
				startActivity(lIntent);
			}
		});
	}

	@Override
	public void initListener()
	{
		super.initListener();
	}

	@Override
	public void onResume()
	{
		super.onResume();
		MobclickAgent.onPageStart("商品专场");
	}

	@Override
	public void onPause()
	{
		super.onPause();
		MobclickAgent.onPageEnd("商品专场");
	}
}
