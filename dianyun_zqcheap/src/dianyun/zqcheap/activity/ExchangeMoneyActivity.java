package dianyun.zqcheap.activity;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import dianyun.baobaowd.data.User;
import dianyun.baobaowd.data.YcoinPay;
import dianyun.baobaowd.db.LightDBHelper;
import dianyun.baobaowd.dto.ResultDTO;
import dianyun.baobaowd.gson.GsonHelper;
import dianyun.baobaowd.interfaces.DialogCallBack;
import dianyun.baobaowd.serverinterface.ExchangeMoney;
import dianyun.baobaowd.serverinterface.GetUserInfo;
import dianyun.baobaowd.util.BroadCastHelper;
import dianyun.baobaowd.util.DialogHelper;
import dianyun.baobaowd.util.GobalConstants;
import dianyun.baobaowd.util.NetworkStatus;
import dianyun.baobaowd.util.ServiceInterfaceHelper;
import dianyun.baobaowd.util.ToastHelper;
import dianyun.baobaowd.util.UserHelper;
import dianyun.baobaowd.util.Utils;
import dianyun.zqcheap.R;
import dianyun.zqcheap.activitybase.BaseActivity;

public class ExchangeMoneyActivity extends BaseActivity {

//	Button mGetCodeBt;
	Button mOkBt;
	EditText mExchangeMoneyEt;
//	EditText mCodeEt;
	// TextView mMoneyCountTv;
	TextView mYoCoinsTv;
	TextView mHintTv;
	TextView mHelpTv;
	

	private Button mActivityBackBt;
	VerifyTextWatcher mVerifyTextWatcher;
	private int mExchangeMoneyCount = 0;

	private User mUser;
//	final int EXCHANGEMONDYSENDCODE = 1;
	final int EXCHANGE = 2;

	int exchangeCount;

	private Handler mHandler = new Handler() {
		@Override
		public void handleMessage(Message msg) {

			switch (msg.what) {
//			case EXCHANGEMONDYSENDCODE:
//				DialogHelper.cancelProgressDialog(mProgressDialog);
//
//				ResultDTO resultDTO = (ResultDTO) msg.obj;
//				if (resultDTO != null && resultDTO.getCode().equals("0")) {
//					Toast.makeText(ExchangeMoneyActivity.this,
//							getString(R.string.autocodewillcome),
//							Toast.LENGTH_SHORT).show();
//					new CountDownTimerThread().start();
//				} else {
//					Toast.makeText(ExchangeMoneyActivity.this,
//							getString(R.string.getautocodefailed),
//							Toast.LENGTH_SHORT).show();
//					mGetCodeBt.setEnabled(true);
//					mGetCodeBt.setSelected(false);
//				}
//
//				break;
			case EXCHANGE:
				DialogHelper.cancelProgressDialog(mProgressDialog);
				mOkBt.setEnabled(true);
				ResultDTO resultDTO2 = (ResultDTO) msg.obj;
				if (resultDTO2 != null && resultDTO2.getCode().equals("0")) {
					mUser.setYcoins(mUser.getYcoins() - mExchangeMoneyCount);
					UserHelper.setUser(mUser);

					// Toast.makeText(ExchangeMoneyActivity.this,
					// getString(R.string.applysuccess),
					// Toast.LENGTH_SHORT).show();
					Intent lIntent = new Intent(ExchangeMoneyActivity.this,
							PaySuccessActivity.class);
					lIntent.putExtra("istixian", true);
					startActivity(lIntent);
					finish();
					
				} else if (resultDTO2 != null
						&& resultDTO2.getCode().equals("-1040")) {
					
					try{
						int minimum = Integer.parseInt(resultDTO2.getResult());
						if(mUser.getYcoins()<minimum){
							ToastHelper.showLessGoldDialog(ExchangeMoneyActivity.this, getString(R.string.lessgoldhint));
						}else{
							Toast.makeText(ExchangeMoneyActivity.this,
									resultDTO2.getErrorMsg(),
									Toast.LENGTH_SHORT).show();
						}
					}catch(Exception e){
						
					}
					
					
				}else if (resultDTO2 != null
						&& resultDTO2.getCode().equals("-1041")) {
					try{
						int minimum = Integer.parseInt(resultDTO2.getResult());
						if(mUser.getYcoins()<minimum){
							ToastHelper.showLessGoldDialog(ExchangeMoneyActivity.this, getString(R.string.lessgoldhint));
						}else{
							Toast.makeText(ExchangeMoneyActivity.this,
									resultDTO2.getErrorMsg(),
									Toast.LENGTH_SHORT).show();
						}
					}catch(Exception e){
						
					}
				} else {
					Toast.makeText(ExchangeMoneyActivity.this,
							resultDTO2.getErrorMsg(), Toast.LENGTH_SHORT)
							.show();
				}

				break;
			}

			super.handleMessage(msg);
		}
	};

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getTemplate().doInActivity(this, R.layout.exchangemoneyactivity);

	}

	@Override
	public void initData() {
		super.initData();
		mUser = UserHelper.getUser();
		// mMoneyCountTv = (TextView) findViewById(R.id.moneycount_tv);
		mHintTv = (TextView) findViewById(R.id.hint_tv);
		mHelpTv = (TextView) findViewById(R.id.help_tv);
		String hint = String.format(getString(R.string.exchangemoneyhint),
				LightDBHelper.getBindPay(ExchangeMoneyActivity.this));
		mHintTv.setText(hint);
		mYoCoinsTv = (TextView) findViewById(R.id.yocoins_tv);
		mYoCoinsTv.setText(String.valueOf(mUser.getYcoins()));
		mActivityBackBt = (Button) findViewById(R.id.activityback_bt);
		mActivityBackBt.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				finish();
			}
		});
		mHelpTv.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Utils.goHtmlActivity(ExchangeMoneyActivity.this, getResources().getString(R.string.jifenbaohowtouse),
						GobalConstants.URL.YOYOBASE+GobalConstants.URL.JIFENBAOHOWTOUSE);
			}
		});

		mExchangeMoneyEt = (EditText) findViewById(R.id.moneycount_et);
//		mCodeEt = (EditText) findViewById(R.id.code_et);
//		mGetCodeBt = (Button) findViewById(R.id.getcode_bt);
		mOkBt = (Button) findViewById(R.id.ok_bt);

//		mGetCodeBt.setOnClickListener(new OnClickListener() {
//
//			@Override
//			public void onClick(View v) {
//				exchangeMoneySendCode(ExchangeMoneyActivity.this);
//			}
//		});
		mOkBt.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				exchange(ExchangeMoneyActivity.this);

			}
		});
		mExchangeMoneyEt.requestFocus();
		mExchangeMoneyEt.addTextChangedListener(new VerifyTextWatcher(
				mExchangeMoneyEt));
//		mCodeEt.addTextChangedListener(new VerifyTextWatcher(mCodeEt));
		activateBt();

		if (NetworkStatus.getNetWorkStatus(ExchangeMoneyActivity.this) > 0) {
			new GetUserInfoThread().start();
			
			 ServiceInterfaceHelper.getUserInfo(ExchangeMoneyActivity.this,mUser.getUid(), mUser.getToken(), 
					 mUser.getUid(), true,new ServiceInterfaceHelper.InterfaceCallBack(){
		
							@Override
							public void getResultDTO(ResultDTO resultDTO) {
								if (resultDTO != null && resultDTO.getCode().equals("0")) {
									User user = GsonHelper.gsonToObj(resultDTO.getResult(),
											User.class);
									if (user != null) {
										mUser.setYcoins(user.getYcoins());
										mYoCoinsTv.setText(String.valueOf(mUser.getYcoins()));
										UserHelper.setUser(mUser);
										BroadCastHelper.sendRefreshMainBroadcast(
												ExchangeMoneyActivity.this,
												GobalConstants.RefreshType.USER);
									}
								}
							}
		        	
		        });
			
		}

	}

	private class VerifyTextWatcher implements TextWatcher {

		EditText view;

		public VerifyTextWatcher(EditText view) {
			this.view = view;
		}

		@Override
		public void beforeTextChanged(CharSequence s, int start, int count,
				int after) {

		}

		@Override
		public void onTextChanged(CharSequence s, int start, int before,
				int count) {

		}

		@Override
		public void afterTextChanged(Editable s) {
			activateBt();
		}

	}

	private void activateBt() {
		if (!TextUtils.isEmpty(mExchangeMoneyEt.getText())) {
			mOkBt.setSelected(false);
			mOkBt.setEnabled(true);
		} else {
			mOkBt.setSelected(true);
			mOkBt.setEnabled(false);
		}
	}

//	private void exchangeMoneySendCode(final Context context) {
//		if (NetworkStatus.getNetWorkStatus(context) > 0) {
//			mProgressDialog = DialogHelper.showProgressDialog(context,
//					getString(R.string.getautocodeing));
//			mGetCodeBt.setEnabled(false);
//			mGetCodeBt.setSelected(true);
//			new Thread() {
//				public void run() {
//					ResultDTO lResultDTO = new ExchangeSendCode(mUser.getUid(),
//							mUser.getToken()).postConnect();
//					Message msg = new Message();
//					msg.what = EXCHANGEMONDYSENDCODE;
//					msg.obj = lResultDTO;
//					mHandler.sendMessage(msg);
//				}
//			}.start();
//		} else {
//			Toast.makeText(context, getString(R.string.no_network),
//					Toast.LENGTH_SHORT).show();
//		}
//	}

	private void exchange(final Context context) {
		if (NetworkStatus.getNetWorkStatus(context) > 0) {

			mExchangeMoneyCount = 0;
			try {
				mExchangeMoneyCount = Integer.parseInt(mExchangeMoneyEt
						.getText().toString().trim());
				
//				if (mExchangeMoneyCount> mUser.getYcoins()) {
//					Toast.makeText(ExchangeMoneyActivity.this,
//							getString(R.string.exchangemoney_noenough),
//							Toast.LENGTH_SHORT).show();
//					return;
//				}

			} catch (Exception e) {
				Toast.makeText(ExchangeMoneyActivity.this,
						getString(R.string.exchangemoneyinputerror),
						Toast.LENGTH_SHORT).show();
				return;
			}

			mProgressDialog = DialogHelper.showProgressDialog(
					ExchangeMoneyActivity.this, getString(R.string.applying));
//			mGetCodeBt.setEnabled(false);
//			mGetCodeBt.setSelected(true);
			new Thread() {
				public void run() {
					ResultDTO lResultDTO = new ExchangeMoney(mUser.getUid(),
							mUser.getToken(), mExchangeMoneyCount,
							YcoinPay.PAYTYPE_MONDY, "").getConnect();
					Message msg = new Message();
					msg.what = EXCHANGE;
					msg.obj = lResultDTO;
					mHandler.sendMessage(msg);
				}
			}.start();
		} else {
			Toast.makeText(context, getString(R.string.no_network),
					Toast.LENGTH_SHORT).show();
		}
	}

	/*class CountDownTimerThread extends Thread {

		private Handler handler;
		private int time = 60;

		public CountDownTimerThread() {
			handler = new Handler();
			mGetCodeBt.setEnabled(false);
			mGetCodeBt.setSelected(true);
		}

		@Override
		public void run() {
			while (time > 0) {
				try {
					Thread.sleep(1000);
					time--;
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				handler.post(new Runnable() {

					@Override
					public void run() {
						if (time == 0) {
							mGetCodeBt.setText(getString(R.string.resend));
							mGetCodeBt.setEnabled(true);
							mGetCodeBt.setSelected(false);
						} else {
							mGetCodeBt.setText(getString(R.string.resend) + "("
									+ time + ")");
							mGetCodeBt.setEnabled(false);
							mGetCodeBt.setSelected(true);
						}
					}
				});
			}
		}
	}*/

	Dialog mProgressDialog;

	class GetUserInfoThread extends Thread {

		private Handler handler;
		private ResultDTO resultDTO;
		private User user;

		public GetUserInfoThread() {
			handler = new Handler();
			mProgressDialog = DialogHelper.showDetailLoadingDialog(
					ExchangeMoneyActivity.this,
					getString(R.string.loginloading), new DialogCallBack() {

						@Override
						public void clickSure() {
							finish();
						}

						@Override
						public void clickCancel() {
						}
					});
			mProgressDialog.setCanceledOnTouchOutside(false);
			mProgressDialog.setCancelable(false);
		}

		@Override
		public void run() {
			resultDTO = new GetUserInfo(mUser.getUid(), mUser.getToken(),
					mUser.getUid()).getConnect();

			handler.post(new Runnable() {

				@Override
				public void run() {
					DialogHelper.cancelProgressDialog(mProgressDialog);
					if (resultDTO != null && resultDTO.getCode().equals("0")) {
						user = GsonHelper.gsonToObj(resultDTO.getResult(),
								User.class);
						if (user != null) {
							mUser.setYcoins(user.getYcoins());
							mYoCoinsTv.setText(String.valueOf(mUser.getYcoins()));
							UserHelper.setUser(mUser);
							BroadCastHelper.sendRefreshMainBroadcast(
									ExchangeMoneyActivity.this,
									GobalConstants.RefreshType.USER);
						}
					}
				}
			});
		}
	}

}
