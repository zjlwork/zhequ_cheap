package dianyun.zqcheap.activity;

import java.util.Date;
import java.util.List;

import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.text.Html;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.reflect.TypeToken;
import com.umeng.analytics.MobclickAgent;

import dianyun.baobaowd.data.CheckinLog;
import dianyun.baobaowd.data.Dig;
import dianyun.baobaowd.data.SysConfig;
import dianyun.baobaowd.data.User;
import dianyun.baobaowd.db.LightDBHelper;
import dianyun.baobaowd.dto.ResultDTO;
import dianyun.baobaowd.gson.GsonHelper;
import dianyun.baobaowd.help.LogFile;
import dianyun.baobaowd.serverinterface.CheckIn;
import dianyun.baobaowd.serverinterface.GetCheckInInfo;
import dianyun.baobaowd.serverinterface.GetDigs;
import dianyun.baobaowd.util.BroadCastHelper;
import dianyun.baobaowd.util.DateHelper;
import dianyun.baobaowd.util.DialogHelper;
import dianyun.baobaowd.util.GobalConstants;
import dianyun.baobaowd.util.NetworkStatus;
import dianyun.baobaowd.util.TaeSdkUtil;
import dianyun.baobaowd.util.ToastHelper;
import dianyun.baobaowd.util.UserHelper;
import dianyun.baobaowd.util.Utils;
import dianyun.zqcheap.R;
import dianyun.zqcheap.activitybase.BaseActivity;

public class TaskActivity extends BaseActivity implements OnClickListener {

	private Button mActivityBackBt;

	private LinearLayout mCheckInLayout;
	private TextView mCheckInTv;
	private View mCheckInView;
	private TextView mGoldTv;
	private TextView mCheckInDesTv;
	private TextView mDesTv;

	private RelativeLayout install_application_lay;
	private User mUser;

	private boolean isloading;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getTemplate().doInActivity(this, R.layout.taskactivity);
	}

	@Override
	public void findView() {
		super.findView();
		mUser = UserHelper.getUser();
		install_application_lay = (RelativeLayout) findViewById(R.id.install_application_lay);
		mActivityBackBt = (Button) findViewById(R.id.activityback_bt);
		mGoldTv = (TextView) findViewById(R.id.gold_tv);
		mDesTv = (TextView) findViewById(R.id.des_tv);
		mCheckInTv = (TextView) findViewById(R.id.checkin_tv);
		mCheckInView = findViewById(R.id.checkin_view);
		mCheckInDesTv = (TextView) findViewById(R.id.checkindes_tv);
		mCheckInLayout = (LinearLayout) findViewById(R.id.checkin_layout);

		mActivityBackBt.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				finish();
			}
		});

		mCheckInLayout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (UserHelper.isGusetUser(TaskActivity.this)) {
					UserHelper.gusestUserGo(TaskActivity.this);
				} else {
					String checkInTime = LightDBHelper
							.getCheckInTime(TaskActivity.this);
					String nowTime = DateHelper.getTextByDate(new Date(),
							DateHelper.YYYY_MM_DD);
					if (!checkInTime.equals(nowTime) && !isloading) {
						if (NetworkStatus.getNetWorkStatus(TaskActivity.this) > 0) {
							new CheckInThread(mUser.getUid(), mUser.getToken())
									.start();
						} else {
							ToastHelper.show(TaskActivity.this,
									getString(R.string.no_network));
						}
					}
				}
			}
		});

		refreshStatus();
		des(0, 0, true);
		setReceiver();
		if (NetworkStatus.getNetWorkStatus(TaskActivity.this) > 0) {
			if (mUser.getIsSelf() == GobalConstants.AllStatusYesOrNo.YES) {
				new GetCheckInInfoThread().start();
			}
		}
		if (LightDBHelper.getJfqSwitch(TaskActivity.this).equals(
				SysConfig.SWITCH_OFF)) {
			install_application_lay.setVisibility(View.GONE);
		} else {
			install_application_lay.setVisibility(View.VISIBLE);
		}

	}

	private void refreshStatus() {
		String checkInTime = LightDBHelper.getCheckInTime(TaskActivity.this);

		String nowTime = DateHelper.getTextByDate(new Date(),
				DateHelper.YYYY_MM_DD);
		if (checkInTime != null && checkInTime.equals(nowTime)) {
			mCheckInTv.setText(getString(R.string.finish));
			mCheckInDesTv.setText(getString(R.string.checkindesfinish));
			mCheckInLayout.setSelected(true);
			mCheckInLayout.setEnabled(false);
			mCheckInTv.setSelected(true);
			mCheckInDesTv.setSelected(true);
			mCheckInView.setBackgroundColor(getResources().getColor(
					R.color.gray));
		} else {
			mCheckInTv.setText(getString(R.string.checkin));
			mCheckInDesTv.setText(getString(R.string.checkindesunfinish));
			mCheckInLayout.setSelected(false);
			mCheckInLayout.setEnabled(true);
			mCheckInTv.setSelected(false);
			mCheckInDesTv.setSelected(false);
			mCheckInView.setBackgroundColor(getResources().getColor(
					R.color.topbgcolor));
		}

		mGoldTv.setText(String.valueOf(mUser.getYcoins()
				+ mUser.getFreezeYcoins()));
	}

	private void des(int maxCheckinNum, int nextCoins, boolean today) {
		String checkincount;
		if (today)
			checkincount = "你已累计签到<font color=\"#ec2553\"> " + maxCheckinNum
					+ " </font>天，今日签到可获取" + "<font color=\"#ec2553\"> "
					+ nextCoins + " </font>金币";
		else
			checkincount = "你已累计签到<font color=\"#ec2553\"> " + maxCheckinNum
					+ " </font>天，明日签到可获取" + "<font color=\"#ec2553\"> "
					+ nextCoins + " </font>金币";

		mDesTv.setText(Html.fromHtml(checkincount));
	}

	@Override
	public void onResume() {
		super.onResume();
		MobclickAgent.onPageStart("任务界面");
	}

	@Override
	public void onPause() {
		super.onPause();
		MobclickAgent.onPageEnd("任务界面");
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.install_application_lay:
			if (UserHelper.isGusetUser(TaskActivity.this)) {
				UserHelper.gusestUserGo(TaskActivity.this);
			} else {
				Utils.goActivity(this, ScoreActivity.class);
			}
			break;
		case R.id.invite_friends_lay:
			if (UserHelper.isGusetUser(TaskActivity.this)) {
				UserHelper.gusestUserGo(TaskActivity.this);
			} else {
				Utils.goActivity(this, InviteFriendActivity.class);
			}
			break;
		case R.id.install_application_getmoney:
			if (UserHelper.isGusetUser(TaskActivity.this)) {
				UserHelper.gusestUserGo(TaskActivity.this);
			} else {
				if (NetworkStatus.getNetWorkStatus(TaskActivity.this) > 0) {
					new GetDigsThread(Dig.DIGTYPE_NOTGOLD, 100, 1).start();
				} else {
					ToastHelper.show(TaskActivity.this,
							getString(R.string.no_network));
				}

			}
			break;
		case R.id.share_order_lay:
			if (UserHelper.isGusetUser(TaskActivity.this)) {
				UserHelper.gusestUserGo(TaskActivity.this);
			} else {
				Intent lIntent = new Intent();
				lIntent.setClass(TaskActivity.this, OrderRecordActivity.class);
				lIntent.putExtra("mode", OrderRecordActivity.REBATE);
				startActivity(lIntent);
			}
			break;
		}
	}

	class CheckInThread extends Thread {

		private Handler handler;
		private ResultDTO resultDTO;
		private long uid;
		private String token;

		public CheckInThread(long uid, String token) {
			handler = new Handler();
			this.uid = uid;
			this.token = token;
			isloading = true;
		}

		@Override
		public void run() {
			resultDTO = new CheckIn(TaskActivity.this, uid, token)
					.postConnect();
			handler.post(new Runnable() {

				@Override
				public void run() {
					isloading = false;
					if (resultDTO != null && resultDTO.getCode().equals("0")) {

						CheckinLog lCheckinLog = GsonHelper.gsonToObj(
								resultDTO.getResult(), CheckinLog.class);
						if (lCheckinLog != null) {
							int coin = lCheckinLog.getCoins();
							if (coin > 0) {
								Toast toast = new Toast(TaskActivity.this);
								toast.setGravity(Gravity.CENTER, 0, 0);
								View toastView = LayoutInflater.from(
										TaskActivity.this).inflate(
										R.layout.toastview, null);
								TextView title_tv = (TextView) toastView
										.findViewById(R.id.title_tv);
								TextView detail_tv = (TextView) toastView
										.findViewById(R.id.detail_tv);
								title_tv.setText("  +" + coin);
								detail_tv
										.setText(getString(R.string.checkinsuccesshint));
								toast.setView(toastView);
								toast.show();
								LightDBHelper.setCheckInTime(DateHelper
										.getTextByDate(new Date(),
												DateHelper.YYYY_MM_DD),
										TaskActivity.this);

								mUser.setYcoins(mUser.getYcoins() + coin);
								UserHelper.setUser(mUser);
								refreshStatus();
								des(lCheckinLog.getMaxCheckinNum(),
										lCheckinLog.getNextCoins(), false);
								BroadCastHelper.sendRefreshMainBroadcast(
										TaskActivity.this,
										GobalConstants.RefreshType.USER);
								BroadCastHelper.sendRefreshMainBroadcast(
										TaskActivity.this,
										GobalConstants.RefreshType.TASKSTATUS);
							}

						} else {
							Toast.makeText(TaskActivity.this,
									getString(R.string.checkinfailed),
									Toast.LENGTH_SHORT).show();
						}

					} else if (resultDTO != null
							&& resultDTO.getCode().equals(
									getString(R.string.errorcode_1013))) {
						Toast.makeText(TaskActivity.this,
								getString(R.string.errorcode_1013_hint),
								Toast.LENGTH_SHORT).show();
						LightDBHelper.setCheckInTime(DateHelper.getTextByDate(
								new Date(), DateHelper.YYYY_MM_DD),
								TaskActivity.this);
						refreshStatus();
						BroadCastHelper.sendRefreshMainBroadcast(
								TaskActivity.this,
								GobalConstants.RefreshType.TASKSTATUS);
					} else {
						Toast.makeText(TaskActivity.this,
								getString(R.string.checkinfailed),
								Toast.LENGTH_SHORT).show();
						refreshStatus();
					}
				}
			});
		}
	}

	Dialog mProgressDialog;

	class GetDigsThread extends Thread {

		private Handler handler;
		private ResultDTO resultDTO;
		private int pagesize;
		private int curPage;
		private int digType;

		public GetDigsThread(int digType, int pagesize, int curPage) {
			handler = new Handler();
			this.pagesize = pagesize;
			this.curPage = curPage;
			this.digType = digType;
			mProgressDialog = DialogHelper.showProgressDialog(
					TaskActivity.this, getString(R.string.loginloading));
		}

		@Override
		public void run() {
			User mUser = UserHelper.getUser();
			resultDTO = new GetDigs(mUser.getUid(), mUser.getToken(), pagesize,
					curPage, digType).getConnect();
			handler.post(new Runnable() {

				@Override
				public void run() {
					DialogHelper.cancelProgressDialog(mProgressDialog);
					if (resultDTO != null && resultDTO.getCode().equals("0")) {
						List<Dig> digs = GsonHelper.gsonToObj(
								resultDTO.getResult(),
								new TypeToken<List<Dig>>() {
								});
						if (digs != null && digs.size() > 0) {
							for (Dig dig : digs) {
								if (dig.digKey.equals(Dig.DIGKEY_MAJIBAO)) {
									// Intent itn = new
									// Intent(TaskActivity.this,
									// WebHtmlActivity.class);
									// itn.putExtra(GobalConstants.Data.URL,
									// dig.showValue);
									// itn.putExtra(GobalConstants.Data.TITLE,
									// getString(R.string.gold_task_getmoney_top_desc));
									// startActivity(itn);

									TaeSdkUtil.showMjbPage(TaskActivity.this,
											dig.showValue);

									return;
								}
							}
						}

					}
				}
			});
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
	}

	class GetCheckInInfoThread extends Thread {

		private Handler handler;
		private ResultDTO resultDTO;

		public GetCheckInInfoThread() {
			handler = new Handler();
		}

		@Override
		public void run() {
			resultDTO = new GetCheckInInfo(mUser.getUid(), mUser.getToken())
					.getConnect();
			handler.post(new Runnable() {

				@Override
				public void run() {
					if (resultDTO != null && resultDTO.getCode().equals("0")) {
						CheckinLog lCheckinLog = GsonHelper.gsonToObj(
								resultDTO.getResult(), CheckinLog.class);

						if (lCheckinLog != null) {
							mGoldTv.setText(String.valueOf(mUser.getYcoins()
									+ mUser.getFreezeYcoins()));
							des(lCheckinLog.getMaxCheckinNum(),
									lCheckinLog.getNextCoins(),
									!lCheckinLog.isHasCheckin());
							mUser.setYcoins(lCheckinLog.getCurrentCoins());
							UserHelper.setUser(mUser);

							if (lCheckinLog.isHasCheckin()
									&& !mCheckInLayout.isSelected()) {
								LightDBHelper.setCheckInTime(DateHelper
										.getTextByDate(new Date(),
												DateHelper.YYYY_MM_DD),
										TaskActivity.this);
								refreshStatus();
							}
						}
					}
				}
			});
		}
	}

	private RefreshReceiver mRefreshReceiver;

	private void setReceiver() {
		try {
			if (null == mRefreshReceiver) {
				// 房间状态
				mRefreshReceiver = new RefreshReceiver();
				registerReceiver(mRefreshReceiver, new IntentFilter(
						GobalConstants.IntentFilterAction.REFRESH));
			}
		} catch (Exception e) {
			LogFile.SaveExceptionLog(e);
		}
	}

	public class RefreshReceiver extends BroadcastReceiver {

		@Override
		public void onReceive(Context context, Intent intent) {
			byte refreshType = intent.getByteExtra(
					GobalConstants.Data.REFRESHTYPE, (byte) 0);
			if (refreshType == GobalConstants.RefreshType.REFRESHTASKSTATUS) {
				refreshStatus();
			} else if (refreshType == GobalConstants.RefreshType.CHANGEUSER) {
				mUser = UserHelper.getUser();
				mGoldTv.setText(String.valueOf(mUser.getYcoins()
						+ mUser.getFreezeYcoins()));
			} else if (refreshType == GobalConstants.RefreshType.CHAGNEUSERGOLD) {
				mUser = UserHelper.getUser();
				mGoldTv.setText(String.valueOf(mUser.getYcoins()
						+ mUser.getFreezeYcoins()));
			} else if (refreshType == GobalConstants.RefreshType.ADDUSERGOLD) {
				try {
					int allGold = intent.getIntExtra(
							GobalConstants.Data.ADDGOLD_COUNT, 0);
					int gold = Integer.parseInt(mGoldTv.getText().toString());
					gold += allGold;
					mGoldTv.setText(String.valueOf(gold));
				} catch (Exception e) {

				}
			}
		}
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		if (mRefreshReceiver != null) {
			unregisterReceiver(mRefreshReceiver);
			mRefreshReceiver = null;
		}
	}

}
