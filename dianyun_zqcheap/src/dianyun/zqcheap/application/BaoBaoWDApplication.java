package dianyun.zqcheap.application;

import java.io.File;

import android.annotation.SuppressLint;
import android.app.Application;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.StatFs;
import android.telephony.TelephonyManager;
import android.util.Log;

import com.android.xkeuops.DynamicSdkManager;
import com.nostra13.universalimageloader.cache.disc.impl.UnlimitedDiscCache;
import com.nostra13.universalimageloader.cache.disc.naming.Md5FileNameGenerator;
import com.nostra13.universalimageloader.cache.memory.impl.LRULimitedMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;
import com.nostra13.universalimageloader.core.download.BaseImageDownloader;

import dianyun.baobaowd.crash.CrashHandler;
import dianyun.baobaowd.help.LogFile;
import dianyun.baobaowd.util.NetworkStatus;
import dianyun.baobaowd.util.TaeSdkUtil;
import dianyun.baobaowd.xiaomipush.ApplicationHandler;
import dianyun.baobaowd.xiaomipush.RegisterPushHelper;
import dianyun.zqcheap.R;

@SuppressLint("NewApi")
public class BaoBaoWDApplication extends Application {

    private ApplicationHandler handler = null;
    private File mDir;
    public static DisplayImageOptions mOptions;
    private static final int DEFAULT_DISK_CACHE_SIZE = 1024 * 1024 * 10; // 10MB
    public static Context context = null;
    public static String versionName;
    public static int versionCode;
    public static String channel = "";
    public static String deviceId;
    public static String cachePath = null, downLoadCachePaht = null;
    private final String TAG = "TAE";

    
    
    
    @Override
    public void onCreate() {
        super.onCreate();
        context = this;
        System.out.println("BaoBaoWDApplication-----------------onCreate ");
        try {
            TaeSdkUtil.initTaeSdk(getApplicationContext());
            initGlobalParams();
            initPhotoBitmap();
            initVersionName();
            initHandler();
            initCrashHandler();
            if (NetworkStatus.getNetWorkStatus(getApplicationContext()) > 0)
                RegisterPushHelper.registerPush(getApplicationContext());
            DynamicSdkManager.onCreate(this);
            
            
        } catch (Exception e) {
            LogFile.SaveExceptionLog(e);
        }
    }

 
    @Override
    public void onTerminate() {
        // TODO Auto-generated method stub
        super.onTerminate();
    }

    private void initGlobalParams() {
//		SpeechUtility
//				.createUtility(this, "appid=" + getString(R.string.app_id));
//		SharedPreferences mSharedPreferences = getSharedPreferences(
//				getPackageName(), Context.MODE_PRIVATE);
//		mSpeechRecongnizeEngnieer = SpeechRecognizer.createRecognizer(this,
//				null);
//		mSpeechRecongnizeEngnieer.setParameter(SpeechConstant.PARAMS, null);
//		String lag = mSharedPreferences.getString("iat_language_preference",
//				"mandarin");
//		// 设置语言
//		mSpeechRecongnizeEngnieer
//				.setParameter(SpeechConstant.LANGUAGE, "zh_cn");
//		// 设置语言区域
//		mSpeechRecongnizeEngnieer.setParameter(SpeechConstant.ACCENT, lag);
//		// 设置语音前端点
//		mSpeechRecongnizeEngnieer.setParameter(SpeechConstant.VAD_BOS,
//				mSharedPreferences.getString("iat_vadbos_preference", "10000"));
//		// 设置语音后端点
//		mSpeechRecongnizeEngnieer.setParameter(SpeechConstant.VAD_EOS,
//				mSharedPreferences.getString("iat_vadeos_preference", "10000"));
//		// 设置标点符号
//		mSpeechRecongnizeEngnieer.setParameter(SpeechConstant.ASR_PTT,
//				mSharedPreferences.getString("iat_punc_preference", "1"));
//		// 设置音频保存路径
//		mSpeechRecongnizeEngnieer.setParameter(SpeechConstant.ASR_AUDIO_PATH,
//				Environment.getExternalStorageDirectory()
//						+ "/iflytek/wavaudio.pcm");
        boolean ready = Environment.MEDIA_MOUNTED.equals(Environment
                .getExternalStorageState());
        boolean readyExternal = isExternalStorageRemovable();
        if (!readyExternal) {// for interal storage
            File dir = getExternalCacheDir(this);
            if (!dir.exists()) {
                dir.mkdirs();
            }
            if (getUsableSpace(dir) > DEFAULT_DISK_CACHE_SIZE) {
                cachePath = dir.getPath();
            } else {
                // low space for application
                // Toast.makeText(context, "low space for application",
                // Toast.LENGTH_SHORT).show();
            }
        } else if (ready) { // for extend storage
            File sdCard_dir = getExternalStorageDir(this);
            if (!sdCard_dir.exists()) {
                sdCard_dir.mkdirs();
            }
            if (getUsableSpace(sdCard_dir) > DEFAULT_DISK_CACHE_SIZE) {
                cachePath = sdCard_dir.getPath();
            } else {
                // low space for application
                // Toast.makeText(context, "low space for application",
                // Toast.LENGTH_SHORT).show();
            }
        }
        if (cachePath == null) {
            File f = getDir("ower", Context.MODE_PRIVATE);
            cachePath = f.getPath();
        }
        downLoadCachePaht = cachePath.concat("Cache");
        mDir = new File(downLoadCachePaht);
        if (!mDir.exists()) {
            mDir.mkdirs();
        }
        Log.d(TAG, mDir.getAbsolutePath());
    }

    private void initPhotoBitmap() {
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(
                context)
                .threadPriority(Thread.NORM_PRIORITY - 2)
                .memoryCache(new LRULimitedMemoryCache(1024 * 1024 * 30))
                .memoryCacheSize(1024 * 1024 * 30)
                .threadPoolSize(5)
                .denyCacheImageMultipleSizesInMemory()
                .diskCacheFileNameGenerator(new Md5FileNameGenerator())
                .diskCacheSize(50 * 1024 * 1024)
                .discCache(new UnlimitedDiscCache(mDir))
                        // 自定义缓存路径
                .defaultDisplayImageOptions(DisplayImageOptions.createSimple())
                .imageDownloader(
                        new BaseImageDownloader(context, 5 * 1000, 30 * 1000))
                        // 50 Mb
                .tasksProcessingOrder(QueueProcessingType.LIFO)
                .writeDebugLogs()// Remove for release app
                .build();
        // Initialize ImageLoader with configuration.
        ImageLoader.getInstance().init(config);
        mOptions = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.defaultsmallimg)
                .showImageForEmptyUri(R.drawable.defaultsmallimg)
                .showImageOnFail(R.drawable.defaultsmallimg)
                        // .resetViewBeforeLoading(true)
                        // 设置图片在下载前是否重置，复位
                .cacheInMemory(true).cacheOnDisk(true).considerExifParams(true)
                .imageScaleType(ImageScaleType.EXACTLY_STRETCHED)
                .bitmapConfig(Bitmap.Config.RGB_565).build();
        // mPhotoBitmap = FinalBitmap.create(context, cachePath, 1024 * 1024 *
        // 8,
        // 100);
        // mPhotoBitmap.configLoadingImage(R.drawable.defaultsmallimg);
        // mPhotoBitmap.configLoadfailImage(R.drawable.defaultsmallimg);
    }

    private void initVersionName() {
        try {
            PackageManager packageManager = getPackageManager();
            PackageInfo packInfo = packageManager.getPackageInfo(
                    getPackageName(), 0);
            versionName = packInfo.versionName;
            versionCode = packInfo.versionCode;
            ApplicationInfo ai = packageManager.getApplicationInfo(
                    getPackageName(), PackageManager.GET_META_DATA);
            Bundle bundle = ai.metaData;
            channel = bundle.getString("UMENG_CHANNEL");
            TelephonyManager tm = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
            deviceId = tm.getDeviceId();
        } catch (NameNotFoundException e) {
            e.printStackTrace();
        }
    }

    public static File getExternalCacheDir(Context context) {
        File f = null;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.FROYO) {
            f = Environment.getExternalStorageDirectory();// 获取跟目录
        }
        if (f == null) {
            // Before Froyo we need to construct the external cache dir
            // ourselves
            final String cacheDir = "/Android/data/" + context.getPackageName()
                    + "/cache/";
            f = new File(Environment.getExternalStorageDirectory().getPath()
                    + cacheDir);
        }
        return f;
    }

    public static File getExternalStorageDir(Context context) {
        // Before Froyo we need to construct the external cache dir ourselves
        final String cacheDir = "/Android/data/" + context.getPackageName()
                + "/cache/";
        return new File(Environment.getExternalStorageDirectory().getPath()
                + cacheDir);
    }

    public static long getUsableSpace(File path) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.GINGERBREAD) {
            return path.getUsableSpace();
        }
        final StatFs stats = new StatFs(path.getPath());
        return (long) stats.getBlockSize() * (long) stats.getAvailableBlocks();
    }

    public static boolean isExternalStorageRemovable() {
        return Environment.getExternalStorageState().equals(
                android.os.Environment.MEDIA_MOUNTED); // 判断sd卡是否存在
    }

    private void initHandler() {
        if (handler == null)
            handler = new ApplicationHandler(getApplicationContext());
    }

    private void initCrashHandler() {
        CrashHandler crashHandler = CrashHandler.getInstance();
        // 注册crashHandler
        crashHandler.init(getApplicationContext());
        // 发送以前没发送的报告(可选)
        crashHandler.sendPreviousReportsToServer();
    }


    public ApplicationHandler getHandler() {
        return handler;
    }
}
