package dianyun.zqcheap.wxapi;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import com.tencent.mm.sdk.openapi.BaseReq;
import com.tencent.mm.sdk.openapi.BaseResp;
import com.tencent.mm.sdk.openapi.ConstantsAPI;
import com.tencent.mm.sdk.openapi.IWXAPI;
import com.tencent.mm.sdk.openapi.IWXAPIEventHandler;
import com.tencent.mm.sdk.openapi.SendAuth;
import com.tencent.mm.sdk.openapi.SendMessageToWX;
import com.tencent.mm.sdk.openapi.WXAPIFactory;

import dianyun.baobaowd.util.BroadCastHelper;
import dianyun.baobaowd.util.GobalConstants;
import dianyun.baobaowd.util.ThirdPartHelper;
import dianyun.baobaowd.util.ToastHelper;
import dianyun.zqcheap.R;
import dianyun.zqcheap.application.BaoBaoWDApplication;

public class WXEntryActivity extends Activity implements IWXAPIEventHandler {

	private IWXAPI api;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// 通过WXAPIFactory工厂，获取IWXAPI的实例
		api = WXAPIFactory.createWXAPI(this, ThirdPartHelper.WEIXINAPP_ID,
				false);
		api.registerApp(ThirdPartHelper.WEIXINAPP_ID);
		api.handleIntent(getIntent(), this);
	}

	@Override
	protected void onNewIntent(Intent intent) {
		super.onNewIntent(intent);
		System.out.println("onNewIntent-------------");
		setIntent(intent);
		api.handleIntent(intent, this);
	}

	// 微信发送请求到第三方应用时，会回调到该方法
	@Override
	public void onReq(BaseReq req) {
		switch (req.getType()) {
		case ConstantsAPI.COMMAND_GETMESSAGE_FROM_WX:
			// goToGetMsg();
			break;
		case ConstantsAPI.COMMAND_SHOWMESSAGE_FROM_WX:
			// goToShowMsg((ShowMessageFromWX.Req) req);
			break;
		default:
			break;
		}
	}

	// 第三方应用发送到微信的请求处理后的响应结果，会回调到该方法
	@Override
	public void onResp(BaseResp resp) {
		String result = "";
		try {

			SendAuth.Resp authResp = ((SendAuth.Resp) resp);
			if (authResp != null && authResp.state != null
					&& authResp.state.equals("login")) {
				switch (resp.errCode) {
				case BaseResp.ErrCode.ERR_OK:
					// 登录成功
					WXKeeper.writeAccessToken(BaoBaoWDApplication.context,
							authResp.token);
					BroadCastHelper.sendRefreshMainBroadcast(
							WXEntryActivity.this,
							GobalConstants.RefreshType.WXLOGINSUCCESS);
					break;
				default:
					result = getString(R.string.loginfailed);
					Toast.makeText(WXEntryActivity.this, result,
							Toast.LENGTH_SHORT).show();
					break;
				}

			} else {
				shareToWxResp(resp);
			}
			// System.out.println("onResp===="+resp.toString());

		} catch (Exception e) {
			e.printStackTrace();
			shareToWxResp(resp);
		}

		finish();

	}

	private void shareToWxResp(BaseResp resp) {
		try {
			SendMessageToWX.Resp sendmsgResp = ((SendMessageToWX.Resp) resp);
			String result = "";
			switch (sendmsgResp.errCode) {
			case BaseResp.ErrCode.ERR_OK:
				result = getString(R.string.sharesuccess);
				BroadCastHelper.sendRefreshMainBroadcast(WXEntryActivity.this,
						GobalConstants.RefreshType.SHAREORDERSUCCESS);
				break;
			case BaseResp.ErrCode.ERR_USER_CANCEL:
				result = getString(R.string.cancelshare);
				break;
			default:
				result = getString(R.string.sharefailed);
				;
				break;
			}
			ToastHelper.show(WXEntryActivity.this, result);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}