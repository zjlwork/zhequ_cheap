package dianyun.zqcheap.wxapi;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONObject;

import android.app.Dialog;
import android.content.Context;
import android.os.AsyncTask;
import android.text.TextUtils;

import com.tencent.mm.sdk.openapi.IWXAPI;
import com.tencent.mm.sdk.openapi.SendAuth;
import com.tencent.mm.sdk.openapi.SendMessageToWX;
import com.tencent.mm.sdk.openapi.WXAPIFactory;

import dianyun.baobaowd.util.DialogHelper;
import dianyun.baobaowd.util.TencentHelper;
import dianyun.baobaowd.util.ThirdPartHelper;
import dianyun.zqcheap.R;

public class WXHelper {
	
	private static IWXAPI api;
	private static WXLoginCallBack mWXLoginCallBack;
	
	private static IWXAPI getWXAPIInstance(Context context)
	{
		if (null == api)
		{
			api = WXAPIFactory.createWXAPI(context, ThirdPartHelper.WEIXINAPP_ID,
					false);
			api.registerApp(ThirdPartHelper.WEIXINAPP_ID);
		}
		return api;
	}
	public static void setWXLoginCallBack(WXLoginCallBack callback){
		mWXLoginCallBack = callback;
	}
	
	

	public boolean bshareWeixin(Context context ,String title, String summaryTxt, String url,
			boolean toSpace)
	{
		if(title==null||summaryTxt==null||url==null)return false;
		
		if (summaryTxt.length() > 100)
			summaryTxt = summaryTxt.substring(0, 99);
		SendMessageToWX.Req req = TencentHelper.getShareWeixinReq(title,
				summaryTxt, url,
				context, toSpace);
		boolean result = getWXAPIInstance(context).sendReq(req);
		return result;
	}
	
	
	
	public static void login(Context context){
		SendAuth.Req req = new SendAuth.Req();
	    req.scope = "snsapi_userinfo";
	    req.state = "login";
	    boolean result = getWXAPIInstance(context).sendReq(req);
	}
	
	
	public static void getUserInfo(Context context){
		new GetWXUserInfoAsnycTask(context, true).execute();
	}
	
	
	  protected static class GetWXUserInfoAsnycTask extends
	    AsyncTask<Void, Void, String> {
	    	
//	    	private GetFreezeYoCoinsCallback mCallback = null;
	    	private Dialog mDialog = null;
	    	private Context mContext;
	    	private boolean mIsShowDialog;
	    	long uid;
	    	public GetWXUserInfoAsnycTask(Context context,boolean isShowDialog) {
	    		this.mContext = context;
	    		this.mIsShowDialog = isShowDialog;
	    	}
	    	
	    	@Override
	    	protected void onPreExecute() {
	    		if (mIsShowDialog) {
	    			mDialog = DialogHelper.showProgressDialog(mContext, mContext
	    					.getResources().getString(R.string.loginloading));
	    		}
	    		super.onPreExecute();
	    	}
	    	
	    	@Override
	    	protected void onPostExecute(String result) {
	    		if (mIsShowDialog && mDialog != null) {
	    			mDialog.dismiss();
	    		}
	    		if (null != mWXLoginCallBack) {
	    			mWXLoginCallBack.result(result);
	    		}
	    		super.onPostExecute(result);
	    	}
	    	
	    	
	    	@Override
	    	protected String doInBackground(Void... params) {
	    		StringBuilder s = new StringBuilder();
	    		try {
	    			
	    			
	    			String gettokenurl = "https://api.weixin.qq.com/sns/oauth2/access_token?appid="+ThirdPartHelper.WEIXINAPP_ID+"&secret="+ThirdPartHelper.WEIXINAPP_SECRET
	    			+"&code="+WXKeeper.readAccessToken(mContext)+"&grant_type=authorization_code";
	    			
	    			
//					String url = "https://api.weixin.qq.com/sns/userinfo?access_token="+token+"&openid="+ThirdPartHelper.WEIXINAPP_ID;
					DefaultHttpClient lDefaultHttpClient =new DefaultHttpClient();
					HttpGet lHttpGet = new HttpGet(gettokenurl);
					HttpResponse response = lDefaultHttpClient.execute(lHttpGet);
					if (200 == response.getStatusLine().getStatusCode())
					{
						InputStream instream = response.getEntity().getContent();
						BufferedReader reader = new BufferedReader(
								new InputStreamReader(instream, "UTF-8"));
						String sResponse;
						while ((sResponse = reader.readLine()) != null)
						{
							s = s.append(sResponse);
						}
//						return s.toString();
					}
					
					if(!TextUtils.isEmpty(s.toString())){
						JSONObject jsonObject = new JSONObject(s.toString());
						String access_token = jsonObject.get("access_token").toString();
						s = new StringBuilder();
						
						String url = "https://api.weixin.qq.com/sns/userinfo?access_token="+access_token+"&openid="+ThirdPartHelper.WEIXINAPP_ID;
						 lHttpGet = new HttpGet(url);
						 response = lDefaultHttpClient.execute(lHttpGet);
						if (200 == response.getStatusLine().getStatusCode())
						{
							InputStream instream = response.getEntity().getContent();
							BufferedReader reader = new BufferedReader(
									new InputStreamReader(instream, "UTF-8"));
							String sResponse;
							while ((sResponse = reader.readLine()) != null)
							{
								s = s.append(sResponse);
							}
							return s.toString();
						}
					}
					
				} catch (Exception e) {
					e.printStackTrace();
				}
	    		return null;
	    	}
	    }
	
	
	public interface WXLoginCallBack{
		void result(String userinfo);
	}
	
	
}
