package com.zbar.lib;

import java.io.IOException;

import android.app.Activity;
import android.content.res.AssetFileDescriptor;
import android.graphics.Point;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.os.Bundle;
import android.os.Handler;
import android.os.Vibrator;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceHolder.Callback;
import android.view.SurfaceView;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.ScaleAnimation;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.zbar.lib.camera.CameraManager;
import com.zbar.lib.decode.CaptureActivityHandler;
import com.zbar.lib.decode.DecodeHandler;
import com.zbar.lib.decode.InactivityTimer;

import dianyun.baobaowd.defineview.DianYunProductDialog;
import dianyun.baobaowd.defineview.DianYunProductDialog.RestartScanInterface;
import dianyun.baobaowd.entity.CateItem;
import dianyun.baobaowd.help.ShopHttpHelper;
import dianyun.baobaowd.help.ShopHttpHelper.ScanCheckCallback;
import dianyun.baobaowd.util.GobalConstants;
import dianyun.baobaowd.util.ToastHelper;
import dianyun.baobaowd.util.Utils;
import dianyun.zqcheap.R;

/**
 * 浣滆��: 闄堟稕(1076559197@qq.com)
 * 
 * 鏃堕棿: 2014骞�5鏈�9鏃� 涓嬪崍12:25:31
 * 
 * 鐗堟湰: V_1.0.0
 * 
 * 鎻忚堪: 鎵弿鐣岄潰
 */
public class CaptureActivity extends Activity implements Callback,
		OnClickListener, RestartScanInterface, ScanCheckCallback
{

	private CaptureActivityHandler handler;
	private boolean hasSurface;
	private InactivityTimer inactivityTimer;
	private MediaPlayer mediaPlayer;
	private boolean playBeep;
	private static final float BEEP_VOLUME = 0.50f;
	private boolean vibrate;
	private int x = 0;
	private int y = 0;
	private int cropWidth = 0;
	private int cropHeight = 0;
	private RelativeLayout mContainer = null;
	private RelativeLayout mCropLayout = null;
	private Button mSearhBT;
	private EditText mSearchEdt;
	private ScaleAnimation mAnimation;
	private ImageView mQrLineView;
	private View mScanTotalLay;
	private View mScanSearchLay;
	private DianYunProductDialog mProductDialog;
	private View mScanHelpLay;
	private boolean mIsCanRestart = true;
	private String mOriginUrl = "";

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_qr_scan);
		// 鍒濆鍖� CameraManager
		CameraManager.init(this);
		DecodeHandler.mIsCanDecode = true;
		hasSurface = false;
		inactivityTimer = new InactivityTimer(this);
		mContainer = (RelativeLayout) findViewById(R.id.capture_containter);
		mCropLayout = (RelativeLayout) findViewById(R.id.capture_crop_layout);
		mQrLineView = (ImageView) findViewById(R.id.capture_scan_line);
		mSearchEdt = (EditText) findViewById(R.id.scan_search_edt);
		mSearhBT = (Button) findViewById(R.id.scan_searchBT);
		mSearhBT.setOnClickListener(this);
		mAnimation = new ScaleAnimation(1.0f, 1.0f, 0.0f, 1.0f);
		mAnimation.setRepeatCount(-1);
		mAnimation.setRepeatMode(Animation.RESTART);
		mAnimation.setInterpolator(new LinearInterpolator());
		mAnimation.setDuration(1200);
		mQrLineView.startAnimation(mAnimation);
		mScanSearchLay = findViewById(R.id.scan_search_lay);
		mScanHelpLay = findViewById(R.id.scan_help);
		mScanHelpLay.setOnClickListener(this);
	}
	boolean flag = true;

	protected void light()
	{
		if (flag == true)
		{
			flag = false;
			// 寮�闂厜鐏�
			CameraManager.get().openLight();
		}
		else
		{
			flag = true;
			// 鍏抽棯鍏夌伅
			CameraManager.get().offLight();
		}
	}

	@SuppressWarnings("deprecation")
	@Override
	protected void onResume()
	{
		Log.d("onResume", "handle isnull:" + (handler == null));
		super.onResume();
		SurfaceView surfaceView = (SurfaceView) findViewById(R.id.capture_preview);
		SurfaceHolder surfaceHolder = surfaceView.getHolder();
		if (hasSurface)
		{
			initCamera(surfaceHolder);
		}
		else
		{
			surfaceHolder.addCallback(this);
			surfaceHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
		}
		playBeep = true;
		AudioManager audioService = (AudioManager) getSystemService(AUDIO_SERVICE);
		if (audioService.getRingerMode() != AudioManager.RINGER_MODE_NORMAL)
		{
			playBeep = false;
		}
		if (mIsCanRestart)
		{
			vibrate = true;
			mQrLineView.startAnimation(mAnimation);
		}
	}

	@Override
	protected void onPause()
	{
		super.onPause();
		if (handler != null)
		{
			handler.quitSynchronously();
			handler = null;
		}
		CameraManager.get().closeDriver();
		mAnimation.cancel();
		Log.d("onPause", "handle isnull:" + (handler == null));
	}

	@Override
	protected void onDestroy()
	{
		inactivityTimer.shutdown();
		DecodeHandler.mIsCanDecode = true;
		super.onDestroy();
	}

	public int getX()
	{
		return x;
	}

	public void setX(int x)
	{
		this.x = x;
	}

	public int getY()
	{
		return y;
	}

	public void setY(int y)
	{
		this.y = y;
	}

	public int getCropWidth()
	{
		return cropWidth;
	}

	public void setCropWidth(int cropWidth)
	{
		this.cropWidth = cropWidth;
	}

	public int getCropHeight()
	{
		return cropHeight;
	}

	public void setCropHeight(int cropHeight)
	{
		this.cropHeight = cropHeight;
	}

	public void handleDecode(String result)
	{
		inactivityTimer.onActivity();
		playBeepSoundAndVibrate();
		// Toast.makeText(getApplicationContext(), result, Toast.LENGTH_SHORT)
		// .show();
		mAnimation.cancel();
		mIsCanRestart = false;
		DecodeHandler.mIsCanDecode = false;
		mOriginUrl = getHttpUrl(result);
		ShopHttpHelper.requestCheckUrl(this, mOriginUrl, this);
		// 杩炵画鎵弿锛屼笉鍙戦�佹娑堟伅鎵弿涓�娆＄粨鏉熷悗灏变笉鑳藉啀娆℃壂鎻�
		// handler.sendEmptyMessage(R.id.restart_preview);
	}

	private String getHttpUrl(String url)
	{
		String res = url+"";
//		if (!TextUtils.isEmpty(url))
//		{
//			int index = url.indexOf("http");
//			if (index >= 0)
//			{
//				res = url.substring(index);
//			}
//		}
		return res;
	}

	private void initCamera(SurfaceHolder surfaceHolder)
	{
		try
		{
			CameraManager.get().openDriver(surfaceHolder);
			Point point = CameraManager.get().getCameraResolution();
			int width = point.y;
			int height = point.x;
			int x = mCropLayout.getLeft() * width / mContainer.getWidth();
			int y = mCropLayout.getTop() * height / mContainer.getHeight();
			int cropWidth = mCropLayout.getWidth() * width
					/ mContainer.getWidth();
			int cropHeight = mCropLayout.getHeight() * height
					/ mContainer.getHeight();
			setX(x);
			setY(y);
			setCropWidth(cropWidth);
			setCropHeight(cropHeight);
		}
		catch (IOException ioe)
		{
			return;
		}
		catch (RuntimeException e)
		{
			return;
		}
		if (handler == null)
		{
			handler = new CaptureActivityHandler(CaptureActivity.this);
		}
	}

	@Override
	public void surfaceChanged(SurfaceHolder holder, int format, int width,
			int height)
	{
	}

	@Override
	public void surfaceCreated(SurfaceHolder holder)
	{
		if (!hasSurface)
		{
			hasSurface = true;
			initCamera(holder);
		}
	}

	@Override
	public void surfaceDestroyed(SurfaceHolder holder)
	{
		hasSurface = false;
	}

	public Handler getHandler()
	{
		return handler;
	}

	private void initBeepSound()
	{
		if (playBeep && mediaPlayer == null)
		{
			setVolumeControlStream(AudioManager.STREAM_MUSIC);
			mediaPlayer = new MediaPlayer();
			mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
			mediaPlayer.setOnCompletionListener(beepListener);
			AssetFileDescriptor file = getResources().openRawResourceFd(
					R.raw.beep);
			try
			{
				mediaPlayer.setDataSource(file.getFileDescriptor(),
						file.getStartOffset(), file.getLength());
				file.close();
				mediaPlayer.setVolume(BEEP_VOLUME, BEEP_VOLUME);
				mediaPlayer.prepare();
			}
			catch (IOException e)
			{
				mediaPlayer = null;
			}
		}
	}
	private static final long VIBRATE_DURATION = 200L;

	private void playBeepSoundAndVibrate()
	{
		if (playBeep && mediaPlayer != null)
		{
			mediaPlayer.start();
		}
		if (vibrate)
		{
			Vibrator vibrator = (Vibrator) getSystemService(VIBRATOR_SERVICE);
			vibrator.vibrate(VIBRATE_DURATION);
		}
	}
	private final OnCompletionListener beepListener = new OnCompletionListener()
	{

		public void onCompletion(MediaPlayer mediaPlayer)
		{
			mediaPlayer.seekTo(0);
		}
	};

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event)
	{
		boolean res = true;
		switch (keyCode)
		{
		case KeyEvent.KEYCODE_VOLUME_UP:
			light();
			res = false;
			break;
		case KeyEvent.KEYCODE_BACK:
			if (mProductDialog != null && mProductDialog.isShowing())
			{
				mProductDialog.dismiss();
				restartScan();
				res = false;
			}
			else
			{
				finish();
			}
			break;
		}
		if (res)
		{
			return super.onKeyDown(keyCode, event);
		}
		else
		{
			return true;
		}
	}

	@Override
	public void onClick(View v)
	{
		switch (v.getId())
		{
		case R.id.scan_searchBT:
			checkUrl();
			break;
		case R.id.scan_help:
	
			Utils.goHtmlActivity(CaptureActivity.this, getResources().getString(R.string.scan_title_describle),
					GobalConstants.URL.YOYOBASE+GobalConstants.URL.SHOP_SCAN_HELP_URL);
			break;
		case R.id.activityback_bt:
			finish();
			break;
		default:
			break;
		}
	}

	private void checkUrl()
	{
		if (mSearchEdt != null)
		{
			if (TextUtils.isEmpty(mSearchEdt.getEditableText().toString()
					.trim()))
			{
				ToastHelper.showByGravity(
						this,
						getResources().getString(
								R.string.shop_scan_can_not_empty),
						Gravity.CENTER);
				return;
			}
			else
			{
				inactivityTimer.onActivity();
				mAnimation.cancel();
				mIsCanRestart = false;
				DecodeHandler.mIsCanDecode = false;
				mOriginUrl = getHttpUrl(mSearchEdt.getEditableText().toString()
						.trim());
				ShopHttpHelper.requestCheckUrl(this, mOriginUrl, this);
			}
		}
	}

	private void showScanResultDialog(CateItem item)
	{
		mProductDialog = null;
		mProductDialog = new DianYunProductDialog(this, item, mContainer,
				mOriginUrl);
		mProductDialog.setOnRestartOnClickListener(this);
		mProductDialog.show();
	}

	@Override
	public void restartScan()
	{
		initBeepSound();
		vibrate = true;
		if (mQrLineView != null && mAnimation != null)
		{
			mQrLineView.startAnimation(mAnimation);
		}
		mIsCanRestart = true;
		DecodeHandler.mIsCanDecode = true;
		if (mSearchEdt != null)
		{
			mSearchEdt.setText("");
		}
		if (mProductDialog != null)
		{
			mProductDialog.dismiss();
		}
		if (handler != null)
		{
			handler.sendEmptyMessage(R.id.restart_preview);
		}
	}

	@Override
	public void getCheckResult(CateItem item, String errmsg)
	{
		if (item != null)
		{
			showScanResultDialog(item);
		}
		else
		{
			if (TextUtils.isEmpty(errmsg))
			{
				errmsg = getResources().getString(
						R.string.shop_scan_controll_failed);
			}
			ToastHelper.showByGravity(this, errmsg, Gravity.CENTER);
			restartScan();
		}
	}

	@Override
	public void goBuy()
	{
		mIsCanRestart = true;
		DecodeHandler.mIsCanDecode = true;
		Log.d("goBuy", "true");
	}

	@Override
	public void onDimiss()
	{
		restartScan();
	}
}